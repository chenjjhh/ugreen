package me.hgj.jetpackmvvm.base

import android.app.Application
import android.content.Context

/**
 * User: wanlang_dev
 * Data: 2022/10/28
 * Time: 11:23
 * Desc:
 */
interface IModuleAppLifecycles {
    fun attachBaseContext(base: Context)
    fun onCreate(application: Application)
    fun onTerminal(application: Application)
}