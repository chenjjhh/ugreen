package com.wl.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wl.db.model.UgDevice

/**
 * User: wanlang_dev
 * Data: 2022/11/14
 * Time: 15:23
 * Desc:
 */
@Dao
interface UgDeviceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUgDevice(ugDevice: UgDevice): Long

    @Query("SELECT * FROM UgDevice ORDER BY id DESC")
    fun queryAll(): LiveData<List<UgDevice>>

    @Query("SELECT * FROM UgDevice WHERE id=:id")
    suspend fun getUgDeviceById(id: Long): UgDevice?

    @Query("SELECT * FROM UgDevice WHERE macAddress=:address")
    suspend fun getUgDeviceByAddress(address: String): UgDevice?

    @Query("DELETE FROM UgDevice WHERE id=:id")
    suspend fun deleteUgDevice(id: Long): Int

    @Delete
    suspend fun deleteUgDevices(devices: List<UgDevice>)

    @Query("DELETE FROM UgDevice")
    suspend fun deleteAll()
}