package com.wl.db

import android.content.Context
import androidx.room.Room
import com.wl.db.dao.UgDeviceDao

/**
 * User: wanlang_dev
 * Data: 2022/11/14
 * Time: 16:04
 * Desc:
 */
object UgDatabaseUtil {
    private var db: UgDatabase? = null

    val ugDeviceDao: UgDeviceDao?
        get() = db?.getUgDeviceDao()

    fun close() {
        db?.close()
    }

    fun initDataBase(context: Context) {
        db = Room.databaseBuilder(
            context.applicationContext,
            UgDatabase::class.java,
            "com.wl.db"
        )
            .build()
    }
}