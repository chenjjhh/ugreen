package com.wl.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * User: wanlang_dev
 * Data: 2022/11/14
 * Time: 15:15
 * Desc: 设备表  type 默认为1 代表耳机
 */
@Entity(tableName = "UgDevice")
data class UgDevice(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var name: String,
    val deviceName: String,
    val macAddress: String,
    val type: String = "1",
    val createAt: Date = Date(),
    var updateAt: Date = Date(),
)