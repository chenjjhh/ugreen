package com.wl.db

import androidx.room.Database
import androidx.room.TypeConverters
import androidx.room.RoomDatabase
import com.wl.db.dao.UgDeviceDao
import com.wl.db.model.UgDevice

/**
 * User: wanlang_dev
 * Data: 2022/11/14
 * Time: 15:36
 * Desc:
 */
@Database(
    version = 1,
    entities =[UgDevice::class],
    exportSchema = false
)
@TypeConverters(com.wl.db.TypeConverters::class)
abstract class UgDatabase : RoomDatabase() {
    abstract fun getUgDeviceDao(): UgDeviceDao
}