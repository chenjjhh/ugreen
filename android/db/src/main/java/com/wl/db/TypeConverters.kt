package com.wl.db

import androidx.room.TypeConverter
import java.util.*

/**
 * User: wanlang_dev
 * Data: 2022/11/14
 * Time: 15:38
 * Desc:
 */
class TypeConverters {
    @TypeConverter
    fun fromTimeStamp(long: Long) = Date(long)

    @TypeConverter
    fun fromDate(date: Date): Long = date.time
}