package com.wl.earbuds.ui.upgrade

import androidx.lifecycle.viewModelScope
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.earbuds.data.model.entity.EarbudsVersionBean
import com.wl.earbuds.utils.ext.mlog
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.StringObservableField
import zlc.season.downloadx.State
import zlc.season.downloadx.core.DownloadTask
import zlc.season.downloadx.download

class EarbudsUpgradeViewModel : BaseViewModel() {

    val statusStr: StringObservableField = StringObservableField("")
    val versionStr: StringObservableField = StringObservableField("")
    val dateStr: StringObservableField = StringObservableField("")
    val sizeStr: StringObservableField = StringObservableField("")
    val descStr: StringObservableField = StringObservableField("")

    var hasDownload: UnPeekLiveData<Boolean> = UnPeekLiveData(false)
    var downloadTask: DownloadTask? = null
    var downloadState = UnPeekLiveData<State>()

    fun isDownloading() = downloadTask?.isStarted() ?: false

    fun startDownload(verionBean: EarbudsVersionBean) {
        mlog("startDownload")
        if (downloadTask != null) {
            if (downloadTask!!.canStart()) {
                downloadTask!!.start()
                mlog("startDownload canStart")
                return
            }
            mlog("startDownload != null")
        }
        downloadTask = viewModelScope.download(verionBean.fileUrl).apply {
            state().onEach {
                downloadState.value = it
                if (it is State.Succeed) {
                    mlog("state: Succeed")
                    hasDownload.value = true
                }
            }.launchIn(viewModelScope)
            start()
        }
    }

    fun cancelDownload() {
        downloadTask?.stop()
    }

    fun getFilePath(): String {
        return downloadTask?.file()?.path ?: ""
    }
}