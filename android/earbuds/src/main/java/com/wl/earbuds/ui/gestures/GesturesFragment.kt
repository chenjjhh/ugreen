package com.wl.earbuds.ui.gestures

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.gyf.immersionbar.ImmersionBar
import com.wanlang.base.BaseDialog
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager.getGestureState
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.bluetooth.earbuds.setGesture
import com.wl.earbuds.data.model.entity.BatteryState
import com.wl.earbuds.data.model.entity.GestureBean
import com.wl.earbuds.data.model.entity.GestureState
import com.wl.earbuds.data.model.eumn.Gesture
import com.wl.earbuds.data.model.eumn.Operation
import com.wl.earbuds.databinding.FragmentGesturesBinding
import com.wl.earbuds.ui.adapter.GestureAdapter
import com.wl.earbuds.ui.device.DeviceListActivity
import com.wl.earbuds.ui.dialog.GestureCommandBottomDialog
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.generateLeftGestureData
import com.wl.earbuds.utils.generateRightGestureData
import com.wl.earbuds.utils.setAnimateAlpha
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration

class GesturesFragment : BaseFragment<GesturesViewModel, FragmentGesturesBinding>() {

    companion object {
        fun newInstance() = GesturesFragment()
    }

    private var leftData: MutableList<GestureBean> =
        mutableListOf()

    private var rightData: MutableList<GestureBean> =
        mutableListOf()

    private var mOperateDialog: GestureCommandBottomDialog? = null

    private val mGestureAdapter: GestureAdapter by lazy {
        GestureAdapter(leftData).apply {
            setOnItemClickListener { _, _, position ->
                if (!CurrentDeviceManager.isOnline()) return@setOnItemClickListener
                val batteryState = CurrentDeviceManager.getBatteryState()
                if (mDatabind.rbLeft.isChecked && !batteryState.isLeftOn()) return@setOnItemClickListener
                if (mDatabind.rbRight.isChecked && !batteryState.isRightOn()) return@setOnItemClickListener
                showGestureDialog(data[position].operation)
            }
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewmodel = mViewModel
        mDatabind.click = ProxyClick()
        initImmersionBar()
        initTitle()
        initRv()

    }

    private fun initImmersionBar() {
        ImmersionBar.with(this)
            .transparentStatusBar()
            .fitsSystemWindows(true)
            .statusBarDarkFont(true)// 默认状态栏字体颜色为黑色
            //            .keyboardEnable(true)  // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.earbuds_operation), enableBack = false) {
            startActivity(Intent(requireContext(), DeviceListActivity::class.java))
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            val decoration =
                SpinnerDividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(18f),
                    dp2px(18f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mGestureAdapter
            itemAnimator = null
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeGestureState(viewLifecycleOwner, ::onGestureChange)
        CurrentDeviceManager.observeBatteryState(viewLifecycleOwner, ::onBatteryChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("isOnline: $isOnline")
        updateStatus()
    }

    private fun onGestureChange(gestureState: GestureState) {
        leftData = generateLeftGestureData(requireContext(), gestureState)
        rightData = generateRightGestureData(requireContext(), gestureState)
        if (mDatabind.rbLeft.isChecked) {
            mGestureAdapter.setNewInstance(leftData)
        } else {
            mGestureAdapter.setNewInstance(rightData)
        }
    }

    private fun onBatteryChange(batteryState: BatteryState) {
        updateStatus()
    }

    private fun updateStatus() {
        val isOnline = CurrentDeviceManager.isOnline()
        val batteryState = CurrentDeviceManager.getBatteryState()
        if (mDatabind.rbLeft.isChecked) {
            val leftOn = batteryState.isLeftOn() && isOnline
            mGestureAdapter.isGrayMode = !leftOn
            setAnimateAlpha(
                mDatabind.rvItem, if (leftOn)
                    EarbudsConstants.ALPHA_ON_VALUE else EarbudsConstants.ALPHA_OFF_VALUE, true
            )
            if (!leftOn) {
                dismissDialog(true)
            }
        } else {
            val rightOn = batteryState.isRightOn() && isOnline
            mGestureAdapter.isGrayMode = !rightOn
            setAnimateAlpha(
                mDatabind.rvItem, if (rightOn)
                    EarbudsConstants.ALPHA_ON_VALUE else EarbudsConstants.ALPHA_OFF_VALUE, true
            )
            if (!rightOn) {
                dismissDialog(false)
            }
        }
    }

    private fun dismissDialog(isLeft: Boolean) {
        val dialog = mOperateDialog
        if (dialog != null ) {
            when (dialog.type) {
                Operation.LEFT_SINGLE,
                Operation.LEFT_DOUBLE,
                Operation.LEFT_TRIPLE,
                Operation.LEFT_LONG -> {
                    if (isLeft) dialog.dismiss()
                }
                Operation.RIGHT_SINGLE,
                Operation.RIGHT_DOUBLE,
                Operation.RIGHT_TRIPLE,
                Operation.RIGHT_LONG -> {
                    if (!isLeft) dialog.dismiss()
                }
            }
        }
    }

    private fun showGestureDialog(operation: Operation) {
        mOperateDialog = GestureCommandBottomDialog(requireContext(), operation)
        mOperateDialog?.setListener(object : GestureCommandBottomDialog.OnListener {
                override fun onCancel(baseDialog: BaseDialog) {
                    baseDialog.dismiss()
                }

                override fun onConfirm(baseDialog: BaseDialog, type: Operation, gesture: Gesture) {
                    val gestureState = getGestureState()
                    val newGestureState = when (type) {
                        Operation.LEFT_SINGLE -> gestureState.copy(leftSingle = gesture)
                        Operation.LEFT_DOUBLE -> gestureState.copy(leftDouble = gesture)
                        Operation.LEFT_TRIPLE -> gestureState.copy(leftTriple = gesture)
                        Operation.LEFT_LONG -> gestureState.copy(leftLong = gesture)
                        Operation.RIGHT_SINGLE -> gestureState.copy(rightSingle = gesture)
                        Operation.RIGHT_DOUBLE -> gestureState.copy(rightDouble = gesture)
                        Operation.RIGHT_TRIPLE -> gestureState.copy(rightTriple = gesture)
                        Operation.RIGHT_LONG -> gestureState.copy(rightLong = gesture)
                    }
                    setGesture(newGestureState)
                    baseDialog.dismiss()
                }
            })?.show()
    }

    private fun startLeftAnim() {
        mLeftAnimatorSet.setDuration(400).start()
    }

    private fun startRightAnim() {
        mRightAnimatorSet.setDuration(400).start()
    }

    private val mLeftAnimatorSet: AnimatorSet by lazy {
        AnimatorSet().apply {
            val ivRight = mDatabind.ivEarbudsRight
            val ivLeft = mDatabind.ivEarbudsLeft
            val leftAlphaAnimator = ObjectAnimator.ofFloat(ivLeft, "alpha", 0f, 1f)
            val leftTranslateAnimator =
                ObjectAnimator.ofFloat(ivLeft, "translationX", -ivLeft.width.toFloat(), 0f)
            val rightAlphaAnimator = ObjectAnimator.ofFloat(ivRight, "alpha", 1f, 0f)
            val rightTranslateAnimator =
                ObjectAnimator.ofFloat(ivRight, "translationX", 0f, ivRight.width.toFloat())
            play(rightAlphaAnimator).with(rightTranslateAnimator)
                .with(leftAlphaAnimator)
                .with(leftTranslateAnimator)
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator, isReverse: Boolean) {
                    super.onAnimationStart(animation, isReverse)
                }

                override fun onAnimationStart(animation: Animator) {
                }

                override fun onAnimationEnd(animation: Animator, isReverse: Boolean) {
                    super.onAnimationEnd(animation, isReverse)
                }

                override fun onAnimationEnd(animation: Animator) {
                    mViewModel.leftAlpha.set(1f)
                    mViewModel.rightAlpha.set(0f)
                }

                override fun onAnimationCancel(animation: Animator) {
                }

                override fun onAnimationRepeat(animation: Animator) {
                }
            })
        }
    }

    private val mRightAnimatorSet by lazy {
        AnimatorSet().apply {
            val ivRight = mDatabind.ivEarbudsRight
            val ivLeft = mDatabind.ivEarbudsLeft
            val rightAlphaAnimator = ObjectAnimator.ofFloat(ivRight, "alpha", 0f, 1f)
            val rightTranslateAnimator =
                ObjectAnimator.ofFloat(ivRight, "translationX", ivRight.width.toFloat(), 0f)
            val leftAlphaAnimator = ObjectAnimator.ofFloat(ivLeft, "alpha", 1f, 0f)
            val leftTranslateAnimator =
                ObjectAnimator.ofFloat(ivLeft, "translationX", 0f, -ivLeft.width.toFloat())
            play(rightAlphaAnimator).with(rightTranslateAnimator)
                .with(leftAlphaAnimator)
                .with(leftTranslateAnimator)
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator, isReverse: Boolean) {
                    super.onAnimationStart(animation, isReverse)
                }

                override fun onAnimationStart(animation: Animator) {
                }

                override fun onAnimationEnd(animation: Animator, isReverse: Boolean) {
                    super.onAnimationEnd(animation, isReverse)
                }

                override fun onAnimationEnd(animation: Animator) {
                    mViewModel.rightAlpha.set(1f)
                    mViewModel.leftAlpha.set(0f)
                }

                override fun onAnimationCancel(animation: Animator) {
                }

                override fun onAnimationRepeat(animation: Animator) {
                }
            })
        }
    }

    inner class ProxyClick {
        val onLeftCheckChange: CompoundButton.OnCheckedChangeListener =
            OnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    mDatabind.rbRight.isChecked = false
                    mGestureAdapter.setNewInstance(leftData)
                    updateStatus()
                    startLeftAnim()
                }
            }

        val onRightCheckChange: CompoundButton.OnCheckedChangeListener =
            OnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    mDatabind.rbLeft.isChecked = false
                    mGestureAdapter.setNewInstance(rightData)
                    updateStatus()
                    startRightAnim()
                }
            }
    }
}