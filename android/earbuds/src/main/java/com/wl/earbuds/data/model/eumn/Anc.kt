package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 16:44
 * Desc:
 */
enum class Anc(val value: Int) {
    ANC_ON(0x01),
    ANC_CLOSE(0x00),
    ANC_TRANS(0x02);

    companion object {
        @JvmStatic
        fun valueOf(value: Int): Anc? = values().find { type -> type.value == value.toInt() }
    }
}