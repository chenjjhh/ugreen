package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 17:51
 * Desc:
 */
data class ConfigState(val batteryState: BatteryState,
                       val ancState: AncState,
                       val eqState: EqualizerState,
                       val dualState: SwitchState,
                       val gameModeState: SwitchState,
                       val hqDecodeState: SwitchState,
                       val gestureState: GestureState,
                       val voiceState: VoiceState,
                       val leftFindState: FindState,
                       val rightFindState: FindState
)