package com.wl.earbuds.utils

import android.content.Context
import com.wl.earbuds.data.model.eumn.Anc
import com.wl.earbuds.data.model.eumn.AncLevel
import com.wl.earbuds.data.model.eumn.Equalizer

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 17:01
 * Desc:
 */

fun getAncText(context: Context, anc: AncLevel): String {
    return when (anc) {
        AncLevel.ANC_DEEP -> context.getString(com.wl.resource.R.string.anc_deep)
        AncLevel.ANC_MEDIUM -> context.getString(com.wl.resource.R.string.anc_medium)
        AncLevel.ANC_LIGHT -> context.getString(com.wl.resource.R.string.anc_deep)
    }
}

fun getEqualizerText(context: Context, equalizer: Equalizer): String {
    return when (equalizer) {
        Equalizer.CLASSIC -> {
            context.getString(com.wl.resource.R.string.equalizer_classic)
        }
        Equalizer.BASS -> {
            context.getString(com.wl.resource.R.string.equalizer_bass)
        }
        Equalizer.VOCAL -> {
            context.getString(com.wl.resource.R.string.equalizer_vocal)
        }
    }
}

fun getGameModeText(context: Context, boolean: Boolean): String {
    return if (boolean) {
        context.getString(com.wl.resource.R.string.mode_on)
    } else {
        context.getString(com.wl.resource.R.string.mode_off)
    }
}

