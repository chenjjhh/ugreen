package com.wl.earbuds.bluetooth.connect

class RescanChecker {
    private var _time: Long = 0

    fun isNeedRescan(): Boolean {
        val currentTime = System.currentTimeMillis()
        return currentTime - _time < 1000L
    }

    fun recordTime() {
        _time = System.currentTimeMillis()
    }

    fun reset() {
        _time = 0
    }
}