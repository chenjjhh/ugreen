package com.wl.earbuds.ui.home

import android.content.Intent
import android.os.Bundle
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import com.wl.earbuds.R
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.base.BaseActivity
import com.wl.earbuds.databinding.ActivityGuideBinding
import com.wl.earbuds.ui.adapter.GuideAdapter
import com.wl.earbuds.ui.device.DeviceListActivity
import com.wl.earbuds.utils.Constants
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class GuideActivity : BaseActivity<BaseViewModel, ActivityGuideBinding>() {

    private val mUrlData = mutableListOf<String>()

    private val mGuideAdapter by lazy {
        GuideAdapter(mUrlData).apply {
            setOnItemChildClickListener { _, _, _ ->
                startActivity(Intent(this@GuideActivity, DeviceListActivity::class.java))
                SettingsUtils.isGuideFinish = true
                finish()
            }
            addChildClickViewIds(R.id.btn_start)
        }
    }

    override fun initImmersionBar() {
        ImmersionBar.with(this)
            .transparentBar()
            .statusBarDarkFont(true)// 默认状态栏字体颜色为黑色
            //            .keyboardEnable(true)  // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init()
    }

    override fun initView(savedInstanceState: Bundle?) {
        intent.getStringArrayListExtra(Constants.START_IMAGES)?.let {
            mUrlData.clear()
            mUrlData.addAll(it)
        }
        initAdapter()
    }

    private fun initAdapter() {
        mDatabind.viewpager.apply {
            offscreenPageLimit = 2
            adapter = mGuideAdapter
            mDatabind.circleIndicator.setViewPager(this)
        }
    }
}