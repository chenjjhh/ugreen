package com.wl.earbuds.ui.home

import android.os.Bundle
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.databinding.FragmentHomeBinding
import com.wl.earbuds.utils.ext.clearToast
import com.wl.earbuds.utils.ext.init
import com.wl.earbuds.utils.ext.initMain
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class HomeFragment : BaseFragment<BaseViewModel, FragmentHomeBinding>() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun initView(savedInstanceState: Bundle?) {
        initTab()
    }

    override fun createObserver() {
        super.createObserver()
        appViewModel.openDeviceState.observe(viewLifecycleOwner, ::onOpenDevice)
    }

    private fun onOpenDevice(boolean: Boolean) {
        mDatabind.mainBottom.selectedItemId = com.wl.resource.R.id.menu_device
    }

    private fun initTab() {
        //初始化viewpager2
        mDatabind.mainViewpager.initMain(this)
        mDatabind.mainBottom.itemIconTintList = null
        mDatabind.mainBottom.clearToast(
            mutableListOf(
                com.wl.resource.R.id.menu_device,
                com.wl.resource.R.id.menu_operate,
                com.wl.resource.R.id.menu_about
            )
        )
        mDatabind.mainBottom.init {
            when (it) {
                com.wl.resource.R.id.menu_device -> {mDatabind.mainViewpager.setCurrentItem(0, false)}
                com.wl.resource.R.id.menu_operate -> {mDatabind.mainViewpager.setCurrentItem(1, false)}
                com.wl.resource.R.id.menu_about -> {mDatabind.mainViewpager.setCurrentItem(2, false)}
                else -> {}
            }
        }
    }


}