package com.wl.earbuds.bluetooth.core

import com.wanlang.base.BuildConfig
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.bluetooth.utils.BytesUtils.setUINT16Little
import com.wl.earbuds.bluetooth.utils.BytesUtils.setUINT8
import com.wl.earbuds.bluetooth.utils.crc16
import com.wl.earbuds.utils.ext.mlog

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 14:23
 * Desc:
 */
class CommandFrame(var commandId: Int = 0, var content: ByteArray = ByteArray(0)) : IFrame {
    companion object {
        val sendSof = ByteArray(3).apply {
            set(0, 0xAA.toByte())
            set(1, 0xBB.toByte())
            set(2, 0xCC.toByte())
        }

        val responseSof = ByteArray(3).apply {
            set(0, 0xDD.toByte())
            set(1, 0xEE.toByte())
            set(2, 0xFF.toByte())
        }

        val notify = ByteArray(3).apply {
            set(0, 0x85.toByte())
            set(1, 0x86.toByte())
            set(2, 0x87.toByte())
        }

        fun isResponse(data: ByteArray): Boolean {
            return data.size > 3 && data[0] == responseSof[0] &&
                    data[1] == responseSof[1] &&
                    data[2] == responseSof[2]
        }

        fun isNotify(data: ByteArray): Boolean {
            return data.size > 3 && data[0] == notify[0] &&
                    data[1] == notify[1] &&
                    data[2] == notify[2]
        }
    }

    private var frameType = FrameType.COMMAND

    override fun getFrameType() = frameType

    fun setFrameType(frameType: FrameType) {
        this.frameType = frameType
    }

    private var _isCrc = true
    private var _checksum = 0

    override fun getSendSof(): ByteArray {
        return sendSof
    }

    override fun getResponseSof(): ByteArray {
        return responseSof
    }

    override fun getData(): ByteArray {
        return content
    }

    override fun setData(data: ByteArray) {
        content = data
    }

    override fun isCrc() = _isCrc

    override fun getChecksum() = _checksum

    override fun format(): ByteArray {
        val length = sendSof.size + 1 + 1 + content.size + 2
        return ByteArray(length).apply {
            var offset = 0
            System.arraycopy(sendSof, 0, this, 0, sendSof.size)
            offset += sendSof.size
            setUINT8(commandId, this, offset)
            offset += 1
            setUINT8(content.size, this, offset)
            offset += 1
            System.arraycopy(content, 0, this, offset, content.size)
            offset += content.size
            val realContent = copyOfRange(3, 5 + content.size)
            _checksum = realContent.crc16()
            setUINT16Little(
                _checksum,
                this,
                offset
            )
            if (BuildConfig.DEBUG) {
                mlog("send: ${BytesUtils.byte2HexStr(this)}")
            }
        }
    }
}