package com.wl.earbuds.bluetooth.connect

import android.bluetooth.BluetoothDevice
import cn.wandersnail.bluetooth.BTManager
import cn.wandersnail.bluetooth.EventObserver

/**
 * User: wanlang_dev
 * Data: 2022/11/16
 * Time: 19:22
 * Desc:
 */
object ClassicManager {

    private var managerMap: MutableMap<String, DeviceManager> = mutableMapOf()

    private fun createConnection(device: BluetoothDevice) {
        if (managerMap[device.address] != null) return
        val connection = BTManager.getInstance().createConnection(device)
        getDeviceManager(device).connection = connection
    }

    fun connect(device: BluetoothDevice) {
        createConnection(device)
        getDeviceManager(device).connect()
    }

    fun removeDeviceManager(address: String) {
        managerMap.remove(address)
    }

    fun removeAllDeviceManager() {
        managerMap.clear()
    }

    fun getDeviceManagerByAddress(address: String): DeviceManager? {
        val upperAddress = address.uppercase()
        return managerMap[upperAddress]
    }

    private fun getDeviceManager(device: BluetoothDevice): DeviceManager {
        val upperAddress = device.address.uppercase()
        if (!managerMap.contains(upperAddress)) {
            managerMap[upperAddress] = DeviceManager(device)
        }
        return managerMap[upperAddress]!!
    }

    private val mEventObserver = object : EventObserver {
        override fun onBluetoothAdapterStateChanged(state: Int) {
        }

        override fun onRead(device: BluetoothDevice, value: ByteArray) {
            super.onRead(device, value)
        }

        override fun onWrite(
            device: BluetoothDevice,
            tag: String,
            value: ByteArray,
            result: Boolean
        ) {
            super.onWrite(device, tag, value, result)
        }

        override fun onConnectionStateChanged(device: BluetoothDevice, state: Int) {
            super.onConnectionStateChanged(device, state)
        }
    }
}