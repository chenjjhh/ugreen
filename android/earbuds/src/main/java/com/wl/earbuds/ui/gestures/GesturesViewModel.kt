package com.wl.earbuds.ui.gestures

import android.view.View
import androidx.databinding.ObservableInt
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.BooleanObservableField
import me.hgj.jetpackmvvm.callback.databind.FloatObservableField

class GesturesViewModel : BaseViewModel() {
    val leftAlpha = FloatObservableField(1f)
    val rightAlpha = FloatObservableField(0f)
    val leftChecked: BooleanObservableField = BooleanObservableField(true)
    val rightChecked: BooleanObservableField = BooleanObservableField(false)

    var leftEarbudsVisible = object : ObservableInt(leftChecked) {
        override fun get(): Int {
            return if (leftChecked.get()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }

    var leftEarbudsCheckedVisible = object : ObservableInt(leftChecked) {
        override fun get(): Int {
            return if (leftChecked.get()) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }

    var rightEarbudsVisible = object : ObservableInt(rightChecked) {
        override fun get(): Int {
            return if (rightChecked.get()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }

    var rightEarbudsCheckedVisible = object : ObservableInt(leftChecked) {
        override fun get(): Int {
            return if (rightChecked.get()) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }
}