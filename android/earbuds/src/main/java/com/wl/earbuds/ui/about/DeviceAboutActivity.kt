package com.wl.earbuds.ui.about

import android.os.Bundle
import com.wl.earbuds.base.BaseActivity
import com.wl.earbuds.databinding.ActivityDeviceAboutBinding
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class DeviceAboutActivity : BaseActivity<BaseViewModel, ActivityDeviceAboutBinding>() {
    override fun initView(savedInstanceState: Bundle?) {
    }

}