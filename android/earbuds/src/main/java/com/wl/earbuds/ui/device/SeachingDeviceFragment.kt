package com.wl.earbuds.ui.device

import android.os.Bundle
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.databinding.FragmentSeachingDeviceBinding
import com.wl.earbuds.utils.ext.initClose
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class SeachingDeviceFragment : BaseFragment<BaseViewModel, FragmentSeachingDeviceBinding>() {

    companion object {
        fun newInstance() = SeachingDeviceFragment()
    }

    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.title_search_device)) {
            activity?.onBackPressed()
        }
    }
}