package com.wl.earbuds.bluetooth.task;


import android.os.Process;

import androidx.annotation.NonNull;

import java.util.concurrent.ThreadFactory;

public class BackgroundThreadFactory implements ThreadFactory {

    private int count = 0;

    @Override
    public Thread newThread(@NonNull Runnable action) {
        Runnable runnable = () -> {
            try {
                Process.setThreadPriority(Process.THREAD_PRIORITY_MORE_FAVORABLE);
            }
            catch (Throwable ignored) {

            }
            action.run();
        };

        count++;
        return new Thread(runnable, "GaiaClient-" + count);
    }
}
