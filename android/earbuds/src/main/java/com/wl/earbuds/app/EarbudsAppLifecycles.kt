package com.wl.earbuds.app

import android.app.Application
import android.bluetooth.le.ScanSettings
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner
import cn.wandersnail.bluetooth.BTManager
import cn.wandersnail.commons.poster.ThreadMode
import com.hjq.toast.ToastUtils
import com.tencent.mmkv.MMKV
import com.wanlang.base.BuildConfig
import com.wl.db.UgDatabaseUtil
import com.wl.earbuds.bluetooth.connect.DeviceManager
import com.wl.earbuds.viewmodel.AppViewModel
import com.wl.earbuds.viewmodel.DeviceManagerViewModel
import me.hgj.jetpackmvvm.base.IModuleAppLifecycles
import me.hgj.jetpackmvvm.base.appContext


/**
 * User: wanlang_dev
 * Data: 2022/10/28
 * Time: 11:27
 * Desc:
 */
val appViewModel: AppViewModel by lazy { EarbudsAppLifecycles.appViewModelInstance }
val deviceManagerViewModel: DeviceManagerViewModel by lazy { EarbudsAppLifecycles.deviceManagerViewModelInstance }

class EarbudsAppLifecycles : IModuleAppLifecycles, ViewModelStoreOwner {

    companion object {
        lateinit var appViewModelInstance: AppViewModel
        lateinit var deviceManagerViewModelInstance: DeviceManagerViewModel
    }

    private lateinit var mAppViewModelStore: ViewModelStore
    private var mFactory: ViewModelProvider.Factory? = null

    override fun attachBaseContext(base: Context) {
    }

    override fun onCreate(application: Application) {
        MMKV.initialize(appContext, application.filesDir.absolutePath + "/mmkv")
        ToastUtils.init(application)
        initBtManager(application)
        UgDatabaseUtil.initDataBase(appContext)
        mAppViewModelStore = ViewModelStore()
        appViewModelInstance = getAppViewModelProvider()[AppViewModel::class.java]
        deviceManagerViewModelInstance = getAppViewModelProvider()[DeviceManagerViewModel::class.java]

    }

    private fun initBtManager(application: Application) {
        BTManager.Builder()
            .setObserveAnnotationRequired(false)
            .setMethodDefaultThreadMode(ThreadMode.BACKGROUND)
            .build().initialize(application)
        BTManager.isDebugMode = BuildConfig.DEBUG
    }

    override fun onTerminal(application: Application) {
    }

    /**
     * 获取一个全局的ViewModel
     */
    private fun getAppViewModelProvider(): ViewModelProvider {
        return ViewModelProvider(this, this.getAppFactory())
    }

    private fun getAppFactory(): ViewModelProvider.Factory {
        if (mFactory == null) {
            mFactory = ViewModelProvider.AndroidViewModelFactory.getInstance(appContext)
        }
        return mFactory as ViewModelProvider.Factory
    }

    override fun getViewModelStore(): ViewModelStore {
        return mAppViewModelStore
    }
}