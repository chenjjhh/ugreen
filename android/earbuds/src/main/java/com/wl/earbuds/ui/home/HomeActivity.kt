package com.wl.earbuds.ui.home

import android.content.Intent
import android.os.Bundle
import cn.wandersnail.bluetooth.BTManager
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.base.BaseActivity
import com.wl.earbuds.bluetooth.connect.ConnectManager
import com.wl.earbuds.databinding.ActivityHomeBinding
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.lifecycle.KtxActivityManger

class HomeActivity : BaseActivity<BaseViewModel, ActivityHomeBinding>() {

    private val mConnectManager by lazy {
        ConnectManager.getInstance(appContext)
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        appViewModel.openDeviceState.value = true
    }

    override fun createObserver() {
        super.createObserver()
    }

    override fun onDestroy() {
        if (!recreateFlag && KtxActivityManger.isLast) {
            BTManager.getInstance().releaseAllConnections()
            appViewModel.mIgnoreUpgrade = false
        }
        super.onDestroy()
    }

}