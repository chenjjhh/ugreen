package com.wl.earbuds.ui.device

import android.os.Bundle
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.ConnectManager
import com.wl.earbuds.databinding.FragmentNoBluetoothBinding
import com.wl.earbuds.utils.ext.initClose
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class NoBluetoothFragment : BaseFragment<BaseViewModel, FragmentNoBluetoothBinding>() {

    companion object {
        fun newInstance() = NoBluetoothFragment()
    }

    private val mConnectManager by lazy {
        ConnectManager.getInstance(appContext)
    }

    private var mRescanCallback: RescanCallback? = null

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        initTitle()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.title_search_device)) {
            requireActivity().onBackPressed()
        }
    }

    fun setRescanCallback(callback: RescanCallback) {
        mRescanCallback = callback
    }

    inner class ProxyClick {
        fun rescan() {
            if (mConnectManager.checkBluetooth(requireActivity())) {
                mRescanCallback?.onRescan()
            }
        }
    }
}