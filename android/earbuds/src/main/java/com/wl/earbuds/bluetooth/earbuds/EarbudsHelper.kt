package com.wl.earbuds.bluetooth.earbuds

import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.bluetooth.utils.ArrayUtil
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.data.model.entity.GestureState
import com.wl.earbuds.data.model.entity.OtaBean
import com.wl.earbuds.data.model.eumn.*

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:13
 * Desc:
 */
fun loadConfig() {

}

fun setAnc(anc: Anc, ancLevel: AncLevel) {
    var value = BytesUtils.setValueAsBits(anc.value, 0, 0)
    value = BytesUtils.setValueAsBits(ancLevel.value, value, 4)
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_ANC,
        generateByteArray(value),
        tag = EarbudsConstants.TAG_ANC
    )
}

fun setEq(equalizer: Equalizer) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_EQ,
        generateByteArray(equalizer.value),
        tag = EarbudsConstants.TAG_EQ
    )
}

fun setVoice(voice: Voice) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_VOICE,
        generateByteArray(voice.value),
        tag = EarbudsConstants.TAG_VOICE
    )
}

fun setGesture(gestureState: GestureState) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_GESTURE,
        ByteArray(8).apply {
            BytesUtils.setUINT8(gestureState.leftSingle.value, this, 0)
            BytesUtils.setUINT8(gestureState.leftDouble.value, this, 1)
            BytesUtils.setUINT8(gestureState.leftTriple.value, this, 2)
            BytesUtils.setUINT8(gestureState.leftLong.value, this, 3)
            BytesUtils.setUINT8(gestureState.rightSingle.value, this, 4)
            BytesUtils.setUINT8(gestureState.rightDouble.value, this, 5)
            BytesUtils.setUINT8(gestureState.rightTriple.value, this, 6)
            BytesUtils.setUINT8(gestureState.rightLong.value, this, 7)
        },
        tag = EarbudsConstants.TAG_GESTURE
    )
}

fun setFind(find: Find) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_FIND,
        ByteArray(2).apply {
            BytesUtils.setUINT8(find.action, this, 0)
            BytesUtils.setUINT8(find.key, this, 1)
        },
        tag = EarbudsConstants.TAG_FIND
    )
}

fun switchGameMode(boolean: Boolean) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_GAME_MODE,
        generateSwitchData(boolean),
        tag = EarbudsConstants.TAG_GAME_MODE
    )
}

fun switchHqDecode(boolean: Boolean) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_HQ_DECODE,
        generateSwitchData(boolean),
        tag = EarbudsConstants.TAG_HQ_DECODE
    )
}

fun switchDual(boolean: Boolean) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_DUAL,
        generateSwitchData(boolean),
        tag = EarbudsConstants.TAG_DUAL
    )
}

fun switchDualOff(index: Int, address: String) {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_DUAL,
        ByteArray(8).apply {
            BytesUtils.setUINT8(EarbudsConstants.SWITCH_OFF_VALUE, this, 0)
            BytesUtils.setUINT8(index, this, 1)
            BytesUtils.copyArray(ArrayUtil.hexStringToByte(address), this, 2)
        },
        tag = EarbudsConstants.TAG_DUAL
    )
}

fun checkInBox() {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_IN_BOX,
        ByteArray(0),
        tag = EarbudsConstants.TAG_IN_BOX
    )
}

fun setRecovery() {
    CurrentDeviceManager.getDeviceManager()?.sendCommand(
        EarbudsConstants.COMMAND_RECOVERY,
        ByteArray(0),
        tag = EarbudsConstants.TAG_RECOVERY
    )
}

fun startOta(otaBean: OtaBean) {
    CurrentDeviceManager.getDeviceManager()?.startOta(otaBean)
}

fun generateByteArray(value: Int) = ByteArray(1).apply {
    BytesUtils.setUINT8(value, this, 0)
}

fun generateSwitchData(boolean: Boolean) = ByteArray(1).apply {
    BytesUtils.setUINT8(if (boolean) EarbudsConstants.SWITCH_ON_VALUE else EarbudsConstants.SWITCH_OFF_VALUE, this, 0)
}