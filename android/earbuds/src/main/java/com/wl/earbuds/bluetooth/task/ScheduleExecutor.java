package com.wl.earbuds.bluetooth.task;

import androidx.annotation.NonNull;


import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScheduleExecutor {

    private final TaskManagerImpl mTaskManagerImpl;

    private final ScheduledExecutorService mScheduleExecutor = Executors.newScheduledThreadPool(2);

    private final ConcurrentHashMap<Runnable, ScheduledRunnable> mDelayedTasks =
            new ConcurrentHashMap<>();

    ScheduleExecutor(TaskManagerImpl taskManager) {
        // empty constructor
        mTaskManagerImpl = taskManager;
    }

    public void execute(@NonNull Runnable action, long delayInMs) {
        ScheduledRunnable scheduled = new ScheduledRunnable(action);
        mDelayedTasks.put(action, scheduled);
        mScheduleExecutor.schedule(scheduled, delayInMs, TimeUnit.MILLISECONDS);
    }

    public void cancel(@NonNull Runnable action) {
        ScheduledRunnable scheduled = mDelayedTasks.remove(action);
        if (scheduled != null) {
            scheduled.cancel();
        }
    }

    private class ScheduledRunnable implements Runnable {

        private final AtomicBoolean mmIsRunning = new AtomicBoolean(true);

        @NonNull
        private final Runnable mmAction;

        private final WeakReference<ConcurrentHashMap<Runnable, ScheduledRunnable>> mmTasksReference =
                new WeakReference<>(mDelayedTasks);

        ScheduledRunnable(@NonNull Runnable action) {
            this.mmAction = action;
        }

        void cancel() {
            mmIsRunning.set(false);
        }

        @Override
        public void run() {
            if (mmIsRunning.get()) {
                ConcurrentHashMap<Runnable, ScheduledRunnable> scheduledTasks =
                        mmTasksReference.get();
                if (scheduledTasks != null) {
                    scheduledTasks.remove(mmAction);
                }
                mTaskManagerImpl.runInBackground(mmAction);
            }
        }
    }

}
