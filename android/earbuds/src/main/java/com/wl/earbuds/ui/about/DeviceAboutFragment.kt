package com.wl.earbuds.ui.about

import android.content.Intent
import android.os.Bundle
import com.gyf.immersionbar.ImmersionBar
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.R
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.data.model.entity.AppVersionResponse
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.databinding.FragmentDeviceAboutBinding
import com.wl.earbuds.ui.language.LanguageActivity
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.LanguageUtils.getLanguageDesc
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.visible

class DeviceAboutFragment : BaseFragment<DeviceAboutViewModel, FragmentDeviceAboutBinding>() {

    companion object {
        fun newInstance() = DeviceAboutFragment()
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initImmersionBar()
        initTitle()
        mlog("AboutFragment initView")
    }

    private fun initImmersionBar() {
        ImmersionBar.with(this)
            .transparentStatusBar()
            .navigationBarColor(com.wanlang.base.R.color.transparent)
            .fitsSystemWindows(true)
            .statusBarDarkFont(true)// 默认状态栏字体颜色为黑色
            //            .keyboardEnable(true)  // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.tab_about)) {
            activity?.finish()
        }
    }

    override fun createObserver() {
        super.createObserver()
        mViewModel.appVersionState.observe(viewLifecycleOwner, ::onVersion)
        mViewModel.getAppVersion()
    }

    override fun initData() {
        super.initData()
        mViewModel.languageStr.set(getLanguageDesc(requireContext()))
        mViewModel.versionStr.set(BuildConfig.versionName)
    }

    private fun onVersion(uiState: ResponseUiState<AppVersionResponse>) {
        if (uiState.isSuccess) {
            val data = uiState.data!!
            if (BuildConfig.versionCode < data.versionCode) {
                mViewModel.versionStr.set(getString(com.wl.resource.R.string.app_find_new_version, BuildConfig.versionName))
                mDatabind.tvVersion.setTextColor(requireContext().getColor(com.wl.resource.R.color.standard_primary))
                mDatabind.upgradePoint.visible()
            }
        }
    }

    inner class ProxyClick {
        fun goUserProtocol() {
            safeNav {
                nav().navigate(R.id.action_deviceAbout_to_protocolWeb, Bundle().apply {
                    putInt(Constants.PROTOCOL_TYPE, Constants.PROTOCOL_USER)
                })
            }
        }

        fun goPrivacyPolice() {
            safeNav {
                nav().navigate(R.id.action_deviceAbout_to_protocolWeb, Bundle().apply {
                    putInt(Constants.PROTOCOL_TYPE, Constants.PROTOCOL_PRIVACY)
                })
            }
        }

        fun goLanguage() {
            startActivity(Intent(requireContext(), LanguageActivity::class.java))
        }

        fun goAppVersion() {
            // 当前已是最新版本,给出提示
            val appVersionResponse = mViewModel.appVersionState.value
            if (appVersionResponse != null && appVersionResponse.isSuccess &&
                BuildConfig.versionCode >= appVersionResponse.data!!.versionCode) {
                showMsg(getString(com.wl.resource.R.string.hint_current_verion_is_latest))
                return
            }
            safeNav {
                nav().navigate(R.id.action_deviceAbout_to_appVersion)
            }
        }
    }
}