package com.wl.earbuds.data.state

import com.wl.db.model.UgDevice
import com.wl.earbuds.data.model.entity.DeviceBean

/**
 * 作者: wanlang_dev
 * 日期: 2022/11/15 18:41
 * 描述:
 */
data class DeviceListUiState(
    val data: MutableList<UgDevice>
)