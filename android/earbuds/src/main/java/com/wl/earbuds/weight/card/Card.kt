package com.wl.earbuds.weight.card

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.wl.earbuds.R
import com.wl.earbuds.utils.ext.mlog

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 16:03
 * Desc:
 */
interface Card {

    abstract fun updateUI()

}