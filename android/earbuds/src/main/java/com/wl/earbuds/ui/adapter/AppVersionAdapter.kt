package com.wl.earbuds.ui.adapter

import android.view.View
import com.chad.library.adapter.base.BaseDelegateMultiAdapter
import com.chad.library.adapter.base.delegate.BaseMultiTypeDelegate
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.DelegateMultiEntity
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/30
 * Time: 9:47
 * Desc:
 */
class AppVersionAdapter(data: MutableList<DelegateMultiEntity>):
    BaseDelegateMultiAdapter<DelegateMultiEntity, BaseViewHolder>(data) {

    init {
        setMultiTypeDelegate(object : BaseMultiTypeDelegate<DelegateMultiEntity>() {
            override fun getItemType(data: List<DelegateMultiEntity>, position: Int): Int {
                return data[position].type
            }
        })
        getMultiTypeDelegate()!!.addItemType(
            DelegateMultiEntity.CURRENT_VERSION, R.layout.item_app_version
        ).addItemType(DelegateMultiEntity.NEW_VERSION, R.layout.item_app_new_version)
    }

    override fun convert(holder: BaseViewHolder, item: DelegateMultiEntity) {
        when (item.type) {
            DelegateMultiEntity.CURRENT_VERSION -> {
                holder.setText(R.id.tv_version, item.version.name)
                if (item.version.isLatest) {
                    holder.getView<View>(R.id.tv_is_latest).visible()
                } else {
                    holder.getView<View>(R.id.tv_is_latest).gone()
                }
            }
            DelegateMultiEntity.NEW_VERSION -> {
                holder.setText(R.id.tv_version, item.version.name)
                holder.setText(R.id.tv_content, item.version.content)
            }
        }
    }
}