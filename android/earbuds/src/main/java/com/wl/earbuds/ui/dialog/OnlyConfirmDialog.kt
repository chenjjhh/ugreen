package com.wl.earbuds.ui.dialog

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.wanlang.base.BaseDialog
import com.wl.earbuds.R

/**
 * User: wanlang_dev
 * Data: 2022/11/9
 * Time: 14:36
 * Desc:
 */
class OnlyConfirmDialog {
    class Builder(context: Context): BaseDialog.Builder<Builder>(context) {
        private var _actionListener: ActioinListener? = null
        private val mTitle: TextView? by lazy { findViewById(R.id.tv_title) }
        private val mContent: TextView? by lazy { findViewById(R.id.tv_content) }
        private val mConfirmView: Button? by lazy { findViewById(R.id.btn_confirm) }

        init {
            setContentView(R.layout.dialog_only_confirm)
            mConfirmView?.setOnClickListener {
                _actionListener?.onConfirm(getDialog())
            }
        }

        fun setTitle(title: String): Builder {
            mTitle?.text = title
            return this
        }

        fun setContent(content: String): Builder {
            mContent?.text = content
            return this
        }

        fun setConfirm(confirm: String): Builder {
            mConfirmView?.text = confirm
            return this
        }

        fun setActionListener(listener: ActioinListener?): Builder {
            _actionListener = listener
            return this
        }
    }

    interface ActioinListener{

        fun onConfirm(dialog: BaseDialog?) {

        }
    }
}