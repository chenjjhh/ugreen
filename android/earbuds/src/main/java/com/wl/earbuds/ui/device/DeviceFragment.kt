package com.wl.earbuds.ui.device

import android.Manifest
import android.bluetooth.*
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.recyclerview.widget.LinearLayoutManager
import cn.wandersnail.bluetooth.BTManager
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.wanlang.base.BaseDialog
import com.wl.db.model.UgDevice
import com.wl.earbuds.R
import com.wl.earbuds.app.deviceManagerViewModel
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.*
import com.wl.earbuds.data.state.DeviceListUiState
import com.wl.earbuds.databinding.FragmentDeviceBinding
import com.wl.earbuds.ui.adapter.DeviceAdapter
import com.wl.earbuds.bluetooth.utils.BluetoothUtils
import com.wl.earbuds.data.model.entity.SavedDevice
import com.wl.earbuds.ui.about.DeviceAboutActivity
import com.wl.earbuds.ui.dialog.OnlyConfirmDialog
import com.wl.earbuds.ui.home.HomeActivity
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.RxIntentTool
import com.wl.earbuds.utils.RxLocationTool
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.postDelay
import com.wl.earbuds.utils.ext.showMsg
import com.wl.earbuds.weight.decoration.SpaceItemDecoration
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.ext.lifecycle.KtxActivityManger
import me.hgj.jetpackmvvm.ext.util.bluetoothManager
import me.hgj.jetpackmvvm.ext.view.invisible
import me.hgj.jetpackmvvm.ext.view.visible


class DeviceFragment : BaseFragment<DeviceViewModel, FragmentDeviceBinding>(), ConnectListener {

    private val mBtAdapter by lazy {
        requireContext().bluetoothManager?.adapter
    }

    companion object {
        fun newInstance() = DeviceFragment()
    }

    private val mConnectManager =
        ConnectManager.getInstance(appContext)

    private var data: MutableList<UgDevice> = mutableListOf<UgDevice>()
    private var mIsDiscovery = false
    private var mIsChecked = false

    private val mDeviceAdapter: DeviceAdapter by lazy {
        DeviceAdapter(data).apply {
            setOnItemClickListener { adapter, view, position ->
                val item = data.get(position)
                mlog("setOnItemClickListener $item")
                if (isSelectMode) {
                    if (selectList.contains(item)) {
                        selectList.remove(item)
                    } else {
                        selectList.add(item)
                    }
                    mDatabind.btnDelete.isEnabled = selectList.isNotEmpty()
                    notifyItemChanged(position)
                } else if (BTManager.getInstance()
                        .getConnection(item.macAddress)?.isConnected == true
                ) {
                    // 打开设备页
                    openDetail(
                        device = mBtAdapter?.getRemoteDevice(item.macAddress),
                        ugDevice = item
                    )
                } else {
                    openDetail(ugDevice = data[position])
                }
            }
            setOnItemChildClickListener { adapter, view, position ->
                val item = data.get(position)
                mlog("setOnItemChildClickListener $item")
                if (view.id == R.id.tv_connect) {
                    if (BTManager.getInstance()
                            .getConnection(item.macAddress)?.isConnected == true
                    ) {
                        openDetail(
                            device = mBtAdapter?.getRemoteDevice(item.macAddress),
                            ugDevice = item
                        )
                        return@setOnItemChildClickListener
                    }
                    if (mConnectManager.checkBluetooth(requireActivity())) {
                        connectUgDevice(data[position])
                    }
                }
            }
            setOnItemLongClickListener { adapter, view, position ->
                if (isSelectMode) {
                    exitSelectMode()
                } else {
                    enterSelectMode()
                }
                true
            }
            addChildClickViewIds(R.id.tv_connect)
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        initRv()
        initBack()
        mConnectManager.addConnectListener(this)
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mDeviceAdapter
            addItemDecoration(SpaceItemDecoration(bottom = dp2px(18f)))
            mDeviceAdapter.setEmptyView(R.layout.layout_device_empty)
            itemAnimator = null
        }
    }

    override fun createObserver() {
        super.createObserver()
        //mViewModel.getConfig()
        deviceManagerViewModel.deviceListUiState.observe(viewLifecycleOwner, ::updateDevice)
        deviceManagerViewModel.ugDeviceLiveData.observe(this, ::onUgDeviceSave)
        deviceManagerViewModel.bgConnectedDeviceState.observe(this, ::onDeviceConnectedOnBackground)
    }

    override fun initData() {
        super.initData()
        deviceManagerViewModel.getDeviceList()
    }

    private fun initBack() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (mDeviceAdapter.isSelectMode) {
                        exitSelectMode()
                    } else {
                        requireActivity().finish()
                    }
                }
            })
    }

    private fun updateDevice(state: DeviceListUiState) {
        data = state.data
        mDeviceAdapter.setNewInstance(data)
    }

    private fun onUgDeviceSave(savedDevice: SavedDevice) {
        mlog("onUgDeviceSave ${savedDevice.device.address}")
        // 自动连接打开设备页
        if (mDeviceAdapter.targetDevice == null || mDeviceAdapter.targetDevice != savedDevice.ugDevice) return
        connectUgDevice(ugDevice = savedDevice.ugDevice)
    }

    private fun onDeviceConnectedOnBackground(device: BluetoothDevice) {
        mlog("onDeviceConnectedOnBackground $device")
        if (!CurrentDeviceManager.isOnline()) {
            data.firstOrNull {
                device.address == it.macAddress
            }?.let { ugDevice ->
                openDetail(device, ugDevice)
            }
        }
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
        mlog("onResume")
        mConnectManager.canRecordBgDevice = true
        postDelay(100) {
            if (!mIsChecked) {
                mIsChecked = true
                if (KtxActivityManger.firstActivity !is HomeActivity) {
                    checkBluetooth()
                }
            }
        }
        mDeviceAdapter.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()
        mlog("onPause")
    }

    override fun onStop() {
        super.onStop()

    }

    private fun openDetail(
        device: BluetoothDevice? = null,
        ugDevice: UgDevice? = mDeviceAdapter.targetDevice
    ) {
        CurrentDeviceManager.device = device
        CurrentDeviceManager.ugDevice = ugDevice
        startActivity(Intent(requireContext(), HomeActivity::class.java))
        mDeviceAdapter.targetDevice = null
        mConnectManager.canRecordBgDevice = false
    }

    private fun checkBluetooth() {
        // 判断蓝牙是否打开
        if (!mBtAdapter!!.isEnabled) {
            // if (requireContext().checkSelfPermission(
            //         Manifest.permission.BLUETOOTH_CONNECT
            //     ) == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                XXPermissions.with(this@DeviceFragment).permission(
                    Permission.BLUETOOTH_CONNECT,
                ).request { _, all ->
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        startActivity(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
                    } else {
                        if (checkSelfPermission(
                                requireContext(),
                                Manifest.permission.BLUETOOTH_CONNECT
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            startActivityForResult(
                                Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                                Constants.ACTION_ENABLE_CODE
                            )
                        }
                    }
                }
            } else {
                startActivityForResult(
                    Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                    Constants.ACTION_ENABLE_CODE
                )
            }
        }
    }

    private fun checkSearch(): Boolean {
        if (mBtAdapter == null) {
            showMsg(getString(com.wl.resource.R.string.bluetooth_not_support))
            return false
        }
        // 判断蓝牙是否打开
        if (!mBtAdapter!!.isEnabled) {
            checkBluetooth()
            //蓝牙未打开不允许跳转
            return false
        }
        // gps是否打开
        if (RxLocationTool.isGpsAvailable(requireContext()) && !RxLocationTool.isGpsEnabled(
                requireContext()
            )
        ) {
            OnlyConfirmDialog.Builder(requireContext())
                .setTitle(getString(com.wl.resource.R.string.operate_hint_title))
                .setContent(getString(com.wl.resource.R.string.need_open_gps))
                .setConfirm(getString(com.wl.resource.R.string.go_settings))
                .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                    override fun onConfirm(dialog: BaseDialog?) {
                        RxLocationTool.openGpsSettings(requireContext())
                        dialog?.dismiss()
                    }
                })
                .show()
            return false
        }
        // 是否有定位权限
        if (RxLocationTool.isGpsAvailable(requireContext()) &&
            (requireContext().checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED ||
                    requireContext().checkSelfPermission(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED)
        ) {
            OnlyConfirmDialog.Builder(requireContext())
                .setTitle(getString(com.wl.resource.R.string.need_permission))
                .setContent(getString(com.wl.resource.R.string.need_location_permission))
                .setConfirm(getString(com.wl.resource.R.string.grant))
                .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                    override fun onConfirm(dialog: BaseDialog?) {
                        XXPermissions.with(context).permission(
                            Permission.ACCESS_FINE_LOCATION,
                            Permission.ACCESS_COARSE_LOCATION
                        ).request(object : OnPermissionCallback {
                            override fun onGranted(
                                permissions: MutableList<String>?,
                                all: Boolean
                            ) {
                            }

                            override fun onDenied(
                                permissions: MutableList<String>?,
                                never: Boolean
                            ) {
                                super.onDenied(permissions, never)
                                if (never && SettingsUtils.locationIgnore) {
                                    startActivity(
                                        RxIntentTool.getAppDetailsSettingsIntent(
                                            requireContext()
                                        )
                                    )
                                }
                                SettingsUtils.locationIgnore = never
                            }
                        })
                        dialog?.dismiss()
                    }
                })
                .show()
            return false
        }
        if (!(BluetoothUtils.areScanPermissionsGranted(requireContext()) &&
                    BluetoothUtils.areConnectPermissionsGranted(requireContext())
                    )
        ) {
            OnlyConfirmDialog.Builder(requireContext())
                .setTitle(getString(com.wl.resource.R.string.need_permission))
                .setContent(getString(com.wl.resource.R.string.need_scan_connect_permission))
                .setConfirm(getString(com.wl.resource.R.string.grant))
                .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                    override fun onConfirm(dialog: BaseDialog?) {
                        XXPermissions.with(requireContext()).permission(
                            Permission.BLUETOOTH_SCAN,
                            Permission.BLUETOOTH_CONNECT,
                        ).request(object : OnPermissionCallback {
                            override fun onGranted(
                                permissions: MutableList<String>?,
                                all: Boolean
                            ) {
                            }

                            override fun onDenied(
                                permissions: MutableList<String>?,
                                never: Boolean
                            ) {
                                super.onDenied(permissions, never)
                                if (never && SettingsUtils.scanIgnore) {
                                    startActivity(
                                        RxIntentTool.getAppDetailsSettingsIntent(
                                            requireContext()
                                        )
                                    )
                                }
                                SettingsUtils.scanIgnore = never
                            }
                        })
                        dialog?.dismiss()
                    }
                })
                .show()
            return false
        }
        return true
    }

    override fun onDeviceListUpdate(devices: MutableList<BluetoothDevice>) {
        super.onDeviceListUpdate(devices)
        if (mDeviceAdapter.targetDevice == null) return
        devices.find { it.address == mDeviceAdapter.targetDevice!!.macAddress }?.let {
            if (BTManager.getInstance().isDiscovering) {
                BTManager.getInstance().stopDiscovery()
            }
            mlog("onDeviceListUpdate")
            BTManager.getInstance().createBond(it)
//            ClassicManager.connect(it)
        }
    }

    private fun connectUgDevice(ugDevice: UgDevice) {
        mlog("connectUgDevice $ugDevice")
        mDeviceAdapter.targetDevice = ugDevice
        mConnectManager.connectUgDevice(ugDevice)
    }

    override fun onDeviceBond(device: BluetoothDevice) {
        super.onDeviceBond(device)
        if (KtxActivityManger.currentActivity !is DeviceListActivity) return
        if (mDeviceAdapter.targetDevice == null || mDeviceAdapter.targetDevice!!.macAddress != device.address) return
        mlog("onDeviceBond ${device.name}")
        ClassicManager.connect(device)
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        if (KtxActivityManger.currentActivity !is DeviceListActivity) return
        // if (mDeviceAdapter.targetDevice == null || mDeviceAdapter.targetDevice!!.macAddress != device.address) return
        openDetail(device = device, deviceManagerViewModel.getUgdeviceByAddress(device.address))
    }

    override fun onPairRefused(device: BluetoothDevice) {
        if (KtxActivityManger.currentActivity !is DeviceListActivity) return
        mlog("onPairRefused")
        super.onPairRefused(device)
        if (mDeviceAdapter.targetDevice == null || mDeviceAdapter.targetDevice!!.macAddress != device.address) return
        showMsg(getString(com.wl.resource.R.string.connect_fail))
        mDeviceAdapter.targetDevice = null
    }

    override fun onDiscoveryStart() {
        super.onDiscoveryStart()
        mIsDiscovery = true
    }

    override fun onDiscoveryFinished(devices: MutableList<BluetoothDevice>) {
        if (mDeviceAdapter.targetDevice == null) return
        if (!mIsDiscovery) return
        if (devices.find { it.address == mDeviceAdapter.targetDevice!!.macAddress } == null) {
            mDeviceAdapter.targetDevice = null
            showMsg(getString(com.wl.resource.R.string.no_device_find))
        }
        mIsDiscovery = false
    }

    override fun onDiscoveryFail(status: Int) {
        super.onDiscoveryFail(status)
        showMsg(getString(com.wl.resource.R.string.discovery_fail))
    }

    override fun onConnectFail(address: String, status: Int) {
        mlog("onConnectFail $address, $status")
        if (mDeviceAdapter.targetDevice != null && mDeviceAdapter.targetDevice!!.macAddress == address) {
            when (status) {
                ConnectError.DEVICE_NOT_FOUND -> {
                    showMsg(getString(com.wl.resource.R.string.scan_device_not_found))
                }
                ConnectError.CONNECT_DISCONNECT,
                ConnectError.CONNECT_FAIL -> {
                    showMsg(getString(com.wl.resource.R.string.connect_fail))
                }
            }
            mDeviceAdapter.targetDevice = null
        } else {
            mDeviceAdapter.notifyDataSetChanged()
        }
    }

    private fun exitSelectMode() {
        mDeviceAdapter.isSelectMode = false
        mDatabind.btnCancel.invisible()
        mDatabind.btnDelete.invisible()
        mDatabind.btnAddDevice.visible()
    }

    private fun enterSelectMode() {
        mDeviceAdapter.isSelectMode = true
        mDatabind.btnCancel.visible()
        mDatabind.btnDelete.visible()
        mDatabind.btnDelete.isEnabled = false
        mDatabind.btnAddDevice.invisible()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mConnectManager.canRecordBgDevice = false
        mConnectManager.removeConnectListener(this)
    }

    inner class ProxyClick {
        fun goSearching() {
            if (checkSearch()) {
                mDeviceAdapter.targetDevice = null
                startActivity(Intent(context, DeviceScanActivity::class.java))
            }
        }

        fun cancelDelete() {
            exitSelectMode()
        }

        fun deleteDevice() {
            mutableListOf<UgDevice>().apply {
                addAll(mDeviceAdapter.selectList)
                val macList = map { it.macAddress }
                ConnectManager.getInstance(requireContext()).disconnectAllDevice(macList)
                macList.forEach { address ->
                    BTManager.getInstance().disconnectConnection(address)
                }
                mViewModel.deleteUgDevices(this)
                exitSelectMode()
                mDeviceAdapter.selectList.clear()
            }
        }

        fun goDeviceAbout() {
            requireContext().startActivity(Intent(context, DeviceAboutActivity::class.java))
        }
    }

}