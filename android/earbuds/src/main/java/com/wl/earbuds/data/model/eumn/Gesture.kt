package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:23
 * Desc:
 */
enum class Gesture(val value: Int) {
    NONE(0x00),
    PLAY_PAUSE(0x01),
    VOLUME_UP(0x02),
    VOLUME_DOWN(0x03),
    // forward
    NEXT(0x04),
    // backward
    PREVIOUS(0x05),
    VA(0x06),
    GAME_MODE(0x07),
    ANC(0x08),
    CALL(0x09);


    companion object {
        @JvmStatic
        fun valueOf(value: Int): Gesture = values().find { type -> type.value == value.toInt() } ?: NONE
    }
}