package com.wl.earbuds.bluetooth.task;

import androidx.annotation.NonNull;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * All messages from the SendingThread and ReceivingThread are run using a ThreadPoolExecutor to
 * avoid blocking them or the MainThread.
 */
final class TaskManagerImpl {

    private final LinkedBlockingQueue<Runnable> mTasksQueue = new LinkedBlockingQueue<>();

    private final MainThreadExecutor mMainExecutor = new MainThreadExecutor();

    private final ThreadPoolExecutor mBackgroundExecutor = new ThreadPoolExecutor(2, 4, 60L,
                                                                            TimeUnit.SECONDS,
                                                                            mTasksQueue,
                                                                            new BackgroundThreadFactory());

    private final ScheduleExecutor mScheduleExecutor = new ScheduleExecutor(this);

    TaskManagerImpl() {
        // empty constructor
    }

    public void runOnMain(@NonNull Runnable runnable) {
        executeOnMainThread(runnable);
    }

    public void runInBackground(@NonNull Runnable runnable) {
        executeOnBackgroundThread(runnable);
    }

    void schedule(@NonNull Runnable runnable, long delayMs) {
        executeDelayedTask(runnable, delayMs);
    }

    void cancelScheduled(@NonNull Runnable runnable) {
        cancelDelayedTask(runnable);
    }

    public void release() {
        resetExecutor();
    }

    private void executeOnMainThread(@NonNull Runnable runnable) {
        mMainExecutor.execute(runnable);
    }

    private void executeOnBackgroundThread(@NonNull Runnable runnable) {
        mBackgroundExecutor.execute(runnable);
    }

    private void executeDelayedTask(@NonNull Runnable runnable, long delayInMs) {
        mScheduleExecutor.execute(runnable, delayInMs);
    }

    private void cancelDelayedTask(@NonNull Runnable runnable) {
        mScheduleExecutor.cancel(runnable);
    }

    private void resetExecutor() {
        mTasksQueue.clear();
        mBackgroundExecutor.purge();
    }

}
