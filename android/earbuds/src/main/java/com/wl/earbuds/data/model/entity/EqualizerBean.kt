package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Equalizer

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:33
 * Desc:
 */
data class EqualizerBean(val equalizer: Equalizer, val name: String)