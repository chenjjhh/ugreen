package com.wl.earbuds.bluetooth.connect

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import cn.wandersnail.bluetooth.BTManager
import cn.wandersnail.bluetooth.Connection
import cn.wandersnail.bluetooth.DiscoveryListener
import cn.wandersnail.bluetooth.EventObserver
import cn.wandersnail.commons.poster.RunOn
import cn.wandersnail.commons.poster.ThreadMode
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.wanlang.base.BaseDialog
import com.wl.db.model.UgDevice
import com.wl.earbuds.app.deviceManagerViewModel
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.bluetooth.utils.BluetoothUtils
import com.wl.earbuds.ui.dialog.OnlyConfirmDialog
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.InvorkUtils
import com.wl.earbuds.utils.RxIntentTool
import com.wl.earbuds.utils.RxLocationTool
import com.wl.earbuds.utils.ext.mlog
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.ext.util.bluetoothManager
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

/**
 * User: wanlang_dev
 * Data: 2022/11/15
 * Time: 14:12
 * Desc:
 */
class ConnectManager private constructor(private val context: Context) {

    companion object {
        private var sInstance: ConnectManager? = null

        fun getInstance(context: Context): ConnectManager {
            synchronized(this) {
                if (sInstance == null) sInstance = ConnectManager(context.applicationContext)
                return sInstance!!
            }
        }
    }

    private val mBtAdapter by lazy {
        context.bluetoothManager?.adapter
    }

    private var deviceList: MutableList<BluetoothDevice> = mutableListOf()
    private var mTargetDevice: BluetoothDevice? = null
    private var mTargetUgDevice: UgDevice? = null
    private var mIsCancelDiscovery: Boolean = false
    private var mIsInitialized: Boolean = false
    private var autoConnectFlag: Boolean = false
    private var isDiscovery: Boolean = false
    private var isAddBondDevices: Boolean = false
    private var rescanChecker = RescanChecker()
    private var lastStartTime: Long = 0
    var canRecordBgDevice = false

    // 允许列表页切换设备标识
    private var a2dpFlag: Boolean = false
    private var mReflectMethod: Method? = null
    private var mNeedConnectList = mutableListOf<String>()
    private var mNeedDisconnectList = mutableListOf<String>()

    private val mConnectListeners = mutableListOf<ConnectListener>()

    private val mEventObserver: EventObserver by lazy {
        object : EventObserver {

            @RunOn(ThreadMode.MAIN)
            override fun onConnectionStateChanged(device: BluetoothDevice, state: Int) {
                mlog("ConnectManager onConnectionStateChanged ${device.address}  $state")
                if (!isAvailableDevice(device)) {
                    mlog("not available Device")
                    return
                }
                super.onConnectionStateChanged(device, state)
                when (state) {
                    Connection.STATE_CONNECTED -> {
                        mConnectListeners.onEach {
                            it.onDeviceConnected(device)
                        }
                    }
                    Connection.STATE_PAIRED -> {
                        mConnectListeners.onEach {
                            it.onDeviceBond(device)
                        }
                    }
                    Connection.STATE_PAIRED_REFUSED -> {
                    }
                    Connection.STATE_DISCONNECTED -> {
                        checkConnectFail(device, ConnectError.CONNECT_DISCONNECT)
                    }
                }
            }
        }
    }

    private val mServiceLister by lazy {
        object :
            BluetoothProfile.ServiceListener {
            @SuppressLint("MissingPermission")
            override fun onServiceConnected(type: Int, profile: BluetoothProfile) {
                if (type == BluetoothProfile.A2DP) {
                    mlog("mA2dpProfile Connected")
                    profile as BluetoothA2dp

                    mNeedConnectList.forEach { address ->
                        val find = profile.connectedDevices.find { it.address == address }
                        mlog("mA2dpProfile find $find")
                        val bluetoothDevice = mBtAdapter?.getRemoteDevice(address)
                        if (bluetoothDevice != null) {
                            val result = InvorkUtils.connectA2dp(profile, bluetoothDevice)
                            mlog("mA2dpProfile connect result: $result")
                        }
                    }
                    /*mA2dpProfile = profile
                    if (a2dpFlag) {
                        a2dpFlag = false
                        connectHeadset()
                    }*/
                }
                if (type == BluetoothProfile.HEADSET) {
                    mlog("mHeadsetProfile Connected $autoConnectFlag ${mNeedConnectList.size}")
                    profile as BluetoothHeadset
                    if (autoConnectFlag) {
                        autoConnectFlag = false
                        checkAutoConnect(profile)
                    } else {
                        mNeedConnectList.forEach { address ->
                            val find = profile.connectedDevices.find { it.address == address }
                            mlog("mHeadsetProfile find $find")
                            if (find == null) {
                                val bluetoothDevice = mBtAdapter?.getRemoteDevice(address)
                                mlog(
                                    "mHeadsetProfile audio ${
                                        profile.isAudioConnected(
                                            bluetoothDevice
                                        )
                                    }"
                                )
                                if (bluetoothDevice != null) {
                                    val result =
                                        InvorkUtils.connectHeadset(profile, bluetoothDevice)
                                    if (!result) {
                                        checkConnectFail(bluetoothDevice, ConnectError.CONNECT_FAIL)
                                    }
                                    mlog("mHeadsetProfile connect result: $result")
                                }
                            } else {
                                mConnectListeners.onEach {
                                    it.onHeadsetConnected(find)
                                }
                            }
                        }
                    }
                    /*mHeadsetProfile = profile
                    if (autoConnectFlag) {
                        autoConnectFlag = false
                        checkAutoConnect()
                    }*/
                }
            }

            override fun onServiceDisconnected(type: Int) {
                if (type == BluetoothProfile.A2DP) {
                    mlog("mA2dpProfile Disconnected")
                }
                if (type == BluetoothProfile.HEADSET) {
                    mlog("mHeadsetProfile Disconnected")
                }
            }
        }
    }

    private val mDisconnectServiceLister by lazy {
        object :
            BluetoothProfile.ServiceListener {
            override fun onServiceConnected(type: Int, profile: BluetoothProfile) {
                if (type == BluetoothProfile.A2DP) {
                    mlog("mA2dpProfile disconnect")
                    profile as BluetoothA2dp
                    mNeedDisconnectList.forEach { address ->
                        val bluetoothDevice = mBtAdapter?.getRemoteDevice(address)
                        if (bluetoothDevice != null) {
                            val result = InvorkUtils.disconnectA2dp(profile, bluetoothDevice)
                            mlog("mA2dpProfile disconnect result: $result")
                        }
                    }
                }
                if (type == BluetoothProfile.HEADSET) {
                    mlog("mHeadsetProfile disconnect")
                    profile as BluetoothHeadset
                    mNeedDisconnectList.forEach { address ->
                        val bluetoothDevice = mBtAdapter?.getRemoteDevice(address)
                        if (bluetoothDevice != null) {
                            val result = InvorkUtils.disconnectHeadset(profile, bluetoothDevice)
                            mlog("mHeadsetProfile disconnect result: $result")
                        }
                    }
                }
            }

            override fun onServiceDisconnected(type: Int) {

            }
        }
    }

    private val discoveryListener: DiscoveryListener = object : DiscoveryListener {

        override fun onDiscoveryStart() {
            mConnectListeners.onEach {
                this@ConnectManager.mlog("onDiscoveryStart")
                it.onDiscoveryStart()
                val currentTime = System.currentTimeMillis()
                if (currentTime - lastStartTime < 1000) {
                    return
                }
                lastStartTime = currentTime
                deviceList.clear()
                if (isAddBondDevices) {
                    isAddBondDevices = false
                    addBondedDevice()
                }
                updateDeviceList()
            }
        }

        override fun onDiscoveryStop() {
            if (mIsCancelDiscovery) {
                mIsCancelDiscovery = false
                this@ConnectManager.mlog("onDiscoveryStop ignoreed")
                return
            }
            if (rescanChecker.isNeedRescan()) {
                this@ConnectManager.mlog("onDiscoveryStop rescan")
                scanDevice()
                return
            }
            this@ConnectManager.mlog("onDiscoveryStop")
            mConnectListeners.onEach {
                it.onDiscoveryFinished(deviceList)
            }
            if (!isDiscovery) return
            if (mTargetDevice != null) {
                if (deviceList.contains(mTargetDevice)) return
                mConnectListeners.onEach {
                    it.onConnectFail(mTargetDevice!!.address, ConnectError.DEVICE_NOT_FOUND)
                }
            } else if (mTargetUgDevice != null) {
                if (deviceList.find { it.address == mTargetUgDevice?.macAddress } != null) return
                mConnectListeners.onEach {
                    it.onConnectFail(mTargetUgDevice!!.macAddress, ConnectError.DEVICE_NOT_FOUND)
                }
            }
        }

        @SuppressLint("MissingPermission")
        override fun onDeviceFound(device: BluetoothDevice, rssi: Int) {
            this@ConnectManager.mlog("onFoundDevice ${device.name}")
            // 操作太快时可能不会调onDiscoveryStart，在这里添加已绑定设备
            if (isAddBondDevices) {
                rescanChecker.reset()
                isAddBondDevices = false
                updateDeviceList()
                this@ConnectManager.mlog("onDeviceAdd ${deviceList.size}")
            }
            // end
            if (!(device.type == BluetoothDevice.DEVICE_TYPE_CLASSIC ||
                        device.type == BluetoothDevice.DEVICE_TYPE_DUAL) || device.name == null
            ) return
            if (deviceList.contains(device)) return
            if (!isAvailableDevice(device)) return
            deviceList.add(device)
            updateDeviceList()

            checkDeviceFound(device)
        }

        override fun onDiscoveryError(errorCode: Int, errorMsg: String) {
            mConnectListeners.onEach {
                it.onDiscoveryFail(errorCode)
            }
        }
    }

    private val mBroadcastReceiver by lazy {
        object : BroadcastReceiver() {
            @SuppressLint("MissingPermission")
            override fun onReceive(context: Context, intent: Intent) {
                val device: BluetoothDevice =
                    intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE) ?: return
                if (!isAvailableDevice(device)) return
                if (intent.action == BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED) {
                    val status: Int = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, -1)
                    if (status != -1) {
                        when (status) {
                            BluetoothProfile.STATE_CONNECTED -> {
                                mlog("A2dp STATE_CONNECTED ${device!!.name}")
                            }
                            BluetoothProfile.STATE_DISCONNECTED -> {
                                mlog("A2dp STATE_DISCONNECTED ${device!!.name}")
                                // checkConnectFail(device, ConnectError.CONNECT_FAIL)
                            }
                            BluetoothProfile.STATE_DISCONNECTING -> {
                                mlog("A2dp STATE_DISCONNECTING ${device!!.name}")
                            }
                            BluetoothProfile.STATE_CONNECTING -> {
                                mlog("A2dp STATE_CONNECTING ${device!!.name}")
                            }
                        }
                    }
                } else if (intent.action == BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED) {
                    val status: Int = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, -1)
                    if (status != -1) {
                        when (status) {
                            BluetoothProfile.STATE_CONNECTED -> {
                                mlog("Headset STATE_CONNECTED ${device.name}")
                                mConnectListeners.onEach {
                                    it.onHeadsetConnected(device)
                                }
                            }
                            BluetoothProfile.STATE_DISCONNECTED -> {
                                mConnectListeners.onEach {
                                    it.onHeadsetDisconnected(device)
                                }
                                checkConnectFail(device, ConnectError.CONNECT_FAIL)
                                mlog("Headset STATE_DISCONNECTED ${device.name}")
                            }
                            BluetoothProfile.STATE_DISCONNECTING -> {
                                mlog("Headset STATE_DISCONNECTING ${device.name}")
                            }
                            BluetoothProfile.STATE_CONNECTING -> {
                                mlog("Headset STATE_CONNECTING ${device.name}")
                            }
                        }
                    }
                } else if (intent.action == BluetoothDevice.ACTION_BOND_STATE_CHANGED) {
                    val bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1)
                    mlog("bondState: $bondState")
                    if (bondState != BluetoothDevice.BOND_BONDED && bondState != BluetoothDevice.BOND_BONDING) {
                        mConnectListeners.onEach {
                            it.onPairRefused(device)
                        }
                    }
                }
            }
        }
    }

    init {
        mlog("ConnectManager init")
        BTManager.getInstance().registerObserver(mEventObserver)
        initDiscovery()
    }

    fun initialize() {
        if (mIsInitialized) return
        initReceiver()
        mIsInitialized = true
        addConnectListener(mAutoConnectListener)
    }

    fun release() {
        if (mIsInitialized) {
            appContext.unregisterReceiver(mBroadcastReceiver)
            removeConnectListener(mAutoConnectListener)
            mIsInitialized = false
        }
    }

    fun getTargetDevice() = mTargetDevice

    private fun initReceiver() {
        appContext.registerReceiver(
            mBroadcastReceiver,
            IntentFilter(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED).apply {
                addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED)
                addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
            }
        )
    }

    fun scanDevice() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.BLUETOOTH_SCAN
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mTargetDevice = null
        mTargetUgDevice = null
        val isDistorvery = BTManager.getInstance().isDiscovering
        if (isDistorvery) {
            rescanChecker.recordTime()
        }
        mlog("startDiscovery $isDistorvery")
        // addBondedDevice()
        isAddBondDevices = true
        isDiscovery = true
        BTManager.getInstance().startDiscovery()
    }

    private fun checkDeviceFound(device: BluetoothDevice) {
        if (mTargetDevice != null && mTargetDevice == device) {
            mConnectListeners.onEach {
                it.onDeviceFound(device)
            }
        }
        if (mTargetUgDevice != null && mTargetUgDevice!!.macAddress == device.address) {
            mConnectListeners.onEach {
                it.onDeviceFound(device)
            }
        }
    }

    private fun checkConnectFail(device: BluetoothDevice, errorCode: Int) {
        if (mTargetDevice == device || mTargetUgDevice?.macAddress == device.address) {
            mConnectListeners.onEach {
                it.onConnectFail(device.address, errorCode)
            }
            mTargetDevice = null
            mTargetUgDevice = null
        } else if (errorCode == ConnectError.CONNECT_DISCONNECT) {
            mConnectListeners.onEach {
                it.onConnectFail(device.address, errorCode)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun bondDevice(device: BluetoothDevice) {
        mTargetDevice = device
        device.createBond()
    }

    @SuppressLint("MissingPermission")
    fun connectDevice(device: BluetoothDevice) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.BLUETOOTH_SCAN
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        // ClassicManager.createConnection(device)
        // BTManager.getInstance().createBond(device)
        mTargetDevice = device
        mTargetUgDevice = null
        if (BTManager.getInstance().isDiscovering) {
            mIsCancelDiscovery = true
            BTManager.getInstance().stopDiscovery()
        }
        isDiscovery = false
        connectDeviceWithAddress(device.address)
    }

    fun connectDeviceNoSearch(device: BluetoothDevice) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.BLUETOOTH_SCAN
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        // ClassicManager.createConnection(device)
        // BTManager.getInstance().createBond(device)
        mTargetDevice = device
        mTargetUgDevice = null
        if (BTManager.getInstance().isDiscovering) {
            mIsCancelDiscovery = true
            BTManager.getInstance().stopDiscovery()
        }
        isDiscovery = false
        connectDeviceWithAddressNoSearch(device.address)
    }

    fun connectUgDevice(ugDevice: UgDevice) {
        mTargetUgDevice = ugDevice
        mTargetDevice = null
        if (BTManager.getInstance().isDiscovering) {
            mIsCancelDiscovery = true
            BTManager.getInstance().stopDiscovery()
        }
        isDiscovery = false
        connectDeviceWithAddress(ugDevice.macAddress)
    }

    @SuppressLint("MissingPermission")
    private fun addBondedDevice() {
        mBtAdapter?.bondedDevices?.filter { device ->
            mlog("${device.name}")
            (isAvailableDevice(device)) &&
                    isBtConnected(device)
        }?.let {
            deviceList.addAll(it)
        }
    }

    private fun updateDeviceList() {
        mConnectListeners.onEach {
            it.onDeviceListUpdate(deviceList)
        }
    }

    @SuppressLint("MissingPermission")
    private fun connectDeviceWithAddress(address: String) {
        val bluetoothDevice = mBtAdapter?.bondedDevices?.find {
            it.address == address
        }
        if (bluetoothDevice == null) {
            onDeviceNotBond()
        } else {
            mNeedConnectList.clear()
            mNeedConnectList.add(address)
            mTargetDevice = bluetoothDevice
            a2dpFlag = true
            mBtAdapter!!.getProfileProxy(context, mServiceLister, BluetoothProfile.A2DP)
            mBtAdapter!!.getProfileProxy(context, mServiceLister, BluetoothProfile.HEADSET)
            // connectHeadset()
        }
    }

    @SuppressLint("MissingPermission")
    private fun connectDeviceWithAddressNoSearch(address: String) {
        val bluetoothDevice = mBtAdapter?.bondedDevices?.find {
            it.address == address
        }
        if (bluetoothDevice == null) {
            mConnectListeners.onEach {
                it.onDeviceFound(mTargetDevice!!)
            }
        } else {
            mNeedConnectList.clear()
            mNeedConnectList.add(address)
            mTargetDevice = bluetoothDevice
            a2dpFlag = true
            mBtAdapter!!.getProfileProxy(context, mServiceLister, BluetoothProfile.A2DP)
            mBtAdapter!!.getProfileProxy(context, mServiceLister, BluetoothProfile.HEADSET)
            // connectHeadset()
        }
    }

    fun connectAllDeviceAddress(list: List<String>) {
        mNeedConnectList.clear()
        mNeedConnectList.addAll(list)

    }

    /**
     * 设备未配对
     */
    private fun onDeviceNotBond() {
        if (mTargetDevice != null && deviceList.contains(mTargetDevice)) {
            mlog("checkDeviceFound")
            checkDeviceFound(mTargetDevice!!)
        } else {
            mlog("startDiscovery")
            isDiscovery = true
            BTManager.getInstance().startDiscovery()
        }
    }

    /*@SuppressLint("MissingPermission")
    private fun connectHeadset(profile: BluetoothHeadset) {
        if (profile.getConnectionState(mTargetDevice!!) == BluetoothProfile.STATE_CONNECTED) {
            ClassicManager.connect(mTargetDevice!!)
        } else {
            InvorkUtils.connectHeadset(profile, mTargetDevice)
            ClassicManager.connect(mTargetDevice!!)
        }
    }*/

    fun prepareAutoConnect() {
        if (mBtAdapter == null) return
        autoConnectFlag = true
        mBtAdapter!!.getProfileProxy(context, mServiceLister, BluetoothProfile.HEADSET)
    }

    @SuppressLint("MissingPermission")
    private fun checkAutoConnect(profile: BluetoothHeadset) {
        val list = mutableListOf<BluetoothDevice>()
        if (BluetoothUtils.areConnectPermissionsGranted(appContext)) {
            val connectedDevices = profile.connectedDevices
            if (connectedDevices.isNotEmpty()) {
                list.addAll(connectedDevices.filter { device ->
                    isAvailableDevice(device)
                })
            }
        }
        mlog("checkAutoConnect ${list.size}")
        mConnectListeners.onEach {
            it.onAutoConnectable(list)
        }

    }

    @SuppressLint("MissingPermission")
    fun disconnectDevice(address: String) {
        val bluetoothDevice = mBtAdapter?.bondedDevices?.find {
            it.address == address
        }
        if (bluetoothDevice != null) {
            mNeedDisconnectList.clear()
            mNeedDisconnectList.add(address)
            mBtAdapter!!.getProfileProxy(
                context,
                mDisconnectServiceLister,
                BluetoothProfile.A2DP
            )
            mBtAdapter!!.getProfileProxy(
                context,
                mDisconnectServiceLister,
                BluetoothProfile.HEADSET
            )
        }
    }

    fun disconnectAllDevice(addresses: List<String>) {
        mNeedDisconnectList.clear()
        mNeedDisconnectList.addAll(addresses)
        mBtAdapter!!.getProfileProxy(context, mDisconnectServiceLister, BluetoothProfile.A2DP)
        mBtAdapter!!.getProfileProxy(
            context,
            mDisconnectServiceLister,
            BluetoothProfile.HEADSET
        )
    }

    @SuppressLint("MissingPermission")
    private fun isAvailableDevice(device: BluetoothDevice): Boolean {
        return device.name == Constants.HEADSET_UGREEN_PREFIX
    }

    fun checkIsTargetDevice(device: BluetoothDevice): Boolean {
        return mTargetDevice != null && device == mTargetDevice
    }

    fun addConnectListener(listener: ConnectListener) {
        if (mConnectListeners.contains(listener)) return
        mConnectListeners.add(listener)
    }

    fun removeConnectListener(listener: ConnectListener) {
        mConnectListeners.remove(listener)
    }

    private fun initDiscovery() {
        BTManager.getInstance().addDiscoveryListener(discoveryListener)
    }

    @SuppressLint("MissingPermission")
    fun checkBluetooth(context: Activity): Boolean {
        if (mBtAdapter == null) {
            return false
        }
        // 判断蓝牙是否打开
        if (!mBtAdapter!!.isEnabled) {
            // if (requireContext().checkSelfPermission(
            //         Manifest.permission.BLUETOOTH_CONNECT
            //     ) == PackageManager.PERMISSION_GRANTED) {
            XXPermissions.with(context).permission(
                Permission.BLUETOOTH_CONNECT,
                Permission.BLUETOOTH_SCAN,
            ).request { _, _ ->
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    if (BluetoothUtils.areScanPermissionsGranted(context) &&
                        BluetoothUtils.areConnectPermissionsGranted(context)
                    ) {
                        context.startActivityForResult(
                            Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                            Constants.ACTION_ENABLE_CODE
                        )
                    }
                } else {
                    context.startActivityForResult(
                        Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                        Constants.ACTION_ENABLE_CODE
                    )
                }
            }
            return false
        }
        // gps是否打开
        if (RxLocationTool.isGpsAvailable(context) && !RxLocationTool.isGpsEnabled(
                context
            )
        ) {
            OnlyConfirmDialog.Builder(context)
                .setTitle(context.getString(com.wl.resource.R.string.operate_hint_title))
                .setContent(context.getString(com.wl.resource.R.string.need_open_gps))
                .setConfirm(context.getString(com.wl.resource.R.string.go_settings))
                .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                    override fun onConfirm(dialog: BaseDialog?) {
                        RxLocationTool.openGpsSettings(context)
                        dialog?.dismiss()
                    }
                })
                .show()
            return false
        }
        // 是否有定位权限
        if (RxLocationTool.isGpsAvailable(context) &&
            (context.checkSelfPermission(
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED ||
                    context.checkSelfPermission(
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED)
        ) {
            OnlyConfirmDialog.Builder(context)
                .setTitle(context.getString(com.wl.resource.R.string.need_permission))
                .setContent(context.getString(com.wl.resource.R.string.need_location_permission))
                .setConfirm(context.getString(com.wl.resource.R.string.grant))
                .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                    override fun onConfirm(dialog: BaseDialog?) {
                        XXPermissions.with(context).permission(
                            Permission.ACCESS_FINE_LOCATION,
                            Permission.ACCESS_COARSE_LOCATION
                        ).request(object : OnPermissionCallback {
                            override fun onGranted(
                                permissions: MutableList<String>?,
                                all: Boolean
                            ) {
                            }

                            override fun onDenied(
                                permissions: MutableList<String>?,
                                never: Boolean
                            ) {
                                super.onDenied(permissions, never)
                                if (never && SettingsUtils.locationIgnore) {
                                    goAppSettings(context)
                                }
                                SettingsUtils.locationIgnore = never
                            }
                        })
                        dialog?.dismiss()
                    }
                })
                .show()
            return false
        }
        if (!(BluetoothUtils.areScanPermissionsGranted(context) &&
                    BluetoothUtils.areConnectPermissionsGranted(context)
                    )
        ) {

            OnlyConfirmDialog.Builder(context)
                .setTitle(context.getString(com.wl.resource.R.string.need_permission))
                .setContent(context.getString(com.wl.resource.R.string.need_scan_connect_permission))
                .setConfirm(context.getString(com.wl.resource.R.string.grant))
                .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                    override fun onConfirm(dialog: BaseDialog?) {
                        XXPermissions.with(context).permission(
                            Permission.BLUETOOTH_SCAN,
                            Permission.BLUETOOTH_CONNECT,
                        ).request(object : OnPermissionCallback {
                            override fun onGranted(
                                permissions: MutableList<String>?,
                                all: Boolean
                            ) {
                            }

                            override fun onDenied(
                                permissions: MutableList<String>?,
                                never: Boolean
                            ) {
                                super.onDenied(permissions, never)
                                if (never && SettingsUtils.scanIgnore) {
                                    goAppSettings(context)
                                }
                                SettingsUtils.scanIgnore = never
                            }
                        })
                        dialog?.dismiss()
                    }
                })
                .show()
            return false
        }
        return true
    }

    fun goAppSettings(activity: Activity) {
        activity.startActivity(RxIntentTool.getAppDetailsSettingsIntent(activity))
    }

    fun isBtConnected(bondedDevice: BluetoothDevice): Boolean {
        try {
            if (mReflectMethod == null) {
                //使用反射调用被隐藏的方法
                mReflectMethod =
                    BluetoothDevice::class.java.getDeclaredMethod(
                        "isConnected"
                    )
                mReflectMethod!!.isAccessible = true
            }
            val isConnected =
                mReflectMethod?.invoke(bondedDevice) as Boolean
            if (isConnected) {
                return true
            }
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
        return false
    }

    private val mAutoConnectListener: ConnectListener by lazy {
        object : ConnectListener {
            override fun onDeviceBond(device: BluetoothDevice) {
                super.onDeviceBond(device)
            }

            override fun onHeadsetConnected(device: BluetoothDevice) {
                super.onHeadsetConnected(device)
                mlog("onHeadsetConnected ${device.address}")
                // 是当前设备，连接设备
              //  val ugDevice = deviceManagerViewModel.getUgdeviceByAddress(device.address) ?: return
                mlog("onHeadsetConnected connect")
                ClassicManager.connect(device)
            }

            override fun onHeadsetDisconnected(device: BluetoothDevice) {
                super.onHeadsetDisconnected(device)
                // 是当前设备，断开连接
                if (device == CurrentDeviceManager.device && CurrentDeviceManager.isOnline()) {
                    CurrentDeviceManager.disconnect()
                }
            }

            override fun onDeviceConnected(device: BluetoothDevice) {
                super.onDeviceConnected(device)
                if (canRecordBgDevice) {
                    deviceManagerViewModel.bgConnectedDeviceState.value = device
                }
            }
        }
    }
}