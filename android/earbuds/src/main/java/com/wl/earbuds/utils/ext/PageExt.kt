package com.wl.earbuds.utils.ext

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.utils.LifecycleHandler

/**
 * 作者: wanlang_dev
 * 日期: 2022/4/8 10:06
 * 描述:
 */
//post, postDelay
fun FragmentActivity.post(action: () -> Unit) {
    LifecycleHandler(this).post { action() }
}

fun FragmentActivity.postDelay(delay: Long = 0, action: () -> Unit) {
    LifecycleHandler(this).postDelayed({ action() }, delay)
}

fun Fragment.post(action: () -> Unit) {
    LifecycleHandler(this).post { action() }
}

fun Fragment.postDelay(delay: Long = 0, action: () -> Unit) {
    LifecycleHandler(this).postDelayed({ action() }, delay)
}

/*
fun BaseFragment<*, *>.showConfirmDialog(
    hint: String,
    content: String,
    confirmText: String,
    title: String = requireContext().getString(R.string.dialog_title_unbind),
    enableTouchOutside: Boolean = true,
    confirmListener: DialogClickListener? = null
) {
    ConfirmDialogBuilder(requireActivity())
        .setContentHint(hint)
        .setContent(content)
        .setTitle(title)
        .setConfirmText(confirmText)
        .setCanceledOnTouchOutside(enableTouchOutside)
        .setOnConfirmListener(confirmListener)
        .show()
}

fun BaseFragment<*, *>.showGenderDialog(
    listener: GenderListener? = null
) {
    GenderDialogBuilder(requireActivity())
        .setOnGenderListener(listener)
        .show()
}*/
