package com.wl.earbuds.bluetooth.earbuds

import com.wl.earbuds.data.model.entity.BatteryState
import com.wl.earbuds.data.model.entity.ConfigState
import com.wl.earbuds.data.model.entity.VersionState

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 15:07
 * Desc:
 */
interface ParserListener {

    fun onConfigUpdate(configState: ConfigState)

    fun onBatteryUpdate(batteryState: BatteryState)

    fun onVersionUpdate(versionState: VersionState)


}