package com.wl.earbuds.ui.dialog

import android.content.Context
import android.content.Intent
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import com.wanlang.base.BottomSheetDialog
import com.wl.earbuds.R
import com.wl.earbuds.ui.web.ContentActivity
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.ext.click
import com.wl.earbuds.utils.ext.mlog

/**
 * User: wanlang_dev
 * Data: 2022/11/8
 * Time: 17:34
 * Desc:
 */
class ProtocolBottomDialog(context: Context) : BottomSheetDialog(context) {

    private val mRejectView: View? by lazy { findViewById(R.id.btn_reject) }
    private val mAgreeView: Button? by lazy { findViewById(R.id.btn_agree) }
    private val mCheckBox: CheckBox? by lazy { findViewById(R.id.checkbox) }
    private val mTvProtocol: TextView? by lazy { findViewById(R.id.tv_protocol) }
    private var _mProtocolClickListener: ProtocolClickListener? = null

    init {
        setContentView(R.layout.dialog_protocol)
        setCanceledOnTouchOutside(false)
        setCancelable(false)
        mTvProtocol?.movementMethod = LinkMovementMethod.getInstance()
        mTvProtocol?.text = buildSpannedString {
            inSpans(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    mCheckBox?.toggle()
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.color = getColor(com.wl.resource.R.color.text_primary)
                    ds.isUnderlineText = false
                }
            }) {
                append(getString(com.wl.resource.R.string.privacy_policy_prefix))
            }
            inSpans(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    mlog("click user protocol")
                    context.startActivity(Intent(context, ContentActivity::class.java).apply {
                        putExtra(Constants.PROTOCOL_TYPE, Constants.PROTOCOL_USER)
                    })
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.color = getColor(com.wl.resource.R.color.standard_primary)
                    ds.isUnderlineText = false
                }
            }) {
                append(getString(com.wl.resource.R.string.user_protocol))
            }
            append(getString(com.wl.resource.R.string.and))
            inSpans(object : ClickableSpan() {
                override fun onClick(widget: View) {
                    mlog("click privacy policy")
                    context.startActivity(Intent(context, ContentActivity::class.java).apply {
                        putExtra(Constants.PROTOCOL_TYPE, Constants.PROTOCOL_PRIVACY)
                    })
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.color = getColor(com.wl.resource.R.color.standard_primary)
                    ds.isUnderlineText = false
                }
            }) {
                append(getString(com.wl.resource.R.string.privacy_policy))
            }
            append(getString(com.wl.resource.R.string.privacy_policy_suffix))
        }
        mCheckBox?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mAgreeView?.setBackgroundResource(com.wl.resource.R.drawable.shape_play_enable)
                mAgreeView?.setTextColor(context.getColor(R.color.white))
            } else {
                mAgreeView?.setBackgroundResource(com.wl.resource.R.drawable.shape_play_disable)
                mAgreeView?.setTextColor(context.getColor(com.wl.resource.R.color.standard_primary))
            }
        }
        mRejectView?.click {
            _mProtocolClickListener?.onReject()
        }
        mAgreeView?.click {
            _mProtocolClickListener?.onAgree(mCheckBox!!.isChecked)
        }
    }

    fun setProtocolClickListener(listener: ProtocolClickListener?) {
        _mProtocolClickListener = listener
    }

    interface ProtocolClickListener {
        fun onReject()
        fun onAgree(isChecked: Boolean)
    }

}