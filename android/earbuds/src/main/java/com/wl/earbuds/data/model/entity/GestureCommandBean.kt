package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Gesture

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:33
 * Desc:
 */
data class GestureCommandBean(val command: String, val gesture: Gesture, val hint: String="")