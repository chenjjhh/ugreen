package com.wl.earbuds.data.model.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * User: wanlang_dev
 * Data: 2022/11/29
 * Time: 15:49
 * Desc:
 */
@Parcelize
data class EarbudsVersionBean(
    @SerializedName("iotVersion")
    val versionName: String,
    @SerializedName("filePath")
    val fileUrl: String,
    val fileSize: String,
    val fileMd5: String,
    @SerializedName("releaseTime")
    val dateStr: String,
    @SerializedName("updateContent")
    val content: String,
): Parcelable