package com.wl.earbuds.bluetooth.analyser;

import androidx.annotation.NonNull;

import com.wl.earbuds.bluetooth.task.TaskManager;


public abstract class StreamAnalyser {

    private final StreamAnalyserListener mListener;

    public StreamAnalyser(StreamAnalyserListener listener) {
        mListener = listener;
    }

    public abstract void reset();

    public abstract void analyse(@NonNull TaskManager taskManager, @NonNull byte[] data);

    public StreamAnalyserListener getListener() {
        return mListener;
    }
}
