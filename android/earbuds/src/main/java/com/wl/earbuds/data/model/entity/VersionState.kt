package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 15:00
 * Desc:
 */
data class VersionState(
    val leftVersion: String = "",
    val rightVersion: String = ""
) {
    fun getVersion(): String {
        if (leftVersion == "0.0.0") {
            if (rightVersion == "0.0.0") {
                return ""
            }
            return rightVersion
        }
        return leftVersion

    }
}