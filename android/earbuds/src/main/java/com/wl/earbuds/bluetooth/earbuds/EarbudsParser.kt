package com.wl.earbuds.bluetooth.earbuds

import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.data.model.entity.*
import com.wl.earbuds.data.model.eumn.*
import com.wl.earbuds.utils.ext.mlog
import java.nio.charset.StandardCharsets

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 14:48
 * Desc:
 */
class EarbudsParser(val listener: EarbudsListener) {

    fun parseResponse(data: ByteArray) {
        val commandId = BytesUtils.getUINT8(data, 0)
        val status = BytesUtils.getUINT8(data, 1)
        val length = BytesUtils.getUINT8(data, 2)
        if (status == EarbudsConstants.STATUS_FAIL) {

            mlog("status == 0")
            return
        }
        try {
            val cmdData = data.copyOfRange(3, 3 + length)
            when (commandId) {
                EarbudsConstants.COMMAND_VERSION -> {
                    parseVersionResponse(cmdData)
                }
                EarbudsConstants.COMMAND_CONFIG -> {
                    parseConfigResponse(cmdData)
                }
                EarbudsConstants.COMMAND_EQ -> {
                    parseEqResponse(cmdData)
                }
                EarbudsConstants.COMMAND_FIND -> {
                    parseFindResponse(cmdData)
                }
                EarbudsConstants.COMMAND_DUAL -> {
                    parseDualResponse(cmdData)
                }
                EarbudsConstants.COMMAND_GAME_MODE -> {
                    parseGameModeResponse(cmdData)
                }
                EarbudsConstants.COMMAND_ANC -> {
                    parseAncResponse(cmdData)
                }
                EarbudsConstants.COMMAND_VOICE -> {
                    parseVoiceResponse(cmdData)
                }
                EarbudsConstants.COMMAND_GESTURE -> {
                    parseGestureResponse(cmdData)
                }
                EarbudsConstants.COMMAND_DUAL_DEVICE -> {
                    parseDualDevice(cmdData)
                }
                EarbudsConstants.COMMAND_CODEC_TYPE -> {
                    parseCodecType(cmdData)
                }
                EarbudsConstants.COMMAND_IN_BOX -> {
                    parseInBox(cmdData)
                }
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun parseNotify(data: ByteArray) {
        val length = BytesUtils.getUINT8(data, 0)
        val type = BytesUtils.getUINT8(data, 1)
        try {
            val notifyData = data.copyOfRange(2, data.size)
            when (type) {
                EarbudsConstants.NOTIFICATION_TYPE_BATTERY -> {
                    parseBatteryNotification(notifyData)
                }
                EarbudsConstants.NOTIFICATION_TYPE_ANC -> {
                    parseAncNotification(notifyData)
                }
                EarbudsConstants.NOTIFICATION_TYPE_CONNECT -> {
                    // 不知道用途
                }
                EarbudsConstants.NOTIFICATION_TYPE_GAME_MODE -> {
                    parseGameModeNotification(notifyData)
                }
                EarbudsConstants.NOTIFICATION_TYPE_EQ -> {
                    parseEqNotification(notifyData)
                }
                EarbudsConstants.NOTIFICATION_TYPE_CODEC -> {
                    parseCodecNotification(notifyData)
                }
                EarbudsConstants.NOTIFICATION_TYPE_FIND -> {
                    parseFindNotification(notifyData)
                }
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    private fun parseBatteryNotification(notifyData: ByteArray) {
        val leftByte = BytesUtils.getUINT8(notifyData, 0)
        val rightByte = BytesUtils.getUINT8(notifyData, 1)
        val boxByte = BytesUtils.getUINT8(notifyData, 2)
        listener.onBattery(
            BatteryState(
                leftCap = BytesUtils.getValueFromBits(leftByte, 0, 7),
                leftCharge = BytesUtils.getValueFromBits(leftByte, 7, 1) == 1,
                rightCap = BytesUtils.getValueFromBits(rightByte, 0, 7),
                rightCharge = BytesUtils.getValueFromBits(rightByte, 7, 1) == 1,
                boxCap = BytesUtils.getValueFromBits(boxByte, 0, 7),
                boxCharge = BytesUtils.getValueFromBits(boxByte, 7, 1) == 1
            )
        )
    }

    /**
     * 解析降噪通知数据
     */
    private fun parseAncNotification(notifyData: ByteArray) {
        listener.onAnc(
            parseAnc(BytesUtils.getUINT8(notifyData, 0))
        )
    }

    /**
     * 解析eq通知数据
     */
    private fun parseEqNotification(notifyData: ByteArray) {
        listener.onEq(EqualizerState(Equalizer.valueOf(BytesUtils.getUINT8(notifyData, 0))))
    }

    /**
     * 解析游戏模式通知数据
     */
    private fun parseGameModeNotification(notifyData: ByteArray) {
        listener.onGameMode(parseSwitch(notifyData))
    }

    private fun parseCodecNotification(notifyData: ByteArray) {
        listener.onCodec(CodecState(codecData = parseCodecData(notifyData)))
    }

    private fun parseFindNotification(notifyData: ByteArray) {
        listener.onFind( FindState(
            Find.valueOf(
                BytesUtils.getUINT8(notifyData, 0),
                1
            )
        ))
        listener.onFind( FindState(
            Find.valueOf(
                BytesUtils.getUINT8(notifyData, 1),
                2
            )
        ))
    }

    /**
     * 解析版本回复数据
     */
    private fun parseVersionResponse(cmdData: ByteArray) {
        val leftData = cmdData.copyOfRange(0, 3)
        val rightData = cmdData.copyOfRange(3, 6)
        listener.onVersion(
            VersionState(
                "${leftData[0]}.${leftData[1]}.${leftData[2]}",
                "${rightData[0]}.${rightData[1]}.${rightData[2]}"
            )
        )
    }

    /**
     * 解析设置回复数据
     */
    private fun parseConfigResponse(cmdData: ByteArray) {
        val leftByte = BytesUtils.getUINT8(cmdData, 0)
        val rightByte = BytesUtils.getUINT8(cmdData, 1)
        val boxByte = BytesUtils.getUINT8(cmdData, 2)
        listener.onConfig(
            ConfigState(
                BatteryState(
                    leftCap = BytesUtils.getValueFromBits(leftByte, 0, 7),
                    leftCharge = BytesUtils.getValueFromBits(leftByte, 7, 1) == 1,
                    rightCap = BytesUtils.getValueFromBits(rightByte, 0, 7),
                    rightCharge = BytesUtils.getValueFromBits(rightByte, 7, 1) == 1,
                    boxCap = BytesUtils.getValueFromBits(boxByte, 0, 7),
                    boxCharge = BytesUtils.getValueFromBits(boxByte, 7, 1) == 1
                ),
                ancState = parseAnc(BytesUtils.getUINT8(cmdData, 3)),
                eqState = EqualizerState(Equalizer.valueOf(BytesUtils.getUINT8(cmdData, 4))),
                dualState = SwitchState(
                    BytesUtils.getUINT8(cmdData, 5) == EarbudsConstants.SWITCH_ON_VALUE
                ),
                gameModeState = SwitchState(
                    BytesUtils.getUINT8(cmdData, 6) == EarbudsConstants.SWITCH_ON_VALUE
                ),
                hqDecodeState = SwitchState(
                    BytesUtils.getUINT8(cmdData, 7) == EarbudsConstants.SWITCH_ON_VALUE
                ),
                gestureState = GestureState(
                    leftSingle = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 8)),
                    leftDouble = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 9)),
                    leftTriple = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 10)),
                    leftLong = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 11)),
                    rightSingle = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 12)),
                    rightDouble = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 13)),
                    rightTriple = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 14)),
                    rightLong = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 15)),
                ),
                voiceState = VoiceState(Voice.valueOf(BytesUtils.getUINT8(cmdData, 16))),
                leftFindState = FindState(Find.valueOf(BytesUtils.getUINT8(cmdData, 17), 1)),
                rightFindState = FindState(Find.valueOf(BytesUtils.getUINT8(cmdData, 18), 2)),
            )
        )
    }

    /**
     * 解析EQ回复数据
     */
    private fun parseEqResponse(cmdData: ByteArray) {
        listener.onEq(EqualizerState(Equalizer.valueOf(BytesUtils.getUINT8(cmdData, 0))))
    }

    private fun parseFindResponse(cmdData: ByteArray) {
        listener.onFind(
            FindState(
                Find.valueOf(
                    BytesUtils.getUINT8(cmdData, 0),
                    BytesUtils.getUINT8(cmdData, 1)
                )
            )
        )
    }

    /**
     * 解析双连开关回复数据
     */
    private fun parseDualResponse(cmdData: ByteArray) {
        when (BytesUtils.getUINT8(cmdData, 0)) {
            EarbudsConstants.SWITCH_ON_VALUE -> {
                listener.onDual(SwitchState(true))
            }
            EarbudsConstants.SWITCH_OFF_VALUE -> {
                listener.onDual(SwitchState(false))
            }
            0xFF -> {
                listener.onDual(SwitchState(false))
                listener.onDualHint(SwitchState(true))
            }
        }
    }

    /**
     * 解析降噪模式回复数据
     */
    private fun parseAncResponse(cmdData: ByteArray) {
        listener.onAnc(
            parseAnc(
                BytesUtils.getUINT8(
                    cmdData,
                    0
                )
            )
        )
    }

    private fun parseAnc(value: Int): AncState {
        return AncState(
            Anc.valueOf(
                BytesUtils.getValueFromBits(
                    value, 0, 4
                )
            ) ?: Anc.ANC_CLOSE,
            AncLevel.valueOf(
                BytesUtils.getValueFromBits(
                    value, 4, 4
                )
            ) ?: AncLevel.ANC_DEEP
        )
    }

    /**
     * 解析游戏模式开关回复数据
     */
    private fun parseGameModeResponse(cmdData: ByteArray) {
        listener.onGameMode(parseSwitch(cmdData))
    }

    /**
     * 解析语音回复数据
     */
    private fun parseVoiceResponse(cmdData: ByteArray) {
        listener.onVoice(VoiceState(Voice.valueOf(BytesUtils.getUINT8(cmdData, 0))))
    }

    /**
     * 解析耳机操控
     */
    private fun parseGestureResponse(cmdData: ByteArray) {
        listener.onGesture(
            GestureState(
                leftSingle = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 0)),
                leftDouble = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 1)),
                leftTriple = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 2)),
                leftLong = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 3)),
                rightSingle = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 4)),
                rightDouble = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 5)),
                rightTriple = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 6)),
                rightLong = Gesture.valueOf(BytesUtils.getUINT8(cmdData, 7)),
            )
        )
    }

    /**
     * 解析连接设备名称回复数据
     */
    private fun parseDualDevice(cmdData: ByteArray) {
        val index = BytesUtils.getUINT8(cmdData, 1)
        listener.onDualDevice(
            DualDeviceState(
                BytesUtils.getUINT8(cmdData, 0),
                index,
                DualDevice(
                    cmdData.copyOfRange(2, cmdData.size - 6).toString(StandardCharsets.UTF_8),
                    index = index,
                    BytesUtils.byte2HexStr(cmdData.copyOfRange(cmdData.size - 6, cmdData.size))
                )
            )
        )
    }

    private fun parseCodecType(cmdData: ByteArray) {
        listener.onCodec(CodecState(codecData = parseCodecData(cmdData)))
    }

    private fun parseInBox(cmdData: ByteArray) {
        listener.onInBox(SwitchState(BytesUtils.getUINT8(cmdData, 0) == 1))
    }

    private fun parseCodecData(cmdData: ByteArray): MutableList<CodecBean> {
        return mutableListOf<CodecBean>().apply {
            for (i in 0 until cmdData.size / 2) {
                add(
                    CodecBean(
                        BytesUtils.getUINT8(cmdData, i * 2),
                        Codec.valueOf(BytesUtils.getUINT8(cmdData, i * 2 + 1))
                    )
                )
            }
        }
    }

    private fun parsePosition(cmdData: ByteArray): PositionState {
        return PositionState(BytesUtils.getUINT8(cmdData, 0))
    }

    private fun parseSwitch(cmdData: ByteArray): SwitchState {
        return SwitchState(BytesUtils.getUINT8(cmdData, 0) == EarbudsConstants.SWITCH_ON_VALUE)
    }
}