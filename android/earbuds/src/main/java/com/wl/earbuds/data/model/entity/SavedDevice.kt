package com.wl.earbuds.data.model.entity

import android.bluetooth.BluetoothDevice
import com.wl.db.model.UgDevice

/**
 * User: wanlang_dev
 * Data: 2023/4/17
 * Time: 18:02
 * Desc:
 */
data class SavedDevice(val device: BluetoothDevice, val ugDevice: UgDevice, val isAdd: Boolean)
