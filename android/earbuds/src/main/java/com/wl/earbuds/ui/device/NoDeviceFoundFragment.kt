package com.wl.earbuds.ui.device

import android.os.Bundle
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.databinding.FragmentNoDeviceFoundBinding
import com.wl.earbuds.utils.ext.initClose
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class NoDeviceFoundFragment : BaseFragment<BaseViewModel, FragmentNoDeviceFoundBinding>() {

    companion object {
        fun newInstance() = NoDeviceFoundFragment()
    }

    private var mRescanCallback: RescanCallback? = null

    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
        mDatabind.click = ProxyClick()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.no_device_find)) {
            activity?.onBackPressed()
        }
    }

    fun setRescanCallback(callback: RescanCallback) {
        mRescanCallback = callback
    }

    inner class ProxyClick {
        fun rescan() {
            mRescanCallback?.onRescan()
        }
    }
}