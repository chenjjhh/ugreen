package com.wl.earbuds.app.settings

import com.dylanc.mmkv.MMKVOwner
import com.dylanc.mmkv.mmkvBool
import com.dylanc.mmkv.mmkvString
import com.tencent.mmkv.MMKV
import com.wl.earbuds.utils.LanguageUtils

/**
 * User: wanlang_dev
 * Data: 2022/11/28
 * Time: 16:33
 * Desc:
 */
object SettingsUtils: MMKVOwner {

    // 同意用户隐私协议
    var isAgree: Boolean by mmkvBool(false)
    var isGuideFinish: Boolean by mmkvBool(true)
    // 语言
    var language: String by mmkvString(LanguageUtils.LANGUAGE_TYPE_CHINESE)

    // 上一次randomCode
    var lastRandomCode: String by mmkvString("")
    // 上一次升级文件md5
    var lastFileMd5: String by mmkvString("")

    var scanIgnore: Boolean by mmkvBool(false)
    var locationIgnore: Boolean by mmkvBool(false)

    override val kv: MMKV
        get() = MMKV.mmkvWithID("settings")

}