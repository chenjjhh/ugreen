package com.wl.earbuds.ui.upgrade

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isGone
import com.wanlang.base.BaseDialog
import com.wl.earbuds.R
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.earbuds.checkInBox
import com.wl.earbuds.bluetooth.earbuds.startOta
import com.wl.earbuds.bluetooth.ota.UpgradeState
import com.wl.earbuds.data.model.entity.EarbudsVersionBean
import com.wl.earbuds.data.model.entity.OtaBean
import com.wl.earbuds.data.model.entity.SwitchState
import com.wl.earbuds.databinding.FragmentEarbudsUpgradeBinding
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.ui.dialog.OnlyConfirmDialog
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.RxNetTool
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible
import zlc.season.downloadx.State

class EarbudsUpgradeFragment :
    BaseFragment<EarbudsUpgradeViewModel, FragmentEarbudsUpgradeBinding>() {

    companion object {
        fun newInstance() = EarbudsUpgradeFragment()
    }

    private var mEarbudsVersionBean: EarbudsVersionBean? = null
    private var isUpgrading = false
    private var isUpgradeSuccess = false
    private var isCheckBox = false
    private var lastCheckTime = 0L

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        mEarbudsVersionBean = args?.get(Constants.EARBUDS_VERSION) as EarbudsVersionBean
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        mDatabind.viewModel = mViewModel
        initTitle()
        initContent()

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (CurrentDeviceManager.isOnline() && isUpgrading) {
                        showUpgradeNoticeDialog()
                    } else {
                        nav().navigateUp()
                    }
                }
            })
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.hardwire_upgrade)) {
            activity?.onBackPressed()
        }
    }

    private fun initContent() {
        if (mEarbudsVersionBean == null) {
            safeNav {
                nav().navigateUp()
            }
            return
        }
        val earbudsBean = mEarbudsVersionBean!!
        // mViewModel.setEarbudsBean(earbudsBean)
        mViewModel.versionStr.set(earbudsBean.versionName)
        mViewModel.dateStr.set(
            getString(
                com.wl.resource.R.string.version_date,
                earbudsBean.dateStr.replace("-", "")
            )
        )
        mViewModel.sizeStr.set(
            getString(
                com.wl.resource.R.string.file_size,
                earbudsBean.fileSize
            )
        )
        // mViewModel.descStr.set(getString(com.wl.resource.R.string.upgrade_content))
        mViewModel.descStr.set(earbudsBean.content)
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeUpgradeState(viewLifecycleOwner, ::onUpgrade)
        CurrentDeviceManager.observeInBoxState(viewLifecycleOwner, ::onInBox)
        // mViewModel.hasDownload.observe(viewLifecycleOwner, ::onDownloaded)
        mViewModel.downloadState.observe(viewLifecycleOwner, ::onDownloadState)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("onOnlineChange: $isOnline")
        if (!isOnline) {
            if (isUpgrading || isUpgradeSuccess) return
            safeNav {
                nav().navigate(R.id.action_earbudsUpgrade_to_home)
            }
        }
    }

    private fun onUpgrade(upgradeState: UpgradeState) {
        when (upgradeState) {
            is UpgradeState.Fail -> {
                isUpgrading = false
                showFailDialog()
                mViewModel.statusStr.set(
                    getString(com.wl.resource.R.string.upgrade_fail)
                )
            }
            is UpgradeState.None -> {
            }
            is UpgradeState.Success -> {
                isUpgrading = false
                isUpgradeSuccess = true
                showSuccessDialog()
                mViewModel.statusStr.set(
                    getString(com.wl.resource.R.string.upgrade_success)
                )
            }
            is UpgradeState.Upgrading -> {
                mViewModel.statusStr.set(
                    getString(
                        com.wl.resource.R.string.upgrade_progress,
                        upgradeState.progress
                    )
                )
            }
            is UpgradeState.Start -> {
                isUpgrading = true
                if (mDatabind.lavUpload.isGone) {
                    mDatabind.lavUpload.visible()
                    mDatabind.ivProduct.gone()
                    mDatabind.lavDownload.gone()
                }
                mDatabind.btnUpgradeStatus.apply {
                    text = getString(com.wl.resource.R.string.upgrading)
                    isEnabled = false
                }
            }
        }
    }

    private fun onInBox(inBoxState: SwitchState) {
        val current = System.currentTimeMillis()
        if (!isCheckBox || current - lastCheckTime > 1000) {
            return
        }
        isCheckBox = false
        if (inBoxState.boolean) {
            downloadAndUpdate()
        } else {
            showMsg(getString(com.wl.resource.R.string.upgrade_hint_2))
            return
        }
    }

    private fun onDownloaded(isDownloaded: Boolean) {
        if (isDownloaded) {
            startOta()
        }
    }

    private fun startOta() {
        val filePath = mViewModel.getFilePath()
        mlog("filePath: $filePath")
        startOta(OtaBean(filePath, mEarbudsVersionBean!!.fileMd5))
    }

    private fun onDownloadState(downloadState: State) {
        when (downloadState) {
            is State.Downloading -> {
                isUpgrading = true
                if (mDatabind.lavDownload.isGone) {
                    mDatabind.lavDownload.visible()
                    mDatabind.lavUpload.gone()
                    mDatabind.ivProduct.gone()
                }
                mViewModel.statusStr.set(
                    getString(
                        com.wl.resource.R.string.download_progress,
                        downloadState.progress.percent().toInt()
                    )
                )
                mDatabind.btnUpgradeStatus.text = getString(com.wl.resource.R.string.cancel)
            }
            is State.Failed -> {
                isUpgrading = false
                mViewModel.statusStr.set(
                    getString(com.wl.resource.R.string.download_fail)
                )
                mDatabind.btnUpgradeStatus.text =
                    getString(com.wl.resource.R.string.one_click_update)
            }
            is State.None -> {}
            is State.Stopped -> {
                isUpgrading = false
                mDatabind.btnUpgradeStatus.text =
                    getString(com.wl.resource.R.string.one_click_update)
            }
            is State.Succeed -> {
                mlog("download State.Succeed")
                onDownloaded(true)
            }
            is State.Waiting -> {}
        }

    }

    private fun showFailDialog() {
        CommonNoticeDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.upgrade_fail))
            .setContent(getString(com.wl.resource.R.string.upgrade_fail_hint))
            .setCancelable(false)
            .setCanceledOnTouchOutside(false)
            .setConfirmText(getString(com.wl.resource.R.string.retry))
            .setActionListener(object : CommonNoticeDialog.ActionListener {
                override fun onCancel(dialog: BaseDialog?) {
                    super.onCancel(dialog)
                    dialog?.dismiss()
                    safeNav {
                        nav().navigateUp()
                    }
                }

                override fun onConfirm(dialog: BaseDialog?) {
                    super.onConfirm(dialog)
                    dialog?.dismiss()
                    startOta()
                }
            }).show()
    }

    private fun showSuccessDialog() {
        OnlyConfirmDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.upgrade_success))
            .setContent(getString(com.wl.resource.R.string.upgrade_success_hint))
            .setConfirm(getString(com.wl.resource.R.string.confirm))
            .setCancelable(false)
            .setCanceledOnTouchOutside(false)
            .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                override fun onConfirm(dialog: BaseDialog?) {
                    super.onConfirm(dialog)
                    dialog?.dismiss()
                    safeNav {
                        nav().navigate(R.id.action_earbudsUpgrade_to_home)
                    }
                }
            }).show()
    }

    private fun showUpgradeNoticeDialog() {
        OnlyConfirmDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.operate_hint_title))
            .setContent(getString(com.wl.resource.R.string.upgrade_exit_hint))
            .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                override fun onConfirm(dialog: BaseDialog?) {
                    dialog?.dismiss()
                }
            })
            .show()
    }

    private fun downloadAndUpdate() {
        if (!RxNetTool.isNetworkAvailable(requireContext())) {
            showMsg(getString(com.wl.resource.R.string.network_error))
            return
        }
        if (!RxNetTool.isWifiConnected(requireContext())) {
            CommonNoticeDialog.Builder(requireContext())
                .setTitle(getString(com.wl.resource.R.string.mobile_download_title))
                .setContent(getString(com.wl.resource.R.string.mobile_download_hint))
                .setActionListener(object : CommonNoticeDialog.ActionListener {
                    override fun onCancel(dialog: BaseDialog?) {
                        super.onCancel(dialog)
                        dialog?.dismiss()
                    }

                    override fun onConfirm(dialog: BaseDialog?) {
                        super.onConfirm(dialog)
                        mViewModel.startDownload(mEarbudsVersionBean!!)
                        dialog?.dismiss()
                    }
                }).show()
        } else {
            mViewModel.startDownload(mEarbudsVersionBean!!)
        }
    }

    inner class ProxyClick {
        fun downloadAndInstall() {
            if (!mViewModel.hasDownload.value!!) {
                // 检查升级条件
                if (!(CurrentDeviceManager.isOnline())) {
                    showMsg(getString(com.wl.resource.R.string.upgrade_hint))
                    return
                }
                if (!(CurrentDeviceManager.getBatteryState().isRightOn() &&
                            CurrentDeviceManager.getBatteryState().isLeftOn() &&
                            CurrentDeviceManager.getBatteryState().isBoxOn())
                ) {
                    showMsg(getString(com.wl.resource.R.string.upgrade_hint_2))
                    return
                }
                mlog(
                    "${CurrentDeviceManager.getBatteryState().boxCap}${
                        CurrentDeviceManager.getBatteryState().isBoxOn()
                    }"
                )
                if (!CurrentDeviceManager.getBatteryState().isUpgradeAvailable()) {
                    showMsg(getString(com.wl.resource.R.string.upgrade_hint_1))
                    return
                }
                if (mViewModel.isDownloading()) {
                    mViewModel.cancelDownload()
                    return
                }
                val current = System.currentTimeMillis()
                if (current - lastCheckTime < 1000) return
                checkInBox()
                isCheckBox = true
                lastCheckTime = System.currentTimeMillis()
                return
            }
        }
    }
}