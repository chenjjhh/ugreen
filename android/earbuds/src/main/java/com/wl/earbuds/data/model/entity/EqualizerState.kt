package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Equalizer

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:38
 * Desc:
 */
data class EqualizerState(val equalizer: Equalizer = Equalizer.CLASSIC) {
}