package com.wl.earbuds.base

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Bundle
import android.os.LocaleList
import androidx.databinding.ViewDataBinding
import com.gyf.immersionbar.ImmersionBar
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.utils.LanguageUtils
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.setAppLocale
import me.hgj.jetpackmvvm.base.activity.BaseVmDbActivity
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

/**
 * 时间　: 2019/12/21
 * 作者　: hegaojian
 * 描述　: 你项目中的Activity基类，在这里实现显示弹窗，吐司，还有加入自己的需求操作 ，如果不想用Databind，请继承
 * BaseVmActivity例如
 * abstract class BaseActivity<VM : BaseViewModel> : BaseVmActivity<VM>() {
 */
abstract class BaseActivity<VM : BaseViewModel, DB : ViewDataBinding> : BaseVmDbActivity<VM, DB>() {

    abstract override fun initView(savedInstanceState: Bundle?)

    protected var recreateFlag: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initImmersionBar()
        switchLanguage()
        observeLanguage()
    }

    open fun initImmersionBar() {
        ImmersionBar.with(this)
            .transparentStatusBar()
            .navigationBarColor(com.wanlang.base.R.color.transparent)
            .statusBarDarkFont(true)// 默认状态栏字体颜色为黑色
            //            .keyboardEnable(true)  // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init()
    }

    /**
     * 创建liveData观察者
     */
    override fun createObserver() {}

    /**
     * 打开等待框
     */
    override fun showLoading(message: String) {
        //        showLoadingExt(message)
    }

    /**
     * 关闭等待框
     */
    override fun dismissLoading() {
        //        dismissLoadingExt()
    }

    /**
     * 在任何情况下本来适配正常的布局突然出现适配失效，适配异常等问题，只要重写 Activity 的 getResources() 方法
     */
    /*
        override fun getResources(): Resources {
            AutoSizeCompat.autoConvertDensityOfGlobal(super.getResources())
            return super.getResources()
        }*/
    private fun switchLanguage() {
        val metrics = resources.displayMetrics
        val configuration = resources.configuration
        val localByLanguageType = LanguageUtils.getLocalByLanguageType()
        configuration.setLocale(localByLanguageType)
        configuration.setLocales(LocaleList(localByLanguageType))
        resources.updateConfiguration(configuration, metrics)
    }

    /**
     * 重新创建
     */
    open fun afterLanguageSwitch() {
        recreateFlag = true
        recreate()
    }

    private fun observeLanguage() {
        appViewModel.observeLanguage(this) {
            switchLanguage()
            afterLanguageSwitch()
        }
    }

}