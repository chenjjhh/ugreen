package com.wl.earbuds.bluetooth.core

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 14:09
 * Desc:
 */
interface IFrame {

    fun getFrameType(): FrameType

    fun getSendSof(): ByteArray

    fun getResponseSof(): ByteArray

    fun getData(): ByteArray

    fun setData(data: ByteArray)

    fun isCrc(): Boolean

    fun getChecksum(): Int

    fun format(): ByteArray

}