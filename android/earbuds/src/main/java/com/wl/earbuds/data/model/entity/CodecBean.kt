package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Codec

/**
 * User: wanlang_dev
 * Data: 2022/12/12
 * Time: 9:41
 * Desc:
 */
data class CodecBean(val index: Int, val codec: Codec)