package com.wl.earbuds.utils.ext

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.wanlang.base.BaseDialog
import com.wl.earbuds.base.BaseFragment

/**
 * @author : hgj
 * @date : 2020/6/28
 */

//loading框
private var loadingDialog: BaseDialog? = null

/**
 * 打开等待框
 */
fun FragmentActivity.showLoadingExt(message: String = "请求网络中") {
    /*if (!this.isFinishing) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialogBuilder(this)
                .show()
        } else {
            loadingDialog?.show()
        }
    }*/
}

/**
 * 打开等待框
 */
/*fun BaseFragment<*, *>.showLoadingExt(message: String = "请求网络中") {
    activity?.let {
        if (!it.isFinishing) {
            if (loadingDialog == null) {
                loadingDialog = LoadingDialogBuilder(requireActivity())
                    .setMessage(message)
                    .show()
            } else {
                loadingDialog?.show()
            }
        }
    }
}*/

/**
 * 关闭等待框
 */
fun Activity.dismissLoadingExt() {
    loadingDialog?.dismiss()
    loadingDialog = null
}

/**
 * 关闭等待框
 */
fun Fragment.dismissLoadingExt() {
    loadingDialog?.dismiss()
    loadingDialog = null
}
