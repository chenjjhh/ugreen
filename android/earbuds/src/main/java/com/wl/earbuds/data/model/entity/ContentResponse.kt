package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/28
 * Time: 12:05
 * Desc:
 */
data class ContentResponse(val content: String)