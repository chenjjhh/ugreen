package com.wl.earbuds.ui.language

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.base.BaseActivity
import com.wl.earbuds.data.LanguageUpdate
import com.wl.earbuds.data.model.entity.LanguageBean
import com.wl.earbuds.databinding.ActivityLanguageBinding
import com.wl.earbuds.ui.adapter.LanguageAdapter
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.generateLanguageData
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class LanguageActivity : BaseActivity<BaseViewModel, ActivityLanguageBinding>() {

    private val data: MutableList<LanguageBean> by lazy {
        generateLanguageData(this)
    }

    private val mLanguageAdapter: LanguageAdapter by lazy {
        LanguageAdapter(data).apply {
            setOnItemClickListener { adapter, view, position ->
                val type = data[position].type
                if (type != SettingsUtils.language) {
                    SettingsUtils.language = type
                    appViewModel.updateLanguage(LanguageUpdate(type))
                }
                notifyDataSetChanged()
            }
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
        initRv()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.select_language)) {
            onBackPressed()
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(this@LanguageActivity)
            val decoration =
                SpinnerDividerItemDecoration(
                    this@LanguageActivity,
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(17f), dp2px(17f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider_line))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mLanguageAdapter
            itemAnimator = null
        }
    }

    override fun afterLanguageSwitch() {
        mDatabind.toolbar.tvTitle.text = getString(com.wl.resource.R.string.select_language)
        mLanguageAdapter.setNewInstance(generateLanguageData(this))
    }

}
