package com.wl.earbuds.bluetooth.utils;

import android.content.Context;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

public final class SystemUtils {

    /**
     * <p>This method checks if all the permissions are granted.</p>
     *
     * @return true if all permissions are granted, false if at least one of them is not.
     */
    public static boolean arePermissionsGranted(@NonNull Context context, @NonNull String[] permissions) {
        for (String wanted : permissions) {
            if (ActivityCompat.checkSelfPermission(context, wanted) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    /**
     * 是否为鸿蒙系统
     *
     * @return true为鸿蒙系统
     */
    public static boolean isHarmonyOs() {
        try {
            Class<?> buildExClass = Class.forName("com.huawei.system.BuildEx");
            Object osBrand = buildExClass.getMethod("getOsBrand").invoke(buildExClass);
            return "Harmony".equalsIgnoreCase(osBrand.toString());
        } catch (Throwable x) {
            return false;
        }
    }
}
