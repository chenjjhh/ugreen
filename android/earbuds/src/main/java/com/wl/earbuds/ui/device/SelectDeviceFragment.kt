package com.wl.earbuds.ui.device

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import cn.wandersnail.bluetooth.BTManager
import cn.wandersnail.bluetooth.Connection
import com.wl.earbuds.R
import com.wl.earbuds.app.deviceManagerViewModel
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.ConnectError
import com.wl.earbuds.bluetooth.connect.ConnectListener
import com.wl.earbuds.bluetooth.connect.ConnectManager
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.databinding.FragmentSelectDeviceBinding
import com.wl.earbuds.ui.adapter.EarbudsDeviceAdapter
import com.wl.earbuds.ui.home.HomeActivity
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import com.wl.earbuds.weight.decoration.SpaceItemDecoration
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class SelectDeviceFragment : BaseFragment<BaseViewModel, FragmentSelectDeviceBinding>(),
    ConnectListener {

    companion object {
        fun newInstance() = SelectDeviceFragment()
    }

    private val mConnectManager by lazy {
        ConnectManager.getInstance(appContext)
    }

    private var mState: Int = Connection.STATE_PAIRED_REFUSED

    private var mEarbudsData: MutableList<BluetoothDevice> = mutableListOf()

    private val deviceAdapter by lazy {
        EarbudsDeviceAdapter(mEarbudsData).apply {
            setOnItemChildClickListener { adapter, view, position ->
                if (targetDevice != null) return@setOnItemChildClickListener
                mEarbudsData[position].let { device ->
                    // 已连接设备直接跳转
                    if (BTManager.getInstance().getConnection(device.address)?.isConnected == true
                    ) {
                        CurrentDeviceManager.device = device
                        CurrentDeviceManager.ugDevice =
                            deviceManagerViewModel.getUgdeviceByAddress(device.address)
                        startActivity(Intent(requireContext(), HomeActivity::class.java))
                        requireActivity().finish()
                    } else {
                        targetDevice = device
                        mConnectManager.connectDeviceNoSearch(device)
                    }
                }
            }
            setDiffCallback(object : DiffUtil.ItemCallback<BluetoothDevice>() {
                override fun areItemsTheSame(
                    oldItem: BluetoothDevice,
                    newItem: BluetoothDevice
                ): Boolean {
                    return oldItem.address == newItem.address
                }

                override fun areContentsTheSame(
                    oldItem: BluetoothDevice,
                    newItem: BluetoothDevice
                ): Boolean {
                    return false
                }
            })
            addChildClickViewIds(R.id.tv_connect)
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
        initRv()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.title_select_device)) {
            activity?.onBackPressed()
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = deviceAdapter
            addItemDecoration(SpaceItemDecoration(bottom = dp2px(18f)))
            deviceAdapter.setEmptyView(R.layout.layout_device_empty)
            itemAnimator = null
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mConnectManager.addConnectListener(this)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDestroyView() {
        mConnectManager.removeConnectListener(this)
        super.onDestroyView()
    }

    override fun onPairRefused(device: BluetoothDevice) {
        super.onPairRefused(device)
        if (deviceAdapter.targetDevice != device) {
            mlog("not this device")
            return
        }
        showMsg(getString(com.wl.resource.R.string.connect_fail))
        deviceAdapter.targetDevice = null
    }

    @SuppressLint("MissingPermission")
    fun updateDevice(deviceList: MutableList<BluetoothDevice>) {
        deviceList.forEach {
            mlog("name: ${it.name}   address: ${it.address}")
        }
        mEarbudsData = mutableListOf<BluetoothDevice>().apply {
            addAll(deviceList)
        }
        deviceAdapter.setDiffNewData(mEarbudsData, null)
    }

    override fun onConnectFail(address: String, status: Int) {
        val device = deviceAdapter.targetDevice ?: return
        if (device.address != address) return
        when (status) {
            ConnectError.DEVICE_NOT_FOUND -> {
                showMsg(getString(com.wl.resource.R.string.scan_device_not_found))
            }
            ConnectError.CONNECT_DISCONNECT,
            ConnectError.CONNECT_FAIL -> {
                showMsg(getString(com.wl.resource.R.string.connect_fail))
            }
        }
        deviceAdapter.targetDevice = null
    }

    override fun onDiscoveryStart() {
        super.onDiscoveryStart()
        mDatabind.tvStatus.text = getString(com.wl.resource.R.string.scanning)
    }

    override fun onDiscoveryFinished(devices: MutableList<BluetoothDevice>) {
        super.onDiscoveryFinished(devices)
        mDatabind.tvStatus.text = getString(com.wl.resource.R.string.scann_finish)
    }
}