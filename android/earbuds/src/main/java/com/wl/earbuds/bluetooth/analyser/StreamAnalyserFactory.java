package com.wl.earbuds.bluetooth.analyser;

import android.util.Log;

public final class StreamAnalyserFactory {

    private static final String TAG = "StreamAnalyserFactory";

    public static StreamAnalyser buildDataAnalyser(StreamAnalyserType type) {
        if (type == StreamAnalyserType.EARBUDS) {
            // return new GreenStreamAnalyser(getHyManager().getStreamAnalyserListener());
            return null;
        }
        else {
            Log.w(TAG, "[buildDataAnalyser] unexpected type: type=" + type);
            return null;
        }
    }

}
