package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Voice

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:42
 * Desc:
 */
data class VoiceState(val voice: Voice = Voice.CHINESE) {
}