package com.wl.earbuds.weight.expandable

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.widget.LinearLayout
import androidx.core.view.animation.PathInterpolatorCompat
import androidx.core.view.children
import com.wl.earbuds.utils.ext.mlog

/**
 * User: wanlang_dev
 * Data: 2022/11/5
 * Time: 11:27
 * Desc:
 */
class ExpandableLayout : LinearLayout {

    private var mVisibleIndex: Int = -1
    private var mTargetIndex: Int = -1
    private var mSlideAnimation: Animation? = null
    private var mAlphaAnimation: Animation? = null
    private val mHeightArray: SparseArray<Int> = SparseArray(8)
    private var mHasMeasured = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        if (visibility == View.VISIBLE) {
            addOnGlobalLayoutListener()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        children.forEachIndexed { index, v ->
            measureChild(v, widthMeasureSpec, 0)
            mHeightArray[index] = v.measuredHeight
        }
        mHasMeasured = true
    }

    private fun addOnGlobalLayoutListener() {
        viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                initView()
            }
        })
    }

    fun initView() {
        clearAnimation()
        children.forEachIndexed { index, view ->
            if (index == mVisibleIndex) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }
        if (mTargetIndex != -1) {
            setIndex(mTargetIndex)
        }
    }


    /**
     * 操作对应view显示和隐藏
     * @param view 对应子view
     */
    fun setView(view: View?) {
        val index = children.indexOf(view)
        setIndex(index)
    }

    /**
     * 根据index操作对应view显示和隐藏
     * @param index 对应index
     */
    private fun setIndex(index: Int) {
        if (!mHasMeasured) {
            mTargetIndex = index
            return
        }
        mTargetIndex = -1
        mlog("index: $index   mVisibleIndex: $mVisibleIndex")
        if (mVisibleIndex != index) {
            if (index == -1) {
                collapse(mVisibleIndex)
            } else {
                expand(index)
            }
        }
    }

    private fun collapse(index: Int) {
        if (index == -1 || index >= childCount) {
            return
        }
        startSlideAnimation(0)
        startAlphaAnimation(2, index)
        mVisibleIndex = -1
    }

    private fun expand(index: Int) {
        if (index == -1 || index >= childCount) {
            return
        }
        startSlideAnimation(mHeightArray[index])
        startAlphaAnimation(1, index)
        mVisibleIndex = index
    }

    private fun startSlideAnimation(toHeight: Int) {
        mSlideAnimation?.cancel()
        mSlideAnimation = getSlideAnimation(toHeight)
        startAnimation(mSlideAnimation)
    }

    private fun getSlideAnimation(toHeight: Int): Animation {
        return SlideAnimation(this, height, toHeight).apply {
            interpolator = PathInterpolatorCompat.create(0.4f, 0.6f, 0f, 1f)
            duration = 400
        }
    }

    /**
     * @param type 1 -> 显示  2 ->隐藏
     */
    private fun startAlphaAnimation(type: Int, index: Int) {
        val visibleIndex = mVisibleIndex
        mAlphaAnimation?.cancel()
        mAlphaAnimation = getAlphaAnimation(type)
        mAlphaAnimation!!.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                if (type == 1) {
                    if (visibleIndex != -1) {
                        getChildAt(visibleIndex).visibility = View.GONE
                    }
                    getChildAt(index).visibility = View.VISIBLE
                }
            }

            override fun onAnimationEnd(animation: Animation?) {
                if (type == 2) {
                    getChildAt(visibleIndex).visibility = View.GONE
                }
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }
        })
        getChildAt(index).startAnimation(mAlphaAnimation)
    }

    private fun getAlphaAnimation(type: Int): AlphaAnimation {
        var startAlpha = 0f
        var endAlpha = 0f
        var animationDuration = 0L
        var animationStartOffset = 0L
        if (type == 1) {
            animationDuration = 200
            startAlpha = 0f
            endAlpha = 1f
            animationStartOffset = 100
        } else {
            animationDuration = 100
            startAlpha = 1f
            endAlpha = 0f

        }
        return AlphaAnimation(startAlpha, endAlpha).apply {
            duration = animationDuration
            startOffset = animationStartOffset
            interpolator = PathInterpolatorCompat.create(0f, 0f, 1f,1f)
        }
    }
}