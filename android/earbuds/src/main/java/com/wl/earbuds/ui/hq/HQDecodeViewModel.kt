package com.wl.earbuds.ui.hq

import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.StringObservableField

class HQDecodeViewModel : BaseViewModel() {
    val codecStr: StringObservableField = StringObservableField("")
}