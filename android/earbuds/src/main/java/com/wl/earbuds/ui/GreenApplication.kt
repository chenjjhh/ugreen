package com.wl.earbuds.ui

import android.app.Application
import android.content.Context
import com.wl.earbuds.app.EarbudsAppLifecycles
import me.hgj.jetpackmvvm.base.IModuleAppLifecycles

/**
 * User: wanlang_dev
 * Data: 2022/10/28
 * Time: 10:46
 * Desc:
 */
open class GreenApplication : Application() {

    private val _modules = mutableListOf<IModuleAppLifecycles>()

    init {
        _modules.add(EarbudsAppLifecycles())
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        _modules.forEach {
            it.attachBaseContext(base)
        }
    }

    override fun onCreate() {
        super.onCreate()
//        CrashReport.initCrashReport(this, "b0d9d33ea8", true)
        // 替换默认字体
        //FontUtils.getInstance().replaceSystemDefaultFontFromAsset(this, "fonts/OPPOSans-M-2.ttf")
        _modules.forEach {
            it.onCreate(this)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        _modules.forEach {
            it.onTerminal(this)
        }
    }
}