package com.wl.earbuds.ui.earbuds

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wl.earbuds.R
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.earbuds.setVoice
import com.wl.earbuds.data.model.entity.VoiceBean
import com.wl.earbuds.data.model.entity.VoiceState
import com.wl.earbuds.databinding.FragmentEarbudsVoiceBinding
import com.wl.earbuds.ui.adapter.VoiceAdapter
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.generateVoiceData
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav

class EarbudsVoiceFragment : BaseFragment<BaseViewModel, FragmentEarbudsVoiceBinding>() {

    companion object {
        fun newInstance() = EarbudsVoiceFragment()
    }

    private var lastClickTime = 0L
    private val mHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    private val data: MutableList<VoiceBean> by lazy {
        generateVoiceData(requireContext())
    }

    private val updateVoiceRunnable by lazy {
        Runnable { updateVoiceAfterSwitch() }
    }

    private val mCommandAdapter: VoiceAdapter by lazy {
        VoiceAdapter(data).apply {
            setOnItemClickListener { adapter, view, position ->
                data[position].voice.apply {
                    currentVoice = this
                    val currentTime = System.currentTimeMillis()
                    if (currentTime - lastClickTime > 1000) {
                        lastClickTime = currentTime
                        setVoice(this)
                    }
                    mHandler.removeCallbacks(updateVoiceRunnable)
                    mHandler.postDelayed(updateVoiceRunnable, 1500)
                }
            }
            currentVoice = CurrentDeviceManager.getVoiceState().voice
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
        initRv()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.select_voice)) {
            requireActivity().onBackPressed()
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(context)
            val decoration =
                SpinnerDividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(18f),
                    dp2px(18f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider_line))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mCommandAdapter
            itemAnimator = null
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeVoiceState(viewLifecycleOwner, ::onVoiceChange)
    }

    private fun onVoiceChange(voiceState: VoiceState) {
        mCommandAdapter.currentVoice = voiceState.voice
    }

    private fun updateVoiceAfterSwitch() {
        mCommandAdapter.currentVoice = CurrentDeviceManager.getVoiceState().voice
    }

    private fun onOnlineChange(isOnline: Boolean) {
        if (!isOnline)
            safeNav {
                nav().navigate(R.id.action_earbudsVoice_to_home)
            }
    }

    override fun onDestroyView() {
        mHandler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }
}