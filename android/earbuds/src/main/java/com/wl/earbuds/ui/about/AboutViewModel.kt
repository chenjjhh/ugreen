package com.wl.earbuds.ui.about

import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.data.model.entity.AppVersionResponse
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.network.apiService
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.StringObservableField
import me.hgj.jetpackmvvm.ext.request

class AboutViewModel : BaseViewModel() {
    val languageStr = StringObservableField("")
    val versionStr = StringObservableField("")
    val appVersionState: UnPeekLiveData<ResponseUiState<AppVersionResponse>> = UnPeekLiveData()

    fun getAppVersion() {
        request({
            apiService.getAppVersion()
        }, {
            appVersionState.value = ResponseUiState(true, it)
        }, {
            appVersionState.value = ResponseUiState(false)
        })
    }
}