package com.wl.earbuds.utils

import android.annotation.TargetApi
import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Base64
import androidx.core.content.FileProvider
import java.io.*

//import android.util.Log;
/**
 * Created in Sep 10, 2016 4:22:18 PM.
 *
 * @author Tamsiree.
 */
class RxFileTool {


    companion object {
        const val BUFSIZE = 1024 * 8
        private const val TAG = "RxFileTool"// 取得sdcard文件路径

        /**
         * 判断文件是否存在
         *
         * @param file 文件
         * @return `true`: 存在<br></br>`false`: 不存在
         */
        @JvmStatic
        fun isFileExists(file: File?): Boolean {
            return file != null && file.exists()
        }

        /**
         * 将文件转换成uri(支持7.0)
         *
         * @param mContext
         * @param file
         * @return
         */
        @JvmStatic
        fun getUriForFile(mContext: Context, file: File?): Uri? {
            var fileUri: Uri? = null
            fileUri = if (Build.VERSION.SDK_INT >= 24) {
                FileProvider.getUriForFile(mContext, mContext.packageName + ".fileprovider", file!!)
            } else {
                Uri.fromFile(file)
            }
            return fileUri
        }

        /**
         * 将图片文件转换成uri
         *
         * @param context
         * @param imageFile
         * @return
         */
        @JvmStatic
        fun getImageContentUri(context: Context?, imageFile: File?): Uri? {
            val filePath = imageFile?.absolutePath
            val cursor = context?.contentResolver?.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, arrayOf(MediaStore.Images.Media._ID), MediaStore.Images.Media.DATA + "=? ", arrayOf(filePath), null)
            return if (cursor != null && cursor.moveToFirst()) {
                val id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID))
                val baseUri = Uri.parse("content://media/external/images/media")
                Uri.withAppendedPath(baseUri, "" + id)
            } else {
                if (imageFile!!.exists()) {
                    val values = ContentValues()
                    values.put(MediaStore.Images.Media.DATA, filePath)
                    context!!.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
                } else {
                    null
                }
            }
        }

        @TargetApi(19)
        @JvmStatic
        fun getPathFromUri(context: Context, uri: Uri): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(
                            split[1]
                    )
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {
                // Return the remote address
                return if (isGooglePhotosUri(uri)) {
                    uri.lastPathSegment
                } else getDataColumn(context, uri, null, null)
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
            return ""
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        @JvmStatic
        fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        @JvmStatic
        fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        @JvmStatic
        fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is Google Photos.
         */
        @JvmStatic
        fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }

        @JvmStatic
        fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
            var cursor: Cursor? = null
            val column = MediaStore.Images.Media.DATA
            val projection = arrayOf(column)
            try {
                cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        /**
         * 安静关闭IO
         *
         * @param closeables closeable
         */
        @JvmStatic
        fun closeIOQuietly(vararg closeables: Closeable?) {
            if (closeables == null) {
                return
            }
            for (closeable in closeables) {
                if (closeable != null) {
                    try {
                        closeable.close()
                    } catch (ignored: IOException) {
                    }
                }
            }
        }

        @JvmStatic
        fun file2Base64(filePath: String?): String {
            var fis: FileInputStream? = null
            var base64String = ""
            val bos = ByteArrayOutputStream()
            try {
                fis = FileInputStream(filePath)
                val buffer = ByteArray(1024 * 100)
                var count = 0
                while (fis.read(buffer).also { count = it } != -1) {
                    bos.write(buffer, 0, count)
                }
                fis.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            base64String = Base64.encodeToString(bos.toByteArray(), Base64.DEFAULT)
            return base64String
        }

        @JvmStatic
        fun exportDb2Sdcard(context: Context, path: String, realDBName: String?, exportDBName: String) {
            val filePath = context.getDatabasePath(realDBName).absolutePath
            val buffer = ByteArray(1024)
            try {
                val input = FileInputStream(File(filePath))
                val output = FileOutputStream(path + File.separator + exportDBName)
                var length: Int
                while (input.read(buffer).also { length = it } > 0) {
                    output.write(buffer, 0, length)
                }
                output.flush()
                output.close()
                input.close()
            } catch (var8: IOException) {
            }
        }
    }
}