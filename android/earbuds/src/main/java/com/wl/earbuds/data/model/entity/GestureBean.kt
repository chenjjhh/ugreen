package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Operation

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:33
 * Desc:
 */
data class GestureBean(val title: String, val command: String, val operation: Operation)