package com.wl.earbuds.bluetooth.ota

import android.os.Handler
import android.os.Looper
import android.os.Message
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.security.InvalidParameterException

class OtaInputThread : Thread {

    val TAG = OtaInputThread::class.java.simpleName
    val ON_DATA_READ = 1
    val ON_DATA_SKIP = 2
    val ON_READ_FINISH = 10

    private var mFileInputStream: FileInputStream? = null
    private val lock = Object()

    //标志线程阻塞情况
    private var pause = true
    private var mBuffer: ByteArray? = null
    private var lastReadLength = 0
    private var bufferSize = 0
    private var mOffset = 0L
    private var mBlockSize = 0
    fun setDataListener(mListener: OnDataListener?) {
        this.mListener = mListener
    }

    private var mListener: OnDataListener? = null
    private val mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            when (msg.what) {
                ON_DATA_READ -> {
                    val length = msg.obj as Int
                    if (mListener != null) {
                        mListener!!.onDataAvailble(mBuffer!!.clone(), length)
                    }
                }
                ON_DATA_SKIP -> {
                    if (mListener != null) {
                        mListener!!.prepare()
                    }
                }
                ON_READ_FINISH -> if (mListener != null) {
                    mListener!!.onDataFinish(lastReadLength == bufferSize)
                }
            }
        }
    }

    constructor(filePath: String?, bufferSize: Int) : super() {
        this.bufferSize = bufferSize
        mBuffer = ByteArray(bufferSize)
        try {
            mFileInputStream = FileInputStream(filePath)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    fun setSkipOffset(offset: Long, blocksize: Int) {
        if (offset < 0) throw InvalidParameterException("offset must > 0")
        mOffset = offset
        mBlockSize = blocksize
    }

    /**
     * 设置线程是否阻塞
     */
    fun pauseThread() {
        pause = true
    }

    /**
     * 调用该方法实现恢复线程的运行
     */
    fun resumeThread() {
        synchronized(lock) {
            //唤醒线程
            lock.notify()
        }
    }

    /**
     * 这个方法只能在run 方法中实现，不然会阻塞主线程，导致页面无响应
     */
    fun onPause() {
        synchronized(lock) {
            try {
                //线程 等待/阻塞
                lock.wait()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 阻塞线程，调用resumeThread继续
     */
    override fun run() {
        super.run()
        var len = 0
        mFileInputStream!!.skip(mOffset)
        val message = mHandler.obtainMessage(ON_DATA_SKIP)
        mHandler.sendMessage(message)
        while (true) {
            try {
                if (mFileInputStream!!.read(mBuffer).also { len = it } != -1) {
                    lastReadLength = len
                    val message = mHandler.obtainMessage(ON_DATA_READ)
                    message.obj = len
                    mHandler.sendMessage(message)
                } else {
                    val message = mHandler.obtainMessage(ON_READ_FINISH)
                    mHandler.sendMessage(message)
                    // 关闭文件流
                    mFileInputStream!!.close()
                }
            } catch (e: IOException) {
                try {
                    // 关闭文件流
                    mFileInputStream!!.close()
                } catch (ex: IOException) {
                }
            }
            if (pause) {
                onPause()
            }
        }
    }

    interface OnDataListener {
        fun prepare()
        fun onDataAvailble(data: ByteArray?, length: Int)
        fun onDataFinish(lastBufferFull: Boolean)
    }

}