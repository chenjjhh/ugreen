package com.wl.earbuds.ui.web

import android.os.Bundle
import android.os.LocaleList
import android.webkit.WebView
import android.webkit.WebViewClient
import com.wanlang.base.BaseDialog
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.databinding.FragmentProtocolWebBinding
import com.wl.earbuds.network.ApiService
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.LanguageUtils
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import me.hgj.jetpackmvvm.ext.lifecycle.KtxActivityManger
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.visible


class ProtocolWebFragment : BaseFragment<ProtocolWebViewModel, FragmentProtocolWebBinding>() {

    companion object {
        fun newInstance() = ProtocolWebFragment()
    }

    private var type = Constants.PROTOCOL_USER

    override fun setArguments(args: Bundle?) {
        super.setArguments(args)
        args?.let {
            type = it.getInt(Constants.PROTOCOL_TYPE)
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        switchLanguage()
        mDatabind.click = ProxyClick()
        initTitle()
        if (type == Constants.PROTOCOL_PRIVACY) {
            mDatabind.btnWithdraw.visible()
        }
        initWebview()
    }

    private fun switchLanguage() {
        val metrics = resources.displayMetrics
        val configuration = resources.configuration
        val localByLanguageType = LanguageUtils.getLocalByLanguageType()
        configuration.setLocale(localByLanguageType)
        configuration.setLocales(LocaleList(localByLanguageType))
        resources.updateConfiguration(configuration, metrics)
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(
            when (type) {
                Constants.PROTOCOL_USER -> getString(com.wl.resource.R.string.user_protocol_title)
                Constants.PROTOCOL_PRIVACY -> getString(
                    com.wl.resource.R.string.privacy_policy_title
                )
                else -> {
                    getString(com.wl.resource.R.string.product_manuals)
                }
            }
        ) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    override fun createObserver() {
        super.createObserver()
    }

    private fun initWebview() {
        mDatabind.webview.let {
            it.setBackgroundColor(0)
            it.background.alpha = 0
            it.settings.apply {
                javaScriptEnabled = true
                useWideViewPort = true
                loadWithOverviewMode = true
                setSupportZoom(false)
                allowFileAccess = false
                allowFileAccessFromFileURLs = false
            }
            it.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    return true
                }
            }
        }
    }

    override fun initData() {
        super.initData()
        val host = if (BuildConfig.DEBUG) ApiService.TEST_HOST else ApiService.HOST
        val url = "${host}agree/index?type=$type&language=${LanguageUtils.getLanguageType()}"
        mlog("url：$url")
        mDatabind.webview.loadUrl(url)
    }

    private fun showWithDrawDialog() {
        CommonNoticeDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.withdraw_title))
            .setCancelText(getString(com.wl.resource.R.string.withdraw_cancel))
            .setConfirmText(getString(com.wl.resource.R.string.withdraw_confirm))
            .setContent(getString(com.wl.resource.R.string.withdraw_hint))
            .contentAlignStart()
            .setActionListener(object : CommonNoticeDialog.ActionListener {
                override fun onCancel(dialog: BaseDialog?) {
                    super.onCancel(dialog)
                    dialog?.dismiss()
                }

                override fun onConfirm(dialog: BaseDialog?) {
                    super.onConfirm(dialog)
                    dialog?.dismiss()
                    SettingsUtils.isAgree = false
                    KtxActivityManger.finishAllActivity()
                }
            }).show()
    }

    inner class ProxyClick {
        fun onWithDraw() {
            showWithDrawDialog()
        }
    }
}