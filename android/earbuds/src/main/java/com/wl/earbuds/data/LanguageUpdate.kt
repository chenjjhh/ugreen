package com.wl.earbuds.data

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 10:26
 * Desc:
 */
data class LanguageUpdate(val type: String)