package com.wl.earbuds.ui.dialog

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wanlang.base.BaseDialog
import com.wanlang.base.BottomSheetDialog
import com.wl.earbuds.R
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.data.model.entity.GestureCommandBean
import com.wl.earbuds.data.model.eumn.Gesture
import com.wl.earbuds.data.model.eumn.Operation
import com.wl.earbuds.ui.adapter.GestureCommandAdapter
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.generateDoubleClickCommandData
import com.wl.earbuds.utils.generateLongPressCommandData
import com.wl.earbuds.utils.generateSingleClickCommandData
import com.wl.earbuds.utils.generateTripleClickCommandData
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration

/**
 * User: wanlang_dev
 * Data: 2022/11/8
 * Time: 16:21
 * Desc:
 */
class GestureCommandBottomDialog(context: Context, val type: Operation) : BottomSheetDialog(context) {

    private val data: MutableList<GestureCommandBean> by lazy {
        when (type) {
            Operation.LEFT_SINGLE,
            Operation.RIGHT_SINGLE
            -> {
                generateSingleClickCommandData(context)
            }
            Operation.LEFT_DOUBLE,
            Operation.RIGHT_DOUBLE-> {
                generateDoubleClickCommandData(context)
            }
            Operation.LEFT_TRIPLE,
            Operation.RIGHT_TRIPLE-> {
                generateTripleClickCommandData(context)
            }
            Operation.LEFT_LONG,
            Operation.RIGHT_LONG-> {
                generateLongPressCommandData(context)
            }
        }
    }

    private val mCommandAdapter: GestureCommandAdapter by lazy {
        GestureCommandAdapter(data).apply {
            setOnItemClickListener { adapter, view, position ->
                data[position].gesture.apply {
                    currentGesture = this
                }
            }
            currentGesture = when (type) {
                Operation.LEFT_SINGLE -> {CurrentDeviceManager.getGestureState().leftSingle}
                Operation.LEFT_DOUBLE -> {CurrentDeviceManager.getGestureState().leftDouble}
                Operation.LEFT_TRIPLE -> {CurrentDeviceManager.getGestureState().leftTriple}
                Operation.LEFT_LONG -> {CurrentDeviceManager.getGestureState().leftLong}
                Operation.RIGHT_SINGLE -> {CurrentDeviceManager.getGestureState().rightSingle}
                Operation.RIGHT_DOUBLE -> {CurrentDeviceManager.getGestureState().rightDouble}
                Operation.RIGHT_TRIPLE -> {CurrentDeviceManager.getGestureState().rightTriple}
                Operation.RIGHT_LONG -> {CurrentDeviceManager.getGestureState().rightLong}
            }
        }
    }

    private val mRecyclerView: RecyclerView? by lazy { findViewById(R.id.rv_item) }
    private val mCancelView: View? by lazy { findViewById(R.id.tv_cancel) }
    private val mConfirmView: View? by lazy { findViewById(R.id.tv_confirm) }
    private var _mListener: OnListener? = null

    init {
        setContentView(R.layout.dialog_gesture)
        mRecyclerView?.apply {
            layoutManager = LinearLayoutManager(context)
            val decoration =
                SpinnerDividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(35f),
                    dp2px(35f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mCommandAdapter
            itemAnimator = null
        }
        mCancelView?.setOnClickListener {
            _mListener?.onCancel(this)
        }
        mConfirmView?.setOnClickListener {
            _mListener?.onConfirm(this, type, mCommandAdapter.currentGesture)
        }
    }

    fun setListener(listener: OnListener?): GestureCommandBottomDialog {
        _mListener = listener
        return this
    }

    interface OnListener {
        fun onCancel(baseDialog: BaseDialog)
        fun onConfirm(baseDialog: BaseDialog, type: Operation, gesture: Gesture)
    }

}