package com.wl.earbuds.ui.adapter

import android.widget.RadioButton
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.AncBean
import com.wl.earbuds.data.model.eumn.AncLevel
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class AncAdapter(data: MutableList<AncBean>): BaseQuickAdapter<AncBean, BaseViewHolder>(R.layout.item_anc, data) {
    private var _currentAnc: AncLevel = AncLevel.ANC_DEEP
    private var _lastIndex: Int = -1
    var currentAnc: AncLevel
        set(value) {
            val selectIndex = data.indexOfFirst { it.ancLevel == value }
            if (selectIndex != -1) {
                _currentAnc = value
                notifyItemChanged(_lastIndex)
                notifyItemChanged(selectIndex)
                _lastIndex = selectIndex
            }
        }
        get() = _currentAnc!!
    override fun convert(holder: BaseViewHolder, item: AncBean) {
        holder.setText(R.id.tv_name, item.name)
        holder.getView<TextView>(R.id.tv_hint).apply {
            if (item.hint.isEmpty()) {
                gone()
            } else {
                visible()
                text = item.hint
            }
        }
        holder.getView<RadioButton>(R.id.radio_btn).isChecked =
            holder.adapterPosition == _lastIndex
    }
}