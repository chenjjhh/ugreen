package com.wl.earbuds.ui.upgrade

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wl.earbuds.R
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.data.model.entity.EarbudsVersionBean
import com.wl.earbuds.data.model.entity.VersionBean
import com.wl.earbuds.data.model.entity.VersionState
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.databinding.FragmentEarbudsVersionBinding
import com.wl.earbuds.ui.adapter.EarBudsVersionAdapter
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav

class EarbudsVersionFragment :
    BaseFragment<EarbudsVersionViewModel, FragmentEarbudsVersionBinding>() {

    companion object {
        fun newInstance() = EarbudsVersionFragment()
    }

    private var isTop = false

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initTitle()
        initRv()
    }

    private var versionData: MutableList<VersionBean> = mutableListOf()

    private val mVersionAdapter: EarBudsVersionAdapter by lazy {
        EarBudsVersionAdapter(versionData).apply {
            setOnItemClickListener { _, _, position ->
                // 有新版本跳转更新
                if (position == 0 && data.size == 2) {
                    safeNav {
                        nav().navigate(
                            R.id.action_earbudsVersion_to_earbudsUpgrade,
                            Bundle().apply {
                                putParcelable(
                                    Constants.EARBUDS_VERSION,
                                    mViewModel.versionState.value!!.data
                                )
                            })
                    }
                }
            }
        }
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.hardwire_version)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mVersionAdapter
            val decoration =
                SpinnerDividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(18f),
                    dp2px(18f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            itemAnimator = null
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        mViewModel.hasNewVersion.observe(viewLifecycleOwner, ::onHasNewVersion)
        mViewModel.versionState.observe(viewLifecycleOwner, ::onVersion)
        CurrentDeviceManager.observeVersionState(viewLifecycleOwner, ::onVersionChange)
        mViewModel.getEarbudsVersion()
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("onOnlineChange")
        if (isTop) {
            checkOnlineStatus()
        }
    }

    private fun onHasNewVersion(isNew: Boolean) {
        if (isNew) {
            mDatabind.btnCheckAndUpdate.text = getString(com.wl.resource.R.string.one_click_update)
        }
    }

    private fun onVersionChange(versionState: VersionState) {
        versionData.clear()
        versionData.add(VersionBean(versionState.getVersion(), isCurrent = true))
    }


    private fun onVersion(uiState: ResponseUiState<EarbudsVersionBean>) {
        versionData.clear()
        val version = CurrentDeviceManager.getVersionState().getVersion()
        if (uiState.isSuccess) {
            if (version == uiState.data!!.versionName) {
                showMsg(getString(com.wl.resource.R.string.hint_current_verion_is_latest))
                mViewModel.versionDesc.set(getString(com.wl.resource.R.string.version_is_latest))
                versionData.add(VersionBean(version, isLatest = true, isCurrent = true))
            } else {
                showMsg(getString(com.wl.resource.R.string.find_new_version))
                mViewModel.versionDesc.set(getString(com.wl.resource.R.string.find_new_version))
                versionData.add(VersionBean(uiState.data!!.versionName, isLatest = false))
                versionData.add(VersionBean(version, isLatest = false, isCurrent = true))
            }
        } else {
            showMsg(getString(com.wl.resource.R.string.network_error))
            versionData.add(VersionBean(version, isLatest = true, isCurrent = true))
        }
        mVersionAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        isTop = true
        checkOnlineStatus()
    }

    private fun checkOnlineStatus() {
        if (!CurrentDeviceManager.isOnline()) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mlog("onPause")
        isTop = false
    }

    inner class ProxyClick {
        fun checkAndUpgrade() {
            if (true != mViewModel.hasNewVersion.value) {
                mViewModel.getEarbudsVersion()
                return
            }
            safeNav {
                nav().navigate(R.id.action_earbudsVersion_to_earbudsUpgrade, Bundle().apply {
                    putParcelable(Constants.EARBUDS_VERSION, mViewModel.versionState.value!!.data)
                })
            }
        }
    }
}