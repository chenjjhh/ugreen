package com.wl.earbuds.ui.web

import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.earbuds.data.model.entity.ContentResponse
import com.wl.earbuds.network.apiService
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.request

class ProtocolWebViewModel : BaseViewModel() {

    var contentState: UnPeekLiveData<ContentResponse> = UnPeekLiveData()

    fun getContent(type: Int) {
        request({
            apiService.getProtocol(type)
        }, {
            contentState.value = it
        })
    }
}