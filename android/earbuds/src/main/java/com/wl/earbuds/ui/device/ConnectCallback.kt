package com.wl.earbuds.ui.device

import android.bluetooth.BluetoothDevice

/**
 * User: wanlang_dev
 * Data: 2022/11/25
 * Time: 13:44
 * Desc:
 */
interface ConnectCallback {
    fun connectDevice(device: BluetoothDevice)
}