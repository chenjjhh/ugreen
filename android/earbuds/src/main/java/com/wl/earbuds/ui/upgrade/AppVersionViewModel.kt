package com.wl.earbuds.ui.upgrade

import androidx.lifecycle.viewModelScope
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.data.model.entity.AppVersionResponse
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.network.apiService
import com.wl.earbuds.utils.ext.mlog
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.StringObservableField
import me.hgj.jetpackmvvm.ext.request
import zlc.season.downloadx.State
import zlc.season.downloadx.core.DownloadTask
import zlc.season.downloadx.download

class AppVersionViewModel : BaseViewModel() {
    var versionDesc = StringObservableField("")
    var hasNewVersion = UnPeekLiveData<Boolean>(false)
    var downloadState = UnPeekLiveData<State>()
    val appVersionState: UnPeekLiveData<ResponseUiState<AppVersionResponse>> = UnPeekLiveData()
    var downloadTask: DownloadTask? = null

    fun getAppVersion() {
        request({
            apiService.getAppVersion()
        }, {
            appVersionState.value = ResponseUiState(true, it)
            hasNewVersion.value = BuildConfig.versionCode < it.versionCode
        }, {
            appVersionState.value = ResponseUiState(false)
        })
    }

    fun startDownload() {
        mlog("startDownload")
        if (downloadTask == null || downloadTask!!.isStarted()) return
        downloadTask = viewModelScope.download(appVersionState.value!!.data!!.fileUrl).apply {
            state().onEach {
                downloadState.value = it
            }.launchIn(viewModelScope)
            start()
        }
    }
}