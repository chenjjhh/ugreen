package com.wl.earbuds.bluetooth.connect

import android.bluetooth.BluetoothDevice

/**
 * User: wanlang_dev
 * Data: 2022/11/15
 * Time: 17:01
 * Desc:
 */
interface ConnectListener {
    fun onAutoConnectable(devices: List<BluetoothDevice>) {}
    fun onDeviceListUpdate(devices: MutableList<BluetoothDevice>) {}
    fun onDeviceBond(device: BluetoothDevice) {}
    fun onPairRefused(device: BluetoothDevice) {}
    fun onDeviceFound(device: BluetoothDevice) {}
    fun onDeviceConnected(device: BluetoothDevice) {}
    fun onHeadsetConnected(device: BluetoothDevice) {}
    fun onHeadsetDisconnected(device: BluetoothDevice) {}
    fun onDiscoveryFinished(devices: MutableList<BluetoothDevice>) {}
    fun onDiscoveryStart() {}
    fun onDiscoveryFail(status: Int) {}
    fun onConnectFail(address: String, status: Int) {}
}