package com.wl.earbuds.bluetooth.utils;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public final class BluetoothUtils {

    private static final String TAG = "BluetoothUtils";

    public static BluetoothAdapter getBluetoothAdapter(Context context) {
        BluetoothManager manager = context == null ? null :
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);

        if (manager == null) {
            Log.i(TAG, "[getBluetoothAdapter] No available BluetoothManager." +
                    " BluetoothAdapter initialised with BluetoothAdapter.getDefaultAdapter().");
            return BluetoothAdapter.getDefaultAdapter();
        } else {
            return manager.getAdapter();
        }
    }

    /**
     * <p>This method requests the {@link BluetoothDevice BluetoothDevice} that corresponds to
     * the given Bluetooth address from the system.</p>
     *
     * @param adapter The BluetoothAdapter to use to request Bluetooth resources from the system.
     * @param address The Bluetooth address to find the corresponding {@link BluetoothDevice
     *                BluetoothDevice} for.
     * @return The corresponding {@link BluetoothDevice BluetoothDevice} or <code>null</code> if
     * it could not be found.
     */
    @Nullable
    public static BluetoothDevice findBluetoothDevice(@NonNull BluetoothAdapter adapter,
                                                      @NonNull String address) {
        if (address.length() == 0) {
            Log.w(TAG, "[findBluetoothDevice] Bluetooth address null or empty.");
            return null;
        }

        if (!BluetoothAdapter.checkBluetoothAddress(address)) {
            Log.w(TAG, "[findBluetoothDevice] Unknown BT address.");
            return null;
        }

        BluetoothDevice device = adapter.getRemoteDevice(address);

        if (device == null) {
            Log.w(TAG, "[findBluetoothDevice] No corresponding remote device.");
            return null;
        }

        return device;
    }

    public static boolean areScanPermissionsGranted(@NonNull Context context) {
        if (Build.VERSION_CODES.S <= Build.VERSION.SDK_INT) {
            String[] permissions = new String[]{Manifest.permission.BLUETOOTH_SCAN};
            return SystemUtils.arePermissionsGranted(context, permissions);
        } else {
            return true;
        }
    }

    public static boolean areConnectPermissionsGranted(@NonNull Context context) {
        if (Build.VERSION_CODES.S <= Build.VERSION.SDK_INT) {
            String[] permissions = new String[]{Manifest.permission.BLUETOOTH_CONNECT};
            return SystemUtils.arePermissionsGranted(context, permissions);
        } else {
            return true;
        }
    }
}
