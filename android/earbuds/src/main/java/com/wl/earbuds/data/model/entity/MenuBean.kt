package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/12/3
 * Time: 14:21
 * Desc:
 */
data class MenuBean(
    val type: Int,
    val name: String
)