package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/12/12
 * Time: 9:43
 * Desc:
 */
data class CodecState(val codecData: List<CodecBean>)
