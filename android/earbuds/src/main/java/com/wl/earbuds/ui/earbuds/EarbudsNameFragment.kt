package com.wl.earbuds.ui.earbuds

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import com.wl.earbuds.R
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.databinding.FragmentEarbudsNameBinding
import com.wl.earbuds.utils.ext.click
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.showMsg
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.visible

class EarbudsNameFragment : BaseFragment<EarbudsNameViewModel, FragmentEarbudsNameBinding>() {

    companion object {
        fun newInstance() = EarbudsNameFragment()
    }

    private val originName = CurrentDeviceManager.ugDevice!!.name


    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
        initInput()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(CurrentDeviceManager.ugDevice!!.name) {
            safeNav {
                nav().navigateUp()
            }
        }
        mDatabind.toolbar.toolbar.findViewById<View>(R.id.tv_right).also {
            it.visible()
            it.click {
                saveName()
            }
        }
    }

    private fun initInput() {
        mDatabind.etName.setText(originName)
    }

    private fun saveName() {
        val newName = mDatabind.etName.text.toString().trim()
        if (newName.isEmpty()) {
            showMsg(getString(com.wl.resource.R.string.name_is_empty))
            return
        }
        if (newName != originName) {
            mViewModel.updateName(newName)
        }
        safeNav {
            nav().navigateUp()
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        if (!isOnline) {
            safeNav {
                nav().navigate(R.id.action_earbudsName_to_home)
            }
        }
    }
}