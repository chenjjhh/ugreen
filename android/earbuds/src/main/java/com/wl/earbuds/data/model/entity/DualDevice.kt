package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 10:04
 * Desc:
 */
data class DualDevice(val name: String, val index: Int = 1, val address: String = "") {
}