package com.wl.earbuds.bluetooth.ota

import android.os.Handler
import android.os.Looper
import android.os.Message
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.bluetooth.task.TaskManager
import com.wl.earbuds.bluetooth.utils.ArrayUtil
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.bluetooth.utils.getFileCrc32
import com.wl.earbuds.data.model.entity.OtaBean
import com.wl.earbuds.utils.ext.mlog
import java.io.File
import java.util.zip.CRC32

/**
 * User: wanlang_dev
 * Data: 2022/11/29
 * Time: 9:40
 * Desc:
 */
class OtaManager(val listener: OtaListener, private val taskManager: TaskManager) {

    companion object {
        const val CODE_RESUME = 100
        const val CODE_CONFIG = 101
        const val CODE_DATA = 102
        const val CODE_VERIFY = 103
        const val CODE_RESULT = 104
        const val CODE_APPLY = 105
        const val TIME_OUT = 3000L
    }

    private val magicCode: ByteArray = byteArrayOf(0x42, 0x45, 0x53, 0x54)

    private var transmitSize = 0L
    private var blockSize = 65536   // 8k
    private var transferSize = 0
    private var startOffset = 0L
    private var mFileCrc32Value = 0L

    private var mOtaInputThread: OtaInputThread? = null
    private var mOtaFileSize = 0L
    private var mFilePath: String? = null
    private var mUploadStopFlag: Boolean = false
    private var mCrc32: CRC32? = null
    private var mOtaBean: OtaBean? = null

    @OtaConstants.Companion.OtaState
    private var mOtaState: Int = OtaConstants.OTA_IDLE

    private val mHandler by lazy {
        object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
                super.handleMessage(msg)
                when (msg.what) {
                    CODE_RESUME -> {
                        mlog("CODE_RESUME timeout")
                        upgradeTimeout()
                    }
                    CODE_CONFIG -> {
                        mlog("CODE_CONFIG timeout")
                        upgradeTimeout()
                    }
                    CODE_DATA -> {
                        mlog("CODE_DATA timeout")
                        upgradeTimeout()
                    }
                    CODE_VERIFY -> {
                        mlog("CODE_VERIFY timeout")
                        upgradeTimeout()
                    }
                    CODE_RESULT -> {
                        mlog("CODE_RESULT timeout")
                        upgradeTimeout()
                    }
                    CODE_APPLY -> {
                        mlog("CODE_APPLY timeout")
                        upgradeTimeout()
                    }
                }
            }
        }
    }

    private fun updateState(@OtaConstants.Companion.OtaState newState: Int) {
        mOtaState = newState
    }

    fun isUpgrading() = mOtaState == OtaConstants.OTA_UPGRADING

    fun startOta(otaBean: OtaBean) {
        if (mOtaState == OtaConstants.OTA_UPGRADING) return
        resetUpgrade()
        onStartUpgrade()
        mOtaBean = otaBean
        updateState(OtaConstants.OTA_UPGRADING)
        mUploadStopFlag = false
        val file = File(otaBean.filePath)
        mFileCrc32Value = getFileCrc32(file)
        mFilePath = otaBean.filePath
        mOtaFileSize = file.length()
        checkRandomCode(otaBean)
        sendResumeVerifyReq()
    }

    private fun checkRandomCode(otaBean: OtaBean) {
        if (otaBean.fileMd5 != SettingsUtils.lastFileMd5) {
            SettingsUtils.lastFileMd5 = otaBean.fileMd5
            SettingsUtils.lastRandomCode = ""
        }
    }

    fun resetUpgrade() {
        transmitSize = 0
        transferSize = 0
        mOtaFileSize = 0
        startOffset = 0
        mCrc32 = null
        mOtaInputThread?.setDataListener(null)
        mOtaInputThread?.interrupt()
    }

    private fun sendResumeVerifyReq() {
        val commandBytes = ByteArray(41).apply {
            set(0, OtaConstants.COMMAND_RESUME_VERIFY.toByte())
            BytesUtils.copyArray(magicCode, this, 1)
            val lastRandomCode = SettingsUtils.lastRandomCode
            val randomCode =
                if (lastRandomCode.isEmpty() || lastRandomCode.length != 64) {
                    ByteArray(32)
                } else {
                    ArrayUtil.toBytes(lastRandomCode)
                }
            BytesUtils.copyArray(
                randomCode, this, 5
            )
            val crc32 = CRC32()
            crc32.update(randomCode)
            BytesUtils.setUINT32Little(crc32.value, this, 37)
        }
        mHandler.sendEmptyMessageDelayed(CODE_RESUME, TIME_OUT)
        listener.sendOtaPacket(commandBytes)
    }

    private fun sendConfigReq() {
        val filesizeBytes = ArrayUtil.intToBytesLittle(mOtaFileSize.toInt())
        val commandBytes = ByteArray(9).apply {
            set(0, OtaConstants.COMMAND_CONFIG.toByte())
            BytesUtils.copyArray(
                filesizeBytes, this, 1
            )
            BytesUtils.setUINT32Little(mFileCrc32Value, this, 5)
        }
        mHandler.sendEmptyMessageDelayed(CODE_CONFIG, TIME_OUT)
        listener.sendOtaPacket(commandBytes)
    }

    private fun sendImageApply() {
        val commandBytes = ByteArray(5).apply {
            set(0, OtaConstants.COMMAND_IMAGE_APPLY.toByte())
            BytesUtils.copyArray(magicCode, this, 1)
        }
        mHandler.sendEmptyMessageDelayed(CODE_APPLY, TIME_OUT)
        listener.sendOtaPacket(commandBytes)
    }

    private fun sendDataPacketReq(packetData: ByteArray, length: Int) {
        val commandBytes = ByteArray(length + 1).apply {
            set(0, OtaConstants.COMMAND_DATA_PACKET.toByte())
            System.arraycopy(packetData, 0, this, 1, length)
        }
        mHandler.sendEmptyMessageDelayed(CODE_DATA, TIME_OUT)
        listener.sendOtaPacket(commandBytes)
    }

    private fun sendSegmentVerifyReq() {
        val commandBytes = ByteArray(9).apply {
            set(0, OtaConstants.COMMAND_SEGMENT_VERIFY.toByte())
            BytesUtils.copyArray(magicCode, this, 1)
            BytesUtils.setUINT32Little(mCrc32!!.value, this, 5)
        }
        mHandler.sendEmptyMessageDelayed(CODE_VERIFY, TIME_OUT)
        listener.sendOtaPacket(commandBytes)
        //重置crc32
        mCrc32!!.reset()
    }

    private fun sendGetOtaResultReq() {
        val commandBytes = ByteArray(1).apply {
            set(0, OtaConstants.COMMAND_GET_OTA_RESULT.toByte())
        }
        mHandler.sendEmptyMessageDelayed(CODE_RESULT, TIME_OUT)
        listener.sendOtaPacket(commandBytes)
    }

    private fun prepareSendFile(transferSize: Int) {
        val filePath = mFilePath!!
        mOtaInputThread = OtaInputThread(filePath, transferSize).apply {
            setDataListener(object : OtaInputThread.OnDataListener {
                override fun prepare() {
                    mlog("prepare: crc32: " + mCrc32?.getValue())
                    transmitSize += startOffset
                    if (mOtaFileSize > 0) {
                        updateProgress((transmitSize * 100 / mOtaFileSize).toInt())
                    }
                }

                override fun onDataAvailble(data: ByteArray?, length: Int) {
                    mlog(
                        "onDataAvailble: length = $length"
                    )
                    if (mCrc32 == null) {
                        mCrc32 = CRC32()
                    }
                    mCrc32?.update(data, 0, length)
                    mlog("onDataAvailble: crc32: " + mCrc32?.getValue())
                    sendDataPacketReq(data!!, length)
                    transmitSize += length
                    startOffset += length
                    mlog(
                        "onDataAvailble: transmitSize / totalsize = $transmitSize / $mOtaFileSize"
                    )
                }

                override fun onDataFinish(lastBufferFull: Boolean) {
                    mlog("onDataFinish: lastBufferFull$lastBufferFull")
                    if (lastBufferFull) {
                        // sendDataPacketReq(ByteArray(0), 0)
                    }
                    sendGetOtaResultReq()
                }
            })
            setSkipOffset(startOffset, blockSize)
            start()
        }
    }

    fun parseOtaResponse(data: ByteArray) {
        mlog("parseOtaResponse")
        val commandId = BytesUtils.getUINT8(data, 0)
        var status = if (data.size > 1) {
            BytesUtils.getUINT8(data, 1)
        } else {
            1
        }
        when (commandId) {
            OtaConstants.RESPONSE_RESUME_VERIFY -> {
                parseResumeVerifyStatus(data)
            }
            OtaConstants.RESPONSE_CONFIG -> {
                status = BytesUtils.getUINT8(data, 3)
                parseConfigStatus(status, data)
            }
            OtaConstants.RESPONSE_DATA_PACKET -> {
                parseDataPacketStatus(status)
            }
            OtaConstants.RESPONSE_SEGMENT_VERIFY -> {
                parseSegmentVerifyStatus(status)
            }
            OtaConstants.RESPONSE_GET_OTA_RESULT -> {
                parseGetOtaResultStatus(status)
            }
            OtaConstants.RESPONSE_IMAGE_APPLY -> {
                parseImageApplyStatus(status)
            }
            else -> {
                mlog("UNKNOWN Response: ${BytesUtils.byte2HexStr(data)}")
            }
        }
    }

    private fun parseResumeVerifyStatus(data: ByteArray) {
        mHandler.removeMessages(CODE_RESUME)
        startOffset = BytesUtils.getUINT32Little(data, 1)
        val randomCode = data.copyOfRange(5, 37)
        SettingsUtils.lastRandomCode = BytesUtils.byte2HexStr(randomCode)
        sendConfigReq()
    }

    private fun parseConfigStatus(status: Int, data: ByteArray) {
        mHandler.removeMessages(CODE_CONFIG)
        if (status == 1) {
            val mtu = BytesUtils.getUINT16Little(data, 1)
            transferSize = if (mtu > 512) 512 else 256
            mlog("parseConfigStatus size: $transferSize")
            prepareSendFile(transferSize)
        } else {
            upgradeFail()
        }
    }

    private fun parseDataPacketStatus(status: Int) {
        mHandler.removeMessages(CODE_DATA)
        if (status == 1) {
            if (mOtaFileSize > 0) {
                updateProgress((transmitSize * 100 / mOtaFileSize).toInt())
            }
            if (transmitSize % blockSize == 0L) {
                sendSegmentVerifyReq()
            } else {
                resumeOtaThread()
            }
        } else {
            upgradeFail()
        }
    }

    private fun parseSegmentVerifyStatus(status: Int) {
        mHandler.removeMessages(CODE_VERIFY)
        if (status == 1) {
            resumeOtaThread()
        } else {
            upgradeFail()
        }
    }

    private fun parseGetOtaResultStatus(status: Int) {
        mHandler.removeMessages(CODE_RESULT)
        if (status == 1) {
            sendImageApply()
        } else {
            upgradeFail()
        }
    }

    private fun parseImageApplyStatus(status: Int) {
        mHandler.removeMessages(CODE_APPLY)
        if (status == 1) {
            upgradeSuccess()
        } else {
            upgradeFail()
        }
    }

    private fun onStartUpgrade() {
        taskManager.runOnMain {
            listener.onStartUpgrade()
        }
    }

    private fun updateProgress(progress: Int) {
        mlog("ota updateProgress $progress")
        taskManager.runOnMain {
            listener.updateProgress(progress)
        }
    }

    private fun upgradeFail() {
        mlog("ota upgradeFail")
        updateState(OtaConstants.OTA_EXCEPTION)
        taskManager.runOnMain {
            listener.onFail()
        }
    }

    private fun upgradeTimeout() {
        mlog("ota upgradeTimeout")
        updateState(OtaConstants.OTA_TIMEOUT)
        taskManager.runOnMain {
            listener.onFail()
        }
    }

    private fun upgradeSuccess() {
        mlog("ota upgradeSuccess")
        taskManager.runOnMain {
            listener.onSuccess()
        }
        updateState(OtaConstants.OTA_IDLE)
    }

    private fun resumeOtaThread() {
        if (mUploadStopFlag) {
            updateState(OtaConstants.OTA_IDLE)
            return
        }
        mOtaInputThread?.resumeThread()
    }

    fun stopOtaUpgrade() {
        mUploadStopFlag = true
    }
}