package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:21
 * Desc:
 */
enum class Voice(val value: Int) {
    CHINESE(0x01),
    ENGLISH(0x00),
    RING(0x02);


    companion object {
        @JvmStatic
        fun valueOf(value: Int): Voice = values().find { type -> type.value == value.toInt() } ?: CHINESE
    }
}