package com.wl.earbuds.bluetooth.ota

import androidx.annotation.IntDef

/**
 * User: wanlang_dev
 * Data: 2022/11/29
 * Time: 9:45
 * Desc:
 */
interface OtaConstants {
    companion object {
        val COMMAND_RESUME_VERIFY = 0x8C
        val COMMAND_CONFIG = 0x86
        val COMMAND_DATA_PACKET = 0x85
        val COMMAND_SEGMENT_VERIFY = 0x82
        val COMMAND_GET_OTA_RESULT = 0x88
        val COMMAND_IMAGE_APPLY = 0x92

        val RESPONSE_RESUME_VERIFY = 0x8D
        val RESPONSE_CONFIG = 0x87
        val RESPONSE_DATA_PACKET = 0x8B
        val RESPONSE_SEGMENT_VERIFY = 0x83
        val RESPONSE_GET_OTA_RESULT = 0x84
        val RESPONSE_IMAGE_APPLY = 0x93

        const val OTA_IDLE = 0
        const val OTA_UPGRADING = 1
        const val OTA_EXCEPTION = 2
        const val OTA_TIMEOUT = 3
        @IntDef(
            OTA_IDLE,
            OTA_UPGRADING,
            OTA_EXCEPTION,
            OTA_TIMEOUT,
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class OtaState
    }
}