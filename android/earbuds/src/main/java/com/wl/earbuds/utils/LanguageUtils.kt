package com.wl.earbuds.utils

import android.content.Context
import com.wl.earbuds.app.settings.SettingsUtils
import java.util.Locale

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 9:50
 * Desc:
 */

object LanguageUtils {
    const val LANGUAGE_TYPE_CHINESE = "cn"
    const val LANGUAGE_TYPE_ENGLISH = "en"
    const val LANGUAGE_TYPE_FRENCH = "fr"
    const val LANGUAGE_TYPE_GERMAN = "de"
    const val LANGUAGE_TYPE_RUSSIAN = "ru"
    const val LANGUAGE_TYPE_JAPANESE = "ja"
    const val LANGUAGE_TYPE_SPANISH = "es"
    const val LANGUAGE_TYPE_ITALIAN = "it"
    const val LANGUAGE_TYPE_ARABIC = "ar"

    fun getLanguageDesc(context: Context): String {
        return when(SettingsUtils.language) {
            LANGUAGE_TYPE_CHINESE -> {context.getString(com.wl.resource.R.string.language_sc_local)}
            LANGUAGE_TYPE_ENGLISH -> {context.getString(com.wl.resource.R.string.language_en_local)}
            LANGUAGE_TYPE_FRENCH -> {context.getString(com.wl.resource.R.string.language_fr_local)}
            LANGUAGE_TYPE_GERMAN -> {context.getString(com.wl.resource.R.string.language_de_local)}
            LANGUAGE_TYPE_RUSSIAN -> {context.getString(com.wl.resource.R.string.language_ru_local)}
            LANGUAGE_TYPE_JAPANESE -> {context.getString(com.wl.resource.R.string.language_ja_local)}
            LANGUAGE_TYPE_SPANISH -> {context.getString(com.wl.resource.R.string.language_es_local)}
            LANGUAGE_TYPE_ITALIAN -> {context.getString(com.wl.resource.R.string.language_it_local)}
            LANGUAGE_TYPE_ARABIC -> {context.getString(com.wl.resource.R.string.language_ar_local)}
            else -> {context.getString(com.wl.resource.R.string.language_sc_local)}
        }
    }

    fun getLocalByLanguageType(): Locale {
        var localType = SettingsUtils.language
        return when (localType) {
            LANGUAGE_TYPE_CHINESE -> {
                Locale.forLanguageTag("zh")
            }
            LANGUAGE_TYPE_ENGLISH -> {
                Locale.forLanguageTag("en")
            }
            LANGUAGE_TYPE_FRENCH -> {
                Locale.forLanguageTag("fr")
            }
            LANGUAGE_TYPE_GERMAN -> {
                Locale.forLanguageTag("de")
            }
            LANGUAGE_TYPE_RUSSIAN -> {
                Locale.forLanguageTag("ru")
            }
            LANGUAGE_TYPE_JAPANESE -> {
                Locale.forLanguageTag("ja")
            }
            LANGUAGE_TYPE_SPANISH -> {
                Locale.forLanguageTag("es")
            }
            LANGUAGE_TYPE_ITALIAN -> {
                Locale.forLanguageTag("it")
            }
            LANGUAGE_TYPE_ARABIC -> {
                Locale.forLanguageTag("ar")
            }
            else -> {
                Locale.forLanguageTag("zh")
            }
        }
    }

    fun getLanguageType(): String {
        return when(SettingsUtils.language) {
            LANGUAGE_TYPE_CHINESE -> {"cn"}
            LANGUAGE_TYPE_ENGLISH -> {"en"}
            LANGUAGE_TYPE_FRENCH -> {"fr"}
            LANGUAGE_TYPE_GERMAN -> {"de"}
            LANGUAGE_TYPE_RUSSIAN -> {"ru"}
            LANGUAGE_TYPE_JAPANESE -> {"ja"}
            LANGUAGE_TYPE_SPANISH -> {"es"}
            LANGUAGE_TYPE_ITALIAN -> {"it"}
            LANGUAGE_TYPE_ARABIC -> {"ar"}
            else -> {"cn"}
        }
    }


}