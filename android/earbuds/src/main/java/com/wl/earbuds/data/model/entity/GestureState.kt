package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Gesture

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 9:45
 * Desc:
 */
data class GestureState(
    val leftSingle: Gesture = Gesture.NONE,
    val leftDouble: Gesture = Gesture.NONE,
    val leftTriple: Gesture = Gesture.NONE,
    val leftLong: Gesture = Gesture.NONE,
    val rightSingle: Gesture = Gesture.NONE,
    val rightDouble: Gesture = Gesture.NONE,
    val rightTriple: Gesture = Gesture.NONE,
    val rightLong: Gesture = Gesture.NONE,
) {
}