package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/12/2
 * Time: 16:31
 * Desc:
 */
data class AppConfig(
    val settingsEnable: Boolean = true,
    val deviceSettingsEnable: Boolean = true,
    val addDeviceEnable: Boolean = true,
    val disconnectEnable: Boolean = true,
    val deleteEnable: Boolean = true,
    val manualEnable: Boolean = true,
    val eqEnable: Boolean = true,
    val gameModeEnable: Boolean = true,
    val dualEnable: Boolean = true,
    val findEnable: Boolean = true,
    val hqEnable: Boolean = true,
    val upgradeEnable: Boolean = true
)
