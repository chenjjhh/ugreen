package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Voice

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:33
 * Desc:
 */
data class VoiceBean(val name: String,val voice: Voice)