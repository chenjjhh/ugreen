package com.wl.earbuds.bluetooth.task;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

public class MainThreadExecutor {

    private final Handler handler = new Handler(Looper.getMainLooper());

    public void execute(@NonNull Runnable runnable) {
        handler.post(runnable);
    }

}
