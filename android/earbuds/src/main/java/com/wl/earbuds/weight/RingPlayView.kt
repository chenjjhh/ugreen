package com.wl.earbuds.weight

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.wl.earbuds.R

/**
 * User: wanlang_dev
 * Data: 2022/11/5
 * Time: 18:51
 * Desc:
 */
class RingPlayView : ConstraintLayout {
    private var _textview: TextView? = null
    private var _playView: ImageView? = null
    private var _isPlay: Boolean = false
    var isPlay: Boolean
        get() = _isPlay
        set(value) {
            if (value) {
                _playView?.setImageResource(R.mipmap.ic_stop_play)
            } else {
                _playView?.setImageResource(R.mipmap.ic_start_play)
            }
            _isPlay = value
        }

    private var _ringOperateCallback: IRingOperateCallback? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.layout_play, this, true)
        _textview = findViewById(R.id.tv_command)
        _playView = findViewById(R.id.iv_play)
        setOnClickListener {
            if (_isPlay) {
                _ringOperateCallback?.stopPlay()
            } else {
                _ringOperateCallback?.startPlay()
            }
            _isPlay = !_isPlay
        }
    }


    override fun setEnabled(enabled: Boolean) {
        _playView?.visibility = if (enabled) VISIBLE else GONE
        super.setEnabled(enabled)
    }

    fun setCommandText(@StringRes strRes: Int) {
        _textview?.setText(strRes)
    }

    fun setRingOperateCallback(callback: IRingOperateCallback?) {
        _ringOperateCallback = callback
    }

    interface IRingOperateCallback {
        fun startPlay()
        fun stopPlay()
    }
}