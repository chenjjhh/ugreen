package com.wl.earbuds.ui.adapter

import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.VersionBean
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.invisible
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/12/1
 * Time: 15:34
 * Desc:
 */
class EarBudsVersionAdapter(data: MutableList<VersionBean>) :
    BaseQuickAdapter<VersionBean, BaseViewHolder>(R.layout.item_version, data) {
    override fun convert(holder: BaseViewHolder, item: VersionBean) {
        holder.setText(
            R.id.tv_name,
            if (item.isCurrent)
                context.getString(com.wl.resource.R.string.current_version)
            else
                context.getString(
                    com.wl.resource.R.string.latest_version
                )
        )
        holder.setText(R.id.tv_version, item.name)
        if (item.isLatest) {
            holder.getView<View>(R.id.tv_is_latest).visible()
        } else {
            holder.getView<View>(R.id.tv_is_latest).gone()
        }
        if (item.isCurrent) {
            holder.getView<View>(R.id.iv_arrow).invisible()
        } else {
            holder.getView<View>(R.id.iv_arrow).visible()
        }
    }
}