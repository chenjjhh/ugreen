package com.wl.earbuds.weight

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.annotation.ColorInt
import com.wl.earbuds.R
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.mlog
import kotlin.math.min

/**
 * User: wanlang_dev
 * Data: 2022/11/1
 * Time: 18:05
 * Desc:
 */
class ColorScanView : View {

    companion object {
        const val INDICATOR_COLOR = "#FF17B34F"
        const val INNER_LINE_COLOR = "#FF17B34F"
        const val OUTER_LINE_COLOR = "#5417B34F"
        const val FAN_SHAPED_COLOR = "#1A17B34F"
        const val BOTTOM_COLOR = "#1417B34F"
        const val BOTTOM_LEFT_CIRCLE_COLOR = "#2900EFFF"
        const val TOP_RIGHT_CIRCLE_COLOR = "#4017B34F"
    }

    private var default_size: Int = 0
    private var mSize: Int = 0
    private var mRadius: Float = 0f
    private var mInnerLineWidth: Int = 0
    private var mOuterLineWidth: Int = 0
    private var mFanShapedRadius: Int = 0
    private var mFanShapedWidth: Int = 0
    private var mIndicatorLineWidth: Int = 0

    // 模糊宽度
    private var mMaskerRadius: Float = 0f
    private var mInnerRadius: Float = 0f
    private var mOuterRadius: Float = 0f
    private var mIndicatorRadius: Float = 0f
    private var mBlurCircleRelativeX: Float = 0f
    private var mBlurCircleRelativeY: Float = 0f

    @ColorInt
    private var mInnerLineColor: Int = 0

    @ColorInt
    private var mOuterLineColor: Int = 0

    @ColorInt
    private var mBottomLeftCircleColor: Int = 0

    @ColorInt
    private var mTopRightCircleColor: Int = 0

    @ColorInt
    private var mBottomColor: Int = 0

    @ColorInt
    private var mFanShapedColor: Int = 0

    @ColorInt
    private var mIndicatorColor: Int = 0
    private var mCircleRadius: Float = 0f

    // 标识Rect
    private var mIndicatorRect: RectF = RectF()
    private var mFanShapedRect: RectF = RectF()

    private val shapedPaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.style = Paint.Style.FILL
        }
    }

    private val linePaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.style = Paint.Style.STROKE
        }
    }

    private val blurPaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.style = Paint.Style.FILL
        }
    }

    private val mFanShapedAnimator: ValueAnimator by lazy {
        ValueAnimator.ofFloat(0f, 45f).apply {
            repeatMode = ValueAnimator.REVERSE
            repeatCount = ValueAnimator.INFINITE
            duration = 650
            interpolator = LinearInterpolator()
            addUpdateListener {
                mFanShapedOffset = animatedValue.toString().toFloat()
            }
        }
    }

    private val mIndicatorAnimator: ValueAnimator by lazy {
        ValueAnimator.ofFloat(0f, 360f).apply {
            repeatMode = ValueAnimator.RESTART
            repeatCount = ValueAnimator.INFINITE
            duration = 2000
            interpolator = LinearInterpolator()
            addUpdateListener {
                val newValue = animatedValue.toString().toFloat()
                if (newValue != mIndicatorOffset) {
                    mIndicatorOffset = animatedValue.toString().toFloat()
                    postInvalidate()
                }
            }
        }
    }

    private val mCircleScaleAnimator: ValueAnimator by lazy {
        ValueAnimator.ofFloat(1f, 0.7f).apply {
            repeatMode = ValueAnimator.REVERSE
            repeatCount = ValueAnimator.INFINITE
            duration = 750
            interpolator = LinearInterpolator()
            addUpdateListener {
                mCircleScale = animatedValue.toString().toFloat()
            }
        }
    }
    private var mCircleScale = 1f
    private var mIndicatorOffset = 0f
    private var mFanShapedOffset = 0f
    private var isAnimatorStarted: Boolean = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        context.obtainStyledAttributes(attrs, R.styleable.ColorScan).also {
            mInnerLineWidth =
                it.getDimensionPixelSize(R.styleable.ColorScan_scanInnerLineWidth, dp2px(1.25f))
            mOuterLineWidth =
                it.getDimensionPixelSize(R.styleable.ColorScan_scanOuterLineWidth, dp2px(1.25f))
            mCircleRadius =
                it.getDimensionPixelSize(R.styleable.ColorScan_scanCircleRadius, dp2px(36f))
                    .toFloat()
            mInnerLineColor = it.getColor(
                R.styleable.ColorScan_scanInnerLineColor,
                Color.parseColor(INNER_LINE_COLOR)
            )
            mOuterLineColor =
                it.getColor(
                    R.styleable.ColorScan_scanOuterLineColor,
                    Color.parseColor(OUTER_LINE_COLOR)
                )
            mBottomColor =
                it.getColor(
                    R.styleable.ColorScan_scanBottomColor,
                    Color.parseColor(BOTTOM_COLOR)
                )
            mBottomLeftCircleColor =
                it.getColor(
                    R.styleable.ColorScan_scanBottomLeftCircleColor,
                    Color.parseColor(BOTTOM_LEFT_CIRCLE_COLOR)
                )
            mTopRightCircleColor =
                it.getColor(
                    R.styleable.ColorScan_scanTopRigthCircleColor,
                    Color.parseColor(TOP_RIGHT_CIRCLE_COLOR)
                )
            mFanShapedColor =
                it.getColor(
                    R.styleable.ColorScan_scanFanShapedColor,
                    Color.parseColor(FAN_SHAPED_COLOR)
                )
            mFanShapedWidth =
                it.getDimensionPixelSize(R.styleable.ColorScan_scanFanShapedWidth, dp2px(62f))
            mIndicatorColor =
                it.getColor(
                    R.styleable.ColorScan_scanIndicatorColor,
                    Color.parseColor(INDICATOR_COLOR)
                )
            mIndicatorLineWidth =
                it.getDimensionPixelSize(
                    R.styleable.ColorScan_scanIndicatorLineWidth,
                    dp2px(7f)
                )
            mInnerRadius = it.getDimensionPixelSize(
                R.styleable.ColorScan_scanInnerLineRadius,
                0
            ).toFloat()
            mOuterRadius = it.getDimensionPixelSize(
                R.styleable.ColorScan_scanOuterLineRadius,
                0
            ).toFloat()
            mIndicatorRadius = it.getDimensionPixelSize(
                R.styleable.ColorScan_scanIndicatorLineRadius,
                0
            ).toFloat()

            it.recycle()
        }

    }

    init {
        default_size = context.dp2px(240f)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getMeasureSize(widthMeasureSpec)
        val height = getMeasureSize(heightMeasureSpec)
        mSize = min(width, height)
        mRadius = mSize / 2f
        setMeasuredDimension(mSize, mSize)
        initSize()
        initPaint()
    }

    private fun getMeasureSize(measureSpec: Int): Int {
        val mode = MeasureSpec.getMode(measureSpec);
        val size = MeasureSpec.getSize(measureSpec);
        return if (mode == MeasureSpec.EXACTLY) size else default_size
    }

    /**
     * 计算各个部分大小
     */
    private fun initSize() {
        //
        mMaskerRadius = mCircleRadius * 2 / 3f
        if (mInnerRadius == 0f) {
            mInnerRadius = mRadius * 69 / 120f
        }
        if (mOuterRadius == 0f) {
            mOuterRadius = mRadius * 9 / 12f
        }
        if (mIndicatorRadius == 0f) {
            mIndicatorRadius = mRadius * 7 / 12f
        }
        mBlurCircleRelativeX = mRadius / 2f
        mBlurCircleRelativeY = mBlurCircleRelativeX
        mIndicatorRect.let {
            it.left = mRadius - mIndicatorRadius
            it.right = mRadius + mIndicatorRadius
            it.top = mRadius - mIndicatorRadius
            it.bottom = mRadius + mIndicatorRadius
        }
        mFanShapedRect.let {
            it.left = mRadius - mInnerRadius
            it.right = mRadius + mInnerRadius
            it.top = mRadius - mInnerRadius
            it.bottom = mRadius + mInnerRadius
        }
        mlog("mRadius: $mRadius")
        mlog("mOuterRadius: $mOuterRadius")
    }

    private fun initPaint() {
        blurPaint.maskFilter = BlurMaskFilter(mMaskerRadius, BlurMaskFilter.Blur.NORMAL)
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    override fun onDraw(canvas: Canvas) {
        // 画背景
        shapedPaint.color = mBottomColor
        canvas.drawCircle(mRadius, mRadius, mRadius, shapedPaint)

        // 画内圈
        linePaint.strokeWidth = mInnerLineWidth.toFloat()
        linePaint.color = mInnerLineColor
        canvas.drawCircle(mRadius, mRadius, mInnerRadius, linePaint)

        // 画外圈
        linePaint.strokeWidth = mOuterLineWidth.toFloat()
        linePaint.color = mOuterLineColor
        canvas.drawCircle(mRadius, mRadius, mOuterRadius, linePaint)

        // 画标识
        linePaint.strokeWidth = mIndicatorLineWidth.toFloat()
        linePaint.color = mIndicatorColor
        canvas.drawArc(mIndicatorRect, -10f + mIndicatorOffset, 124f, false, linePaint)

        // 画扇形
        linePaint.strokeWidth = mFanShapedWidth.toFloat()
        linePaint.color = mFanShapedColor
        canvas.drawArc(mFanShapedRect, 120f + mFanShapedOffset, 105f, false, linePaint)

        // 画左下圆形
        blurPaint.color = mBottomLeftCircleColor
        canvas.drawCircle(
            mRadius / 2,
            mRadius + mRadius / 2,
            mCircleRadius * mCircleScale,
            blurPaint
        )

        // 画左下圆形
        blurPaint.color = mTopRightCircleColor
        canvas.drawCircle(
            mRadius + mRadius / 2,
            mRadius / 2,
            mCircleRadius * mCircleScale,
            blurPaint
        )

        if (!isAnimatorStarted) {
            mIndicatorAnimator.start()
            mCircleScaleAnimator.start()
            mFanShapedAnimator.start()
            isAnimatorStarted = !isAnimatorStarted
        }
    }

    override fun onVisibilityChanged(changedView: View, visibility: Int) {
        super.onVisibilityChanged(changedView, visibility)
        if (visibility == View.VISIBLE) {
            if (mIndicatorAnimator.isPaused) {
                mIndicatorAnimator.resume()
                mCircleScaleAnimator.resume()
                mFanShapedAnimator.resume()
            }
        } else {
            if (isAnimatorStarted) {
                mIndicatorAnimator.pause()
                mCircleScaleAnimator.pause()
                mFanShapedAnimator.pause()
            }
        }
    }

    override fun onDetachedFromWindow() {
        mIndicatorAnimator.end()
        mCircleScaleAnimator.end()
        mFanShapedAnimator.end()
        super.onDetachedFromWindow()
    }

}