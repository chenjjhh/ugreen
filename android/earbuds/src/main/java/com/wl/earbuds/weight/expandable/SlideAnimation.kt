package com.wl.earbuds.weight.expandable

import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

/**
 * User: wanlang_dev
 * Data: 2022/11/5
 * Time: 13:48
 * Desc:
 */
class SlideAnimation(val view: View, private val fromHeight: Int, private val toHeight: Int) :
    Animation() {
    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
        if (view.height != toHeight) {
            view.layoutParams.height =
                (fromHeight + (toHeight - fromHeight) * interpolatedTime).toInt()
            view.requestLayout()
        }
    }
}