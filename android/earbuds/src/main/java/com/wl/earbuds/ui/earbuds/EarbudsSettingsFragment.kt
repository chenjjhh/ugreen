package com.wl.earbuds.ui.earbuds

import android.os.Bundle
import androidx.fragment.app.activityViewModels
import com.wl.db.model.UgDevice
import com.wl.earbuds.R
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.databinding.FragmentEarbudsSettingsBinding
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.updateTitle
import com.wl.earbuds.utils.setAnimateAlpha
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav

class EarbudsSettingsFragment : BaseFragment<BaseViewModel, FragmentEarbudsSettingsBinding>() {

    companion object {
        fun newInstance() = EarbudsSettingsFragment()
    }

    private var isTop = false

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        initTitle()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(CurrentDeviceManager.ugDevice!!.name) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeUgDeviceState(viewLifecycleOwner, ::onNameChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("$isOnline")
        /*if (isOnline) {
            setAnimateAlpha(mDatabind.clSelectVoice, EarbudsConstants.ALPHA_ON_VALUE, true)
        } else {
            setAnimateAlpha(mDatabind.clSelectVoice, EarbudsConstants.ALPHA_OFF_VALUE)
        }*/
        if (isTop) {
            checkOnlineStatus()
        }
    }

    override fun onResume() {
        super.onResume()
        isTop = true
        checkOnlineStatus()
    }

    override fun onPause() {
        super.onPause()
        mlog("onPause")
        isTop = false
    }

    private fun checkOnlineStatus() {
        if (!CurrentDeviceManager.isOnline()) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun onNameChange(ugDevice: UgDevice?) {
        mDatabind.toolbar.toolbar.updateTitle(ugDevice?.name ?: "")
    }

    inner class ProxyClick {
        fun goEarbudsName() {
            safeNav {
                nav().navigate(R.id.action_earbudsSettings_to_earbudsName)
            }
        }

        fun goEarbudsVoice() {
            if (!CurrentDeviceManager.isOnline()) return
            safeNav {
                nav().navigate(R.id.action_earbudsSettings_to_earbudsVoice)
            }
        }
    }
}