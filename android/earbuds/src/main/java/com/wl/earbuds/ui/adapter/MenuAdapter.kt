package com.wl.earbuds.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.MenuBean

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class MenuAdapter(data: MutableList<MenuBean>): BaseQuickAdapter<MenuBean, BaseViewHolder>(R.layout.item_menu, data) {
    override fun convert(holder: BaseViewHolder, item: MenuBean) {
        holder.setText(R.id.tv_menu, item.name)
    }
}