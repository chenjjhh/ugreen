package com.wl.earbuds.weight.card

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.wl.earbuds.R

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 16:36
 * Desc:
 */
class ProductCard : FrameLayout, Card {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.layout_product, this)
        updateUI()
    }

    override fun updateUI() {
    }

}