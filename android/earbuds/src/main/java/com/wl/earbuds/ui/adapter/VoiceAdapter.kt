package com.wl.earbuds.ui.adapter

import android.widget.RadioButton
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.GestureCommandBean
import com.wl.earbuds.data.model.entity.VoiceBean
import com.wl.earbuds.data.model.eumn.AncLevel
import com.wl.earbuds.data.model.eumn.Voice

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class VoiceAdapter(data: MutableList<VoiceBean>): BaseQuickAdapter<VoiceBean, BaseViewHolder>(R.layout.item_voice, data) {
    private var _currentVoice: Voice = Voice.CHINESE
    private var _lastIndex: Int = -1
    var currentVoice: Voice
        set(value) {
            val selectIndex = data.indexOfFirst { it.voice == value }
            if (selectIndex != -1) {
                _currentVoice = value
                notifyItemChanged(_lastIndex)
                notifyItemChanged(selectIndex)
                _lastIndex = selectIndex
            }
        }
        get() = _currentVoice
    override fun convert(holder: BaseViewHolder, item: VoiceBean) {
        holder.setText(R.id.tv_name, item.name)
        holder.getView<RadioButton>(R.id.radio_btn).isChecked =
            holder.adapterPosition == _lastIndex
    }
}