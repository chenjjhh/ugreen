package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 9:54
 * Desc: 位置状态
 */
data class PositionState(val position: Int = 0)