package com.wl.earbuds.app.settings

import com.dylanc.mmkv.MMKVOwner
import com.dylanc.mmkv.mmkvBool
import com.dylanc.mmkv.mmkvInt
import com.tencent.mmkv.MMKV

/**
 * User: wanlang_dev
 * Data: 2022/11/28
 * Time: 16:33
 * Desc:
 */
object ConfigUtils: MMKVOwner {
    var settingsEnable by mmkvBool(true)
    var deviceSettingsEnable by mmkvBool(true)
    var addDeviceEnable by mmkvBool(true)
    var disconnectEnable by mmkvBool(true)
    var deleteEnable by mmkvBool(true)
    var manualEnable by mmkvBool(true)
    var eqEnable by mmkvBool(true)
    var gameModeEnable by mmkvBool(true)
    var dualEnable by mmkvBool(true)
    var findEnable by mmkvBool(true)
    var hqEnable by mmkvBool(true)
    var upgradeEnable by mmkvBool(true)

    override val kv: MMKV
        get() = MMKV.mmkvWithID("config")

}