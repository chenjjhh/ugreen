package com.wl.earbuds.bluetooth.task;

import androidx.annotation.NonNull;

public class TaskManagerWrapper implements TaskManager {

    private final TaskManagerImpl mTaskManager;

    public TaskManagerWrapper() {
        mTaskManager = new TaskManagerImpl();
    }

    @Override
    public void runOnMain(@NonNull Runnable runnable) {
        mTaskManager.runOnMain(runnable);
    }

    @Override
    public void runInBackground(@NonNull Runnable runnable) {
        mTaskManager.runInBackground(runnable);
    }

    @Override
    public void schedule(@NonNull Runnable runnable, long delayMs) {
        mTaskManager.schedule(runnable, delayMs);
    }

    @Override
    public void cancelScheduled(@NonNull Runnable runnable) {
        mTaskManager.cancelScheduled(runnable);
    }

    public void release() {
        mTaskManager.release();
    }
}
