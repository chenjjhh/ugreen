package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 9:53
 * Desc: 开关状态
 */
data class SwitchState(val boolean: Boolean = false) {
}