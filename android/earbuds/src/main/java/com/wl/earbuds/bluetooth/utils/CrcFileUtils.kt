package com.wl.earbuds.bluetooth.utils

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.zip.CRC32

/**
 * 作者: wanlang_dev
 * 日期: 2022/8/30 10:36
 * 描述:
 */
fun getFileCrc32(file: File): Long {
    val crc32 = CRC32()
    var input: FileInputStream? = null
    var len = 0
    val buffer = ByteArray(1024)
    try {
        input = FileInputStream(file)
        while (input.read(buffer).also { len = it } != -1) {
            crc32.update(buffer, 0, len)
        }
        input?.close()
    } catch (e: IOException) {

    } finally {
        try {
            // 关闭文件流
            input?.close()
        } catch (_: IOException) {
        }
    }
    return crc32.value
}
