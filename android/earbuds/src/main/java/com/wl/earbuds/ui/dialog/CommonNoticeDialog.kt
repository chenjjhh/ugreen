package com.wl.earbuds.ui.dialog

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.wanlang.base.BaseDialog
import com.wl.earbuds.R

/**
 * User: wanlang_dev
 * Data: 2022/11/9
 * Time: 14:36
 * Desc:
 */
class CommonNoticeDialog {
    class Builder(context: Context): BaseDialog.Builder<Builder>(context) {
        private var _actionListener: ActionListener? = null
        private val mTitleView: TextView? by lazy { findViewById(R.id.tv_title) }
        private val mContentView: TextView? by lazy { findViewById(R.id.tv_content) }
        private val mCancelView: TextView? by lazy { findViewById(R.id.btn_cancel) }
        private val mConfirmView: TextView? by lazy { findViewById(R.id.btn_confirm) }

        init {
            setContentView(R.layout.dialog_common_notice)
            mCancelView?.setOnClickListener {
                _actionListener?.onCancel(getDialog())
            }
            mConfirmView?.setOnClickListener {
                _actionListener?.onConfirm(getDialog())
            }
        }

        fun setActionListener(listener: ActionListener?): Builder {
            _actionListener = listener
            return this
        }

        fun setTitle(title: String): Builder {
            mTitleView?.text = title
            return this
        }

        fun setContent(content: String): Builder {
            mContentView?.text = content
            return this
        }

        fun contentAlignStart(): Builder {
            mContentView?.gravity = Gravity.START
            return this
        }

        fun setConfirmText(confirm: String): Builder {
            mConfirmView?.text = confirm
            return this
        }

        fun setCancelText(cancelStr: String): Builder {
            mCancelView?.text = cancelStr
            return this
        }
    }

    interface ActionListener{
        fun onCancel(dialog: BaseDialog?) {

        }

        fun onConfirm(dialog: BaseDialog?) {

        }
    }
}