package com.wl.earbuds.bluetooth.connect

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 9:13
 * Desc:
 */
interface IDeviceManager {
    fun connect()
    fun disconnect()

}