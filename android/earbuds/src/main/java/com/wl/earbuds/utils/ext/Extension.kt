package com.wl.earbuds.utils.ext

import android.content.Context
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.annotation.CheckResult
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestOptions
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.R
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow

/**
 * User: wanlang_dev
 * Data: 2021/4/21
 * Time: 9:32
 */

var tag = "earbuds"

/** dp和px转换 **/
fun Context.dp2px(dpValue: Float): Int {
    return (TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, dpValue,
        resources.getDisplayMetrics() )+ 0.5f).toInt()
}

fun Context.px2dp(pxValue: Float): Int {
    return (pxValue / resources.displayMetrics.density + 0.5f).toInt()
}

fun Context.sp2px(spValue: Float): Int {
    return (TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_SP, spValue,
        resources.getDisplayMetrics()
    )+ 0.5f).toInt()
}

fun Context.px2sp(pxValue: Float): Int {
    return (pxValue / resources.displayMetrics.scaledDensity + 0.5f).toInt()
}

fun View.px2dp(pxValue: Float): Int {
    return context!!.px2dp(pxValue)
}

fun View.dp2px(dpValue: Float): Int {
    return context!!.dp2px(dpValue)
}

fun View.sp2px(dpValue: Float): Int {
    return context!!.sp2px(dpValue)
}

fun View.px2sp(pxValue: Float): Int {
    return context!!.px2sp(pxValue)
}

fun Any.mlog(any: Any) {
    if (BuildConfig.LOG_DEBUG) {
        var classname = ""
        if (this.javaClass.simpleName != null) {
            classname = this.javaClass.simpleName
        } else if (this.javaClass.superclass?.simpleName != null) {
            classname = "Supper class(${this.javaClass.superclass?.simpleName})"
        }
        if (classname.isNullOrBlank()) {
            Log.i(tag, "Null -> ${any as String}")
        } else {
            Log.i(tag, "$classname -> ${any as String}")
        }
    }
}

fun ImageView.image(path: String) {
    val options = RequestOptions()
        .error(R.color.white)
//        .placeholder(R.drawable.icon_avatar)
//        //异常占位图(当加载异常的时候出现的图片)
//        .error(R.drawable.icon_avatar)
        .transform(CenterCrop())
        //禁止Glide硬盘缓存缓存
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

    Glide.with(context)
        .load(path)
        .apply(options)
        .into(this)
}

fun ImageView.normalImage(path: String) {
    val options = RequestOptions()
        .placeholder(R.color.white)
        .error(R.color.white)
//        .placeholder(R.drawable.icon_avatar)
//        //异常占位图(当加载异常的时候出现的图片)
//        .error(R.drawable.icon_avatar)
        //禁止Glide硬盘缓存缓存
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

    Glide.with(context)
        .load(path)
        .apply(options)
        .into(this)
}

fun ImageView.image(path: Int?) {
    val options = RequestOptions()
        .placeholder(R.color.white)
//        //异常占位图(当加载异常的时候出现的图片)
        .error(R.color.white)
        .transform(CenterCrop())
        //禁止Glide硬盘缓存缓存
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

    Glide.with(context)
        .load(path!!)
        .apply(options)
        .into(this)

}

//点击事件防抖
@OptIn(ExperimentalCoroutinesApi::class)
@CheckResult
fun View.singleClick(duration:Long = 200) = callbackFlow<View> {
    var lastClick = 0L
    val listener = View.OnClickListener {
        if(System.currentTimeMillis() - lastClick > duration){
            kotlin.runCatching { trySend(it) }
            lastClick = System.currentTimeMillis()
        }
    }
    setOnClickListener(listener)
    awaitClose { setOnClickListener(null) }
}

/*@OptIn(ExperimentalCoroutinesApi::class)
fun View.singleClick(owner: LifecycleOwner, duration:Long = 600, observer: Observer<View>) =
    singleClick(duration)
        .asLiveData()
        .observe(owner,observer)*/
