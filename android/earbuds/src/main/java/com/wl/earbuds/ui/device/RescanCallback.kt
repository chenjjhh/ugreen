package com.wl.earbuds.ui.device

/**
 * User: wanlang_dev
 * Data: 2022/11/25
 * Time: 13:44
 * Desc:
 */
interface RescanCallback {
    fun onRescan()
}