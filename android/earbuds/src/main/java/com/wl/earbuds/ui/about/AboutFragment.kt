package com.wl.earbuds.ui.about

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import cn.wandersnail.bluetooth.BTManager
import com.gyf.immersionbar.ImmersionBar
import com.wanlang.base.BaseDialog
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.R
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.app.deviceManagerViewModel
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.ClassicManager
import com.wl.earbuds.bluetooth.connect.ConnectManager
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.connect.DeviceManager
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.bluetooth.earbuds.setRecovery
import com.wl.earbuds.data.model.entity.AppVersionResponse
import com.wl.earbuds.data.model.entity.BatteryState
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.databinding.FragmentAboutBinding
import com.wl.earbuds.ui.device.DeviceListActivity
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.ui.language.LanguageActivity
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.LanguageUtils.getLanguageDesc
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.postDelay
import com.wl.earbuds.utils.ext.showMsg
import com.wl.earbuds.utils.setAnimateAlpha
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.lifecycle.KtxActivityManger
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.visible

class AboutFragment : BaseFragment<AboutViewModel, FragmentAboutBinding>() {

    companion object {
        fun newInstance() = AboutFragment()
    }

    private var mRecoveryDialog: BaseDialog? = null

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initImmersionBar()
        initTitle()
        mlog("AboutFragment initView")
    }

    private fun initImmersionBar() {
        ImmersionBar.with(this)
            .transparentStatusBar()
            .navigationBarColor(com.wanlang.base.R.color.transparent)
            .fitsSystemWindows(true)
            .statusBarDarkFont(true)// 默认状态栏字体颜色为黑色
            //            .keyboardEnable(true)  // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(
            getString(com.wl.resource.R.string.tab_about),
            enableBack = false
        ) {
            startActivity(Intent(requireContext(), DeviceListActivity::class.java))
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeBatteryState(viewLifecycleOwner, ::onBatteryChange)
        mViewModel.appVersionState.observe(viewLifecycleOwner, ::onVersion)
//        mViewModel.getAppVersion()
    }

    override fun initData() {
        super.initData()
        mViewModel.languageStr.set(getLanguageDesc(requireContext()))
        mViewModel.versionStr.set(BuildConfig.versionName)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("isOnline: $isOnline")
        updateStatus()
    }

    private fun updateStatus() {
        val isOnline = CurrentDeviceManager.isOnline()
        val batteryState = CurrentDeviceManager.getBatteryState()
        val isEnable = isOnline && batteryState.isLeftOn() && batteryState.isRightOn()
        if (isEnable) {
            setAnimateAlpha(mDatabind.clRecovery, EarbudsConstants.ALPHA_ON_VALUE, true)
        } else {
            mRecoveryDialog?.dismiss()
            setAnimateAlpha(mDatabind.clRecovery, EarbudsConstants.ALPHA_OFF_VALUE)
        }
    }

    private fun onBatteryChange(batteryState: BatteryState) {
        updateStatus()
    }

    private fun onVersion(uiState: ResponseUiState<AppVersionResponse>) {
        if (uiState.isSuccess) {
            val data = uiState.data!!
            if (BuildConfig.versionCode < data.versionCode) {
                mViewModel.versionStr.set(
                    getString(
                        com.wl.resource.R.string.app_find_new_version,
                        BuildConfig.versionName
                    )
                )
                mDatabind.tvVersion.setTextColor(requireContext().getColor(com.wl.resource.R.color.standard_primary))
                mDatabind.upgradePoint.visible()
            }
        }
    }

    private fun realRecovery() {
        postDelay(500) {
            val ugDevice = CurrentDeviceManager.ugDevice
            ConnectManager.getInstance(requireContext())
                .disconnectDevice(CurrentDeviceManager.getAddress())
            CurrentDeviceManager.disconnect()
            appViewModel.deleteUgDevice(ugDevice!!)
            ClassicManager.removeDeviceManager(ugDevice!!.macAddress)
            CurrentDeviceManager.ugDevice = null
            CurrentDeviceManager.device = null
            if (KtxActivityManger.isLast) {
                startActivity(Intent(requireContext(), DeviceListActivity::class.java))
                activity?.finish()
            } else {
                requireActivity().onBackPressed()
            }
        }
    }

    inner class ProxyClick {
        fun goUserProtocol() {
            safeNav {
                nav().navigate(R.id.action_home_to_protocolWeb, Bundle().apply {
                    putInt(Constants.PROTOCOL_TYPE, Constants.PROTOCOL_USER)
                })
            }
        }

        fun goPrivacyPolice() {
            safeNav {
                nav().navigate(R.id.action_home_to_protocolWeb, Bundle().apply {
                    putInt(Constants.PROTOCOL_TYPE, Constants.PROTOCOL_PRIVACY)
                })
            }
        }

        fun recovery() {
            if (!CurrentDeviceManager.isOnline()) return
            val batteryState = CurrentDeviceManager.getBatteryState()
            if (!batteryState.isRightOn() || !batteryState.isLeftOn()) return
            mRecoveryDialog = CommonNoticeDialog.Builder(requireContext())
                .setTitle(getString(com.wl.resource.R.string.dialog_title_recovery))
                .setContent(getString(com.wl.resource.R.string.recovery_hint))
                .setActionListener(object : CommonNoticeDialog.ActionListener {
                    override fun onCancel(dialog: BaseDialog?) {
                        super.onCancel(dialog)
                        dialog?.dismiss()
                    }

                    override fun onConfirm(dialog: BaseDialog?) {
                        super.onConfirm(dialog)
                        setRecovery()
                        realRecovery()
                        dialog?.dismiss()
                    }
                }).create()
            mRecoveryDialog?.show()
        }

        fun goLanguage() {
            startActivity(Intent(requireContext(), LanguageActivity::class.java))
        }

        fun goAppVersion() {
            // 当前已是最新版本,给出提示
            val appVersionResponse = mViewModel.appVersionState.value
            if (appVersionResponse != null && appVersionResponse.isSuccess &&
                BuildConfig.versionCode >= appVersionResponse.data!!.versionCode
            ) {
                showMsg(getString(com.wl.resource.R.string.hint_current_verion_is_latest))
                return
            }
            safeNav {
                nav().navigate(R.id.action_home_to_appVersion)
            }
        }
    }
}