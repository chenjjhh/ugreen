package com.wl.earbuds.ui.device

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import cn.wandersnail.bluetooth.BTManager
import com.wl.db.model.UgDevice
import com.wl.earbuds.R
import com.wl.earbuds.app.deviceManagerViewModel
import com.wl.earbuds.base.BaseActivity
import com.wl.earbuds.bluetooth.connect.*
import com.wl.earbuds.data.model.entity.SavedDevice
import com.wl.earbuds.databinding.ActivityDeviceScanBinding
import com.wl.earbuds.ui.home.HomeActivity
import com.wl.earbuds.utils.ext.mlog
import java.util.*

class DeviceScanActivity : BaseActivity<DeviceScanViewModel, ActivityDeviceScanBinding>(),
    ConnectListener, RescanCallback, ConnectCallback {

    private var mCurrentFragment: Fragment? = null
    private var mDeviceList: MutableList<BluetoothDevice> = mutableListOf()

    private val mConnectManager by lazy {
        ConnectManager.getInstance(application).apply {
            addConnectListener(this@DeviceScanActivity)
        }
    }

    private val scanFragment: SeachingDeviceFragment by lazy {
        SeachingDeviceFragment.newInstance()
    }
    val noBluetoothFragment: NoBluetoothFragment by lazy {
        NoBluetoothFragment.newInstance().apply {
            setRescanCallback(this@DeviceScanActivity)
        }
    }
    private val selectDeviceFragment: SelectDeviceFragment by lazy {
        SelectDeviceFragment.newInstance()
    }
    private val noDeviceFoundFragment: NoDeviceFoundFragment by lazy {
        NoDeviceFoundFragment.newInstance().apply {
            setRescanCallback(this@DeviceScanActivity)
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        deviceManagerViewModel.getDeviceList()
        if (BTManager.getInstance().isBluetoothOn) {
            replaceFragment(scanFragment)
            startScanDevice()
        } else {
            replaceFragment(noBluetoothFragment)
        }
    }

    override fun createObserver() {
        super.createObserver()
        deviceManagerViewModel.ugDeviceLiveData.observe(this, ::onUgDeviceSave)
    }

    private fun onUgDeviceSave(savedDevice: SavedDevice) {
        mlog("onUgDeviceSave ${savedDevice.device}")
        // if (savedDevice.isAdd) {
        mlog("ClassicManager.connect")
        ClassicManager.connect(savedDevice.device)
        // }
    }

    private fun startScanDevice() {
        mConnectManager.scanDevice()
    }

    private fun replaceFragment(fragment: Fragment) {
        if (fragment == mCurrentFragment) return
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(R.id.container, fragment)
        beginTransaction.commitAllowingStateLoss()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (BTManager.getInstance().isDiscovering) {
            BTManager.getInstance().stopDiscovery()
        }
        mConnectManager.removeConnectListener(this)
    }

    override fun onDeviceListUpdate(devices: MutableList<BluetoothDevice>) {
        mlog("onDeviceListUpdate ${devices.size}")
        if (devices.isNotEmpty()) {
            mDeviceList = devices
            replaceFragment(selectDeviceFragment)
            selectDeviceFragment.updateDevice(devices)
        }
    }

    override fun onDeviceFound(device: BluetoothDevice) {
        super.onDeviceFound(device)
        if (!mDeviceList.contains(device)) return
        BTManager.getInstance().createBond(device)
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        if (mConnectManager.getTargetDevice() != device) return
        if (!mDeviceList.contains(device)) return
        CurrentDeviceManager.device = device
        CurrentDeviceManager.ugDevice = deviceManagerViewModel.getUgdeviceByAddress(device.address)
        startActivity(Intent(this@DeviceScanActivity, HomeActivity::class.java))
        finish()
    }

    @SuppressLint("MissingPermission")
    override fun onDeviceBond(device: BluetoothDevice) {
        if (!mDeviceList.contains(device)) return
    }

    override fun onHeadsetConnected(device: BluetoothDevice) {
        mlog("onHeadsetConnected $device")
        mlog("onHeadsetConnected $mDeviceList")
        if (!mDeviceList.contains(device)) {
            return
        }
        // 不是目标设备，不处理
        if (!mConnectManager.checkIsTargetDevice(device)) return
        deviceManagerViewModel.saveIfNewDevice(device)
    }

    override fun onDiscoveryFinished(devices: MutableList<BluetoothDevice>) {
        mlog("onDiscoveryFinished")
        if (devices.isEmpty()) {
            replaceFragment(noDeviceFoundFragment)
        }
    }

    override fun onDiscoveryStart() {
        if (mDeviceList.isNotEmpty()) return
        replaceFragment(scanFragment)
    }

    override fun onRescan() {
        replaceFragment(scanFragment)
        startScanDevice()
    }

    override fun connectDevice(device: BluetoothDevice) {

    }

}