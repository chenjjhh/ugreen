package com.wl.earbuds.viewmodel

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.bluetooth.BluetoothDevice
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.db.UgDatabaseUtil
import com.wl.db.model.UgDevice
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.data.LanguageUpdate
import com.wl.earbuds.data.model.entity.AppVersionResponse
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.RxIntentTool
import com.wl.earbuds.utils.ext.mlog
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.util.notificationManager
import zlc.season.downloadx.State
import zlc.season.downloadx.core.DownloadTask
import zlc.season.downloadx.download
import java.io.File

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 10:24
 * Desc:
 */
class AppViewModel : BaseViewModel() {
    // 设备双连
    var mLastSwitchTime: Long = 0L
    var mFreezeInterval: Long = 5000L

    // 升级弹窗
    var mIgnoreUpgrade = false

    // app下载
    var mNotificationBuilder: NotificationCompat.Builder? = null
    var mNotification: Notification? = null

    val languageState: UnPeekLiveData<LanguageUpdate> = UnPeekLiveData()
    val earbudsUpgradeState: UnPeekLiveData<Boolean> = UnPeekLiveData()
    val openDeviceState: UnPeekLiveData<Boolean> = UnPeekLiveData()

    // 下载
    var downloadTask: DownloadTask? = null
    var downloadState = UnPeekLiveData<State>()

    fun updateLanguage(languageUpdate: LanguageUpdate) {
        languageState.value = languageUpdate
    }

    fun observeLanguage(owner: LifecycleOwner, observer: Observer<LanguageUpdate>) {
        languageState.observe(owner, observer)
    }

    fun startDownload(appVersion: AppVersionResponse) {
        mlog("startDownload")
        if (downloadTask != null && downloadState.value !is State.Failed) {
            if (downloadState.value is State.Succeed) {
                val file = appViewModel.downloadTask!!.file()
                if (file?.exists() == true) {
                    downloadFinish(file)
                } else {
                    downloadTask = null
                }
            }
            return
        }
        if (downloadTask != null && downloadTask!!.param.url == appVersion.fileUrl) {
            downloadTask?.start()
            return
        } else {
            downloadTask?.stop()
        }
        downloadTask = viewModelScope.download(appVersion.fileUrl).apply {
            state().onEach {
                downloadState.value = it
                onDownloadState(it)
            }.launchIn(viewModelScope)
            start()
        }
        mlog("downloadTask: ${appViewModel.downloadTask}")
    }

    fun deleteUgDevice(device: UgDevice) {
        viewModelScope.launch {
            UgDatabaseUtil.ugDeviceDao?.deleteUgDevice(device.id)
        }
    }

    fun deleteAllDevice() {
        viewModelScope.launch {
            UgDatabaseUtil.ugDeviceDao?.deleteAll()
        }
    }

    private fun onDownloadState(state: State) {
        when (state) {
            is State.Downloading -> updateProgress(state)
            is State.Failed -> {
                mlog("download Failed")
                removeNotification()
            }
            is State.None -> {}
            is State.Stopped -> {
                mlog("download Stopped")
            }
            is State.Succeed -> {
                mlog("downloadTask: ${appViewModel.downloadTask}")
                downloadFinish(appViewModel.downloadTask!!.file()!!)
            }
            is State.Waiting -> {
                mlog("download Waiting")
            }
        }
    }

    private fun initNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            appContext.notificationManager?.createNotificationChannel(
                NotificationChannel(
                    Constants.NOTIFICATION_CHANNEL_ID,
                    "notify",
                    NotificationManager.IMPORTANCE_LOW
                )
            )
        }
        mNotificationBuilder =
            NotificationCompat.Builder(appContext, Constants.NOTIFICATION_CHANNEL_ID)
                .setContentTitle(appContext.getString(com.wl.resource.R.string.downloading))
                .setSmallIcon(com.wl.resource.R.mipmap.ic_launcher)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        appContext.resources,
                        com.wl.resource.R.mipmap.ic_icon_app
                    )
                )
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(false)
                .setChannelId(Constants.NOTIFICATION_CHANNEL_ID)
                .setContentText(appContext.getString(com.wl.resource.R.string.download_progress, 0))
                .setProgress(100, 0, false)
    }

    private fun updateProgress(state: State) {
        if (mNotificationBuilder == null ||
            appContext.notificationManager?.getNotificationChannel(Constants.NOTIFICATION_CHANNEL_ID) == null) {
            initNotification()
        }
        mNotificationBuilder?.also {
            it.setProgress(100, state.progress.percent().toInt(), false)
                .setContentText(
                    appContext.getString(
                        com.wl.resource.R.string.download_progress,
                        state.progress.percent().toInt()
                    )
                )
            mNotification = it.build()
            appContext.notificationManager?.notify(1, mNotification!!)
        }
    }

    private fun downloadFinish(file: File) {
        /*mNotificationBuilder?.run {
            mNotification = setContentTitle(getString(com.wl.resource.R.string.download_finish))
                .setContentText(getString(com.wl.resource.R.string.click_install))
                .setAutoCancel(true)
                .setContentIntent(
                    PendingIntent.getActivity(
                        requireContext(),
                        0,
                        RxIntentTool.getInstallAppIntent(
                            appContext,
                            file.path
                        ),
                        0
                    )
                )
                .build()
            val manager = NotificationManagerCompat.from(appContext)
            manager.notify(1, mNotification!!)
        }*/
        removeNotification()
        appContext.startActivity(
            RxIntentTool.getInstallAppIntent(
                appContext,
                file.path
            )
        )
    }

    private fun removeNotification() {
        val manager = NotificationManagerCompat.from(appContext)
        manager.deleteNotificationChannel(Constants.NOTIFICATION_CHANNEL_ID)
    }

}