package com.wl.earbuds.utils

import android.content.Context
import com.wl.earbuds.R
import com.wl.earbuds.app.settings.ConfigUtils
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.data.model.entity.*
import com.wl.earbuds.data.model.eumn.*

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:45
 * Desc:
 */

fun generateLanguageData(context: Context): MutableList<LanguageBean> {
    return mutableListOf(
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_CHINESE,
            context.getString(com.wl.resource.R.string.language_sc_local),
            context.getString(com.wl.resource.R.string.language_sc_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_ENGLISH,
            context.getString(com.wl.resource.R.string.language_en_local),
            context.getString(com.wl.resource.R.string.language_en_translate),
        ),
        /*LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_FRENCH,
            context.getString(com.wl.resource.R.string.language_fr_local),
            context.getString(com.wl.resource.R.string.language_fr_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_GERMAN,
            context.getString(com.wl.resource.R.string.language_de_local),
            context.getString(com.wl.resource.R.string.language_de_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_RUSSIAN,
            context.getString(com.wl.resource.R.string.language_ru_local),
            context.getString(com.wl.resource.R.string.language_ru_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_JAPANESE,
            context.getString(com.wl.resource.R.string.language_ja_local),
            context.getString(com.wl.resource.R.string.language_ja_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_SPANISH,
            context.getString(com.wl.resource.R.string.language_es_local),
            context.getString(com.wl.resource.R.string.language_es_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_ITALIAN,
            context.getString(com.wl.resource.R.string.language_it_local),
            context.getString(com.wl.resource.R.string.language_it_translate),
        ),
        LanguageBean(
            LanguageUtils.LANGUAGE_TYPE_ARABIC,
            context.getString(com.wl.resource.R.string.language_ar_local),
            context.getString(com.wl.resource.R.string.language_ar_translate),
        )*/
    )
}

fun generateEqulizerData(context: Context): MutableList<EqualizerBean> {
    return mutableListOf(
        EqualizerBean(
            Equalizer.CLASSIC,
            context.getString(com.wl.resource.R.string.equalizer_classic)
        ),
        EqualizerBean(Equalizer.BASS, context.getString(com.wl.resource.R.string.equalizer_bass)),
        EqualizerBean(Equalizer.VOCAL, context.getString(com.wl.resource.R.string.equalizer_vocal)),
    )
}

fun generateLeftGestureData(
    context: Context,
    gestureState: GestureState
): MutableList<GestureBean> {
    return mutableListOf(
        GestureBean(
            context.getString(
                com.wl.resource.R.string.left_single_click
            ), getGestureCommandText(context, gestureState.leftSingle), Operation.LEFT_SINGLE
        ),
        GestureBean(
            context.getString(
                com.wl.resource.R.string.left_double_click
            ), getGestureCommandText(context, gestureState.leftDouble), Operation.LEFT_DOUBLE
        ),
        GestureBean(
            context.getString(
                com.wl.resource.R.string.left_triple_click
            ), getGestureCommandText(context, gestureState.leftTriple), Operation.LEFT_TRIPLE
        ),
        GestureBean(
            context.getString(
                com.wl.resource.R.string.left_long_press
            ), getGestureCommandText(context, gestureState.leftLong), Operation.LEFT_LONG
        ),
    )
}

fun generateRightGestureData(
    context: Context,
    gestureState: GestureState
): MutableList<GestureBean> {
    return mutableListOf(
        GestureBean(
            context.getString(
                com.wl.resource.R.string.right_single_click
            ), getGestureCommandText(context, gestureState.rightSingle), Operation.RIGHT_SINGLE
        ),
        GestureBean(
            context.getString(
                com.wl.resource.R.string.right_double_click
            ), getGestureCommandText(context, gestureState.rightDouble), Operation.RIGHT_DOUBLE
        ),
        GestureBean(
            context.getString(
                com.wl.resource.R.string.right_triple_click
            ), getGestureCommandText(context, gestureState.rightTriple), Operation.RIGHT_TRIPLE
        ),
        GestureBean(
            context.getString(
                com.wl.resource.R.string.right_long_press
            ),
            getGestureCommandText(context, gestureState.rightLong), Operation.RIGHT_LONG
        )
    )
}

fun getGestureCommandText(context: Context, gesture: Gesture) =
    when (gesture) {
        Gesture.NONE -> context.getString(
            com.wl.resource.R.string.command_none
        )
        Gesture.PLAY_PAUSE -> context.getString(
            com.wl.resource.R.string.command_play
        )
        Gesture.VOLUME_UP -> context.getString(
            com.wl.resource.R.string.command_volume_up
        )
        Gesture.VOLUME_DOWN -> context.getString(
            com.wl.resource.R.string.command_volume_down
        )
        Gesture.NEXT -> context.getString(
            com.wl.resource.R.string.command_next
        )
        Gesture.PREVIOUS -> context.getString(
            com.wl.resource.R.string.command_previous
        )
        Gesture.VA -> context.getString(
            com.wl.resource.R.string.command_voice_assistant
        )
        Gesture.GAME_MODE -> context.getString(
            com.wl.resource.R.string.command_game_mode
        )
        Gesture.ANC -> context.getString(
            com.wl.resource.R.string.command_anc
        )
        Gesture.CALL -> context.getString(
            com.wl.resource.R.string.command_call
        )
    }

fun generateSingleClickCommandData(context: Context): MutableList<GestureCommandBean> {
    return mutableListOf(
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_none
            ), Gesture.NONE
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_play
            ), Gesture.PLAY_PAUSE
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_volume_up
            ), Gesture.VOLUME_UP
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_volume_down
            ), Gesture.VOLUME_DOWN
        ),
    )
}

fun generateDoubleClickCommandData(context: Context): MutableList<GestureCommandBean> {
    return mutableListOf(
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_none
            ), Gesture.NONE
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_play
            ), Gesture.PLAY_PAUSE
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_previous
            ), Gesture.PREVIOUS
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_next
            ), Gesture.NEXT
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_call
            ), Gesture.CALL
        ),
    )
}


fun generateTripleClickCommandData(context: Context): MutableList<GestureCommandBean> {
    return mutableListOf(
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_none
            ), Gesture.NONE
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_voice_assistant
            ), Gesture.VA
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_game_mode
            ), Gesture.GAME_MODE
        ),
    )
}

fun generateLongPressCommandData(context: Context): MutableList<GestureCommandBean> {
    return mutableListOf(
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_none
            ), Gesture.NONE
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_voice_assistant
            ), Gesture.VA
        ),
        GestureCommandBean(
            context.getString(
                com.wl.resource.R.string.command_anc
            ),
            Gesture.ANC,
            context.getString(
                com.wl.resource.R.string.command_anc_hint
            ),
        ),
    )
}

fun generateMenuData(context: Context): MutableList<MenuBean> {
    return mutableListOf<MenuBean>().apply {
        if (ConfigUtils.deviceSettingsEnable)
            add(
                MenuBean(
                    Constants.MENU_DEVICE_SETTINGS,
                    context.getString(com.wl.resource.R.string.device_settings),
                )
            )
        if (ConfigUtils.addDeviceEnable)
            add(
                MenuBean(
                    Constants.MENU_ADD_DEVICE,
                    context.getString(com.wl.resource.R.string.add_device),
                )
            )
        if (ConfigUtils.disconnectEnable)
            add(
                MenuBean(
                    Constants.MENU_DISCONNECT,
                    context.getString(com.wl.resource.R.string.disconnect_device),
                )
            )
        if (ConfigUtils.deleteEnable)
            add(
                MenuBean(
                    Constants.MENU_DELETE_DEVICE,
                    context.getString(com.wl.resource.R.string.delete_device),
                )
            )
        if (ConfigUtils.manualEnable)
            add(
                MenuBean(
                    Constants.MENU_MANUAL,
                    context.getString(com.wl.resource.R.string.product_manuals),
                )
            )
    }
}

fun generateVoiceData(context: Context): MutableList<VoiceBean> {
    return mutableListOf(
        VoiceBean(
            context.getString(
                com.wl.resource.R.string.voice_chinese
            ), Voice.CHINESE
        ),
        VoiceBean(
            context.getString(
                com.wl.resource.R.string.voice_english
            ), Voice.ENGLISH
        ),
        VoiceBean(
            context.getString(
                com.wl.resource.R.string.voice_ring
            ), Voice.RING
        ),
    )
}

fun generateAncData(context: Context): MutableList<AncBean> {
    return mutableListOf(
        AncBean(
            context.getString(
                com.wl.resource.R.string.anc_light
            ), context.getString(
                com.wl.resource.R.string.anc_light_hint
            ), AncLevel.ANC_LIGHT
        ),
        AncBean(
            context.getString(
                com.wl.resource.R.string.anc_medium
            ), context.getString(
                com.wl.resource.R.string.anc_medium_hint
            ), AncLevel.ANC_MEDIUM
        ),
        AncBean(
            context.getString(
                com.wl.resource.R.string.anc_deep
            ), context.getString(
                com.wl.resource.R.string.anc_deep_hint
            ), AncLevel.ANC_DEEP
        ),
    )
}

fun generateCodecText(context: Context, codec: Codec): String {
    return when (codec) {
        Codec.SBC -> context.getString(com.wl.resource.R.string.codec_sbc)
        Codec.AAC -> context.getString(com.wl.resource.R.string.codec_aac)
        Codec.LDAC -> context.getString(com.wl.resource.R.string.codec_ldac)
    }
}