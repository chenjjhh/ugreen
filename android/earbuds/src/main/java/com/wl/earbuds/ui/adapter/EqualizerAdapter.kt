package com.wl.earbuds.ui.adapter

import android.widget.RadioButton
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.EqualizerBean
import com.wl.earbuds.data.model.eumn.Equalizer

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class EqualizerAdapter(data: MutableList<EqualizerBean>) :
    BaseQuickAdapter<EqualizerBean, BaseViewHolder>(R.layout.item_equlizer, data) {

    private var _currentEq: Equalizer? = null
    private var _lastIndex: Int = -1
    var currentEq: Equalizer?
        set(value) {
            val selectIndex = data.indexOfFirst { it.equalizer == value }
            if (selectIndex != -1) {
                _currentEq = value
                notifyItemChanged(_lastIndex)
                notifyItemChanged(selectIndex)
                _lastIndex = selectIndex
            }
        }
        get() = _currentEq

    override fun convert(holder: BaseViewHolder, item: EqualizerBean) {
        holder.setText(R.id.tv_name, item.name)
        holder.getView<RadioButton>(R.id.radio_btn).isChecked =
            holder.adapterPosition == _lastIndex
    }
}