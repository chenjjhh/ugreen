package com.wl.earbuds.weight.decoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * 作者: wanlang_dev
 * 日期: 2022-11-4 15:56:25
 * 描述:
 */
class SpaceItemDecoration(
    val left: Int = 0,
    val top: Int = 0,
    val right: Int = 0,
    val bottom: Int = 0
) :
    RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        // if (parent.getChildAdapterPosition(view) < parent.adapter!!.itemCount -1) {
        outRect.left = left
        outRect.top = top
        outRect.right = right
        outRect.bottom = bottom
        // }
    }
}