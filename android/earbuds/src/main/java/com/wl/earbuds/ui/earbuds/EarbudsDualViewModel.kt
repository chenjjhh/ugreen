package com.wl.earbuds.ui.earbuds

import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.BooleanObservableField

class EarbudsDualViewModel : BaseViewModel() {
    var isDual = BooleanObservableField(CurrentDeviceManager.getDualState().boolean)
}