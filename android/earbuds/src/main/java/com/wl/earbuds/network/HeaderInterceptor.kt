package com.wl.earbuds.network

import com.wl.earbuds.app.settings.SettingsUtils
import okhttp3.FormBody
import okhttp3.Interceptor
import okhttp3.RequestBody
import okhttp3.Response

/**
 * Created by wanlang_dev on 2022/2/9 16:37
 */
class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val urlString = request.url.toString()
        if (!urlString.startsWith(ApiService.REMOTE_HOST)) {
            return chain.proceed(request)
        }
        val header = request.headers.newBuilder().add("platform", "android").build()
        val requestBody = request.body
        if (requestBody is FormBody) {
            val formBody = FormBody.Builder().apply {
                for (index in 0 until requestBody.size) {
                    add(requestBody.name(index), requestBody.value(index))
                }
                add("language", SettingsUtils.language)
            }.build()
            request = request.newBuilder().post(formBody).headers(header).build()
        } else {
            val httpUrl = request.url.newBuilder()
                .addQueryParameter("language", SettingsUtils.language)
                .build()
            request = request.newBuilder().url(httpUrl).headers(header).build()
        }
        return chain.proceed(request)
    }
}