package com.wl.earbuds.bluetooth.ota


/**
 * User: wanlang_dev
 * Data: 2022/11/29
 * Time: 9:42
 * Desc:
 */
interface OtaListener {
    fun onStartUpgrade()
    fun sendOtaPacket(data: ByteArray)
    fun updateProgress(progress: Int)
    fun onFail()
    fun onSuccess()
}