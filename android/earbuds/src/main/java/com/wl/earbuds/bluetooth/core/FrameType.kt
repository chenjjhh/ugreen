package com.wl.earbuds.bluetooth.core

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 19:39
 * Desc:
 */
enum class FrameType {
    COMMAND,
    RESPONSE,
    NOTIFY,
    OTA;
}