package com.wl.earbuds.utils;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * User: wanlang_dev
 * Data: 2022/11/24
 * Time: 14:18
 * Desc:
 */
public class InvorkUtils {
    public static Boolean connectA2dp(BluetoothA2dp bluetoothA2dp, BluetoothDevice device) {
        Boolean result = false;
        try {
            Method method = BluetoothA2dp.class.getMethod("connect", new Class[]{BluetoothDevice.class});
            result = (Boolean) method.invoke(bluetoothA2dp, device);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Boolean disconnectA2dp(BluetoothA2dp bluetoothA2dp, BluetoothDevice device) {
        Boolean result = false;
        try {
            Method method = BluetoothA2dp.class.getMethod("disconnect", new Class[]{BluetoothDevice.class});
            result = (Boolean) method.invoke(bluetoothA2dp, device);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Boolean connectHeadset(BluetoothHeadset bluetoothHeadset, BluetoothDevice device) {
        Boolean result = false;
        try {
            Method method = BluetoothHeadset.class.getMethod("connect", new Class[]{BluetoothDevice.class});
            result = (Boolean) method.invoke(bluetoothHeadset, device);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Boolean disconnectHeadset(BluetoothHeadset bluetoothHeadset, BluetoothDevice device) {
        Boolean result = false;
        try {
            Method method = BluetoothHeadset.class.getMethod("disconnect", new Class[]{BluetoothDevice.class});
            result = (Boolean) method.invoke(bluetoothHeadset, device);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getBluetoothAddress(BluetoothAdapter bluetoothAdapter) {
        Field field = null;
        try {
            field = BluetoothAdapter.class.getDeclaredField("mService");
            field.setAccessible(true);
            Object bluetoothManagerService = field.get(bluetoothAdapter);
            if (bluetoothManagerService == null) {
                return null;
            }
            Method method = bluetoothManagerService.getClass().getMethod("getAddress");

            if (method != null) {
                Object obj = method.invoke(bluetoothManagerService);
                if (obj != null) {
                    return obj.toString().replace(":", "");
                }
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
