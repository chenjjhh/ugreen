package com.wl.earbuds.weight

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.wl.earbuds.R
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.mlog
import java.security.InvalidParameterException
import kotlin.math.roundToInt

/**
 * User: wanlang_dev
 * Data: 2022/11/1
 * Time: 18:05
 * Desc:
 */
class BatteryView : View {

    private var outerStartX: Float = 0f
    private var outerStartY: Float = 0f
    private var outerEndX: Float = 0f
    private var outerEndY: Float = 0f
    private var startX: Float = 0f
    private var startY: Float = 0f
    private var endX: Float = 0f
    private var endY: Float = 0f
    // 外层圆角
    private var outerRadiu: Float = 0f
    // 内层圆角
    private var innerRadiu: Float = 2f
    private var mHeaderRect: RectF = RectF()
    private var mLightingRect: RectF = RectF()
    private var lightningId: Int = 0
    private var lightningWidth: Int = 0
    private var lightningHeight: Int = 0
    private var green = Color.parseColor("#FF36CF69")
    private var red = Color.parseColor("#FFFF4040")
    private var yellow = Color.parseColor("#FFFFEB3B")
    private var currentBattery: Int = 5
    private var _isCharge: Boolean = false
    private lateinit var lightningBitmap: Bitmap
    var isCharge: Boolean
        get() = _isCharge
        set(value) {
            if (value != _isCharge) {
                _isCharge = value
                invalidate()
            }
        }
    var warming = 50
        set(value) {
            field = value
            invalidate()
        }

    var urgent = 20
        set(value) {
            field = value
            invalidate()
        }

    private val headerPaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.color = Color.parseColor("#A6A6A6")
            it.style = Paint.Style.FILL
        }
    }

    private val corePaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.color = Color.parseColor("#32D74B")
            it.style = Paint.Style.FILL
        }
    }

    private val outerPaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.strokeWidth = context.dp2px(1f).toFloat()
            it.color = Color.parseColor("#A6A6A6")
            it.style = Paint.Style.STROKE
        }
    }

    private val lightingPaint by lazy {
        Paint().also {
            it.isAntiAlias = true
            it.isDither = true
        }
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {

    }

    init {
        outerRadiu = context.dp2px(3f).toFloat()
        innerRadiu = context.dp2px(2f).toFloat()
        lightningId = R.mipmap.lightning
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val mWidth = dp2px(26.5f)
        val mHeight = dp2px(15.6f)
        setMeasuredDimension(mWidth, mHeight)
        lightningWidth = dp2px(8f)
        lightningHeight = mHeight
        // 计算头部尺寸
        val headerHeight = dp2px(5f)
        mHeaderRect.let {
            it.left = (mWidth - dp2px(1.5f)).toFloat()
            it.top = (mHeight - headerHeight) / 2f
            it.right = mWidth.toFloat()
            it.bottom = (mHeight + headerHeight) / 2f
        }
        // 复用值
        val temp = dp2px(2.5f).toFloat()
        // 计算外层
        outerStartX = dp2px(.5f).toFloat()
        outerStartY = temp
        outerEndX = mHeaderRect.left - dp2px(1.5f)
        outerEndY = mHeight - outerStartY
        // 计算内层
        startX = dp2px(2f).toFloat()
        startY = dp2px(4f).toFloat()
        endX = outerEndX - (startX - outerStartX)
        endY = mHeight - startY
        mLightingRect.let {
            it.left = outerStartX + (outerEndX - outerStartX - lightningWidth) / 2
            it.top = 0f
            it.right = it.left + lightningWidth
            it.bottom = mHeight.toFloat()
        }
        // 加载图片
        lightningBitmap = resizeBitmap(getScaleBitmap(), mLightingRect.width().toInt())
        mlog("outerStartX: $outerStartX,  outerEndX: $outerEndX")
        mlog("startX: $startX,  endX: $endX")
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        corePaint.color =
            if (currentBattery > warming) {
                green
            }  else if (currentBattery > urgent) {
                yellow
            } else {
                red
            }
        canvas.drawRoundRect(
            outerStartX,
            outerStartY,
            outerEndX,
            outerEndY,
            outerRadiu,
            outerRadiu,
            outerPaint
        )
        // 画头部
        canvas.drawRoundRect(
            mHeaderRect, mHeaderRect.width() / 2, mHeaderRect.width() / 2, headerPaint
        )
        canvas.drawRoundRect(
            startX,
            startY,
            (startX + (endX - startX) * currentBattery / 100),
            endY,
            innerRadiu,
            innerRadiu,
            corePaint
        )
        if (_isCharge) {
            canvas.drawBitmap(lightningBitmap, mLightingRect.left, mLightingRect.top, lightingPaint)
        }
    }

    fun updateBattery(battery: Int) {
        if (battery < 0 || battery > 100) return
        if (currentBattery != battery) {
            currentBattery = battery
            invalidate()
        }
    }

    fun isSafeState() = currentBattery > warming

    /**
     * 获取缩放后的Bitmap
     *
     * @return
     */
    private fun getScaleBitmap(): Bitmap {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        options.inPreferredConfig = Bitmap.Config.RGB_565
        BitmapFactory.decodeResource(resources, lightningId, options);
        options.inSampleSize = calcSampleSize(options, lightningWidth, lightningHeight)
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(resources, lightningId, options);
    }

    /**
     * 调整bitmap尺寸
     * @param bitmap
     * @param targetWidth
     */
    private fun resizeBitmap(bitmap: Bitmap, targetWidth: Int): Bitmap {
        val matrix = Matrix()
        val scale = (targetWidth / bitmap.width.toFloat()).roundToInt()
        if (scale == 1) return bitmap
        matrix.postScale(scale.toFloat(), scale.toFloat())
        return Bitmap.createBitmap(bitmap,0,0,bitmap.width,bitmap.height,matrix,true);
    }

    /**
     * 计算缩放比例
     *
     * @param option
     * @param width
     * @param height
     * @return
     */
    private fun calcSampleSize(option: BitmapFactory.Options, width: Int, height: Int): Int{
        var originWidth = option.outWidth
        var originHeight = option.outHeight
        var sampleSize = 1
        val maxWidth = 1.1 * width
        val maxHeight = 1.1 * height
        while (originWidth > maxWidth && originHeight > maxHeight) {
            originWidth = originWidth shr 1
            originHeight = originHeight shr 1
            sampleSize = sampleSize shl  1;
        }
        return sampleSize;
    }
}