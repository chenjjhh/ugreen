package com.wl.earbuds.weight

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.webkit.PermissionRequest
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ProgressBar
import com.wl.earbuds.R

/**
 * User: wanlang_dev
 * Data: 2022/11/2
 * Time: 18:01
 * Desc: web
 */
class CommonWebView : WebView {
    private var progressBar: ProgressBar? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    init {
        progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal).also {
            it.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 3, 0, 0)
            it.progressDrawable =context.getDrawable(R.drawable.layer_list_web_progress_bar)
        }
        addView(progressBar)
        webChromeClient = DefaultWebChromeClient()
        //默认配置
        initDefaultSetting()
    }

    private fun initDefaultSetting() {
        val webSettings = settings
        webSettings.useWideViewPort = true //可任意比例缩放
        webSettings.loadWithOverviewMode = true
        webSettings.builtInZoomControls = true //显示缩放按钮
        webSettings.setSupportZoom(true)
        webSettings.javaScriptEnabled = true //js交互
        webSettings.cacheMode = WebSettings.LOAD_CACHE_ONLY //缓存模式
        webSettings.domStorageEnabled = true //DOM storage
        webSettings.databaseEnabled = true //database storage
        webSettings.allowContentAccess = true
        webSettings.allowFileAccess = true
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.allowUniversalAccessFromFileURLs = true

        //https与http混合资源处理
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
    }

    inner class DefaultWebChromeClient: WebChromeClient() {
        override fun onReceivedTitle(view: WebView?, title: String?) {
            super.onReceivedTitle(view, title)
            if (null != webChromeClient) {
                webChromeClient!!.onReceivedTitle(view, title)
            }
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            if (newProgress == 100) {
                progressBar?.visibility = GONE
            } else {
                if (progressBar?.visibility == GONE) progressBar?.visibility = VISIBLE
                progressBar?.progress = newProgress
            }
            if (null != webChromeClient) webChromeClient!!.onProgressChanged(view, newProgress)
        }

        override fun onPermissionRequest(request: PermissionRequest) {
            if (null == webChromeClient) {
                request.grant(request.resources)
            } else {
                webChromeClient!!.onPermissionRequest(request)
            }
        }
    }
}