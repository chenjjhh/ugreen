package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/12/2
 * Time: 9:40
 * Desc:
 */
data class OtaBean(
    val filePath: String,
    val fileMd5: String
)
