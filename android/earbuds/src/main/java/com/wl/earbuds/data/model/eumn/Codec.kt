package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 16:44
 * Desc:
 */
enum class Codec(val value: Int) {
    SBC(0x00),
    AAC(0x02),
    LDAC(0xFF);

    companion object {
        @JvmStatic
        fun valueOf(value: Int): Codec = values().find { type -> type.value == value } ?: SBC
    }
}