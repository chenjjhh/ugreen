package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 16:44
 * Desc:
 */
enum class AncLevel(val value: Int) {
    ANC_DEEP(0x0A),
    ANC_MEDIUM(0x0B),
    ANC_LIGHT(0x0C);

    companion object {
        @JvmStatic
        fun valueOf(value: Int): AncLevel? = values().find { type -> type.value == value.toInt() }
    }
}