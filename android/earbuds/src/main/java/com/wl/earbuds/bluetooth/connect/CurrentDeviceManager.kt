package com.wl.earbuds.bluetooth.connect

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import cn.wandersnail.bluetooth.BTManager
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.db.model.UgDevice
import com.wl.earbuds.bluetooth.ota.UpgradeState
import com.wl.earbuds.data.model.entity.*
import com.wl.earbuds.data.model.eumn.Anc
import com.wl.earbuds.data.model.eumn.Find

/**
 * User: wanlang_dev
 * Data: 2022/11/16
 * Time: 18:13
 * Desc:
 */
object CurrentDeviceManager : IDeviceManager {

    private var _ugDevice: UgDevice? = null
    var ugDevice: UgDevice?
        get() = _ugDevice
        set(value) {
            val isSame = value?.macAddress == _ugDevice?.macAddress
            _ugDevice = value
            if (!isSame) {
                resetAllState()
            }
            _deviceState.postValue(value)
            if (value != null) {
                if (isOnline()) {
                    getDeviceManager()?.syncState()
                }
            }
        }
    private var _device: BluetoothDevice? = null
    var device: BluetoothDevice?
        get() = _device
        set(value) {
            if (value == _device) return
            if (value?.address != _device?.address) {
                _device = value
            }
        }
    // 当前查看的设备
    /*private var _currentDevice: Device? = null

    var currentDevice: Device?
        set(value) {
            _currentDevice = value
        }
        get() = _currentDevice*/

    private val _deviceState: UnPeekLiveData<UgDevice?> = UnPeekLiveData(_ugDevice)
    private val _onlineState: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _versionState: MutableLiveData<VersionState> = MutableLiveData(VersionState())
    private val _ancState: MutableLiveData<AncState> = MutableLiveData(AncState())
    private val _eqState: MutableLiveData<EqualizerState> = MutableLiveData(EqualizerState())
    private val _dualState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _dualHintState: UnPeekLiveData<SwitchState> = UnPeekLiveData(SwitchState(false))
    private val _gameModeState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _hqDecodeState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _voiceState: MutableLiveData<VoiceState> = MutableLiveData(VoiceState())
    private val _gestureState: MutableLiveData<GestureState> = MutableLiveData(GestureState())
    private val _leftFindState: MutableLiveData<FindState> =
        MutableLiveData(FindState(Find.FIND_LEFT_CLOSE))
    private val _rightFindState: MutableLiveData<FindState> =
        MutableLiveData(FindState(Find.FIND_RIGHT_CLOSE))
    private val _batteryState: MutableLiveData<BatteryState> = MutableLiveData(BatteryState())
    private val _dualDeviceListState: MutableLiveData<List<DualDevice>> = MutableLiveData()
    private val _codecState: MutableLiveData<CodecState> = MutableLiveData()
    private val _inBoxState: UnPeekLiveData<SwitchState> = UnPeekLiveData()
    private val _failState: MutableLiveData<FailState> = MutableLiveData()
    private val _upgradeState: UnPeekLiveData<UpgradeState> = UnPeekLiveData()

    /*private var _connection: Connection? = null

    var connection: Connection?
        set(value) {
            _connection = value
        }
        get() = _connection*/

    @SuppressLint("MissingPermission")
    fun getName(): String {
        return _ugDevice?.name ?: _device?.name ?: ""
    }

    fun getAddress(): String {
        return _ugDevice?.macAddress ?: ""
    }

    fun getDeviceManager(): DeviceManager? {
        return ClassicManager.getDeviceManagerByAddress(getAddress())
    }

    fun resetAllState() {
        _onlineState.postValue(false)
        _ancState.postValue(AncState())
        _eqState.postValue(EqualizerState())
        _leftFindState.postValue(FindState(Find.FIND_LEFT_CLOSE))
        _rightFindState.postValue(FindState(Find.FIND_RIGHT_CLOSE))
        _dualState.postValue(SwitchState(false))
        _dualHintState.postValue(SwitchState(false))
        _gameModeState.postValue(SwitchState(false))
        _hqDecodeState.postValue(SwitchState(false))
        _voiceState.postValue(VoiceState())
        _gestureState.postValue(GestureState())
        _batteryState.postValue(BatteryState())
    }

    fun isOnline(): Boolean {
        return getDeviceManager()?.isOnline() ?: false
    }

    fun getBatteryState(): BatteryState = _batteryState.value!!

    fun getAncState(): AncState = _ancState.value!!

    fun getEqState() = _eqState.value!!

    fun getLeftState() = _leftFindState.value!!

    fun getRightState() = _rightFindState.value!!

    fun getDualState(): SwitchState = _dualState.value!!

    fun getGameModeState(): SwitchState = _gameModeState.value!!

    fun getHqState(): SwitchState = _hqDecodeState.value!!

    fun getVoiceState() = _voiceState.value!!

    fun getGestureState(): GestureState = _gestureState.value!!

    fun getVersionState(): VersionState = _versionState.value!!

    fun getDualDeviceList(): List<DualDevice>? = _dualDeviceListState.value

    fun getCodecState(): CodecState? = _codecState.value

    fun updateOnlineState(device: BluetoothDevice, isOnline: Boolean) {
        if (device.address != _device?.address) return
        _onlineState.postValue(isOnline)
    }

    private fun isCurDevice(device: BluetoothDevice): Boolean {
        return device.address == _device?.address
    }

    fun updateVersionState(device: BluetoothDevice, versionState: VersionState) {
        if (!isCurDevice(device)) return
        _versionState.postValue(versionState)
    }

    fun updateAncState(device: BluetoothDevice, ancState: AncState) {
        if (!isCurDevice(device)) return
        _ancState.postValue(
            if (ancState.mode == Anc.ANC_CLOSE || ancState.mode == Anc.ANC_TRANS) {
                AncState(ancState.mode, _ancState.value!!.level)
            } else {
                ancState
            }
        )
    }

    fun updateEqState(device: BluetoothDevice, eqState: EqualizerState) {
        if (!isCurDevice(device)) return
        _eqState.postValue(eqState)
    }

    fun updateLeftFindState(device: BluetoothDevice, findState: FindState) {
        if (!isCurDevice(device)) return
        _leftFindState.postValue(findState)
    }

    fun updateRightFindState(device: BluetoothDevice, findState: FindState) {
        if (!isCurDevice(device)) return
        _rightFindState.postValue(findState)
    }

    fun updateDualState(device: BluetoothDevice, dualState: SwitchState) {
        if (!isCurDevice(device)) return
        _dualState.postValue(dualState)
    }

    fun updateDualHintState(device: BluetoothDevice, dualHintState: SwitchState) {
        if (!isCurDevice(device)) return
        _dualHintState.postValue(dualHintState)
    }

    fun updateGameModeState(device: BluetoothDevice, gameModeState: SwitchState) {
        if (!isCurDevice(device)) return
        _gameModeState.postValue(gameModeState)
    }

    fun updateHqDecodeState(device: BluetoothDevice, hqDecodeState: SwitchState) {
        if (!isCurDevice(device)) return
        _hqDecodeState.postValue(hqDecodeState)
    }

    fun updateVoiceState(device: BluetoothDevice, voiceState: VoiceState) {
        if (!isCurDevice(device)) return
        _voiceState.postValue(voiceState)
    }

    fun updateGestureState(device: BluetoothDevice, gestureState: GestureState) {
        if (!isCurDevice(device)) return
        _gestureState.postValue(gestureState)
    }

    fun updateBatteryState(device: BluetoothDevice, batteryState: BatteryState) {
        if (!isCurDevice(device)) return
        _batteryState.postValue(batteryState)
    }

    fun updateDualDeviceState(device: BluetoothDevice, dualDeviceList: List<DualDevice>) {
        if (!isCurDevice(device)) return
        _dualDeviceListState.postValue(dualDeviceList)
    }

    fun updateCodecState(device: BluetoothDevice, codecState: CodecState) {
        if (!isCurDevice(device)) return
        _codecState.postValue(codecState)
    }

    fun updateInBoxState(device: BluetoothDevice, inBoxState: SwitchState) {
        if (!isCurDevice(device)) return
        _inBoxState.postValue(inBoxState)
    }

    fun updateFailState(device: BluetoothDevice, failState: FailState) {
        if (!isCurDevice(device)) return
        _failState.postValue(failState)
    }

    fun updateUpgradeState(device: BluetoothDevice, upgradeState: UpgradeState) {
        if (!isCurDevice(device)) return
        _upgradeState.postValue(upgradeState)
    }

    fun updateConnectionErrorStatus(device: BluetoothDevice, state: Int) {
        if (!isCurDevice(device)) return
        _onlineState.postValue(false)
    }

    fun observeUgDeviceState(owner: LifecycleOwner, observer: Observer<UgDevice?>) {
        _deviceState.observe(owner, observer)
    }

    fun observeOnlineState(owner: LifecycleOwner, observer: Observer<Boolean>) {
        _onlineState.observe(owner, observer)
    }

    fun observeVersionState(owner: LifecycleOwner, observer: Observer<VersionState>) {
        _versionState.observe(owner, observer)
    }

    fun observeAncState(owner: LifecycleOwner, observer: Observer<AncState>) {
        _ancState.observe(owner, observer)
    }

    fun observeEqState(owner: LifecycleOwner, observer: Observer<EqualizerState>) {
        _eqState.observe(owner, observer)
    }

    fun observeLeftFindState(owner: LifecycleOwner, observer: Observer<FindState>) {
        _leftFindState.observe(owner, observer)
    }

    fun observeRightFindState(owner: LifecycleOwner, observer: Observer<FindState>) {
        _rightFindState.observe(owner, observer)
    }

    fun observeDualState(owner: LifecycleOwner, observer: Observer<SwitchState>) {
        _dualState.observe(owner, observer)
    }

    fun observeDualHintState(owner: LifecycleOwner, observer: Observer<SwitchState>) {
        _dualHintState.observe(owner, observer)
    }

    fun observeGameModeState(owner: LifecycleOwner, observer: Observer<SwitchState>) {
        _gameModeState.observe(owner, observer)
    }

    fun observeHqDecodeState(owner: LifecycleOwner, observer: Observer<SwitchState>) {
        _hqDecodeState.observe(owner, observer)
    }

    fun observeVoiceState(owner: LifecycleOwner, observer: Observer<VoiceState>) {
        _voiceState.observe(owner, observer)
    }

    fun observeGestureState(owner: LifecycleOwner, observer: Observer<GestureState>) {
        _gestureState.observe(owner, observer)
    }

    fun observeBatteryState(owner: LifecycleOwner, observer: Observer<BatteryState>) {
        _batteryState.observe(owner, observer)
    }

    fun observeDualDeviceState(owner: LifecycleOwner, observer: Observer<List<DualDevice>>) {
        _dualDeviceListState.observe(owner, observer)
    }

    fun observeCodecState(owner: LifecycleOwner, observer: Observer<CodecState>) {
        _codecState.observe(owner, observer)
    }

    fun observeInBoxState(owner: LifecycleOwner, observer: Observer<SwitchState>) {
        _inBoxState.observe(owner, observer)
    }

    fun observeFailState(owner: LifecycleOwner, observer: Observer<FailState>) {
        _failState.observe(owner, observer)
    }

    fun observeUpgradeState(owner: LifecycleOwner, observer: Observer<UpgradeState>) {
        _upgradeState.observe(owner, observer)
    }

    override fun connect() {
    }

    override fun disconnect() {
        if (isOnline()) {
            BTManager.getInstance().disconnectConnection(_device!!)
        }
    }
}