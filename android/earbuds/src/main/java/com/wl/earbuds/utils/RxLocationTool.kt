package com.wl.earbuds.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.provider.Settings


object RxLocationTool {

    /**
     * 判断是否支持Gps
     *
     * @return `true`: 是<br></br>`false`: 否
     */
    @JvmStatic
    fun isGpsAvailable(context: Context): Boolean {
        val pm: PackageManager = context.packageManager
        return pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)
    }

    /**
     * 判断Gps是否可用
     *
     * @return `true`: 是<br></br>`false`: 否
     */
    @JvmStatic
    fun isGpsEnabled(context: Context): Boolean {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    /**
     * 判断定位是否可用
     *
     * @return `true`: 是<br></br>`false`: 否
     */
    @JvmStatic
    fun isLocationEnabled(context: Context): Boolean {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || lm.isProviderEnabled(
            LocationManager.GPS_PROVIDER
        )
    }

    /**
     * 打开Gps设置界面
     */
    @JvmStatic
    fun openGpsSettings(context: Context) {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }
}