package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.AncLevel

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:33
 * Desc:
 */
data class AncBean(val name: String, val hint: String, val ancLevel: AncLevel)