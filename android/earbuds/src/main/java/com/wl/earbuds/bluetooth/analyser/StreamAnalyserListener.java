package com.wl.earbuds.bluetooth.analyser;

import androidx.annotation.NonNull;

import com.wl.earbuds.bluetooth.core.FrameType;


public interface StreamAnalyserListener {

    void onDataFound(@NonNull FrameType type, @NonNull byte[] data);

    void onOtaResponse(@NonNull byte[] data);

}
