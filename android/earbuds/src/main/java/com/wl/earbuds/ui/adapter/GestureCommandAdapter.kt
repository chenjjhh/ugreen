package com.wl.earbuds.ui.adapter

import android.view.View
import android.widget.RadioButton
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.GestureCommandBean
import com.wl.earbuds.data.model.eumn.AncLevel
import com.wl.earbuds.data.model.eumn.Gesture
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class GestureCommandAdapter(data: MutableList<GestureCommandBean>) :
    BaseQuickAdapter<GestureCommandBean, BaseViewHolder>(R.layout.item_command, data) {
    private var _currentGesture: Gesture = Gesture.NONE
    private var _lastIndex: Int = -1
    var currentGesture: Gesture
        set(value) {
            val selectIndex = data.indexOfFirst { it.gesture == value }
            if (selectIndex != -1) {
                _currentGesture = value
                notifyItemChanged(_lastIndex)
                notifyItemChanged(selectIndex)
                _lastIndex = selectIndex
            }
        }
        get() = _currentGesture

    override fun convert(holder: BaseViewHolder, item: GestureCommandBean) {
        holder.setText(R.id.tv_command, item.command)
        holder.getView<TextView>(R.id.tv_hint).apply {
            if (item.hint.isEmpty()) {
                gone()
            } else {
                visible()
                text = item.hint
            }
        }
        holder.getView<RadioButton>(R.id.radio_btn).isChecked =
            holder.adapterPosition == _lastIndex
    }
}