package com.wl.earbuds.bluetooth.utils

import com.wl.earbuds.bluetooth.core.CommandFrame

/**
 * 作者: wanlang_dev
 * 日期: 2022/4/19 16:08
 * 描述:
 */
/**
 * 创建payload （含有一个key）
 */
fun generatePureCommandPack(key: Int): ByteArray {
    return ByteArray(1).also {
        BytesUtils.setUINT8(key, it, 0)
    }
}

/**
 * 解析payloadLength
 */
fun parsePayloadLength(source: ByteArray): Int {
    val lengthPack = BytesUtils.getUINT16Little(source, 1)
    return BytesUtils.getValueFromBits(lengthPack, 7, 9)
}

/**
 * 解析只含有值的payload
 */
fun parsePureCommondValue(source: ByteArray): Int {
    return BytesUtils.getUINT32Little(source, 3).toInt()
}

fun formatCommandData(commandId: Int, content: ByteArray): ByteArray {
    val commandFrame = CommandFrame(commandId, content)
    return commandFrame.format()
}

fun formatOtaData(content: ByteArray): ByteArray {
    return ByteArray(3)
}