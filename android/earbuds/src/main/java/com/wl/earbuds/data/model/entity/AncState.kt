package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Anc
import com.wl.earbuds.data.model.eumn.AncLevel

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 9:59
 * Desc:
 */
data class AncState(val mode: Anc = Anc.ANC_CLOSE, val level: AncLevel = AncLevel.ANC_DEEP) {
}