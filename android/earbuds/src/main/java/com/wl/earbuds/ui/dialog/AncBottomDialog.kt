package com.wl.earbuds.ui.dialog

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wanlang.base.BaseDialog
import com.wanlang.base.BottomSheetDialog
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.AncBean
import com.wl.earbuds.data.model.eumn.AncLevel
import com.wl.earbuds.ui.adapter.AncAdapter
import com.wl.earbuds.utils.ext.click
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.generateAncData
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration

/**
 * User: wanlang_dev
 * Data: 2022/11/8
 * Time: 16:21
 * Desc:
 */
class AncBottomDialog(context: Context, ancLevel: AncLevel) : BottomSheetDialog(context) {

    private val data: MutableList<AncBean> by lazy {
        generateAncData(context)
    }

    private val mAncAdapter: AncAdapter by lazy {
        AncAdapter(data).apply {
            setOnItemClickListener { adapter, view, position ->
                data[position].ancLevel.apply {
                    currentAnc = this
                }
            }
            currentAnc = ancLevel
        }
    }

    private val mRecyclerView: RecyclerView? by lazy { findViewById(R.id.rv_item) }
    private val mCancelView: View? by lazy { findViewById(R.id.tv_cancel) }
    private val mConfirmView: View? by lazy { findViewById(R.id.tv_confirm) }
    private var _mListener: OnListener? = null

    init {
        setContentView(R.layout.dialog_anc)
        mRecyclerView?.apply {
            layoutManager = LinearLayoutManager(context)
            val decoration =
                SpinnerDividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(35f),
                    dp2px(35f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mAncAdapter
            itemAnimator = null
        }
        mCancelView?.click {
            _mListener?.onCancel(this)
        }
        mConfirmView?.click {
            _mListener?.onConfirm(this, mAncAdapter.currentAnc)
        }
    }

    fun updateAncLevel(level: AncLevel) {
        mAncAdapter.currentAnc = level
    }

    fun setListener(listener: OnListener?): AncBottomDialog {
        _mListener = listener
        return this
    }

    interface OnListener {
        fun onCancel(baseDialog: BaseDialog)
        fun onConfirm(baseDialog: BaseDialog, ancLevel: AncLevel)
    }

}