package com.wl.earbuds.network

import com.wl.earbuds.BuildConfig
import com.wl.earbuds.data.model.entity.*
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * User: wanlang_dev
 * Data: 2022/11/28
 * Time: 11:33
 * Desc:
 */
interface ApiService {

    companion object {
        const val HOST = "http://hituneapi.ugreengroup.com/"
//        const val HOST = "http://ugreenmusicapi.aaake.com/"
//        const val TEST_HOST = "http://ugreenmusicapi.aaake.com/"
        const val TEST_HOST = "http://hituneapi.ugreengroup.com/"
         const val REMOTE_HOST = "http://hituneapi.ugreengroup.com/"
//        const val REMOTE_HOST = "http://hituneapi.ugreengroup.com/"
        fun getHost() = if (BuildConfig.DEBUG) TEST_HOST else HOST
    }

    /**
     * (1隐私政策2用户协议3产品说明书)
     */
    @GET("/app/system/agreement/listAll")
    suspend fun getProtocol(@Query("type") type: Int): ApiResponse<ContentResponse>

    @GET("/app/system/iotversion/getIotNewRelease")
    suspend fun getEarbudsVersion(): ApiResponse<EarbudsVersionBean>

    @GET("/app/system/appversion/getAppNewRelease")
    suspend fun getAppVersion(): ApiResponse<AppVersionResponse>

    @GET("/app/system/funcswitch/getConfig")
    suspend fun getAppConfig(): ApiResponse<AppConfig>

    @GET("/app/system/startpage/listAll")
    suspend fun getWelcomePic(): ApiResponse<List<String>>
}