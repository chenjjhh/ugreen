package com.wl.earbuds.ui.find

import android.bluetooth.BluetoothDevice
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.view.View
import cn.wandersnail.bluetooth.BTManager
import com.wanlang.base.BaseDialog
import com.wl.db.model.UgDevice
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.*
import com.wl.earbuds.bluetooth.earbuds.setFind
import com.wl.earbuds.data.model.entity.BatteryState
import com.wl.earbuds.data.model.entity.FindState
import com.wl.earbuds.data.model.eumn.Find
import com.wl.earbuds.databinding.FragmentFindEarbudsBinding
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

class FindEarbudsFragment : BaseFragment<FindViewModel, FragmentFindEarbudsBinding>(),
    ConnectListener {

    private val mConnectManager by lazy {
        ConnectManager.getInstance(requireContext().applicationContext)
    }

    private var mReconnectDevice: UgDevice? = null
    private var mIsDiscovery = false
    private var mFindDialog: BaseDialog? = null
//    private var isReminded = false

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initTitle()
        initButton()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.find_earbuds)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun initButton() {
        mDatabind.rpvLeft.setCommandText(com.wl.resource.R.string.earbuds_left_ring)
        mDatabind.rpvRight.setCommandText(com.wl.resource.R.string.earbuds_right_ring)
        val isLeftPlay = CurrentDeviceManager.getLeftState().find == Find.FIND_LEFT_ON
        mDatabind.rpvLeft.isPlay = isLeftPlay
        if (isLeftPlay) {
            startAnimLeft()
        }
        val isRightPlay = CurrentDeviceManager.getLeftState().find == Find.FIND_RIGHT_ON
        mDatabind.rpvRight.isPlay = isRightPlay
        if (isRightPlay) {
            startAnimRight()
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeBatteryState(viewLifecycleOwner, ::onBatteryChange)
        CurrentDeviceManager.observeLeftFindState(viewLifecycleOwner, ::onLeftFind)
        CurrentDeviceManager.observeRightFindState(viewLifecycleOwner, ::onRightFind)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mDatabind.groupReconnect.visibility = if (isOnline) View.GONE else View.VISIBLE
        mDatabind.tvFindEarbudsHint.visibility = if (isOnline) View.VISIBLE else View.GONE
        if (isOnline) {
            mDatabind.tvReconnect.text = getString(com.wl.resource.R.string.reconnect)
        } else {
            if (mFindDialog?.isShowing == true) {
                mFindDialog?.dismiss()
            }
            safeNav {
                nav().navigateUp()
            }
            // onBatteryChange(CurrentDeviceManager.getBatteryState())
        }
    }

    private fun onBatteryChange(batteryState: BatteryState) {

        (batteryState.isLeftOn() && CurrentDeviceManager.isOnline()).also {
            mlog("isLeftOn： $it")
            mDatabind.rpvLeft.isEnabled = it
        }

        (batteryState.isRightOn() && CurrentDeviceManager.isOnline()).also {
            mlog("isRightOn： $it")
            mDatabind.rpvRight.isEnabled = it
        }
    }

    private fun onLeftFind(findState: FindState) {
        val isLeftPlay = findState.find == Find.FIND_LEFT_ON
        mDatabind.rpvLeft.isPlay = isLeftPlay
        if (isLeftPlay) {
            startAnimLeft()
        } else {
            stopAnimLeft()
        }
    }

    private fun onRightFind(findState: FindState) {
        val isRightPlay = findState.find == Find.FIND_RIGHT_ON
        mDatabind.rpvRight.isPlay = isRightPlay
        if (isRightPlay) {
            startAnimRight()
        } else {
            stopAnimRight()
        }
    }

    private fun showNotice(find: Find) {
        mFindDialog = CommonNoticeDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.volume_notice))
            .setContent(getString(com.wl.resource.R.string.volume_notice_hint))
            .setConfirmText(getString(com.wl.resource.R.string.play))
            .setActionListener(object : CommonNoticeDialog.ActionListener {
                override fun onCancel(dialog: BaseDialog?) {
                    super.onCancel(dialog)
                    dialog?.dismiss()
                }

                override fun onConfirm(dialog: BaseDialog?) {
                    super.onConfirm(dialog)
                    processFind(find)
                    dialog?.dismiss()
                }
            }).create()
        mFindDialog?.show()
    }

    private fun processFind(find: Find) {
        setFind(find)
//        isReminded = true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mConnectManager.addConnectListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mConnectManager.removeConnectListener(this)
    }

    override fun onDeviceFound(device: BluetoothDevice) {
        super.onDeviceFound(device)
        if (mReconnectDevice?.macAddress == device.address) {
            BTManager.getInstance().createBond(device)
        }
    }

    override fun onDeviceBond(device: BluetoothDevice) {
        super.onDeviceBond(device)
        if (mReconnectDevice?.macAddress != device.address) return
        ClassicManager.connect(device)
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        super.onDeviceConnected(device)
        if (CurrentDeviceManager.getAddress() != device.address) return
        CurrentDeviceManager.device = device
        resetConnectStatus()
    }

    override fun onPairRefused(device: BluetoothDevice) {
        super.onPairRefused(device)
        if (mReconnectDevice?.macAddress != device.address) return
        showMsg(getString(com.wl.resource.R.string.connect_fail))
        resetConnectStatus()
    }

    private fun resetConnectStatus() {
        mDatabind.tvReconnect.text = getString(com.wl.resource.R.string.reconnect)
        mReconnectDevice = null
    }

    override fun onDiscoveryStart() {
        super.onDiscoveryStart()
        mIsDiscovery = true
    }

    override fun onDiscoveryFinished(devices: MutableList<BluetoothDevice>) {
        if (mReconnectDevice == null) return
        if (!mIsDiscovery) return
        if (devices.find { it.address == mReconnectDevice!!.macAddress } == null) {
            resetConnectStatus()
            showMsg(getString(com.wl.resource.R.string.no_device_find))
        }
        mIsDiscovery = false
    }

    override fun onConnectFail(address: String, status: Int) {
        if (mReconnectDevice == null) return
        if (CurrentDeviceManager.getAddress() != address) return
        when (status) {
            ConnectError.DEVICE_NOT_FOUND -> {
                showMsg(getString(com.wl.resource.R.string.scan_device_not_found))
            }
            ConnectError.CONNECT_DISCONNECT, ConnectError.CONNECT_FAIL -> {
                showMsg(getString(com.wl.resource.R.string.connect_fail))
            }
        }
        if (!CurrentDeviceManager.isOnline()) {
            resetConnectStatus()
        }
    }

    private fun startAnimLeft() {
        mDatabind.ivLeftAnim.visible()
        mDatabind.ivLabelLeft.setImageResource(com.wl.resource.R.mipmap.ic_left_green)
        val animationDrawable = mDatabind.ivLeftAnim.background as AnimationDrawable
        if (!animationDrawable.isRunning) {
            animationDrawable.start()
        }
    }

    private fun stopAnimLeft() {
        mDatabind.ivLabelLeft.setImageResource(com.wl.resource.R.mipmap.ic_left_gray)
        val animationDrawable = mDatabind.ivLeftAnim.background as AnimationDrawable
        if (animationDrawable.isRunning) {
            animationDrawable.stop()
        }
        mDatabind.ivLeftAnim.gone()
    }

    private fun startAnimRight() {
        mDatabind.ivRightAnim.visible()
        mDatabind.ivLabelRight.setImageResource(com.wl.resource.R.mipmap.ic_right_green)
        val animationDrawable = mDatabind.ivRightAnim.background as AnimationDrawable
        if (!animationDrawable.isRunning) {
            animationDrawable.start()
        }
    }

    private fun stopAnimRight() {
        mDatabind.ivLabelRight.setImageResource(com.wl.resource.R.mipmap.ic_right_gray)
        val animationDrawable = mDatabind.ivRightAnim.background as AnimationDrawable
        if (animationDrawable.isRunning) {
            animationDrawable.stop()
        }
        mDatabind.ivRightAnim.gone()
    }

    inner class ProxyClick {
        fun clickLeft() {
            if (mDatabind.rpvLeft.isPlay) {
                setFind(Find.FIND_LEFT_CLOSE)
            } else {
//                if (isReminded) {
//                    processFind(Find.FIND_LEFT_ON)
//                } else {
                showNotice(Find.FIND_LEFT_ON)
//                }
            }
        }

        fun clickRight() {
            if (mDatabind.rpvRight.isPlay) {
                setFind(Find.FIND_RIGHT_CLOSE)
            } else {
//                if (isReminded) {
//                    processFind(Find.FIND_RIGHT_ON)
//                } else {
                showNotice(Find.FIND_RIGHT_ON)
//                }
            }
        }

        fun reconnect() {
            if (mReconnectDevice != null) return
            if (mConnectManager.checkBluetooth(requireActivity())) {
                mReconnectDevice = CurrentDeviceManager.ugDevice!!
                mConnectManager.connectUgDevice(mReconnectDevice!!)
                mDatabind.tvReconnect.text = getString(com.wl.resource.R.string.connecting)
            }
        }
    }
}