package com.wl.earbuds.ui.earbuds

import android.annotation.SuppressLint
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wanlang.base.BaseDialog
import com.wanlang.base.BuildConfig
import com.wl.earbuds.R
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.earbuds.switchDual
import com.wl.earbuds.bluetooth.earbuds.switchDualOff
import com.wl.earbuds.data.model.entity.DualDevice
import com.wl.earbuds.data.model.entity.SwitchState
import com.wl.earbuds.databinding.FragmentEarbudsDualBinding
import com.wl.earbuds.ui.adapter.DualDeviceAdapter
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.util.bluetoothManager

class EarbudsDualFragment : BaseFragment<EarbudsDualViewModel, FragmentEarbudsDualBinding>() {

    companion object {
        fun newInstance() = EarbudsDualFragment()
    }

    private var data: MutableList<DualDevice> =
        if (BuildConfig.DEBUG) {
            mutableListOf<DualDevice>().apply {
                // add(DualDevice("test1"))
            }
        } else {
            mutableListOf<DualDevice>()
        }


    private val mDualDeviceAdapter: DualDeviceAdapter by lazy {
        DualDeviceAdapter(data).apply {
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initTitle()
        initRv()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.earbuds_dual)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            val decoration =
                SpinnerDividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(18f), dp2px(18f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider_line))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mDualDeviceAdapter
            itemAnimator = null
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeDualState(viewLifecycleOwner, ::onDualChange)
        CurrentDeviceManager.observeDualHintState(viewLifecycleOwner, ::onDualHint)
        CurrentDeviceManager.observeDualDeviceState(viewLifecycleOwner, ::onDualDeviceChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        if (!isOnline)
            safeNav {
                nav().navigateUp()
            }
    }

    private fun onDualChange(dualState: SwitchState) {
        mViewModel.isDual.set(dualState.boolean)
    }

    private fun onDualDeviceChange(dualDeviceList: List<DualDevice>) {
        data = mutableListOf<DualDevice>().apply {
            addAll(dualDeviceList)
        }
        mDualDeviceAdapter.setNewInstance(data)
    }

    private fun onDualHint(dualHintState: SwitchState) {
        if (dualHintState.boolean) {
            showMsg(getString(com.wl.resource.R.string.dual_switch_hint))
        }
    }

    private fun showDualDialog() {
        CommonNoticeDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.earbuds_dual_close))
            .setContent(
                getString(
                    com.wl.resource.R.string.earbuds_dual_close_hint,
                    mDualDeviceAdapter.data[1].name
                )
            )
            .setConfirmText(getString(com.wl.resource.R.string.disconnect))
            .setYOffset(requireContext().dp2px(60f))
            .setActionListener(object : CommonNoticeDialog.ActionListener {
                override fun onCancel(dialog: BaseDialog?) {
                    super.onCancel(dialog)
                    dialog?.dismiss()
                }

                override fun onConfirm(dialog: BaseDialog?) {
                    super.onConfirm(dialog)
                    appViewModel.mLastSwitchTime = System.currentTimeMillis()
                    mViewModel.isDual.set(!mViewModel.isDual.get())
                    val dualDevice = mDualDeviceAdapter.data[1]
                    switchDualOff(dualDevice.index, dualDevice.address)
                    dialog?.dismiss()
                }
            }).show()
    }

    inner class ProxyClick {
        @SuppressLint("ClickableViewAccessibility")
        var switchDualListener = View.OnTouchListener { v, event ->
            if (mViewModel.isDual.get() && data.size == 2) {
                showDualDialog()
            } else if (System.currentTimeMillis() - appViewModel.mLastSwitchTime > appViewModel.mFreezeInterval) {
                mViewModel.isDual.set(!mViewModel.isDual.get())
                switchDual(mViewModel.isDual.get())
            } else {
                showMsg(getString(com.wl.resource.R.string.operate_freeze))
            }
            return@OnTouchListener true
        }
    }
}