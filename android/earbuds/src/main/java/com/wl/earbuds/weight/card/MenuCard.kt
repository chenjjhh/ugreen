package com.wl.earbuds.weight.card

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.wl.earbuds.R
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.utils.setAnimateAlpha
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 16:36
 * Desc:
 */
class MenuCard : FrameLayout, Card, OnClickListener {

    lateinit var clDeviceDual: ConstraintLayout
    lateinit var clFindEarbuds: ConstraintLayout
    lateinit var clHqDecode: ConstraintLayout
    lateinit var clHardwireUpgrade: ConstraintLayout
    lateinit var tvEarbudsVersion: TextView
    lateinit var upgradePoint: View

    private var mClickListener: MenuCard.OnMenuClickListener? = null
    private var isInit: Boolean = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.layout_menu, this)
        clDeviceDual = findViewById(R.id.cl_device_dual)
        clFindEarbuds = findViewById(R.id.cl_find_earbuds)
        clHqDecode = findViewById(R.id.cl_hq_decode)
        clHardwireUpgrade = findViewById(R.id.cl_earbuds_upgrade)
        tvEarbudsVersion = findViewById(R.id.tv_earbuds_version)
        upgradePoint = findViewById(R.id.upgrade_point)
        updateUI()
    }
    
    override fun updateUI() {
        if (!isInit) {
            initClick()
        }
        if (CurrentDeviceManager.isOnline()) {
            setAnimateAlpha(this, EarbudsConstants.ALPHA_ON_VALUE, true)
        } else {
            setAnimateAlpha(this, EarbudsConstants.ALPHA_OFF_VALUE)
        }
    }

    fun updateVersionText(versionStr: String, canUpgrade: Boolean = false) {
        tvEarbudsVersion.text = versionStr
        if (canUpgrade) {
            tvEarbudsVersion.setTextColor(context.getColor(com.wl.resource.R.color.standard_primary))
            upgradePoint.visible()
        } else {
            tvEarbudsVersion.setTextColor(context.getColor(com.wl.resource.R.color.text_tertiary))
            upgradePoint.gone()
        }
    }

    private fun initClick() {
        isInit = true
        clDeviceDual.setOnClickListener(this)
        clFindEarbuds.setOnClickListener(this)
        clHqDecode.setOnClickListener(this)
        clHardwireUpgrade.setOnClickListener(this)
    }

    fun setMenuClickListener(listener: OnMenuClickListener?) {
        mClickListener = listener
    }

    override fun onClick(v: View) {
        if (!CurrentDeviceManager.isOnline()) return
        when (v.id) {
            R.id.cl_device_dual -> {mClickListener?.onDeviceDualClick()}
            R.id.cl_find_earbuds -> {mClickListener?.onFindClick()}
            R.id.cl_hq_decode -> {mClickListener?.onHQDecodeClick()}
            R.id.cl_earbuds_upgrade -> {mClickListener?.onUpgradeClick()}
            else -> {}
        }
    }
    
    interface OnMenuClickListener {
        fun onDeviceDualClick()
        fun onFindClick()
        fun onHQDecodeClick()
        fun onUpgradeClick()
    }
}