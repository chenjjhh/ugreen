package com.wl.earbuds.ui.device

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cn.wandersnail.bluetooth.BTManager
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.base.BaseActivity
import com.wl.earbuds.bluetooth.connect.ConnectManager
import com.wl.earbuds.databinding.ActivityDeviceListBinding
import com.wl.earbuds.utils.Constants
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.lifecycle.KtxActivityManger

class DeviceListActivity : BaseActivity<BaseViewModel, ActivityDeviceListBinding>() {

    override fun initView(savedInstanceState: Bundle?) {
        ConnectManager.getInstance(application).initialize()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val isExit = intent?.getBooleanExtra(Constants.EXIT, false) ?: false
        if (isExit) {
            finish()
        }
    }

    override fun onDestroy() {
        if (BTManager.getInstance().isDiscovering) {
            BTManager.getInstance().stopDiscovery()
        }
        if (!recreateFlag && KtxActivityManger.isLast) {
            BTManager.getInstance().releaseAllConnections()
            appViewModel.mIgnoreUpgrade = false
        }
        super.onDestroy()
    }
}