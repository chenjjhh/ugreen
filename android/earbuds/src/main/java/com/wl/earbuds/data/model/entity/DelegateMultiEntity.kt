package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/30
 * Time: 10:06
 * Desc:
 */
data class DelegateMultiEntity(val type: Int, val version: VersionBean) {
    companion object {
        const val CURRENT_VERSION = 0
        const val NEW_VERSION = 1
    }
}