package com.wl.earbuds.ui.adapter

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import cn.wandersnail.bluetooth.BTManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.utils.ext.mlog

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class EarbudsDeviceAdapter(data: MutableList<BluetoothDevice>) :
    BaseQuickAdapter<BluetoothDevice, BaseViewHolder>(R.layout.item_device, data) {
    private var _selectedIndex: Int = -1
    private var _targetDevice: BluetoothDevice? = null
    private var _isConnecting: Boolean = false

    var isConnecting: Boolean
        set(value) {
            _isConnecting = value
        }
        get() = _isConnecting

    var targetDevice: BluetoothDevice?
        set(value) {
            _targetDevice = value
            if (value == null && _selectedIndex != -1) {
                notifyItemChanged(_selectedIndex)
                _selectedIndex = -1
            } else {
                notifyDataSetChanged()
            }
        }
        get() = _targetDevice

    @SuppressLint("MissingPermission")
    override fun convert(holder: BaseViewHolder, item: BluetoothDevice) {
        holder.setText(R.id.tv_name, item.name)
        if (item == targetDevice) {
            holder.setText(
                R.id.tv_connect,
                context.getString(com.wl.resource.R.string.connecting)
            )
            _selectedIndex = holder.adapterPosition
        } else if (BTManager.getInstance().getConnection(item.address)?.isConnected == true) {
            holder.setText(
                R.id.tv_connect, context.getString(com.wl.resource.R.string.connected)
            )
            // holder.getView<View>(R.id.tv_status).visible()
        } else {
            holder.setText(
                R.id.tv_connect, context.getString(com.wl.resource.R.string.connect)
            )
        }
    }
}