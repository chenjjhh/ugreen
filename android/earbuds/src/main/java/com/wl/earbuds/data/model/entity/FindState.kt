package com.wl.earbuds.data.model.entity

import com.wl.earbuds.data.model.eumn.Find

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:38
 * Desc:
 */
data class FindState(val find: Find) {
}