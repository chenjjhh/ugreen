package com.wl.earbuds.ui.adapter

import android.view.View
import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.utils.ext.image
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/12/10
 * Time: 17:04
 * Desc:
 */
class GuideAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, BaseViewHolder>(
        R.layout.item_guide,
        data
    ) {
    override fun convert(holder: BaseViewHolder, item: String) {
        holder.getView<ImageView>(R.id.image).image(item)
        if (holder.adapterPosition == data.size - 1) {
            holder.getView<View>(R.id.btn_start).visible()
        } else {
            holder.getView<View>(R.id.btn_start).gone()
        }
    }
}