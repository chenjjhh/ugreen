package com.wl.earbuds.ui.upgrade

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wanlang.base.BaseDialog
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.R
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.data.model.entity.AppVersionResponse
import com.wl.earbuds.data.model.entity.DelegateMultiEntity
import com.wl.earbuds.data.model.entity.VersionBean
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.databinding.FragmentAppVersionBinding
import com.wl.earbuds.ui.adapter.AppVersionAdapter
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.ui.dialog.OnlyConfirmDialog
import com.wl.earbuds.utils.RxIntentTool
import com.wl.earbuds.utils.RxLocationTool
import com.wl.earbuds.utils.RxNetTool
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.showMsg
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import zlc.season.downloadx.State

class AppVersionFragment : BaseFragment<AppVersionViewModel, FragmentAppVersionBinding>() {

    companion object {
        fun newInstance() = AppVersionFragment()
    }

    private var mIsDownloading = false

    private var versionData: MutableList<DelegateMultiEntity> =
        mutableListOf<DelegateMultiEntity>().apply {
            add(
                DelegateMultiEntity(
                    DelegateMultiEntity.CURRENT_VERSION,
                    VersionBean(BuildConfig.versionName)
                )
            )
        }

    private val mVersionAdapter: AppVersionAdapter by lazy {
        AppVersionAdapter(versionData)
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initTitle()
        initRv()
        checkVersion()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.app_version)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mVersionAdapter
            val decoration =
                SpinnerDividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(18f),
                    dp2px(18f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            itemAnimator = null
        }
    }

    private fun checkVersion() {
        mViewModel.getAppVersion()
    }

    override fun createObserver() {
        super.createObserver()
        mViewModel.hasNewVersion.observe(viewLifecycleOwner, ::onHasNewVersion)
        mViewModel.appVersionState.observe(viewLifecycleOwner, ::onVersion)
        appViewModel.downloadState.observe(this, ::onDownloadState)
    }

    private fun onDownloadState(state: State) {
        when (state) {
            is State.Downloading -> {
                if (!mIsDownloading) {
                    mIsDownloading = true
                    showMsg(getString(com.wl.resource.R.string.background_downloading))
                }
                mDatabind.btnCheckAndUpdate.text = getString(com.wl.resource.R.string.downloading)
            }
            is State.Failed -> {
                if (mIsDownloading) {
                    showMsg(getString(com.wl.resource.R.string.download_fail))
                    mDatabind.btnCheckAndUpdate.text = getString(com.wl.resource.R.string.download_fail)
                }
                mIsDownloading = false
            }
            is State.None -> {}
            is State.Stopped -> {}
            is State.Succeed -> {
                mDatabind.btnCheckAndUpdate.text = getString(com.wl.resource.R.string.download_finish)
            }
            is State.Waiting -> {}
        }
    }

    private fun onHasNewVersion(isNew: Boolean) {
        if (isNew) {
            mDatabind.btnCheckAndUpdate.text = getString(com.wl.resource.R.string.upgrade_now)
        }
    }

    private fun onVersion(uiState: ResponseUiState<AppVersionResponse>) {
        versionData = mutableListOf()

        if (uiState.isSuccess) {
            val data = uiState.data!!
            if (BuildConfig.versionCode < data.versionCode) {
                showMsg(getString(com.wl.resource.R.string.find_new_version))
                mViewModel.versionDesc.set(getString(com.wl.resource.R.string.find_new_version))
                versionData.add(
                    DelegateMultiEntity(
                        DelegateMultiEntity.CURRENT_VERSION,
                        VersionBean(BuildConfig.versionName)
                    )
                )
                versionData.add(
                    DelegateMultiEntity(
                        DelegateMultiEntity.NEW_VERSION,
                        VersionBean("${data.versionName} (${data.fileSize})", data.content)
                    )
                )
            } else {
                showMsg(getString(com.wl.resource.R.string.hint_current_verion_is_latest))
                mViewModel.versionDesc.set(getString(com.wl.resource.R.string.version_is_latest))
                versionData.add(
                    DelegateMultiEntity(
                        DelegateMultiEntity.CURRENT_VERSION,
                        VersionBean(BuildConfig.versionName, isLatest = true)
                    )
                )
            }
        } else {
            showMsg(getString(com.wl.resource.R.string.network_error))
            mViewModel.versionDesc.set(getString(com.wl.resource.R.string.version_is_latest))
            versionData.add(
                DelegateMultiEntity(
                    DelegateMultiEntity.CURRENT_VERSION,
                    VersionBean(BuildConfig.versionName, isLatest = true)
                )
            )
            // showMsg(getString(com.wl.resource.R.string.check_fail))
        }
        mVersionAdapter.setNewInstance(versionData)
    }

    private fun downloadAndUpdate() {
        if (!RxNetTool.isWifiConnected(requireContext())) {
            CommonNoticeDialog.Builder(requireContext())
                .setTitle(getString(com.wl.resource.R.string.mobile_download_title))
                .setContent(getString(com.wl.resource.R.string.mobile_download_hint))
                .setActionListener(object : CommonNoticeDialog.ActionListener {
                    override fun onCancel(dialog: BaseDialog?) {
                        super.onCancel(dialog)
                        dialog?.dismiss()
                    }

                    override fun onConfirm(dialog: BaseDialog?) {
                        super.onConfirm(dialog)
                        appViewModel.startDownload(mViewModel.appVersionState.value!!.data!!)
                        dialog?.dismiss()
                    }
                }).show()
        } else {
            appViewModel.startDownload(mViewModel.appVersionState.value!!.data!!)
        }
    }

    private fun requestPermission() {
        OnlyConfirmDialog.Builder(requireContext())
            .setTitle(getString(com.wl.resource.R.string.operate_hint_title))
            .setContent(getString(com.wl.resource.R.string.install_app_hint))
            .setConfirm(getString(com.wl.resource.R.string.go_settings))
            .setActionListener(object : OnlyConfirmDialog.ActioinListener {
                override fun onConfirm(dialog: BaseDialog?) {
                    startActivity(RxIntentTool.getInstallIntent(requireContext()))
                    dialog?.dismiss()
                }
            })
            .show()
    }

    inner class ProxyClick {
        fun clickCheck() {
            if (!requireContext().packageManager.canRequestPackageInstalls()) {
                requestPermission()
                return
            }
            if (true == mViewModel.hasNewVersion.value) {
                downloadAndUpdate()
                return
            }
            checkVersion()
        }
    }
}