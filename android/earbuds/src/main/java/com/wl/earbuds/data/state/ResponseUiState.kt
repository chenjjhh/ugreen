package com.wl.earbuds.data.state

/**
 * User: wanlang_dev
 * Data: 2022/12/1
 * Time: 10:01
 * Desc:
 */
data class ResponseUiState<B>(
    val isSuccess: Boolean,
    val data: B? = null
)
