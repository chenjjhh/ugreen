package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 10:05
 * Desc:
 */
data class DualDeviceState(val deviceCount: Int, val index: Int, val device: DualDevice) {
}