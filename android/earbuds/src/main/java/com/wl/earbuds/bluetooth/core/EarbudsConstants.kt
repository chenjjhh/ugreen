package com.wl.earbuds.bluetooth.core

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 16:00
 * Desc:
 */
interface EarbudsConstants {
    companion object {
        const val SWITCH_ON_VALUE = 0x01
        const val SWITCH_OFF_VALUE = 0x00

        const val STATUS_FAIL = 0x00
        const val STATUS_SUCCESS = 0x01

        const val COMMAND_VERSION = 0x01
        const val COMMAND_PRODUCT_ID = 0x02
        const val COMMAND_PRODUCT_COLOR = 0x03
        const val COMMAND_CONFIG = 0x04
        const val COMMAND_EQ = 0x05
        const val COMMAND_DUAL = 0x06
        const val COMMAND_FIND = 0x07
        const val COMMAND_GAME_MODE = 0x08
        const val COMMAND_ANC = 0x09
        const val COMMAND_GESTURE = 0x0A
        const val COMMAND_HQ_DECODE = 0x0B
        const val COMMAND_VOICE = 0x0C
        const val COMMAND_DUAL_DEVICE = 0x0D
        const val COMMAND_RECOVERY = 0x0E
        const val COMMAND_CODEC_TYPE = 0x0F
        const val COMMAND_IN_BOX = 0x10

        const val NOTIFICATION_TYPE_BATTERY = 0x01
        const val NOTIFICATION_TYPE_ANC = 0x02
        const val NOTIFICATION_TYPE_CONNECT = 0x03
        const val NOTIFICATION_TYPE_GAME_MODE = 0x04
        const val NOTIFICATION_TYPE_EQ = 0x05
        const val NOTIFICATION_TYPE_CODEC = 0x07
        const val NOTIFICATION_TYPE_FIND = 0x09

        const val TAG_VERSION = "version"
        const val TAG_CONFIG = "config"
        const val TAG_EQ = "eq"
        const val TAG_DUAL = "dual"
        const val TAG_FIND = "find"
        const val TAG_GAME_MODE = "game_mode"
        const val TAG_ANC = "anc"
        const val TAG_GESTURE = "gesture"
        const val TAG_HQ_DECODE = "hq_decode"
        const val TAG_VOICE = "voice"
        const val TAG_DUAL_DEVICE = "dual_info"
        const val TAG_RECOVERY = "recovery"
        const val TAG_CODEC= "codec"
        const val TAG_IN_BOX= "inbox"

        const val ALPHA_ON_VALUE = 1f
        const val ALPHA_OFF_VALUE = 0.4f
    }
}