package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 9:18
 * Desc:
 */
enum class Equalizer(val value: Int) {
    CLASSIC(0x00),
    BASS(0x01),
    VOCAL(0x02);

    companion object {
        @JvmStatic
        fun valueOf(value: Int): Equalizer = values().find { type -> type.value == value.toInt() } ?: CLASSIC
    }
}