package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/18
 * Time: 14:52
 * Desc:
 */
data class FailState(val commandId: Int) {
}