package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/30
 * Time: 16:55
 * Desc:
 */
data class VersionBean(
    val name: String,
    val content: String = "",
    val isLatest: Boolean = false,
    val isCurrent: Boolean = false
)
