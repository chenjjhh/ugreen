package com.wl.earbuds.bluetooth.connect

/**
 * User: wanlang_dev
 * Data: 2023/1/13
 * Time: 14:11
 * Desc:
 */
interface ConnectError {
    companion object {
        const val DEVICE_NOT_FOUND = 1
        const val CONNECT_FAIL = 2
        const val BOND_FAIL = 3
        const val CONNECT_DISCONNECT = 4
    }
}