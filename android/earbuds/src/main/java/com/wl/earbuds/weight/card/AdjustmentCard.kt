package com.wl.earbuds.weight.card

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.wl.earbuds.R
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.data.model.entity.EqualizerState
import com.wl.earbuds.data.model.entity.SwitchState
import com.wl.earbuds.utils.getEqualizerText
import com.wl.earbuds.utils.getGameModeText
import com.wl.earbuds.utils.setAnimateAlpha

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 16:36
 * Desc:
 */
class AdjustmentCard : FrameLayout, Card, OnClickListener {
    lateinit var clEqualizer: ConstraintLayout
    lateinit var clGameMode: ConstraintLayout
    lateinit var tvEqualizer: TextView
    lateinit var tvGameMode: TextView
    lateinit var gap: View
    private var mClickListener: OnAdjustmentClickListener? = null
    private var isInit: Boolean = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.layout_adjustment, this)
        clEqualizer = findViewById(R.id.cl_equalizer)
        clGameMode = findViewById(R.id.cl_game_mode)
        tvEqualizer = findViewById(R.id.tv_equalizer)
        tvGameMode = findViewById(R.id.tv_game_mode)
        gap = findViewById(R.id.gap)
        updateUI()
    }


    override fun updateUI() {
        // 点击按钮初始化后跳过
        if (!isInit) {
            initClick()
        }
        updateEqText(CurrentDeviceManager.getEqState())
        updateGameModeText(CurrentDeviceManager.getGameModeState())
        if (CurrentDeviceManager.isOnline()) {
            setAnimateAlpha(this, EarbudsConstants.ALPHA_ON_VALUE, true)
        } else {
            setAnimateAlpha(this, EarbudsConstants.ALPHA_OFF_VALUE)
        }
    }

    private fun initClick() {
        isInit = true
        clEqualizer.setOnClickListener(this)
        clGameMode.setOnClickListener(this)
    }

    fun updateEqText(equalizerState: EqualizerState) {
        tvEqualizer.apply {
            text = getEqualizerText(context, equalizerState.equalizer)
        }
    }

    fun updateGameModeText(switchState: SwitchState) {
        tvGameMode.apply {
            text = getGameModeText(context, switchState.boolean)
        }
    }

    fun setListener(listener: OnAdjustmentClickListener?) {
        mClickListener = listener
    }

    override fun onClick(v: View) {
        if (!CurrentDeviceManager.isOnline()) return
        when (v.id) {
            R.id.cl_equalizer -> {
                mClickListener?.onEqualizerClick()
            }
            R.id.cl_game_mode -> {
                mClickListener?.onGameModeClick()
            }
        }
    }

    interface OnAdjustmentClickListener {
        fun onEqualizerClick()
        fun onGameModeClick()
    }
}