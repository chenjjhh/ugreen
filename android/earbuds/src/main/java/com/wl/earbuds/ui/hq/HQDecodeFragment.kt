package com.wl.earbuds.ui.hq

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.CompoundButton.OnCheckedChangeListener
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.earbuds.switchHqDecode
import com.wl.earbuds.bluetooth.utils.BluetoothUtils
import com.wl.earbuds.data.model.entity.CodecState
import com.wl.earbuds.data.model.entity.SwitchState
import com.wl.earbuds.databinding.FragmentHQDecodeBinding
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.generateCodecText
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.util.bluetoothManager

class HQDecodeFragment : BaseFragment<HQDecodeViewModel, FragmentHQDecodeBinding>() {

    companion object {
        fun newInstance() = HQDecodeFragment()
    }

    private val mBtAdapter by lazy {
        requireContext().bluetoothManager?.adapter
    }

    private var lastClickTime = 0L

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.viewModel = mViewModel
        mDatabind.click = ProxyClick()
        initTitle()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.hq_decode)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeHqDecodeState(viewLifecycleOwner, ::onHqDecodeChange)
        CurrentDeviceManager.observeCodecState(viewLifecycleOwner, ::onCodecChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        // 区分手动操作引起的断连和关盖断连（目前为2s）
        val isNotManualSwitch = System.currentTimeMillis() - lastClickTime > 2000
        if (!isOnline && isNotManualSwitch) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun onHqDecodeChange(hqSwitchState: SwitchState) {
        mDatabind.switchBtn.isChecked = hqSwitchState.boolean
    }

    @SuppressLint("MissingPermission")
    private fun onCodecChange(codecState: CodecState) {
        if (!BluetoothUtils.areConnectPermissionsGranted(requireContext())) return
        val dualDeviceList = CurrentDeviceManager.getDualDeviceList()
        if (dualDeviceList == null || dualDeviceList.isEmpty()) return
        dualDeviceList.find { it.name == mBtAdapter?.name }?.let { dualDevice ->
            val codecBean = codecState.codecData.find {
                it.index == dualDevice.index
            }
            mViewModel.codecStr.set(
                if (codecBean != null) generateCodecText(requireContext(), codecBean.codec) else ""
            )
        }
    }

    inner class ProxyClick {
        val checkChangeListener =
            OnCheckedChangeListener { _, isChecked ->
                if (isChecked != CurrentDeviceManager.getHqState().boolean) {
                    mlog("hqDecode switch $isChecked")
                    lastClickTime = System.currentTimeMillis()
                    switchHqDecode(isChecked)
                }
            }
    }
}