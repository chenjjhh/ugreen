package com.wl.earbuds.ui.game

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.CompoundButton.OnCheckedChangeListener
import com.wl.earbuds.R
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.earbuds.switchGameMode
import com.wl.earbuds.data.model.entity.SwitchState
import com.wl.earbuds.databinding.FragmentGameModeBinding
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.ext.showMsg
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav

class GameModeFragment : BaseFragment<BaseViewModel, FragmentGameModeBinding>() {

    companion object {
        fun newInstance() = GameModeFragment()
    }

    private var lastClickTime = 0L

    private val mHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    private val updateStatusRunnable by lazy {
        Runnable { updateStatus() }
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        initTitle()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.game_mode)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeGameModeState(viewLifecycleOwner, ::onGameModeChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        if (!isOnline) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun updateStatus() {
        mDatabind.switchBtn.isChecked = CurrentDeviceManager.getGameModeState().boolean
    }

    private fun onGameModeChange(gameModeState: SwitchState) {
        mDatabind.switchBtn.isChecked = gameModeState.boolean
    }

    override fun onDestroyView() {
        mHandler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }

    inner class ProxyClick {
        val checkChangeListener =
            OnCheckedChangeListener { button, isChecked ->
                if (isChecked != CurrentDeviceManager.getGameModeState().boolean) {
                    if (isChecked && CurrentDeviceManager.getHqState().boolean) {
                        showMsg(getString(com.wl.resource.R.string.game_mode_cannot_switch))
                        button.postDelayed({
                            button.isChecked = false
                        }, 300)
                    } else {
                        mlog("gameMode switch $isChecked")
                        val currentTime = System.currentTimeMillis()
                        if (currentTime - lastClickTime > 1000) {
                            lastClickTime = currentTime
                            switchGameMode(isChecked)
                        }
                        mHandler.removeCallbacks(updateStatusRunnable)
                        mHandler.postDelayed(updateStatusRunnable, 1500)
                    }
                }
            }
    }
}