package com.wl.earbuds.ui.adapter

import android.bluetooth.BluetoothDevice
import android.view.View
import android.widget.RadioButton
import android.widget.TextView
import cn.wandersnail.bluetooth.BTManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.db.model.UgDevice
import com.wl.earbuds.R
import com.wl.earbuds.utils.ext.mlog
import me.hgj.jetpackmvvm.ext.view.invisible
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class DeviceAdapter(data: MutableList<UgDevice>) :
    BaseQuickAdapter<UgDevice, BaseViewHolder>(R.layout.item_ug_device, data) {
    private var _selectedIndex: Int = -1
    private var _targetDevice: UgDevice? = null

    var targetDevice: UgDevice?
        set(value) {
            _targetDevice = value
            if (value == null && _selectedIndex != -1) {
                notifyItemChanged(_selectedIndex)
                _selectedIndex = -1
            } else {
                notifyDataSetChanged()
            }
        }
        get() = _targetDevice

    var selectList = arrayListOf<UgDevice>()
    var isSelectMode: Boolean = false
        set(value) {
            if (value) {
                selectList.clear()
            }
            field = value
            notifyDataSetChanged()
        }

    override fun convert(holder: BaseViewHolder, item: UgDevice) {
        holder.setText(R.id.tv_name, item.name)
        if (item == targetDevice) {
            holder.setText(
                R.id.tv_connect,
                context.getString(com.wl.resource.R.string.connecting)
            )
            _selectedIndex = holder.adapterPosition
            // holder.getView<View>(R.id.tv_status).visible()
        } else if (BTManager.getInstance().getConnection(item.macAddress)?.isConnected == true) {
            holder.setTextColor(
                R.id.tv_status,
                context.getColor(com.wl.resource.R.color.black)
            )
            holder.setTextColor(
                R.id.tv_name,
                context.getColor(com.wl.resource.R.color.black)
            )
            holder.setText(
                R.id.tv_connect, context.getString(com.wl.resource.R.string.connected)
            )
            holder.setText(
                R.id.tv_status, context.getString(com.wl.resource.R.string.connected)
            )
            // holder.getView<View>(R.id.tv_status).visible()
        } else {
            holder.setTextColor(
                R.id.tv_status,
                context.getColor(com.wl.resource.R.color.text_additional_opposite)
            )
            holder.setTextColor(
                R.id.tv_name,
                context.getColor(com.wl.resource.R.color.text_additional_opposite)
            )
            holder.setText(
                R.id.tv_connect, context.getString(com.wl.resource.R.string.connect)
            )
            holder.setText(
                R.id.tv_status, context.getString(com.wl.resource.R.string.disconnected)
            )
            // holder.getView<View>(R.id.tv_status).visible()
        }
        if (isSelectMode) {
            holder.getView<RadioButton>(R.id.radio_btn).let {
                it.isChecked = selectList.contains(item)
                it.visible()
            }
            holder.getView<TextView>(R.id.tv_connect).invisible()
        } else {
            holder.getView<RadioButton>(R.id.radio_btn).invisible()
            holder.getView<TextView>(R.id.tv_connect).visible()
        }
    }
}