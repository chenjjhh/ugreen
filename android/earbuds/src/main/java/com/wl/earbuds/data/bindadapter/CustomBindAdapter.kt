package com.wl.earbuds.data.bindadapter

import android.annotation.SuppressLint
import android.os.SystemClock
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.wl.earbuds.utils.ext.click
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.weight.checkable.CheckableConstraintLayout
import me.hgj.jetpackmvvm.ext.view.textString

/**
 * 作者　: hegaojian
 * 时间　: 2019/12/23
 * 描述　: 自定义 BindingAdapter
 */
object CustomBindAdapter {

    var lastClickTime = 0L
    var interval = 400L

    @BindingAdapter("singleClick", requireAll = false)
    @JvmStatic
    fun setSingleClick(view: View, listener: View.OnClickListener?) {
        mlog("listener: $listener")
        view.click {
            listener?.onClick(it)
        }
        /*view.singleClick(owner) {
            listener.onClick(it)
        }*/
    }

    @BindingAdapter(value = ["isConstraintCheck"])
    @JvmStatic
    fun setConstraintCheck(layout: CheckableConstraintLayout, isChecked: Boolean) {
        layout.isChecked = isChecked
    }

    @BindingAdapter(value = ["checkChange"])
    @JvmStatic
    fun checkChange(checkbox: CheckBox, listener: CompoundButton.OnCheckedChangeListener) {
        checkbox.setOnCheckedChangeListener(listener)
    }

    @SuppressLint("ClickableViewAccessibility")
    @BindingAdapter(value = ["checkClick"])
    @JvmStatic
    fun checkClick(compoundButton: CompoundButton, action: View.OnTouchListener) {
        compoundButton.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                action.onTouch(v, event)
            }
            return@setOnTouchListener true
        }
    }

    @BindingAdapter(value = ["showPwd"])
    @JvmStatic
    fun showPwd(view: EditText, boolean: Boolean) {
        if (boolean) {
            view.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        } else {
            view.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        }
        view.setSelection(view.textString().length)
    }

    @BindingAdapter(value = ["imageUrl"])
    @JvmStatic
    fun imageUrl(view: ImageView, url: String) {
        Glide.with(view.context.applicationContext)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(view)
    }

    @BindingAdapter(value = ["circleImageUrl"])
    @JvmStatic
    fun circleImageUrl(view: ImageView, url: String) {
        Glide.with(view.context.applicationContext)
            .load(url)
            .apply(RequestOptions.bitmapTransform(CircleCrop()))
            .transition(DrawableTransitionOptions.withCrossFade(500))
            .into(view)
    }

    /*@BindingAdapter(value = ["colorCircleViewColor"])
    @JvmStatic
    fun colorCircleViewColor(view: MyColorCircleView, color: Int) {
        view.setView(color)
    }*/

    @BindingAdapter(value = ["afterTextChanged"])
    @JvmStatic
    fun EditText.afterTextChanged(action: (String) -> Unit) {
        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                action(s.toString())
            }
        })
    }

    @BindingAdapter(value = ["noRepeatClick"])
    @JvmStatic
    fun setOnClick(view: View, clickListener: () -> Unit) {
        val mHits = LongArray(2)
        view.setOnClickListener {
            System.arraycopy(mHits, 1, mHits, 0, mHits.size - 1)
            mHits[mHits.size - 1] = SystemClock.uptimeMillis()
            if (mHits[0] < SystemClock.uptimeMillis() - 500) {
                clickListener.invoke()
            }
        }
    }

}