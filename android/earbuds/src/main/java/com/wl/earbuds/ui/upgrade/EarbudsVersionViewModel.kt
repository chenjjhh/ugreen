package com.wl.earbuds.ui.upgrade

import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.earbuds.BuildConfig
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.data.model.entity.EarbudsVersionBean
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.network.apiService
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.callback.databind.StringObservableField
import me.hgj.jetpackmvvm.ext.request

class EarbudsVersionViewModel : BaseViewModel() {
    var versionDesc = StringObservableField("")
    var hasNewVersion = UnPeekLiveData<Boolean>(false)
    val versionState: UnPeekLiveData<ResponseUiState<EarbudsVersionBean>> = UnPeekLiveData()

    fun getEarbudsVersion() {
        request({
            apiService.getEarbudsVersion()
        }, {
            versionState.value = ResponseUiState(true, it)
            hasNewVersion.value = CurrentDeviceManager.getVersionState().getVersion() != it.versionName
        }, {
            versionState.value = ResponseUiState(false)
        })
    }
}