package com.wl.earbuds.bluetooth.connect

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.MutableLiveData
import cn.wandersnail.bluetooth.BTManager
import cn.wandersnail.bluetooth.ConnectCallback
import cn.wandersnail.bluetooth.Connection
import cn.wandersnail.bluetooth.EventObserver
import cn.wandersnail.commons.poster.RunOn
import cn.wandersnail.commons.poster.ThreadMode
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.bluetooth.earbuds.EarbudsListener
import com.wl.earbuds.bluetooth.earbuds.ProtocolManager
import com.wl.earbuds.bluetooth.ota.OtaListener
import com.wl.earbuds.bluetooth.ota.OtaManager
import com.wl.earbuds.bluetooth.ota.UpgradeState
import com.wl.earbuds.bluetooth.task.TaskManager
import com.wl.earbuds.bluetooth.task.TaskManagerWrapper
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.bluetooth.utils.formatCommandData
import com.wl.earbuds.data.model.entity.*
import com.wl.earbuds.data.model.eumn.Find
import com.wl.earbuds.utils.ext.mlog
import java.util.*

/**
 * User: wanlang_dev
 * Data: 2022/11/16
 * Time: 18:13
 * Desc:
 */
class DeviceManager(val mDevice: BluetoothDevice) : IDeviceManager, EarbudsListener, OtaListener {

    companion object {
        val SPP_UUID = UUID.fromString("66666666-6666-6666-6666-666666666666")
        // val SERVICE_UUID = UUID.fromString("66666666-6666-6666-6666-666666666666")
        // val WRITE_UUID = UUID.fromString("77777777-7777-7777-7777-777777777777")
        // val NOTIFY_UUID = UUID.fromString("77777777-7777-7777-7777-777777777777")
    }

    // 当前查看的设备
    /*private var _currentDevice: BluetoothDevice? = null

    var currentDevice: BluetoothDevice?
        set(value) {
            _currentDevice = value
        }
        get() = _currentDevice*/
    private val mOtaManager by lazy {
        OtaManager(this, taskManager)
    }

    private val _onlineState: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _versionState: MutableLiveData<VersionState> = MutableLiveData(VersionState())
    private val _ancState: MutableLiveData<AncState> = MutableLiveData(AncState())
    private val _eqState: MutableLiveData<EqualizerState> = MutableLiveData(EqualizerState())
    private val _dualState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _gameModeState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _hqDecodeState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _voiceState: MutableLiveData<VoiceState> = MutableLiveData(VoiceState())
    private val _gestureState: MutableLiveData<GestureState> = MutableLiveData(GestureState())
    private val _leftFindState: MutableLiveData<FindState> =
        MutableLiveData(FindState(Find.FIND_LEFT_CLOSE))
    private val _rightFindState: MutableLiveData<FindState> =
        MutableLiveData(FindState(Find.FIND_RIGHT_CLOSE))
    private val _batteryState: MutableLiveData<BatteryState> = MutableLiveData(BatteryState())
    private val _dualDeviceState: MutableLiveData<List<DualDevice>> = MutableLiveData()
    private val _codecState: MutableLiveData<CodecState> = MutableLiveData()
    private val _inBoxState: MutableLiveData<SwitchState> = MutableLiveData(SwitchState(false))
    private val _failState: MutableLiveData<FailState> = MutableLiveData()
    private val _upgradeState: MutableLiveData<UpgradeState> = MutableLiveData()

    private var _connection: Connection? = null
    private lateinit var earbudsManager: ProtocolManager
    private lateinit var taskManager: TaskManager
    private var lastGestureState: GestureState = GestureState()
    private var dualDeviceList = mutableListOf<DualDevice>()

    private var syncConfig = false
    private var syncVersion = false
    private var syncDualDevice = false

    var connection: Connection?
        set(value) {
            _connection = value
        }
        get() = _connection

    private val eventObserver: EventObserver by lazy {
        object : EventObserver {
            @RunOn(ThreadMode.MAIN)
            override fun onConnectionStateChanged(device: BluetoothDevice, state: Int) {
                super.onConnectionStateChanged(device, state)
                if (device != mDevice) return
                when (state) {
                    Connection.STATE_CONNECTED -> {
                        updateOnlineState(true)
                        getConfig()
                    }
                    Connection.STATE_DISCONNECTED -> {
                        updateOnlineState(false)
                    }
                    Connection.STATE_RELEASED -> {
                        release()
                        ClassicManager.removeDeviceManager(device.address)
                    }
                }
            }

            override fun onRead(device: BluetoothDevice, value: ByteArray) {
                super.onRead(device, value)
                if (device != mDevice) return
                earbudsManager.parseData(value)
            }

            override fun onWrite(
                device: BluetoothDevice,
                tag: String,
                value: ByteArray,
                result: Boolean
            ) {
                if (device != mDevice) return
                super.onWrite(device, tag, value, result)
                mlog("write: ${BytesUtils.getHexadecimalStringFromBytes(value)}}")
            }
        }
    }

    private val mConnectCallback by lazy {
        object : ConnectCallback {
            override fun onSuccess() {
                mlog("mConnectCallback onSuccess")
            }

            override fun onFail(errMsg: String, e: Throwable?) {
                mlog("mConnectCallback onFail $errMsg $e")
            }
        }
    }

    init {
        taskManager = TaskManagerWrapper()
        BTManager.getInstance().registerObserver(eventObserver)
        earbudsManager = ProtocolManager(this, taskManager)
        /*if (connection?.isConnected == true) {
            updateOnlineState(true)
            getConfig()
            getVersion()
            getDualDevice()
        }*/
    }

    fun release() {
        BTManager.getInstance().unregisterObserver(eventObserver)
    }

    fun isOnline(): Boolean {
        return _connection?.isConnected ?: false
    }

    fun isUpgrade(): Boolean {
        return mOtaManager.isUpgrading()
    }

    fun syncState() {
        updateOnlineState(_onlineState.value!!)
        updateVersionState(_versionState.value!!)
        updateAncState(_ancState.value!!)
        updateEqState(_eqState.value!!)
        updateLeftFindState(_leftFindState.value!!)
        updateRightFindState(_rightFindState.value!!)
        updateDualState(_dualState.value!!)
        updateGameModeState(_gameModeState.value!!)
        updateHqDecodeState(_hqDecodeState.value!!)
        updateVoiceState(_voiceState.value!!)
        updateGestureState(_gestureState.value!!)
        updateBatteryState(_batteryState.value!!)
        if (_dualDeviceState.value != null) {
            updateDualDeviceState(_dualDeviceState.value!!)
        }
        if (_codecState.value != null) {
            updateCodecState(_codecState.value!!)
        }
        if (_failState.value != null) {
            updateFailState(_failState.value!!)
        }
        if (_upgradeState.value != null) {
            updateUpgradeState(_upgradeState.value!!)
        }
    }

    private fun updateOnlineState(isOnline: Boolean) {
        CurrentDeviceManager.updateOnlineState(mDevice, isOnline)
        _onlineState.postValue(isOnline)
    }

    private fun updateVersionState(versionState: VersionState) {
        CurrentDeviceManager.updateVersionState(mDevice, versionState)
        _versionState.postValue(versionState)
    }

    private fun updateAncState(ancState: AncState) {
        CurrentDeviceManager.updateAncState(mDevice, ancState)
        _ancState.postValue(ancState)
    }

    private fun updateEqState(eqState: EqualizerState) {
        CurrentDeviceManager.updateEqState(mDevice, eqState)
        _eqState.postValue(eqState)
    }

    private fun updateLeftFindState(findState: FindState) {
        CurrentDeviceManager.updateLeftFindState(mDevice, findState)
        _leftFindState.postValue(findState)
    }

    private fun updateRightFindState(findState: FindState) {
        CurrentDeviceManager.updateRightFindState(mDevice, findState)
        _rightFindState.postValue(findState)
    }

    private fun updateDualState(dualState: SwitchState) {
        CurrentDeviceManager.updateDualState(mDevice, dualState)
        _dualState.postValue(dualState)
    }

    private fun updateDualHintState(dualHintState: SwitchState) {
        CurrentDeviceManager.updateDualHintState(mDevice, dualHintState)
    }

    private fun updateGameModeState(gameModeState: SwitchState) {
        CurrentDeviceManager.updateGameModeState(mDevice, gameModeState)
        _gameModeState.postValue(gameModeState)
    }

    private fun updateHqDecodeState(hqDecodeState: SwitchState) {
        CurrentDeviceManager.updateHqDecodeState(mDevice, hqDecodeState)
        _hqDecodeState.postValue(hqDecodeState)
    }

    private fun updateVoiceState(voiceState: VoiceState) {
        CurrentDeviceManager.updateVoiceState(mDevice, voiceState)
        _voiceState.postValue(voiceState)
    }

    private fun updateGestureState(gestureState: GestureState) {
        CurrentDeviceManager.updateGestureState(mDevice, gestureState)
        _gestureState.postValue(gestureState)
    }

    private fun updateBatteryState(batteryState: BatteryState) {
        CurrentDeviceManager.updateBatteryState(mDevice, batteryState)
        if (!batteryState.isLeftOn()) {
            updateLeftFindState(FindState(Find.FIND_LEFT_CLOSE))
        }
        if (!batteryState.isRightOn()) {
            updateRightFindState(FindState(Find.FIND_RIGHT_CLOSE))
        }
        _batteryState.postValue(batteryState)
    }

    private fun updateDualDeviceState(dualDeviceList: List<DualDevice>) {
        CurrentDeviceManager.updateDualDeviceState(mDevice, dualDeviceList)
        _dualDeviceState.postValue(dualDeviceList)
    }

    private fun updateCodecState(codecState: CodecState) {
        CurrentDeviceManager.updateCodecState(mDevice, codecState)
        _codecState.postValue(codecState)
    }

    private fun updateInBoxState(inBoxState: SwitchState) {
        CurrentDeviceManager.updateInBoxState(mDevice, inBoxState)
        _inBoxState.postValue(inBoxState)

    }

    private fun updateFailState(failState: FailState) {
        CurrentDeviceManager.updateFailState(mDevice, failState)
        _failState.postValue(failState)
    }

    private fun updateUpgradeState(upgradeState: UpgradeState) {
        CurrentDeviceManager.updateUpgradeState(mDevice, upgradeState)
        _upgradeState.postValue(upgradeState)
    }

    fun getConfig() {
        syncConfig = true
        sendCommand(
            EarbudsConstants.COMMAND_CONFIG,
            ByteArray(0),
            EarbudsConstants.TAG_CONFIG
        )
    }

    fun getVersion() {
        syncVersion = true
        sendCommand(
            EarbudsConstants.COMMAND_VERSION,
            ByteArray(0),
            EarbudsConstants.TAG_VERSION
        )
    }

    fun getDualDevice() {
        syncDualDevice = true
        sendCommand(
            EarbudsConstants.COMMAND_DUAL_DEVICE,
            ByteArray(0),
            EarbudsConstants.TAG_DUAL_DEVICE
        )
    }

    fun getCodec() {
        sendCommand(
            EarbudsConstants.COMMAND_CODEC_TYPE,
            ByteArray(0),
            EarbudsConstants.TAG_CODEC
        )
    }

    fun setEq(commandId: Int, value: Int) {
        sendCommand(
            EarbudsConstants.COMMAND_EQ,
            ByteArray(1).apply {
                BytesUtils.setUINT8(value, this, 0)
            },
            EarbudsConstants.TAG_CONFIG
        )
    }

    fun startOta(otaBean: OtaBean) {
        if (_connection == null) return
        taskManager.runInBackground { mOtaManager.startOta(otaBean) }
    }

    fun sendCommand(commandId: Int, byteArray: ByteArray, tag: String = "") {
        if (_connection == null) return
        _connection?.write(tag, formatCommandData(commandId, byteArray), null)
    }


    override fun connect() {
        connection?.connect(SPP_UUID, mConnectCallback)
    }

    override fun disconnect() {
        connection?.disconnect()
    }

    override fun onConfig(configState: ConfigState) {
        if (syncConfig) {
            getVersion()
            syncConfig = false
        }
        mlog("configState: $configState")
        updateBatteryState(configState.batteryState)
        updateAncState(configState.ancState)
        updateEqState(configState.eqState)
        updateDualState(configState.dualState)
        updateGameModeState(configState.gameModeState)
        updateHqDecodeState(configState.hqDecodeState)
        updateGestureState(configState.gestureState)
        updateVoiceState(configState.voiceState)
        updateLeftFindState(configState.leftFindState)
        updateRightFindState(configState.rightFindState)
    }

    override fun onBattery(batteryState: BatteryState) {
        mlog("onBattery: $batteryState")
        updateBatteryState(batteryState)
    }

    override fun onVersion(versionState: VersionState) {
        mlog("onVersion: $versionState")
        updateVersionState(versionState)
        if (syncVersion) {
            getDualDevice()
            syncVersion = false
        }
    }

    override fun onEq(eqState: EqualizerState) {
        mlog("onEq: $eqState")
        updateEqState(eqState)
    }

    override fun onFind(findState: FindState) {
        when (findState.find) {
            Find.FIND_LEFT_ON,
            Find.FIND_LEFT_CLOSE -> {
                updateLeftFindState(findState)
            }
            Find.FIND_RIGHT_ON,
            Find.FIND_RIGHT_CLOSE,
            -> {
                updateRightFindState(findState)
            }
        }
    }

    override fun onDual(dualState: SwitchState) {
        mlog("onDual: $dualState")
        updateDualState(dualState)
    }

    override fun onDualHint(dualHintState: SwitchState) {
        updateDualHintState(dualHintState)
    }

    override fun onGameMode(gameModeState: SwitchState) {
        mlog("onGameMode: $gameModeState")
        updateGameModeState(gameModeState)
    }

    override fun onAnc(ancState: AncState) {
        mlog("onAnc: $ancState")
        updateAncState(ancState)
    }

    override fun onGesture(gestureState: GestureState) {
        mlog("onGesture: $gestureState")
        updateGestureState(gestureState)
    }

    override fun onHqDecode(switchState: SwitchState) {
        mlog("onHqDecode: $switchState")
        updateHqDecodeState(switchState)
    }

    override fun onVoice(voiceState: VoiceState) {
        mlog("onVoice: $voiceState")
        updateVoiceState(voiceState)
    }

    override fun onDualDevice(dualDeviceState: DualDeviceState) {
        mlog("onDualDevice: $dualDeviceState")
        if (dualDeviceState.index == 1) {
            dualDeviceList.clear()
            dualDeviceList.add(dualDeviceState.device)
        } else {
            dualDeviceList.add(dualDeviceState.device)
        }
        if (dualDeviceState.deviceCount == dualDeviceState.index) {
            // 获取codec
            if (syncDualDevice) {
                getCodec()
                syncDualDevice = false
            }
        }
        val result = mutableListOf<DualDevice>()
        result.addAll(dualDeviceList)
        updateDualDeviceState(result)
    }

    override fun onCodec(codecState: CodecState) {
        updateCodecState(codecState)
    }

    override fun onInBox(inBoxState: SwitchState) {
        updateInBoxState(inBoxState)
    }

    override fun onCommandFail(commandId: Int) {
        mlog("onCommandFail: $commandId")
        updateFailState(FailState(commandId = commandId))
    }

    override fun getOtaManager(): OtaManager {
        return mOtaManager
    }

    override fun onStartUpgrade() {
        updateUpgradeState(UpgradeState.Start())
    }

    override fun sendOtaPacket(data: ByteArray) {
        if (_connection == null) return
        _connection?.write("ota", data, null)
    }

    override fun updateProgress(progress: Int) {
        updateUpgradeState(UpgradeState.Upgrading().apply {
            this.progress = progress
        })
    }

    override fun onFail() {
        updateUpgradeState(UpgradeState.Fail())
    }

    override fun onSuccess() {
        updateUpgradeState(UpgradeState.Success())
    }
}