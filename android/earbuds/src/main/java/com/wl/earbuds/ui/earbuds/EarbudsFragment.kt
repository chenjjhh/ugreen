package com.wl.earbuds.ui.earbuds

import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import cn.wandersnail.bluetooth.BTManager
import com.gyf.immersionbar.ImmersionBar
import com.wanlang.base.BaseDialog
import com.wl.db.model.UgDevice
import com.wl.earbuds.R
import com.wl.earbuds.app.appViewModel
import com.wl.earbuds.app.deviceManagerViewModel
import com.wl.earbuds.app.settings.ConfigUtils
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.*
import com.wl.earbuds.bluetooth.earbuds.setAnc
import com.wl.earbuds.data.model.entity.*
import com.wl.earbuds.data.model.eumn.Anc
import com.wl.earbuds.data.model.eumn.AncLevel
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.databinding.FragmentEarbudsBinding
import com.wl.earbuds.ui.device.DeviceScanActivity
import com.wl.earbuds.ui.dialog.AncBottomDialog
import com.wl.earbuds.ui.dialog.CommonNoticeDialog
import com.wl.earbuds.ui.popup.MenuPopup
import com.wl.earbuds.utils.Constants
import com.wl.earbuds.utils.ext.*
import com.wl.earbuds.weight.card.*
import me.hgj.jetpackmvvm.ext.lifecycle.KtxActivityManger
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav
import me.hgj.jetpackmvvm.ext.view.gone

class EarbudsFragment : BaseFragment<EarbudsViewModel, FragmentEarbudsBinding>(),
    ConnectListener {

    companion object {
        fun newInstance() = EarbudsFragment()
    }

    private var mReconnectDevice: UgDevice? = null
    private var mIsDiscovery = false
    private var mRemoteVersionGet = false
    private var mNeedReload = false
    private var mReloadId = -1L
    private var mAncDialog: AncBottomDialog? = null
    private var mUpgradeDialog: CommonNoticeDialog.Builder? = null
    private var mPopupMenu: MenuPopup.Builder? = null

    private var lastClickTime = 0L
    private val mHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    private val mConnectManager by lazy {
        ConnectManager.getInstance(requireContext().applicationContext)
    }

    private val productCard by lazy {
        mDatabind.cardProduct
    }

    private val updateAncRunnable by lazy {
        Runnable { updateAncAfterSwitch() }
    }

    private val batteryAndTipsCard by lazy {
        mDatabind.cardBatteryAndTip.apply {
            setOnTipClickListener(object : BatteryAndTipsCard.OnTipClickListener {
                override fun onReconnectClick() {
                    if (mConnectManager.checkBluetooth(requireActivity())) {
                        mReconnectDevice = CurrentDeviceManager.ugDevice!!
                        mConnectManager.connectUgDevice(mReconnectDevice!!)
                        isConnect(true)
                    }
                }
            })
        }
    }

    private val ancCard by lazy {
        mDatabind.cardAnc.apply {
            setListener(object : AncCard.OnAncClickListener {
                override fun onAncClick() {
                    val currentTime = System.currentTimeMillis()
                    if (currentTime - lastClickTime > 1000) {
                        lastClickTime = currentTime
                        when (CurrentDeviceManager.getAncState().level) {
                            AncLevel.ANC_DEEP -> setAnc(Anc.ANC_ON, AncLevel.ANC_DEEP)
                            AncLevel.ANC_MEDIUM -> setAnc(Anc.ANC_ON, AncLevel.ANC_MEDIUM)
                            AncLevel.ANC_LIGHT -> setAnc(Anc.ANC_ON, AncLevel.ANC_LIGHT)
                        }
                    } else {
                        updateAnc(AncState(Anc.ANC_ON, CurrentDeviceManager.getAncState().level))
                    }
                    mHandler.removeCallbacks(updateAncRunnable)
                    mHandler.postDelayed(updateAncRunnable, 1500)
                }

                override fun onCloseClick() {
                    val currentTime = System.currentTimeMillis()
                    if (currentTime - lastClickTime > 1000) {
                        lastClickTime = currentTime
                        setAnc(Anc.ANC_CLOSE, CurrentDeviceManager.getAncState().level)
                    } else {
                        updateAnc(AncState(Anc.ANC_CLOSE, CurrentDeviceManager.getAncState().level))
                    }
                    mHandler.removeCallbacks(updateAncRunnable)
                    mHandler.postDelayed(updateAncRunnable, 1500)
                }

                override fun onTransparentClick() {
                    val currentTime = System.currentTimeMillis()
                    if (currentTime - lastClickTime > 1000) {
                        lastClickTime = currentTime
                        setAnc(Anc.ANC_TRANS, CurrentDeviceManager.getAncState().level)
                    } else {
                        updateAnc(AncState(Anc.ANC_TRANS, CurrentDeviceManager.getAncState().level))
                    }
                    mHandler.removeCallbacks(updateAncRunnable)
                    mHandler.postDelayed(updateAncRunnable, 1500)
                }

                override fun onAncMethodClick() {
                    if (mAncDialog == null) {
                        mAncDialog = AncBottomDialog(
                            requireContext(),
                            CurrentDeviceManager.getAncState().level
                        )
                            .setListener(object : AncBottomDialog.OnListener {
                                override fun onCancel(baseDialog: BaseDialog) {
                                    baseDialog.dismiss()
                                }

                                override fun onConfirm(baseDialog: BaseDialog, ancLevel: AncLevel) {
                                    when (ancLevel) {
                                        AncLevel.ANC_DEEP -> setAnc(Anc.ANC_ON, AncLevel.ANC_DEEP)
                                        AncLevel.ANC_MEDIUM -> setAnc(
                                            Anc.ANC_ON,
                                            AncLevel.ANC_MEDIUM
                                        )
                                        AncLevel.ANC_LIGHT -> setAnc(Anc.ANC_ON, AncLevel.ANC_LIGHT)
                                    }
                                    baseDialog.dismiss()
                                }
                            })
                    } else {
                        mAncDialog?.updateAncLevel(CurrentDeviceManager.getAncState().level)
                    }
                    mAncDialog?.show()
                }
            })
        }
    }

    private val adjustmentCard by lazy {
        mDatabind.cardAdjustment.apply {
            setListener(
                object : AdjustmentCard.OnAdjustmentClickListener {
                    override fun onEqualizerClick() {
                        mDatabind.click?.goEqualizer()
                    }

                    override fun onGameModeClick() {
                        mDatabind.click?.goGameMode()
                    }
                })
        }
    }

    private val menuCard by lazy {
        mDatabind.cardMenu.apply {
            setMenuClickListener(object : MenuCard.OnMenuClickListener {
                override fun onDeviceDualClick() {
                    mDatabind.click?.goEarbudsDual()
                }

                override fun onFindClick() {
                    mDatabind.click?.goFind()
                }

                override fun onHQDecodeClick() {
                    mDatabind.click?.goHQDecode()
                }

                override fun onUpgradeClick() {
                    mDatabind.click?.goEarbudsVersion()
                }
            })
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        mDatabind.click = ProxyClick()
        initImmersionBar()
        if (savedInstanceState != null && CurrentDeviceManager.ugDevice == null) {
            val deviceID = savedInstanceState.getLong(Constants.DEVICE_ID, -1)
            if (deviceID == -1L) {
                /*startActivity(
                    Intent(
                        requireContext(),
                        DeviceListActivity::class.java
                    )
                )*/
                activity?.finish()
            } else {
                ConnectManager.getInstance(requireContext().applicationContext).initialize()
                deviceManagerViewModel.getDeviceList()
                mNeedReload = true
                mReloadId = deviceID
            }
        }
        initTitle()
        initCard()
    }

    private fun initImmersionBar() {
        ImmersionBar.with(this)
            .transparentStatusBar()
            .fitsSystemWindows(true)
            .statusBarDarkFont(true)// 默认状态栏字体颜色为黑色
            //            .keyboardEnable(true)  // 解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
            .init()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initCloseWithMenu(CurrentDeviceManager.getName(), {
            activity?.finish()
            //startActivity(Intent(requireContext(), DeviceListActivity::class.java))
        }) {
            if (mPopupMenu == null) {
                mPopupMenu = MenuPopup.Builder(requireContext())
                    .setXOffset(requireContext().dp2px(-102f))
                    .setYOffset(requireContext().dp2px(22f))
                    .setOnListener { position ->
                        when (position) {
                            Constants.MENU_DEVICE_SETTINGS -> {
                                if (CurrentDeviceManager.isOnline()) {
                                    safeNav {
                                        nav().navigate(R.id.action_home_to_earbudsSettings)
                                    }
                                } else {
                                    showMsg(getString(com.wl.resource.R.string.device_not_connect))
                                }
                            }
                            Constants.MENU_ADD_DEVICE -> {
                                startActivity(
                                    Intent(
                                        requireContext(),
                                        DeviceScanActivity::class.java
                                    )
                                )
                            }
                            Constants.MENU_DISCONNECT -> {
                                mConnectManager.disconnectDevice(CurrentDeviceManager.getAddress())
                                CurrentDeviceManager.disconnect()
                            }
                            Constants.MENU_DELETE_DEVICE -> {
                                val ugDevice = CurrentDeviceManager.ugDevice
                                if (ugDevice != null) {
                                    mConnectManager.disconnectDevice(CurrentDeviceManager.getAddress())
                                    CurrentDeviceManager.disconnect()
                                    appViewModel.deleteUgDevice(ugDevice!!)
                                    ClassicManager.removeDeviceManager(ugDevice.macAddress)
                                    CurrentDeviceManager.ugDevice = null
                                    CurrentDeviceManager.device = null
                                    if (KtxActivityManger.isLast) {
                                       /* startActivity(
                                            Intent(
                                                requireContext(),
                                                DeviceListActivity::class.java
                                            )
                                        )*/
                                        activity?.finish()
                                    } else {
                                        requireActivity().onBackPressed()
                                    }
                                }
                            }
                            Constants.MENU_MANUAL -> {
                                safeNav {
                                    nav().navigate(
                                        R.id.action_home_to_protocolWeb,
                                        Bundle().apply {
                                            putInt(
                                                Constants.PROTOCOL_TYPE,
                                                Constants.USER_MANUAL
                                            )
                                        })
                                }
                            }
                        }
                    }
            }
            mPopupMenu!!.showAsDropDown(it)
        }
    }

    private fun initCard() {
        ancCard
        adjustmentCard.apply {
            if (!ConfigUtils.eqEnable && !ConfigUtils.gameModeEnable) {
                this.gone()
                return@apply
            }
            if (!ConfigUtils.eqEnable) {
                clEqualizer.gone()
                gap.gone()
            }
            if (!ConfigUtils.gameModeEnable) {
                clGameMode.gone()
                gap.gone()
            }
        }
        menuCard.apply {
            if (
                !ConfigUtils.dualEnable && !ConfigUtils.findEnable &&
                !ConfigUtils.hqEnable && !ConfigUtils.upgradeEnable
            ) {
                this.gone()
                return@apply
            }
            if (!ConfigUtils.dualEnable) {
                clDeviceDual.gone()
            }
            if (!ConfigUtils.findEnable) {
                clFindEarbuds.gone()
            }
            if (!ConfigUtils.hqEnable) {
                clHqDecode.gone()
            }
            if (!ConfigUtils.upgradeEnable) {
                clHardwireUpgrade.gone()
            }
        }
    }

    private fun disconnectDevice() {
        CurrentDeviceManager.disconnect()
        requireActivity().onBackPressed()
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeUgDeviceState(viewLifecycleOwner, ::onUgDeviceChange)
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeBatteryState(viewLifecycleOwner, ::onBatteryChange)
        CurrentDeviceManager.observeAncState(viewLifecycleOwner, ::onAncChange)
        CurrentDeviceManager.observeEqState(viewLifecycleOwner, ::onEqualizerChange)
        CurrentDeviceManager.observeGameModeState(viewLifecycleOwner, ::onGameModeChange)
        CurrentDeviceManager.observeVersionState(viewLifecycleOwner, ::onVersionChange)
        mViewModel.deviceState.observe(viewLifecycleOwner, ::onReload)
        mViewModel.versionState.observe(viewLifecycleOwner, ::onVersion)
    }

    override fun initData() {
        super.initData()
        if (mNeedReload) {
            mViewModel.reloadDevice(mReloadId)
            mNeedReload = false
        }
    }

    private fun onReload(ugDevice: UgDevice) {
        CurrentDeviceManager.ugDevice = ugDevice
//        mDatabind.cardBatteryAndTip.performReconnect()
    }

    private fun onUgDeviceChange(ugDevice: UgDevice?) {
        mDatabind.toolbar.toolbar.updateTitle(ugDevice?.name ?: "")
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("isOnline $isOnline")
        batteryAndTipsCard.updateUI()
        ancCard.updateUI()
        menuCard.updateUI()
        adjustmentCard.updateUI()
        if (!isOnline) {
            mAncDialog?.dismiss()
        }
        if (isOnline && !mRemoteVersionGet) {
            mViewModel.getEarbudsVersion()
        }
    }

    private fun onBatteryChange(batteryState: BatteryState) {
        mlog("batteryState $batteryState")
        batteryAndTipsCard.updateBattery(batteryState)
    }

    private fun onAncChange(ancState: AncState) {
        ancCard.updateAnc(ancState)
    }

    private fun updateAncAfterSwitch() {
        mlog("updateAncAfterSwitch")
        ancCard.updateAnc(CurrentDeviceManager.getAncState())
    }

    private fun onEqualizerChange(equalizerState: EqualizerState) {
        adjustmentCard.updateEqText(equalizerState)
    }

    private fun onGameModeChange(gameModeState: SwitchState) {
        adjustmentCard.updateGameModeText(gameModeState)
    }

    private fun onVersionChange(versionState: VersionState) {
        updateDataVersionContent()
    }

    private fun onVersion(uiState: ResponseUiState<EarbudsVersionBean>) {
        mRemoteVersionGet = true
        updateDataVersionContent()
    }

    private fun updateDataVersionContent() {
        val versionStr = CurrentDeviceManager.getVersionState().getVersion()
        val versionUiState = mViewModel.versionState.value
        if (versionStr.isEmpty()) return
        if (versionUiState == null || !versionUiState!!.isSuccess) {
            menuCard.updateVersionText(versionStr)
            return
        }
        if (versionUiState!!.isSuccess) {
            if (versionStr != versionUiState.data!!.versionName) {
                menuCard.updateVersionText(
                    requireContext().getString(
                        com.wl.resource.R.string.app_find_new_version,
                        versionStr
                    ), true
                )
                showNewVersionDialog()
            } else {
                menuCard.updateVersionText(
                    versionStr
                            + requireContext().getString(com.wl.resource.R.string.current_verion_is_latest)
                )
            }
        }
    }

    private fun showNewVersionDialog() {
        if (appViewModel.mIgnoreUpgrade) return
        appViewModel.mIgnoreUpgrade = true
        val version = mViewModel.versionState.value!!.data!!
        if (mUpgradeDialog == null) {
            mUpgradeDialog = CommonNoticeDialog.Builder(requireContext())
                .setContent(getString(com.wl.resource.R.string.new_version_request, version.fileSize))
                .setCancelText(getString(com.wl.resource.R.string.ignore))
                .setConfirmText(getString(com.wl.resource.R.string.upgrade))
                .setActionListener(object : CommonNoticeDialog.ActionListener {
                    override fun onCancel(dialog: BaseDialog?) {
                        super.onCancel(dialog)
                        dialog?.dismiss()
                    }

                    override fun onConfirm(dialog: BaseDialog?) {
                        super.onConfirm(dialog)
                        safeNav {
                            nav().navigate(R.id.action_home_to_earbudsUpgrade, Bundle().apply {
                                putParcelable(
                                    Constants.EARBUDS_VERSION,
                                    version
                                )
                            })
                        }
                        dialog?.dismiss()
                    }
                })
        }
        val upgradeDialog = mUpgradeDialog!!
        if (!upgradeDialog.isShowing()) {
            upgradeDialog.show()
        }
    }

    override fun onDeviceFound(device: BluetoothDevice) {
        super.onDeviceFound(device)
        if (mReconnectDevice?.macAddress == device.address) {
            BTManager.getInstance().createBond(device)
        }
    }

    override fun onDeviceBond(device: BluetoothDevice) {
        super.onDeviceBond(device)
        if (mReconnectDevice?.macAddress != device.address) return
        ClassicManager.connect(device)
    }

    override fun onDeviceConnected(device: BluetoothDevice) {
        super.onDeviceConnected(device)
        if (CurrentDeviceManager.getAddress() != device.address) return
        CurrentDeviceManager.device = device
        resetConnectStatus()
    }

    override fun onPairRefused(device: BluetoothDevice) {
        super.onPairRefused(device)
        if (mReconnectDevice?.macAddress != device.address) return
        showMsg(getString(com.wl.resource.R.string.connect_fail))
        resetConnectStatus()
    }

    private fun resetConnectStatus() {
        batteryAndTipsCard.isConnect(false)
        mReconnectDevice = null
    }

    override fun onDiscoveryStart() {
        super.onDiscoveryStart()
        mIsDiscovery = true
    }

    override fun onDiscoveryFinished(devices: MutableList<BluetoothDevice>) {
        if (mReconnectDevice == null) return
        if (!mIsDiscovery) return
        if (devices.find { it.address == mReconnectDevice!!.macAddress } == null) {
            resetConnectStatus()
            showMsg(getString(com.wl.resource.R.string.scan_device_not_found))
        }
        mIsDiscovery = false
    }

    override fun onConnectFail(address: String, status: Int) {
        if (mReconnectDevice == null) return
        if (CurrentDeviceManager.getAddress() != address) return
        when (status) {
            ConnectError.DEVICE_NOT_FOUND -> {
                if (mIsDiscovery) {
                    showMsg(getString(com.wl.resource.R.string.scan_device_not_found))
                }
            }
            ConnectError.CONNECT_DISCONNECT,
            ConnectError.CONNECT_FAIL -> {
                showMsg(getString(com.wl.resource.R.string.connect_fail))
            }
        }
        if (!CurrentDeviceManager.isOnline()) {
            resetConnectStatus()
        }
    }

    override fun onStart() {
        super.onStart()
        mConnectManager.addConnectListener(this)
    }

    override fun onStop() {
        super.onStop()
        mConnectManager.removeConnectListener(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
            outState.putLong(Constants.DEVICE_ID, CurrentDeviceManager.ugDevice?.id ?: -1L)
    }

    override fun onDestroyView() {
        mHandler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }

    inner class ProxyClick {
        fun goEqualizer() {
            safeNav {
                nav().navigate(R.id.action_home_to_equlizer)
            }
        }

        fun goGameMode() {
            safeNav {
                nav().navigate(R.id.action_home_to_gameMode)
            }
        }

        fun goEarbudsDual() {
            safeNav {
                nav().navigate(R.id.action_home_to_earbudsDual)
            }
        }

        fun goFind() {
            safeNav {
                nav().navigate(R.id.action_home_to_findEarbuds)
            }
        }

        fun goHQDecode() {
            // startActivity(Intent(Settings.ACTION_BLUETOOTH_SETTINGS))
            safeNav {
                nav().navigate(R.id.action_home_to_hqDecode)
            }
        }

        fun goEarbudsVersion() {
            // 当前已是最新版本,给出提示
            val versionStr = CurrentDeviceManager.getVersionState().getVersion()
            if (versionStr.isEmpty()) return
            val versionUiState = mViewModel.versionState.value
            if (versionUiState != null &&
                versionUiState.isSuccess &&
                versionStr == versionUiState.data!!.versionName
            ) {
                showMsg(getString(com.wl.resource.R.string.hint_current_verion_is_latest))
                return
            }
            safeNav {
                nav().navigate(R.id.action_home_to_earbudsVersion)
                // nav().navigate(R.id.action_home_to_earbudsUpgrade)
            }
        }
    }
}