package com.wl.earbuds.bluetooth.ota

/**
 * User: wanlang_dev
 * Data: 2022/11/29
 * Time: 17:55
 * Desc:
 */
sealed class UpgradeState {
    var progress: Int = 0
        internal set

    class Start : UpgradeState()
    class Success : UpgradeState()
    class Fail : UpgradeState()
    class Upgrading : UpgradeState()
    class None : UpgradeState()
}