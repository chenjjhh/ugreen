package com.wl.earbuds.ui.equalizer

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wl.earbuds.base.BaseFragment
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.earbuds.setEq
import com.wl.earbuds.data.model.entity.EqualizerState
import com.wl.earbuds.data.model.entity.EqualizerBean
import com.wl.earbuds.databinding.FragmentEqulizerBinding
import com.wl.earbuds.ui.adapter.EqualizerAdapter
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.ext.initClose
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.generateEqulizerData
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.nav
import me.hgj.jetpackmvvm.ext.safeNav

class EqualizerFragment : BaseFragment<BaseViewModel, FragmentEqulizerBinding>() {

    companion object {
        fun newInstance() = EqualizerFragment()
    }

    private var lastClickTime = 0L
    private val mHandler by lazy {
        Handler(Looper.getMainLooper())
    }

    private val data: MutableList<EqualizerBean> by lazy {
        generateEqulizerData(requireContext())
    }

    private val updateEqualizerRunnable by lazy {
        Runnable { updateEqualizerAfterSwitch() }
    }

    private val mEqualizerAdapter: EqualizerAdapter by lazy {
        EqualizerAdapter(data).apply {
            setOnItemClickListener { adapter, view, position ->
                data[position].equalizer.apply {
                    currentEq = this
                    val currentTime = System.currentTimeMillis()
                    if (currentTime - lastClickTime > 1000) {
                        lastClickTime = currentTime
                        setEq(this)
                    }
                    mHandler.removeCallbacks(updateEqualizerRunnable)
                    mHandler.postDelayed(updateEqualizerRunnable, 1500)
                }
            }
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
        initTitle()
        initRv()
    }

    /**
     * 设置title
     */
    private fun initTitle() {
        mDatabind.toolbar.toolbar.initClose(getString(com.wl.resource.R.string.equalizer)) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun initRv() {
        mDatabind.rvItem.apply {
            layoutManager = LinearLayoutManager(requireContext())
            val decoration =
                SpinnerDividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL,
                    false,
                    dp2px(17f), dp2px(17f)
                )
            val shape = GradientDrawable().apply {
                shape = GradientDrawable.RECTANGLE
                setSize(width, dp2px(1f))
                setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider_line))
            }
            decoration.setDrawable(shape)
            addItemDecoration(decoration)
            adapter = mEqualizerAdapter
            itemAnimator = null
        }
    }

    override fun createObserver() {
        super.createObserver()
        CurrentDeviceManager.observeOnlineState(viewLifecycleOwner, ::onOnlineChange)
        CurrentDeviceManager.observeEqState(viewLifecycleOwner, ::onEqualizerChange)
    }

    private fun onOnlineChange(isOnline: Boolean) {
        mlog("isOnline $isOnline")
        if (!isOnline) {
            safeNav {
                nav().navigateUp()
            }
        }
    }

    private fun onEqualizerChange(equalizerState: EqualizerState) {
        mEqualizerAdapter.currentEq = equalizerState.equalizer
    }

    private fun updateEqualizerAfterSwitch() {
        mEqualizerAdapter.currentEq = CurrentDeviceManager.getEqState().equalizer
    }

    override fun onDestroyView() {
        mHandler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }
}