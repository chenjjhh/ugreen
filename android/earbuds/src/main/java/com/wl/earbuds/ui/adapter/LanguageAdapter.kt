package com.wl.earbuds.ui.adapter

import android.widget.RadioButton
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.app.settings.SettingsUtils
import com.wl.earbuds.data.model.entity.LanguageBean

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class LanguageAdapter(data: MutableList<LanguageBean>): BaseQuickAdapter<LanguageBean, BaseViewHolder>(R.layout.item_language, data) {
    override fun convert(holder: BaseViewHolder, item: LanguageBean) {
        holder.setText(R.id.tv_language_title, item.local)
        holder.setText(R.id.tv_language_sample, item.translate)
        holder.getView<RadioButton>(R.id.radio_btn).isChecked = item.type == SettingsUtils.language
    }
}