package com.wl.earbuds.bluetooth.analyser

import com.wl.earbuds.bluetooth.core.CommandFrame
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.bluetooth.core.FrameType
import com.wl.earbuds.bluetooth.ota.OtaConstants
import com.wl.earbuds.bluetooth.task.TaskManager
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.utils.ext.mlog
import kotlin.experimental.and


/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 19:17
 * Desc:
 */
class StreamAnalyserImpl(listener: StreamAnalyserListener) : StreamAnalyser(listener) {

    private var mFrameOffset = 0


    override fun reset() {
        mFrameOffset = 0
    }

    override fun analyse(taskManager: TaskManager, data: ByteArray) {
        if (CommandFrame.isResponse(data)) {
            listener.onDataFound(FrameType.RESPONSE, data.copyOfRange(3, data.size))
            val status = BytesUtils.getUINT8(data, 4)
            if (status == EarbudsConstants.STATUS_SUCCESS) {
                val length = BytesUtils.getUINT8(data, 5)
                val endIndex = 6 + length + 2
                if (endIndex < data.size) {
                    // 粘包处理，通知拆包
                    analyse(taskManager, data.copyOfRange(endIndex, data.size))
                }
            } else {
                val endIndex = 5 + 2
                if (endIndex < data.size) {
                    // 粘包处理，通知拆包
                    analyse(taskManager, data.copyOfRange(endIndex, data.size))
                }
            }
        } else if (CommandFrame.isNotify(data)) {
            val length = BytesUtils.getUINT8(data, 3)
            val endIndex = 4 + length
            listener.onDataFound(FrameType.NOTIFY, data.copyOfRange(3, endIndex))
            if (endIndex < data.size) {
                // 粘包处理，通知拆包
                analyse(taskManager, data.copyOfRange(endIndex, data.size))
            }
        } else {
            if (data.isEmpty()) return
            when (BytesUtils.getUINT8(data, 0)) {
                OtaConstants.RESPONSE_RESUME_VERIFY,
                OtaConstants.RESPONSE_CONFIG,
                OtaConstants.RESPONSE_DATA_PACKET,
                OtaConstants.RESPONSE_SEGMENT_VERIFY,
                OtaConstants.RESPONSE_GET_OTA_RESULT,
                OtaConstants.RESPONSE_IMAGE_APPLY,
                -> {
                    listener.onOtaResponse(data)
                }
            }
        }
    }


}