package com.wl.earbuds.bluetooth.earbuds

import com.wl.earbuds.bluetooth.ota.OtaManager
import com.wl.earbuds.data.model.entity.*

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 14:58
 * Desc:
 */
interface EarbudsListener {
    fun onConfig(configState: ConfigState)

    fun onBattery(batteryState: BatteryState)

    fun onVersion(versionState: VersionState)

    fun onEq(eqState: EqualizerState)

    fun onFind(findState: FindState)

    fun onDual(dualState: SwitchState)

    fun onDualHint(dualHintState: SwitchState)

    fun onGameMode(gameModeState: SwitchState)

    fun onAnc(ancState: AncState)

    fun onGesture(gestureState: GestureState)

    fun onHqDecode(switchState: SwitchState)

    fun onVoice(voiceState: VoiceState)

    fun onDualDevice(dualDeviceState: DualDeviceState)

    fun onCodec(codecState: CodecState)

    fun onInBox(inBoxState: SwitchState)

    fun onCommandFail(commandId: Int)

    fun getOtaManager(): OtaManager
}