package com.wl.earbuds.utils.ext

import android.app.Activity
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.hjq.toast.ToastUtils
import me.hgj.jetpackmvvm.ext.view.lastClickTime

/**
 * User: wanlang_dev
 * Data: 2022年11月5日
 * Time: 17:15
 */

/**
 * 设置点击监听, 并实现事件节流
 */
var _lastClick = 0L
fun View.click(action: (view: View) -> Unit) {
    setOnClickListener {
        val now = System.currentTimeMillis()
        if (now - _lastClick > 250) {
            mlog("_viewClickFlag : true")
            _lastClick = now
            action(it)
        } else {
            mlog("_viewClickFlag : false")
        }
    }
}

fun Fragment.showMsg(msg: String) {
    ToastUtils.show(msg)
}

fun Activity.showMsg(msg: String) {
    ToastUtils.show(msg)
}

/**
 * 防抖600ms
 */
fun safeClick(action: () -> Unit) {
    val now = System.currentTimeMillis()
    if ( now - _lastClick > 300) {
        _lastClick = now
        action.invoke()
    }
}