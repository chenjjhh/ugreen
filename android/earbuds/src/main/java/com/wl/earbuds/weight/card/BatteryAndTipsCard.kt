package com.wl.earbuds.weight.card

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.BatteryState
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.weight.BatteryView
import me.hgj.jetpackmvvm.base.appContext
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 16:36
 * Desc:
 */
class BatteryAndTipsCard : FrameLayout, Card, View.OnClickListener {

    lateinit var clBattery: ConstraintLayout
    lateinit var clConnect: ConstraintLayout
    lateinit var tvStatus: TextView
    lateinit var clBatteryLeft: ConstraintLayout
    lateinit var batteryLeft: BatteryView
    lateinit var tvBatteryLeft: TextView
    lateinit var clBatteryRight: ConstraintLayout
    lateinit var batteryRight: BatteryView
    lateinit var tvBatteryRight: TextView
    lateinit var clBatteryBox: ConstraintLayout
    lateinit var batteryBox: BatteryView
    lateinit var tvBatteryBox: TextView

    private var mTipClickListener: OnTipClickListener? = null
    private var isInit: Boolean = false
    private var _isConnect = false

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.layout_battery_and_tips, this)
        clBattery = findViewById(R.id.cl_battery)
        clConnect = findViewById(R.id.cl_connect)
        tvStatus = findViewById(R.id.tv_status)

        clBatteryLeft = findViewById(R.id.cl_battery_left)
        batteryLeft = findViewById(R.id.battery_left)
        tvBatteryLeft = findViewById(R.id.tv_battery_left)

        clBatteryRight = findViewById(R.id.cl_battery_right)
        batteryRight = findViewById(R.id.battery_right)
        tvBatteryRight = findViewById(R.id.tv_battery_right)

        clBatteryBox = findViewById(R.id.cl_battery_box)
        batteryBox = findViewById(R.id.battery_box)
        tvBatteryBox = findViewById(R.id.tv_battery_box)
        updateUI()
    }
    
    override fun updateUI() {
        if (!isInit) {
            initClick()
        }
        if (CurrentDeviceManager.isOnline()) {
            clBattery.visible()
            clConnect.gone()
            updateBattery(CurrentDeviceManager.getBatteryState())
        } else {
            clBattery.gone()
            clConnect.visible()
        }
    }

    private fun initClick() {
        isInit = true
        tvStatus.setOnClickListener(this)
    }

    fun setOnTipClickListener(listener: OnTipClickListener?) {
        mTipClickListener = listener
    }

    fun performReconnect() {
        tvStatus.performClick()
    }

    fun isConnect(boolean: Boolean) {
        _isConnect = boolean
        if (boolean) {
            tvStatus.setText(com.wl.resource.R.string.connecting)
        } else {
            tvStatus.setText(com.wl.resource.R.string.connect)
        }
    }

    fun updateBattery(batteryState: BatteryState) {
        if (!CurrentDeviceManager.isOnline()) return
        batteryState.apply {
            if (leftCap > 100) {
                clBatteryLeft.gone()
            } else {
                clBatteryLeft.visible()
                batteryLeft.updateBattery(leftCap)
                tvBatteryLeft.text =
                    appContext.getString(com.wl.resource.R.string.battery_percent, leftCap)
                batteryLeft.isCharge = leftCharge
            }
            if (rightCap > 100) {
                clBatteryRight.gone()
            } else {
                clBatteryRight.visible()
                batteryRight.updateBattery(rightCap)
                tvBatteryRight.text =
                    appContext.getString(com.wl.resource.R.string.battery_percent, rightCap)
                batteryRight.isCharge = rightCharge
            }
            if (boxCap > 100) {
                clBatteryBox.gone()
            } else {
                clBatteryBox.visible()
                batteryBox.updateBattery(boxCap)
                tvBatteryBox.text =
                    appContext.getString(com.wl.resource.R.string.battery_percent, boxCap)
                batteryBox.isCharge = boxCharge
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tv_status -> {
                if (_isConnect) return
                mTipClickListener?.onReconnectClick()
            }
        }
    }

    interface OnTipClickListener {
        fun onReconnectClick()
    }
}