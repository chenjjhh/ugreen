package com.wl.earbuds.data.model.entity

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 9:37
 * Desc:
 */
data class BatteryState(
    val leftCap: Int = 128,
    val leftCharge: Boolean = false,
    val rightCap: Int = 128,
    val rightCharge: Boolean = false,
    val boxCap: Int = 128,
    val boxCharge: Boolean = false
) {
    fun isLeftOn() = leftCap in 0..100
    fun isRightOn() = rightCap in 0..100
    fun isBoxOn() = boxCap in 0..100
    fun isUpgradeAvailable() = leftCap > 20 && rightCap > 20 && boxCap > 20
}