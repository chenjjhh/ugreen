package com.wl.earbuds.ui.earbuds

import androidx.lifecycle.viewModelScope
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.db.UgDatabaseUtil
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import kotlinx.coroutines.launch
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

class EarbudsNameViewModel : BaseViewModel() {
    // TODO: Implement the ViewModel
    val name: UnPeekLiveData<String> = UnPeekLiveData()

    fun updateName(name: String) {
        viewModelScope.launch {
        val copy = CurrentDeviceManager.ugDevice!!.copy(name = name)
            UgDatabaseUtil.ugDeviceDao?.insertUgDevice(copy)
            CurrentDeviceManager.ugDevice = copy
        }
    }

}