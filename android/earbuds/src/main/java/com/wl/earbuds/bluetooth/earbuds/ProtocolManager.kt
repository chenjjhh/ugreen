package com.wl.earbuds.bluetooth.earbuds

import com.wl.earbuds.bluetooth.analyser.StreamAnalyser
import com.wl.earbuds.bluetooth.analyser.StreamAnalyserImpl
import com.wl.earbuds.bluetooth.analyser.StreamAnalyserListener
import com.wl.earbuds.bluetooth.core.*
import com.wl.earbuds.bluetooth.ota.OtaManager
import com.wl.earbuds.bluetooth.task.TaskManager
import com.wl.earbuds.data.model.entity.*
import com.wl.earbuds.utils.ext.mlog

/**
 * User: wanlang_dev
 * Data: 2022/11/17
 * Time: 14:57
 * Desc:
 */
class ProtocolManager(val listener: EarbudsListener, private val taskManager: TaskManager) :
    EarbudsListener, StreamAnalyserListener {

    private val parser = EarbudsParser(this)
    private val analyser: StreamAnalyser = StreamAnalyserImpl(this)
    override fun onConfig(configState: ConfigState) {
        taskManager.runOnMain {
            listener.onConfig(configState)
        }
    }

    override fun onBattery(batteryState: BatteryState) {
        taskManager.runOnMain {
            listener.onBattery(batteryState)
        }
    }

    override fun onVersion(versionState: VersionState) {
        taskManager.runOnMain {
            listener.onVersion(versionState)
        }
    }

    override fun onEq(eqState: EqualizerState) {
        taskManager.runOnMain {
            listener.onEq(eqState)
        }
    }

    override fun onFind(findState: FindState) {
        taskManager.runOnMain {
            listener.onFind(findState)
        }
    }

    override fun onDual(dualState: SwitchState) {
        taskManager.runOnMain {
            listener.onDual(dualState)
        }
    }

    override fun onDualHint(dualHintState: SwitchState) {
        taskManager.runOnMain {
            listener.onDualHint(dualHintState)
        }
    }

    override fun onGameMode(gameModeState: SwitchState) {
        taskManager.runOnMain {
            listener.onGameMode(gameModeState)
        }
    }

    override fun onAnc(ancState: AncState) {
        taskManager.runOnMain {
            listener.onAnc(ancState)
        }
    }

    override fun onGesture(gestureState: GestureState) {
        taskManager.runOnMain {
            listener.onGesture(gestureState)
        }
    }

    override fun onHqDecode(switchState: SwitchState) {
        taskManager.runOnMain {
            listener.onHqDecode(switchState)
        }
    }

    override fun onVoice(voiceState: VoiceState) {
        taskManager.runOnMain {
            listener.onVoice(voiceState)
        }
    }

    override fun onDualDevice(dualDeviceState: DualDeviceState) {
        taskManager.runOnMain {
            listener.onDualDevice(dualDeviceState)
        }
    }

    override fun onCodec(codecState: CodecState) {
        taskManager.runOnMain {
            listener.onCodec(codecState)
        }
    }

    override fun onInBox(inBoxState: SwitchState) {
        taskManager.runOnMain {
            listener.onInBox(inBoxState)
        }
    }

    override fun onCommandFail(commandId: Int) {
        taskManager.runOnMain {
            listener.onCommandFail(commandId)
        }
    }

    override fun getOtaManager(): OtaManager {
        return listener.getOtaManager()
    }

    fun parseData(data: ByteArray) {
        analyser.analyse(taskManager, data)
    }

    override fun onDataFound(frameType: FrameType, data: ByteArray) {
        when (frameType) {
            FrameType.RESPONSE -> {
                parser.parseResponse(data)
            }
            FrameType.NOTIFY -> {
                parser.parseNotify(data)
            }
            else -> {}
        }
    }

    override fun onOtaResponse(data: ByteArray) {
        getOtaManager().parseOtaResponse(data)
    }


}
