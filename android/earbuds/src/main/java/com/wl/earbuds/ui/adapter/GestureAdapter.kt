package com.wl.earbuds.ui.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.GestureBean

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class GestureAdapter(data: MutableList<GestureBean>) :
    BaseQuickAdapter<GestureBean, BaseViewHolder>(R.layout.item_gesture, data) {
    private var _selectedIndex: Int = 0
    private var _isGrayMode = false
    var isGrayMode: Boolean
        get() = _isGrayMode
        set(value) {
            val temp = _isGrayMode
            _isGrayMode = value
            if (temp != value) {
                notifyDataSetChanged()
            }
        }

    override fun convert(holder: BaseViewHolder, item: GestureBean) {
        holder.setText(R.id.tv_operation, item.title)
        holder.setText(R.id.tv_command, item.command)
        holder.setTextColor(
            R.id.tv_command,
            context.getColor(
                if (_isGrayMode) {
                    com.wl.resource.R.color.text_secondary
                } else {
                    com.wl.resource.R.color.standard_secondary
                }
            )
        )
    }
}