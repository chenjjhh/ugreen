package com.wl.earbuds.ui.popup

import android.content.Context
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wanlang.base.BasePopupWindow
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.MenuBean
import com.wl.earbuds.ui.adapter.MenuAdapter
import com.wl.earbuds.utils.ext.dp2px
import com.wl.earbuds.utils.generateMenuData
import com.wl.earbuds.weight.decoration.SpinnerDividerItemDecoration

/**
 * User: wanlang_dev
 * Data: 2022/11/9
 * Time: 15:26
 * Desc:
 */
class MenuPopup {
    class Builder(context: Context): BasePopupWindow.Builder<Builder>(context) {
        private var mListener: OnListener? = null

        private val data: MutableList<MenuBean> by lazy {
            generateMenuData(context)
        }

        private val menuAdapter by lazy {
            MenuAdapter(data).apply {
                setOnItemClickListener { _, _, position ->
                    mListener?.onSelected(data[position].type)
                    dismiss()
                }
            }
        }

        private val rvItem: RecyclerView? by lazy { findViewById(R.id.rv_item) }

        init {
            setContentView(R.layout.layout_pop_menu)
            rvItem?.apply {
                layoutManager = LinearLayoutManager(context)
                val decoration =
                    SpinnerDividerItemDecoration(
                        context,
                        DividerItemDecoration.VERTICAL,
                        false,
                    )
                val shape = GradientDrawable().apply {
                    shape = GradientDrawable.RECTANGLE
                    setSize(width, dp2px(1f))
                    setColor(ContextCompat.getColor(context, com.wl.resource.R.color.divider_line))
                }
                decoration.setDrawable(shape)
                addItemDecoration(decoration)
                adapter = menuAdapter
                itemAnimator = null
            }
        }

        fun setOnListener(listener: OnListener?): Builder {
            mListener = listener
            return this
        }
    }

    fun interface OnListener {
        fun onSelected(position: Int)
    }
}