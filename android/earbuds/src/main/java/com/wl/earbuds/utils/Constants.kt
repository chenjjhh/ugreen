package com.wl.earbuds.utils

/**
 * User: wanlang_dev
 * Data: 2022/11/11
 * Time: 15:56
 * Desc:
 */
interface Constants {
    companion object {
        const val NOTIFICATION_CHANNEL_ID = "green"
        const val DEVICE_ID = "DEVICE_ID"
        const val PROTOCOL_TYPE = "protocol_type"
        const val PROTOCOL_PRIVACY = 1                  // 隐私政策
        const val PROTOCOL_USER = 2                     // 用户协议
        const val USER_MANUAL = 3                       // 用户手册
        const val HEADSET_UGREEN_PREFIX = "UGREEN HiTune T6"      // 名称前缀
        const val EARBUDS_VERSION = "earbuds_version"   // 耳机版本
        const val EXIT = "exit"                         // 退出app
        const val START_IMAGES = "start_images"         // 欢迎页

        const val MENU_DEVICE_SETTINGS = 0              // 设备设置
        const val MENU_ADD_DEVICE = 1                   // 添加设备
        const val MENU_DISCONNECT = 2                   // 断开连接
        const val MENU_DELETE_DEVICE = 3                // 删除设备
        const val MENU_MANUAL = 4                       // 产品说明书

        const val ACTION_ENABLE_CODE = 1000
    }
}