package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 10:10
 * Desc:
 */
enum class Find(val action: Int, val key: Int) {
    FIND_LEFT_CLOSE(0x00, 0x01),
    FIND_LEFT_ON(0x01, 0x01),
    FIND_RIGHT_CLOSE(0x00, 0x02),
    FIND_RIGHT_ON(0x01, 0x02);
    companion object {
        @JvmStatic
        fun valueOf(action: Int, key: Int): Find = Find.values().find { type ->
            type.action == action && type.key == key
        } ?: Find.FIND_LEFT_CLOSE
    }
}