package com.wl.earbuds.data.model.eumn

/**
 * User: wanlang_dev
 * Data: 2022/11/19
 * Time: 19:42
 * Desc:
 */
enum class Operation {
    LEFT_SINGLE,
    LEFT_DOUBLE,
    LEFT_TRIPLE,
    LEFT_LONG,
    RIGHT_SINGLE,
    RIGHT_DOUBLE,
    RIGHT_TRIPLE,
    RIGHT_LONG;
}