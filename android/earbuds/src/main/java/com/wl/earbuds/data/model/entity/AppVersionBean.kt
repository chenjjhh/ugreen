package com.wl.earbuds.data.model.entity

import com.google.gson.annotations.SerializedName

/**
 * User: wanlang_dev
 * Data: 2022/11/30
 * Time: 11:23
 * Desc:
 */
data class AppVersionResponse(
    @SerializedName("appVersion")
    val versionName: String,
    @SerializedName("versionCode")
    val versionCode: Int,
    @SerializedName("filePath")
    val fileUrl: String,
    @SerializedName("fileSize")
    val fileSize: String,
    @SerializedName("updateContent")
    val content: String,
)
