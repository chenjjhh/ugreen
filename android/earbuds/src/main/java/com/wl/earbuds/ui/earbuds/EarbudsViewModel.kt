package com.wl.earbuds.ui.earbuds

import androidx.lifecycle.viewModelScope
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.db.UgDatabaseUtil
import com.wl.db.model.UgDevice
import com.wl.earbuds.data.model.entity.EarbudsVersionBean
import com.wl.earbuds.data.state.ResponseUiState
import com.wl.earbuds.network.apiService
import kotlinx.coroutines.launch
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.request

class EarbudsViewModel : BaseViewModel() {
    val versionState: UnPeekLiveData<ResponseUiState<EarbudsVersionBean>> = UnPeekLiveData()
    val deviceState: UnPeekLiveData<UgDevice> = UnPeekLiveData()

    fun getEarbudsVersion() {
        request({
            apiService.getEarbudsVersion()
        }, {
            versionState.value = ResponseUiState(true, it)
        }, {
            versionState.value = ResponseUiState(false)
        })
    }

    fun reloadDevice(deviceId: Long) {
        viewModelScope.launch {
            val device = UgDatabaseUtil.ugDeviceDao?.getUgDeviceById(deviceId)
            deviceState.value = device!!
        }
    }
}