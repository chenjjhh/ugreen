package com.wl.earbuds.ui.adapter

import android.annotation.SuppressLint
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.wl.earbuds.R
import com.wl.earbuds.data.model.entity.DualDevice
import me.hgj.jetpackmvvm.ext.util.bluetoothManager
import me.hgj.jetpackmvvm.ext.view.gone
import me.hgj.jetpackmvvm.ext.view.visible

/**
 * User: wanlang_dev
 * Data: 2022/11/7
 * Time: 11:31
 * Desc:
 */
class DualDeviceAdapter(data: MutableList<DualDevice>) :
    BaseQuickAdapter<DualDevice, BaseViewHolder>(R.layout.item_dual, data) {

    private val mBtAdapter by lazy {
        context.bluetoothManager?.adapter
    }

    private var _index = -1
    var index: Int = -1
        private set
        get() = _index

    override fun setNewInstance(list: MutableList<DualDevice>?) {
        _index = -1
        super.setNewInstance(list)
    }

    @SuppressLint("MissingPermission")
    override fun convert(holder: BaseViewHolder, item: DualDevice) {
        holder.setText(R.id.tv_name, item.name)
        if (holder.adapterPosition == 0) {
            holder.getView<View>(R.id.tv_label).visible()
        } else {
            holder.getView<View>(R.id.tv_label).gone()
        }
    }
}