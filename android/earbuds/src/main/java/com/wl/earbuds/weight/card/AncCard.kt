package com.wl.earbuds.weight.card

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.wl.earbuds.R
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.bluetooth.core.EarbudsConstants
import com.wl.earbuds.data.model.entity.AncState
import com.wl.earbuds.data.model.eumn.Anc
import com.wl.earbuds.data.model.eumn.AncLevel
import com.wl.earbuds.utils.ext.click
import com.wl.earbuds.utils.ext.mlog
import com.wl.earbuds.utils.setAnimateAlpha
import com.wl.earbuds.weight.checkable.CheckableConstraintLayout
import com.wl.earbuds.weight.expandable.ExpandableLayout

/**
 * User: wanlang_dev
 * Data: 2022/11/5
 * Time: 14:14
 * Desc:
 */
class AncCard : FrameLayout, Card {
    lateinit var clAncControl: View
    lateinit var elAncMethod: ExpandableLayout
    lateinit var tvAncLevel: TextView
    lateinit var clAncMethod: ConstraintLayout
    lateinit var cclAnc: CheckableConstraintLayout
    lateinit var cclAncClose: CheckableConstraintLayout
    lateinit var cclTransparent: CheckableConstraintLayout
    
    private var lastAncState: AncState? = AncState()
    private var isInit: Boolean = false

    private var mClickListener: AncCard.OnAncClickListener? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    @SuppressLint("CustomViewStyleable")
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(R.layout.layout_anc, this)
        clAncControl = findViewById(R.id.cl_anc)
        elAncMethod = findViewById(R.id.el_anc_method)
        clAncMethod = findViewById(R.id.cl_anc_method)
        cclAnc = findViewById(R.id.ccl_anc)
        cclAncClose = findViewById(R.id.ccl_anc_close)
        cclTransparent = findViewById(R.id.ccl_anc_transparent)
        tvAncLevel = findViewById(R.id.tv_denoise_level)
        updateUI()
    }

    fun setListener(listener: AncCard.OnAncClickListener?) {
        mClickListener = listener
    }

    override fun updateUI() {

        // setAnimateAlpha(clNoiseControl, 0.4f)
        // elAncMethod.setView(null)
        // 点击按钮初始化后跳过
        if (!isInit) {
            initClick()
        }
        updateAnc(CurrentDeviceManager.getAncState())
        if (CurrentDeviceManager.isOnline()) {
            setAnimateAlpha(this, EarbudsConstants.ALPHA_ON_VALUE, true)
        } else {
            setAnimateAlpha(this, EarbudsConstants.ALPHA_OFF_VALUE)
        }
        // updateAncMethod(CurrentDeviceManager.getAncState())
    }

    private fun initClick() {
        isInit = true
        cclAnc.click {
            if (!CurrentDeviceManager.isOnline()) return@click
            if (!cclAnc.isChecked) {
                mClickListener?.onAncClick()
                mlog("anc click")
                switchToAnc()
            }
        }
        cclAncClose.click {
            if (!CurrentDeviceManager.isOnline()) return@click
            if (!cclAncClose.isChecked) {
                mClickListener?.onCloseClick()
                mlog("anc close click")
                switchToClose()
            }
        }
        cclTransparent.click {
            if (!CurrentDeviceManager.isOnline()) return@click
            if (!cclTransparent.isChecked) {
                mClickListener?.onTransparentClick()
                mlog("anc transparent click")
                switchToTransparent()
            }
        }
        clAncMethod.click {
            if (!CurrentDeviceManager.isOnline()) return@click
            if (cclAnc.isChecked) {
                mClickListener?.onAncMethodClick()
                mlog("anc method click")
            }
        }
    }

    private fun switchToAnc() {
        elAncMethod.setView(clAncMethod)
        cclAnc.isChecked = true
        cclAncClose.isChecked = false
        cclTransparent.isChecked = false
    }

    private fun switchToClose() {
        elAncMethod.setView(null)
        cclAnc.isChecked = false
        cclAncClose.isChecked = true
        cclTransparent.isChecked = false
    }

    private fun switchToTransparent() {
        elAncMethod.setView(null)
        cclAnc.isChecked = false
        cclAncClose.isChecked = false
        cclTransparent.isChecked = true
    }

    fun updateAnc(ancState: AncState) {
        lastAncState = ancState
        updateAncMethod(ancState)
        // tvAncLevel.text =
        //     getAncText(tvAncLevel.context, ancState.level)
        when (ancState.mode) {
            Anc.ANC_CLOSE -> {
                switchToClose()
            }
            Anc.ANC_TRANS -> {
                switchToTransparent()
            }
            else -> {
                switchToAnc()
            }
        }
    }

    fun updateAncMethod(ancState: AncState) {
        when (ancState.level) {
            AncLevel.ANC_LIGHT -> {
                tvAncLevel.text =
                    tvAncLevel.context.getString(com.wl.resource.R.string.anc_light)
            }
            AncLevel.ANC_MEDIUM -> {
                tvAncLevel.text =
                    tvAncLevel.context.getString(com.wl.resource.R.string.anc_medium)
            }
            AncLevel.ANC_DEEP -> {
                tvAncLevel.text =
                    tvAncLevel.context.getString(com.wl.resource.R.string.anc_deep)
            }
        }
    }

    interface OnAncClickListener {
        fun onAncClick()
        fun onCloseClick()
        fun onTransparentClick()
        fun onAncMethodClick()
    }
}