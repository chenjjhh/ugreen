package com.wl.earbuds.viewmodel

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.kunminx.architecture.ui.callback.UnPeekLiveData
import com.wl.db.UgDatabaseUtil
import com.wl.db.model.UgDevice
import com.wl.earbuds.bluetooth.utils.SystemUtils
import com.wl.earbuds.data.model.entity.SavedDevice
import com.wl.earbuds.data.state.DeviceListUiState
import com.wl.earbuds.utils.ext.mlog
import kotlinx.coroutines.launch
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel

/**
 * User: wanlang_dev
 * Data: 2023/1/12
 * Time: 11:40
 * Desc:
 */
class DeviceManagerViewModel : BaseViewModel() {

    // var isHarmony = SystemUtils.isHarmonyOs()

    var isSyncDeviceList = false

    val bgConnectedDeviceState: UnPeekLiveData<BluetoothDevice> = UnPeekLiveData()

    val deviceListUiState: MutableLiveData<DeviceListUiState> = MutableLiveData()

    fun getUgdeviceByAddress(address: String): UgDevice? {
        return deviceListUiState.value?.data?.find { it.macAddress == address }
    }

    fun getDeviceList() {
        mlog("isSyncDeviceList: $isSyncDeviceList")
        if (isSyncDeviceList) return
        isSyncDeviceList = true
        UgDatabaseUtil.ugDeviceDao?.queryAll()?.observeForever {
            deviceListUiState.value = DeviceListUiState(mutableListOf<UgDevice>().apply {
                addAll(it)
            })
        }
    }

    val ugDeviceLiveData: UnPeekLiveData<SavedDevice> = UnPeekLiveData()

    @SuppressLint("MissingPermission")
    fun saveIfNewDevice(device: BluetoothDevice) {
        viewModelScope.launch {
            var ugDevice = UgDatabaseUtil.ugDeviceDao?.getUgDeviceByAddress(device.address)
            var isAdd = false
            if (ugDevice == null) {
                UgDatabaseUtil.ugDeviceDao?.insertUgDevice(
                    UgDevice(
                        name = device.name,
                        deviceName = device.name,
                        macAddress = device.address
                    )
                )
                ugDevice = UgDatabaseUtil.ugDeviceDao?.getUgDeviceByAddress(device.address)
                isAdd = true
            }
            mlog("ugdevice: $ugDevice")
            ugDeviceLiveData.value = SavedDevice(device, ugDevice!!, isAdd)
        }
    }
}