package com.wl.earbuds.ui.device

import androidx.lifecycle.viewModelScope
import com.wl.db.UgDatabaseUtil
import com.wl.db.model.UgDevice
import com.wl.earbuds.app.settings.ConfigUtils
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager
import com.wl.earbuds.network.apiService
import kotlinx.coroutines.launch
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.ext.request

class DeviceViewModel : BaseViewModel() {
    var mAutoConnect = true

    fun getConfig() {
        request({ apiService.getAppConfig()}, {
            ConfigUtils.settingsEnable = it.settingsEnable
            ConfigUtils.deviceSettingsEnable = it.deviceSettingsEnable
            ConfigUtils.addDeviceEnable = it.addDeviceEnable
            ConfigUtils.disconnectEnable = it.disconnectEnable
            ConfigUtils.deleteEnable = it.deleteEnable
            ConfigUtils.manualEnable = it.manualEnable
            ConfigUtils.eqEnable = it.eqEnable
            ConfigUtils.gameModeEnable = it.gameModeEnable
            ConfigUtils.dualEnable = it.dualEnable
            ConfigUtils.findEnable = it.findEnable
            ConfigUtils.hqEnable = it.hqEnable
            ConfigUtils.upgradeEnable = it.upgradeEnable
        })
    }

    fun deleteUgDevices(devices: List<UgDevice>) {
        val currentUgDevice = CurrentDeviceManager.ugDevice
        if (currentUgDevice != null && CurrentDeviceManager.isOnline() && devices.contains(currentUgDevice)) {
            CurrentDeviceManager.disconnect()
        }
        viewModelScope.launch {
            UgDatabaseUtil.ugDeviceDao?.deleteUgDevices(devices)
        }
    }

}