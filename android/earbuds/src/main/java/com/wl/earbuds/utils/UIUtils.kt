package com.wl.earbuds.utils

import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.children
import com.wl.earbuds.R
import com.wl.earbuds.bluetooth.connect.CurrentDeviceManager

/**
 * User: wanlang_dev
 * Data: 2022/11/4
 * Time: 16:44
 * Desc:
 */
val ALPHA_TAG = R.id.tag

fun checkIsOnline() = CurrentDeviceManager.isOnline()
/**
 * 设置透明度动画
 */
fun setAnimateAlpha(view: View, toAlpah: Float, directly: Boolean = false) {
    if (directly) {
        setAnimateAlphaDirectly(view, toAlpah)
    } else {
        setAnimateAlpha(view, toAlpah)
    }
}

fun setAnimateAlphaDirectly(view: View, toAlpha: Float) {
    val tagId = ALPHA_TAG
    view.animate().cancel()
    view.setTag(tagId, toAlpha)
    view.alpha = toAlpha
}

fun setAnimateAlpha(view: View, toAlpha: Float) {
    val tagId = ALPHA_TAG
    view.takeIf {
        it.getTag(tagId)?.toString()?.toFloat() != toAlpha
    }?.let {
        it.setTag(tagId, toAlpha)
        it.animate().setDuration(300).alpha(toAlpha).start()
    }
}

/**
 * 设置enable
 */
fun setEnableWithChild(view: View, isEnable: Boolean, isImageViewAlone: Boolean = false) {
    view.isEnabled = isEnable
    if (isImageViewAlone && view is ImageView) {
        if (isEnable) {
            view.setColorFilter(null)
            view.imageAlpha = 255
        } else {
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            view.colorFilter = ColorMatrixColorFilter(colorMatrix)
            view.imageAlpha = 128
        }
    }
    if (view is ViewGroup) {
        view.children.forEach {
            setEnableWithChild(it, isEnable)
        }
    }
}