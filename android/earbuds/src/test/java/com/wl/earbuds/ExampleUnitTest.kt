package com.wl.earbuds

import com.wl.earbuds.bluetooth.earbuds.generateByteArray
import com.wl.earbuds.bluetooth.utils.BytesUtils
import com.wl.earbuds.bluetooth.utils.crc16
import com.wl.earbuds.data.model.eumn.Anc
import org.junit.Test

import org.junit.Assert.*
import java.util.zip.CRC32

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testCommand() {
        val bytes = generateByteArray(Anc.ANC_TRANS.value)
        println("data: ${BytesUtils.byte2HexStr(bytes)}")
    }

    @Test
    fun testCrc16() {
        val data = byteArrayOf(0x0F, 0x00)
        val result = data.crc16()
        println("data: $result")
    }

    @Test
    fun testCrc() {
        val randomCode = ByteArray(32)
        val crc32 = CRC32()
        crc32.update(randomCode)
        val result = ByteArray(4)
        BytesUtils.setUINT32(crc32.value, result, 0)
        println("data: ${BytesUtils.byte2HexStr(result)}")
    }
}