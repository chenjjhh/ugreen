package com.guoxuansource;

import android.bluetooth.BluetoothGatt;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;
import com.granwin.apkit.ble.BleManager;
import com.granwin.apkit.ble.callback.BleGattCallback;
import com.granwin.apkit.ble.callback.BleMtuChangedCallback;
import com.granwin.apkit.ble.callback.BleNotifyCallback;
import com.granwin.apkit.ble.callback.BleScanCallback;
import com.granwin.apkit.ble.callback.BleWriteCallback;
import com.granwin.apkit.ble.data.BleDevice;
import com.granwin.apkit.ble.exception.BleException;
import com.granwin.apkit.ble.scan.BleScanRuleConfig;
import com.granwin.apkit.ble.utils.HexUtil;
import com.granwin.apkit.entity.SetDeviceNetworkResultEntity;
import com.granwin.apkit.utils.CRC16Util;
import com.granwin.apkit.utils.CommonUtils;
import com.granwin.apkit.utils.DesUtil;
import com.granwin.apkit.utils.LogUtil;
import com.granwin.apkit.utils.buffer.ReadBuffer;
import com.granwin.apkit.utils.buffer.WriteBuffer;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RNBLEModule extends ReactContextBaseJavaModule {
    private ReactApplicationContext context;
    BleDevice curBleDevice;

    public static UUID SERVICE_UUID = UUID.fromString("0000ABF0-0000-1000-8000-00805F9B34FB");
    public static UUID WRITE_CHARACTERISTIC_UUID = UUID.fromString("0000ABF1-0000-1000-8000-00805F9B34FB");
    public static UUID NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ABF2-0000-1000-8000-00805F9B34FB");

    private int bleDataID;

    //数据包的长度
    private int receiveBleDataLength;
    //数据包id
    private int receiveBleDataID;
    //数据包的当前index
    private int receiveBleDataPackageIndex;

    private String encryptionKey = "gwin0801";

    //缓存有效数据
    private List<byte[]> validData;

    public RNBLEModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.context = reactContext;
    }

    @Override
    public String getName() {
        return "RNBLEModule";
    }

    public static void sendEvent(ReactContext reactContext, String eventName, @Nullable WritableMap params) {
        reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName, params);
    }

    public boolean isNameInArray(String name, String[] nameArray) {
        boolean isHave = false;
        for (int i = 0; i < nameArray.length; i++) {
            if (name.contains(nameArray[i])) {
                isHave = true;
            }
        }
        return isHave;
    }

    /**
     * 扫描蓝牙设备
     */
 /*   @ReactMethod
    public void scanDevices(String[] bleDeviceName, Callback callback) {
        LogUtil.d("RNBLEModule scanDevices, device name=" + bleDeviceName);
        BleScanRuleConfig scanRuleConfig = new BleScanRuleConfig.Builder()
                .setAutoConnect(false)      // 连接时的autoConnect参数，可选，默认false
                .setScanTimeOut(6000)              // 扫描超时时间，可选，默认10秒
                .setDeviceName(true, bleDeviceName)
                .build();
        BleManager.getInstance().initScanRule(scanRuleConfig);

        BleManager.getInstance().scan(new BleScanCallback() {
            @Override
            public void onScanStarted(boolean success) {
                if (success) {
                    LogUtil.d("RNBLEModule 开启扫描成功");
                    callback.invoke("success");
                } else {
                    LogUtil.d("RNBLEModule 开启扫描失败");
                    callback.invoke("fail");
                }
            }

            @Override
            public void onLeScan(BleDevice bleDevice) {
                super.onLeScan(bleDevice);
                if (bleDevice == null) {
                    return;
                }
                String deviceName = bleDevice.getName();

                if (bleDevice == null || deviceName == null) {
                    return;
                }
                if (isNameInArray(deviceName, bleDeviceName)) {
                    LogUtil.d("RNBLEModule 描到符合要求的设备->" + bleDevice.getMac() + "---" + bleDevice.getName());

                    LogUtil.d("RNBLEModule getScanRecord->" + CommonUtils.getHexBinString(bleDevice.getScanRecord()));
                    WritableMap params = Arguments.createMap();
                    params.putString("mac", bleDevice.getMac());
                    params.putString("name", bleDevice.getName());
                    try {
                        params.putString("showMac", formatMac(CommonUtils.getHexBinString(subByte(bleDevice.getScanRecord(), 31, 6))));
                    } catch (Exception e) {

                    }
                    //目前只返回了mac地址给rn，有需求再修改
                    sendEvent(getReactApplicationContext(), "RNBLEModule_onScan", params);
                } else {
                    if (bleDevice.getMac() != null)
                        LogUtil.d("RNBLEModule 扫描到其他设备->" + deviceName + "," + bleDevice.getMac());
                }
            }

            @Override
            public void onScanning(BleDevice bleDevice) {

            }

            @Override
            public void onScanFinished(List<BleDevice> scanResultList) {
                LogUtil.d("RNBLEModule onScanFinished,not device");
            }
        });
    }*/

    /**
     * 扫描蓝牙设备
     */
    @ReactMethod
    public void scanDevices(String bleDeviceName, Callback callback) {
        LogUtil.d("RNBLEModule scanDevices, device name=" + bleDeviceName);
        BleScanRuleConfig scanRuleConfig = new BleScanRuleConfig.Builder()
                .setAutoConnect(false)      // 连接时的autoConnect参数，可选，默认false
                .setScanTimeOut(6000)              // 扫描超时时间，可选，默认10秒
                .setDeviceName(true, bleDeviceName)
                .build();
        BleManager.getInstance().initScanRule(scanRuleConfig);

        BleManager.getInstance().scan(new BleScanCallback() {
            @Override
            public void onScanStarted(boolean success) {
                if (success) {
                    LogUtil.d("RNBLEModule 开启扫描成功");
                    callback.invoke("success");
                } else {
                    LogUtil.d("RNBLEModule 开启扫描失败");
                    callback.invoke("fail");
                }
            }

            @Override
            public void onLeScan(BleDevice bleDevice) {
                super.onLeScan(bleDevice);
                if (bleDevice == null) {
                    return;
                }
                String deviceName = bleDevice.getName();

                if (bleDevice == null || deviceName == null) {
                    return;
                }
                if (deviceName.contains(bleDeviceName)) {
                    LogUtil.d("RNBLEModule 描到符合要求的设备->" + bleDevice.getMac() + "---" + bleDevice.getName());

                    LogUtil.d("RNBLEModule getScanRecord->" + CommonUtils.getHexBinString(bleDevice.getScanRecord()));
                    WritableMap params = Arguments.createMap();
                    params.putString("mac", bleDevice.getMac());
                    params.putString("name", bleDevice.getName());
                    try {
                        params.putString("showMac", formatMac(CommonUtils.getHexBinString(subByte(bleDevice.getScanRecord(), 31, 6))));
                    } catch (Exception e) {

                    }
                    //目前只返回了mac地址给rn，有需求再修改
                    sendEvent(getReactApplicationContext(), "RNBLEModule_onScan", params);
                } else {
                    if (bleDevice.getMac() != null)
                        LogUtil.d("RNBLEModule 扫描到其他设备->" + deviceName + "," + bleDevice.getMac());
                }
            }

            @Override
            public void onScanning(BleDevice bleDevice) {

            }

            @Override
            public void onScanFinished(List<BleDevice> scanResultList) {
                LogUtil.d("RNBLEModule onScanFinished,not device");
            }
        });
    }

    private String formatMac(String mac) {
        String finalMac = "";
        finalMac += mac.substring(0, 2);
        finalMac += ":";
        finalMac += mac.substring(2, 4);
        finalMac += ":";
        finalMac += mac.substring(4, 6);
        finalMac += ":";
        finalMac += mac.substring(6, 8);
        finalMac += ":";
        finalMac += mac.substring(8, 10);
        finalMac += ":";
        finalMac += mac.substring(10, 12);
        return finalMac;
    }

    /**
     * 根据mac地址连接蓝牙设备
     */
    @ReactMethod
    public void connectDevice(String mac, Callback callback) {
        LogUtil.d("RNBLEModule connectDevice, mac=" + mac);
        //先停止扫描
        BleManager.getInstance().cancelScan();
        //先断开
        BleManager.getInstance().disconnectAllDevice();
        BleManager.getInstance().connect(mac, new BleGattCallback() {
            @Override
            public void onStartConnect() {
                LogUtil.d("RNBLEModule onStartConnect");

                WritableMap params = Arguments.createMap();
                params.putString("status", "onStartConnect");
                sendEvent(getReactApplicationContext(), "RNBLEModule_Connect_Status", params);
            }

            @Override
            public void onConnectFail(BleDevice bleDevice, BleException exception) {
                LogUtil.d("RNBLEModule onConnectFail");

                WritableMap params = Arguments.createMap();
                params.putString("status", "onConnectFail");
                params.putString("exception", exception.toString());
                sendEvent(getReactApplicationContext(), "RNBLEModule_Connect_Status", params);
            }

            @Override
            public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt gatt, int status) {
                LogUtil.d("RNBLEModule onConnectSuccess");

                WritableMap params = Arguments.createMap();
                params.putString("status", "onConnectSuccess");
                sendEvent(getReactApplicationContext(), "RNBLEModule_Connect_Status", params);

                //缓存蓝牙设备实体
                curBleDevice = bleDevice;

                openNotify();
            }

            @Override
            public void onDisConnected(boolean isActiveDisConnected, BleDevice device, BluetoothGatt gatt, int status) {
                //连接中断
                LogUtil.d("RNBLEModule onDisConnected");

                WritableMap params = Arguments.createMap();
                params.putString("status", "onDisConnected");
                sendEvent(getReactApplicationContext(), "RNBLEModule_Connect_Status", params);

                //清除蓝牙设备实体
                curBleDevice = null;
            }
        });
    }


    /**
     * 暂停扫描
     */
    @ReactMethod
    public void stopScanDevice() {
        LogUtil.d("RNBLEModule ReactMethod");
        BleManager.getInstance().cancelScan();
    }

    /**
     * 断开当前连接的设备
     */
    @ReactMethod
    public void disConnectDevice() {
        LogUtil.d("RNBLEModule disConnectDevice");
        BleManager.getInstance().disconnectAllDevice();
    }

    /**
     * 发送加密后的结果 分多个包
     *
     * @param bleCode
     */
    @ReactMethod
    private void sendData(String bleCode) {
        byte[] orginalBleData = bleCode.getBytes();
        //加密后的内容
        byte[] encryptData = DesUtil.encrypt2(orginalBleData, encryptionKey);

        WriteBuffer firstBuffer = new WriteBuffer(17);
        firstBuffer.writeBytes(CRC16Util.getParamCRC(orginalBleData));
        firstBuffer.writeShort(orginalBleData.length);
        firstBuffer.writeBytes(subByte(encryptData, 0, 13));

        List<byte[]> dataList = new ArrayList<>();
        dataList.add(firstBuffer.array());

        byte[] endByteArray = new byte[encryptData.length - 13];
        System.arraycopy(encryptData, 13, endByteArray, 0, encryptData.length - 13);
        int allPackageNum = endByteArray.length % 17 == 0 ? endByteArray.length / 17 : endByteArray.length / 17 + 1;
        for (int i = 0; i < allPackageNum; i++) {
            dataList.add(subByte(endByteArray, i * 17, endByteArray.length >= (i * 17 + 17) ? 17 : endByteArray.length - i * 17));
        }

        for (int i = 0; i < dataList.size(); i++) {
            WriteBuffer writeBuffer = new WriteBuffer(dataList.get(i).length + 3);
            writeBuffer.writeShort(bleDataID);
            writeBuffer.writeByte(i + 1);
            writeBuffer.writeBytes(dataList.get(i));

            LogUtil.d("sendEffectiveData->" + CommonUtils.getHexBinString(writeBuffer.array()));
            BleManager.getInstance().write(curBleDevice, String.valueOf(SERVICE_UUID), String.valueOf(WRITE_CHARACTERISTIC_UUID), writeBuffer.array(), true, new BleWriteCallback() {
                @Override
                public void onWriteSuccess(int current, int total, byte[] justWrite) {
                    LogUtil.d("sendEffectiveData->onWriteSuccess");
                }

                @Override
                public void onWriteFailure(BleException exception) {
                    LogUtil.d("sendEffectiveData->onWriteFailure->" + exception.getDescription());
                }
            });

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 发送加密后的结果 分多个包
     *
     * @param bleCode
     */
    @ReactMethod
    private void sendSimpleData(String bleCode) {
        byte[] orginalBleData = bleCode.getBytes();

        LogUtil.d("sendEffectiveData->" + CommonUtils.getHexBinString(orginalBleData));
        BleManager.getInstance().write(curBleDevice, String.valueOf(SERVICE_UUID), String.valueOf(WRITE_CHARACTERISTIC_UUID), orginalBleData, true, new BleWriteCallback() {
            @Override
            public void onWriteSuccess(int current, int total, byte[] justWrite) {
                LogUtil.d("sendEffectiveData->onWriteSuccess");
            }

            @Override
            public void onWriteFailure(BleException exception) {
                LogUtil.d("sendEffectiveData->onWriteFailure->" + exception.getDescription());
            }
        });
    }

    /**
     * 发送加密后的结果 分多个包
     */
    @ReactMethod
    private void sendSimpleDataBytes(String bleCode) {
        BleManager.getInstance().write(curBleDevice, String.valueOf(SERVICE_UUID), String.valueOf(WRITE_CHARACTERISTIC_UUID), HexUtil.hexStringToBytes(bleCode), true, new BleWriteCallback() {
            @Override
            public void onWriteSuccess(int current, int total, byte[] justWrite) {
                LogUtil.d("sendEffectiveData->onWriteSuccess");
            }

            @Override
            public void onWriteFailure(BleException exception) {
                LogUtil.d("sendEffectiveData->onWriteFailure->" + exception.getDescription());
            }
        });


    }

    private byte[] subByte(byte[] b, int off, int length) {
        byte[] b1 = new byte[length];
        System.arraycopy(b, off, b1, 0, length);
        return b1;
    }

    /**
     * 打开notify
     */
    private void openNotify() {
        BleManager.getInstance().setMtu(curBleDevice, 500, new BleMtuChangedCallback() {
            @Override
            public void onSetMTUFailure(BleException e) {

            }

            @Override
            public void onMtuChanged(int i) {
                BleManager.getInstance().notify(
                        curBleDevice,
                        SERVICE_UUID.toString(),
                        NOTIFY_CHARACTERISTIC_UUID.toString(),
                        new BleNotifyCallback() {

                            @Override
                            public void onNotifySuccess() {
                                LogUtil.d("RNBLEModule onNotifySuccess success");

                                WritableMap params = Arguments.createMap();
                                params.putString("status", "onNotifySuccess");
                                sendEvent(getReactApplicationContext(), "RNBLEModule_Notify_Status", params);
                            }

                            @Override
                            public void onNotifyFailure(BleException exception) {
                                LogUtil.d("RNBLEModule onNotifyFailure fail," + exception.getDescription());

                                WritableMap params = Arguments.createMap();
                                params.putString("status", "onNotifyFailure");
                                params.putString("exception", exception.getDescription());
                                sendEvent(getReactApplicationContext(), "RNBLEModule_Notify_Status", params);
                            }

                            @Override
                            public void onCharacteristicChanged(byte[] data) {
                                LogUtil.d("RNBLEModule onCharacteristicChanged," + CommonUtils.getHexBinString(data));
                                try {
                                    parseSimpleBLEData(data);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
        });
    }

    /**
     * 解析收到的蓝牙数据（加密）
     *
     * @param data
     */
    private void parseSimpleBLEData(byte[] data) {
        //最终数据
        WritableMap params = Arguments.createMap();
        params.putString("data", CommonUtils.getHexBinString(data));
        sendEvent(getReactApplicationContext(), "RNBLEModule_Data", params);
    }

    /**
     * 解析收到的蓝牙数据（加密）
     *
     * @param data
     */
    private void parseBLEData(byte[] data) {
        ReadBuffer readBuffer = new ReadBuffer(data, 0);
        //id
        receiveBleDataID = readBuffer.readShort();
        //包序号
        receiveBleDataPackageIndex = readBuffer.readByte();
        if (receiveBleDataPackageIndex == 1) {
            //首包，带crc16 和 数据长度

            //暂时没用，解析完丢掉
            readBuffer.readShort();
            receiveBleDataLength = readBuffer.readShort();

            validData = new ArrayList<>();

            //读取剩余字节
            byte[] surplusData = readBuffer.readEndByte();
            validData.add(surplusData);

            if (data.length < 20) {
                //总数据小于20个字节  直接解析
                int size = 0;
                for (int i = 0; i < validData.size(); i++) {
                    size += validData.get(i).length;
                }
                WriteBuffer writeBuffer = new WriteBuffer(size);
                for (int i = 0; i < validData.size(); i++) {
                    writeBuffer.writeBytes(validData.get(i));
                }

                // LogUtil.d("aa->" + CommonUtils.getHexBinString(writeBuffer.array()));
                try {
                    //crc
                    byte[] decryptData = DesUtil.decrypt2(writeBuffer.array(), encryptionKey);
                    ReadBuffer fianlData = new ReadBuffer(decryptData, 0);
                    byte[] endData = fianlData.readBytes(receiveBleDataLength);
                    LogUtil.d("解密后：" + CommonUtils.getHexBinString(endData));
                    // LogUtil.d("解密后转字符串：" + new String(endData, "utf-8"));

                    //最终数据
                    WritableMap params = Arguments.createMap();
                    params.putString("data", CommonUtils.getHexBinString(endData));
                    sendEvent(getReactApplicationContext(), "RNBLEModule_Data", params);
                } catch (Exception e1) {
                    LogUtil.d("解密后异常");
                    e1.printStackTrace();
                }
            }
        } else {
            //读取剩余字节
            byte[] surplusData = readBuffer.readEndByte();
            validData.add(surplusData);

            if (surplusData.length < 17) {
                int size = 0;
                for (int i = 0; i < validData.size(); i++) {
                    size += validData.get(i).length;
                }
                WriteBuffer writeBuffer = new WriteBuffer(size);
                for (int i = 0; i < validData.size(); i++) {
                    writeBuffer.writeBytes(validData.get(i));
                }

                // LogUtil.d("aa->" + CommonUtils.getHexBinString(writeBuffer.array()));
                try {
                    //crc
                    byte[] decryptData = DesUtil.decrypt2(writeBuffer.array(), encryptionKey);
                    ReadBuffer fianlData = new ReadBuffer(decryptData, 0);
                    byte[] endData = fianlData.readBytes(receiveBleDataLength);
                    LogUtil.d("解密后：" + CommonUtils.getHexBinString(endData));
                    // LogUtil.d("解密后转字符串：" + new String(endData, "utf-8"));

                    //最终数据
                    WritableMap params = Arguments.createMap();
                    params.putString("data", CommonUtils.getHexBinString(endData));
                    sendEvent(getReactApplicationContext(), "RNBLEModule_Data", params);
                } catch (Exception e1) {
                    LogUtil.d("解密后异常");
                    e1.printStackTrace();
                }
            }
        }
    }
}
