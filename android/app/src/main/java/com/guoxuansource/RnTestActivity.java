package com.guoxuansource;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.facebook.react.bridge.ReactContext;
import com.granwin.apkit.utils.LogUtil;

public class RnTestActivity extends ReactActivity {
    Button btnClose;
    Button btnTo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rn_test);

        btnClose=findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"*****",Toast.LENGTH_LONG);
                finish();
            }
        });
        btnTo=findViewById(R.id.btn_to);
        btnTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUtil.d("haoge6666666666666666");
                RNBLEModule.sendEvent(getReactNativeHost().getReactInstanceManager().getCurrentReactContext(), "NativeToRn_Event",null);
                finish();
            }
        });
    }
}