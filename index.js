/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
// import Login from "./reactnative/Main/Login";
import Enter from "./reactnative/Main/Enter"
import I18n from "i18n-js";
import StorageHelper from "./reactnative/Main/Utils/StorageHelper";
import {strings, setLanguage} from './reactnative/Main/Language/I18n';
// import cn from './reactnative/Main/Language/zh';
// import en from './reactnative/Main/Language/en';
// import NewMine from './reactnative/Main/Mine/NewMine';
// import NewAccount from './reactnative/Main/Mine/NewAccount';
// import MessegeSetting from './reactnative/Main/Mine/MessageSetting';
// import LanguageChoose from './reactnative/Main/Mine/LanguageChoose';
// import AccountSave from './reactnative/Main/Mine/AccountSave';
// import NewChangePassword from './reactnative/Main/Mine/NewChangePassword';
// import SignOutAccount from './reactnative/Main/Mine/SignOutAccount';
// import NewAboutUs from './reactnative/Main/Mine/NewAboutUs';
// import ShareManage from './reactnative/Main/Mine/ShareManage';
// import DeviceShareMember from './reactnative/Main/Mine/DeviceShareMember';
// import PersonalInfoProtection from './reactnative/Main/Mine/PersonalInfoProtection';
// import SystemPermissionsManagement from './reactnative/Main/Mine/SystemPermissionsManagement';
// import NewUseHelp from './reactnative/Main/Mine/NewUseHelp';
// import NewUseHelpDeviceDetails from './reactnative/Main/Mine/NewUseHelpDeviceDetails';
// import NewUseHelpDetails from './reactnative/Main/Mine/NewUseHelpDetails';
// import SuggestionAndFeedback from './reactnative/Main/Mine/SuggestionAndFeedback';
// import NewFeedback from './reactnative/Main/Mine/NewFeedback';
// import SelectDeviceList from './reactnative/Main/Mine/SelectDeviceList';
// import NewSelectDeviceList from './reactnative/Main/Mine/NewSelectDeviceList';
// import SelectProductListView from './reactnative/Main/Mine/SelectProductListView';
// import afterSalesApplication from './reactnative/Main/Mine/afterSalesApplication';
// import BatteryDetails from './reactnative/Main/Device/BatteryDetails';
// import ChargingPackage from './reactnative/Main/Device/ChargingPackage';
// import BatterySetting from './reactnative/Main/Device/BatterySetting';
// import BatteryHealth from './reactnative/Main/Device/BatteryHealth';
// import RegularSettings from './reactnative/Main/Device/RegularSettings';
// import DeviceUpgrade from './reactnative/Main/Device/DeviceUpgrade';
// import DeviceUpgradeDetails from './reactnative/Main/Device/DeviceUpgradeDetails';
// import IntelligentSelfExamination from './reactnative/Main/Device/IntelligentSelfExamination';
// import NewAddDevice from './reactnative/Main/Device/NewAddDevice';
// import ChooseNetwork from './reactnative/Main/Device/ChooseNetwork';
// import ResetDevice from './reactnative/Main/Device/ResetDevice';
// import DeviceAdded from './reactnative/Main/Device/DeviceAdded';
// import ConnectingDeviceWifi from './reactnative/Main/Device/ConnectingDeviceWifi';
// import SelectArea from './reactnative/Main/Mine/SelectArea';
// import SelectCountry from './reactnative/Main/Mine/SelectCountry';
// import HeadsetDetail from './reactnative/Main/Device/Headset/HeadsetDetail';
StorageHelper.getLanguage()
.then((value) => {
    if (value) {
        setLanguage(value)
    }else{
        // 如果本地没有已设置的语言类型，则默认设置中文
        setLanguage('zh')
        StorageHelper.saveLanguage('zh')
    }
})

AppRegistry.registerComponent(appName, () => Enter);


// 关闭其中某些yellow警告
console.ignoredYellowBox = ['Warning: BackAndroid is deprecated. Please use BackHandler instead.', 'source.uri should not be an empty string', 'Invalid props.style key'];
// 关闭全部yellow警告
console.disableYellowBox = true;