/**
 * To find your Firebase config object:
 *
 * 1. Go to your [Project settings in the Firebase console](https://console.firebase.google.com/project/_/settings/general/)
 * 2. In the "Your apps" card, select the nickname of the app for which you need a config object.
 * 3. Select Config from the Firebase SDK snippet pane.
 * 4. Copy the config object snippet, then add it here.
 */
const config = {
    apiKey: "rQQoqUJVRzWrANtVoR3aDw",
    authDomain: "gendome-45dfe.firebaseapp.com",
    databaseURL: "https://gendome-45dfe.firebaseio.com",
    projectId: "gendome-45dfe",
    storageBucket: "gendome-45dfe.appspot.com",
    messagingSenderId: "84864021613",
    appId: "1:84864021613:web:d7128b6168717d79d5fb64",
    measurementId: "G-T9YY6QD1NC",
};

export function getFirebaseConfig() {
    if (!config || !config.apiKey) {
        throw new Error('No Firebase configuration object provided.' + '\n' +
            'Add your web app\'s configuration object to firebase-config.js');
    } else {
        return config;
    }
}