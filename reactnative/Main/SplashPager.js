import BaseComponent from '../Main/Base/BaseComponent';
import React from 'react';
import {Image, StatusBar, View} from 'react-native';
import StorageHelper from "./Utils/StorageHelper";
import RouterUtil from "./Utils/RouterUtil";
import {setLanguage, strings} from './Language/I18n';
import ViewHelper from "./View/ViewHelper";
// import { Amplify } from 'aws-amplify';
// import awsconfig from '../../src/aws-exports';
import {Settings} from 'react-native-fbsdk-next';
import NetConstants from "./Net/NetConstants";
import NetUtil from "./Net/NetUtil";
import LogUtil from "./Utils/LogUtil";
import MessageDialog from "./View/MessageDialog";
import RNExitApp from 'react-native-exit-app';

/*
*启动页
 */
//Amplify.configure(awsconfig);
Settings.initializeSDK()
Settings.setAppID(NetConstants.FACEBOOK_APPID)
export default class SplashPager extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            firstEnterDialog: false,
        }
    }

    componentWillMount() {
        this._setSaveLanguage()

        StorageHelper.getFirstEnter().then((res) => {
            if (res) {
                RouterUtil.setProps(this.props)

                //倒计时
                setTimeout(() => {
                    this._toWay()
                }, 1000);
            } else {
                //首次进入
                this.setState({
                    firstEnterDialog: true
                })
            }
        }).catch((error) => {
            console.log('error+' + JSON.stringify(error))
        })
    }

    //设置用户设置的语言
    _setSaveLanguage() {
        StorageHelper.getLanguage()
            .then((value) => {
                if (value) {
                    console.log('value: ', value);
                    setLanguage(value)
                } else {
                    console.log('>>>>>');
                    // 如果本地没有已设置的语言类型，则默认设置中文
                    setLanguage('zh')
                    StorageHelper.saveLanguage('zh')
                }
            })
    }

    render() {
        StatusBar.setBarStyle('light-content');
        return (
            <View
                //source={require('../resources/log_bg.png')}
                style={{
                    flex: 1,
                    alignItems: 'center',
                }}>
                <StatusBar translucent={true} backgroundColor="transparent"/>

                {ViewHelper.getFlexView()}

                {/*{this._logoView()}*/}

                {ViewHelper.getFlex2View()}

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({firstEnterDialog: false})
                        RNExitApp.exitApp();
                    }}
                    overViewClick={() => {
                        this.setState({firstEnterDialog: false})
                    }}
                    modalVisible={this.state.firstEnterDialog}
                    title={strings('个人信息保护条款')}
                    message={strings('个人信息保护条款')}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({firstEnterDialog: false})
                        RNExitApp.exitApp();
                    }}
                    rightBtnText={strings('confirm')}
                    rightBtnTextColor={'#F77979'}
                    onRightBtnClick={() => {
                        //确认
                        this.setState({firstEnterDialog: false})
                        StorageHelper.saveFirstEnter(1)

                        RouterUtil.setProps(this.props)

                        //倒计时
                        setTimeout(() => {
                            this._toWay()
                        }, 1000);
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>
            </View>
        );
    }

    _logoView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center'
                }}>
                <Image
                    style={{
                        width: this.viewCommonWith,
                        resizeMode: 'contain',
                    }}
                    source={require('../resources/logo_white_ic.png')}/>
            </View>
        )
    }

    _toWay() {
        StorageHelper.getToken().then((token) => {
            LogUtil.debugLog('StorageHelper.getToken()->' + token)
            if (token == undefined || token == 'undefined') {
                this._toLogin()
            } else {
                StorageHelper.getLoginInfo().then((res) => {
                    //LogUtil.debugLog('refreshToken----->res:' + JSON.stringify(res))
                    LogUtil.debugLog('refreshToken----->res.info.refreshToken：' + res.info.refreshToken)
                    this._refreshToken(token, res.info.refreshToken)
                }).catch((error) => {
                    LogUtil.debugLog('refreshToken----->res.info.refresh：error:' + JSON.stringify(error))
                    this._toLogin()
                })
            }
        })
    }

    _toLogin() {
        this.props.navigation.replace('Login');
    }

    _toMain(token) {
        StorageHelper.setTempToken(token)
        this.props.navigation.replace('Home');
    }

    _refreshToken(token, refreshToken) {
        LogUtil.debugLog('refreshToken----->当前Token：' + token)
        NetUtil.refreshToken(token, refreshToken, false).then((res) => {
            var newToken = res?.info?.token
            StorageHelper.saveToken(newToken)
            StorageHelper.saveLoginInfo(res)
            LogUtil.debugLog('refreshToken----->刷新后的Token：' + newToken)
            this._toMain(newToken)
        }).catch((error) => {
            this._toLogin()
        })
    }
}