import {XHttp} from 'react-native-easy-app';
import NetConstants from './NetConstants'
import Toast from 'react-native-root-toast';
import RouterUtil from "../Utils/RouterUtil";
import StorageHelper from "../Utils/StorageHelper";
import LoadingUtil from "../View/Loading/LoadingUtil";
import Helper from "../Utils/Helper";
import {strings} from "../Language/I18n";
import ErrorCodeUtil from "../Utils/ErrorCodeUtil";
import {Platform} from "react-native";
import LogUtil from "../Utils/LogUtil";
import I18n from 'react-native-i18n';

var canToLoginFlag = true
var netStatus = 200
var netMessage = ''
var netRequestUrl = ''
var lang
export default class NetUtil {
    /*
     *app用户账户密码登录接口
     * account账号（邮箱）
     * password密码
     */
    static userLogin(account, password, showLoading) {
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_LOGIN
                + '?account=' + account + '&password=' + password + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
     *app用户验证码登录接口
     * account账号（邮箱）
     * code 验证码
     */
    static userCodeLogin(account, code, showLoading) {
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_LOGIN_code
                + '?account=' + account + '&code=' + code + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
     *注册
     * merchantId商户ID
     * account账号（邮箱）
     * password密码
     * code验证码
     * phoneCode 国家手机区号 比如 中国 +86
     */
    static userRegister(account, password, code, phoneCode, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_REGISTER
                + '?account=' + account + '&password=' + password + '&merchantId=' + NetConstants.MERCHANTID + '&code=' + code + '&phoneCode' + phoneCode
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *获取邮箱验证码
    * type类型  REGISTER 注册 PWD_RESET 忘记密码 LOGIN 登录
    * email账号（邮箱）
    * merchantId  商户编号
    * lang 语言类型zh_CN/en-US 
    */
    static getEmailVerCode(type, account, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.EMAIL_VER_CODE
                + '?type=' + type + '&account=' + account + '&lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
     *密码重置
     * account账号（邮箱）
     * newPassword新密码
     * code验证码
     */
    static pdReset(account, password, code, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_PD_RESET
                + '?account=' + account + '&password=' + password + '&code=' + code + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
     *广告接口  banner
     * apId广告位置 ID    1 运营 2 首页轮播 3 启动页面
     */
    static getBannerAd(token) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.BANNER_AD
                + '?apId=' + NetConstants.BANNER_TYPE1 + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, false, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
     *消息中心/设备推送
     * pageNum 页码
     * pageSize 每页数量
     * lang 语言类型 如：zh_CN
     */
    static devicePush(token, pageNum, pageSize, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_PUSH
                + '?pageNum=' + pageNum + '&pageSize=' + pageSize + '&lang=' + lang
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
     *消息中心/设备分享
     * pageNum 页码
     * pageSize 每页数量
     * lang 语言类型 如：zh_CN
     */
    static deviceShare(token, pageNum, pageSize, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_SHARE
                + '?pageNum=' + pageNum + '&pageSize=' + pageSize + '&lang=' + lang
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
     *消息中心/系统通知
     * pageNum 页码
     * pageSize 每页数量
     * lang 语言类型 如：zh_CN
     */
    static systemNotice(token, pageNum, pageSize, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.SYSTEM_PUSH
                + '?pageNum=' + pageNum + '&pageSize=' + pageSize + '&lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *获取用户信息
    */
    static getUserInfo(token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_INFO_GET
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *密码更新
    */
    static updatePassword(token, oldPassword, newPassword, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_PD_UPDATE
                + '?oldPassword=' + oldPassword + '&newPassword=' + newPassword
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *设备列表
  */
    static getDeviceList(token, showLoading, hideErrorToast, flag, count) {
        var requestParams = {}
        flag && (requestParams['flag'] = flag)
        count && (requestParams['count'] = count)
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_LIST,
                requestParams, 'POST', token, showLoading, hideErrorToast)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *设备推送消息详情
  devicePushId 设备消息id
  */
    static devicePushDetails(token, devicePushId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_PUSH_DETAILS
                + '?devicePushId=' + devicePushId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *设备分享消息详情
 deviceShareId 设备分享消息id
 receive 1-接收 2-稍后处理
 deviceNickName 设备名称
 */
    static deviceReceveDetails(token, deviceShareId, receive, deviceNickName, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_RECEVE_DETAILS
                + '?deviceShareId=' + deviceShareId + '&receive=' + receive + '&deviceNickName=' + deviceNickName + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *系统公告消息详情
  noticePushId 公告推送id
  */
    static sysNoticeDetails(token, noticePushId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.SYS_NOTICE_DETAILS
                + '?noticePushId=' + noticePushId + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *应用使用帮助
    */
    static getApplicationHelpList(token, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.APPLICATIONHELP_LIST
                + '?lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *产品使用帮助
    blurry:模糊搜索
    productId: 产品id
    */
    static getHelpList(token, productId, blurry, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.HELP_LIST
                + '?productId=' + productId + '&lang=' + lang + '&blurry=' + blurry + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *提交问题反馈
    deviceId: 设备id
    type: 反馈类型 1-账号问题 2-固件升级问题 3-体验问题 4-性能问题 5-友好建议
    source: 来源 【0】ios 【1】Android
    userContent: 用户提交的内容
    contactInformation: 联系方式
    userFiles: 文件列表，约定为json数组格式[{“fileName”:”xxx”,”fileUrl”:”xxx”,”fileType”:”xxx”}] 文件类型 picture-图片 video-视频
    productId: 产品id
    deviceSN: 设备sn
    */
    static addFeedback(token, type, source, userContent, contactInformation, userFiles, productId, deviceId, deviceSN, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {deviceId, type, source, userContent, contactInformation, userFiles, productId, deviceSN}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FEEDBACK_ADD
                + '?lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *提交售后申请
    type	RETURN_GOODS	是	string	售后类型 RETURN_GOODS-退货 EXCHANGE_GOODS-换货 REPAIR_GOODS-维修 OTHER-其他
    source	0	是	string	来源 【0】ios 【1】Android
    productId	2344	否	string	产品id
    deviceSn	1234	否	string	设备sn
    deviceId		否	string	设备id
    orderId	123	是	string	订单编号
    channel	淘宝	是	string	渠道
    contactInformation	13875110551	是	string	联系方式
    contacts	caoyu	是	string	联系人
    contactAddress	阿里巴巴	是	string	联系地址
    userContent	不好用	是	string	用户提交内容
    userFiles	[{“fileName”:”xxx”,”fileUrl”:”xxx”,”fileType”:”video”}]	是	string	用户提交文件 约定为json数组格式[{“fileName”:”xxx”,”fileUrl”:”xxx”,”fileType”:”xxx”}] 文件类型 picture-图片 video-视频
    */
    static addAfterSales(token, type, productId, deviceId, deviceSn, orderId, channel, contactInformation, contacts, contactAddress, userContent, userFiles, source, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {
            type,
            productId,
            deviceId,
            deviceSn,
            orderId,
            channel,
            contactInformation,
            contacts,
            contactAddress,
            userContent,
            userFiles,
            source
        }
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.AFTER_SALES_ADD
                + '?lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *问题反馈或申请售后列表
    pageNum	1	是	string	无
    pageSize	10	是	string	无
    */
    static recordList(token, pageNum, pageSize, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.RECORD_LIST
                + '?pageNum=' + pageNum + '&pageSize=' + pageSize
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *更新反馈状态
    feedbackId	890145084082061312	是	string	无
    solveStatus	UNSOLVED	是	string	解决状态 UNSOLVED-未解决 SOLVED-已解决 PROCESSING-处理中 UNDETERMINED-待确认
    */
    static upDateFeedback(token, feedbackId, solveStatus, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.UPDAPE_FEEDBACK
                + '?feedbackId=' + feedbackId + '&solveStatus=' + solveStatus
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *更新售后状态
    afterSalesId	890479074808700928	是	string	售后id
    customerTrackingNumber	123	否	string	快递单号
    */
    static upDateAfterSale(token, afterSalesId, customerTrackingNumber, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.UPDAPE_AFTER_SALES
                + '?afterSalesId=' + afterSalesId + '&customerTrackingNumber=' + customerTrackingNumber
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *获取固件列表
    deviceId		否	string	设备id
    mac	G06FC2303212585	否	string	设备mac
    lang	zh_CN	是	string	语言类型
    */
    static firmwareList(token, deviceId, mac, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FIRMWARE_LIST
                + '?deviceId=' + deviceId + '&mac=' + mac + '&lang=' + lang
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *固件升级
    upgradeId	895839061517692928	是	string	升级id
    */
    static firmwareUpgrade(token, upgradeId, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FIRMWARE_UPGRADE
                + '?upgradeId=' + upgradeId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *获取升级进度
    upgradeId	895839061517692928	是	string	升级id
    */
    static firmwareUpgradeProgress(token, upgradeId, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FIRMWARE_UPGRADE_PROGRESS
                + '?upgradeId=' + upgradeId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *获取升级历史
    mac	G06FC2303212585	否	string	无
    device		否	string	无
    pageNum	1	是	string	页码
    pageSize	10	是	string	页码大小
    lang	zh_CN	是	string	语言类型
    */
    static firmwareUpgradeLog(token, mac, device, pageNum, pageSize, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FIRMWARE_UPGRADE_LOG
                + '?mac=' + mac + '&device=' + device + '&pageNum=' + pageNum + '&pageSize=' + pageSize + '&lang=' + lang
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *APP上报升级结果
    upgradeId	895932453661917184	是	string	升级id
    status	UPGRADING	是	string	升级状态 SUCCESS-成功 FAIL-失败 UPGRADING-升级中
    progress	20	否	string	无
    */
    static upgradeResultReport(token, upgradeId, status, progress, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.UPGRADE_RESULT_REPORT
                + '?upgradeId=' + upgradeId + '&status=' + status + '&progress=' + progress
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
      *查询设备分享成员
      deviceId 设备ID
      flag 1—获取所有成员 2-排除设备所有者
      */
    static deviceShareMember(token, deviceId, flag, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_SHARE_MEMBER
                + '?deviceId=' + deviceId + '&flag=' + flag
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *系统消息和设备推送一键已读
  */
    static allRead(token, type, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.ALL_READ
                + '?type=' + type + '&lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *系统消息和设备推送一键已读
  */
    static getMsgCount(token, showLoading) {
        lang = I18n.locale == 'zh' ? 'zh_CN' : 'en_US';
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.MSG_COUNT
                + '?lang=' + lang + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *获取服务协议
  */
    static getRule(token) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_RULE
                + '?merchantId=' + NetConstants.MERCHANTID + '&languageType=' + Helper.getLanguageType()
                , requestParams, 'POST', token)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *获取隐私协议
 */
    static getPrivacy(token) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_PRIVACY
                + '?merchantId=' + NetConstants.MERCHANTID + '&languageType=' + Helper.getLanguageType()
                , requestParams, 'POST', token)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *绑定设备
 */
    static bindDevice(token, mac, hideErrorToast) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.BIND_DEVICE
                + '?mac=' + mac + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, false, hideErrorToast)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
*设备评分
*/
    static deviceGrade(token, remark, grade, deviceId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_GRADE
                + '?remark=' + remark + '&grade=' + grade + '&source=' + Helper.getPlatformOsFlag() + '&deviceId=' + deviceId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *获取设备属性
    */
    static getDeviceProps(token, deviceId, showLoading, hideErrorToast) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_DEVICE_SPEC
                + '?deviceId=' + deviceId
                , requestParams, 'POST', token, showLoading, hideErrorToast)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *获取使用帮助
    */
    static getUseHelp(token, productId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USE_HELP
                + '?productId=' + productId + '&languageType=' + Helper.getLanguageType()
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *获取使用帮助详情
    */
    static getUseHelpDetail(token, id, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.HELP_DATA_CONTENT
                + '?id=' + id
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *更改设备名称
    */
    static updateDeviceName(deviceNickName, token, deviceId, showLoading) {
        var requestParams = {
            deviceId: deviceId,
            deviceName: deviceNickName
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_UPDATE_DEVICE
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *删除设备
    */
    static delDevice(deviceId, token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.del_DEVICE
                + '?deviceId=' + deviceId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *设备分享
    */
    static shareDevice(token, deviceId, account, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_SHARE_DEVICE
                + '?deviceId=' + deviceId + '&account=' + account + '&type=2' + '&merchantId=' + NetConstants.MERCHANTID
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *删除设备成员
    deviceId 设备id
    userId 用户id
    */
    static removeDeviceMember(token, deviceId, userId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.REMOVE_DEVICE_MEMBER
                + '?deviceId=' + deviceId + '&userId=' + userId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *解绑设备
    */
    static unbindDevice(token, deviceId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.UNBIND_DEVICE
                + '?deviceId=' + deviceId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
   *分享设备
   */
    // static shareDevice(token, deviceId, email, showLoading) {
    //     var requestParams = {}

    //     var p = new Promise(function (resolve, reject) {
    //         NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.SHARE_DEVICE_TO_USER
    //             + '?deviceId=' + deviceId + '&email=' + email
    //             , requestParams, 'POST', token, showLoading)
    //             .then((res) => {
    //                 resolve(res)
    //             })
    //             .catch((error) => {
    //                 reject(error)
    //             })
    //     })

    //     return p
    // }

    /*
   *修改用户信息
   */
    static updateUserInfo(showLoading, token, name, avatar, background) {
        var requestParams = {}
        if (name) {
            requestParams['name'] = name
        }
        if (avatar) {
            requestParams['avatar'] = avatar
        }
        if (background) {
            requestParams['background'] = background
        }


        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_USER_UPDATE
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
  *获取文件上传地址
  */
    static getFileUploadUrl(token, fileName, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FILE_UPLOAD_URL
                + '?fileName=' + fileName
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *app用户文件上传(新)
    */
    static uploadFile(token, file, showLoading) {
        if (!file) {
            return
        }

        if (showLoading) {
            LoadingUtil.showLoading(NetConstants.TIMEOUT)
        }

        let headers = {
            'Content-Type': 'multipart/form-data',
            'Authorization': token,
            'wl-lang': I18n.locale == 'zh' ? 'zh_CN' : 'en_US',
        }

        let body = new FormData();
        body.append('file', file);

        var p = new Promise(function (resolve, reject) {
            fetch(NetConstants.HTTP_URL + NetConstants.UPLOAD_FILE, {
                method: 'POST',
                headers,
                body,
            })
                .then((response) => response.text())
                .then((responseData) => {
                    LogUtil.debugLog('上传图片====》' + JSON.stringify(responseData))
                    resolve(responseData)
                    if (showLoading) {
                        LoadingUtil.dismissLoading()
                    }
                })
                .catch((error) => {
                    LogUtil.debugLog('error', error)
                    reject(error)
                    if (showLoading) {
                        LoadingUtil.dismissLoading()
                    }
                });

        })

        return p


    }

    /*
    *上传文件
    */
    static uploadFile(preUrl, file, showLoading) {
        if (preUrl == '' || file == '') {
            return
        }

        if (showLoading) {
            LoadingUtil.showLoading(NetConstants.TIMEOUT)
        }
        var p = new Promise(function (resolve, reject) {
            fetch(preUrl, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'image/png;image/jpeg;',
                },
                body: file
            })
                .then((response) => response.text())
                .then((responseData) => {
                    LogUtil.debugLog('上传图片====》' + JSON.stringify(responseData))
                    resolve(responseData)
                    if (showLoading) {
                        LoadingUtil.dismissLoading()
                    }
                })
                .catch((error) => {
                    LogUtil.debugLog('error', error)
                    reject(error)
                    if (showLoading) {
                        LoadingUtil.dismissLoading()
                    }
                });

        })

        return p


    }

    /*
   *注销账号
   */
    static userCancelAccount(token, account, password, code, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_DEL
                + '?account=' + account + '&password=' + password + '&code=' + code
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *意见反馈
    * type 【1】 app反馈、【 2】设备反馈、 【3】 其他
    * source【0】ios 【1】Android
    */
    static feedback(token, deviceId, type, remark, source, imgs, showLoading) {
        var requestParams = {
            type: type,
            remark: remark,
            source: source,
        }

        if (deviceId) {
            requestParams['deviceId'] = deviceId
        }

        if (imgs) {
            requestParams['imgs'] = imgs
        }
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.FEEDBACK
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *共享给我 - 设备列表
    */
    static shareWithMeDeviceList(token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.SHARE_WITH_ME_DEVICE_LIST
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *设备分享推送消息列表
    */
    static getDeviceSharePushList(token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_SHARE_PUSH_INFO
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        // fetch(NetConstants.HTTP_URL + NetConstants.DEVICE_SHARE_PUSH_INFO, {
        //     method: "POST",
        //     headers: {
        //         'Content-Type': 'application/json',
        //         'token': token
        //     },
        // }).then((response) => response.text())
        //     .then((response) => {
        //         //alert(JSON.stringify(JSONbig(response).parse)+'*****'+JSON.stringify(response))
        //
        //         const str = '{ "id": 1253585734669959168 }'
        //         //var data=str.replace(/("id":)(\d{0,})(,)/g, '$1'+'"'+'$2'+'"'+'$3')
        //         var data=response.toString().replace(/\"id\":(\d+)/g,'"id": "$1"')
        //             .replace(/\"userId\":(\d+)/g,'"userId": "$1"')
        //
        //         //alert(JSON.stringify(JSON.parse(str))) // 1253585734669959200
        //         alert('****'+JSON.stringify(JSON.parse(data)))
        //         //alert(JSONbig.parse(str).id.toString()) // 1253585734669959168
        //     }).catch((error) => {
        //     alert(JSON.stringify(error))
        // });

        return p
    }

    /*
    *接收用户分享设备
    * deviceNickname  app用户设置的设备昵称
    * order  分享口令
    */
    static acceptShareDevice(token, deviceNickname, order, showLoading) {
        var requestParams = {
            deviceNickname: deviceNickname,
            order: order
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.ACCEPT_SHARE_DEVICE
                , requestParams, 'POST', token, showLoading, false)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
    *设备分享推送消息列表
    */
    static getDeviceSharePushDetail(token, id, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_SHARE_PUSH_DETAIL + '?id=' + id
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
   *通过v3 dp控制设备
    deviceId		否	string	设备id，设备id和mac二选一
    mac	G06FC2212140609	否	string	设备mac，设备id和mac二选一
    propertyName	switch_all	否	string	属性名称
    propertyValue		否	string	属性值
    qos		否	string	0-最多一次 1-至少一次,默认为1
    propertyMap	{“switch_all”:”1”}	否	string	属性批量下发，propertyMap和propertyName填写一份即可
   */
    static deviceControlV3(token, deviceId, keys, value, showLoading, more) {
        var requestParams
        if (more) {
            requestParams = {
                deviceId: deviceId,
                propertyMap: keys
            }
        } else {
            requestParams = {
                deviceId: deviceId,
                propertyName: keys,
                propertyValue: value,
            }
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_CONTROL_V3
                , requestParams, 'POST', token, showLoading, false)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }


    /*
   *通过16进制控制设备
   */
    static deviceControl16(token, sendData, showLoading) {
        var requestParams = {}
        requestParams = sendData

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_CONTROL_16
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *可取消设备分享的列表
    */
    static getCanCancelShareList(token, deviceId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.CAN_CANCEL_SHARE_DEVICE_LIST + '?deviceId=' + deviceId
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *设备所有者取消分享
    */
    static deviceOwnerCancalShare(token, deviceId, userId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.DEVICE_OWN_REMOVE_SHARE + '?deviceId=' + deviceId + '&userId=' + userId
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *批量获取设备属性
  */
    static getSpecsByDeviceId(token, devices, showLoading) {
        var requestParams = {
            'macList': devices
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_ALL_DEVICE_SPEC
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
  *获取MCU新版本
  */
    static getMcuVersion(token, deviceId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_MCU_VERSION + '?deviceId=' + deviceId
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *升级MCU
 * upgradeId MCU版本中的 升级ID
 * decide 决定，1不升级(忽略本次升级) 2升级(确定升级) 0无效的决定
 */
    static updateMcu(token, upgradeId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.UPDATE_MCU_VERSION + '?upgradeId=' + upgradeId + '&decide=2'
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *根据ip地址，获取地址信息
 */
    static getLocationByIp(ip, showLoading) {
        var requestParams = {}

        //?ak=50v83o72ilaTsoxiGNDl2w5H4NvKWIi4&ip=120.238.191.116&coor=bd09ll
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.BD_LOCATION_URL + '?ak=50v83o72ilaTsoxiGNDl2w5H4NvKWIi4&coor=bd09ll&ip=' + ip
                , requestParams, 'GET', null, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *查询属性日志
 */
    static getDeviceSpecLog(token, deviceId, attribute, exclusiveStartKey, startTime, endTime, showLoading) {
        var requestParams = {
            id: deviceId,
            attribute: attribute,
            startTime: startTime,
            endTime: endTime
        }
        // var queryParams = '&attribute=' + attribute
        // var param1 = exclusiveStartKey ? ('&exclusiveStartKey=' + exclusiveStartKey) : ''
        // var param2 = startTime ? ('&startTime=' + startTime) : ''
        // var param3 = endTime ? ('&endTime=' + endTime) : ''
        // queryParams = queryParams + param1 + param2 + param3
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_DEVICE_SPEC_LOG
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *获取折线图
 */
    static getLineData(token, mac, deviceId, attribute, showLoading) {
        var requestParams = {
            attribute: attribute,
        }

        if (mac) {
            requestParams['mac'] = mac
        }
        if (deviceId) {
            requestParams['deviceId'] = deviceId
        }


        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_LINE_DATA
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
   *Facebook授权验证
   */
    static facebookAuth(accessToken, showLoading) {
        var requestParams = {
            clientId: NetConstants.FACEBOOK_APPID,
            accessToken: accessToken
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.AUTH_FACEBOOK
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
   *Google授权验证
   */
    static googleAuth(idToken, showLoading) {
        var requestParams = {
            clientId: Platform.OS == 'android' ? NetConstants.GOOGLE_ANDROID_CLIENTID : NetConstants.GOOGLE_IOS_CLIENTID,
            idToken: idToken
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.AUTH_GOOGLE
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
   *第三方授权后绑定账号
   * 邮箱 验证码 用户名字 后台返回的唯一值 密码
   */
    static thirdBind(email, code, name, key, showLoading) {
        var requestParams = {
            "email": email,
            "code": code,
            "name": name,
            "key": key
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.THIRD_BIND_ACCOUNT
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
   *获取用户所有第三方授权商
   */
    static getThirdInfo(token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_THIRD_INFO
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
   *解除第三方授权绑定
   */
    static unBindThird(token, channel, showLoading) {
        var requestParams = {
            channel: channel
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.UNBIND_THIRD
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
  *Apple授权验证
  */
    static appleAuth(identityToken, showLoading) {
        var requestParams = {
            clientId: NetConstants.APPLE_CLIENTID,
            identityToken: identityToken
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.AUTH_APPLE
                , requestParams, 'POST', null, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
  *产品分类列表
  */
    static getProductClassList(token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.PRODUCT_CLASS_LIST
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
*产品列表
*/
    static getProductList(token, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.PRODUCT_LIST
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
   *已有账号绑定第三方账号
   */
    static accountBindThird(clientId, idToken, channel, token, showLoading) {
        if (token == '' || token == undefined || token == null) {
            return
        }
        var requestParams = {
            clientId: clientId,
            idToken: idToken,
            channel: channel
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.ACCOUNT_BIND_THIRD
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
  *获取MCU升级状态
  */
    static getMcuUpgradeStatus(upgradeId, token, showLoading) {
        var requestParams = {}
        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.GET_MCU_UPGRADE_STATUS + '?upgradeId=' + upgradeId
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
  *首次绑定设置用户密码接口
  */
    static userSetPd(password, userKey, token, showLoading) {
        var requestParams = {
            "password": password,
            "userKey": userKey
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_SET_PD
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })
        return p
    }

    /*
   *产品配网说明
   */
    static getProductConnectNetTips(token, productId, showLoading) {
        var requestParams = {}

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.PRODUCT_CONNECT_NET_TIPS
                + '?productId=' + productId + '&languageType=' + Helper.getLanguageType()
                , requestParams, 'POST', token, showLoading)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
 *设置用户终端上报
 */
    static setUserUp(token, awsToken, status, showLoading, hideErrorToast) {
        var requestParams = {
            token: awsToken,
            channel: Helper.getMobileChannel(),
            status: status
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_UP
                //+ '?token=' + awsToken + '&channel=' + Helper.getMobileChannel() + '&status=' + status
                , requestParams, 'POST', token, showLoading, hideErrorToast)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
    *设置附加参数
    */
    static setUserParams(token, param, showLoading) {
        var requestParams = {
            param: param
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.USER_SET_PARAM
                , requestParams, 'POST', token, showLoading, false)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    /*
   *设置附加参数
   */
    static refreshToken(token, refreshToken, showLoading) {
        var requestParams = {
            refreshToken: refreshToken
        }

        var p = new Promise(function (resolve, reject) {
            NetUtil.commonNetRequest(NetConstants.HTTP_URL + NetConstants.REFRESH_TOKEN
                , requestParams, 'POST', token, showLoading, true)
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //封装统一请求
    //hideErrorToast  true为请求返回错误时，不显示错误toast，默认为false
    static commonNetRequest(requestUrl, requestParams, requestType, token, showLoading, hideErrorToast) {
        if (showLoading) {
            LoadingUtil.showLoading(NetConstants.TIMEOUT)
        }

        var p = new Promise(function (resolve, reject) {
            if (requestType == 'GET') {
                NetUtil._commonNetRequestGet(requestUrl, requestType, resolve, reject, token, hideErrorToast, showLoading)
            } else if (requestType == 'POST') {
                NetUtil._commonNetRequestPost(requestUrl, requestParams, requestType, resolve, reject, token, hideErrorToast, showLoading, StorageHelper.getTempToken())
            }
        })

        return p
    }

    //统一Get请求
    static _commonNetRequestGet(requestUrl, requestType, resolve, reject, token, hideErrorToast, showLoading) {
        var header
        if (token) {
            header = {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        } else {
            header = {
                'Content-Type': 'application/json',
            }
        }
        XHttp().url(requestUrl).timeout(NetConstants.TIMEOUT)
            .pureText()//返回text数据
            .header(header)
            .execute(requestType)
            .then(({success, json, message, status}) => {
                netStatus = status
                netMessage = message
                netRequestUrl = requestUrl
                LogUtil.debugLog(`get接口====>\n` + success + '\n' + json + '\n' + message + '\n' + status);

                var replaceJsonText = NetUtil.replaceResponseLongIntData(json)
                var jsonResponse = JSON.parse(replaceJsonText)

                if (success && jsonResponse.code == 0 && status == 200) {
                    resolve(jsonResponse)
                } else {
                    if (jsonResponse.tip) {
                        if (!hideErrorToast)
                            this.onShowToast(ErrorCodeUtil.getErrorCodeText(jsonResponse.code, jsonResponse.tip))
                        this.tokenInvalidToLogin(jsonResponse.code)
                    } else if (status == -1) {
                        this._showNetErrorToast(netStatus, netMessage, netRequestUrl)
                    }
                    reject(jsonResponse)
                }
                if (showLoading) {
                    LoadingUtil.dismissLoading()
                }
            })
            .catch(({message}) => {
                LogUtil.debugLog(requestUrl + '===>请求error：' + JSON.stringify(message))
                reject(message)
                if (showLoading) {
                    LoadingUtil.dismissLoading()
                }
                this._showNetErrorToast(netStatus, netMessage, netRequestUrl)
            });
    }

    //统一Post请求
    static _commonNetRequestPost(requestUrl, requestParams, requestType, resolve, reject, token, hideErrorToast, showLoading, refreshTokenFlag) {
        var header
        if (token) {
            header = {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        } else {
            header = {
                'Content-Type': 'application/json',
            }
        }

        XHttp().url(requestUrl).timeout(NetConstants.TIMEOUT)
            .pureText()//返回text数据
            .header(header)
            .param(requestParams)
            .execute(requestType)
            .then(({success, json, message, status}) => {
                netStatus = status
                netMessage = message
                netRequestUrl = requestUrl
                console.log('请求地址: ', requestUrl);
                // console.log(`post接口====>` + '请求token:' + token + '\n' );
                console.log('请求参数:' + JSON.stringify(requestParams) + '\n');
                // console.log( 'success:' + success + '\n' );
                console.log('json:' + json + '\n');
                // console.log( 'message:' + message + '\n' );
                // console.log( 'status:' + status + '\n' );
                var replaceJsonText = NetUtil.replaceResponseLongIntData(json)
                var jsonResponse = JSON.parse(replaceJsonText)

                if (success && jsonResponse.code == 0 && status == 200) {
                    resolve(jsonResponse)
                } else {
                    if (jsonResponse.tip) {
                        if (!hideErrorToast)
                            this.onShowToast(ErrorCodeUtil.getErrorCodeText(jsonResponse.code, jsonResponse.tip))
                        this.tokenInvalidToLogin(jsonResponse.code, refreshTokenFlag)
                    } else if (status == -1) {
                        this._showNetErrorToast(netStatus, netMessage, netRequestUrl)
                    }
                    reject(jsonResponse)
                }
                if (showLoading) {
                    LoadingUtil.dismissLoading()
                }
            })
            .catch(({message}) => {
                LogUtil.debugLog(requestUrl + '===>请求error：' + JSON.stringify(message))
                reject(message)
                if (showLoading) {
                    LoadingUtil.dismissLoading()
                }
                this._showNetErrorToast(netStatus, netMessage, netRequestUrl)
            });
    }

    static _showNetErrorToast(status, message, requestUrl) {
        if (requestUrl == NetConstants.HTTP_URL + NetConstants.DEVICE_LIST || requestUrl == NetConstants.HTTP_URL + NetConstants.GET_DEVICE_SPEC
            || requestUrl == NetConstants.HTTP_URL + NetConstants.GET_DEVICE_SPEC_LOG) {
            return
        }

        if (status == -1 && message.indexOf('failed') != -1) {
            this.onShowToast(strings('net_error'))
            netStatus = 200
            netMessage = ''
            netRequestUrl = ''
        }
    }

    //token失效跳转登录页
    static tokenInvalidToLogin(code, refreshTokenFlag) {
        if (!canToLoginFlag) {
            return
        }
        LogUtil.debugLog('tokenInvalidToLogin=====>refreshTokenFlag:' + refreshTokenFlag
            + '\nStorageHelper.getTempToken:' + StorageHelper.getTempToken())
        if (code && refreshTokenFlag == StorageHelper.getTempToken()) {
            if (parseInt(code) == 1010) {
                StorageHelper.setTempToken('')
                StorageHelper.clearAllData().then(() => {
                    RouterUtil.toLogin()
                })
            }
        }
    }

    static _setCanToLoginFlag(flag) {
        canToLoginFlag = flag
    }

    static _getCanToLoginFlag() {
        return canToLoginFlag
    }

    static onShowToast = (message) => {
        Toast.show(message, {
            duration: Toast.durations.SHORT,
            position: Toast.positions.CENTER,
            shadow: false,
            backgroundColor: 'rgba(122,122,122,0.9)',
            textColor: '#fff',
            onShow: () => {

            },
            onShown: () => {

            },
            onHide: () => {

            },
            onHidden: () => {

            }
        });
    }

    //由于js处理长整型会有精确度问题，将接口返回的长整型替换成字符串
    static replaceResponseLongIntData(responseText) {
        var replaceResponseText = responseText.toString()
            .replace(/\"id\":(\d+)/g, '"id": "$1"')
            .replace(/\"userId\":(\d+)/g, '"userId": "$1"')
            .replace(/\"createBy\":(\d+)/g, '"createBy": "$1"')
            .replace(/\"merchantId\":(\d+)/g, '"merchantId": "$1"')
            .replace(/\"updateBy\":(\d+)/g, '"updateBy": "$1"')
            .replace(/\"advertisingPositionId\":(\d+)/g, '"advertisingPositionId": "$1"')
            .replace(/\"productTypeId\":(\d+)/g, '"productTypeId": "$1"')
            .replace(/\"upgradeId\":(\d+)/g, '"upgradeId": "$1"')
            .replace(/\"productId\":(\d+)/g, '"productId": "$1"')
            .replace(/\"deviceId\":(\d+)/g, '"deviceId": "$1"')
            .replace(/\"appuserId\":(\d+)/g, '"appuserId": "$1"')
            .replace(/\"categoryId\":(\d+)/g, '"categoryId": "$1"')
            .replace(/\"roleId\":(\d+)/g, '"roleId": "$1"')

        return replaceResponseText
    }
}