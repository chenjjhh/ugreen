export default class NetConstants {
    static BD_LOCATION_URL = 'http://api.map.baidu.com/location/ip'

    static isRelease = true//是否正式环境
    // static HTTP_URL = NetConstants.isRelease ? 'https://nm68rk09pi.execute-api.us-west-2.amazonaws.com/Prod' : 'https://oghafnxkic.execute-api.us-west-2.amazonaws.com/Prod'
    //正式环境  https://app.admin.gendome.net
    static HTTP_URL = NetConstants.isRelease ? 'https://test.ugreeniot.com' : 'https://test.ugreeniot.com'
    //国内测试环境 https://g9wrcx4v4l.execute-api.cn-north-1.amazonaws.com.cn/Prod
    //static HTTP_URL = 'https://g9wrcx4v4l.execute-api.cn-north-1.amazonaws.com.cn/Prod'
    static TIMEOUT = 30 * 1000//请求超时时间  30秒

    /*
    *验证码获取类型
     */
    static EMAIL_VER_CODE_REGISTER = 'REGISTER'//邮箱验证码——注册
    static EMAIL_VER_CODE_PWD_RESET = 'PWD_RESET'//邮箱验证码——忘记密码
    static EMAIL_VER_CODE_LOGIN = 'LOGIN'//邮箱验证码——登录
    static EMAIL_VER_CODE_PHONE_SET = 'PHONE_RESET'//邮箱验证码——重置手机号
    static EMAIL_VER_CODE_EMAIL_RESET = 'EMAIL_RESET'//邮箱验证码——重置邮箱
    static EMAIL_VER_CODE_BIND_EMAIL = 'BIND_EMAIL'//邮箱验证码——绑定邮箱
    static EMAIL_VER_CODE_LOGOFF = 'LOGOFF'//用户注销账号
   


    /*
    *广告获取类型
     */
    //1 运营 2 首页轮播 3 启动页面
    static BANNER_TYPE1 = '1'
    static BANNER_TYPE2 = '2'
    static BANNER_TYP3 = '3'


    /*
    *数据列表获取类型
     */
    static DATA_CENTER_TYPE_1 = '1'//语言包管理
    static DATA_CENTER_TYPE_2 = '2'//设备类帮助
    static DATA_CENTER_TYPE_3 = '3'//应用类帮助
    static DATA_CENTER_TYPE_4 = '4'//服务协议
    static DATA_CENTER_TYPE_5 = '5'//隐私协议
    static DATA_CENTER_TYPE_6 = '6'//关于我们

    /*
   *获取文件上传地址参数
   * fileName
    */
    static IMAGE_AVATAE_FILE_NAME = 'avatar.png'//头像
    static IMAGE_BG_FILE_NAME = 'mineBg.png'//背景

    static MERCHANTID = '100000000000000000'//商户ID

    static USER_LOGIN = '/app/user/login'//app用户账户密码登录接口
    static USER_LOGIN_code = '/app/user/login/code'//app用户验证码登录接口
    static DEVICE_PUSH = '/msg/device/msg/page'//消息中心/设备推送
    static DEVICE_SHARE = '/msg/device/share/push/list'//消息中心/设备分享
    static SYSTEM_PUSH = '/msg/notice/list'//消息中心/系统通知
   
    static USER_REGISTER = '/app/user/register'//注册
    static USER_INFO_UPDATE = '/user/info/update'//用户信息更新(旧)
    static USER_USER_UPDATE = '/app/user/update'//用户信息更新(新)
   
    static USER_INFO_GET = '/app/user/get'//用户信息获取
    static USER_PD_UPDATE = '/user/password/update'//用户密码更新
    static USER_PD_RESET = '/app/reset/password'//用户密码重置
    static DEVICE_LIST = '/device/user/device/list'//设备列表
    static BIND_DEVICE = '/device/user/device/add'//绑定设备
    static UNBIND_DEVICE = '/user/device/unbind'//解绑设备
    static del_DEVICE = '/device/user/device/del'//删除设备

    static DEVICE_PUSH_DETAILS = '/msg/device/msg/detail'//设备推送消息详情
    static DEVICE_RECEVE_DETAILS = '/device/user/device/receive'//设备分享消息详情
    static SYS_NOTICE_DETAILS = '/msg/notice/info'//系统公告消息详情
    static DEVICE_SHARE_MEMBER = '/device/user/device/member/query'//查询设备分享成员
    static REMOVE_DEVICE_MEMBER = '/device/user/device/member/remove'//删除设备成员
    

    static ALL_READ = '/msg/app/message/all/read'//全部已读
    static MSG_COUNT = '/msg/app/message/cnt'//获取消息数量
    
    static USER_UPDATE_DEVICE = '/device/user/device/update'//用户更新设备
    static USER_SHARE_DEVICE = '/device/user/device/share'//设备分享
    static USER_REMOTE_CONTROL_DEVICE = '/device/shadow/update'//远程控制设备
    static GET_ALL_DEVICE_SPEC = '/device/property/bath/query'//批量获取设备属性
    static GET_DEVICE_SPEC = '/device/property/query'//获取设备属性
    static SHARE_DEVICE_TO_USER = '/device/share/push/add'//分享设备
    static USER_CAN_SHARE_DEVICE = '/user/device/own/devices'//用户可分享的设备
    static DEVICE_OWN_REMOVE_SHARE = '/user/device/member/remove'//设备所有者取消分享
    static USE_HELP = '/data/center/help/list'//使用帮助
    static FEEDBACK = '/app/user/feedback/add'//反馈意见
    static DEVICE_GRADE = '/app/user/feedback/grade'//设备评分
    static EMAIL_VER_CODE = '/app/code/get'//邮箱验证码
    static BANNER_AD = '/app/advertising/info'//banner 广告接口
    static DATA_CENTER_LIST = '/data/center/list'//数据列表
    static USER_RULE = '/data/center/service/list'//用户协议
    static USER_PRIVACY = '/data/center/privacy/list'//隐私协议
    static FILE_UPLOAD_URL = '/file/info/upload'//获取文件上传地址
    static USER_CANCEL = '/user/cancel'//注销账号(旧)
    static USER_DEL = '/app/user/logoff'//用户删除账号(新)
    static SHARE_WITH_ME_DEVICE_LIST = '/user/device/share/withme'//共享给我 - 设备列表
    static ACCEPT_SHARE_DEVICE = '/user/device/accept'//接收用户分享设备
    static DEVICE_SHARE_PUSH_INFO = '/device/share/push/list'//设备分享推送消息列表
    static DEVICE_SHARE_PUSH_DETAIL = '/device/share/push/details'//设备分享详情
    static DEVICE_CONTROL = '/device/shadow/update'//设备控制
    static DEVICE_CONTROL_16 = '/device/shadow/transparentData/update'//设备控制-透传16进制
    static DEVICE_CONTROL_V3 = '/device/control/alink'//设备控制-v3
    static APPLICATIONHELP_LIST = '/app/application/list'//应用使用帮助
    static HELP_LIST = '/app/help/list'//产品使用帮助
    static FEEDBACK_ADD = '/app/feedback/add'//提交问题反馈
    static AFTER_SALES_ADD = 'app/after/sales/add'//提交问题反馈
    static RECORD_LIST = '/app/record/list'//问题反馈或申请售后列表
    static UPDAPE_FEEDBACK = '/app/feedback/update'//更新问题反馈状态
    static UPDAPE_AFTER_SALES = '/app/after/sales/update'//更新售后申请状态
    static UPLOAD_FILE = '/app/file/info/upload'//app用户上传文件

    static FIRMWARE_LIST = '/device/firmware/list'//固件升级列表
    static FIRMWARE_UPGRADE = '/device/firmware/upgrade'//固件升级
    static FIRMWARE_UPGRADE_PROGRESS = '/device/upgrade/progress'//获取升级进度
    static FIRMWARE_UPGRADE_LOG = '/device/upgrade/log'//升级历史
    static UPGRADE_RESULT_REPORT = '/device/upgrade/result/report'//APP上报升级结果
    

    static HELP_DATA_CONTENT = '/data/center/get/content'//数据详情
    static CAN_CANCEL_SHARE_DEVICE_LIST = '/user/device/share/cancel/list'//可取消设备分享的列表
    static GET_MCU_VERSION = '/user/device/mcuota/get'//获取MCU新版本
    static UPDATE_MCU_VERSION = '/user/device/mcuota/decide'//升级MCU
    static GET_DEVICE_SPEC_LOG = '/device/property/log'//获取设备属性日志
    static AUTH_FACEBOOK = '/authorize/facebook'//Facebook授权验证
    static THIRD_BIND_ACCOUNT = '/user/bind/account'//第三方授权后绑定账号
    static AUTH_GOOGLE = '/authorize/google'//Google授权验证
    static GET_THIRD_INFO = '/user/channel/get'//获取用户所有第三方授权商
    static UNBIND_THIRD = '/user/unbind/account'//解除第三方授权绑定
    static AUTH_APPLE = '/authorize/apple'//Apple授权验证
    static PRODUCT_CLASS_LIST = '/device/product/tree'//产品分类列表
    static PRODUCT_LIST = '/device/product/list'//产品列表
    static ACCOUNT_BIND_THIRD = '/authorize/bind/oauth/user'//已有账号绑定第三方账号
    static GET_MCU_UPGRADE_STATUS = '/user/device/mcuota/query'//获取MCU升级状态
    static USER_SET_PD = '/user/password/set'//首次绑定设置用户密码接口
    static PRODUCT_CONNECT_NET_TIPS = '/app/network/get'//产品配网说明
    static USER_UP = '/user/mobile/set'//设置用户终端上报
    static USER_SET_PARAM='/user/set/param'//设置附加参数
    static REFRESH_TOKEN='/app/refresh/token'//刷新token
    static GET_LINE_DATA='/device/get/polyline'//获取折线图

    static  FACEBOOK_APPID = '543105234152560'
    static GOOGLE_ANDROID_CLIENTID = '933766445343-asjoj4q1m09kggr2k5uk4evi6kvp0frd.apps.googleusercontent.com'
    static GOOGLE_IOS_CLIENTID = '933766445343-o8mqbaub4cj7unhm6pikn2r30e6bbngv.apps.googleusercontent.com'
    static APPLE_CLIENTID = 'com.gotion.gendome'
}