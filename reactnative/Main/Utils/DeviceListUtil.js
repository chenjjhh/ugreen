/*
*获取设备列表（处理设备列表缓存数据和接口数据）
 */
import NetUtil from "../Net/NetUtil";
import StorageHelper from "./StorageHelper";

export default class DeviceListUtil {
    //接口获取设备列表 
    // flag: 1-查询自己的设备 2-查询接受的设备 不传-查所有设备 count: 不传-不统计设备分享数量 1-统计设备分享数量
    static _getDeviceList(showLoading, token, hideErrorToast, flag, count) {
        var p = new Promise(function (resolve, reject) {
            if (token) {
                NetUtil.getDeviceList(token, showLoading, hideErrorToast, flag, count)
                    .then((res) => {
                        resolve(res)
                        var deviceInfo = res.info
                        if (deviceInfo) {
                            !flag && DeviceListUtil.saveDeviceListToLocal(deviceInfo) 
                        }
                    })
                    .catch((error) => {
                        reject(error)
                    })
            } else {
                reject('token is null')
            }
        })

        return p
    }

    static saveDeviceListToLocal(deviceList) {
        StorageHelper.saveDeviceList(deviceList)
    }

    //获取本地缓存的设备列表
    static _getLocalDeviceList() {
        var p = new Promise(function (resolve, reject) {
            StorageHelper.getDeviceList()
                .then((res) => {
                    resolve(res)
                })
                .catch((error) => {
                    reject(error)
                })
        })

        return p
    }
}