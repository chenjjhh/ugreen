//https://www.showdoc.com.cn/1822902899875419/89631528899595254 访问密码 PFr3YQaH

import {strings} from "../Language/I18n";

const ERROR_FAIL = 1
const ERROR_TIMEOUT = 2
const ERROR_ERROR = 3
const ERROR_TOKEN_NOT_EMPTY = 4
const ERROR_PARAM_ERROR = 1001
const ERROR_SUBSYSTEM_FAIL = 1002
const ERROR_METHOD_ERROR = 1003
const ERROR_USER_NOT_LOGIN = 1010
const ERROR_REQUEST_SIGN_ERROR = 1011
const ERROR_PARAM_FORMAT_ERROR = 1013
const ERROR_REQUEST_DATA_DECODER_ERROR = 1014
const ERROR_REQUEST_VERSION_ERROR = 1015
const ERROR_SYS_ERROR = 1016
const ERROR_SIGN_EMPTY = 1019
const ERROR_SIGN_CHECK_FALSE = 1020
const ERROR_SYS_INDEX_ACCOUNT_OR_PASSWORD_ERROR = 1034
const ERROR_PASSWORD_ERROR = 40016
const ERROR_SYS_INDEX_PASSWORD_ERROR = 1035
const ERROR_SYS_INDEX_OLD_PASSWORD_ERROR = 1036
const ERROR_SYS_INDEX_ACCOUNT_DISABLE = 1037
const ERROR_SYS_INDEX_ACCOUNT_NOT_EXIST = 1038
const ERROR_SYS_INDEX_PASSWORD_CANNOT_EQUALS_ACCOUNT = 1039
const ERROR_SYS_INDEX_CODE_WRONG = 40024
const ERROR_PHONE_CODE_WRONG = 40021
const ERROR_SYS_INDEX_ACCOUNT_DOES_NOT_MATCH_EMAIL = 1041
const ERROR_USER_NOT_EXIST = 1023
const ERROR_USER_LOGIN_TOMORROW = 1024
const ERROR_USER_PASSWORD_ERROR = 1025
const ERROR_USER_DELETE = 1026
const ERROR_USER_NOT_ACTIVE = 1027
const ERROR_ROLE_NOT_EXIST = 1028
const ERROR_ROLE_ALERADY_DELETE = 1029
const ERROR_ACCOUNT_ALREADY_EXISTED = 1030
const ERROR_PHONE_ALREADY_BIND = 1031
const ERROR_NO_OPERATION_ALLOWED = 1033
const ERROR_ACCOUNT_OR_PASSWORD_ERROR = 1034
const ERROR_MERCHANT_APIKEY_REDIS_LOCK = 2001
const ERROR_MERCHANT_APIKEY_USER_NOT_PERMISSION = 2002
const ERROR_MERCHANT_APIKEY_USER_NOT_EXIST = 2009
const ERROR_MERCHANT_APIKEY_USER_TYPE_ERROR = 2010
const ERROR_MERCHANT_APIKEY_USER_STATUS_ERROR = 2011
const ERROR_MERCHANT_APIKEY_DATA_ERROR = 2012
const ERROR_SYS_ROLE_EXIST = 2000
const ERROR_SYS_ROLE_NOT_EXIST = 2100
const ERROR_SYS_USER_ACCOUNT_EXIST = 2306
const ERROR_SYS_USER_PHONE_EXIST = 2307
const ERROR_SYS_USER_EMAIL_EXIST = 2308
const ERROR_EMAIL_CONFIG_NOT_EXIST = 2309
const ERROR_COMMON_DATABASE_NOT_EXIST = 2326
const ERROR_COMMON_DATABASE_ERROR = 2327
const ERROR_COMMON_USER_NOT_PERMISSION = 2328
const ERROR_COMMON_REDIS_LOCK = 2329
const ERROR_MERCHANT_INFO_NO_OPERATION_ALLOWED = 3002
const ERROR_MERCHANT_INFO_PHONE_ALREADY_EXISTED = 3003
const ERROR_MERCHANT_INFO_ACCOUNT_ALREADY_EXISTED = 3004
const ERROR_MERCHANT_INFO_SYS_USER_NOT_MERCHANT = 3005
const ERROR_APP_USER_SERVICE_ACCOUNT_ALERADY_EXISTED = 3170
const ERROR_APP_USER_SERVICE_PHONE_ALERADY_BIND = 3171
const ERROR_APP_USER_SERVICE_VERCODE_NUM_NIMIETY = 3172
const ERROR_APP_USER_SERVICE_VERCODE_WRONG = 3173
const ERROR_APP_USER_SERVICE_PHONE_NO_EXISTED = 3174
const ERROR_APP_USER_SERVICE_PHONE_FORMAT = 3175
const ERROR_APP_USER_SERVICE_EMAIL_FORMAT = 3180
const ERROR_APP_USER_SERVICE_ACCOUNT_NO_EXISTED = 3176
const ERROR_APP_USER_SERVICE_PASSWORD_WRONG = 3177
const ERROR_APP_USER_SERVICE_ACCOUNT_FORBIDDEN = 3178
const ERROR_APP_USER_PHONE_NOT_BIND_ACCOUNT = 3179
const ERROR_APP_USER_SERVICE_ACCOUNT_UNDER_VERIFIED = 3182
const ERROR_APP_USER_AUTH_CODE_FAIL = 3188
const ERROR_APP_USER_AUTH_CODE_ERROR = 3183
const ERROR_APP_USER_AUTH_CODE_NOT_EXIST = 3181
const ERROR_APP_USER_AUTH_CODE_AUTHORIZED = 3184
const ERROR_APP_USER_AUTH_CODE_AUTHORIZING = 3199
const ERROR_APP_USER_AUTH_CODE_FAILED = 3200
const ERROR_APP_USER_EXIST = 10001
const ERROR_APP_USER_NOT_EXIST = 10002
const ERROR_APP_USER_PASSWORD_WRONG = 10003
const ERROR_APP_CODE_WRONG = 10004
const ERROR_UNVERIFIED_USER_REQUEST = 10005
const ERROR_APP_USER_NOT_TOKEN = 10007
const ERROR_APP_USER_FAILED_TO_CREATE_CREDENTIAL = 10008
const ERROR_APP_USEREMAIL_NOT_BIND = 10009
const ERROR_APP_USERPHONE_NOT_BIND = 10011
const ERROR_APP_USERE_NOT_BIND = 10012
const ERROR_APP_USEREMAIL_ALREADY_BIND = 10013
const ERROR_APP_USERPHONE_ALREADY_BIND = 10014
const TARGET_HAVE_BOUND_THE_DEVICE = 12013
const ERROR_SYS_INDEX_EMAIL_USER_NOT_EXIST = 1042
const ERROR_APP_USER_SHARE_ALREADY_EXISTS = 7101
const ERROR_DO_NOT_SHARE_WITH_YOURSELF = 7102
const ERROR_UPGRADE_INFO_NOT_EXIT = 2324
const ERROR_ACCOUNT_HAD_BIND = 10016
const ERROR_DEVICE_OFFLINE = 80002
const ERROR_PROTOCOL_NOT_MATCH = 80027
const ERROR_WAITING_RECEPTION = 90019
const ERROR_CANT_SHARE_WITH_YOUR_SELF = 80017
const ERROR_SHARE_USER_NOT_EXIT = 80015
const ERROR_SHARE_MSG_EXPIRED = 80020
const ERROR_CODE_ERROR = 20030



export default class ErrorCodeUtil {
    static getErrorCodeText(errorCode, errorText) {
        switch (parseInt(errorCode)) {
            case ERROR_SHARE_MSG_EXPIRED:
                return strings('分享消息已过期')
                break
            case ERROR_SHARE_USER_NOT_EXIT:
                return strings('用户不存在')
                break
            case ERROR_PASSWORD_ERROR:
                return strings('密码错误')
                break
            case ERROR_CANT_SHARE_WITH_YOUR_SELF:
                return strings('无法分享给自己')
                break
            case ERROR_FAIL:
                return strings('响应失败')
                break
            case ERROR_TIMEOUT:
                return strings('超时')
                break
            case ERROR_ERROR:
                return strings('请求错误')
                break
            case ERROR_TOKEN_NOT_EMPTY:
                return strings('token_不能为空')
                break
            case ERROR_PARAM_ERROR:
                return strings('请求参数错误')
                break
            case ERROR_SUBSYSTEM_FAIL:
                return strings('调用子系统失败')
                break
            case ERROR_METHOD_ERROR:
                return strings('方法不存在')
                break
            case ERROR_USER_NOT_LOGIN:
                return strings('用户未登录或登录失效')
                break
            case ERROR_REQUEST_SIGN_ERROR:
                return strings('签名验证失败')
                break
            case ERROR_PARAM_FORMAT_ERROR:
                return strings('请求数据格式有误')
                break
            case ERROR_REQUEST_DATA_DECODER_ERROR:
                return strings('请求数据解密失败1014')
                break
            case ERROR_REQUEST_VERSION_ERROR:
                return strings('请求数据解密失败1015')
                break
            case ERROR_SYS_ERROR:
                return strings('系统错误')
                break
            case ERROR_SIGN_EMPTY:
                return strings('签名为空')
                break
            case ERROR_SIGN_CHECK_FALSE:
                return strings('签名验证失败1020')
                break
            case ERROR_SYS_INDEX_ACCOUNT_OR_PASSWORD_ERROR:
                return strings('账号或密码错误')
                break
            case ERROR_SYS_INDEX_PASSWORD_ERROR:
                return strings('密码错误')
                break
            case ERROR_SYS_INDEX_OLD_PASSWORD_ERROR:
                return strings('原始密码错误')
                break
            case ERROR_SYS_INDEX_ACCOUNT_DISABLE:
                return strings('账号已被停用')
                break
            case ERROR_SYS_INDEX_ACCOUNT_NOT_EXIST:
                return strings('账号不存在')
                break
            case ERROR_SYS_INDEX_PASSWORD_CANNOT_EQUALS_ACCOUNT:
                return strings('密码不能与账号信息一致')
                break
            case ERROR_SYS_INDEX_CODE_WRONG:
            case ERROR_PHONE_CODE_WRONG:
            case ERROR_CODE_ERROR:
                return strings('验证码错误')
                break
            case ERROR_SYS_INDEX_ACCOUNT_DOES_NOT_MATCH_EMAIL:
                return strings('账号与邮箱不匹配')
                break
            case ERROR_USER_NOT_EXIST:
                return strings('用户不存在')
                break
            case ERROR_USER_LOGIN_TOMORROW:
                return strings('今天已不可登录_请明天再试')
                break
            case ERROR_USER_PASSWORD_ERROR:
                return strings('登录密码错误')
                break
            case ERROR_USER_DELETE:
                return strings('该账户被删除')
                break
            case ERROR_USER_NOT_ACTIVE:
                return strings('该账户未激活')
                break
            case ERROR_ROLE_NOT_EXIST:
                return strings('角色不存在')
                break
            case ERROR_ROLE_ALERADY_DELETE:
                return strings('角色已经删除')
                break
            case ERROR_ACCOUNT_ALREADY_EXISTED:
                return strings('该账户已经存在')
                break
            case ERROR_PHONE_ALREADY_BIND:
                return strings('手机号已经被绑定')
                break
            case ERROR_NO_OPERATION_ALLOWED:
                return strings('不允许操作')
                break
            case ERROR_ACCOUNT_OR_PASSWORD_ERROR:
                return strings('用户名或者密码错误')
                break
            case ERROR_MERCHANT_APIKEY_REDIS_LOCK:
                return strings('正在处理')
                break
            case ERROR_MERCHANT_APIKEY_USER_NOT_PERMISSION:
                return strings('当前用户无权限')
                break
            case ERROR_MERCHANT_APIKEY_USER_NOT_EXIST:
                return strings('用户不存在2009')
                break
            case ERROR_MERCHANT_APIKEY_USER_TYPE_ERROR:
                return strings('用户类型不正确')
                break
            case ERROR_MERCHANT_APIKEY_USER_STATUS_ERROR:
                return strings('用户状态不正确')
                break
            case ERROR_MERCHANT_APIKEY_DATA_ERROR:
                return strings('数据错误')
                break
            case ERROR_SYS_ROLE_EXIST:
                return strings('角色已存在')
                break
            case ERROR_SYS_ROLE_NOT_EXIST:
                return strings('角色不存在2100')
                break
            case ERROR_SYS_USER_ACCOUNT_EXIST:
                return strings('登录账号已存在')
                break
            case ERROR_SYS_USER_PHONE_EXIST:
                return strings('手机号码已存在')
                break
            case ERROR_SYS_USER_EMAIL_EXIST:
                return strings('邮箱地址已存在')
                break
            case ERROR_EMAIL_CONFIG_NOT_EXIST:
                return strings('邮箱服务不存在')
                break
            case ERROR_COMMON_DATABASE_NOT_EXIST:
                return strings('数据不存在')
                break
            case ERROR_COMMON_DATABASE_ERROR:
                return strings('数据错误2327')
                break
            case ERROR_COMMON_USER_NOT_PERMISSION:
                return strings('当前用户无权限2328')
                break
            case ERROR_COMMON_REDIS_LOCK:
                return strings('正在处理2329')
                break
            case ERROR_MERCHANT_INFO_NO_OPERATION_ALLOWED:
                return strings('不允许操作3002')
                break
            case ERROR_MERCHANT_INFO_PHONE_ALREADY_EXISTED:
                return strings('手机号已被绑定')
                break
            case ERROR_MERCHANT_INFO_ACCOUNT_ALREADY_EXISTED:
                return strings('该账号已存在')
                break
            case ERROR_MERCHANT_INFO_SYS_USER_NOT_MERCHANT:
                return strings('操作用户没有所属商户')
                break
            case ERROR_APP_USER_SERVICE_ACCOUNT_ALERADY_EXISTED:
                return strings('账号已存在')
                break
            case ERROR_APP_USER_SERVICE_PHONE_ALERADY_BIND:
                return strings('手机号码已经被绑定')
                break
            case ERROR_APP_USER_SERVICE_VERCODE_NUM_NIMIETY:
                return strings('今天次数已经超限_明天再来')
                break
            case ERROR_APP_USER_SERVICE_VERCODE_WRONG:
                return strings('验证码错误3173')
                break
            case ERROR_APP_USER_SERVICE_PHONE_NO_EXISTED:
                return strings('手机号码不存在')
                break
            case ERROR_APP_USER_SERVICE_PHONE_FORMAT:
                return strings('手机号码格式不正确')
                break
            case ERROR_APP_USER_SERVICE_EMAIL_FORMAT:
                return strings('邮箱格式不正确')
                break
            case ERROR_APP_USER_SERVICE_ACCOUNT_NO_EXISTED:
                return strings('账号不存在3176')
                break
            case ERROR_APP_USER_SERVICE_PASSWORD_WRONG:
                return strings('密码错误3177')
                break
            case ERROR_APP_USER_SERVICE_ACCOUNT_FORBIDDEN:
                return strings('该账号禁用')
                break
            case ERROR_APP_USER_PHONE_NOT_BIND_ACCOUNT:
                return strings('手机并未有绑定账号')
                break
            case ERROR_APP_USER_SERVICE_ACCOUNT_UNDER_VERIFIED:
                return strings('该账号未验证邮箱和手机号')
                break
            case ERROR_APP_USER_AUTH_CODE_FAIL:
                return strings('授权失败')
                break
            case ERROR_APP_USER_AUTH_CODE_ERROR:
                return strings('授权码错误')
                break
            case ERROR_APP_USER_AUTH_CODE_NOT_EXIST:
                return strings('授权码不存在')
                break
            case ERROR_APP_USER_AUTH_CODE_AUTHORIZED:
                return strings('授权码已授权')
                break
            case ERROR_APP_USER_AUTH_CODE_AUTHORIZING:
                return strings('授权中')
                break
            case ERROR_APP_USER_AUTH_CODE_FAILED:
                return strings('授权失败3200')
                break
            case ERROR_APP_USER_EXIST:
                return strings('用户已存在')
                break
            case ERROR_APP_USER_NOT_EXIST:
                return strings('用户不存在10002')
                break
            case ERROR_APP_USER_PASSWORD_WRONG:
                return strings('密码错误10003')
                break
            case ERROR_APP_CODE_WRONG:
                return strings('验证码错误10004')
                break
            case ERROR_UNVERIFIED_USER_REQUEST:
                return strings('未受验证的用户请求')
                break
            case ERROR_APP_USER_NOT_TOKEN:
                return strings('token不存在')
                break
            case ERROR_APP_USER_FAILED_TO_CREATE_CREDENTIAL:
                return strings('创建用户凭证失败')
                break
            case ERROR_APP_USEREMAIL_NOT_BIND:
                return strings('未绑定邮箱')
                break
            case ERROR_APP_USERPHONE_NOT_BIND:
                return strings('未绑定手机')
                break
            case ERROR_APP_USERE_NOT_BIND:
                return strings('请绑定手机号或邮箱')
                break
            case ERROR_APP_USEREMAIL_ALREADY_BIND:
                return strings('邮箱已被绑定')
                break
            case ERROR_APP_USERPHONE_ALREADY_BIND:
                return strings('手机号已被绑定10014')
                break
            case TARGET_HAVE_BOUND_THE_DEVICE:
                return strings('目标用户已经绑定该设备')
                break
            case ERROR_SYS_INDEX_EMAIL_USER_NOT_EXIST:
                return strings('邮箱绑定用户不存在')
                break
            case ERROR_APP_USER_SHARE_ALREADY_EXISTS:
                return strings('已发送过邀请_请等待对方处理')
                break
            case ERROR_DO_NOT_SHARE_WITH_YOURSELF:
                return strings('参数错误_不可分享设备给自己')
                break
            case ERROR_UPGRADE_INFO_NOT_EXIT:
                return strings('设备升级信息不存在或无效')
                break
            case ERROR_ACCOUNT_HAD_BIND:
                return strings('账号已被绑定')
                break
            case ERROR_DEVICE_OFFLINE:
                return strings('device_off_line')
                break
            case ERROR_PROTOCOL_NOT_MATCH:
                return strings('协议不匹配')
                break
            case ERROR_WAITING_RECEPTION:
                return strings('正在等待设备接收')
                break
            default:
                return errorText
        }
    }
}