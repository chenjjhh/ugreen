//关于设备数据：
let AboutDeviceData = {
  deviceName: "绿联星辰GS600",//设备标题
  productName: "星辰系列",//产品名称
  productType: "GS600",//产品型号
  PN: "12345",//P/N
  BatteryType: "磷酸铁锂电池",//电池类型
  productSize: "1x19x24（cm）",//产品尺寸
  productWeight: "9.0 Kg",//产品净重
  chargingTemperature: "0-40 ℃",//充电温度
  dischargeTemperature: "-10-40 ℃",//放电温度

  acInput: "90-110V~15AMax50Hz/60Hz",//交流输入
  solarChargingInput: "DC12-28V12A，200W Max",//太阳能充电输入
  carChargingInput: "DC12V/24V 8AMax",//车充输入

  acOutput: "100V~50Hz/60Hz 6A 600W Max",//交流输出
  usbAInput: "单口5V/3A.9V/2A，12V/1.5A，5V/4.5A，22.5W Max（双口45W Max）",//USB-A输出
  usbCInput:
    "单口5V/3A，9V/3A，12V/3A，15V/3A，20V/5A，100WMax（双口200W Max）",//USB-C输出
  vehicleOutput: "DC12V/10A，120W Max",//车充口输出
  dc5521Output: `（Car Charger + DC5521 x 2=120W）\n DC 12V/5A，60W Max`,//DC5521输出
  totalOutput: "965WMax",//总输出
};

export { AboutDeviceData };
