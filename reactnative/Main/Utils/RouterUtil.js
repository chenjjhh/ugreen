import {StackActions, NavigationActions} from 'react-navigation'
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "./StorageHelper";

var props
export default class RouterUtil {

    static setProps(mainProps) {
        props = mainProps
    }

    //路由重置  跳转到登录页面
    static toLogin() {
        StorageHelper.setTempToken('')
        EventUtil.sendEventWithoutValue(EventUtil.TO_LOGIN)
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({routeName: 'Login'}),  //Login 要跳转的路由
            ]
        })
        props.navigation.dispatch(resetAction);
    }

    //跳转到首页，清空栈顶(新版因为MainDeviceList被嵌套标签路由中，所以无法传参)
    static toMainDeviceList(token, getUserFlag) {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Home',
                    // params: {
                    //     token: token,
                    //     getUserFlag: getUserFlag
                    // }
                }),  //Login 要跳转的路由
            ]
        })
        props.navigation.dispatch(resetAction);
    }
}