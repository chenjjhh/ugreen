import {XHttp, XStorage} from 'react-native-easy-app';
import {AsyncStorage} from 'react-native';
import LogUtil from "./LogUtil";


var tempThirdAvatar = ''
let RNStorage = { // 自定义对象
    token: undefined,//接口会话token
    userInfo: undefined,//用户信息
    language: 'en',
    deviceList: undefined,//设备列表
    loginInfo: undefined,//登录信息
    tempUnit: undefined,//温度单位 0摄氏度 1华氏度
    loginAccount: undefined,
    shareIds: undefined,//分享id集合
    wifiInfo: undefined,//已连接的WiFi密码信息
    thirdAvatar: undefined,//第三方登录头像,

    isFirstEnter: undefined,//首次进入app

    isFirstUseUTurbo: true,
    isFirstUseWorkMode: true,
    isFirstUseDC: true,
    isFirstUseAC: true,
    isFirstUseQuiet: true,
    isFirstUseTimerOff: true,
    isFirstUseChildLock: true,
};

var tempToken = ''
export default class StorageHelper {

    static getFirstEnter() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.isFirstEnter)
            });
        })

        return p
    }

    static saveFirstEnter(first) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstEnter = first;
        });
    }

    static setTempToken(token) {
        tempToken = token
    }

    static getTempToken() {
        return tempToken
    }

    static setTempThirdAvatar(thirdAvatar) {
        tempThirdAvatar = thirdAvatar
    }

    static getTempThirdAvatar() {
        return tempThirdAvatar
    }

    static saveThirdAvatar(thirdAvatar) {
        if (thirdAvatar)
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                RNStorage.thirdAvatar = thirdAvatar;
            });
    }

    static getThirdAvatar() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.thirdAvatar)
            });
        })

        return p
    }

    static saveWifiInfo(data) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.wifiInfo = data;
        });
    }

    static getWifiInfo() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.wifiInfo)
            });
        })

        return p
    }

    static saveShareIds(data) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.shareIds = data;
        });
    }

    static getShareIds() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.shareIds)
            });
        })

        return p
    }

    static saveLoginAccount(account) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.loginAccount = account;
        });
    }

    static getLoginAccount() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.loginAccount)
            });
        })

        return p
    }

    static saveLoginPassword(password) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 密码
            RNStorage.loginPassword = password;
        });
    }

    static getLoginPassword() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => {
                resolve(RNStorage.loginPassword)
            });
        })

        return p
    }

    static saveToken(token) {
        tempToken = token
        LogUtil.debugLog('保存token：' + token)
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.token = token;
        });
    }

    //保存登录方式 
    static saveLoginType(LoginType) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.LoginType = LoginType; //1:账号密码登录 2:验证码登录  3:访客登录 4:一键登录 5：第三方登录
        });
    }

    //获取登录方式 
    static getLoginType() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.LoginType)
            });
        })
        return p
    }

    static getToken() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.token)
            });
        })

        return p
    }

    static saveLoginInfo(loginInfo) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.loginInfo = loginInfo;
        });
    }

    static getLoginInfo() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.loginInfo)
            });
        })

        return p
    }

    //保存用户信息
    static saveUserInfo(userInfo) {
        LogUtil.debugLog('保存用户信息：' + JSON.stringify(userInfo))
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.userInfo = userInfo;
        });
    }

    static getUserInfo() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.userInfo)
            });
        })

        return p
    }

    //保存当前设置的语言
    static saveLanguage(language) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.language = language;
        });
    }

    static getLanguage() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.language)
            });
        })

        return p
    }

    //保存当前设置的温度单位
    static saveTempUnit(unit) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.tempUnit = unit;
        });
    }

    static getTempUnit() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.tempUnit)
            });
        })

        return p
    }

    //保存设备列表
    static saveDeviceList(deviceList) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.deviceList = deviceList;
        });
    }

    static getDeviceList() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                resolve(RNStorage.deviceList)
            });
        })

        return p
    }

    //清空数据
    static clearAllData() {
        tempToken = ''

        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                RNStorage.token = undefined;
                RNStorage.userInfo = undefined;
                RNStorage.deviceList = undefined;
                //RNStorage.tempUnit = undefined;
                RNStorage.shareIds = undefined;
                RNStorage.thirdAvatar = undefined;
                RNStorage.loginAccount = undefined;
                resolve()
            });
        })
        return p
    }

    static getIsFirstUseUTurbo() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseUTurbo == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseUTurbo)
            });
        })

        return p
    }

    static saveIsFirstUseUTurbo(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseUTurbo = falg;
        });
    }

    static getIsFirstUseWorkMode() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseWorkMode == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseWorkMode)
            });
        })

        return p
    }

    static saveIsFirstUseWorkMode(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseWorkMode = falg;
        });
    }

    static getIsFirstUseDC() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseDC == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseDC)
            });
        })

        return p
    }

    static saveIsFirstUseDC(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseDC = falg;
        });
    }

    static getIsFirstUseAC() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseAC == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseAC)
            });
        })

        return p
    }

    static saveIsFirstUseAC(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseAC = falg;
        });
    }

    static getIsFirstUseQuiet() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseQuiet == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseQuiet)
            });
        })

        return p
    }

    static saveIsFirstUseQuiet(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseQuiet = falg;
        });
    }

    static getIsFirstUseTimerOff() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseTimerOff == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseTimerOff)
            });
        })

        return p
    }

    static saveIsFirstUseTimerOff(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseTimerOff = falg;
        });
    }

    static getIsFirstUseChildLock() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                if (RNStorage.isFirstUseChildLock == undefined) {
                    resolve(true)
                }
                resolve(RNStorage.isFirstUseChildLock)
            });
        })

        return p
    }

    static saveIsFirstUseChildLock(falg) {
        XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
            RNStorage.isFirstUseChildLock = falg;
        });
    }

    //清空数据
    static clearAllFirstUse() {
        var p = new Promise(function (resolve, reject) {
            XStorage.initStorage(RNStorage, AsyncStorage, () => { // 初始化自定义数据管理器
                RNStorage.isFirstUseUTurbo = true;
                RNStorage.isFirstUseWorkMode = true;
                RNStorage.isFirstUseDC = true;
                RNStorage.isFirstUseAC = true;
                RNStorage.isFirstUseQuiet = true;
                RNStorage.isFirstUseTimerOff = true;
                RNStorage.isFirstUseChildLock = true;
                resolve()
            });
        })
        return p
    }
}
