export default class TimerUtil {
    /**  定时器 */
    interval = null;

    /*
    *time  秒数
    */
    static setTimer(time, callbak) {
        this.startTimer(time, callbak)
    }

    static startTimer(time, callbak) {
        var endTime = new Date().getTime() + time * 1000
        this.stopTimer()
        this.interval = setInterval(() => {
            var resultTime = this._getTime(endTime)
            callbak && callbak(resultTime)
            if (resultTime <= 0) {
                this.stopTimer()
            }
        }, 1000)
    }

    static _getTime(endTime) {
        return Math.ceil((endTime - new Date().getTime()) / 1000)
    }

    static stopTimer() {
        if (this.interval)
            clearInterval(this.interval);
    }
}