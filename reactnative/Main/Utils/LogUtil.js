import {NativeModules, Platform} from "react-native";

export default class LogUtil {
    //上线时改为关闭状态
    static writeLogEnable = true
    static debugLogEnable = true
    static globalDebugLogEnable = true

    static AndroidLogWrite(log) {
        if (Platform.OS === 'android' && this.writeLogEnable) {
            var logValue = new Date().toString() + ':  rn======>' + log
            console.log(logValue)
            NativeModules.RNModule.log(logValue);
        }
    }

    static debugLog(logName, logText) {
        try {
            if (this.debugLogEnable) {
                if (logName && logText) {
                    console.log(logName + ':' + logText)
                } else {
                    console.log(logName)
                }
            }
        } catch (e) {
            if (this.debugLogEnable) {
                console.log('logUtil===error===>' + e.toString())
            }
        }
    }
}