import {NativeModules, Platform} from "react-native";
import NetUtil from "../Net/NetUtil";
import Bluetooth from "../Bluetooth/Bluetooth";
import Helper from "./Helper";
import LogUtil from "./LogUtil";

var token = ''
var deviceId = ''
var isWifiConnect = true

var GXRNManager = NativeModules.GXRNManager

export default class SpecUtil {
    static initBleProps(isWifiConnectType) {
        isWifiConnect = isWifiConnectType
    }

    static initProps(tokenValue, deviceIdValue, isWifiConnectType) {
        token = tokenValue
        deviceId = deviceIdValue
        isWifiConnect = isWifiConnectType
    }

    static key_inverter_temp1 = 'inverter_temp1'//逆变散热器1
    static key_inverter_temp2 = 'inverter_temp2'//逆变散热器2
    static key_timeoff_zoom = 'timeoff_zoom'//选择区域

    static key_type_c1_vol = 'type_c1_vol'//type-c1输出功率
    static key_type_c2_vol = 'type_c2_vol'//type-c2输出功率
    static key_type_c3_vol = 'type_c3_vol'//type-c3输出功率
    static key_type_c4_vol = 'type_c4_vol'//type-c4输出功率
    static key_discharge_remain_time = 'discharge_remain_time'//放电剩余时间
    static key_switch_dc = 'switch_dc'//DC12V控制开关
    static key_discharge_pow = 'discharge_pow'//放电总功率
    static key_ac_vol = 'ac_vol'//AC输出电压
    static key_cell1_vol = 'cell1_vol'//电池1电压
    static key_cell2_vol = 'cell2_vol'//电池2电压
    static key_cell3_vol = 'cell3_vol'//电池3电压
    static key_cell4_vol = 'cell4_vol'//电池4电压
    static key_cell5_vol = 'cell5_vol'//电池5电压
    static key_cell6_vol = 'cell6_vol'//电池6电压
    static key_cell7_vol = 'cell7_vol'//电池7电压


    static key_low_power_al_set = 'low_power_al_set'//低电量警告阀值
    static key_car_charge_i_set = 'car_charge_i_set'//车载充电电流
    static key_bat_cap_remain = 'bat_cap_remain'//电池剩余容量
    static key_usb_discharge_pow = 'usb_discharge_pow'//usb放电功率
    static key_bluetooth_id = 'bluetooth_id'//蓝牙mac
    static key_ac_in_vol = 'ac_in_vol'//AC输入电压
    static key_self_check = 'self_check'//设备自检开关
    static key_charge_remain_time = 'charge_remain_time'//充电剩余时间
    static key_bat_temp1 = 'bat_temp1'//电芯温度1
    static key_bat_temp2 = 'bat_temp2'//电池温度2
    static key_usb1_vol = 'usb1_vol'//usb1输出功率
    static key_usb2_vol = 'usb2_vol'//usb2输出功率
    static key_usb3_vol = 'usb3_vol'//usb3输出功率
    static key_usb4_vol = 'usb4_vol'//usb4输出功率

    static key_bms_solfware_version = 'bms_solfware_version'//bms软件版本
    static key_inv_solfware_version = 'inv_solfware_version'//逆变器软件版本
    static key_ac_discharge_pow = 'ac_discharge_pow'//AC放电功率
    static key_switch_lock = 'switch_lock'//一键锁定开关
    static key_timeoff_set = 'timeoff_set'//定时开关
    static key_switch_all = 'switch_all'//一键待机开关
    static key_time_dis_shutdown = 'time_dis_shutdown'//熄屏时间
    static key_bat_cycle_num = 'bat_cycle_num'//电池循环次数
    static key_charge_power_all = 'charge_power_all'//充电总功率
    static key_usb_sw = 'usb_sw'//usb开关
    static key_low_battery = 'low_battery'//低电量告警
    static key_dc_vol = 'dc_vol'//dc口输出功率
    static key_sn_num = 'sn_num'//sn
    static key_bee_sound_set_key = 'bee_sound_set_key'//蜂鸣器设置-按键音
    static key_low_sound_set = 'low_sound_set'//静享模式
    static key_bee_sound_set_warning = 'bee_sound_set_warning'//蜂鸣器设置-警告声
    static key_switch_ac = 'switch_ac'//ac输出开关
    static key_bat_health_set = 'bat_health_set'//电池健康开关
    static key_switch_conpower = 'switch_conpower'//u-turbo开关
    static key_ac_freq_set = 'ac_freq_set'//ac频率切换
    static key_time_shutdown = 'time_shutdown'//定时关机时间
    static key_solar_energy_vol = 'solar_energy_vol'//太阳能输入电压
    static key_work_mode = 'work_mode'//工作模式
    static key_display_bright_set = 'display_bright_set'//屏幕亮度调节
    static key_lamp_sw = 'lamp_sw'//照明灯
    static key_device_fault2 = 'device_fault2'//设备故障2
    static key_timeoff_cap = 'timeoff_cap'//定时关机容量
    static key_mainboard_solfware_version = 'mainboard_solfware_version'//主板软件版本
    static key_cell_total_vol = 'cell_total_vol'//电池总电压
    static key_battery_percentage = 'battery_percentage'//电池电量百分比
    static key_car_discharge_pow = 'car_discharge_pow'//车充口放电总功率
    static key_bat_health = 'bat_health'//电池健康度

    //解析设备属性
    static resolveSpecs(value, showLog) {
        var inverter_temp1 = 0//逆变散热器1
        var inverter_temp2 = 0//逆变散热器2
        var timeoff_zoom = 0//选择区域

        var type_c1_vol = '0'//type-c1输出功率
        var type_c2_vol = '0'//type-c2输出功率
        var type_c3_vol = '0'//type-c3输出功率
        var type_c4_vol = '0'//type-c4输出功率
        var discharge_remain_time = 0//放电剩余时间
        var switch_dc = '0'//DC12V控制开关
        var discharge_pow = 0//放电总功率
        var ac_vol = 0//AC输出电压
        var cell1_vol = 0//电池1电压
        var cell2_vol = 0//电池2电压
        var cell3_vol = 0//电池3电压
        var cell4_vol = 0//电池4电压
        var cell5_vol = 0//电池5电压
        var cell6_vol = 0//电池6电压
        var cell7_vol = 0//电池7电压


        var low_power_al_set = 0//低电量警告阀值
        var car_charge_i_set = 0//车载充电电流
        var bat_cap_remain = 0//电池剩余容量
        var usb_discharge_pow = 0//usb放电功率
        var bluetooth_id = ''//蓝牙mac
        var ac_in_vol = 0//AC输入电压
        var self_check = false//设备自检开关
        var charge_remain_time = 0//充电剩余时间
        var bat_temp1 = 0//电芯温度1
        var bat_temp2 = 0//电芯温度2
        var usb1_vol = '0'//usb1输出功率
        var usb2_vol = '0'//usb2输出功率
        var usb3_vol = '0'//usb3输出功率
        var usb4_vol = '0'//usb4输出功率
        var bms_solfware_version = ''//bms软件版本
        var inv_solfware_version = ''//逆变器软件版本
        var ac_discharge_pow = 0//AC放电功率
        var switch_lock = false//一键锁定开关
        var timeoff_set = false//定时开关
        var switch_all = false//一键待机开关
        var time_dis_shutdown = 0//熄屏时间
        var bat_cycle_num = 0//电池循环次数
        var charge_power_all = 0//充电总功率
        var usb_sw = false//usb开关
        var low_battery = false//低电量告警
        var dc_vol = 0//dc口输出功率
        var sn_num = ''//sn
        var bee_sound_set_key = false//蜂鸣器设置-按键音
        var low_sound_set = false//静享模式
        var bee_sound_set_warning = false//蜂鸣器设置-警告声
        var switch_ac = '0'//ac输出开关
        var bat_health_set = false//电池健康开关
        var switch_conpower = '0'//u-turbo开关
        var ac_freq_set = 0//ac频率切换
        var time_shutdown = 0//定时关机时间
        var solar_energy_vol = 0//太阳能输入电压
        var work_mode = 0//工作模式
        var display_bright_set = 0//屏幕亮度调节
        var lamp_sw = '0'//照明灯 0:off 1:bright 2: high_bright 3: flash 4: sos
        var device_fault2 = ''//设备故障2
        var timeoff_cap = 0//定时关机容量
        var mainboard_solfware_version = ''//主板软件版本
        var cell_total_vol = 0//电池总电压
        var battery_percentage = 0//电池电量百分比
        var car_discharge_pow = 0//车充口放电总功率
        var bat_health = 0//电池健康度

        var result = {}
        Object.keys(value).map((key, index) => {
            var specValue = value[key]

            if (key == this.key_inverter_temp1) {
                if (!Helper.dataIsNull(specValue)) {
                    inverter_temp1 = specValue
                    result[this.key_inverter_temp1] = inverter_temp1
                }
            } else if (key == this.key_inverter_temp2) {
                if (!Helper.dataIsNull(specValue)) {
                    inverter_temp2 = specValue
                    result[this.key_inverter_temp2] = inverter_temp2
                }
            } else if (key == this.key_timeoff_zoom) {
                if (!Helper.dataIsNull(specValue)) {
                    timeoff_zoom = specValue
                    result[this.key_timeoff_zoom] = timeoff_zoom
                }
            } else if (key == this.key_type_c1_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    type_c1_vol = specValue
                    result[this.key_type_c1_vol] = type_c1_vol
                }
            } else if (key == this.key_type_c2_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    type_c2_vol = specValue
                    result[this.key_type_c2_vol] = type_c2_vol
                }
            }else if (key == this.key_type_c3_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    type_c3_vol = specValue
                    result[this.key_type_c3_vol] = type_c3_vol
                }
            }else if (key == this.key_type_c4_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    type_c4_vol = specValue
                    result[this.key_type_c4_vol] = type_c4_vol
                }
            } else if (key == this.key_discharge_remain_time) {
                if (!Helper.dataIsNull(specValue)) {
                    discharge_remain_time = specValue
                    result[this.key_discharge_remain_time] = discharge_remain_time
                }
            } else if (key == this.key_switch_dc) {
                if (!Helper.dataIsNull(specValue)) {
                    switch_dc = specValue
                    result[this.key_switch_dc] = switch_dc
                }
            } else if (key == this.key_discharge_pow) {
                if (!Helper.dataIsNull(specValue)) {
                    discharge_pow = specValue
                    result[this.key_discharge_pow] = discharge_pow
                }
            } else if (key == this.key_ac_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    ac_vol = specValue
                    result[this.key_ac_vol] = ac_vol
                }
            } else if (key == this.key_cell1_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell1_vol = specValue
                    result[this.key_cell1_vol] = cell1_vol
                }
            } else if (key == this.key_cell2_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell2_vol = specValue
                    result[this.key_cell2_vol] = cell2_vol
                }
            } else if (key == this.key_cell3_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell3_vol = specValue
                    result[this.key_cell3_vol] = cell3_vol
                }
            } else if (key == this.key_cell4_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell4_vol = specValue
                    result[this.key_cell4_vol] = cell4_vol
                }
            } else if (key == this.key_cell5_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell5_vol = specValue
                    result[this.key_cell5_vol] = cell5_vol
                }
            } else if (key == this.key_cell6_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell6_vol = specValue
                    result[this.key_cell6_vol] = cell6_vol
                }
            } else if (key == this.key_cell7_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell7_vol = specValue
                    result[this.key_cell7_vol] = cell7_vol
                }
            } else if (key == this.key_low_power_al_set) {
                if (!Helper.dataIsNull(specValue)) {
                    low_power_al_set = specValue
                    result[this.key_low_power_al_set] = low_power_al_set
                }
            } else if (key == this.key_car_charge_i_set) {
                if (!Helper.dataIsNull(specValue)) {
                    car_charge_i_set = specValue
                    result[this.key_car_charge_i_set] = car_charge_i_set
                }
            } else if (key == this.key_bat_cap_remain) {
                if (!Helper.dataIsNull(specValue)) {
                    bat_cap_remain = specValue
                    result[this.key_bat_cap_remain] = bat_cap_remain
                }
            } else if (key == this.key_usb_discharge_pow) {
                if (!Helper.dataIsNull(specValue)) {
                    usb_discharge_pow = specValue
                    result[this.key_usb_discharge_pow] = usb_discharge_pow
                }
            } else if (key == this.key_bluetooth_id) {
                if (!Helper.dataIsNull(specValue)) {
                    bluetooth_id = specValue
                    result[this.key_bluetooth_id] = bluetooth_id
                }
            } else if (key == this.key_ac_in_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    ac_in_vol = specValue
                    result[this.key_ac_in_vol] = ac_in_vol
                }
            } else if (key == this.key_self_check) {
                if (!Helper.dataIsNull(specValue)) {
                    self_check = specValue
                    result[this.key_self_check] = self_check
                }
            } else if (key == this.key_charge_remain_time) {
                if (!Helper.dataIsNull(specValue)) {
                    charge_remain_time = specValue
                    result[this.key_charge_remain_time] = charge_remain_time
                }
            } else if (key == this.key_bat_temp1) {
                if (!Helper.dataIsNull(specValue)) {
                    bat_temp1 = specValue
                    result[this.key_bat_temp1] = bat_temp1
                }
            } else if (key == this.key_bat_temp2) {
                if (!Helper.dataIsNull(specValue)) {
                    bat_temp2 = specValue
                    result[this.key_bat_temp2] = bat_temp2
                }
            } else if (key == this.key_usb1_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    usb1_vol = specValue
                    result[this.key_usb1_vol] = usb1_vol
                }
            } else if (key == this.key_usb2_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    usb2_vol = specValue
                    result[this.key_usb2_vol] = usb2_vol
                }
            } else if (key == this.key_usb3_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    usb3_vol = specValue
                    result[this.key_usb3_vol] = usb3_vol
                }
            } else if (key == this.key_usb4_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    usb4_vol = specValue
                    result[this.key_usb4_vol] = usb4_vol
                }
            }
             else if (key == this.key_bms_solfware_version) {
                if (!Helper.dataIsNull(specValue)) {
                    bms_solfware_version = specValue
                    result[this.key_bms_solfware_version] = bms_solfware_version
                }
            } else if (key == this.key_inv_solfware_version) {
                if (!Helper.dataIsNull(specValue)) {
                    inv_solfware_version = specValue
                    result[this.key_inv_solfware_version] = inv_solfware_version
                }
            } else if (key == this.key_ac_discharge_pow) {
                if (!Helper.dataIsNull(specValue)) {
                    ac_discharge_pow = specValue
                    result[this.key_ac_discharge_pow] = ac_discharge_pow
                }
            } else if (key == this.key_switch_lock) {
                if (!Helper.dataIsNull(specValue)) {
                    switch_lock = specValue
                    result[this.key_switch_lock] = switch_lock
                }
            } else if (key == this.key_timeoff_set) {
                if (!Helper.dataIsNull(specValue)) {
                    timeoff_set = specValue
                    result[this.key_timeoff_set] = timeoff_set
                }
            } else if (key == this.key_switch_all) {
                if (!Helper.dataIsNull(specValue)) {
                    switch_all = specValue
                    result[this.key_switch_all] = switch_all
                }
            } else if (key == this.key_time_dis_shutdown) {
                if (!Helper.dataIsNull(specValue)) {
                    time_dis_shutdown = specValue
                    result[this.key_time_dis_shutdown] = time_dis_shutdown
                }
            } else if (key == this.key_bat_cycle_num) {
                if (!Helper.dataIsNull(specValue)) {
                    bat_cycle_num = specValue
                    result[this.key_bat_cycle_num] = bat_cycle_num
                }
            } else if (key == this.key_charge_power_all) {
                if (!Helper.dataIsNull(specValue)) {
                    charge_power_all = specValue
                    result[this.key_charge_power_all] = charge_power_all
                }
            } else if (key == this.key_usb_sw) {
                if (!Helper.dataIsNull(specValue)) {
                    usb_sw = specValue
                    result[this.key_usb_sw] = usb_sw
                }
            } else if (key == this.key_low_battery) {
                if (!Helper.dataIsNull(specValue)) {
                    low_battery = specValue
                    result[this.key_low_battery] = low_battery
                }
            } else if (key == this.key_dc_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    dc_vol = specValue
                    result[this.key_dc_vol] = dc_vol
                }
            } else if (key == this.key_sn_num) {
                if (!Helper.dataIsNull(specValue)) {
                    sn_num = specValue
                    result[this.key_sn_num] = sn_num
                }
            } else if (key == this.key_bee_sound_set_key) {
                if (!Helper.dataIsNull(specValue)) {
                    bee_sound_set_key = specValue
                    result[this.key_bee_sound_set_key] = bee_sound_set_key
                }
            } else if (key == this.key_low_sound_set) {
                if (!Helper.dataIsNull(specValue)) {
                    low_sound_set = specValue
                    result[this.key_low_sound_set] = low_sound_set
                }
            } else if (key == this.key_bee_sound_set_warning) {
                if (!Helper.dataIsNull(specValue)) {
                    bee_sound_set_warning = specValue
                    result[this.key_bee_sound_set_warning] = bee_sound_set_warning
                }
            } else if (key == this.key_switch_ac) {
                if (!Helper.dataIsNull(specValue)) {
                    switch_ac = specValue
                    result[this.key_switch_ac] = switch_ac
                }
            } else if (key == this.key_bat_health_set) {
                if (!Helper.dataIsNull(specValue)) {
                    bat_health_set = specValue
                    result[this.key_bat_health_set] = bat_health_set
                }
            } else if (key == this.key_switch_conpower) {
                if (!Helper.dataIsNull(specValue)) {
                    switch_conpower = specValue
                    result[this.key_switch_conpower] = switch_conpower
                }
            } else if (key == this.key_ac_freq_set) {
                if (!Helper.dataIsNull(specValue)) {
                    ac_freq_set = specValue
                    result[this.key_ac_freq_set] = ac_freq_set
                }
            } else if (key == this.key_time_shutdown) {
                if (!Helper.dataIsNull(specValue)) {
                    time_shutdown = specValue
                    result[this.key_time_shutdown] = time_shutdown
                }
            } else if (key == this.key_solar_energy_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    solar_energy_vol = specValue
                    result[this.key_solar_energy_vol] = solar_energy_vol
                }
            } else if (key == this.key_work_mode) {
                if (!Helper.dataIsNull(specValue)) {
                    work_mode = specValue
                    result[this.key_work_mode] = work_mode
                }
            } else if (key == this.key_display_bright_set) {
                if (!Helper.dataIsNull(specValue)) {
                    display_bright_set = specValue
                    result[this.key_display_bright_set] = display_bright_set
                }
            } else if (key == this.key_lamp_sw) {
                if (!Helper.dataIsNull(specValue)) {
                    lamp_sw = specValue
                    result[this.key_lamp_sw] = lamp_sw
                }
            } else if (key == this.key_device_fault2) {
                if (!Helper.dataIsNull(specValue)) {
                    device_fault2 = specValue
                    result[this.key_device_fault2] = device_fault2
                }
            } else if (key == this.key_timeoff_cap) {
                if (!Helper.dataIsNull(specValue)) {
                    timeoff_cap = specValue
                    result[this.key_timeoff_cap] = timeoff_cap
                }
            } else if (key == this.key_mainboard_solfware_version) {
                if (!Helper.dataIsNull(specValue)) {
                    mainboard_solfware_version = specValue
                    result[this.key_mainboard_solfware_version] = mainboard_solfware_version
                }
            } else if (key == this.key_cell_total_vol) {
                if (!Helper.dataIsNull(specValue)) {
                    cell_total_vol = specValue
                    result[this.key_cell_total_vol] = cell_total_vol
                }
            } else if (key == this.key_battery_percentage) {
                if (!Helper.dataIsNull(specValue)) {
                    battery_percentage = specValue
                    result[this.key_battery_percentage] = battery_percentage
                }
            } else if (key == this.key_car_discharge_pow) {
                if (!Helper.dataIsNull(specValue)) {
                    car_discharge_pow = specValue
                    result[this.key_car_discharge_pow] = car_discharge_pow
                }
            } else if (key == this.key_bat_health) {
                if (!Helper.dataIsNull(specValue)) {
                    bat_health = specValue
                    result[this.key_bat_health] = bat_health
                }
            }
        });

        if (showLog) {

        }
        return result
    }

    static _getTruthBoolValue(specValue) {
        var boolValue = false
        if (specValue != '') {
            if (parseInt(specValue) == 1) {
                boolValue = true
            }
        }
        return boolValue
    }

    /////////////////////////////////////////////发送指令///////////////////////////////////////////////////////////////////
    
    //下发控制模式
    static _sendWorkMode(value) {
        //value: 0=normal、1=eco、2=keepon_single、3=keepon_rep
        this._sendSpecCodeByApi(SpecUtil.key_work_mode, value, false)
    }


    //蓝牙下发数据点指令  params例子：{Switch_light:true}（已弃用）
    static _sendAcOutputPowerSwitch(account, pk, mac, params) {
        if (account == '' || pk == '' || mac == '' || params == '') {
            LogUtil.debugLog('数据点下发===>参数为空')
            return
        }
        LogUtil.debugLog('数据点下发：'
            + '\naccount:' + account
            + '\npk:' + pk
            + '\nmac:' + mac
            + '\nparams:' + JSON.stringify(params))
        NativeModules.RNModule.setDevParams(account, pk, mac, params);
    }

    static _sendSpecCodeByApi(key, value, showLoading) {
        if (isWifiConnect) {
            if (token == '' || deviceId == '') {
                LogUtil.debugLog('_sendSpecCodeByApi数据点下发===>参数为空')
                return
            }
            LogUtil.debugLog('_sendSpecCodeByApi数据点下发：'
                + '\nkey:' + key + ',value:' + value)
            NetUtil.deviceControlV3(token, deviceId, key, value, showLoading)
                .then((res) => {

                }).catch((error) => {

            })
        } else {
            //蓝牙下发指令  (暂无处理)
            /*var specsArray = []
            Object.keys(data).map((key, index) => {
                var specValue = data[key]
                specsArray.push({
                    specId: key,
                    specValue: this.getBleValue(specValue)
                })
            });
            //转成16进制指令
            var mulSpecCode = Bluetooth._mulSpec2Hex(specsArray)
            //发送指令
            LogUtil.debugLog('BleModule_log_ble蓝牙下发指令===>mulSpecCode:' + mulSpecCode + '\n' + JSON.stringify(data))
            if (Platform.OS === "android") {
                NativeModules.RNBLEModule.sendData(mulSpecCode)
            } else {
                GXRNManager.sendData(mulSpecCode);
            }*/
        }
    }

    //蓝牙下发指令  上报需要显示的数据
    static _bleSendQueryCode() {
        //转成16进制指令
        var singleSpecCode = Bluetooth._singleSpec2Hex('26', this.getBleValue('1'))
        //发送指令
        LogUtil.debugLog('BleModule_log_ble:蓝牙下发指令===>上报需要显示的数据:' + singleSpecCode)
        if (Platform.OS === "android") {
            NativeModules.RNBLEModule.sendData(singleSpecCode)
        } else {
            GXRNManager.sendData(singleSpecCode);
        }
    }

    static _sendSpecCodeByApiWithParams(data, token, deviceId, showLoading) {
        var sendData = {
            deviceId: deviceId,
            data: data
        }

        if (token == '' || deviceId == '' || data == '') {
            LogUtil.debugLog('_sendSpecCodeByApi数据点下发===>参数为空')
            return
        }
        LogUtil.debugLog('_sendSpecCodeByApiWithParams数据点下发：'
            + '\nsendData:' + JSON.stringify(sendData))
        NetUtil.deviceControl16(token, sendData, showLoading)
            .then((res) => {

            }).catch((error) => {

        })
    }

    static valueTranslate(value) {
        //return (((parseFloat(value) * 0.1) * 10) / 10).toFixed(1)
        return Math.round((parseFloat(value) * 0.1))
    }

    static getBleValue(value) {
        var bleValue = value
        if (value == true) {
            bleValue = 1
        } else if (value == false) {
            bleValue = 0
        }

        return bleValue
    }
}