import {
    Alert,
    NativeModules,
    Platform,
} from 'react-native';
import I18n from "react-native-i18n";
import {strings} from "../Language/I18n";
import LogUtil from "./LogUtil";
import moment from 'moment';

export default class Helper {
    static DEVICE_OWN_TYPE_ALL = 1//所有者
    static DEVICE_OWN_TYPE_MANAGER = 2//管理员
    static DEVICE_OWN_TYPE_MEMBER = 3//成员

    static type_login_facebook = 1
    static type_login_google = 2
    static type_login_apple = 3

    static channel_login_facebook = 'Facebook'
    static channel_login_google = 'Google'
    static channel_login_apple = 'Apple'

    static type_error_176_3 = 1
    static type_error_168_10 = 2
    static type_error_170_4 = 3
    static type_error_170_5 = 4
    static type_error_170_8 = 5
    static type_error_170_9 = 5
    static type_error_170_10 = 5
    static type_error_170_11 = 5
    static type_error_170_3 = 6

    static _ifValidEmail(email) {//判断是否有效邮箱
        let value = email;
        if (!(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(value))) {
            return false
        } else
            return true
    }

    static _encryptedPhoneNumber(phoneNumber) {//加密手机号
        let encryptedNumber = phoneNumber?.replace(/(\d{3})\d{4}(\d{4})/, '$1****$2');
        return encryptedNumber;
    }

    // 格式化时间 示例：时间戳=》2022-12-12 23:44
    static formatTimestamp = (timestamp) => {

        const date = new Date(Number(timestamp) * 1000); // 时间戳转换为毫秒  
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0'); // 月份从0开始，所以需要+1  
        const day = String(date.getDate()).padStart(2, '0');
        const hours = String(date.getHours()).padStart(2, '0');
        const minutes = String(date.getMinutes()).padStart(2, '0');

        return `${year}-${month}-${day} ${hours}:${minutes}`;
    }

    // celsius摄氏度转换成华氏度fahrenheit
    static temperatureUnitConvert = (offlineDisableMode, temperatureUnit, value) => {
        // temperatureUnit: 0 摄氏度 1:华氏度
        let temperatureUnitValue;
        // let celsius =  (Number(value) - 32) * 5 / 9;
        if (!temperatureUnit) {
            temperatureUnitValue = (!offlineDisableMode && value !== undefined && value !== '' && value !== null) ? `${Number(value)?.toFixed(0)}℃` : '-℃';
        } else {
            temperatureUnitValue = (!offlineDisableMode && value !== undefined && value !== '' && value !== null) ? `${((Number(value) * 9 / 5) + 32)?.toFixed(1)}℉` : '-℉';
        }
        return temperatureUnitValue;
    }

    // 匹配state和下发的参数名后请求
    static getKey = (key, keyData) => {
        let currentValue;
        keyData?.forEach((element) => {
            if (element?.key1 === key) {
                currentValue = element?.key2;
            }
        });
        return currentValue;
    };

    // 处理日期格式
    static _formatTimestamp(timestamp) {
        // 将数字时间戳转换为可被 moment() 解析的日期字符串  
        let dateString = new Date(timestamp).toISOString();
        // 创建一个 moment 对象，它将根据给定的时间戳进行初始化  
        let date = moment(dateString);
        // 设置语言环境为中文  
        // date.locale('zh-cn');  
        // 判断是今天、昨天还是之前的日期  
        let day = date.format('YYYY-MM-DD HH:mm:ss');
        let time = '';
        let relativeDate = '';
        if (date.isSame(moment(), 'day')) {
            relativeDate = strings('今天');
            // 获取小时和分钟，并格式化为 '时:分' 的格式  
            time = date.format('HH:mm');
        } else if (date.isSame(moment().subtract(1, 'days'), 'day')) {
            relativeDate = strings('昨天');
            time = date.format('HH:mm');
        } else {
            relativeDate = day; //输出格式为 "YYYY-MM-DD HH:mm:ss"
        }
        return relativeDate + ' ' + time;
    }


    static _ifValidPhone(phoneNumber) {//判断手机号码是否有效
        const regex = /^1[3-9]\d{9}$/;
        return regex.test(phoneNumber);
    }

    static _ifValidPassword(pwText) {//密码中必须包含字母（不区分大小写）、数字，至少6个字符，最多32个字符；
        var pwdRegex = new RegExp('(?=.*[0-9])(?=.*[a-zA-Z]).{8,16}');

        if (!pwdRegex.test(pwText)) {
            return false
        } else
            return true

    }

    static isRealNum(val) {//判断是否数字
        // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除，

        if (val === "" || val == null) {
            return false;
        }
        if (!isNaN(val)) {
            //对于空数组和只有一个数值成员的数组或全是数字组成的字符串，isNaN返回false，例如：'123'、[]、[2]、['123'],isNaN返回false,
            //所以如果不需要val包含这些特殊情况，则这个判断改写为if(!isNaN(val) && typeof val === 'number' )
            return true;
        } else {
            return false;
        }
    }

    static getRequestPlatformOs() {
        if (Platform.OS == 'android') {
            return 'androidapp'
        } else {
            return 'iosapp'
        }
    }

    static getPlatformOsFlag() {
        if (Platform.OS == 'android') {
            return '1'
        } else {
            return '0'
        }
    }

    static getLanguageType() {
        //【1】简体中文、【2】繁体中文、【3】英语
        var localLanguage = I18n.locale == 'en' ? 3 : 1

        return localLanguage
    }

    static getMobileChannel() {
        return Platform.OS == 'android' ? 'APP_PUSH_ANDROID' : 'APP_PUSH_IOS'
    }

    static openSettings(title) {

        Alert.alert(
            '',
            title,
            [
                {
                    text: 'cancel',
                    onPress: () => LogUtil.debugLog('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'sure', onPress: () => {
                        if (Platform.OS == 'android') {
                            NativeModules.OpenSettings.openNetworkSettings(data => {
                                LogUtil.debugLog('call back data', data)
                            })
                        } else {
                            Linking.openURL('app-settings:')
                                .catch((err) => LogUtil.debugLog('error', err));
                        }
                    },
                },
            ],
            {cancelable: false},
        );


    }

    static alertDialog(title, message, confirmClick, cancelClick) {

        Alert.alert(
            title,
            message,
            [
                {
                    text: strings('cancel'),
                    onPress: () => {
                        if (cancelClick) {
                            cancelClick()
                        }
                    },
                    style: 'cancel',
                },
                {
                    text: strings('confirm'), onPress: confirmClick,
                },
            ],
            {cancelable: false},
        );


    }

    //获取一行三列的数组
    //[
    //[{},{},{}],
    //[{},{},{}],
    // ]
    static _getArray3(array) {
        var tempArrary = []
        var flag = 0
        array.map((item, index) => {
            if (flag == index) {
                var smallArray = []
                if (item)
                    smallArray.push(item)
                if (array[index + 1])
                    smallArray.push(array[index + 1])
                if (array[index + 2])
                    smallArray.push(array[index + 2])

                tempArrary.push(smallArray)
                flag = flag + 3
            }
        })
        return tempArrary
    }

    static getFormatDHbyHour(hour) {
        var value = ''
        var day = parseInt(hour / 24)
        var hour = hour % 24

        if (day > 0) {
            value = strings('day_hour_value')(day, hour)
        } else {
            value = strings('hour_value')(hour)
        }

        return value
    }

    static dateCount(min) {
        if (parseInt(min) == 0) {
            return ''
        }
        var value = ''

        // 现在时间
        var now = new Date();
        var endTime = now.getTime() + (min * 60 * 1000)
        //截止时间
        var until = new Date(endTime);
        // 计算时会发生隐式转换，调用valueOf()方法，转化成时间戳的形式
        var days = (until - now) / 1000 / 3600 / 24;
        // 下面都是简单的数学计算
        var day = Math.floor(days);
        var hours = (days - day) * 24;
        var hour = Math.floor(hours);
        var minutes = (hours - hour) * 60;
        var minute = Math.floor(minutes);
        var seconds = (minutes - minute) * 60;
        var second = Math.floor(seconds);
        // var back = '剩余时间：' + day + '天' + hour + '小时' + minute + '分钟' + second + '秒';
        // return back;

        if (parseInt(min) > 24 * 60) {
            //显示天、小时
            value = strings('day_hour_value')(day, hour)
        } else {
            //显示小时、分钟
            value = strings('hour_min_value')(hour, minute)
        }
        return value
    }

    // 摄氏度转华氏度
    static _centigradeTofahrenheitL(degree) {
        return (32 + (degree * 1.8)).toFixed(1);
    }

    // 华氏度转摄氏度
    static _fahrenheitTocentigradeL(fahrenheit) {
        return ((fahrenheit - 32) / 1.8).toFixed(1);
    }

    static getTextErrorTitle(type) {
        var errorTitle = ''
        if (type == this.type_error_176_3 || type == this.type_error_168_10 || type == this.type_error_170_5) {
            errorTitle = strings('warming')
        } else if (type == this.type_error_170_4) {
            errorTitle = strings('remind')
        } else if (type == this.type_error_170_8 || type == this.type_error_170_9 || type == this.type_error_170_10 || type == this.type_error_170_11 || type == this.type_error_170_3) {
            errorTitle = strings('shutdown_warming')
        }

        return errorTitle
    }

    static getTextErrorTips(type, value) {
        var errorText = ''
        if (type == this.type_error_176_3) {
            errorText = strings('text_error_176_3')
        } else if (type == this.type_error_168_10) {
            errorText = strings('text_error_168_10')
        } else if (type == this.type_error_170_4) {
            errorText = strings('text_error_170_4')(value)
        } else if (type == this.type_error_170_5) {
            errorText = strings('text_error_170_5')
        } else if (type == this.type_error_170_8 || type == this.type_error_170_9 || type == this.type_error_170_10 || type == this.type_error_170_11) {
            errorText = strings('text_error_170_8_9_10_11')
        } else if (type == this.type_error_170_3) {
            errorText = strings('text_error_170_3')(value)
        }

        return errorText
    }

    //（华氏度=摄氏度×1.8+32)
    static getFTempVlaue(temp) {
        return (temp * 1.8 + 32).toFixed(1)
    }

    static getSTempVlaue(hTemp) {
        return (hTemp - 32) / 1.8
    }

    static compare(property) {
        return function (a, b) {
            let value1 = a[property];
            let value2 = b[property];
            return value1 - value2;
        };
    }

    static compareDown(property) {
        return function (a, b) {
            let value1 = a[property];
            let value2 = b[property];
            return value2 - value1;
        };
    }

    //处理产品分类列表
    static _resolveAllProductInfo(productArray) {
        var productTypeIdArray = []
        productArray.map((item, index) => {
            if (productTypeIdArray.indexOf(item.productTypeId) == -1) {
                productTypeIdArray.push(item.productTypeId)
            }
        })

        var productBigClassArray = []
        productTypeIdArray.map((item, index) => {
            var productSmallClassArray = []
            var className = ''
            productArray.map((row, pos) => {
                if (item == row.productTypeId) {
                    className = row.productTypeName
                    productSmallClassArray.push(row)
                }
            })
            productBigClassArray.push({
                name: className,
                data: this._getArray3(productSmallClassArray.sort(this.compare('createTime')))
            })
        })

        return productBigClassArray
    }

    /**
     * 判断是否5Gwifi
     * @param frequency
     * @return
     */
    static is5GHzWifi(frequency) {
        return frequency > 4900 && frequency < 5900;
    }

    static stringToByte(str) {
        var bytes = new Array();
        var len, c;
        len = str.length;
        for (var i = 0; i < len; i++) {
            c = str.charCodeAt(i);
            if (c >= 0x010000 && c <= 0x10FFFF) {
                bytes.push(((c >> 18) & 0x07) | 0xF0);
                bytes.push(((c >> 12) & 0x3F) | 0x80);
                bytes.push(((c >> 6) & 0x3F) | 0x80);
                bytes.push((c & 0x3F) | 0x80);
            } else if (c >= 0x000800 && c <= 0x00FFFF) {
                bytes.push(((c >> 12) & 0x0F) | 0xE0);
                bytes.push(((c >> 6) & 0x3F) | 0x80);
                bytes.push((c & 0x3F) | 0x80);
            } else if (c >= 0x000080 && c <= 0x0007FF) {
                bytes.push(((c >> 6) & 0x1F) | 0xC0);
                bytes.push((c & 0x3F) | 0x80);
            } else {
                bytes.push(c & 0xFF);
            }
        }
        return bytes;


    }


    static byteToString(arr) {
        if (typeof arr === 'string') {
            return arr;
        }
        var str = '',
            _arr = arr;
        for (var i = 0; i < _arr.length; i++) {
            var one = _arr[i].toString(2),
                v = one.match(/^1+?(?=0)/);
            if (v && one.length == 8) {
                var bytesLength = v[0].length;
                var store = _arr[i].toString(2).slice(7 - bytesLength);
                for (var st = 1; st < bytesLength; st++) {
                    store += _arr[st + i].toString(2).slice(2);
                }
                str += String.fromCharCode(parseInt(store, 2));
                i += bytesLength - 1;
            } else {
                str += String.fromCharCode(_arr[i]);
            }
        }
        return str;
    }

    static dataIsNull(data) {
        var strValue = data + ''
        if (strValue == '0' || strValue == 'false')
            return false
        return data == '' || strValue == 'NaN' || data == NaN
            || data == undefined || strValue == 'undefined' || data == null || strValue == 'null'
    }

    static dataNanReset(data) {
        var strValue = data + ''
        return (strValue.toString() == 'NaN' || data == NaN || data == undefined) ? '-' : data
    }
}