import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import ViewHelper from "../View/ViewHelper";
import TextInputView from "../View/TextInputView";
import CommonCheckView from "../View/CommonCheckView";
import CommonBtnView from "../View/CommonBtnView";
import CommonTextView from "../View/CommonTextView";
import StorageHelper from "../Utils/StorageHelper";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import RouterUtil from "../Utils/RouterUtil";
import NetConstants from "../Net/NetConstants";
import CountdownUtil from "../Utils/CountdownUtil";
import TimerUtil from "../Utils/TimerUtil";
import ShadowCardView from "../View/ShadowCardView";

/*
*注册页面
 */
export default class BindEmail extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            emailAccount: '',
            passwordText: '',
            comfirePasswordText: '',
            verCode: '',
            verCodeText: strings('get_vail_code'),
            ruleCheck: false,
            showPdError1: true,
            showPdError2: true,
            data: props.navigation.state.params.data,
            loginType: props.navigation.state.params.loginType,
            ruleUrl: '',//用户协议
            privacyUrl: '',//隐私协议
            submitBtnEnable: false,
            getCodeAble: true,
            setPdFlag: false,
            sureBtnEnable: false,
            loginInfo: '',
            info: props.navigation.state.params.info,
            token: '',
            userKey: '',
            accountValue: '',
        }
    }

    componentWillMount() {
        var info = this.state.info
        if (info && parseInt(info.register) == 2) {
            this.setState({
                setPdFlag: parseInt(info.register) == 2,
                loginInfo: info,
                userKey: info.userKey,
                accountValue: info.data.account
            })
            if (info.data.granwin_token) {
                this.setState({
                    token: info.data.granwin_token
                })
            }
        }
        this._getRule(false)
        this._getPrivacy(false)
        this.setPdErrorView(this.state.passwordText, this.state.comfirePasswordText)
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB'
                }}>
                {this._titleView()}

                <View
                    style={{
                        width: this.mScreenWidth,
                        alignItems: 'center',
                        flex: 1,
                        paddingTop: 50
                    }}>
                    {
                        this.state.setPdFlag ?
                            this._setPdContainer() : this._bindEmailContainer()
                    }

                    {
                        !this.state.setPdFlag ?
                            <CommonTextView
                                textSize={14}
                                text={strings('bind_email_tips')}
                                style={{
                                    color: '#BBBEC1',
                                    width: this.viewCommonWith,
                                    marginTop: 15
                                }}/> : null
                    }
                    {ViewHelper.getFlexView()}
                </View>
            </View>
        );
    }

    _accountView() {
        return (
            <ShadowCardView
                style={{
                    alignItems: 'center',
                    height: 56,
                    justifyContent: 'center',
                    width: this.mScreenWidth * 0.73,
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000000',
                    }}
                    text={this.state.accountValue}/>
            </ShadowCardView>
        )
    }

    _bindEmailContainer() {
        return (
            <View>
                {this._emailView()}
                {this._getVerCodeView()}
                {this._verCodeView()}
                {this._ruleView()}
                {this._signUpBtnView()}
            </View>
        )
    }

    _setPdContainer() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                {this._accountView()}
                {this._passwordView()}
                {this.state.showPdError1 && this.state.passwordText ? this._pdErrorTipsView() : null}
                {this._passwordView1()}
                {this.state.showPdError2 && this.state.comfirePasswordText ? this._pdErrorTipsView() : null}
                {this._setPdSureBtnView()}
            </View>
        )
    }

    _setPdSureBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.sureBtnEnable}
                onPress={() => {
                    //确认
                    this._setPdSure()
                }}
                style={{
                    marginTop: 25,
                }}
                btnText={strings('confirm')}/>
        )
    }

    _setPdSure() {
        if (this.state.passwordText != this.state.comfirePasswordText) {
            this.onShowToast(strings('tiwce_pd_dif'))
            return
        }

        if (this.state.userKey && this.state.token) {
            NetUtil.userSetPd(this.state.passwordText, this.state.userKey, this.state.token, true)
                .then((res) => {
                    if (this.state.loginInfo) {
                        this._saveTokenAndToMain(this.state.loginInfo)
                    }
                }).catch((error) => {

            })
        }
    }

    _getSureBtnEnable() {
        var flag = (Helper._ifValidPassword(this.state.passwordText)
            && Helper._ifValidPassword(this.state.comfirePasswordText))
        this.setState({
            sureBtnEnable: flag
        })
    }

    _titleView() {
        return (
            <CommonTextView
                textSize={24}
                text={this.state.setPdFlag ? strings('set_pd') : strings('bind_email')}
                style={{
                    color: '#000000',
                    marginLeft: 18
                }}
            />
        )
    }

    _emailView() {
        return (
            <TextInputView
                maxLength={64}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({emailAccount: newText})

                    this.state.emailAccount = newText
                    this._getBtnEnable()
                }}
                value={this.state.emailAccount}
                placeholder={strings('input_email_account')}
                keyboardType={'email-address'}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}
            />
        )
    }

    _passwordView() {
        return (
            <TextInputView
                style={{
                    marginTop: 18
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText: newText})

                    this.setPdErrorView(newText, this.state.comfirePasswordText)

                    this.state.passwordText = newText
                    this._getSureBtnEnable()
                }}
                value={this.state.passwordText}
                placeholder={strings('input_password')}
                keyboardType={'default'}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}/>
        )
    }

    _passwordView1() {
        return (
            <TextInputView
                style={{
                    marginTop: 18
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({comfirePasswordText: newText})

                    this.setPdErrorView(this.state.passwordText, newText)

                    this.state.comfirePasswordText = newText
                    this._getSureBtnEnable()
                }}
                value={this.state.comfirePasswordText}
                placeholder={strings('sure_password')}
                keyboardType={'default'}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}/>
        )
    }

    _verCodeView() {
        return (
            <TextInputView
                style={{
                    marginTop: 14
                }}
                maxLength={6}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({verCode: newText})

                    this.state.verCode = newText
                    this._getBtnEnable()
                }}
                value={this.state.verCode}
                placeholder={strings('input_vail_code')}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}
            />
        )
    }

    _getVerCodeView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginTop: 18,
                    width: this.viewCommonWith
                }}>
                {ViewHelper.getFlexView()}

                <TouchableOpacity
                    activeOpacity={this.state.getCodeAble ? 0.5 : 1}
                    onPress={() => {
                        this._getVerCode()
                    }}>
                    <CommonTextView
                        textSize={14}
                        text={this.state.verCodeText}
                        style={{
                            color: '#D0D0D0'
                        }}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    _ruleView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.viewCommonWith,
                    marginTop: 25,
                }}>
                <CommonCheckView
                    style={{
                        marginRight: 9
                    }}
                    onCheck={(isCheck) => {
                        this.setState({ruleCheck: isCheck})

                        this.state.ruleCheck = isCheck
                        this._getBtnEnable()
                    }}
                    isCheck={this.state.ruleCheck}/>

                <Text
                    style={{
                        fontSize: this.getSize(11),
                        color: '#000',
                        fontFamily: 'DIN Alternate Bold'
                    }}>
                    {strings('had_read_agree')}

                    <Text
                        onPress={() => {
                            //用户协议
                            this._toRule()
                        }}
                        style={{
                            fontSize: this.getSize(11),
                            color: '#38579D',
                            fontFamily: 'DIN Alternate Bold'
                        }}>
                        {strings('user_rule')}
                    </Text>

                    <Text
                        style={{
                            fontSize: this.getSize(11),
                            color: '#000',
                            fontFamily: 'DIN Alternate Bold'
                        }}>
                        {strings('and')}
                    </Text>

                    <Text
                        onPress={() => {
                            //隐私政策
                            this._toPrivacy()
                        }}
                        style={{
                            fontSize: this.getSize(11),
                            color: '#38579D',
                            fontFamily: 'DIN Alternate Bold'
                        }}>
                        {strings('privacy_rule')}
                    </Text>
                </Text>
            </View>
        )
    }

    _signUpBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.submitBtnEnable}
                onPress={() => {
                    //提交
                    this._submitAndLogin()
                }}
                style={{
                    marginTop: 25,
                }}
                btnText={strings('submit_login')}/>
        )
    }

    _pdErrorTipsView() {
        return (
            <CommonTextView
                textSize={12}
                text={strings('input_password_error_tips')}
                style={{
                    color: '#DE7575',
                    width: this.viewCommonWith,
                    marginTop: 12
                }}
            />
        )
    }

    _getBtnEnable() {
        var flag = (this.state.emailAccount
            && this.state.verCode.length >= 5
            && this.state.ruleCheck)
        this.setState({
            submitBtnEnable: flag
        })
    }

    setPdErrorView(passwordText, comfirePasswordText) {
        this.setState({
            showPdError1: !Helper._ifValidPassword(passwordText),
            showPdError2: !Helper._ifValidPassword(comfirePasswordText),
        })
    }

    //获取验证码
    _getVerCode() {
        if (!this.state.getCodeAble) {
            return
        }

        if (!Helper._ifValidEmail(this.state.emailAccount)) {
            this.onShowToast(strings('input_valid_email_account'))
            return
        }

        this.setState({
            verCodeClickTime: new Date().getTime(),
            getCodeAble: false
        })


        NetUtil.getEmailVerCode(NetConstants.EMAIL_VER_CODE_BIND_EMAIL, this.state.emailAccount, true)
            .then((res) => {
                this._startTimer()
            }).catch((error) => {
            this.setState({
                verCodeText: strings('get_vail_code'),
                getCodeAble: true,
            });
        })


    }

    componentWillUnmount() {
        this._stopTimer()
    }

    _startTimer() {
        this._stopTimer()
        let countdownDate = new Date(new Date().getTime() + 61 * 1000);
        this.intervalTimer = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
            if (dif <= 0) {
                this.setState({
                    verCodeText: strings('get_vail_code'),
                    getCodeAble: true,
                });
                this._stopTimer()
            } else {
                this.setState({
                    verCodeText: parseInt(dif) + 's',
                    getCodeAble: false,
                });
            }
        }, 1000)
    }

    _stopTimer() {
        if (this.intervalTimer) {
            clearInterval(this.intervalTimer)
            this.intervalTimer = null
        }
    }

    _toRule() {
        if (this.state.ruleUrl == '') {
            this._getRule(true)
        } else {
            this._toWeb(this.state.ruleUrl, strings('user_rule'))
        }
    }

    _toPrivacy() {
        if (this.state.privacyUrl == '') {
            this._getPrivacy(true)
        } else {
            this._toWeb(this.state.privacyUrl, strings('privacy_rule'))
        }
    }

    _getRule(toWeb) {
        NetUtil.getRule(StorageHelper.getTempToken())
            .then((res) => {
                var url = res.info.path
                this.setState({ruleUrl: url})
                if (toWeb)
                    this._toWeb(url, strings('user_rule'))
            })
            .catch((error) => {
            })
    }

    _getPrivacy(toWeb) {
        NetUtil.getPrivacy(StorageHelper.getTempToken())
            .then((res) => {
                var url = res.info.path
                this.setState({privacyUrl: url})
                if (toWeb)
                    this._toWeb(url, strings('privacy_rule'))
            })
            .catch((error) => {
            })
    }

    _submitAndLogin() {
        if (!Helper._ifValidEmail(this.state.emailAccount)) {
            this.onShowToast(strings('input_valid_email_account'))
            return
        }

        if (this.state.data) {
            var name = this.state.data.name
            var key = this.state.data.id
            NetUtil.thirdBind(this.state.emailAccount, this.state.verCode, name, key, true)
                .then((res) => {
                    var resInfo = res.info
                    var newUser = resInfo.newUser
                    if (newUser) {
                        //新用户，需要设置密码
                        this.setState({
                            setPdFlag: true,
                            loginInfo: res,
                            userKey: resInfo.userKey,
                            accountValue: resInfo.account
                        })
                    } else {
                        //用户已存在，直接登录
                        this._saveTokenAndToMain(res)
                    }
                    if (resInfo.granwin_token) {
                        this.setState({
                            token: resInfo.granwin_token
                        })
                    }
                }).catch((error) => {

            })
        }
    }

    _toWeb(toUrl, title) {
        if (toUrl) {
            this.props.navigation.navigate('UrlWebView', {url: toUrl, title: title});
        }
    }

    _saveTokenAndToMain(res) {
        var token = res.info ? res.info.token : res.data.token
        var tempThirdAvatar = StorageHelper.getTempThirdAvatar()
        StorageHelper.saveThirdAvatar(tempThirdAvatar)
        StorageHelper.saveToken(token)
        StorageHelper.saveLoginInfo(res)
        RouterUtil.toMainDeviceList(token)
    }
}