import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import ViewHelper from "../View/ViewHelper";
import TextInputView from "../View/TextInputView";
import CommonCheckView from "../View/CommonCheckView";
import CommonBtnView from "../View/CommonBtnView";
import CommonTextView from "../View/CommonTextView";
import CountdownUtil from "../Utils/CountdownUtil";
import Helper from "../Utils/Helper";
import NetUtil from "../Net/NetUtil";
import NetConstants from "../Net/NetConstants";
import I18n from 'react-native-i18n';
// network_map 热点示意图 我的头像 消息图例
/*
*重置密码
 */
export default class ResetPassword extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    titleText={strings('reset_pd')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            phoneAccount:'',
            emailAccount: '',
            passwordText: '',
            comfirePasswordText: '',
            verCode: '',
            verCodeText: strings('get_vail_code'),
            showPdError1: false,
            showPdError2: false,
            getCodeAble: true,
            submitBtnEnable: false,
            verCodeClickTime: 0
        }
    }

    componentWillMount() {
        this.setPdErrorView(this.state.passwordText, this.state.comfirePasswordText)
    }

    componentWillUnmount() {
        this._stopTimer()
    }

    render() {
        const {showPdError1, passwordText, showPdError2, verCode,comfirePasswordText,submitBtnEnable,emailAccount,phoneAccount} = this.state;
        const _emailView = (
            <TextInputView
            maxLength={64}
            onChangeText={(text) => {
                const newText = text.replace(' ', '');
                this.setState({emailAccount: newText},()=>{
                    this._getBtnEnable()
                })
                
            }}
            value={emailAccount}
            placeholder={strings('input_email_account')}
            keyboardType={'email-address'}//邮箱
            placeholderTextColor={'#C4C6CD'}
            isEmail={true}
        />)
        
        const _phoneView = (
            <TextInputView
                maxLength={64}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({phoneAccount: newText},()=>{
                        this._getBtnEnable()
                    })
                }}
                value={phoneAccount}
                placeholder={strings('input_phone_account')}
                keyboardType={'phone-pad'}//手机
                placeholderTextColor={'#C4C6CD'}
                isPhone={true}
            />)
        const _verCodeView = (
            <TextInputView
            style={{
                marginTop: 18
            }}
            verifyCodeLogin={true}
            maxLength={6}
            onChangeText={(text) => {
                const newText = text.replace(' ', '');
                this.setState({verCode: newText},()=>{
                    this._getBtnEnable()
                })
            }}
            value={verCode}
            placeholder={strings('please_enter_verification_code')}
            keyboardType={'numeric'}
            placeholderTextColor={'#C4C6CD'}
            onPress={()=>this._getVerCode()}
            getCodeAble={this.state.getCodeAble}
            verCodeText={this.state.verCodeText}
            {...this.props}/>
            )
        const _passwordView = (
            <TextInputView
                key={0}
                style={{
                    marginTop: 18
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText: newText},()=>{
                        this._getBtnEnable()
                    })
                    this.setPdErrorView(newText, comfirePasswordText)
                }}
                value={passwordText}
                placeholder={strings('input_password')}
                keyboardType={'default'}
                placeholderTextColor={'#C4C6CD'}/>)
        const _pdErrorTipsView = (
            <CommonTextView
                textSize={12}
                text={strings('input_password_error_tips')}
                style={{
                    color: '#DE7575',
                    width: this.viewCommonWith,
                    marginTop: 12
                }}
            />)
        const _passwordView1 =(
            <TextInputView
                key={1}
                style={{
                    marginTop: 18
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({comfirePasswordText: newText},()=>{
                        this._getBtnEnable()
                    })
                    this.setPdErrorView(passwordText, newText)
                }}
                value={comfirePasswordText}
                placeholder={strings('sure_password')}
                keyboardType={'default'}
                placeholderTextColor={'#C4C6CD'}/>)
        const _signUpBtnView = (
            <CommonBtnView
                clickAble={submitBtnEnable}
                onPress={() => {
                    //完成重置
                    this._submit()
                }}
                style={{marginVertical:32}}
                btnText={strings('submit_reset_password')}/>)    
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: this.mScreenWidth,
                        alignItems: 'center',
                        flex: 1,
                        paddingTop:54,
                    }}>
                    {I18n.locale === 'en' ? _emailView : _phoneView}
                    {_verCodeView}
                    {_passwordView}
                    {showPdError1 && passwordText ? _pdErrorTipsView : null}
                    {_passwordView1}
                    {showPdError2 && comfirePasswordText ? _pdErrorTipsView : null}
                    {_signUpBtnView}
                </View>
            </View>
        );
    }

    setPdErrorView(passwordText, comfirePasswordText) {
        this.setState({
            showPdError1: !Helper._ifValidPassword(passwordText),
            showPdError2: !Helper._ifValidPassword(comfirePasswordText),
        })
    }

    _getBtnEnable() {
        var flag = ((this.state.emailAccount || this.state.phoneAccount)
            && Helper._ifValidPassword(this.state.passwordText)
            && Helper._ifValidPassword(this.state.comfirePasswordText)
            && this.state.verCode.length >= 5)

        this.setState({
            submitBtnEnable: flag
        })
    }

    //提交
    _submit() {
        const {emailAccount, phoneAccount, passwordText, verCode, comfirePasswordText} = this.state
        if (emailAccount == '' && I18n.locale === 'en') {
            this.onShowToast(strings('input_email_account'))
            return
        }
        if (phoneAccount == '' && I18n.locale === 'zh') {
            this.onShowToast(strings('input_phone_account'))
            return
        } else if (!Helper._ifValidEmail(emailAccount) && I18n.locale === 'en') {
            this.onShowToast(strings('input_valid_email_account'))
            return
        }else if (!Helper._ifValidPhone(phoneAccount) && I18n.locale === 'zh') {
            this.onShowToast(strings('input_valid_phone_account'))
            return
        }else if(!verCode){
            this.onShowToast(strings('please_enter_verification_code'))
            return
        }else if (passwordText != comfirePasswordText) {
            this.onShowToast(strings('tiwce_pd_dif'))
            return
        }
        const account = I18n.locale === 'en' ? emailAccount : phoneAccount;
        NetUtil.pdReset(account, passwordText, verCode, true)
            .then((res) => {
                this.onShowToast(strings('update_success'))
                this.props.navigation.goBack();
            }).catch((error) => {

        })
    }


    //获取验证码
    _getVerCode() {
        const {getCodeAble, emailAccount, verCodeText, verCodeClickTime, phoneAccount} = this.state;
        const lang = I18n.locale === 'en' ? 'en-US ' : 'zh_CN'
        const account = I18n.locale === 'en' ? emailAccount : phoneAccount;
        if (!getCodeAble) {
            return
        }
        if (!Helper._ifValidEmail(emailAccount) && I18n.locale === 'en') {
            this.onShowToast(strings('input_valid_email_account'))
            return
        }
        if (!Helper._ifValidPhone(phoneAccount) && I18n.locale === 'zh') {
            this.onShowToast(strings('input_valid_phone_account'))
            return
        }

        this.setState({
            verCodeClickTime: new Date().getTime(),
            getCodeAble: false
        })


        NetUtil.getEmailVerCode(NetConstants.EMAIL_VER_CODE_PWD_RESET, account, true)
            .then((res) => {
                this._startTimer()
            }).catch((error) => {
            this.setState({
                verCodeText: strings('get_vail_code'),
                getCodeAble: true,
            });
        })


    }

    _startTimer() {
        this._stopTimer()
        let countdownDate = new Date(new Date().getTime() + 61 * 1000);
        this.intervalTimer = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
            if (dif <= 0) {
                this.setState({
                    verCodeText: strings('get_vail_code'),
                    getCodeAble: true,
                });
                this._stopTimer()
            } else {
                this.setState({
                    verCodeText: parseInt(dif) + 's',
                    getCodeAble: false,
                });
            }
        }, 1000)
    }

    _stopTimer() {
        if (this.intervalTimer) {
            clearInterval(this.intervalTimer)
            this.intervalTimer = null
        }
    }

}