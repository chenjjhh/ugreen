import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {ImageBackground, NativeModules, Platform, ScrollView, StatusBar, Text, View} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings} from '../Language/I18n';
import TextInputView from "../View/TextInputView";
import CommonCheckView from "../View/CommonCheckView";
import CommonBtnView from "../View/CommonBtnView";
import CommonTextView from "../View/CommonTextView";
import Helper from "../Utils/Helper";
import NetUtil from "../Net/NetUtil";
import NetConstants from "../Net/NetConstants";
import StorageHelper from "../Utils/StorageHelper";
import {_titleView} from '../Login'
import ChooseList from '../View/ChooseList'
import I18n from 'react-native-i18n';
import MessageChildDialog from "../View/MessageChildDialog";

/*
*注册页面
*/
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;
export default class Register extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'rgba(255,255,255,0)'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            phoneAccount: '',
            emailAccount: '',
            passwordText: '',
            comfirePasswordText: '',
            verCode: '',
            verCodeText: strings('get_vail_code'),
            ruleCheck: false,
            showPdError1: false,
            showPdError2: false,
            getCodeAble: true,
            submitBtnEnable: false,
            verCodeClickTime: 0,
            ruleUrl: '',//用户协议
            privacyUrl: '',//隐私协议
            agreeDialog:false,
            phoneCodeData: {
                phoneCode: '+1',
                name: '美国'
            },//选择国家数据
        }
    }

    componentWillMount() {
        this._getRule(false)
        this._getPrivacy(false)
        this.setPdErrorView(this.state.passwordText, this.state.comfirePasswordText)
    }

    componentWillUnmount() {
        this._stopTimer()
    }

    render() {
        const {
            showPdError1,
            passwordText,
            showPdError2,
            phoneCodeData,
            comfirePasswordText,
            submitBtnEnable,
            phoneAccount
        } = this.state;
        const _CountryView = (
            <ChooseList
                style={{marginBottom: 20}}
                onPress={() => {
                }}
                text={phoneCodeData?.name}
                leftIcon={require('../../resources/greenUnion/log_countory_ic.png')}
                rightIcon={require('../../resources/greenUnion/list_arrow_ic.png')}
            />)
        const _emailView = (
            <TextInputView
                key={0}
                maxLength={64}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({emailAccount: newText}, () => {
                        this._getBtnEnable()
                    })

                }}
                value={this.state.emailAccount}
                placeholder={strings('input_email_account')}
                keyboardType={'email-address'}//邮箱
                placeholderTextColor={'#C4C6CD'}
                isEmail={true}
            />)
        const _phoneView = (
            <TextInputView
                key={1}
                maxLength={64}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({phoneAccount: newText}, () => {
                        this._getBtnEnable()
                    })
                }}
                value={phoneAccount}
                placeholder={strings('input_phone_account')}
                keyboardType={'phone-pad'}//手机
                placeholderTextColor={'#C4C6CD'}
                isPhone={true}
            />)
        const _passwordView = (
            <TextInputView
                key={2}
                style={{
                    marginTop: 18
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText: newText}, () => {
                        this._getBtnEnable()
                    })
                    this.setPdErrorView(newText, comfirePasswordText)
                }}
                value={passwordText}
                placeholder={strings('input_password')}
                keyboardType={'default'}
                placeholderTextColor={'#C4C6CD'}
            />)
        const _pdErrorTipsView = (
            <CommonTextView
                textSize={12}
                text={strings('input_password_error_tips')}
                style={{
                    color: '#DE7575',
                    width: this.viewCommonWith,
                    marginTop: 12
                }}
            />)
        const _passwordView1 = (
            <TextInputView
                key={3}
                style={{
                    marginTop: 18
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({comfirePasswordText: newText}, () => {
                        this._getBtnEnable()
                    })
                    this.setPdErrorView(passwordText, newText)
                }}
                value={comfirePasswordText}
                placeholder={strings('sure_password')}
                keyboardType={'default'}
                placeholderTextColor={'#C4C6CD'}
            />)
        const _verCodeView = (
            <TextInputView
                key={4}
                style={{
                    marginTop: 18
                }}
                verifyCodeLogin={true}
                maxLength={6}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({verCode: newText}, () => {
                        this._getBtnEnable()
                    })
                }}
                value={this.state.verCode}
                placeholder={strings('please_enter_verification_code')}
                keyboardType={'numeric'}
                placeholderTextColor={'#C4C6CD'}
                onPress={() => this._getVerCode()}
                getCodeAble={this.state.getCodeAble}
                verCodeText={this.state.verCodeText}
                {...this.props}
            />)
        const _ruleView = (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.viewCommonWith,
                    marginVertical: 20
                }}>
                <CommonCheckView
                    style={{
                        marginRight: 9, marginTop: 3
                    }}
                    onCheck={(isCheck) => {
                        this.setState({ruleCheck: isCheck}, () => {
                            this._getBtnEnable()
                        })
                    }}
                    isCheck={this.state.ruleCheck}/>
                <Text>
                    <CommonTextView
                        textSize={12}
                        text={strings('agree')}
                        style={{
                            color: '#808080',
                            marginTop: 5
                        }}/>
                    <CommonTextView
                        onPress={() => {
                            //用户协议
                            this._toRule()
                        }}
                        textSize={12}
                        text={strings('user_agreement')}
                        style={{
                            color: '#18B34F',
                            marginTop: 5
                        }}/>
                    <CommonTextView
                        textSize={12}
                        text={strings('and')}
                        style={{
                            color: '#808080',
                            marginTop: 5
                        }}/>
                    <CommonTextView
                        onPress={() => {
                            //隐私政策
                            this._toPrivacy()
                        }}
                        textSize={12}
                        text={strings('personal_information')}
                        style={{
                            color: '#18B34F',
                            marginTop: 5
                        }}/>
                    <CommonTextView
                        textSize={12}
                        text={strings('and')}
                        style={{
                            color: '#808080',
                            marginTop: 5
                        }}/>
                    <CommonTextView
                        onPress={() => {
                        }}
                        textSize={12}
                        text={strings('third_party_information')}
                        style={{
                            color: '#18B34F',
                            marginTop: 5
                        }}/>
                </Text>
            </View>)
        const _signUpBtnView = (
            <CommonBtnView
                clickAble={submitBtnEnable}
                onPress={() => {
                    if (!this.state.ruleCheck) {
                        this.setState({agreeDialog: true});
                        return
                    }
                    //提交并注册
                    this._submitRegister()
                }}
                btnText={strings('submit_register')}/>)
        const _agreeDialogView = (
            <View>
                <MessageChildDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({agreeDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({agreeDialog: false});
                    }}
                    modalVisible={this.state.agreeDialog}
                    title={strings("同意使用")}
                    message={''}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({agreeDialog: false});
                    }}
                    rightBtnText={strings("同意")}
                    onRightBtnClick={() => {
                        //确认
                        this.setState({ruleCheck: true});
                        this.setState({agreeDialog: false}, () => {
                        });
                    }}
                    messageStyle={{
                        textAlign: "center",
                    }}
                >
                    <View
                        style={{
                            width: this.mScreenWidth - 80,
                            marginTop: 35,
                            marginBottom: 35,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <View style={{flex: 1}}>
                            <Text>
                                <CommonTextView
                                    textSize={12}
                                    text={strings("agree")}
                                    style={{
                                        color: "#808080",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    onPress={() => {
                                    }}
                                    textSize={12}
                                    text={strings("user_agreement")}
                                    style={{
                                        color: "#18B34F",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    textSize={12}
                                    text={strings("and")}
                                    style={{
                                        color: "#808080",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    onPress={() => {
                                    }}
                                    textSize={12}
                                    text={strings("personal_information")}
                                    style={{
                                        color: "#18B34F",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    textSize={12}
                                    text={strings("and")}
                                    style={{
                                        color: "#808080",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    onPress={() => {
                                    }}
                                    textSize={12}
                                    text={strings("third_party_information")}
                                    style={{
                                        color: "#18B34F",
                                        marginTop: 5,
                                    }}
                                />
                            </Text>
                        </View>
                    </View>
                </MessageChildDialog>
            </View>
        );

        return (
            <View style={{
                position: 'absolute',
                top: -(statusBarHeight + 55),
                height: this.mScreenHeight + statusBarHeight + 55,
                width: this.mScreenWidth
            }}>
                <ImageBackground
                    source={require('../../resources/greenUnion/loginBack.png')}
                    resizeMode={'stretch'}
                    style={{
                        flex: 1,
                        paddingTop: statusBarHeight + 55,
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                    <ScrollView style={{flex: 1}}>
                        <View style={{flex: 1, alignItems: 'center'}}>
                            <_titleView
                                style={{marginBottom: 40}}
                                text={strings('register_account')}
                                {...this.props}/>
                            {I18n.locale === 'en' ?
                                (<>
                                    {_CountryView}
                                    {_emailView}
                                </>) : _phoneView}

                            {_verCodeView}
                            {_passwordView}
                            {showPdError1 && passwordText ? _pdErrorTipsView : null}
                            {_passwordView1}
                            {showPdError2 && comfirePasswordText ? _pdErrorTipsView : null}
                            {_ruleView}
                            {_signUpBtnView}
                            {_agreeDialogView}
                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>);
    }

    setPdErrorView(passwordText, comfirePasswordText) {
        this.setState({
            showPdError1: !Helper._ifValidPassword(passwordText),
            showPdError2: !Helper._ifValidPassword(comfirePasswordText),
        })
    }

    _getBtnEnable() {
        var flag = ((this.state.emailAccount || this.state.phoneAccount)
            && Helper._ifValidPassword(this.state.passwordText)
            && Helper._ifValidPassword(this.state.comfirePasswordText)
            && this.state.verCode?.length >= 5)
        this.setState({
            submitBtnEnable: flag
        })
    }

    //提交注册
    _submitRegister() {
        const {emailAccount, phoneAccount, passwordText, verCode, comfirePasswordText, phoneCodeData} = this.state
        if (emailAccount == '' && I18n.locale === 'en') {
            this.onShowToast(strings('input_email_account'))
            return
        }
        if (phoneAccount == '' && I18n.locale === 'zh') {
            this.onShowToast(strings('input_phone_account'))
            return
        } else if (!Helper._ifValidEmail(emailAccount) && I18n.locale === 'en') {
            this.onShowToast(strings('input_valid_email_account'))
            return
        } else if (!Helper._ifValidPhone(phoneAccount) && I18n.locale === 'zh') {
            this.onShowToast(strings('input_valid_phone_account'))
            return
        } else if (!verCode) {
            this.onShowToast(strings('please_enter_verification_code'))
            return
        } else if (passwordText != comfirePasswordText) {
            this.onShowToast(strings('tiwce_pd_dif'))
            return
        } else if (!Helper._ifValidEmail(phoneCodeData?.phoneCode) && I18n.locale === 'en') {
            this.onShowToast(strings('selectNationalTips'))
            return
        }

        const account = I18n.locale === 'en' ? emailAccount : phoneAccount;
        NetUtil.userRegister(account, passwordText, verCode, phoneCodeData?.phoneCode, true)
            .then((res) => {
                this.onShowToast(strings('register_success'))
                this.props.navigation.goBack();
            }).catch((error) => {
        })
    }

    //获取验证码
    _getVerCode() {
        const {getCodeAble, emailAccount, phoneAccount} = this.state;
        const lang = I18n.locale === 'en' ? 'en-US ' : 'zh_CN'
        const account = I18n.locale === 'en' ? emailAccount : phoneAccount;
        if (!getCodeAble) {
            return
        }

        if (!Helper._ifValidEmail(emailAccount) && I18n.locale === 'en') {
            this.onShowToast(strings('input_valid_email_account'))
            return
        }
        if (!Helper._ifValidPhone(phoneAccount) && I18n.locale === 'zh') {
            this.onShowToast(strings('input_valid_phone_account'))
            return
        }

        this.setState({
            verCodeClickTime: new Date().getTime(),
            getCodeAble: false
        })


        NetUtil.getEmailVerCode(NetConstants.EMAIL_VER_CODE_REGISTER, account, true)
            .then((res) => {
                this._startTimer()
            }).catch((error) => {
            this.setState({
                verCodeText: strings('get_vail_code'),
                getCodeAble: true,
            });
        })


    }

    _startTimer() {
        this._stopTimer()
        let countdownDate = new Date(new Date().getTime() + 61 * 1000);
        this.intervalTimer = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
            if (dif <= 0) {
                this.setState({
                    verCodeText: strings('get_vail_code'),
                    getCodeAble: true,
                });
                this._stopTimer()
            } else {
                this.setState({
                    verCodeText: parseInt(dif) + 's',
                    getCodeAble: false,
                });
            }
        }, 1000)
    }

    _stopTimer() {
        if (this.intervalTimer) {
            clearInterval(this.intervalTimer)
            this.intervalTimer = null
        }
    }


    _toRule() {
        if (this.state.ruleUrl == '') {
            this._getRule(true)
        } else {
            this._toWeb(this.state.ruleUrl, strings('user_rule'))
        }
    }

    _toPrivacy() {
        if (this.state.privacyUrl == '') {
            this._getPrivacy(true)
        } else {
            this._toWeb(this.state.privacyUrl, strings('privacy_rule'))
        }
    }

    _toWeb(toUrl, title) {
        if (toUrl) {
            this.props.navigation.navigate('UrlWebView', {url: toUrl, title: title});
        }
    }

    _getRule(toWeb) {
        StorageHelper.getToken().then((token) => {
            NetUtil.getRule(token)
                .then((res) => {
                    var url = res.info.path
                    this.setState({ruleUrl: url})
                    if (toWeb)
                        this._toWeb(url, strings('user_rule'))
                })
                .catch((error) => {
                })
        })
    }

    _getPrivacy(toWeb) {
        StorageHelper.getToken().then((token) => {
            NetUtil.getPrivacy(token)
                .then((res) => {
                    var url = res.info.path
                    this.setState({privacyUrl: url})
                    if (toWeb)
                        this._toWeb(url, strings('privacy_rule'))
                })
                .catch((error) => {
                })
        })
    }
}