
/**
 * 主页
 */
import React from 'react';  
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Image,
  useColorScheme,
  View,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import { createAppContainer } from 'react-navigation';  
import { TabBar, TabBarItem, createBottomTabNavigator } from 'react-navigation-tabs';  
import Mine from '../../reactnative/Main/Mine/Mine';
import NewMine from '../../reactnative/Main/Mine/NewMine';
import MainDeviceList from '../../reactnative/Main/Device/MainDeviceList';  
const TabNavigator = createBottomTabNavigator(  
  {
    MainDeviceList: {
        screen: MainDeviceList,
        navigationOptions: ({navigation}) => ({
            title: '', 
            tabBarLabel: ({focused}) => {
                return (
                    <Text style={{
                        textAlign: 'center',
                        fontSize: 12,
                        marginBottom: 1.5,
                        backgroundColor: 'transparent',
                        height: 20,
                        position: 'absolute',
                        alignSelf: 'center',
                        color: focused ? '#18B34F' : '#808080',
                    }}>首页</Text>
                )
            },
            tabBarIcon: ({focused}) => {
                return focused ? (
                        <Image
                            source={require('../resources/greenUnion/nav_home_sel_ic.png')}
                            style={{
                                position: 'absolute',
                                top: 2
                            }}
                        />
                    ) :
                    (
                        <Image
                            source={require('../resources/greenUnion/nav_home_ic.png')}
                            style={{
                                position: 'absolute',
                                top: 2
                            }}
                        />
                    )
            }
        }),
    },
    Mine: {
      screen: NewMine,
      navigationOptions: ({navigation}) => ({
          title: '',
          tabBarLabel: ({focused}) => {
              return (
                  <Text style={{
                      textAlign: 'center',
                      fontSize: 12,
                      marginBottom: 1.5,
                      backgroundColor: 'transparent',
                      height: 20,
                      position: 'absolute',
                      alignSelf: 'center',
                      color: focused ? '#18B34F' : '#808080',
                  }}>我的</Text>
              )
          },
          tabBarIcon: ({focused}) => {
              return focused ? (
                      <Image
                          source={require('../resources/greenUnion/nav_me_sel_ic.png')}
                          style={{
                              position: 'absolute',
                              top: 2
                          }}
                      />
                  ) :
                  (
                      <Image
                          source={require('../resources/greenUnion/nav_me_ic.png')}
                          style={{
                              position: 'absolute',
                              top: 2
                          }}
                      />
                  )
          }
      }),
    },
  },   
  {
    backBehavior:'none',
    initialRouteName: 'MainDeviceList',
    swipeEnabled: false,
    lazy: true,//是否懒加载
    animationEnabled: false,
    tabBarOptions: {
        //当前选中的tab bar的文本颜色和图标颜色
        activeTintColor: '#18B34F',
        //当前未选中的tab bar的文本颜色和图标颜色
        inactiveTintColor: '#808080',
        //是否显示tab bar的图标，默认是false
        showIcon: true,
        //showLabel - 是否显示tab bar的文本，默认是true
        showLabel: true,
        //是否将文本转换为大小，默认是true
        // upperCaseLabel: false,
        //material design中的波纹颜色(仅支持Android >= 5.0)
        pressColor: '#808080',
        //按下tab bar时的不透明度(仅支持iOS和Android < 5.0).
        pressOpacity: 0.8,
        //tab bar的样式
        style: {
            backgroundColor: '#fff',
            paddingBottom: 0,
            borderTopWidth: 0,
            paddingTop:5,
            // borderTopColor: '#ccc',
            height: 60,
            borderWidth:0,
        },
        //每个tab的样式
        tabStyle: {
            borderWidth:0,
            height: 55,
  
        },
        //tab bar的文本样式
        labelStyle: {
            height:11,
            fontSize: 12,
            // margin: 1
        },
        //tab 页指示符的样式 (tab页下面的一条线).
        indicatorStyle: {height: 0},
        //是否启用滚动标签。
        // scrollEnabled:true,//false,
    }
  }
);  
  
export default createAppContainer(TabNavigator);  