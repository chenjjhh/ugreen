import BaseComponent from "../Main/Base/BaseComponent";
import React from "react";
import {
    DeviceEventEmitter,
    Image,
    ImageBackground,
    NativeModules,
    Platform,
    StatusBar,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import {strings} from "./Language/I18n";
import TextInputView from "./View/TextInputView";
import ChooseList from "./View/ChooseList";
import TabView from "./View/TabView";
import CommonBtnView from "./View/CommonBtnView";
import CommonTextView from "./View/CommonTextView";
import NetUtil from "./Net/NetUtil";
import Helper from "./Utils/Helper";
import StorageHelper from "./Utils/StorageHelper";
import EventUtil from "./Event/EventUtil";
import SplashScreen from "react-native-splash-screen";
import {AccessToken, LoginManager,} from "react-native-fbsdk-next";
import {GoogleSignin, statusCodes,} from "@react-native-google-signin/google-signin";
import NetConstants from "./Net/NetConstants";
import LogUtil from "./Utils/LogUtil";
import I18n from "react-native-i18n";
import MessageDialog from "./View/MessageDialog";
import MessageChildDialog from "./View/MessageChildDialog";
import CommonCheckView from "./View/CommonCheckView";

var GXRNManager = NativeModules.GXRNManager;

GoogleSignin.configure({
    webClientId:
        Platform.OS === "android"
            ? NetConstants.GOOGLE_ANDROID_CLIENTID
            : NetConstants.GOOGLE_IOS_CLIENTID,
    offlineAccess: true,
});

/*
 *登录页面
 */
export default class Login extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            emailAccount: "",
            phoneAccount: "",
            passwordText: "",
            bleTipsDialog: false,
            agreeDialog: false,
            domesticLoginMethod: 1, //国内登录方式 默认1:账号密码登录 0:验证码登录
            oneClickLogin: undefined, //一键登录
            switch_verification: false, //表示是否已经切换过验证码登录
            ruleCheck: false,
            verificationCode: "",
            verCodeText: strings("get_vail_code"),
            getCodeAble: true,
            verCodeClickTime: 0,
            loginType: "",
        };
        this.tabData = [
            {text: strings("verification_code_login"), id: 0},
            {text: strings("password_login"), id: 1},
        ];
    }

    hexToString(hex) {
        var hexString = hex.toString();  // 将十六进制转换为字符串
        //var decimal = parseInt(hexString, 16);  // 将字符串转换为十进制数值
        var str = String.fromCharCode(hexString);  // 将十进制数值转换为字符
        return str;
    }

    componentWillMount() {
        //倒计时
        setTimeout(() => {
            SplashScreen.hide();
        }, 2000);
        this.getLocaleAccount();
        this.thirdLoginListener = DeviceEventEmitter.addListener(
            EventUtil.THIRD_LOGIN_KEY,
            (res) => {
                LogUtil.debugLog("第三方登录回调：\n" + JSON.stringify(res));
            }
        );
        NetUtil._setCanToLoginFlag(false);

        this.nativeToRnListener = DeviceEventEmitter.addListener(
            EventUtil.NATIVE_TO_RN,
            (res) => {
                alert('fff')
                this.props.navigation.navigate("Register");
            }
        );
    }

    componentWillUnmount() {
        this.thirdLoginListener && this.thirdLoginListener.remove();
        this.nativeToRnListener && this.nativeToRnListener.remove();
    }

    // 本地登录信息
    getLocaleAccount = () => {
        StorageHelper.getLoginAccount().then((account) => {
            const accountKey = I18n.locale === "en" ? "emailAccount" : "phoneAccount";
            StorageHelper.getLoginPassword().then((passwordText) => {
                if (passwordText && account) {
                    this.setState(() => {
                        if (I18n.locale === "zh") {
                            return {[accountKey]: account, switch_verification: true};
                        } else {
                            return {[accountKey]: account, switch_verification: false};
                        }
                    });
                }
            });
        });
    };

    render() {
        StatusBar.setBarStyle("light-content");
        const {backColor, fontColor} = this.props;
        const {
            domesticLoginMethod,
            switch_verification,
            ruleCheck,
            emailAccount,
            phoneAccount,
            passwordText,
            verificationCode,
            verCodeText,
            getCodeAble,
        } = this.state;
        const _tabView = (
            <TabView
                data={this.tabData}
                currentId={domesticLoginMethod}
                _checkTab={this._checkTab}
                style={{with: "73%"}}
            />
        );
        const _CountryView = (
            <ChooseList
                style={{marginBottom: 20}}
                onPress={() => {
                }}
                text={"英国"}
                leftIcon={require("../resources/greenUnion/log_countory_ic.png")}
                rightIcon={require("../resources/greenUnion/list_arrow_ic.png")}
            />
        );

        const _userNameEmailView = (
            <TextInputView
                key={0}
                maxLength={64}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({emailAccount: newText});
                }}
                value={emailAccount}
                placeholder={strings("input_email_account")}
                keyboardType={"email-address"} //邮箱
                placeholderTextColor={"#C4C6CD"}
                isEmail={true}
            />
        );
        const _userNamePhoneView = (
            <TextInputView
                key={1}
                maxLength={64}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({phoneAccount: newText});
                }}
                value={phoneAccount}
                placeholder={strings("input_phone_account")}
                keyboardType={"phone-pad"} //手机
                placeholderTextColor={"#C4C6CD"}
                isPhone={true}
            />
        );

        const _verifyCodeLogin = (
            <TextInputView
                key={2}
                style={{
                    marginTop: 18,
                }}
                verifyCodeLogin={true}
                maxLength={6}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({verificationCode: newText});
                }}
                value={verificationCode}
                placeholder={strings("please_enter_verification_code")}
                keyboardType={"numeric"}
                placeholderTextColor={"#C4C6CD"}
                onPress={() => {
                    this._getVerCode();
                }}
                getCodeAble={getCodeAble}
                verCodeText={verCodeText}
                {...this.props}
            />
        );
        const _passwordView = (
            <TextInputView
                key={3}
                style={{
                    marginTop: 18,
                }}
                isPassword={true}
                doubt={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({passwordText: newText});
                }}
                value={passwordText}
                placeholder={strings("input_password")}
                keyboardType={"default"}
                placeholderTextColor={"#C4C6CD"}
                {...this.props}
            />
        );
        const _oneClickLoginView = (
            <View style={{alignItems: "center", marginBottom: 100}}>
                <CommonTextView
                    textSize={16}
                    text={Helper._encryptedPhoneNumber(phoneAccount)}
                    style={{
                        color: "#404040",
                    }}
                />
                <CommonTextView
                    textSize={12}
                    text={strings("log_in")}
                    style={{
                        color: "#808080",
                    }}
                />
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 一键登录
                        this._oneClickLogin();
                    }}
                    style={{
                        height: 54,
                        backgroundColor: "#18B34F",
                        marginTop: 40,
                    }}
                    btnText={strings("one_click_login")}
                />
                <TouchableOpacity
                    style={{
                        marginTop: 28,
                    }}
                    onPress={() => {
                        //切换验证码登录
                        this._checkTab({id: 0});
                    }}
                >
                    <CommonTextView
                        textSize={16}
                        text={strings("switch_verification_code_login")}
                        style={{
                            color: "#808080",
                        }}
                    />
                </TouchableOpacity>
            </View>
        );

        const _signInBtnView = (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //国外或者国内账户密码登录
                    if (domesticLoginMethod || I18n.locale === "en") {
                        this._login();
                    } else {
                        this._codeLogin();
                    }
                    //NativeModules.IntentModule.startActivityFromJS("com.guoxuansource.RnTestActivity", "我是从JS传过来的参数信息.")
                }}
                style={{
                    marginTop: 29,
                    height: 54,
                    backgroundColor: "#18B34F",
                }}
                btnText={strings("sign_in")}
            />
        );
        const _registerView = (
            <TouchableOpacity
                style={{
                    marginTop: 28,
                }}
                onPress={() => {
                    //注册
                    this.props.navigation.navigate("Register");
                }}
            >
                <CommonTextView
                    textSize={16}
                    text={strings("register")}
                    style={{
                        color: "#808080",
                    }}
                />
            </TouchableOpacity>
        );
        const _btnItemView = (type, icon, clickEvent) => {
            return (
                <TouchableOpacity
                    style={{justifyContent: "center", alignItems: "center"}}
                    onPress={clickEvent}
                >
                    <Image source={icon}/>
                    <CommonTextView
                        textSize={12}
                        text={strings(type)}
                        style={{
                            color: "#808080",
                            marginTop: 5,
                        }}
                    />
                </TouchableOpacity>
            );
        };
        const _bottomShareView = (
            <View
                style={{
                    flexDirection: "row",
                    width: this.viewCommonWith,
                    marginBottom: 20,
                    paddingLeft: 20,
                    paddingRight: 20,
                    justifyContent: "space-around",
                    backgroundColor: this.props.backColor,
                }}
            >
                {I18n.locale === "zh" ? (
                    <>
                        {_btnItemView(
                            "weChat",
                            require("../resources/greenUnion/log_qq_btn.png"),
                            () => {
                                // 微信登录
                                // if(!ruleCheck){
                                //     this.onShowToast(strings('请先阅读并同意用户协议'))
                                //     return
                                // }
                            }
                        )}
                        {_btnItemView(
                            "qq",
                            require("../resources/greenUnion/log_qq_btn.png"),
                            () => {
                                // QQ登录
                                // if(!ruleCheck){
                                //     this.onShowToast(strings('请先阅读并同意用户协议'))
                                //     return
                                // }
                            }
                        )}
                    </>
                ) : (
                    <>
                        {_btnItemView(
                            "Google",
                            require("../resources/greenUnion/log_google_btn.png"),
                            () => {
                                //谷歌
                                // if(!ruleCheck){
                                //     this.onShowToast(strings('请先阅读并同意用户协议'))
                                //     return
                                // }
                                //NativeModules.RNModule.googleLogin();
                                //Linking.openURL('https://granwin.auth.us-west-2.amazoncognito.com/login?response_type=code&client_id=2ca19dq761p28jofchgmr5ltim&redirect_uri=myapp://callback/').catch(err => console.error('An error occurred', err))
                                this._googleLogin();
                            }
                        )}
                        {_btnItemView(
                            "FaceBook",
                            require("../resources/greenUnion/log_facebook_btn.png"),
                            () => {
                                //facebook
                                // if(!ruleCheck){
                                //     this.onShowToast(strings('请先阅读并同意用户协议'))
                                //     return
                                // }
                                //this.props.navigation.navigate('BindEmail');
                                //NativeModules.RNModule.faceBookLogin();
                                this._facebookLogin();
                            }
                        )}
                        {Helper.getPlatformOsFlag() == 0
                            ? _btnItemView(
                                "Apple",
                                require("../resources/greenUnion/log_apple_btn.png"),
                                () => {
                                    //苹果
                                    // if(!ruleCheck){
                                    //     this.onShowToast(strings('请先阅读并同意用户协议'))
                                    //     return
                                    // }
                                    this._appleLogin();
                                }
                            )
                            : null}
                    </>
                )}
                {_btnItemView(
                    "visitor",
                    require("../resources/greenUnion/log_vistor_btn.png"),
                    () => {
                        if (!ruleCheck) {
                            this.setState({agreeDialog: true});
                            return;
                        }
                        // 访客登录
                        this.setState({
                            bleTipsDialog: true,
                        });
                    }
                )}
            </View>
        );
        const _protocolView = (
            <View
                style={{
                    flexDirection: "row",
                    alignItems: "flex-start",
                    marginBottom: 16,
                    width: this.mScreenWidth * 0.73,
                }}
            >
                <CommonCheckView
                    style={{
                        marginRight: 7,
                        marginTop: 3,
                    }}
                    onCheck={(isCheck) => {
                        this.setState({ruleCheck: isCheck});
                    }}
                    isCheck={ruleCheck}
                />
                <View style={{flex: 1}}>
                    <Text>
                        <CommonTextView
                            textSize={12}
                            text={strings("agree")}
                            style={{
                                color: "#808080",
                                marginTop: 5,
                            }}
                        />
                        <CommonTextView
                            onPress={() => {
                            }}
                            textSize={12}
                            text={strings("user_agreement")}
                            style={{
                                color: "#18B34F",
                                marginTop: 5,
                            }}
                        />
                        <CommonTextView
                            textSize={12}
                            text={strings("and")}
                            style={{
                                color: "#808080",
                                marginTop: 5,
                            }}
                        />
                        <CommonTextView
                            onPress={() => {
                            }}
                            textSize={12}
                            text={strings("personal_information")}
                            style={{
                                color: "#18B34F",
                                marginTop: 5,
                            }}
                        />
                        <CommonTextView
                            textSize={12}
                            text={strings("and")}
                            style={{
                                color: "#808080",
                                marginTop: 5,
                            }}
                        />
                        <CommonTextView
                            onPress={() => {
                            }}
                            textSize={12}
                            text={strings("third_party_information")}
                            style={{
                                color: "#18B34F",
                                marginTop: 5,
                            }}
                        />
                    </Text>
                </View>
            </View>
        );
        const _tipsDialogView = (
            <View>
                <MessageDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({bleTipsDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({bleTipsDialog: false});
                    }}
                    modalVisible={this.state.bleTipsDialog}
                    title={strings("kind_tips")}
                    message={strings("kind_tips_content")}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({bleTipsDialog: false});
                    }}
                    rightBtnText={strings("confirm")}
                    onRightBtnClick={() => {
                        //确认
                        this.setState({bleTipsDialog: false}, () => {
                            StorageHelper.saveLoginType(3);
                            this.props.navigation.replace("Home");
                        });
                    }}
                    messageStyle={{
                        textAlign: "center",
                    }}
                />
            </View>
        );
        const _agreeDialogView = (
            <View>
                <MessageChildDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({agreeDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({agreeDialog: false});
                    }}
                    modalVisible={this.state.agreeDialog}
                    title={strings("同意使用")}
                    message={''}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({agreeDialog: false});
                    }}
                    rightBtnText={strings("同意")}
                    onRightBtnClick={() => {
                        //确认
                        this.setState({ruleCheck: true});
                        this.setState({agreeDialog: false}, () => {
                        });
                    }}
                    messageStyle={{
                        textAlign: "center",
                    }}
                >
                    <View
                        style={{
                            width: this.mScreenWidth - 80,
                            marginTop: 35,
                            marginBottom: 35,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <View style={{flex: 1}}>
                            <Text>
                                <CommonTextView
                                    textSize={12}
                                    text={strings("agree")}
                                    style={{
                                        color: "#808080",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    onPress={() => {
                                    }}
                                    textSize={12}
                                    text={strings("user_agreement")}
                                    style={{
                                        color: "#18B34F",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    textSize={12}
                                    text={strings("and")}
                                    style={{
                                        color: "#808080",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    onPress={() => {
                                    }}
                                    textSize={12}
                                    text={strings("personal_information")}
                                    style={{
                                        color: "#18B34F",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    textSize={12}
                                    text={strings("and")}
                                    style={{
                                        color: "#808080",
                                        marginTop: 5,
                                    }}
                                />
                                <CommonTextView
                                    onPress={() => {
                                    }}
                                    textSize={12}
                                    text={strings("third_party_information")}
                                    style={{
                                        color: "#18B34F",
                                        marginTop: 5,
                                    }}
                                />
                            </Text>
                        </View>
                    </View>
                </MessageChildDialog>
            </View>
        );
        return (
            <View style={{flex: 1}}>
                <ImageBackground
                    source={require("../resources/greenUnion/loginBack.png")}
                    style={{
                        flex: 1,
                        justifyContent: "space-between",
                        alignItems: "center",
                    }}
                >
                    <View style={{alignItems: "center"}}>
                        <StatusBar translucent={true} backgroundColor="transparent"/>
                        <_titleView
                            style={{marginTop: 70, marginBottom: 75}}
                            text={strings("login_account")}
                            {...this.props}
                        />
                        {switch_verification ? (
                            _oneClickLoginView
                        ) : (
                            <>
                                {_tabView}
                                {I18n.locale === "en" ? _CountryView : null}
                                {I18n.locale === "en" ? _userNameEmailView : _userNamePhoneView}
                                {I18n.locale === "en" ||
                                (I18n.locale === "zh" && domesticLoginMethod)
                                    ? _passwordView
                                    : _verifyCodeLogin}
                                {_signInBtnView}
                                {_registerView}
                            </>
                        )}
                    </View>

                    <View style={{width: this.mScreenWidth, alignItems: "center"}}>
                        {_bottomShareView}
                        {_protocolView}
                    </View>

                </ImageBackground>
                {_tipsDialogView}
                {_agreeDialogView}
            </View>
        );
    }

    //获取验证码
    _getVerCode() {
        const {getCodeAble, emailAccount, phoneAccount} = this.state;
        const lang = I18n.locale === "en" ? "en_US " : "zh_CN";
        const account = I18n.locale === "en" ? emailAccount : phoneAccount;
        if (!getCodeAble) {
            return;
        }

        if (!Helper._ifValidEmail(emailAccount) && I18n.locale === "en") {
            this.onShowToast(strings("input_valid_email_account"));
            return;
        }
        if (!Helper._ifValidPhone(phoneAccount) && I18n.locale === "zh") {
            this.onShowToast(strings("input_valid_phone_account"));
            return;
        }

        this.setState({
            verCodeClickTime: new Date().getTime(),
            getCodeAble: false,
        });

        NetUtil.getEmailVerCode(NetConstants.EMAIL_VER_CODE_LOGIN, account, true)
            .then((res) => {
                this._startTimer();
            })
            .catch((error) => {
                this.setState({
                    verCodeText: strings("get_vail_code"),
                    getCodeAble: true,
                });
            });
    }

    _startTimer() {
        this._stopTimer();
        let countdownDate = new Date(new Date().getTime() + 61 * 1000);
        this.intervalTimer = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000;
            if (dif <= 0) {
                this.setState({
                    verCodeText: strings("get_vail_code"),
                    getCodeAble: true,
                });
                this._stopTimer();
            } else {
                this.setState({
                    verCodeText: parseInt(dif) + "s",
                    getCodeAble: false,
                });
            }
        }, 1000);
    }

    _stopTimer() {
        if (this.intervalTimer) {
            clearInterval(this.intervalTimer);
            this.intervalTimer = null;
        }
    }

    // 验证码登录
    _codeLogin = () => {
        const {ruleCheck, phoneAccount, verificationCode} = this.state;
        if (phoneAccount == "") {
            this.onShowToast(strings("input_phone_account"));
            return;
        } else if (verificationCode == "") {
            this.onShowToast(strings("please_enter_verification_code"));
            return;
        } else if (!Helper._ifValidPhone(phoneAccount)) {
            this.onShowToast(strings("input_valid_phone_account"));
            return;
        }
        if (!ruleCheck) {
            this.setState({agreeDialog: true});
            return;
        }

        NetUtil.userCodeLogin(phoneAccount, verificationCode, true)
            .then((res) => {
                if (res) {
                    var token = res?.info?.token;
                    StorageHelper.saveToken(token);
                    StorageHelper.saveLoginInfo(res);
                    StorageHelper.saveLoginAccount(phoneAccount);
                    StorageHelper.saveLoginType(2);
                    this.props.navigation.replace("Home");
                    LogUtil.debugLog("test==============token=========>" + token);
                    LogUtil.debugLog(
                        "test==============res.info=========>" + JSON.stringify(res.info)
                    );
                }
            })
            .catch((error) => {
            });
    };

    //密码登录
    _login() {
        const {emailAccount, phoneAccount, passwordText, ruleCheck} = this.state;
        //        GXRNManager.start();

        // GXRNManager.getWifiSSid().then((datas)=> {
        //     console.warn('data', datas);
        //     this.onShowToast(datas);
        // }).catch((err)=> {
        //     console.warn('err', err);
        //     this.onShowToast(err);
        // });

        /// 蓝牙配网
        //        GXRNManager.bleSetDeviceNetwork('ChinaNet-mvt4', 'dueva5ce', 'Granwin_BLE', 'https://cloud.granwin.com/gateway/granwin_device/aliyun/iot/device/register').then((datas)=> {
        //             console.warn('data', datas);
        //            this.onShowToast(datas.strings);
        //            this.onShowToast('配网成功');
        //         }).catch((err)=> {
        //             console.warn('err', err);
        //             this.onShowToast(err);
        //         });

        /// 连接到热点
        //         GXRNManager.connectDeviceHot('Granwin_AP_EF1C', '12345678').then((datas)=> {
        //             /// WiFi配网
        //             GXRNManager.wifiSetDeviceNetwork('ChinaNet-mvt4', 'dueva5ce', 'https://cloud.granwin.com/gateway/granwin_device/aliyun/iot/device/register', 60).then((datas)=> {
        //                  console.warn('data', datas);
        //              }).catch((err)=> {
        //                  console.warn('err', err);
        //              });
        //         }).catch((err)=> {
        //             console.warn('err', err);
        //         });

        //        /// WiFi配网
        //        GXRNManager.wifiSetDeviceNetwork('Redmi', '2ab5b44009d8', 'https://cloud.granwin.com/gateway/granwin_device/aliyun/iot/device/register', 60).then((datas)=> {
        //             console.warn('data', datas);
        //         }).catch((err)=> {
        //             console.warn('err', err);
        //         });

        //        /// 扫描设备
        //        GXRNManager.scanDevices('bleName').then((datas)=> {
        //            console.warn('data', datas);
        //        }).catch((err)=> {
        //            console.warn('err', err);
        //        });
        //
        //        /// 连接设备
        //        GXRNManager.connectDevice('mac').then((datas)=> {
        //            console.warn('data', datas);
        //        }).catch((err)=> {
        //            console.warn('err', err);
        //        });
        //
        //        /// 发送数据
        //        GXRNManager.sendData('data');
        //
        //        // 停止扫描
        //        GXRNManager.stopScanDevice();
        //
        //        /// 断开
        //        GXRNManager.disConnectDevice();

        if (emailAccount == "" && I18n.locale === "en") {
            this.onShowToast(strings("input_email_account"));
            return;
        }
        if (phoneAccount == "" && I18n.locale === "zh") {
            this.onShowToast(strings("input_phone_account"));
            return;
        } else if (passwordText == "") {
            this.onShowToast(strings("input_password"));
            return;
        } else if (!Helper._ifValidEmail(emailAccount) && I18n.locale === "en") {
            this.onShowToast(strings("input_valid_email_account"));
            return;
        } else if (!Helper._ifValidPhone(phoneAccount) && I18n.locale === "zh") {
            this.onShowToast(strings("input_valid_phone_account"));
            return;
        } else if (!Helper._ifValidPassword(passwordText)) {
            this.onShowToast(strings("input_valid_password"));
            return;
        }
        if (!ruleCheck) {
            this.setState({agreeDialog: true});
            return;
        }

        NetUtil.userLogin(
            I18n.locale === "en" ? emailAccount : phoneAccount,
            passwordText,
            true
        )
            .then((res) => {
                if (res) {
                    var token = res?.info?.token;
                    StorageHelper.saveToken(token);
                    StorageHelper.saveLoginInfo(res);
                    StorageHelper.saveLoginAccount(
                        I18n.locale === "en" ? emailAccount : phoneAccount
                    );
                    StorageHelper.saveLoginPassword(passwordText);
                    StorageHelper.saveLoginType(1);
                    this.props.navigation.replace("Home");
                    LogUtil.debugLog("test==============token=========>" + token);
                    LogUtil.debugLog(
                        "test==============res.info=========>" + JSON.stringify(res.info)
                    );
                }
            })
            .catch((error) => {
            });
    }

    // 一键登录
    _oneClickLogin = () => {
        const {ruleCheck} = this.state;
        if (!ruleCheck) {
            this.setState({agreeDialog: true});
            return;
        }
        StorageHelper.getLoginAccount().then((account) => {
            if (!account) return;
            StorageHelper.getLoginPassword()
                .then((passwordText) => {
                    LogUtil.debugLog("获取一键登录信息:" + passwordText);
                    if (passwordText) {
                        NetUtil.userLogin(account, passwordText, true)
                            .then((res) => {
                                if (res) {
                                    var token = res?.info?.token;
                                    StorageHelper.saveToken(token);
                                    StorageHelper.saveLoginInfo(res);
                                    StorageHelper.saveLoginAccount(account);
                                    StorageHelper.saveLoginPassword(passwordText);
                                    StorageHelper.saveLoginType(4);
                                    this.props.navigation.replace("Home");
                                    LogUtil.debugLog("test==============token=========>" + token);
                                    LogUtil.debugLog(
                                        "test==============res.info=========>" +
                                        JSON.stringify(res.info)
                                    );
                                }
                            })
                            .catch((error) => {
                            });
                    }
                })
                .catch((error) => {
                    LogUtil.debugLog("一键登录异常：" + JSON.stringify(error));
                });
        });
    };

    _checkTab = (data) => {
        this.setState({domesticLoginMethod: data.id, switch_verification: false});
    };

    _facebookLogin() {
        LoginManager.logInWithPermissions(["public_profile"]).then(
            (result) => {
                if (result.isCancelled) {
                    LogUtil.debugLog("Login cancelled");
                } else {
                    LogUtil.debugLog(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then((data) => {
                        LogUtil.debugLog(
                            "facebook_AccessToken:" + data.accessToken.toString()
                        );
                        if (data.accessToken) {
                            NetUtil.facebookAuth(data.accessToken, true).then((res) => {
                                this._thirdAuth(res.info, Helper.type_login_facebook);
                            });
                        }
                    });
                }
            },
            (error) => {
                LogUtil.debugLog("Login fail with error: " + error);
            }
        );
    }

    // Somewhere in your code
    _googleLogin = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            LogUtil.debugLog("google_login_userInfo:" + JSON.stringify(userInfo));
            const idToken = await GoogleSignin.getTokens();
            LogUtil.debugLog("google_login_idToken:" + JSON.stringify(idToken));
            if (userInfo.idToken) {
                NetUtil.googleAuth(userInfo.idToken, true).then((res) => {
                    this._thirdAuth(res.info, Helper.type_login_google);
                });
                if (userInfo.user.photo) {
                    StorageHelper.setTempThirdAvatar(userInfo.user.photo);
                }
            } else {
                this.onShowToast("idToken is null");
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                LogUtil.debugLog(
                    "google_login_fail:user cancelled ====>" + JSON.stringify(error)
                );
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
                LogUtil.debugLog(
                    "google_login_fail:is in progress already ====>" +
                    JSON.stringify(error)
                );
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                LogUtil.debugLog(
                    "google_login_fail:play services not available or outdated ====>" +
                    JSON.stringify(error)
                );
            } else {
                // some other error happened
                LogUtil.debugLog(
                    "google_login_fail:some other error happened ====>" +
                    JSON.stringify(error)
                );
            }
        }
    };

    _appleLogin() {
        GXRNManager.signInWithAppleId()
            .then((datas) => {
                LogUtil.debugLog("apple_login:", JSON.stringify(datas));
                if (datas.identityToken) {
                    NetUtil.appleAuth(datas.identityToken, true).then((res) => {
                        this._thirdAuth(res.info, Helper.type_login_apple);
                    });
                }
            })
            .catch((err) => {
                LogUtil.debugLog("apple_login_fail:", JSON.stringify(err));
            });
    }

    _thirdAuth(info, loginType) {
        if (info) {
            // register
            // 1=新用户，需要绑定
            // 2=已绑定邮箱，未设置密码
            // 0=登录成功
            if (parseInt(info.register) == 0) {
                //直接登录
                var token = info.data.granwin_token;
                StorageHelper.saveToken(token);
                StorageHelper.saveLoginInfo(info);
                if (info.data.picture) {
                    this._saveLocalThirdAvatar(info.data.picture);
                } else {
                    var tempThirdAvatar = StorageHelper.getTempThirdAvatar();
                    this._saveLocalThirdAvatar(tempThirdAvatar);
                }

                this.props.navigation.replace("Home");
            } else {
                //需要绑定邮箱或者设置密码
                StorageHelper.setTempThirdAvatar(info.data.picture);
                this._toBindEmail(info.data, loginType, info);
            }
        }
    }

    _toBindEmail(data, loginType, info) {
        this.props.navigation.navigate("BindEmail", {
            data: data,
            loginType: loginType,
            info: info,
        });
    }

    _saveLocalThirdAvatar(avatar) {
        if (avatar) StorageHelper.saveThirdAvatar(avatar);
    }
}

export class _titleView extends BaseComponent {
    render() {
        return (
            <View
                style={[
                    {
                        width: this.mScreenWidth,
                        alignItems: "center",
                    },
                    this.props.style,
                ]}
            >
                <CommonTextView
                    textSize={24}
                    text={this.props.text}
                    style={{
                        color: "#2F2F2F",
                        fontWeight: "bold",
                        marginBottom: 12,
                    }}
                />
                <CommonTextView
                    textSize={18}
                    text={strings("welcome_message")}
                    style={{
                        color: "#808080",
                        fontWeight: "normal",
                    }}
                />
            </View>
        );
    }
}
