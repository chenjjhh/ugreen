// 上传附件
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import StorageHelper from "../Utils/StorageHelper";
var GXRNManager = NativeModules.GXRNManager;

var options = {
  quality: 0.3,
  mediaType: 'photo'
}
export default class UpLoadView extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      imagePickerDialogArray: [strings("take_photo"), strings("photo")],
      imagePickerDialog: false,
      imageData: [],
      token: StorageHelper.getTempToken() || "",
    };
  }

  componentWillMount() {}

  UpLoad = () => {
    const { title, style, onPress } = this.props;
    // onPress && onPress();
  };

  //打开相机
  _launchCamera() {
    launchCamera(options, (response) => {
      LogUtil.debugLog("Response = ", JSON.stringify(response));
      if (response.didCancel) {
        LogUtil.debugLog("User cancelled image picker");
      } else if (response.errorMessage) {
        LogUtil.debugLog("ImagePicker Error: ", response.errorMessage);
        Helper.openSettings(strings("camera_permission_tips"));
      } else if (response.errorCode) {
        LogUtil.debugLog("errorCode: ", response.errorCode);
        Helper.openSettings(strings("camera_permission_tips"));
      } else {
        const { uri, fileName } = response?.assets[0] || {};
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        let file = {
          uri: uri,
          type: "multipart/form-data",
          name: fileName,
        }; //这里的key(uri和type和name)不能改变,
        this._uploadImage(file);
      }
    });
  }

  //打开相册
  _launchImageLibrary() {
    launchImageLibrary(options, (response) => {
      LogUtil.debugLog("Response = ", JSON.stringify(response));

      if (response.didCancel) {
        LogUtil.debugLog("User cancelled image picker");
      } else if (response.errorMessage) {
        LogUtil.debugLog("ImagePicker Error: ", response.errorMessage);
        Helper.openSettings(strings("camera_permission_tips"));
      } else if (response.errorCode) {
        LogUtil.debugLog("errorCode: ", response.errorCode);
        Helper.openSettings(strings("camera_permission_tips"));
      } else {
        const { uri, fileName } = response?.assets[0] || {};
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        let file = {
          uri: uri,
          type: "multipart/form-data",
          name: fileName,
        }; //这里的key(uri和type和name)不能改变,
        this._uploadImage(file);
      }
    });
  }

  //上传图片
  _uploadImage(multipartFile) {
    const { token } = this.state;
    NetUtil.uploadFile(token, multipartFile, true)
      .then((res) => {
        console.log('res:???', res);
        //图片上传成功
        const { fileUrl } = res?.info;
        if (fileUrl) {
          this._putImageToArray(multipartFile, fileUrl);
        }
      })
      .catch((error) => {
        console.log('error??? ', error);

      });
  }

  // 拼接已上传成功图片数据
  _putImageToArray(multipartFile, fileUrl) {
    // imageUrl本地地址 fileUrl网络地址
    const { imageData } = this.state;
    const { getImageData } = this.props;
    let currentImage = {
      imageUrl:multipartFile?.uri,
      fileUrl,
      fileName:multipartFile?.name,
      fileType:'picture',
    };
    this.setState(
      {
        imageData: [...imageData, currentImage],
      },
      () => {
        console.log('imageData???',this.state.imageData);
        getImageData?.([...imageData, currentImage], currentImage);
      }
    );
  }

  render() {
    const { title, style } = this.props;
    const { imagePickerDialog, imagePickerDialogArray, imageData } = this.state;

    {
      /* 选择图片弹窗 */
    }
    const _dialogView = (
      <SelectDialog
        onCancelClick={() => {
          this.setState({ imagePickerDialog: false });
        }}
        title={""}
        onRequestClose={() => {
          this.setState({ imagePickerDialog: false });
        }}
        overViewClick={() => {
          this.setState({ imagePickerDialog: false });
        }}
        modalVisible={imagePickerDialog}
        selectArray={imagePickerDialogArray}
        onItemClick={(index) => {
          this.setState({ imagePickerDialog: false }, () => {
            if (index == 0) {
              //拍照
              if (Platform.OS == "android") {
                this._launchCamera();
              } else {
                GXRNManager.checkPhotoPermissions(1)
                  .then((datas) => {
                    this._launchCamera();
                  })
                  .catch((err) => {
                    LogUtil.debugLog("err", err);
                  });
              }
            } else {
              //相册
              if (Platform.OS == "android") {
                this._launchImageLibrary();
              } else {
                GXRNManager.checkPhotoPermissions(2)
                  .then((datas) => {
                    this._launchImageLibrary();
                  })
                  .catch((err) => {
                    LogUtil.debugLog("err", err);
                  });
              }
            }
          });
        }}
      />
    );
    return (
      <View
        style={[
          {
            width: this.mScreenWidth - 40,
            borderRadius: 10,
            overflow: "hidden",
            backgroundColor: "#fff",
            paddingHorizontal: 20,
            paddingBottom: 20,
          },
          style,
        ]}
      >
        <Text
          style={{
            lineHeight: 50,
            color: "#404040",
            fontWeight: "normal",
            fontSize: 16,
          }}
        >
          {title}
        </Text>
        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
          {imageData?.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                this.setState({
                  imagePickerDialog: true,
                });
              }}
              style={{
                flexDirection: "row",
                backgroundColor: "#F7F8FB",
                justifyContent: "center",
                alignItems: "center",
                width: (this.mScreenWidth - 122) / 5,
                height: (this.mScreenWidth - 122) / 5,
                borderRadius: 10,
                marginBottom: 10,
                marginRight: 10,
              }}
            >
              <Image
                source={
                  item?.imageUrl
                    ? { uri: item.imageUrl }
                    : item?.fileUrl
                    ? { uri: item.fileUrl }
                    : 0
                }
              />
            </TouchableOpacity>
          ))}
          <TouchableOpacity
            onPress={() => {
              this.setState({
                imagePickerDialog: true,
              });
            }}
            style={{
              flexDirection: "row",
              backgroundColor: "#F7F8FB",
              justifyContent: "center",
              alignItems: "center",
              width: (this.mScreenWidth - 122) / 5,
              height: (this.mScreenWidth - 122) / 5,
              borderRadius: 10,
              marginBottom: 10,
              marginRight: 10,
            }}
          >
            <Image
              source={require("../../resources/greenUnion/upload_ic.png")}
            />
          </TouchableOpacity>
        </View>
        {_dialogView}
      </View>
    );
  }
}
