import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "./CommonTextView";
import DialogBottomBtnView from "./DialogBottomBtnView";
import Slider from "./Slider";
import CommonBtnView from "./CommonBtnView";
import SwitchView from "./SwitchView";
import Helper from "../Utils/Helper";

/*
*设备详情灯光控制弹窗
 */
var tempBrightness = 1
var tempLightBrightness = 1
export default class DetailLightControlDialog extends BaseComponent {
    render() {
        return (
            <Modal
                statusBarTranslucent={true}
                animationType={"fade"}
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}>

                <TouchableOpacity
                    activeOpacity={this.props.overViewClick ? 0.5 : 1}
                    onPress={() => {
                        if (this.props.overViewClick) {
                            this.props.overViewClick()
                        }
                    }}
                    style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.22)',
                    }}>
                    {ViewHelper.getFlexView()}

                    {this._bottomView()}
                </TouchableOpacity>

            </Modal>
        );
    }

    _bottomView() {
        return (
            <TouchableOpacity
                activeOpacity={1}
                style={{
                    width: this.mScreenWidth,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    backgroundColor: '#fff',
                    paddingTop: 32,
                    paddingLeft: 25,
                    paddingRight: 25,
                    alignItems: 'center'
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 54,
                        alignItems: 'center',
                        flexDirection: 'row',
                        marginBottom: 60
                    }}>
                    <View
                        style={{
                            width: 15,
                            height: 15
                        }}/>
                    {ViewHelper.getFlexView()}
                    <CommonTextView
                        textSize={20}
                        style={{
                            color: '#000000',
                        }}
                        text={strings('change_light')}/>
                    {ViewHelper.getFlexView()}
                    <TouchableOpacity
                        onPress={this.props.closeClickEvent}>
                        <Image
                            style={{
                                width: 15,
                                height: 15,
                                resizeMode: 'contain'
                            }}
                            source={require('../../resources/ic_close.png')}/>
                    </TouchableOpacity>
                </View>

                {this._brightnessView(strings('device_screen_light'), this.props.brightness, (value) => {
                    tempBrightness = value;
                    if (this.props.onValueChange) {
                        this.props.onValueChange(value)
                    }
                }, () => {
                    if (this.props.onSlidingComplete) {
                        this.props.onSlidingComplete(tempBrightness)
                    }
                }, true)}
                <View
                    style={{
                        width: this.mScreenWidth - 50,
                        height: 2 / PixelRatio.get(),
                        backgroundColor: '#ddd',
                        marginTop: 26
                    }}/>
                {this._moodLightView()}
                <View
                    style={{
                        height: 30,
                        width: 1
                    }}/>
                {this._brightnessView(strings('birghtness'), this.props.lightBrightness, (value) => {
                    tempLightBrightness = value;
                    if (this.props.onLightBrightnessValueChange) {
                        this.props.onLightBrightnessValueChange(value)
                    }
                }, () => {
                    if (this.props.onLightBrightnessSlidingComplete) {
                        this.props.onLightBrightnessSlidingComplete(tempLightBrightness)
                    }
                }, this.props.lightSwitchValue)}
                {this._btoomSwitchView()}
                {/*{this._submitBtnView()}*/}
            </TouchableOpacity>
        )
    }

    _brightnessView(text, value, onValueChange, onSlidingComplete, enable) {
        return (
            <View>
                {this._brightnessTitleView(text, value)}
                {this._brightnessSliderView(value, onValueChange, onSlidingComplete)}

                {
                    !enable ?
                        <TouchableOpacity
                            activeOpacity={1}
                            style={{
                                width: this.mScreenWidth,
                                height: '100%',
                                backgroundColor: 'rgba(0,0,0,0)',
                                position: 'absolute',
                                marginLeft: 25,
                                top: 15,
                                left: -25
                            }}/> : null
                }

            </View>
        )
    }

    _brightnessTitleView(text, value) {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: 25,
                    paddingRight: 25
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000000',
                    }}
                    text={text}/>

                {ViewHelper.getFlexView()}

                <CommonTextView
                    textSize={18}
                    style={{
                        color: '#FF8748',
                    }}
                    text={Helper.dataNanReset(value) + '%'}/>

            </View>
        )
    }

    _brightnessSliderView(value, onValueChange, onSlidingComplete) {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 20
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 50,
                        height: 10,
                        position: 'absolute',
                        backgroundColor: '#F7F7F7',
                        borderRadius: 5,
                    }}>
                    <View
                        style={{
                            width: 5 + ((this.mScreenWidth - 56 - 5) * (value) / 99),
                            height: 10,
                            borderRadius: 5,
                            backgroundColor: '#FF8748',
                        }}/>
                </View>

                <Slider
                    value={value}
                    style={{
                        width: this.mScreenWidth - 50,
                        height: 10
                    }}
                    trackStyle={{
                        height: 10,
                        borderRadius: 5,
                        backgroundColor: 'rgba(0,0,0,0)'
                    }}
                    //thumbImage={require('../../resources/voice_switcher_btn.png')}
                    thumbStyle={{
                        width: 20,
                        height: 20
                    }}
                    step={1}
                    minimumValue={1}
                    maximumValue={100}
                    minimumTrackTintColor="rgba(0,0,0,0)"
                    maximumTrackTintColor="rgba(0,0,0,0)"
                    thumbTintColor="#efefef"
                    onValueChange={onValueChange}
                    onSlidingComplete={onSlidingComplete}
                />
            </View>
        )
    }

    //情景灯氛围
    _moodLightView() {
        return (
            <View
                style={{
                    marginTop: 27,
                    paddingLeft: 25,
                    paddingRight: 25
                }}>
                {this._moodLightTitleView()}
                {this._moodLightTopBtnView()}
                {/*{this._moodLightBottomBtnView()}*/}
            </View>
        )
    }

    //情景灯氛围标题
    _moodLightTitleView() {
        return (
            <CommonTextView
                textSize={14}
                style={{
                    color: '#000000',
                }}
                text={strings('mood_light')}/>
        )
    }

    // 底灯 功能定义表为
    // 休闲:0X00;柔和:0X01;
    // 缤纷:0X02;;炫彩:0X03;
    // 动感:0X04;梦幻:0X05;
    // 地中海关:0X06;色彩:0x07;
    // 关:0XFF


    // 顶灯 功能定义表为
    // 休闲:0X00;柔和:0X01;
    // 缤纷:0X02;;炫彩:0X03;
    // 动感:0X04;梦幻:0X05;
    // 地中海关:0X06;色彩:0x07;
    // 关:0XFF


    _moodLightTopBtnView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 50
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 21,
                        marginBottom: 7
                    }}>
                    {this._moodLightTopBtnItemView(require('../../resources/light_leisure_btn.png'), require('../../resources/light_leisure_sel_btn.png'), strings('leisure'), this.props.moodLight == 0, () => {
                        this._onMoodLightEvent(0)
                    })}
                    {this._moodLightTopBtnItemView(require('../../resources/light_soft_btn.png'), require('../../resources/light_soft_sel_btn.png'), strings('soft'), this.props.moodLight == 1, () => {
                        this._onMoodLightEvent(1)
                    })}
                    {this._moodLightTopBtnItemView(require('../../resources/light_profusion_btn.png'), require('../../resources/light_profusion_sel_btn.png'), strings('profusion'), this.props.moodLight == 2, () => {
                        this._onMoodLightEvent(2)
                    })}
                    {this._moodLightTopBtnItemView(require('../../resources/light_colorful_btn.png'), require('../../resources/light_colorful_sel_btn.png'), strings('colorful'), this.props.moodLight == 3, () => {
                        this._onMoodLightEvent(3)
                    })}
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                    }}>
                    {this._moodLightTopBtnItemView(require('../../resources/light_gorgeous_btn.png'), require('../../resources/light_gorgeous_sel_btn.png'), strings('gorgeous'), this.props.moodLight == 4, () => {
                        this._onMoodLightEvent(4)
                    })}
                    {this._moodLightTopBtnItemView(require('../../resources/light_dynamic_btn.png'), require('../../resources/light_dynamic_sel_btn.png'), strings('dynamic'), this.props.moodLight == 5, () => {
                        this._onMoodLightEvent(5)
                    })}
                    {this._moodLightTopBtnItemView(require('../../resources/light_fantasy_btn.png'), require('../../resources/light_fantasy_sel_btn.png'), strings('fantasy'), this.props.moodLight == 6, () => {
                        this._onMoodLightEvent(6)
                    })}
                    {this._moodLightTopBtnItemView(require('../../resources/light_mediterranean_btn.png'), require('../../resources/light_mediterranean_sel_btn.png'), strings('mediterranean'), this.props.moodLight == 7, () => {
                        this._onMoodLightEvent(7)
                    })}
                </View>
            </View>
        )
    }

    _onMoodLightEvent(value) {
        if (this.props.onMoodLightEvent && (parseInt(this.props.topLight) != 255 || parseInt(this.props.bottomLight) != 255)) {
            this.props.onMoodLightEvent(value)
        }
    }

    _moodLightTopBtnItemView(icon, selIcon, text, isCheck, onClick) {
        return (
            <TouchableOpacity
                activeOpacity={(parseInt(this.props.topLight) != 255 || parseInt(this.props.bottomLight) != 255) ? 0.5 : 1}
                onPress={onClick}
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <Image
                    source={isCheck ? selIcon : icon}/>

                <CommonTextView
                    textSize={12}
                    style={{
                        color: '#000000',
                        marginTop: 7
                    }}
                    text={text}/>
            </TouchableOpacity>
        )
    }

    _moodLightBottomBtnView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginTop: 23,
                    width: this.mScreenWidth - 50
                }}>
                {this._moodLightBottomBtnItemView(strings('bottom_light'), this.props.topBottomLight == 0, () => {
                    //底灯
                    this._onMoodLightTopBottomEvent(0)
                })}

                <View
                    style={{
                        width: 14,
                        height: 1
                    }}/>

                {this._moodLightBottomBtnItemView(strings('top_light'), this.props.topBottomLight == 1, () => {
                    //顶灯
                    this._onMoodLightTopBottomEvent(1)
                })}
            </View>
        )
    }

    _onMoodLightTopBottomEvent(value) {
        if (this.props.onMoodLightTopBottomEvent) {
            this.props.onMoodLightTopBottomEvent(value)
        }
    }

    _moodLightBottomBtnItemView(text, isCheck, onClickEvent) {
        return (
            <TouchableOpacity
                onPress={onClickEvent}
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: 13,
                    borderRadius: 14,
                    backgroundColor: isCheck ? '#38579D' : '#F7F7F7',
                    flexDirection: 'row'
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: isCheck ? '#FFFFFF' : '#000000',
                    }}
                    text={text}/>

                {ViewHelper.getFlexView()}

                <Image
                    source={isCheck ? require('../../resources/light_power_on_btn.png') : require('../../resources/light_power_off_btn.png')}/>
            </TouchableOpacity>
        )
    }


    _btoomSwitchView() {
        return (
            <View
                style={{
                    marginTop: 26
                }}>
                {this._bottomSwitchItemView(strings('light_switch'), this.props.lightSwitchValue, this.props.lightSwitchClick)}
                {/*{this._bottomSwitchItemView(strings('top_light'), this.props.topLightSwitchValue, this.props.topLightSwitchClick)}*/}
                {/*{this._bottomSwitchItemView(strings('bottom_light'), this.props.bottomLightSwitchValue, this.props.bottomLightSwitchClick)}*/}
            </View>
        )
    }

    _bottomSwitchItemView(text, switchValue, clickEvent) {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 50,
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 50,
                        height: 2 / PixelRatio.get(),
                        backgroundColor: '#ddd',
                    }}/>

                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingBottom: 15,
                        paddingTop: 15
                    }}>
                    <CommonTextView
                        textSize={14}
                        style={{
                            color: '#000000',
                        }}
                        text={text}/>

                    {ViewHelper.getFlexView()}

                    <SwitchView
                        isCheck={switchValue}
                        onCheck={clickEvent}
                        style={{
                            marginLeft: 17,
                            backgroundColor: switchValue ? '#FF8748' : '#D9DCE1',
                        }}/>
                </View>
            </View>
        )
    }
}