import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import React from "react";
import DialogBottomBtnView from "./DialogBottomBtnView";
import {View} from "react-native";
import Slider from "./Slider";
import CommonTextView from "./CommonTextView";
import {strings} from "../Language/I18n";
import ViewHelper from "./ViewHelper";
import LeftRightSliderView from "./LeftRightSliderView";

/*
*设备设置页-slider弹窗-选择范围
 */
var tempSliderValue = 0
var tempSliderMinValue = 0
var tempSliderMaxValue = 0
export default class DeviceSettingSlideView extends BaseComponent {
    render() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    paddingTop: 22,
                }}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>

                {this._titleView()}
                {/*{this._sliderView()}*/}
                {/*{this._sliderRangeView()}*/}
                <LeftRightSliderView
                    style={{
                        marginTop: 20,
                    }}
                    sliderMinValue={this.props.sliderMinValue}
                    sliderMaxValue={this.props.sliderMaxValue}
                    onSlider1ValueComplete={(index) => {
                        if (this.props.onSlider1ValueComplete) {
                            this.props.onSlider1ValueComplete(index)
                        }
                    }}
                    onSlider1ValueChange={(index) => {
                        if (this.props.onSlider1ValueChange) {
                            this.props.onSlider1ValueChange(index)
                        }
                    }}
                    onSlider2ValueComplete={(index) => {
                        if (this.props.onSlider2ValueComplete) {
                            this.props.onSlider2ValueComplete(index)
                        }
                    }}
                    onSlider2ValueChange={(index) => {
                        if (this.props.onSlider2ValueChange) {
                            this.props.onSlider2ValueChange(index)
                        }
                    }}
                />
                <View
                    style={{
                        height: 26,
                        width: this.mScreenWidth
                    }}/>
                {this._btnView()}

            </DialogContainerView>
        );
    }

    _titleView() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    width: this.mScreenWidth
                }}>
                <CommonTextView
                    textSize={20}
                    style={{
                        color: '#000000',
                    }}
                    text={this.props.title}/>


                <CommonTextView
                    textSize={20}
                    style={{
                        color: '#FF8748',
                        marginTop: 17
                    }}
                    text={this._getTruthSliderMinValue() + (this.props.valueUnit ? this.props.valueUnit : '')
                    + '-' + this._getTruthSliderMaxValue() + (this.props.valueUnit ? this.props.valueUnit : '')}/>

            </View>
        )
    }

    _sliderView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 20,
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 50,
                        height: 34,
                        position: 'absolute',
                        backgroundColor: '#F7F7F7',
                        borderRadius: 17,
                        paddingLeft: 3,
                        paddingRight: 3,
                        paddingTop: 2,
                        paddingBottom: 2
                    }}>
                    <View
                        style={{
                            width: 28 + ((this.mScreenWidth - 56 - 28) * (this.props.sliderValue - this.props.minimumValue) / (this.props.maximumValue - this.props.minimumValue)),
                            height: 30,
                            borderRadius: 17,
                            backgroundColor: '#FF8748',
                        }}/>
                </View>

                <Slider
                    value={this.props.sliderValue}
                    style={{
                        width: this.mScreenWidth - 50 - 6 - 8,
                        height: 34
                    }}
                    trackStyle={{
                        height: 34,
                        borderRadius: 17,
                        backgroundColor: 'rgba(0,0,0,0)'
                    }}
                    //thumbImage={require('../resources/icon_thumb.png')}
                    thumbStyle={{
                        width: 20,
                        height: 20
                    }}
                    step={this.props.step}
                    minimumValue={this.props.minimumValue}
                    maximumValue={this.props.maximumValue}
                    minimumTrackTintColor="rgba(0,0,0,0)"
                    maximumTrackTintColor="rgba(0,0,0,0)"
                    thumbTintColor="#fff"
                    onValueChange={(value) => {
                        tempSliderValue = value;
                        if (this.props.onValueChange) {
                            this.props.onValueChange(value)
                        }
                    }}
                    onSlidingComplete={() => {
                        if (this.props.onSlidingComplete) {
                            this.props.onSlidingComplete(tempSliderValue)
                        }
                    }}
                />
            </View>
        )
    }

    _sliderRangeView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 20,
                    flexDirection: 'row',
                    justifyContent: 'center'
                }}>

                <View
                    style={{
                        width: this.mScreenWidth - 50,
                        height: 34,
                        position: 'absolute',
                        backgroundColor: '#F7F7F7',
                        borderRadius: 17,
                        paddingLeft: 3,
                        paddingRight: 3,
                        paddingTop: 2,
                        paddingBottom: 2,
                        flexDirection: 'row'
                    }}>
                    <View
                        style={{
                            flex: 1
                        }}>
                        <View
                            style={{
                                width: 25 + ((this.mScreenWidth - 50 - 6) / 2) - (25 + ((((this.mScreenWidth - 50 - 6) / 2) - 25) * (this._getTruthSliderMinValue() - this.props.minimumValue)
                                    / ((parseInt(this.props.maximumValue - this.props.minimumValue) / 2) - this.props.minimumValue))),
                                height: 30,
                                borderTopLeftRadius: 17,
                                borderBottomLeftRadius: 17,
                                backgroundColor: '#FF8748',
                                position: 'absolute',
                                right: 0
                            }}/>
                    </View>

                    <View
                        style={{
                            flex: 1
                        }}>
                        <View
                            style={{
                                width: 25 + ((((this.mScreenWidth - 50 - 6) / 2) - 25) * (this._getTruthSliderMaxValue() - ((parseInt(this.props.maximumValue - this.props.minimumValue) / 2) + 1))
                                    / (this.props.maximumValue - ((parseInt(this.props.maximumValue - this.props.minimumValue) / 2) + 1))),
                                height: 30,
                                borderTopRightRadius: 17,
                                borderBottomRightRadius: 17,
                                backgroundColor: '#FF8748',
                            }}/>
                    </View>
                </View>

                <Slider
                    value={this._getTruthSliderMinValue()}
                    style={{
                        width: (this.mScreenWidth - 50 - 6 - 8) / 2,
                        height: 34
                    }}
                    trackStyle={{
                        height: 34,
                        borderRadius: 17,
                        backgroundColor: 'rgba(0,0,0,0)'
                    }}
                    //thumbImage={require('../resources/icon_thumb.png')}
                    thumbStyle={{
                        width: 20,
                        height: 20
                    }}
                    step={this.props.step}
                    minimumValue={this.props.minimumValue}
                    maximumValue={parseInt(this.props.maximumValue - this.props.minimumValue) / 2}
                    minimumTrackTintColor="rgba(0,0,0,0)"
                    maximumTrackTintColor="rgba(0,0,0,0)"
                    thumbTintColor="#fff"
                    onValueChange={(value) => {
                        tempSliderMinValue = value;
                        if (this.props.onMinValueChange) {
                            this.props.onMinValueChange(value)
                        }
                    }}
                    onSlidingComplete={() => {
                        if (this.props.onMinSlidingComplete) {
                            this.props.onMinSlidingComplete(tempSliderMinValue)
                        }
                    }}
                />

                <Slider
                    value={this._getTruthSliderMaxValue()}
                    style={{
                        width: (this.mScreenWidth - 50 - 6 - 8) / 2,
                        height: 34,
                    }}
                    trackStyle={{
                        height: 34,
                        borderRadius: 17,
                        backgroundColor: 'rgba(0,0,0,0)'
                    }}
                    //thumbImage={require('../resources/icon_thumb.png')}
                    thumbStyle={{
                        width: 20,
                        height: 20
                    }}
                    step={this.props.step}
                    minimumValue={(parseInt(this.props.maximumValue - this.props.minimumValue) / 2) + 1}
                    maximumValue={this.props.maximumValue}
                    minimumTrackTintColor="rgba(0,0,0,0)"
                    maximumTrackTintColor="rgba(0,0,0,0)"
                    thumbTintColor="#fff"
                    onValueChange={(value) => {
                        tempSliderMaxValue = value;
                        if (this.props.onMaxValueChange) {
                            this.props.onMaxValueChange(value)
                        }
                    }}
                    onSlidingComplete={() => {
                        if (this.props.onMaxSlidingComplete) {
                            this.props.onMaxSlidingComplete(tempSliderMaxValue)
                        }
                    }}
                />

            </View>
        )
    }

    _btnView() {
        return (
            <DialogBottomBtnView
                onLeftText={this.props.leftBtnText}
                onLeftTextColor={this.props.leftBtnTextColor}
                onLeftBtnClick={() => {
                    if (this.props.leftBtnText) {
                        this.props.onLeftBtnClick()
                    }
                }}
                onRightText={this.props.rightBtnText}
                onRightTextColor={this.props.rightBtnTextColor}
                onRightBtnClick={() => {
                    if (this.props.rightBtnText) {
                        this.props.onRightBtnClick()
                    }
                }}/>
        )
    }

    _getTruthSliderMinValue() {
        var sliderMinValue = this.props.sliderMinValue
        if (sliderMinValue < this.props.minimumValue) {
            sliderMinValue = this.props.minimumValue
        }
        return sliderMinValue
    }

    _getTruthSliderMaxValue() {
        var sliderMaxValue = this.props.sliderMaxValue
        if (sliderMaxValue > this.props.maximumValue) {
            sliderMaxValue = this.props.maximumValue
        }
        return sliderMaxValue
    }
}