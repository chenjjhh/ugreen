import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";

/*
*开关
 */
export default class SwitchView extends BaseComponent {
    render() {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.onCheck()
                }}
                style={[{
                    width: 45,
                    height: 23,
                    backgroundColor: this.props.isCheck ? (this.props.isCheckColor ? this.props.isCheckColor : '#38579d') : '#D9DCE1',
                    borderRadius: 23 / 2,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }, this.props.style]}>
                {this.props.isCheck ? ViewHelper.getFlexView() : null}
                <View
                    style={{
                        width: 17,
                        height: 17,
                        borderRadius: 9,
                        backgroundColor: '#fff',
                        marginLeft: 3,
                        marginRight: 3
                    }}/>
                {!this.props.isCheck ? ViewHelper.getFlexView() : null}
            </TouchableOpacity>
        );
    }
}