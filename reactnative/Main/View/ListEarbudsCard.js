import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ElectricQuantityProgress from "../View/ElectricQuantityProgress";
import {strings} from "../Language/I18n";
import CommonTextView from "./CommonTextView";

/*
*设备列表卡片
 */
export default class ListEarbudsCard extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    height: 100,
                    alignItems: 'center',
                    marginBottom: 20
                }}>
                <TouchableOpacity
                    activeOpacity={this.props.onPress ? 0.5 : 1}
                    onPress={() => {
                        if (this.props.onPress) {
                            this.props.onPress()
                        }
                    }}
                    onLongPress={() => {
                        if (this.props.onLongPress) {
                            this.props.onLongPress()
                        }
                    }}
                    style={{
                        width: this.mScreenWidth - 24,
                        backgroundColor: '#fff',
                        borderRadius: 10,
                        flexDirection: 'row',
                        padding: 20,
                        // elevation: Platform.OS === 'android' ? 5 : 1, // 设置阴影角度，通过这个设置有无阴影（这个是最重要的，决定有没有阴影）
                        // shadowColor: '#000', // 阴影颜色
                        // shadowOffset: {width: 0, height: 0}, // 阴影偏移
                        // shadowOpacity: Platform.OS === 'android' ? 1 : 0.1, // 阴影不透明度
                        // shadowRadius: 12, // 圆角
                        marginBottom: 10,
                        alignItems: 'center'
                    }}>

                    {this._iconView()}
                    {this.props.data ? this._rightView() : null}
                </TouchableOpacity>
            </View>
        );
    }

    _iconView() {
        return (
            <Image
                source={require('../../resources/earbuds/t6/list_headphone_img.png')}/>
        )
    }

    _rightView() {
        return (
            <View
                style={{
                    marginLeft: 30,
                    flex: 1,
                }}>
                {
                    <View style={{width: '100%'}}>
                        <Text numberOfLines={1}>
                            <CommonTextView
                                textSize={14}
                                text={this.props.data?.deviceName}
                                style={{
                                    color: '#404040',
                                    width: this.mScreenWidth / 2,
                                    fontWeight: 'bold'
                                }}
                            />
                        </Text>
                    </View>
                }

                {/* 在线状态 t-在线 f-不在线 */}
                {/* {!this.props.data?.isOnline ?
                    this._offlineIconView() :
                    this._onlineIconView()}*/}

                {this._onlineIconView()}
            </View>
        )
    }

    _offlineIconView() {
        return (
            <Image source={require('../../resources/greenUnion/list_unconnect_ic.png')}/>
        )
    }

    _onlineIconView() {
        return (
            <Image source={require('../../resources/greenUnion/list_connect_ic.png')}/>
        )
    }

}