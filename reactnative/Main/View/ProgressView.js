import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";

/*
*进度条
 */
export default class ProgressView extends BaseComponent {
    render() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 80,
                    height: 8,
                    backgroundColor: '#F7F7F7',
                    borderRadius: 4,
                    borderColor: 'rgba(0,0,0,0.1)',
                    borderWidth: 0.2
                }}>

                <View
                    style={{
                        height: 8,
                        borderRadius: 4,
                        backgroundColor: '#FF8748',
                        width: 8 + (this.mScreenWidth - 80 - 8) * (this.props.progress / 100)
                    }}/>

            </View>
        );
    }
}