import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "./CommonTextView";

/*
*页面顶部标题
 */
export default class PagerTitleView extends BaseComponent {
    render() {
        return (
            <CommonTextView
                textSize={24}
                text={this.props.titleText}
                style={[
                    {
                        color: '#000000',
                        marginLeft: 18
                    },this.props.style
                ]}
            />
        );
    }
}