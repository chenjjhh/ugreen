import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "./CommonTextView";

/*
*弹窗底部按钮
 */
export default class DialogBottomBtnView extends BaseComponent {
    render() {
        return (
            <View style={{width:'100%'}}>
                {this._bottomBtnView()}
            </View>
        );
    }

    _bottomBtnView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent:(this.props.onLeftText && this.props.onRightText)?'space-between':'center',
                    alignItems: 'center',
                }}>
                {this.props.onLeftText && this.props.onLeftBtnClick ?
                    this._btnItemView(this.props.onLeftText, this.props.onLeftTextColor,this.props.onLeftBackgroundColor, () => {
                        this.props.onLeftBtnClick()
                    }) : null}
                {this.props.onRightText && this.props.onRightBtnClick ?
                    this._btnItemView(this.props.onRightText, this.props.onRightTextColor,this.props.onRightBackgroundColor, () => {
                        this.props.onRightBtnClick()
                    }) : null}
            </View>
        )
    }

    _btnItemView(text, textColor, bgColor,clickEvent) {
        return (
            <TouchableOpacity
                activeOpacity={this.props.clickAble ? 0.5 : 1}
                onPress={clickEvent}
                style={[
                    {
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: bgColor,
                        height: 50,
                        borderRadius: this.viewCommonWith / 2,
                        flex:(this.props.onLeftText && this.props.onRightText)?0.4:1,
                    }
                ]}>
                <Text
                    style={[
                        {
                            color: textColor,
                            fontSize: this.getSize(14),
                            fontFamily:'DIN Alternate Bold'
                        }
                    ]}>
                    {text}
                </Text>
            </TouchableOpacity>

            // <TouchableOpacity
            //     onPress={clickEvent}
            //     style={{
            //         alignItems: 'center',
            //         justifyContent: 'center',
            //         height: 44,
            //         width:139,
            //         backgroundColor:bgColor,
            //         borderRadius: 22.5,
            //     }}>
            //     <CommonTextView
            //         textSize={16}
            //         text={text}
            //         style={{
            //             color: textColor,
            //         }}
            //     />
            // </TouchableOpacity>
        )
    }
}