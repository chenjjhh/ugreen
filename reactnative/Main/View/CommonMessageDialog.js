import React from 'react';
import {Keyboard, View} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import CommonTextView from "./CommonTextView";
import DialogBottomBtnView from "./DialogBottomBtnView";
/*
*消息弹窗
 */
export default class MessageDialog extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight: 0,
        }
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (event) => {
            this.setState({keyboardHeight: event.endCoordinates.height})
        });
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (event) => {
            this.setState({keyboardHeight: 0})
        });
    }

    componentWillUnmount() {
        this.keyboardDidShowListener && this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener && this.keyboardDidHideListener.remove()
    }

    render() {
        return (
            <DialogContainerView
                cardStyle={[{
                    alignItems: 'center',
                    paddingTop: 35,
                    with: this.mScreenWidth,
                    paddingHorizontal: 29,
                    paddingBottom: 20
                }, this.props.style]}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>
                <CommonTextView
                    textSize={18}
                    text={this.props.title}
                    style={{
                        color: '#282A30',
                        fontWeight: 'bold'
                    }}/>
                {this.props.children ? this.props.children :
                    (<CommonTextView
                            textSize={14}
                            text={this.props.message}
                            style={[
                                {
                                    color: '#969AA1',
                                    marginTop: 35,
                                    marginBottom: 35,
                                    lineHeight: 25,
                                    fontWeight: 'bold',
                                }, this.props.messageStyle
                            ]}/>
                    )}
                <DialogBottomBtnView
                    onLeftText={this.props.leftBtnText}
                    onLeftTextColor={this.props.leftBtnTextColor}
                    onLeftBackgroundColor={this.props.onLeftBackgroundColor}
                    onRightBackgroundColor={this.props.onRightBackgroundColor}
                    onLeftBtnClick={() => {
                        if (this.props.leftBtnText) {
                            this.props.onLeftBtnClick()
                        }
                    }}
                    onRightText={this.props.rightBtnText}
                    onRightTextColor={this.props.rightBtnTextColor}
                    onRightBtnClick={() => {
                        if (this.props.rightBtnText) {
                            this.props.onRightBtnClick()
                        }
                    }}/>
                {this.state.keyboardHeight == 0 ? null :
                    <View
                        style={{
                            width: this.mScreenWidth,
                            height: this.state.keyboardHeight,
                            backgroundColor:'rgba(255,255,255,0)'
                        }}/>}
            </DialogContainerView>
        );
    }
}