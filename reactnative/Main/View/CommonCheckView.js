import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";

/*
*通用复选框
 */
const viewWH = 16

export default class CommonCheckView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            isCheck: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isCheck != this.state.isCheck) {
            this.setState({ isCheck: nextProps.isCheck });
        }
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => {
                    this._onCheck()
                }}
                style={[
                    {
                        width: viewWH,
                        height: viewWH,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }, this.props.style
                ]}>
                {
                    this.state.isCheck ?
                        <Image
                            style={{
                                width: viewWH + 2,
                                height: viewWH + 2,
                                resizeMode: 'contain'
                            }}
                            source={require('../../resources/greenUnion/sel_ic.png')}/>
                        : <Image
                            style={{
                                width: viewWH + 2,
                                height: viewWH + 2,
                                resizeMode: 'contain'
                            }}
                            source={require('../../resources/greenUnion/unsel_ic.png')}/>

                        }
            </TouchableOpacity>
        );
    }

    _onCheck() {
        this.setState({isCheck: !this.state.isCheck})
        if (this.props.onCheck) {
            this.props.onCheck(!this.state.isCheck)
        }
    }

}