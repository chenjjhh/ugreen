import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import CommonTextView from './CommonTextView'
import {container} from 'aws-amplify';

/*
*itemLIst项
 */
export default class ItemBtnView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const {
            leftIcon,
            rightIcon,
            text,
            onPress,
            onLeftIcon2Press,
            rightText,
            leftIconStyle,
            rightIconStyle,
            rightIconContainerStyle,
            containerStyle,
            leftIcon2,
            disabled,
            activeOpacity
        } = this.props
        return (
            <View
                style={[
                    {

                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        overflow: 'hidden',
                        backgroundColor: '#ffffff',
                        width: this.mScreenWidth - 40,
                        height: 60,
                    }, this.props.style
                ]}>
                <View style={{
                    height: 60,
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <View style={{flexDirection: 'row'}}>
                        {leftIcon ? (<Image
                            style={[{
                                resizeMode: 'contain',
                                paddingHorizontal: 30
                            }, leftIconStyle]}
                            source={leftIcon}/>) : null}
                    </View>
                    <View style={[{
                        flex: 1,
                        height: '100%',
                        flexDirection: 'row',
                        marginHorizontal: 15,
                        borderBottomColor: '#EBEBEB',
                        borderBottomWidth: 0.5,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }, containerStyle]}>
                        <View style={{height: '100%', flexDirection: 'row', alignItems: 'center', paddingRight: 7}}>
                            <View style={{flexDirection: 'row', alignItems: 'center', height: '100%',}}>
                                <CommonTextView
                                    textSize={14}
                                    text={text}
                                    style={[{
                                        color: disabled ? "#ccc" : '#404040',
                                        fontWeight: 'normal',
                                    }, this.props.leftTextStyle]}/>
                                {leftIcon2 ? (
                                    <TouchableOpacity
                                        style={{height: '100%', paddingHorizontal: 10, justifyContent: 'center',}}
                                        onPress={() => onLeftIcon2Press && onLeftIcon2Press()}>
                                        <Image
                                            style={[{
                                                resizeMode: 'contain',
                                            }, leftIconStyle]}
                                            source={leftIcon2}/>
                                    </TouchableOpacity>
                                ) : null}

                            </View>
                        </View>
                        <TouchableOpacity
                            activeOpacity={activeOpacity || 0.5}
                            disabled={disabled}
                            onPress={() => onPress && onPress()}
                            style={{
                                flexDirection: 'row',
                                flex: 1,
                                alignItems: 'center',
                                height: '100%',
                                justifyContent: 'flex-end',
                            }}>
                            <View style={{marginHorizontal: 5}}>
                                {rightText ? (<CommonTextView
                                    textSize={12}
                                    text={rightText}
                                    style={[{
                                        color: disabled ? "#ccc" : '#404040',
                                        fontWeight: 'normal',
                                    }, this.props.rightTextStyle]}/>) : null}
                            </View>
                            {rightIcon ? (
                                <View style={rightIconContainerStyle}>
                                    <Image
                                        style={[{
                                            tintColor: disabled ? "#ccc" : null,
                                            resizeMode: 'contain',
                                        }, rightIconStyle]}
                                        source={rightIcon}/>
                                </View>
                            ) : null}
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}