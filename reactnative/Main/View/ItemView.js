import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import CommonTextView from '../View/CommonTextView'

/*
*itemLIst项
 */
export default class ItemView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    render() {
        const { leftIcon, rightIcon, text, onPress, rightText } = this.props
        return (
            <TouchableOpacity
                onPress={()=>onPress && onPress()}
                style={[
                    {
                        borderRadius: 27,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        overflow: 'hidden',
                        backgroundColor:'#fff',
                        width:this.mScreenWidth,
                        height:60,
                    }, this.props.style
                ]}>
                <View style={{height: 60,flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                    <View style={{flexDirection:'row'}}>
                        {leftIcon ?(<Image
                            style={[{
                                resizeMode: 'contain',
                                paddingHorizontal: 30
                            }, ]}
                            source={leftIcon}/>):null}
                    </View>
                    <View style={{flex:1,height:'100%',flexDirection:'row',borderBottomColor:'#EBEBEB',borderBottomWidth:1,alignItems:'center',justifyContent:'space-between',marginRight:20}}>
                        <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center',paddingRight:7}}>
                            <CommonTextView
                                textSize={14}
                                text={text}
                                style={[{
                                    color: '#404040',
                                    fontWeight: 'normal',
                                },this.props.leftTextStyle]}/>
                            {rightText ? ( <CommonTextView
                                textSize={12}
                                text={rightText}
                                style={[{
                                    color: '#808080',
                                    fontWeight: 'normal',
                                },this.props.rightTextStyle]}/>):null}
                        </View>
                        {rightIcon ?(<Image
                            style={[{
                                resizeMode: 'contain',
                                // paddingHorizontal: 25,
                            }, ]}
                            source={rightIcon}/>):null}

                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}