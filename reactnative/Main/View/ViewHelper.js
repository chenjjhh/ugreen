import React from 'react';
import {
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';

export default class ViewHelper {
    static getFlexView() {
        return (
            <View style={{flex: 1}}/>
        )
    }

    static getFlex2View() {
        return (
            <View style={{flex: 2}}/>
        )
    }

    static getFixFlexView(flex) {
        return (
            <View style={{flex: flex}}/>
        )
    }
}