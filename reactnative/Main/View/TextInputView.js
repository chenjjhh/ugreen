import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";

/*
*通用输入框（密码输入框）
 */
export default class TextInputView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            showPdText: true,
        }
    }

    render() {
        return (
            <View
                style={[
                    {
                        height: 50,
                        width: this.mScreenWidth * 0.73,
                        backgroundColor: '#F7F8FB',
                        borderRadius: 27,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        overflow: 'hidden',
                    }, this.props.style
                ]}>
                {
                this.props.isPassword ?
                    <TouchableOpacity
                        style={{
                            paddingLeft: 20,
                            paddingRight:12,
                        }}
                        onPress={() => {
                            this.setState({showPdText: !this.state.showPdText})
                            this.dissmissKeyboard()
                        }}>
                        <Image
                            style={[{
                                resizeMode: 'contain'
                            }, this.props.passwordIconStyle]}
                            source={this.state.showPdText ? require('../../resources/greenUnion/log_hide_ic.png') : require('../../resources/greenUnion/log_show_ic.png')}/>
                    </TouchableOpacity> : null
                }
                {
                this.props.verifyCodeLogin ?
                    <TouchableOpacity
                        style={{
                            paddingLeft: 20,
                            paddingRight:12,
                        }}
                        onPress={() => {
                            this.setState({showPdText: !this.state.showPdText})
                            this.dissmissKeyboard()
                        }}>
                        <Image
                            style={[{
                                resizeMode: 'contain'
                            }, this.props.passwordIconStyle]}
                            source={require('../../resources/greenUnion/log_vcode_ic.png')}/>
                    </TouchableOpacity> : null
                }
                
                {this.props.isPhone ?
                    <View
                        style={{
                            paddingLeft: 20,
                            paddingRight:12,
                        }}
                        >
                        <Image
                            style={[{
                                resizeMode: 'contain'
                            }, this.props.passwordIconStyle]}
                            source={require('../../resources/greenUnion/log_phone_ic.png')}/>
                    </View> : null
                }
                {this.props.isEmail ?
                    <View
                        style={{
                            paddingLeft: 20,
                            paddingRight:12,
                        }}
                        >
                        <Image
                            style={[{
                                resizeMode: 'contain'
                            }, this.props.passwordIconStyle]}
                            source={require('../../resources/greenUnion/log_mail_ic.png')}/>
                    </View> : null
                }


                <TextInput
                    multiline={this.props.multiline}
                    style={[
                        {
                            height: 50,
                            textAlign: 'left',
                            paddingRight: this.props.isPassword ? 13 : 23,
                            flex: 1,
                            fontSize: this.getSize(14),
                            color: '#404040',
                            
                            // fontFamily:'DIN Alternate Bold',
                        }, this.props.inputStyle
                    ]}
                    secureTextEntry={this.props.isPassword ? this.state.showPdText : false}
                    maxLength={this.props.maxLength}
                    onChangeText={(text) => {
                        this._onChangeText(text);
                    }}
                    value={this.props.value}
                    placeholderTextColor={this.props.placeholderTextColor || '#C4C6CD'}
                    placeholder={this.props.placeholder}
                    keyboardType={this.props.keyboardType}/>

                {this.props.isPassword && this.props.doubt ?
                    <TouchableOpacity
                        style={{
                            height:'100%',
                            paddingHorizontal: 20,
                            justifyContent:'center',
                            alignItems:'center',
                        }}
                        onPress={() => {
                            this.props.navigation.navigate('ResetPassword');
                        }}>
                        <Text style={{color:'#808080',fontSize: this.getSize(12),}}>{strings('forget_pd')+'?'}</Text>
                    </TouchableOpacity> : null
                }
                {this.props.verifyCodeLogin ?
                    <TouchableOpacity
                        activeOpacity={this.props.getCodeAble ? 0.5 : 1}
                        style={{
                            height:'100%',
                            paddingHorizontal: 20,
                            justifyContent:'center',
                            alignItems:'center',
                        }}
                        onPress={()=> this.props.onPress && this.props.onPress()}>
                        <Text style={{color:'#808080',fontSize: this.getSize(12),}}>{this.props.verCodeText}</Text>
                    </TouchableOpacity> : null
                }
                

            </View>
        );
    }

    _onChangeText(text) {
        if (this.props.onChangeText) {
            this.props.onChangeText(text);
        }
    }
}