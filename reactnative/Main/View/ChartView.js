import BaseComponent from '../../Main/Base/BaseComponent';
import {Device, DeviceEvent, Host, Package, Service} from "../../../../miot-sdk";
import React from "react";
import {
    Image,
    ImageBackground,
    Platform,
    ScrollView,
    Slider,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Alert,
    PanResponder,
    ART,
    StatusBar,
    Dimensions,
    Animated,
    TouchableHighlight,
    Modal,
    DeviceEventEmitter,
    AppState,
    ToastAndroid
} from 'react-native';

import {Surface, Shape, Path} from "@react-native-community/art";

let SMOOTHNESS = 0.1;

let chartHeight = 50;


export default class ChartView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            viewWidth: this.mScreenWidth * 0.77
        };
    }

    componentWillMount() {
    }


    componentWillReceiveProps(props) {
    }

    render() {


        return (
            <View style={{
                width: this.state.viewWidth,
                alignItems: 'center',
                backgroundColor: 'rgba(0,0,0,0)'
            }}>

                <View style={{
                    width: this.state.viewWidth
                }}>

                    <Surface
                        height={chartHeight + 10}
                        width={this.state.viewWidth}
                    >
                        <Shape
                            d={this._drawChartPath(this.props.yAllArray, this.props.data, false, chartHeight + 10)}
                            stroke={this.props.lineColor} strokeWidth={1}
                        />
                    </Surface>


                </View>

            </View>
        );
    }


    _drawChartPath(yArray, chartArray, showShaw, height) {


        if (chartArray.length == 0)
            return;
        let pointList = [];
        let controlPointList = [];

        let firstIndex = -1;
        yArray.map((item, index) => {
            chartArray.map((item1, index1) => {
                if (parseInt(yArray[index].timestamp) == parseInt(chartArray[index1].time)) {
                    if (firstIndex == -1) {
                        firstIndex = index;
                    }
                }
            });
        });

        pointList = this._calculateChartValuePoint(firstIndex, chartArray, yArray, height);

        let strPath = '';
        if (pointList.length > 0) {

            pointList.map((item, index) => {
                if (index == 0) {
                    strPath = `M${ item.x } ${ item.y }`;
                    strPath = `${ strPath } L${ pointList[index + 1].x } ${ item.y } `;
                } else if (index != pointList.length - 1) {
                    strPath = `${ strPath } L${ item.x } ${ item.y } `;
                    strPath = `${ strPath } L${ pointList[index + 1].x } ${ item.y } `;
                }
            })

            // strPath = `M${ pointList[0].x } ${ pointList[0].y }`;
            // strPath = `${ strPath } L${ pointList[1].x } ${ pointList[0].y } `;
            //
            // strPath = `${ strPath } L${ pointList[1].x } ${ pointList[1].y } `;
            // strPath = `${ strPath } L${ pointList[2].x } ${ pointList[1].y } `;
            //
            // strPath = `${ strPath } L${ pointList[2].x } ${ pointList[2].y } `;
            // strPath = `${ strPath } L${ pointList[3].x } ${ pointList[2].y } `;
            //
            // strPath = `${ strPath } L${ pointList[3].x } ${ pointList[3].y } `;
            // strPath = `${ strPath } L${ pointList[4].x } ${ pointList[3].y } `;


            return new Path(strPath);
        }
    }

    _calculateChartValuePoint(position, dataArray, yArray, max) { // 计算数据点坐标
        let xyArray = [];
        dataArray.map((item, index) => {
            xyArray.push({
                x: ((index + position) * ((this.state.viewWidth)) / (yArray.length)).toFixed(1),
                y: (max - parseInt(this._getTruthValue(item.value)))
            });
        });
        xyArray.push({
            x: ((yArray.length) * ((this.state.viewWidth)) / (yArray.length)).toFixed(1),
            y: (max - parseInt(this._getTruthValue(dataArray[dataArray.length - 1].value)))
        });

        return xyArray;
    }

    _getTruthValue(value) { // value大于100时 除以10  否则不变
        // return parseInt(value) > 100 ? parseInt(value) / 10 : parseInt(value)
        let text = 0;
        if (this.props.scaleAble) {
            let max = this._getMaxForArray(this.props.data);

            if (max != 0)
                text = (value == 0 ? 0 : ((value / max) * chartHeight));
        } else {
            text = value;
        }

        return text;
    }

    _getMaxForArray(array) {
        let max = 0;
        max = Math.max(...array.map((item) => item.value));

        return max;
    }
}
