import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import CommonTextView from "./CommonTextView";
import ViewHelper from "./ViewHelper";
import SwitchView from "./SwitchView";

/*
*key value  View
 */
export default class ListItemView extends BaseComponent {
    render() {
        return (
            <TouchableOpacity
                activeOpacity={this.props.onPress ? 0.5 : 1}
                onPress={() => {
                    if (this.props.onPress) {
                        this.props.onPress()
                    }
                }}
                style={[{
                    height: 56,
                    flexDirection: 'row',
                    width: this.mScreenWidth - 36,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: 24,
                    paddingRight: 11,
                }, this.props.style
                ]}>
                <CommonTextView
                    textSize={14}
                    style={[
                        {
                            color: '#000000',
                        }, this.props.titleStyle
                    ]}
                    text={this.props.title}
                />

                {ViewHelper.getFlexView()}

                {
                    this.props.onCheck ?
                        <SwitchView
                            isCheck={this.props.isCheck}
                            onCheck={this.props.onCheck}/>
                        : null
                }

                {
                    this.props.rightIcon ?
                        <Image
                            style={{
                                width: 16,
                                height: 16,
                                resizeMode: 'contain',
                                marginRight: 8,
                            }}
                            source={this.props.rightIcon}/> : null
                }

                {
                    this.props.rightIcons && this.props.rightIcons.length > 0 ?
                        this.props.rightIcons.map((item, index) => {
                            return (
                                <Image
                                    key={index}
                                    style={{
                                        width: 16,
                                        height: 16,
                                        resizeMode: 'contain',
                                        marginRight: 8,
                                    }}
                                    source={item}/>
                            )
                        }) : null
                }

                {
                    this.props.value ?
                        <CommonTextView
                            textSize={14}
                            style={{
                                color: '#B5BAC5',
                                textAlign: 'center'
                            }}
                            text={this.props.value}
                        /> : null
                }

                {
                    this.props.hideArrowIcon ?
                        this.props.rightPadding ?
                            <View
                                style={{
                                    marginLeft: 4,
                                    width: 25,
                                    height: 25,
                                }}
                            /> :
                            null :
                        <Image
                            style={{
                                marginLeft: 4,
                                width: 25,
                                height: 25,
                                resizeMode: 'contain'
                            }}
                            source={require('../../resources/list_arrow_ic.png')}/>
                }

            </TouchableOpacity>
        );
    }
}