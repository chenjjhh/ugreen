import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import CommonTextView from "./CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import Helper from "../Utils/Helper";
/*
*关联第三方弹窗
 */
export default class LinkThirdDialog extends BaseComponent {
    render() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    paddingTop: 35,
                    paddingLeft: 50,
                    paddingRight: 50,
                    paddingBottom: 50
                }}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>
                <CommonTextView
                    textSize={18}
                    text={strings('link_third_account')}
                    style={{
                        color: '#282A30',
                    }}/>

                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 50
                    }}>
                    {this._thirdBtnItemView(require('../../resources/link_facebook_btn.png'), () => {
                        if (this.props.onFacebookClickEvent) {
                            this.props.onFacebookClickEvent()
                        }
                    })}
                    {Helper.getPlatformOsFlag() == 0 ? this._thirdBtnItemView(require('../../resources/account_apple_ic.png'), () => {
                        if (this.props.onAppleClickEvent) {
                            this.props.onAppleClickEvent()
                        }
                    }) : null}
                    {this._thirdBtnItemView(require('../../resources/link_google_btn.png'), () => {
                        if (this.props.onGoogleClickEvent) {
                            this.props.onGoogleClickEvent()
                        }
                    })}
                </View>
            </DialogContainerView>
        );
    }

    _thirdBtnItemView(btnIcon, onClickEvent) {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
                onPress={onClickEvent}>
                <Image
                    source={btnIcon}/>
            </TouchableOpacity>
        )
    }
}