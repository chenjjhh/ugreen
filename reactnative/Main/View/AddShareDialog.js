import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio,Keyboard
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import CommonTextView from "./CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "./DialogBottomBtnView";
/*
*添加分享弹窗
 */
export default class AddShareDialog extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight: 0,
        }
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (event) => {
            this.setState({keyboardHeight: event.endCoordinates.height})
        });
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (event) => {
            this.setState({keyboardHeight: 0})
        });
    }

    componentWillUnmount() {
        this.keyboardDidShowListener && this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener && this.keyboardDidHideListener.remove()
    }

    render() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    paddingTop: 35,
                    paddingLeft: 40,
                    paddingRight: 40,
                }}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>

                <CommonTextView
                    textSize={18}
                    text={this.props.title}
                    style={{
                        color: '#282A30',
                    }}/>


                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 30,
                        marginBottom: 36,
                        height: 50,
                        backgroundColor: '#EBEEF0',
                        borderRadius: 10,
                        width: this.mScreenWidth * 0.66,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>

                    <TextInput
                        style={[
                            {
                                height: 40,
                                textAlign: 'left',
                                paddingLeft: 15,
                                paddingRight: 12,
                                flex: 1,
                                fontSize: this.getSize(14),
                                color: '#000',
                                fontFamily:'DIN Alternate Bold'
                            }, this.props.inputStyle
                        ]}
                        maxLength={32}
                        onChangeText={(text) => {
                            this.props.onChangeText(text)
                        }}
                        value={this.props.emailValue}
                        placeholderTextColor={this.props.placeholderTextColor}
                        placeholder={this.props.placeholder}
                        keyboardType={'email-address'}/>
                </View>


                <DialogBottomBtnView
                    onLeftText={this.props.leftBtnText}
                    onLeftTextColor={this.props.leftBtnTextColor}
                    onLeftBtnClick={() => {
                        if (this.props.leftBtnText) {
                            this.props.onLeftBtnClick()
                        }
                    }}/>

                {this.state.keyboardHeight == 0 ? null :
                    <View
                        style={{
                            width: this.mScreenWidth,
                            height: this.state.keyboardHeight
                        }}/>}
            </DialogContainerView>
        );
    }
}