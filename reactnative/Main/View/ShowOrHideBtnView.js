import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Animated,
    TouchableOpacity,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import CommonTextView from "../View/CommonTextView";
/*
*展开或者收起组件
 */
export default class ShowOrHideBtnView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
          rotateValue: new Animated.Value(0), // 旋转角度
        };
        this.isRotated = false;// 是否已旋转
      }
    
      handlePress = () => {
        const { rotateValue } = this.state;
        const { onPress, type} = this.props;
        
        if (this.isRotated) {
          Animated.spring(rotateValue, {
            toValue: 0,
            useNativeDriver: true,
          }).start(() => {
            this.isRotated = false
          });
        } else {
          Animated.timing(rotateValue, {
            toValue: 1,
            duration: 100,
            useNativeDriver: true,
          }).start(() => {
            this.isRotated = true
          });
        }
        onPress?.(type,this.isRotated)
      };

    render() {
        const { rightIcon, text, onPress, rightIconStyle, rightIconContainerStyle,containerStyle, disabled, boxStyle } = this.props
        const { rotateValue } = this.state;
        const rotateZ = rotateValue.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '90deg'],
        });
        const myFunction = this.debounce(this.handlePress, 500); // 延迟 500 毫秒
        return (
            <TouchableOpacity
                onPress={()=>myFunction()}
                disabled={disabled}
                style={[
                    {
                        
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        overflow: 'hidden',
                        backgroundColor:'#ffffff',
                        width:this.mScreenWidth-40,
                        height:60,
                        borderRadius:10,
                        marginBottom:10
                    }, this.props.style
                ]}>
                <View style={[{height: 60,width:'100%', borderBottomColor:'#EBEBEB',borderBottomWidth:0.5,flexDirection:'row',justifyContent:'space-between',alignItems:'center'},boxStyle]}>
                    <View style={[{flex:1,height:'100%',flexDirection:'row',marginHorizontal:15,borderBottomColor:'#EBEBEB',borderBottomWidth:0.5,alignItems:'center',justifyContent:'space-between',},containerStyle]}>
                        <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',alignItems:'center',paddingRight:7}}>
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <CommonTextView
                                    textSize={14}
                                    text={text}
                                    style={[{
                                        color: disabled ? '#ccc': '#404040',
                                        fontWeight: 'normal',
                                    },this.props.leftTextStyle]}/>
                            </View>
                        </View>
                        {rightIcon ?(
                        <View style={rightIconContainerStyle}>
                            <Animated.Image 
                                style={[{
                                    tintColor: disabled ? '#ccc': null,
                                    resizeMode: 'contain',
                                    transform: [{ rotateZ }]
                                }, rightIconStyle ]}
                                source={rightIcon}/>
                        </View>
                            ):null}
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}