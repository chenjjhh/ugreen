import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, PanResponder
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import CountdownUtil from "../Utils/CountdownUtil";

/*
*双向滑动的Slider
 */
const thumMarTop = 7
const thumSize = 20
const initThumMarLeft = 6
const layoutMarLeftRight = 25
const initPoint1CenterX = layoutMarLeftRight + 6 + thumSize / 2
const initPoint2CenterX = Dimensions.get('window').width - initPoint1CenterX
const nerViewWidth = Dimensions.get('window').width - 56
const outViewWidth = Dimensions.get('window').width - 50

const sumIndex = 100;

var tempSlider1Value = 0
var tempSlider2Value = 0
export default class LeftRightSliderView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            slider1CenterX: initPoint1CenterX,
            slider2CenterX: initPoint2CenterX,
            slide1Enable: false,
            slide2Enable: false,
            sliderMarLeft1: initPoint1CenterX - layoutMarLeftRight - thumSize / 2,
            sliderMarLeft2: initPoint2CenterX - layoutMarLeftRight - thumSize / 2,
            slider1Value: 0,
            slider2Value: 0,
        }
        this._initSpec(props)
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        let props = this.props;
        if (nextProps.sliderMinValue !== props.sliderMinValue) {
            var sliderValue1 = nextProps.sliderMinValue
            if (parseInt(sliderValue1 < 0)) {
                sliderValue1 = 0
                this._setCicleX1(sliderValue1)
            }
        }

        if (nextProps.sliderMaxValue !== props.sliderMaxValue) {
            var sliderValue2 = nextProps.sliderMaxValue
            if (parseInt(sliderValue2 > sumIndex)) {
                sliderValue2 = sumIndex
            }
            this._setCicleX2(sliderValue2)
        }
    }

    _initSpec(nextProps) {
        setTimeout(() => {
            var sliderValue1 = nextProps.sliderMinValue
            if (parseInt(sliderValue1) < 0) {
                sliderValue1 = 0
            }
            this._setCicleX1(sliderValue1)

            var sliderValue2 = nextProps.sliderMaxValue
            if (parseInt(sliderValue2) > sumIndex) {
                sliderValue2 = sumIndex
            }
            this._setCicleX2(sliderValue2)
        }, 0);
    }


    componentWillMount() {
        this._panResponderSlide = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => {
                return true;
            },
            onStartShouldSetPanResponderCapture: (evt, gestureState) => { // 表示，是否成为事件的劫持者，如果返回是，则不会把事件传递给它的子元素
                return true;
            },
            onPanResponderGrant: (evt, gestureState) => {

                this.setState({
                    slide1Enable: this._inCircle1(gestureState.x0),
                    slide2Enable: this._inCircle2(gestureState.x0)
                });

            },
            onPanResponderMove: (evt, gestureState) => {

                if (this.state.slide1Enable) {
                    var index1 = this._getIndex1(
                        parseInt(gestureState.moveX) < initPoint1CenterX ? initPoint1CenterX : (parseInt(gestureState.moveX) > this.state.slider2CenterX - thumSize ? this.state.slider2CenterX - thumSize : parseInt(gestureState.moveX))
                    )
                    if (Math.abs(this.state.slider2Value - index1) < 10) {
                        return
                    }
                    this._setCicleX1(index1)
                }

                if (this.state.slide2Enable) {
                    var index2 = this._getIndex2(
                        parseInt(gestureState.moveX) > initPoint2CenterX ? initPoint2CenterX : parseInt(gestureState.moveX) < this.state.slider1CenterX + thumSize + 3 ? this.state.slider1CenterX + thumSize + 3 : parseInt(gestureState.moveX)
                    )
                    if (Math.abs(this.state.slider1Value - index2) < 10) {
                        return
                    }
                    this._setCicleX2(index2)
                }


            },
            onPanResponderRelease: (evt, gestureState) => { // 释放
                if (this.state.slide1Enable) {
                    if (this.props.onSlider1ValueComplete) {
                        this.props.onSlider1ValueComplete(tempSlider1Value)
                    }
                }
                if (this.state.slide2Enable) {
                    if (this.props.onSlider2ValueComplete) {
                        this.props.onSlider2ValueComplete(tempSlider2Value)
                    }
                }
                this.setState({
                    slide1Enable: false,
                    slide2Enable: false
                });
            },
            onPanResponderTerminate: (evt, gestureState) => {
            }
        });
    }

    _inCircle1(x) {
        if (x <= (this.state.slider1CenterX + (thumSize / 2)) && x >= (this.state.slider1CenterX - (thumSize / 2)))
            return true;
        else
            return false;
    }

    _inCircle2(x) {
        if (x <= (this.state.slider2CenterX + (thumSize / 2)) && x >= (this.state.slider2CenterX - (thumSize / 2)))
            return true;
        else
            return false;
    }

    render() {
        return (
            <View
                style={[
                    {
                        height: 34,
                        width: outViewWidth,
                        backgroundColor: '#F7F7F7',
                        borderRadius: 17,
                        paddingLeft: 3,
                        paddingRight: 3,
                        paddingTop: 2,
                        paddingBottom: 2,
                        alignItems: 'center',
                        justifyContent: 'center',
                    },
                    this.props.style
                ]}
                {...this._panResponderSlide.panHandlers}>
                <View
                    style={{
                        height: 30,
                        position: 'absolute',
                        width: (this.state.slider2CenterX - this.state.slider1CenterX + initThumMarLeft + thumSize),
                        borderRadius: 15,
                        backgroundColor: '#FF8748',
                        top: 2,
                        left: this.state.sliderMarLeft1 - initThumMarLeft / 2
                    }}/>
                <View
                    style={{
                        width: thumSize,
                        height: thumSize,
                        backgroundColor: '#fff',
                        borderRadius: thumSize / 2,
                        position: 'absolute',
                        top: thumMarTop,
                        left: this.state.sliderMarLeft1
                    }}/>

                <View
                    style={{
                        width: thumSize,
                        height: thumSize,
                        backgroundColor: '#fff',
                        borderRadius: thumSize / 2,
                        position: 'absolute',
                        top: thumMarTop,
                        left: this.state.sliderMarLeft2
                    }}/>
            </View>
        );
    }


    _getIndex1(x) {
        let a = (initPoint2CenterX - initPoint1CenterX) / sumIndex;
        return parseInt((x - initPoint1CenterX) / a);
    }

    _getIndex2(x) {
        let a = (initPoint2CenterX - initPoint1CenterX) / sumIndex;
        return parseInt((x - initPoint1CenterX) / a);
    }

    _setCicleX1(index) { // 设置第一个圆点档位
        if (index < 0 || index > (sumIndex - 1))
            return;
        tempSlider1Value = index
        this.setState({slider1Value: index})
        this.setState({
            slider1CenterX: (index * ((initPoint2CenterX - initPoint1CenterX) / sumIndex)) + initPoint1CenterX,
            sliderMarLeft1: ((index * ((initPoint2CenterX - initPoint1CenterX) / sumIndex)) + initPoint1CenterX) - layoutMarLeftRight - (thumSize / 2)
        });

        if (this.props.onSlider1ValueChange) {
            this.props.onSlider1ValueChange(index)
        }
    }

    _setCicleX2(index) { // 设置第二个圆点档位
        if (index < 1 || index > sumIndex)
            return;
        tempSlider2Value = index
        this.setState({slider2Value: index})
        this.setState({
            slider2CenterX: (index * ((initPoint2CenterX - initPoint1CenterX) / sumIndex)) + initPoint1CenterX,
            sliderMarLeft2: ((index * ((initPoint2CenterX - initPoint1CenterX) / sumIndex)) + initPoint1CenterX) - layoutMarLeftRight - (thumSize / 2)
        });

        if (this.props.onSlider2ValueChange) {
            this.props.onSlider2ValueChange(index)
        }
    }
}