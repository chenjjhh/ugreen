import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import CommonTextView from "./CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "./DialogBottomBtnView";
/*
*消息弹窗
 */
export default class MessageDialog extends BaseComponent {
    render() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    paddingTop: 35,
                    paddingBottom: 35,
                    with: this.mScreenWidth,
                    paddingHorizontal: 29,
                }}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>

                <CommonTextView
                    textSize={18}
                    text={this.props.title}
                    style={{
                        color: '#282A30',
                    }}/>

                <CommonTextView
                    textSize={14}
                    text={this.props.message}
                    style={[
                        {
                            color: '#969AA1',
                            width: this.mScreenWidth - 80,
                            marginTop: 35,
                            marginBottom: 35,
                            lineHeight: 25
                        }, this.props.messageStyle
                    ]}/>

                <DialogBottomBtnView
                    onLeftText={this.props.leftBtnText}
                    onLeftTextColor={this.props.leftBtnTextColor}
                    onLeftBackgroundColor={this.props.onLeftBackgroundColor}
                    onRightBackgroundColor={this.props.onRightBackgroundColor}
                    onLeftBtnClick={() => {
                        if (this.props.leftBtnText) {
                            this.props.onLeftBtnClick()
                        }
                    }}
                    onRightText={this.props.rightBtnText}
                    onRightTextColor={this.props.rightBtnTextColor}
                    onRightBtnClick={() => {
                        if (this.props.rightBtnText) {
                            this.props.onRightBtnClick()
                        }
                    }}/>

            </DialogContainerView>
        );
    }
}