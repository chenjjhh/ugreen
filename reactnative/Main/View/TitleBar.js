import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    StatusBar,
    SafeAreaView,
    NativeModules,
    Platform, TouchableOpacity,
} from 'react-native';

import BaseComponent from "../Base/BaseComponent";
import FastImage from "react-native-fast-image";

const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;


export default class TitleBar extends BaseComponent {
    constructor(props) {
        super(props);
    }

    render() {
        StatusBar.setBarStyle('dark-content');
        return (
            <View>
                <StatusBar translucent={true} backgroundColor="transparent"/>

                <View
                    style={{
                        height: 55 + statusBarHeight,
                        width: Dimensions.get('window').width,
                        backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : '#fff',
                    }}>

                    <View
                        style={[
                            {
                                //width: Dimensions.get('window').width,
                                flex: 1,
                                marginTop: 30,
                                alignItems: 'center',
                                justifyContent: 'center',
                                flexDirection: 'row',
                                marginLeft: 18,
                                marginRight: 18,
                            }, this.props.style
                        ]}>
                        {
                            this.props.titleText ?
                                <View
                                    style={{
                                        width: Dimensions.get('window').width,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        position: 'absolute',
                                    }}>
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            color: '#2F2F2F',
                                            // fontFamily: 'DIN Alternate Bold',
                                            fontSize: this.getSize(16),
                                            width: this.mScreenWidth * (3 / 5),
                                            textAlign: 'center',
                                            fontWeight:'bold'
                                        }}>
                                        {this.props.titleText}
                                    </Text>
                                </View>
                                : null
                        }

                        {
                            this.props.showLogo ?
                                <View
                                    style={{
                                        width: Dimensions.get('window').width,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        position: 'absolute',
                                    }}>
                                    <Image
                                        style={{
                                            width: this.mScreenWidth * 0.34,
                                            resizeMode: 'contain'
                                        }}
                                        source={require('../../resources/logo_black_ic.png')}/>
                                </View> : null
                        }

                        {
                            this.props.leftIconUrl ?
                                <TouchableOpacity
                                    style={{
                                        width: this.mScreenWidth * 0.07,
                                        height: this.mScreenWidth * 0.07,
                                        borderRadius: (this.mScreenWidth * 0.07) / 2,
                                        overflow: 'hidden'
                                    }}
                                    onPress={this.props.leftIconClick}>
                                    <Image
                                        style={{
                                            position:'absolute'
                                        }}
                                        source={this.props.leftIcon}/>
                                    <FastImage
                                        style={{ width: this.mScreenWidth * 0.07, height: this.mScreenWidth * 0.07 }}
                                        source={{
                                            uri: this.props.leftIconUrl,
                                            priority: FastImage.priority.normal,
                                        }}
                                        resizeMode={FastImage.resizeMode.cover}
                                    />
                                    {/*<Image*/}
                                        {/*style={{*/}
                                            {/*width: this.mScreenWidth * 0.07,*/}
                                            {/*height: this.mScreenWidth * 0.07,*/}
                                            {/*resizeMode: 'cover'*/}
                                        {/*}}*/}
                                        {/*source={{uri: this.props.leftIconUrl}}/>*/}
                                </TouchableOpacity> :
                                this.props.leftIcon && this.props.leftIconClick ?
                                    <TouchableOpacity
                                        style={{
                                            paddingRight: 20,
                                            height: '100%',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                        onPress={this.props.leftIconClick}>
                                        <Image source={this.props.leftIcon}/>
                                    </TouchableOpacity>
                                    : null
                        }


                        <View style={{flex: 1}}/>

                        {
                            this.props.rightIcon && this.props.rightIconClick ?
                                <TouchableOpacity
                                    onPress={this.props.rightIconClick}>
                                    <Image source={this.props.rightIcon}/>
                                </TouchableOpacity> : null
                        }
                        {
                            this.props.rightText && this.props.rightTextClick ?
                                <TouchableOpacity
                                    onPress={this.props.rightTextClick}>
                                    <Text
                                        numberOfLines={1}
                                        style={[{
                                            color: '#808080',
                                            fontSize: this.getSize(13),
                                            // fontFamily: 'DIN Alternate Bold'
                                        },this.props.rightTextStyle]}>
                                        {this.props.rightText}
                                    </Text>
                                </TouchableOpacity> : null
                        }


                    </View>

                </View>
            </View>
        );
    }
}