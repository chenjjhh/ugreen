import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';

export default class SwitchBtn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
        };
    }

    toggleSwitch = () => {
        // contraryMode为真时，表示相反模式开关 
        const {onValueChange, type, contraryMode} = this.props;
        const {value} = this.state;
        onValueChange && onValueChange((contraryMode ? !value : value) ? (contraryMode ? '1' : '0') : (contraryMode ? '0' : '1'), type);
    };

    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value) {
            this.setState({
                value: this.props.value
            })
        }
    }

    render() {
        const {value} = this.state;
        const {style, contraryMode} = this.props;
        return (<TouchableOpacity
            style={[styles.container, {backgroundColor: value ? (contraryMode ? '#F1F2F6' : '#18B34F') : (contraryMode ? '#18B34F' : '#F1F2F6')}, style]}
            activeOpacity={0.8}
            onPress={this.toggleSwitch}>
            <View
                style={[styles.switch, value ? (contraryMode ? styles.switchOff : styles.switchOn) : (contraryMode ? styles.switchOn : styles.switchOff)]}/>
        </TouchableOpacity>);
    }
}
const styles = StyleSheet.create(
    {
        container: {
            flexDirection: 'row',
            width: 42,
            height: 23,
            borderRadius: 15,
            backgroundColor: '#F1F2F6',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        switch: {
            width: 14,
            height: 14,
            borderRadius: 10,
        },
        switchOn: {
            backgroundColor: '#fff',
            transform: [{translateX: 20}],
        },
        switchOff: {
            backgroundColor: '#fff',
            transform: [{translateX: 5}],
        },
    });
