import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ElectricQuantityProgress from "../View/ElectricQuantityProgress";
import {strings} from "../Language/I18n";
import CommonTextView from "./CommonTextView";

/*
*添加设备-设备卡片
 */
export default class DeviceCard extends BaseComponent {
    render() {
        return (
            <View
                style={[
                    {
                        flex: 1,
                        padding: 3,
                    }, this.props.style
                ]}>
                <TouchableOpacity
                    onPress={() => {
                        this.props.onPress()
                    }}
                    style={[
                        {
                            width: (this.mScreenWidth - 62) / 3,
                            height: (this.mScreenWidth - 62) / 3,
                            borderRadius: 10,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }, this.props.style
                    ]}>

                    <Image
                        style={{
                            width: 50,
                            height: 50,
                            resizeMode: 'contain'
                        }}
                        source={this.props.imageUrl ? this.props.imageUrl : require('../../resources/greenUnion/new_popup_dev_img.png')}/>

                    <CommonTextView
                        textSize={12}
                        text={this.props.name}
                        style={{
                            fontWeight: 'bold',
                            color: '#000000',
                            marginTop: 7,
                        }}
                    />

                    <CommonTextView
                        textSize={8}
                        text={this.props.deviceTypeName}
                        style={{
                            color: '#808080',
                            marginTop: 3,
                        }}
                    />

                </TouchableOpacity>

            </View>
        );
    }

    _pointView() {
        return (
            <View
                style={{
                    width: 13,
                    height: 13,
                    borderRadius: 13 / 2,
                    backgroundColor: '#FF8748',
                    position: 'absolute',
                    top: 0,
                    right: 0
                }}/>
        )
    }
}