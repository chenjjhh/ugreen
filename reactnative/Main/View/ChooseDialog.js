import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,Modal
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import ViewHelper from "./ViewHelper";

/*
*选择列表弹窗
 */
export default class ChooseDialog extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentId: props.currentId || 0,
            show : props.show || false,
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.currentId !== this.props.currentId) {
            this.setState({
                currentId:this.props.currentId
            })
        }

        if(prevProps.show !== this.props.show) {
            this.setState({
                show:this.props.show
            })
        }
        
    }

    _checkTab = (item,index) =>{
        const {_checkTab} = this.props;
        this.setState({
            currentId:item?.id && index,
        },()=> _checkTab(item))
    }

    _onCancel = ()=>{
        this.props._onHide()
    }

    render() {
        const { data, style, currentId, title } = this.props;
        return (
            <Modal
                statusBarTranslucent={true}
                animationType={"fade"}
                transparent={true}
                visible={this.state.show}
                onRequestClose={() => {
                    this._onCancel()
                }}>
                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.22)',
                }}>
                    <TouchableOpacity onPress={()=>{this._onCancel()}} style={{flex:1,}}></TouchableOpacity>
                    <View style={[{width:this.mScreenWidth,justifyContent:'center',alignItems:'center',backgroundColor:'#ffffff',borderTopRightRadius:10,borderTopLeftRadius:10},style]}>
                        <View style={{height:68,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'#2F2F2F',fontSize: this.getSize(16),fontWeight:'bold'}}>{title}</Text>
                        </View>
                        <View style={{width:this.mScreenWidth-66,justifyContent:'center',alignItems:'center',}}>
                            {data?.map((item,index)=>(
                                <TouchableOpacity style={{flexDirection:'row',width:'100%',height:64,justifyContent:'space-between',alignItems:'center',borderBottomWidth:0.5,borderBottomColor:'#EBEBEB'}} key={index} onPress={()=>this._checkTab(item,index)}>
                                    <Text style={{color:'#2F2F2F',fontSize: this.getSize(14),fontWeight:'bold'}}>{item?.text || ''}</Text>
                                    { <Image style={{width:21,height:21}}  source={item.id === currentId ? require('../../resources/greenUnion/sel_ic.png'): require('../../resources/greenUnion/unsel_ic.png')}/>}
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                </View>

            </Modal>

            
        );
    }
}