import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";

/*
*选项卡
 */
export default class TabView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentId: props.currentId || 0,
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.currentId !== this.props.currentId) {
            this.setState({
                currentId:this.props.currentId
            })
        }
    }

    _checkTab = (item,index) =>{
    const {_checkTab} = this.props;
    this.setState({
        currentId:item?.id && index,
    },()=> _checkTab(item))
    }

    render() {
        const { data, style, itemStyle, containerStyle } = this.props;
        const { currentId } = this.state;
        return (
            <View style={[{width:this.mScreenWidth,justifyContent:'center',alignItems:'center',marginBottom:28},containerStyle]}>
                <View style={[{width:'100%',flexDirection:'row',justifyContent:'space-around',alignItems:'center',paddingHorizontal:36,height:29},style]}>
                    {data?.map((item,index)=>(
                        <TouchableOpacity  key={index} style={[{paddingHorizontal:15},itemStyle]} onPress={()=>this._checkTab(item,index)}>
                            <Text style={{color:currentId === item.id ?'#18B34F':'#808080',fontSize: this.getSize(12),}}>{item?.text || ''}</Text>
                            {item?.isNoRead ? <View style={{width:6,height:6,borderRadius:4,backgroundColor:'red',position:'absolute',top:0,right:10}}></View> : null} 
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        );
    }
}