import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";

/*
*通用TextView,方便统一管理
 */
export default class CommonTextView extends BaseComponent {
    render() {
        return (
            <Text
                onPress={this.props.onPress}
                style={[{
                    fontSize: this.getSize(this.props.textSize),
                    // fontFamily:'DIN Alternate Bold'
                }
                    , this.props.style]}>
                {this.props.text}
            </Text>
        );
    }
}