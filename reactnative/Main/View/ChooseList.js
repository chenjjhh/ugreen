import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import CommonTextView from '../View/CommonTextView'

/*
*选择列表
 */
export default class ChooseList extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    render() {
        const { leftIcon, rightIcon, text, onPress } = this.props
        return (
            <TouchableOpacity
                onPress={onPress && onPress()}
                style={[
                    {
                        height: 50,
                        width: this.mScreenWidth * 0.73,
                        backgroundColor: '#F7F8FB',
                        borderRadius: 27,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        overflow: 'hidden',
                    }, this.props.style
                ]}>
                <View style={{flexDirection:'row'}}>
                    {leftIcon ?(<Image
                        style={[{
                            resizeMode: 'contain',
                            paddingHorizontal: 30
                        }, ]}
                        source={leftIcon}/>):null}
                    <CommonTextView
                        textSize={14}
                        text={text}
                        style={{
                            color: '#404040',
                            fontWeight: 'normal',
                        }}/>
                </View>
                {rightIcon ?(<Image
                    style={[{
                        resizeMode: 'contain',
                        paddingHorizontal: 25,
                    }, ]}
                    source={rightIcon}/>):null}
                

            </TouchableOpacity>
        );
    }
}