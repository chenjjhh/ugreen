import React, { Component } from 'react';
import { View, StyleSheet, PanResponder, Platform, Text, Image } from 'react-native';
import Svg, { Circle, Path } from 'react-native-svg';
import PropTypes from "prop-types";
import BaseComponent from "../../Base/BaseComponent";

let tempMax = 0;
let tempMin = 0;

let flagMax = false;
let flagMin = false;
export default class CircleView extends BaseComponent {
    static propTypes = {
      height: PropTypes.number,
      width: PropTypes.number,
      r: PropTypes.number,
      angle: PropTypes.number,
      outArcColor: PropTypes.object,
      progressvalue: PropTypes.object,
      tabColor: PropTypes.object,
      tabStrokeColor: PropTypes.object,
      strokeWidth: PropTypes.number,
      value: PropTypes.number,
      min: PropTypes.number,
      max: PropTypes.number,
      tabR: PropTypes.number,
      step: PropTypes.number,
      tabStrokeWidth: PropTypes.number,
      valueChange: PropTypes.func,
      renderCenterView: PropTypes.func,
      complete: PropTypes.func,
      enTouch: PropTypes.boolean
    };

    static defaultProps = {
      width: 300,
      height: 300,
      r: 100,
      angle: 60,
      outArcColor: 'white',
      strokeWidth: 10,
      value: 20,
      min: 10,
      max: 70,
      progressvalue: '#ED8D1B',
      tabR: 15,
      tabColor: '#EFE526',
      tabStrokeWidth: 5,
      tabStrokeColor: '#86BA38',
      valueChange: () => {
      },
      complete: () => {
      },
      renderCenterView: () => {
      },
      step: 1,
      enTouch: true
    };

    constructor(props) {
      super(props);
      this.state = {
        temp: this.props.value,
        maxFlag: false,
        minFlag: false
      };
      this.iniPanResponder();
    }

    iniPanResponder() {
      this.parseToDeg = this.parseToDeg.bind(this);
      this._panResponder = PanResponder.create({
        // 要求成为响应者：
        onStartShouldSetPanResponder: () => true,
        onStartShouldSetPanResponderCapture: () => true,
        onMoveShouldSetPanResponder: () => true,
        onMoveShouldSetPanResponderCapture: () => true,
        onPanResponderGrant: (evt) => {
          // 开始手势操作。给用户一些视觉反馈，让他们知道发生了什么事情！
          if (this.props.enTouch) {
            this.lastTemper = this.state.temp;
            const x = evt.nativeEvent.locationX;
            const y = evt.nativeEvent.locationY;
            this.parseToDeg(x, y);
          }
        },
        onPanResponderMove: (evt, gestureState) => {
          if (this.props.enTouch) {
            let x = evt.nativeEvent.locationX;
            let y = evt.nativeEvent.locationY;
            if (Platform.OS === 'android') {
              x = evt.nativeEvent.locationX + gestureState.dx;
              y = evt.nativeEvent.locationY + gestureState.dy;
            }
            this.parseToDeg(x, y);
          }
        },
        onPanResponderTerminationRequest: () => true,
        onPanResponderRelease: () => {
          if (this.props.enTouch) this.props.complete(this.state.temp);

          this.setState({
            maxFlag: false,
            minFlag: false
          });
          flagMax = false;
          flagMin = false;
        },
        // 另一个组件已经成为了新的响应者，所以当前手势将被取消。
        onPanResponderTerminate: () => {
        },
        // 返回一个布尔值，决定当前组件是否应该阻止原生组件成为JS响应者
        // 默认返回true。目前暂时只支持android。
        onShouldBlockNativeResponder: () => true
      });
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.value != this.state.temp) {
        this.setState({ temp: nextProps.value });
      }
    }

    parseToDeg(x, y) {


      const cx = this.props.width / 2;
      const cy = this.props.height / 2;
      let deg;
      let temp;
      if (x >= cx && y <= cy) { // 1
        flagMax = false;
        if (flagMin)
          return;

        deg = Math.atan((cy - y) / (x - cx)) * 180 / Math.PI;
        temp =
                (270 - deg - this.props.angle / 2) /
                (360 - this.props.angle) *
                (this.props.max - this.props.min) +
                this.props.min;
      } else if (x >= cx && y >= cy) { // 2
        if (flagMin)
          return;

        deg = Math.atan((cy - y) / (cx - x)) * 180 / Math.PI;
        if (parseInt(deg) > 70)
          return;
        temp =
                (270 + deg - this.props.angle / 2) /
                (360 - this.props.angle) *
                (this.props.max - this.props.min) +
                this.props.min;
        flagMax = true;
      } else if (x <= cx && y <= cy) { // 4
        flagMin = false;
        if (flagMax)
          return;

        deg = Math.atan((x - cx) / (y - cy)) * 180 / Math.PI;
        temp =
                (180 - this.props.angle / 2 - deg) /
                (360 - this.props.angle) *
                (this.props.max - this.props.min) +
                this.props.min;
      } else if (x <= cx && y >= cy) { // 3
        if (flagMax)
          return;

        deg = Math.atan((cx - x) / (y - cy)) * 180 / Math.PI;
        if (parseInt(deg) < 20)
          return;
        if (deg < this.props.angle / 2) {
          deg = this.props.angle / 2;
        }
        temp =
                (deg - this.props.angle / 2) /
                (360 - this.props.angle) *
                (this.props.max - this.props.min) +
                this.props.min;

        flagMin = true;
      }

      if (temp <= this.props.min) {
        temp = this.props.min;
      }
      if (temp >= this.props.max) {
        temp = this.props.max;
      }


      temp = this.getTemps(temp);

      // this.setState({
      //     maxFlag: temp==this.props.max,
      //     minFlag: temp==this.props.min
      // });
      // this.state.maxFlag=(temp==this.props.max)
      // this.state.minFlag=(temp==this.props.min)
      // if(this.state.minFlag||this.state.maxFlag)
      //     return

      this.setState({
        temp
      });
      this.props.valueChange(this.state.temp);


    }

    getTemps(tmps) {
      const k = parseInt((tmps - this.props.min) / this.props.step, 10);
      const k1 = this.props.min + this.props.step * k;
      const k2 = this.props.min + this.props.step * (k + 1);
      if (Math.abs(k1 - tmps) > Math.abs(k2 - tmps)) return k2;
      return k1;
    }


    render() {
      return (
        <View pointerEvents={'box-only'} {...this._panResponder.panHandlers}>
          {this._renderCircleSvg()}
          <View
            style={{
              position: 'relative',
              top: -this.props.height / 2 - this.props.r,
              left: this.props.width / 2 - this.props.r,
              flex: 1
            }}>
            {this.props.renderCenterView(this.state.temp)}
          </View>
        </View>
      );
    }

    _circlerate() {
      let rate = parseInt(
        (this.state.temp - this.props.min) *
            100 /
            (this.props.max - this.props.min),
        10
      );
      if (rate < 0) {
        rate = 0;
      } else if (rate > 100) {
        rate = 100;
      }
      return rate;
    }

    _renderCircleSvg() {
      const cx = this.props.width / 2;
      const cy = this.props.height / 2;
      const prad = this.props.angle / 2 * (Math.PI / 180);
      const startX = -(Math.sin(prad) * this.props.r) + cx;
      const startY = cy + Math.cos(prad) * this.props.r; // // 最外层的圆弧配置
      const endX = Math.sin(prad) * this.props.r + cx;
      const endY = cy + Math.cos(prad) * this.props.r;

      // 计算进度点
      const progress = parseInt(
        this._circlerate() * (360 - this.props.angle) / 100,
        10
      );
        // 根据象限做处理 苦苦苦 高中数学全忘了，参考辅助线
      const t = progress + this.props.angle / 2;
      const progressX = cx - Math.sin(t * (Math.PI / 180)) * this.props.r;
      const progressY = cy + Math.cos(t * (Math.PI / 180)) * this.props.r;

      const outDescriptions = [
        'M',
        startX,
        startY,
        'A',
        this.props.r,
        this.props.r,
        0,
        t >= 180 + this.props.angle / 2 ? 1 : 0,
        1,
        progressX,
        progressY
      ].join(' ');

      const descriptions = [
        'M',
        startX,
        startY,
        'A',
        this.props.r,
        this.props.r,
        0,
        1,
        1,
        endX,
        endY
      ].join(' ');

      const progressdescription = [
        'M',
        startX,
        startY,
        'A',
        this.props.r,
        this.props.r,
        0,
        t >= 180 + this.props.angle / 2 ? 1 : 0,
        1,
        progressX,
        progressY
      ].join(' ');
      return (
        <Svg
          height={this.props.height}
          width={this.props.width}
          style={styles.svg}>
          {/* <Path */}
          {/* d={outDescriptions} */}
          {/* fill="none" */}
          {/* stroke={'rgba(100,154,255,0.55)'} */}
          {/* strokeWidth={this.props.strokeWidth + 20}/> */}

          {/* <View */}
          {/* style={{ */}
          {/* position: 'absolute', */}
          {/* width: this.props.width, */}
          {/* height: this.props.height, */}
          {/* alignItems:'center', */}
          {/* justifyContent:'center' */}
          {/* }}> */}
          {/* <View */}
          {/* style={{ */}
          {/* width: this.props.r*2+this.props.strokeWidth+1, */}
          {/* height: this.props.r*2+this.props.strokeWidth+1, */}
          {/* backgroundColor: '#fff', */}
          {/* borderRadius:this.props.r+this.props.strokeWidth+1, */}
          {/* position: 'absolute', */}
          {/* }}/> */}

          {/* <View */}
          {/* style={{ */}
          {/* width: this.props.r*2+this.props.strokeWidth, */}
          {/* height: this.props.r*2+this.props.strokeWidth, */}
          {/* backgroundColor: this.props.outArcColor, */}
          {/* borderRadius:this.props.r+this.props.strokeWidth */}
          {/* }}/> */}
          {/* </View> */}

          <Path
            d={descriptions}
            fill="none"
            stroke={'#fff'}
            strokeWidth={this.props.strokeWidth + 1}/>

          <Path
            d={descriptions}
            fill="none"
            stroke={this.props.outArcColor}
            strokeWidth={this.props.strokeWidth}/>
          {/* <Path */}
          {/* d={progressdescription} */}
          {/* fill="none" */}
          {/* stroke={this.props.progressvalue} */}
          {/* strokeWidth={this.props.strokeWidth} */}
          {/* strokeOpacity={0.2}/> */}

          {/* <Circle */}
          {/* cx={progressX} */}
          {/* cy={progressY} */}
          {/* r={this.props.tabR} */}
          {/* stroke={this.props.tabStrokeColor} */}
          {/* strokeWidth={this.props.tabStrokeWidth} */}
          {/* fill={this.props.tabColor}/> */}

          {/* <Circle */}
          {/* cx={cx} */}
          {/* cy={cy} */}
          {/* r={((((this.mScreenWidth * (2 / 3)) / 2) + 3) / 2)} */}
          {/* stroke={'#fff'} */}
          {/* fill={'#fff'}/> */}
        </Svg>
      );
    }
}

const styles = StyleSheet.create({
  svg: {}
});