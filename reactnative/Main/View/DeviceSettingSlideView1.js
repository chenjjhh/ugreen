import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import React from "react";
import DialogBottomBtnView from "./DialogBottomBtnView";
import {View, TouchableOpacity} from "react-native";
import Slider from "./Slider";
import CommonTextView from "./CommonTextView";
import {strings} from "../Language/I18n";
import ViewHelper from "./ViewHelper";
import SwitchView from "./SwitchView";

/*
*设备设置页-slider弹窗
 */
var tempSliderValue = 0
export default class DeviceSettingSlideView1 extends BaseComponent {
    render() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    paddingTop: 22,
                }}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>

                {this._titleView()}
                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 20,
                    }}>
                    {this._sliderView()}
                    {this._alwaysSwitchView()}
                </View>
                {/*{(this.props.alwaysText && this.props.onCheck) ? this._alwaysOnView() : null}*/}
                <View
                    style={{
                        height: 26,
                        width: this.mScreenWidth
                    }}/>
                {this._btnView()}

            </DialogContainerView>
        );
    }

    _alwaysOnView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.mScreenWidth - 50,
                    marginTop: 37
                }}>
                <CommonTextView
                    textSize={16}
                    style={{
                        color: '#282A30',
                    }}
                    text={this.props.alwaysText}/>

                {ViewHelper.getFlexView()}

                <SwitchView
                    isCheckColor={this.props.isCheckColor ? this.props.isCheckColor : '#FF8748'}
                    isCheck={this.props.isCheck}
                    onCheck={this.props.onCheck}/>
            </View>
        )
    }

    _titleView() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: 25,
                    paddingRight: 25,
                    width: this.mScreenWidth
                }}>
                <CommonTextView
                    textSize={20}
                    style={{
                        color: '#000000',
                    }}
                    text={this.props.title}/>


                <CommonTextView
                    textSize={20}
                    style={{
                        color: '#FF8748',
                        marginTop: 17
                    }}
                    text={this._getTruthSliderValue() + (this.props.valueUnit ? this.props.valueUnit : '')}/>

            </View>
        )
    }

    _sliderView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 50 - 61,
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 50 - 61,
                        height: 34,
                        position: 'absolute',
                        backgroundColor: '#F7F7F7',
                        borderRadius: 17,
                        paddingLeft: 3,
                        paddingRight: 3,
                        paddingTop: 2,
                        paddingBottom: 2
                    }}>
                    <View
                        style={{
                            width: 28 + ((this.mScreenWidth - 56 - 28 - 61) * (this._getTruthSliderValue() - this.props.minimumValue) / (this.props.maximumValue - this.props.minimumValue)),
                            height: 30,
                            borderRadius: 17,
                            backgroundColor: '#FF8748',
                        }}/>
                </View>

                <Slider
                    value={this._getTruthSliderValue()}
                    style={{
                        width: this.mScreenWidth - 50 - 6 - 8 - 61,
                        height: 34
                    }}
                    trackStyle={{
                        height: 34,
                        borderRadius: 17,
                        backgroundColor: 'rgba(0,0,0,0)'
                    }}
                    //thumbImage={require('../resources/icon_thumb.png')}
                    thumbStyle={{
                        width: 20,
                        height: 20
                    }}
                    step={this.props.step}
                    minimumValue={this.props.minimumValue}
                    maximumValue={this.props.maximumValue}
                    minimumTrackTintColor="rgba(0,0,0,0)"
                    maximumTrackTintColor="rgba(0,0,0,0)"
                    thumbTintColor="#fff"
                    onValueChange={(value) => {
                        tempSliderValue = value;
                        if (this.props.onValueChange) {
                            this.props.onValueChange(value)
                        }
                    }}
                    onSlidingComplete={() => {
                        if (this.props.onSlidingComplete) {
                            this.props.onSlidingComplete(tempSliderValue)
                        }
                    }}
                />
            </View>
        )
    }

    _alwaysSwitchView() {
        return (
            <TouchableOpacity
                onPress={this.props.onCheck}
                style={{
                    backgroundColor: this.props.isCheck ? '#FF8748' : '#F7F7F7',
                    height: 34,
                    width: 52,
                    borderRadius: 34 / 2,
                    marginLeft: 9,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: this.props.isCheck ? '#fff' : '#B5BAC5',
                        textAlign: 'center'
                    }}
                    text={'N/O'}/>
            </TouchableOpacity>
        )
    }

    _btnView() {
        return (
            <DialogBottomBtnView
                onLeftText={this.props.leftBtnText}
                onLeftTextColor={this.props.leftBtnTextColor}
                onLeftBtnClick={() => {
                    if (this.props.leftBtnText) {
                        this.props.onLeftBtnClick()
                    }
                }}
                onRightText={this.props.rightBtnText}
                onRightTextColor={this.props.rightBtnTextColor}
                onRightBtnClick={() => {
                    if (this.props.rightBtnText) {
                        this.props.onRightBtnClick()
                    }
                }}/>
        )
    }

    _getTruthSliderValue() {
        var sliderValue = this.props.sliderValue
        if (parseFloat(this.props.sliderValue) < this.props.minimumValue) {
            sliderValue = this.props.minimumValue
        } else if (parseFloat(this.props.sliderValue) > this.props.maximumValue) {
            sliderValue = this.props.maximumValue
        }
        return sliderValue
    }
}