import Swiper from 'react-native-swiper'
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";


export default class MainBannerView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            bannerWidth: this.mScreenWidth - 36
        }
    }


    render() {
        return (
            <View
                style={{
                    height: this.props.bannerHeight + 20,
                }}>
                <Swiper
                    height={this.props.bannerHeight ? this.props.bannerHeight : 120}
                    width={this.state.bannerWidth}
                    horizontal={true}
                    autoplay={true}
                    loop={true}
                    paginationStyle={{bottom: 0}}
                    showsPagination={true}
                    activeDot={<View style={{    //选中的圆点样式
                        backgroundColor: '#000',
                        width: 8,
                        height: 8,
                        borderRadius: 4,
                        marginLeft: 4,
                        marginRight: 4,
                    }}/>}
                    dot={<View style={{           //未选中的圆点样式
                        backgroundColor: '#D8D8D8',
                        width: 8,
                        height: 8,
                        borderRadius: 4,
                        marginLeft: 4,
                        marginRight: 4,
                    }}/>}
                >
                    {
                        this.props.bannerData && this.props.bannerData.length > 0 ?
                            this.props.bannerData.map((item, index) => {
                                return (this._bannerItemView(item, index))
                            }) : null
                    }

                </Swiper>
            </View>
        );
    }

    _bannerItemView(item, index) {
        return (
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                    if (this.props.onItemClick) {
                        this.props.onItemClick(index)
                    }
                }}
                key={index}
                style={{
                    width: this.state.bannerWidth,
                    height: this.props.bannerHeight,
                    borderRadius: 10,
                    overflow: 'hidden'
                }}>
                {/*<Image*/}
                {/*style={{*/}
                {/*width: this.mScreenWidth,*/}
                {/*height: this.state.bannerHeight,*/}
                {/*resizeMode: 'stretch',*/}
                {/*position: 'absolute'*/}
                {/*}} source={require('../resources/ic_default_image.png')}/>*/}
                <Image
                    style={{
                        width: this.state.bannerWidth,
                        height: this.props.bannerHeight,
                        resizeMode: 'stretch'
                    }} source={{uri: item.imgUrl}}/>
            </TouchableOpacity>
        )
    }
}