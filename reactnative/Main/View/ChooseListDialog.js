import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,Modal
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import ViewHelper from "./ViewHelper";

/*
*选择列表弹窗
 */
export default class ChooseListDialog extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            currentId: props.currentId || 0,
            show : props.show || false,
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.currentId !== this.props.currentId) {
            this.setState({
                currentId:this.props.currentId
            })
        }

        if(prevProps.show !== this.props.show) {
            this.setState({
                show:this.props.show
            })
        }
        
    }

    _checkTab = (item,index) =>{
        const {_checkTab} = this.props;
        this.setState({
            currentId:item?.id && index,
        },()=> _checkTab(item))
    }

    _onCancel = ()=>{
        this.props._onHide()
    }

    render() {
        const { data, style } = this.props;
        return (
            <Modal
                statusBarTranslucent={true}
                animationType={"fade"}
                transparent={true}
                visible={this.state.show}
                onRequestClose={() => {

                }}>

                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.22)',
                }}>
                    {ViewHelper.getFlexView()}
                    <View style={[{width:this.mScreenWidth,justifyContent:'center',alignItems:'center',pqddingBottom:28,backgroundColor:'#ffffff',borderTopRightRadius:10,borderTopLeftRadius:10},style]}>
                        <View style={{width:this.mScreenWidth-40,justifyContent:'center',alignItems:'center',}}>
                            {data?.map((item,index)=>(
                                <TouchableOpacity style={{width:'100%',height:65,justifyContent:'center',alignItems:'center',borderBottomWidth:0.5,borderBottomColor:'#EBEBEB'}} key={index} onPress={()=>this._checkTab(item,index)}>
                                    <Text style={{color:'#2F2F2F',fontSize: this.getSize(14),fontWeight:'bold'}}>{item?.text || ''}</Text>
                                </TouchableOpacity>
                            ))}
                        </View>
                        <TouchableOpacity style={{width:'100%',height:65,justifyContent:'center',alignItems:'center',borderBottomWidth:0.5,borderBottomColor:'#EBEBEB'}} onPress={()=>{this._onCancel()}}>
                            <Text style={{color:'#808080',fontSize: this.getSize(14),fontWeight:'bold'}}>{strings('cancel')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </Modal>

            
        );
    }
}