import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import CommonTextView from "./CommonTextView";

/*
*电量进度条
 */
export default class ElectricQuantityProgress extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            progressWidth: this.mScreenWidth * 0.48
        }
    }

    render() {
        var value = (parseInt(this.props.value) < 3 && parseInt(this.props.value) != 0) ? 3 : parseInt(this.props.value)
        return (
            <View
                style={[
                    {
                        flexDirection: 'row',
                        alignItems: 'center',
                    }, this.props.style
                ]}>

                <View>
                    <View
                        style={{
                            height: 6,
                            width: this.state.progressWidth,
                            borderRadius: 3,
                            backgroundColor: '#EFEFEF'
                        }}/>
                    {
                        this.props.enable ?
                            <View
                                style={{
                                    height: 6,
                                    width: this.state.progressWidth * ((parseInt(value) > 100 ? 100 : value) / 100),
                                    borderRadius: 3,
                                    backgroundColor: this.props.enable ? '#18B34F' : '#E8E8E8',
                                    position: 'absolute'
                                }}/> : null
                    }

                </View>


                <CommonTextView
                    textSize={10}
                    text={this.props.enable ? this.props.value + '%' : '- -'}
                    style={{
                        color: this.props.enable ? '#18B34F' : '#E8E8E8',
                        marginLeft: 8,
                        marginBottom:5
                    }}
                />

            </View>
        );
    }
}