// 输入框组件
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";

export default class TextInputComponent extends BaseComponent{
    render(){
        const {type, title, style, placeholder, onChangeText,titleTipsIcon,rightIcon,titleTipsIcOnClick,rightIconClick, value, multiline, containerStyle, inputBoxStyle} = this.props;
        return(
            <View style={[{width:this.mScreenWidth-40,borderRadius:10,overflow:'hidden',backgroundColor:'#fff',paddingHorizontal:20,paddingBottom:20},containerStyle]}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Text style={{lineHeight: 50, color: '#404040',fontWeight: 'normal',fontSize:16}}>{title}</Text>
                    {titleTipsIcon ? <TouchableOpacity onPress={()=>{titleTipsIcOnClick?.()}}><Image style={{marginHorizontal:5}} source={titleTipsIcon}/></TouchableOpacity> : null}
                </View>
                <View style={[{flexDirection:'row',alignItems:'center',justifyContent:'space-between',backgroundColor:'#F7F8FB',borderRadius:10,overflow:'hidden'},inputBoxStyle]}>
                    <TextInput
                        style={[
                            {
                                textAlign: 'left',
                                paddingHorizontal:15,
                                fontSize: this.getSize(14),
                                color: '#404040',
                                backgroundColor: '#F7F8FB',
                                borderRadius:10,
                                flex:1
                            },style
                        ]}
                        onChangeText={(text) => {
                            onChangeText && onChangeText(type, text)
                        }}
                        defaultValue={value}
                        placeholderTextColor={'#C4C6CD'}
                        placeholder={placeholder}
                        multiline={multiline || false}
                        textAlignVertical={multiline ? "top" : 'center'}//Android设备默认内容垂直居中
                        />
                        {rightIcon && <TouchableOpacity style={{backgroundColor: '#F7F8FB',marginHorizontal:15}} onPress={()=>{rightIconClick && rightIconClick()}}>
                            <Image style={{width:18,height:18}} source={rightIcon}/>
                        </TouchableOpacity>}
                </View>     
            </View>
        )
    }
}