import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";

/*
*通用按钮
 */
export default class CommonBtnView extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <TouchableOpacity
                activeOpacity={this.props.clickAble ? 0.5 : 1}
                onPress={() => {
                    if(this.props.clickAble){
                        this._onPressEvent()
                    }
                }}
                style={[
                    {
                        width: this.viewCommonWith,
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: this.props.clickAble ? '#18B34F' : 'rgba(16, 179, 79, 0.3)',
                        height: 50,
                        borderRadius: this.viewCommonWith / 2
                    }, this.props.style
                ]}>
                <Text
                    style={[
                        {
                            color: this.props.clickAble ? '#fff' : 'rgba(255, 255, 255, 0.3)',
                            fontSize: this.getSize(16),
                            fontFamily:'DIN Alternate Bold'
                        }, this.props.textStyle
                    ]}>
                    {this.props.btnText}
                </Text>
            </TouchableOpacity>
        );
    }

    _onPressEvent() {
        if (this.props.clickAble && this.props.onPress) {
            this.props.onPress()
        }
    }
}