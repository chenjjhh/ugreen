import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";

/*
*星星评分
 */
export default class StarRatingView extends BaseComponent {
    render() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginTop: 25,
                    width: this.mScreenWidth - 76,
                }}>
                {
                    this.props.starScore >= 0 ?
                        Array.apply(null, Array(10)).map((_, index) => {
                            return (this._starItemView(index < this.props.starScore, index))
                        }) : null
                }

            </View>
        );
    }

    _starItemView(isSelect, index) {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (this.props.onSelect) {
                        this.props.onSelect(index)
                    }
                }}
                style={{
                    flex: 1
                }}
                key={index}>
                <Image
                    style={{
                        width: this.mScreenWidth * 0.06,
                        height: this.mScreenWidth * 0.06,
                        resizeMode: 'contain'
                    }}
                    source={isSelect ? require('../../resources/rank_sel_ic.png') : require('../../resources/rank_ic.png')}
                />
            </TouchableOpacity>
        )
    }
}