import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "./DialogContainerView";
import CommonTextView from "./CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "./DialogBottomBtnView";
/*
*选择弹窗（语言选择、温度单位选择）
 */

export default class SelectDialog extends BaseComponent {
    render() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    paddingTop: 30,
                    paddingLeft: 40,
                    paddingRight: 40,
                    paddingBottom: 0
                }}
                overViewClick={() => {
                    if (this.props.overViewClick) {
                        this.props.overViewClick()
                    }
                }}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}
                modalVisible={this.props.modalVisible}>
                {
                    this.props.title ?
                        <CommonTextView
                            textSize={20}
                            style={{
                                color: '#000',
                                marginBottom: 50
                            }}
                            text={this.props.title}/>
                        : null
                }
                {
                    this.props.selectArray.map((item, index) => {
                        return (this._selectItemView(item, index))
                    })
                }

                {this._cancelView()}
            </DialogContainerView>
        );
    }

    _selectItemView(item, index) {
        return (
            <TouchableOpacity
                key={index}
                onPress={() => {
                    if (this.props.onItemClick) {
                        this.props.onItemClick(index)
                    }
                }}>
                <CommonTextView
                    key={index}
                    textSize={18}
                    style={{
                        color: '#000',
                        width: this.mScreenWidth,
                        textAlign: 'center',
                        //height: 50,
                        marginBottom: 30
                    }}
                    text={item}
                />
            </TouchableOpacity>
        )
    }

    _cancelView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                }}>
                <View
                    style={{
                        backgroundColor: '#rgba(0,0,0,0.2)',
                        width: this.mScreenWidth,
                        height: 1 / PixelRatio.get()
                    }}/>
                <TouchableOpacity
                    onPress={() => {
                        if (this.props.onCancelClick) {
                            this.props.onCancelClick()
                        }
                    }}>
                    <CommonTextView
                        textSize={18}
                        style={{
                            color: '#000',
                            width: this.mScreenWidth,
                            textAlign: 'center',
                            paddingTop: 15,
                            paddingBottom: 15
                        }}
                        text={strings('cancel')}
                    />
                </TouchableOpacity>
            </View>
        )
    }
}