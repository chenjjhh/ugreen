import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";
import {strings, setLanguage} from '../Language/I18n';

/*
*弹窗容器
 */
export default class DialogContainerView extends BaseComponent {
    render() {
        return (
            <Modal
                statusBarTranslucent={true}
                animationType={"fade"}
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => {
                    if (this.props.onRequestClose) {
                        this.props.onRequestClose()
                    }
                }}>

                <TouchableOpacity
                    activeOpacity={this.props.overViewClick ? 0.5 : 1}
                    onPress={() => {
                        if (this.props.overViewClick) {
                            this.props.overViewClick()
                        }
                    }}
                    style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.22)',
                    }}>
                    {ViewHelper.getFlexView()}
                    <TouchableOpacity
                        activeOpacity={1}
                        style={[{
                            width: this.mScreenWidth,
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            backgroundColor: '#fff',
                        }, this.props.cardStyle]}>
                        {this.props.children}
                    </TouchableOpacity>
                </TouchableOpacity>

            </Modal>
        );
    }
}