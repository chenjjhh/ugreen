import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio, Keyboard
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import ViewHelper from "./ViewHelper";
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "./CommonTextView";
import DialogBottomBtnView from "./DialogBottomBtnView";

/*
*接收设备分享弹窗
 */
export default class AcceptDeviceShareDialog extends BaseComponent {

    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight: 0,
        }
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (event) => {
            this.setState({keyboardHeight: event.endCoordinates.height})
        });
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (event) => {
            this.setState({keyboardHeight: 0})
        });
    }

    componentWillUnmount() {
        this.keyboardDidShowListener && this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener && this.keyboardDidHideListener.remove()
    }

    render() {
        return (
            <Modal
                statusBarTranslucent={true}
                animationType={"fade"}
                transparent={true}
                visible={this.props.modalVisible}
                onRequestClose={() => {

                }}>

                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.22)',
                }}>
                    {ViewHelper.getFlexView()}

                    {this._bottomView()}

                    {this.state.keyboardHeight == 0 ? null :
                        <View
                            style={{
                                width: this.mScreenWidth,
                                height: this.state.keyboardHeight,
                            }}/>}
                </View>

            </Modal>
        );
    }

    _bottomView() {
        return (
            <View>
                {this._dialogView()}
            </View>
        )
    }

    _dialogView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    backgroundColor: '#fff',
                    paddingVertical:20,
                    paddingHorizontal:20,
                }}>
                {this._dialogTopView()}
                <DialogBottomBtnView
                    clickAble={true}
                    onLeftText={strings('later')}
                    onLeftTextColor={'#808080'}
                    onLeftBtnClick={() => {
                        this.props.onLeftBtnClick()
                    }}
                    onRightText={strings('accept')}
                    onRightTextColor={'#FFFFFF'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    onRightBackgroundColor={'#18B34F'}
                    onRightBtnClick={() => {
                        this.props.onRightBtnClick()
                    }}/>
            </View>
        )
    }

    _dialogTopView() {
        return (
            <View
                style={{
                    marginBottom: 31,
                }}>
                <CommonTextView
                    textSize={22}
                    text={strings('new_device')}
                    style={{
                        color: '#000000',
                    }}
                />
                <Text numberOfLines={1}>
                        <Text style={{fontSize: this.getSize(14),
                            color: '#808080',
                            marginTop: 10,
                            fontFamily: 'DIN Alternate Bold'}}>
                            {strings('用户')}
                            <Text style={{
                            color: '#18B34F',
                        }}>
                        {this.props.userName}
                        </Text>
                    </Text>
                    <Text
                        style={{
                            fontSize: this.getSize(14),
                            color: '#969AA1',
                            fontFamily: 'DIN Alternate Bold'
                        }}>
                        {strings('share_device_with_you')}
                    </Text>
                </Text>
                <View style={{ alignItems:'center',width:'100%'}}>
                    <View style={{marginVertical:10}}>
                        <Text style={{fontSize:13,color:'#C4C6CD',}}>{strings('接收设备提示')}</Text>
                    </View>
                    {this._shareIconView()}
                    {this._updateDeviceNameView()}
                </View>
            </View>
        )
    }

    _updateDeviceNameView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    height: 45,
                    width: this.mScreenWidth-40,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius:40,
                    backgroundColor: '#EBEEF0',
                    overflow:'hidden'
                }}>
                <TextInput
                    style={[
                        {
                            height: 45,
                            textAlign: 'left',
                            paddingHorizontal:15,
                            fontSize: this.getSize(14),
                            color: '#404040',
                            backgroundColor: '#EBEEF0',
                            flex:1,
                        }, this.props.inputStyle
                    ]}
                    onChangeText={(text) => {
                        this.props.onChangeText(text)
                    }}
                    defaultValue={this.props.deviceName}
                    placeholderTextColor={this.props.placeholderTextColor}
                    placeholder={strings('请输入设备名称')}/>

                {
                    this.props.deviceName  && this.props.allowDeleteAll?
                        <TouchableOpacity
                            style={{
                                padding: 10,
                            }}
                            onPress={() => {
                                this.props.clearNameTextInput()
                                this.dissmissKeyboard()
                            }}>
                            <Image
                                style={{width:20,height:20}}
                                source={require('../../resources/delete_all_ic.png')}/>
                        </TouchableOpacity> : null
                }

            </View>
        )
    }

    _shareIconView() {
        return (
            <View style={{marginVertical:30,alignItems:'center'}}>
                <Image
                    style={{
                        width: 128,
                        height: 101,
                    }}
                    source={this.props.deviceImage ? {uri: this.props.deviceImage} : require('../../resources/greenUnion/list_power_img.png')}/>
                <CommonTextView
                    textSize={10}
                    text={this.props.deviceType}
                    style={{
                        color: '#737578',
                    }}
                />
            </View>
        )
    }
}