import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput,
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";

/*
*阴影卡片
 */
export default class ShadowCardView extends BaseComponent {
    render() {
        return (
            <TouchableOpacity
                onPress={() => {
                    if (this.props.onPress) {
                        this.props.onPress()
                    }
                }}
                activeOpacity={this.props.onPress ? 0.5 : 1}
                style={[
                    {
                        width: this.mScreenWidth - 36,
                        backgroundColor: '#fff',
                        borderRadius: 10,
                        elevation: Platform.OS === 'android' ? 5 : 1, // 设置阴影角度，通过这个设置有无阴影（这个是最重要的，决定有没有阴影）
                        shadowColor: '#000', // 阴影颜色
                        shadowOffset: {width: 0, height: 0}, // 阴影偏移
                        shadowOpacity: Platform.OS === 'android' ? 1 : 0.1, // 阴影不透明度
                        shadowRadius: 12, // 圆角
                    }, this.props.style
                ]}>
                {this.props.children}
            </TouchableOpacity>
        );
    }
}