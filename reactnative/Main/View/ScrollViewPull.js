import React from 'react';
import {
    ScrollView,
    RefreshControl,
} from 'react-native';
import PropTypes from 'prop-types';

class ScrollViewPull extends React.Component {
    static navigationOptions = {
        header: null,
    };

    static propTypes = {
        style:PropTypes.object, // 样式
        refreshing:PropTypes.bool.isRequired,//是否开始下拉刷新动画
        refreshBegin: PropTypes.func,// 开始下拉刷新回调
        scrollEnd: PropTypes.func,// 触底回调
    };

    constructor(props) {
        super(props);
        this.initState();
    }


    initState=()=>{

    };

    onRefresh = () => {
        this.props.refreshBegin();
    };

    // 监听上拉触底
    _contentViewScroll = (e: Object) => {
        let offsetY = e.nativeEvent.contentOffset.y; //滑动距离
        let contentSizeHeight = e.nativeEvent.contentSize.height; //scrollView contentSize高度
        let oriageScrollHeight = e.nativeEvent.layoutMeasurement.height; //scrollView高度
        if (offsetY + oriageScrollHeight >= contentSizeHeight){
            this.props.scrollEnd();
        }
    };

    render() {
        const {children,refreshing,style} = this.props;
        return (
            <ScrollView
                style={[{flex:1},style]}
                showsVerticalScrollIndicator={false}
                scrollToIndex
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
                onMomentumScrollEnd = {this._contentViewScroll}
            >
                {children}
            </ScrollView>
        );
    }
}

export default ScrollViewPull;