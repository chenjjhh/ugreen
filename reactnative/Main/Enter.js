import React from 'react';
import {
    DeviceEventEmitter, View, AsyncStorage
} from 'react-native';
import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer} from 'react-navigation'
import {RootSiblingParent} from 'react-native-root-siblings';
import Login from '../../reactnative/Main/Login';
import MainDeviceList from '../../reactnative/Main/Device/MainDeviceList';
import HomeView from '../../reactnative/Main/HomeView';
import Register from '../../reactnative/Main/User/Register';
import BindEmail from '../../reactnative/Main/User/BindEmail';
import ResetPassword from '../../reactnative/Main/User/ResetPassword';
import AddDevice from '../../reactnative/Main/Device/AddDevice';
import Mine from '../../reactnative/Main/Mine/Mine';
import NewMine from '../../reactnative/Main/Mine/NewMine';
import Account from '../../reactnative/Main/Mine/Account';
import NewAccount from '../../reactnative/Main/Mine/NewAccount';
import ChangePassword from '../../reactnative/Main/Mine/ChangePassword';
import NewChangePassword from '../../reactnative/Main/Mine/NewChangePassword';
import SignOutAccount from '../../reactnative/Main/Mine/SignOutAccount';

import AboutUs from '../../reactnative/Main/Mine/AboutUs';
import NewAboutUs from '../../reactnative/Main/Mine/NewAboutUs';
import ShareManage from '../../reactnative/Main/Mine/ShareManage';
import DeviceShareMember from '../../reactnative/Main/Mine/DeviceShareMember';
import PersonalInfoProtection from '../../reactnative/Main/Mine/PersonalInfoProtection';
import SystemPermissionsManagement from '../../reactnative/Main/Mine/SystemPermissionsManagement';
import NewUseHelp from '../../reactnative/Main/Mine/NewUseHelp';
import NewUseHelpDeviceDetails from '../../reactnative/Main/Mine/NewUseHelpDeviceDetails';
import NewUseHelpDetails from '../../reactnative/Main/Mine/NewUseHelpDetails';
import SuggestionAndFeedback from '../../reactnative/Main/Mine/SuggestionAndFeedback';
import NewFeedback from '../../reactnative/Main/Mine/NewFeedback';
import NewSelectDeviceList from '../../reactnative/Main/Mine/NewSelectDeviceList';
import SelectProductListView from '../../reactnative/Main/Mine/SelectProductListView';
import afterSalesApplication from '../../reactnative/Main/Mine/afterSalesApplication';
import TechnicalSupport from '../../reactnative/Main/Mine/TechnicalSupport';
import RecordListView from '../../reactnative/Main/Mine/RecordListView';

import BatteryDetails from './Device/BatteryDetails';
import ChargingPackage from './Device/ChargingPackage';
import BatterySetting from './Device/BatterySetting';
import BatteryHealth from './Device/BatteryHealth';
import RegularSettings from './Device/RegularSettings';
import DeviceUpgrade from './Device/DeviceUpgrade';
import DeviceUpgradeDetails from './Device/DeviceUpgradeDetails';
import NewAddDevice from './Device/NewAddDevice';
import ChooseNetwork from './Device/ChooseNetwork';
import ResetDevice from './Device/ResetDevice';
import DeviceAdded from './Device/DeviceAdded';
import ConnectingDeviceWifi from './Device/ConnectingDeviceWifi';
import SelectArea from './Mine/SelectArea';
import SelectCountry from './Mine/SelectCountry';
import AboutDevice from './Device/AboutDevice';
import IntelligentSelfExamination from './Device/IntelligentSelfExamination';


import UseHelp from '../../reactnative/Main/Mine/UseHelp';
import DeviceRating from '../../reactnative/Main/Mine/DeviceRating';
import UseHelpDetail from '../../reactnative/Main/Mine/UseHelpDetail';
import Feedback from '../../reactnative/Main/Mine/Feedback';
import DeviceRatingDetail from '../../reactnative/Main/Mine/DeviceRatingDetail';
import BleAddDevice from '../../reactnative/Main/Device/Connect/BleAddDevice';
import DeviceConnecting from '../../reactnative/Main/Device/Connect/DeviceConnecting';
import DeviceConnectedResult from '../../reactnative/Main/Device/Connect/DeviceConnectedResult';
import WifiDeviceConnection from '../../reactnative/Main/Device/Connect/WifiDeviceConnection';
import WifiDeviceConnectionAndroid from '../../reactnative/Main/Device/Connect/WifiDeviceConnectionAndroid';
import WifiDeviceConnectionIos from '../../reactnative/Main/Device/Connect/WifiDeviceConnectionIos';
import DeviceDetail from '../../reactnative/Main/Device/DeviceDetail';
import DeviceSetting from '../../reactnative/Main/Device/DeviceSetting';
import DeviceShare from '../../reactnative/Main/Device/DeviceShare';
import DeviceFirewareUpdate from '../../reactnative/Main/Device/DeviceFirewareUpdate';
import DeviceSpecifications from '../../reactnative/Main/Device/DeviceSpecifications';
import SplashPager from '../../reactnative/Main/SplashPager';
import UrlWebView from '../../reactnative/Main/Web/UrlWebView';
import GlobeLoadingView from '../../reactnative/Main/View/Loading/GlobeLoadingView';
import SelectDeviceList from '../../reactnative/Main/Mine/SelectDeviceList';
import EventUtil from "./Event/EventUtil";
import BleDeviceConnecting from '../../reactnative/Main/Device/Connect/BleDeviceConnecting';
import MessegeCenterView from './Message/MessegeCenterView';
import MessageSetting from '../../reactnative/Main/Mine/MessageSetting';
import LanguageChoose from '../../reactnative/Main/Mine/LanguageChoose';
import AccountSave from '../../reactnative/Main/Mine/AccountSave';
import HeadsetDetail from '../../reactnative/Main/Device/Headset/HeadsetDetail';


import analytics from '@react-native-firebase/analytics';
import LogUtil from "./Utils/LogUtil";

const AppNavigator = createStackNavigator({
    mainTab: {
        screen: HomeView,
        navigationOptions: ({navigation}) => ({
            header: (
                <View></View>
            ),
        }),
    },
}, {
    // headerMode: 'screen',
    // navigationOptions: NavigationHeader,
})

const RootStack = createStackNavigator({
    Login: Login,
    MainDeviceList: MainDeviceList,
    Home: {
        screen: AppNavigator,
        navigationOptions: ({navigation}) => ({
            header: (
                <View></View>
            ),
        }),
    },
    Register: Register,
    BindEmail: BindEmail,
    ResetPassword: ResetPassword,
    AddDevice: AddDevice,
    Mine: Mine,
    NewMine: NewMine,
    MessegeCenter: MessegeCenterView,
    Account: Account,
    NewAccount: NewAccount,
    MessageSetting: MessageSetting,
    LanguageChoose: LanguageChoose,
    AccountSave: AccountSave,
    NewChangePassword: NewChangePassword,
    SignOutAccount: SignOutAccount,
    ChangePassword: ChangePassword,
    AboutUs: AboutUs,
    NewAboutUs: NewAboutUs,
    ShareManage: ShareManage,
    DeviceShareMember: DeviceShareMember,
    PersonalInfoProtection: PersonalInfoProtection,
    SystemPermissionsManagement: SystemPermissionsManagement,
    NewUseHelp: NewUseHelp,
    NewUseHelpDeviceDetails: NewUseHelpDeviceDetails,
    NewUseHelpDetails: NewUseHelpDetails,
    SuggestionAndFeedback: SuggestionAndFeedback,
    NewFeedback: NewFeedback,
    NewSelectDeviceList: NewSelectDeviceList,
    SelectProductListView: SelectProductListView,
    afterSalesApplication: afterSalesApplication,
    TechnicalSupport: TechnicalSupport,
    RecordListView: RecordListView,
    BatteryDetails: BatteryDetails,
    ChargingPackage: ChargingPackage,
    BatterySetting: BatterySetting,
    BatteryHealth: BatteryHealth,
    RegularSettings: RegularSettings,
    DeviceUpgrade: DeviceUpgrade,
    DeviceUpgradeDetails: DeviceUpgradeDetails,
    NewAddDevice: NewAddDevice,
    ChooseNetwork: ChooseNetwork,
    ResetDevice: ResetDevice,
    DeviceAdded: DeviceAdded,
    ConnectingDeviceWifi: ConnectingDeviceWifi,
    SelectArea: SelectArea,
    SelectCountry: SelectCountry,
    AboutDevice: AboutDevice,
    IntelligentSelfExamination: IntelligentSelfExamination,
    UseHelp: UseHelp,
    DeviceRating: DeviceRating,
    UseHelpDetail: UseHelpDetail,
    Feedback: Feedback,
    DeviceRatingDetail: DeviceRatingDetail,
    BleAddDevice: BleAddDevice,
    DeviceConnecting: DeviceConnecting,
    DeviceConnectedResult: DeviceConnectedResult,
    WifiDeviceConnection: WifiDeviceConnection,
    WifiDeviceConnectionAndroid: WifiDeviceConnectionAndroid,
    WifiDeviceConnectionIos: WifiDeviceConnectionIos,
    DeviceDetail: DeviceDetail,
    DeviceSetting: DeviceSetting,
    DeviceShare: DeviceShare,
    DeviceFirewareUpdate: DeviceFirewareUpdate,
    DeviceSpecifications: DeviceSpecifications,
    SplashPager: SplashPager,
    UrlWebView: UrlWebView,
    SelectDeviceList: SelectDeviceList,
    BleDeviceConnecting: BleDeviceConnecting,
    HeadsetDetail: HeadsetDetail
}, {
    initialRouteName: 'SplashPager',
    navigationOptions: ({navigation}) => {
        return {
            header: null
        };
    }
})

const App1 = createAppContainer(RootStack)
var tempRouteName = ''

class Enter extends React.Component {
    render() {
        return (
            <RootSiblingParent>
                {/* <Provider store={store}> */}
                <App1
                    onNavigationStateChange={(prevState, newState, action) => {
                        DeviceEventEmitter.emit(EventUtil.ROUTES_LENGTH, newState.routes.length);//发送通知

                        if (newState && newState.routes && newState.routes.length > 0) {
                            var firstData = newState.routes[newState.routes.length - 1]
                            var routeName = firstData.routeName

                            if (tempRouteName == '' || tempRouteName != routeName) {
                                analytics().logEvent(routeName, {
                                    time: new Date().getTime(),
                                })
                                tempRouteName = routeName
                                DeviceEventEmitter.emit(EventUtil.CUR_ROUTE, tempRouteName);//发送通知
                            }

                        }

                    }}/>
                <GlobeLoadingView
                    ref={(ref) => {
                        global.mLoadingComponentRef = ref;
                    }}
                />
                {/* </Provider> */}
            </RootSiblingParent>
        )
            ;
    }
}

export default Enter