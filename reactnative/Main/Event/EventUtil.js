import {DeviceEventEmitter} from "react-native";

export default class EventUtil {
    static REFRESH_DEVICE_LIST_KEY = 'refresh_device_list'//更新设备列表
    static AWS_LOGIN_STATUS_KEY = 'Granwin_AWS_status'//aws登录回调
    static AWS_DEVICE_SHADOW_KEY = 'Granwin_AWS_shadow'//设备状态回调
    static DEVICE_NETWORK_KEY = 'Granwin_SetDeviceNetwork'//设备配网回调
    static UPDATE_DEVICE_NAME_SUCCESS = 'update_device_name_success'//修改设备昵称成功
    static UPDATE_USER_INFO = 'update_user_info'//重新获取用户信息
    static FEEDBACK_DEVICE = 'feedback_device'//意见反馈传递设备信息
    static ROUTES_LENGTH = 'routesLength'//监听路由长度
    static SEND_TEMP_UNIT = 'temp_unit'//下发温度单位事件
    static THIRD_LOGIN_KEY = 'Third_Login_Result'//第三方登录回调
    static WIFI_LIST = 'BroadcastReceiver_Wifi'//Wifi列表
    static CUR_ROUTE = 'curRoute'//当前路由
    static DEVICE_PROPS = 'Device_props'
    static UPDATEFIRWARELIST = 'update_firware_list'//更新固件列表
    
    static BLE_SCAN = 'RNBLEModule_onScan'//蓝牙扫描设备
    static BLE_CONNECT_STATUS = 'RNBLEModule_Connect_Status'//蓝牙连接状态
    static BLE_NOTIFY_STATUS = 'RNBLEModule_Notify_Status'//蓝牙通知状态
    static BLE_NOTIFY_DATA = 'RNBLEModule_Data'//蓝牙数据上报
    static AWS_MSG = 'Granwin_AWS_message'//AWS消息通知
    static Route_BleAddDevice = 'BleAddDevice'
    static AWS_NOTIFICATION_IOS_TOKEN = 'Granwin_AWS_notification_ios_token'//iOS推送token
    static TO_LOGIN = 'to_login'
    static DEVICE_OFFLINE = 'device_offline'
    static DEVICE_ONLINE = 'device_online'
    static SYS_ALL_READ = 'sty_all_read'//系统消息一键已读监听
    static DEVICE_ALL_READ = 'device_all_read'//设备推送消息一键已读监听
    static NATIVE_TO_RN = 'NativeToRn_Event'

    static EARBUDS_LIST_UPDATE = 'UGreen_earbuds_list_update'//耳机列表通知

    static sendEventWithoutValue(key) {
        DeviceEventEmitter.emit(key, null);//发送通知
    }

    static sendEvent(key, value) {
        DeviceEventEmitter.emit(key, value);//发送通知
    }
}