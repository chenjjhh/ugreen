import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput, PixelRatio, NativeEventEmitter
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import ShadowCardView from "../View/ShadowCardView";
import CommonBtnView from "../View/CommonBtnView";
import CommonTextView from "../View/CommonTextView";
import ViewHelper from "../View/ViewHelper";
import ProgressView from "../View/ProgressView";
import NetUtil from "../Net/NetUtil";
import CountdownUtil from "../Utils/CountdownUtil";
import EventUtil from "../Event/EventUtil";
import LogUtil from "../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager

/*
*固件升级
 */

var isAfter90s = false
var upFlag = false
export default class DeviceFirewareUpdate extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={'#FBFBFB'}
                    titleText={strings('fireware_update')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            deviceId: props.navigation.state.params.deviceId,
            token: props.navigation.state.params.token,
            step: 0,//0当前版本 1发现新版本 2未发现可用版本 3固件下载中 4固件升级中 5升级失败
            curVersion: '',
            newVersion: '',
            upgradeId: '',
            progress: 0
        }
    }

    componentWillMount() {
        upFlag = false
        //var testValue={"value":"{\"data\":{\"deviceId\":707807836954890240,\"mac\":\"84F70313B878\",\"currentVersion\":\"2.0.19\",\"status\":0},\"messageType\":4}"}
        //var jsonData=JSON.parse(testValue.value)
        // this._upgrageMessage(jsonData)
        //alert(JSON.stringify(jsonData.messageType))
        isAfter90s = false

        if (Platform.OS === 'android') {
            this.awsMessageListListener = DeviceEventEmitter.addListener(EventUtil.AWS_MSG, (message) => {
                //aws 消息回调
                LogUtil.debugLog('aws消息======》' + JSON.stringify(message))
                if (message && message.value) {
                    if (message.value) {
                        var jsonData = JSON.parse(message.value)
                        this._upgrageMessage(jsonData)
                    }
                }
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            eventEmitter.addListener(EventUtil.AWS_MSG, (message) => {
                //aws 消息回调
                LogUtil.debugLog('aws消息======》' + JSON.stringify(message))
                if (message && message.value) {
                    if (message.value) {
                        var jsonData = JSON.parse(message.value)
                        this._upgrageMessage(jsonData)
                    }
                }
            });
        }

        this._getMucVersion(true)
    }

    componentWillUnmount() {
        this._cleanTimer()
        this.awsMessageListListener && this.awsMessageListListener.remove()
        this._stopTimerToGetUpStatus()
    }

    _cleanTimer() {
        if (this.intervalEnd) {
            clearInterval(this.intervalEnd)
        }
        isAfter90s = false

        this._stopTimer2()
        this._stopTimer10()
        this._stopTimer90()
        this._stopTimerAfter90()
        this._stopTimerToGetUpStatus()
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                    alignItems: 'center'
                }}>
                <View
                    style={{
                        flex: 0.59
                    }}>
                    {ViewHelper.getFlexView()}
                    {this._deviceImageView()}
                </View>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center'
                    }}>
                    {this.state.step == 0 || this.state.step == 1 || this.state.step == 2 || this.state.step == 5 ? this._curVersionView() : null}
                    {this.state.step == 1 || this.state.step == 3 || this.state.step == 4 ? this._findNewVersionView() : null}
                    {this.state.step == 2 || this.state.step == 5 ? this._centerTipsView() : null}
                    {ViewHelper.getFlexView()}
                    {/*<CommonBtnView*/}
                    {/*clickAble={true}*/}
                    {/*onPress={() => {*/}
                    {/*var info = {*/}
                    {/*data: {*/}
                    {/*status: 0*/}
                    {/*}*/}
                    {/*}*/}
                    {/*this._upgrageMessage(info)*/}
                    {/*}}*/}
                    {/*style={{*/}
                    {/*width: this.mScreenWidth - 25,*/}
                    {/*marginBottom: 24*/}
                    {/*}}*/}
                    {/*btnText={'test'}/>*/}
                    {this.state.step == 3 || this.state.step == 4 ? this._downLoadingView() : null}
                    {this.state.step == 0 || this.state.step == 2 || this.state.step == 5 ? this._checkUpdateBtnView() : null}
                    {this.state.step == 1 ? this._updateBtnView() : null}
                </View>
            </View>
        );
    }

    _deviceImageView() {
        return (
            <Image
                source={require('../../resources/add_dev_dev_img.png')}/>
        )
    }

    _curVersionView() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 50
                }}>
                <CommonTextView
                    textSize={18}
                    style={{
                        color: '#000',
                    }}
                    text={strings('cur_fireware_version')}/>

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#B5BAC5',
                        marginTop: 5
                    }}
                    text={'v' + this._getNewRuleVersion(this.state.curVersion)}/>
            </View>
        )
    }

    //20.3.56  在APP端只显示：3.56
    _getNewRuleVersion(version) {
        var curVersionArray = version.split(".")

        if (curVersionArray.length == 3) {
            return curVersionArray[1] + '.' + curVersionArray[2]
        } else {
            return version
        }
    }

    //检查更新按钮
    _checkUpdateBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //检查更新
                    this._getMucVersion(false)
                }}
                style={{
                    width: this.mScreenWidth - 25,
                    marginBottom: 24
                }}
                btnText={strings('check_update')}/>
        )
    }

    //更新按钮
    _updateBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //更新
                    this._upgradeVersion()
                }}
                style={{
                    width: this.mScreenWidth - 25,
                    marginBottom: 24,
                    backgroundColor: '#FF8748'
                }}
                btnText={strings('update_now')}/>
        )
    }

    //发现新版本
    _findNewVersionView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 28
                }}>
                <ShadowCardView
                    style={{
                        width: this.mScreenWidth - 62,
                        flexDirection: 'row',
                        height: 82,
                        paddingLeft: 26,
                        paddingRight: 26,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <Image
                        source={require('../../resources/firmware_ic.png')}/>

                    <View
                        style={{
                            marginLeft: 20,
                            flex: 1
                        }}>
                        <CommonTextView
                            textSize={18}
                            style={{
                                color: '#000',
                            }}
                            text={strings('find_new_fireware')}/>

                        <CommonTextView
                            textSize={14}
                            style={{
                                color: '#FF8748',
                                marginTop: 5
                            }}
                            text={'v' + this._getNewRuleVersion(this.state.newVersion)}/>
                    </View>
                </ShadowCardView>
            </View>
        )
    }

    //固件下载
    _downLoadingView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginBottom: 95
                }}>
                <CommonTextView
                    textSize={18}
                    style={{
                        color: '#000',
                    }}
                    text={this.state.step == 3 ? strings('fireware_downloading') : this.state.step == 4 ? strings('fireware_update_ing') : ''}/>

                <CommonTextView
                    textSize={24}
                    style={{
                        color: '#FF8748',
                        marginTop: 10,
                        marginBottom: 20
                    }}
                    text={this.state.progress + '%'}/>

                <ProgressView
                    progress={parseInt(this.state.progress)}/>
            </View>
        )
    }

    _centerTipsView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 28
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 62,
                        flexDirection: 'row',
                        height: 82,
                        paddingLeft: 26,
                        paddingRight: 26,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>

                    <CommonTextView
                        textSize={18}
                        style={{
                            color: '#FF8748',
                        }}
                        text={this.state.step == 2 ? strings('not_find_new_fireware') : this.state.step == 5 ? strings('update_fireware_fail') : ''}/>
                </View>
            </View>
        )
    }

    _getMucVersion(isInit) {
        NetUtil.getMcuVersion(this.state.token, this.state.deviceId, true)
            .then((res) => {
                var resInfo = res.info
                if (isInit) {
                    if (resInfo.currentVersion && resInfo.currentVersion != 'default') {
                        this.setState({
                            curVersion: resInfo.currentVersion,
                        })
                    }
                } else {
                    if (resInfo) {
                        if (resInfo.isUpgrade) {
                            var curVersionArray = this.state.curVersion.split(".")
                            var upgradeVersionArray = resInfo.upgradeVersion.split(".")

                            if (curVersionArray.length == 3 && upgradeVersionArray.length == 3) {
                                var curVersionAdd = this._getArrayToStr(curVersionArray)
                                var upgradeVersionAdd = this._getArrayToStr(upgradeVersionArray)
                                // var curVersion = this.state.curVersion.toString().replace(/\./g, '')
                                // var upgradeVersion = resInfo.upgradeVersion.toString().replace(/\./g, '')
                                var curVersion = parseInt(curVersionAdd)
                                var upgradeVersion = parseInt(upgradeVersionAdd)
                                if (parseInt(upgradeVersion) > parseInt(curVersion)) {
                                    //有可升级的版本
                                    this.setState({
                                        newVersion: resInfo.upgradeVersion,
                                        upgradeId: resInfo.upgradeId,
                                        step: 1
                                    })
                                } else {
                                    //未发现可升级版本
                                    this.setState({step: 2})
                                }
                            } else {
                                //未发现可升级版本
                                this.setState({step: 2})
                            }
                        } else {
                            //未发现可升级版本
                            this.setState({step: 2})
                        }
                    }
                }
            }).catch((error) => {
            //未发现可升级版本
            this.setState({step: 2})
        })
    }

    _getArrayToStr(array) {
        var arrayStr = ''
        array.map((item, index) => {
            if (index != 0) {
                arrayStr += this._getStr2(item)
            }
        })

        return arrayStr
    }

    _getStr2(str) {
        return str.toString().length == 1 ? str + '0' : str
    }

    //更新版本
    _upgradeVersion() {
        if (this.state.token && this.state.upgradeId) {
            // this._getMcuUpgradeStatus(this.state.upgradeId, this.state.token)
            NetUtil.updateMcu(this.state.token, this.state.upgradeId, true)
                .then((res) => {
                    //下发升级成功
                    this.setState({step: 3})
                    this._versionDownLoading()
                }).catch((error) => {

            })
        }
    }

    //显示升级进度，90秒内从0%变为90%
    _versionDownLoading() {
        isAfter90s = false
        let allTime = 90
        let countdownDate = new Date(new Date().getTime() + allTime * 1000);
        this.intervalTimer90 = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
            if (dif <= 0) {
                //倒计时结束  固件下载还未完成
                //若90%时依然未检查到新版本，则进度停止
                if (this.state.progress < 90) {
                    this.setState({progress: 90})
                }
                this._stopTimer90()
                this._notFinishIn90Second()
            } else {
                LogUtil.debugLog('固件下载中========>' + JSON.stringify(dif))
                this.setState({progress: parseInt(allTime - dif)})
                if (dif <= 40 && (this.intervalGetStatus == null || this.intervalGetStatus == undefined)) {
                    this._timerToGetUpStatus()
                }
            }
        }, 1000)
    }

    _stopTimer90() {
        if (this.intervalTimer90) {
            clearInterval(this.intervalTimer90)
            this.intervalTimer90 = null
        }
    }

    //获取升级状态
    _getUpgradeStatus() {
        var isUpgrageSuccessed = true
        //若升级成功，则在10秒内跑完剩余进度
        if (isUpgrageSuccessed) {
            //停止定时
            let curProgress = this.state.progress//当前进度
            let syProgress = parseInt(100 - curProgress)//距离100剩余的进度
            let tempProgress = parseInt(syProgress / 10)
            let countdownDate = new Date(new Date().getTime() + 10 * 1000);
            this.intervalTimer10 = setInterval(() => {
                var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
                LogUtil.debugLog('固件下载中========>' + JSON.stringify(dif))
                if (dif <= 0) {
                    //倒计时结束  升级完成
                    if (this.state.progress < 100) {
                        this.setState({progress: 100})
                    }

                    this.intervalEnd = setInterval(() => {
                        this.setState({
                            step: 0,
                            curVersion: this.state.newVersion
                        })
                        this._cleanTimer()
                        upFlag = false
                    }, 3000)
                    this._stopTimer10()
                } else {
                    this.setState({progress: parseInt(this.state.progress + tempProgress)})
                }
            }, 1000)
        }
    }

    _stopTimer10() {
        if (this.intervalTimer10) {
            clearInterval(this.intervalTimer10)
            this.intervalTimer10 = null
        }
    }

    //90秒内未完成更新   每隔5秒查询一次版本更新情况，共查询30秒
    _notFinishIn90Second() {
        isAfter90s = true
        var allTime = 30
        let countdownDate = new Date(new Date().getTime() + allTime * 1000);
        this.intervalTimerAfter90 = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
            if (dif <= 0) {
                //倒计时结束  若依然查询不到升级成功信息，则显示升级失败
                this.setState({step: 5})
                isAfter90s = false
                this._stopTimerAfter90()
                this._cleanTimer()
            } else {
                LogUtil.debugLog('90秒内未完成更新========>' + JSON.stringify(dif))

            }
        }, 1000)
    }

    _stopTimerAfter90() {
        if (this.intervalTimerAfter90) {
            clearInterval(this.intervalTimerAfter90)
            this.intervalTimerAfter90 = null
        }
    }


    _getUpgradeStatusAfter90Second() {
        var allTime = 2
        var isUpgrageSuccessed = true
        //若升级成功，则在2秒内跑完剩余进度
        if (isUpgrageSuccessed) {
            //停止定时
            let curProgress = this.state.progress//当前进度
            let syProgress = parseInt(100 - curProgress)//距离100剩余的进度
            let tempProgress = parseInt(syProgress / allTime)
            let countdownDate = new Date(new Date().getTime() + allTime * 1000);
            this.intervalTimer2 = setInterval(() => {
                var dif = (countdownDate.getTime() - new Date().getTime()) / 1000
                LogUtil.debugLog('90秒内未完成更新========>' + JSON.stringify(dif))
                if (dif <= 0) {
                    //倒计时结束  升级完成
                    if (this.state.progress < 100) {
                        this.setState({progress: 100})
                    }

                    this.intervalEnd = setInterval(() => {
                        this.setState({
                            step: 0,
                            curVersion: this.state.newVersion
                        })
                        this._cleanTimer()
                        upFlag = false
                    }, 3000)

                    this._stopTimer2()
                } else {
                    this.setState({progress: parseInt(this.state.progress + tempProgress)})
                }
            }, 1000)
        }
    }

    _stopTimer2() {
        if (this.intervalTimer2) {
            clearInterval(this.intervalTimer2)
            this.intervalTimer2 = null
        }
    }

    _getMcuUpgradeStatus(upgradeId, token) {
        NetUtil.getMcuUpgradeStatus(upgradeId, token, false)
            .then((res) => {

            })
    }

    //mcu升级状态通知处理
    _upgrageMessage(info) {
        LogUtil.debugLog('aws消息--========>_upgrageMessage===>' + info.messageType)
        if (info && info.data && parseInt(info.messageType) == 4) {

            var status = parseInt(info.data.status)
            LogUtil.debugLog('aws消息--========>status：' + status)
            if (status == 0 && !upFlag) {
                upFlag = true
                //升级完成

                if (isAfter90s) {
                    this._stopTimerAfter90()
                    LogUtil.debugLog('aws消息--90秒后成功========>')
                    //90秒后成功
                    this._getUpgradeStatusAfter90Second()
                } else {
                    this._stopTimer90()
                    LogUtil.debugLog('aws消息--90秒内成功========>')
                    //90秒内成功
                    this._getUpgradeStatus()
                }
                this._stopTimerToGetUpStatus()
            } else if (status == 1) {
                //升级失败
                this.setState({step: 5})
                this._cleanTimer()
                LogUtil.debugLog('aws消息--升级失败========>')
                upFlag = false
                this._stopTimerToGetUpStatus()
            } else if (status == 3) {
                //升级中
                //upFlag = false
            }
        }
    }


    _timerToGetUpStatus() {
        this._stopTimerToGetUpStatus()
        this.intervalGetStatus = setInterval(() => {
            LogUtil.debugLog('======>_timerToGetUpStatus')
            this._getMucLastVersion()
        }, 5000)
    }

    _stopTimerToGetUpStatus() {
        if (this.intervalGetStatus) {
            clearInterval(this.intervalGetStatus)
            this.intervalGetStatus = null
        }
    }

    //查询升级状态，通过对比新版本号和获取的当前版本号是否相同
    _getMucLastVersion() {
        if (this.state.newVersion == '' || this.state.newVersion == undefined) {
            return
        }

        NetUtil.getMcuVersion(this.state.token, this.state.deviceId, false)
            .then((res) => {
                var resInfo = res.info
                if (resInfo.currentVersion && resInfo.currentVersion != 'default') {
                    var newVersion = this.state.newVersion.toString().replace(/\./g, '')
                    var lastVersion = resInfo.currentVersion.toString().replace(/\./g, '')
                    if (parseInt(newVersion) == parseInt(lastVersion)) {
                        //版本号相等，升级完成
                        //为了重复使用_upgrageMessage方法，写死一个成功的json格式数据去调用_upgrageMessage方法
                        var jsonData = {
                            "data": {
                                "deviceId": this.state.deviceId,
                                "mac": "",
                                "currentVersion": resInfo.currentVersion,
                                "status": 0
                            }, "messageType": 4
                        }
                        this._upgrageMessage(jsonData)
                    }
                }
            }).catch((error) => {
        })
    }
}