import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput, PixelRatio
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import ShadowCardView from "../View/ShadowCardView";
import CommonBtnView from "../View/CommonBtnView";
import CommonTextView from "../View/CommonTextView";
import ViewHelper from "../View/ViewHelper";
import MessageDialog from "../View/MessageDialog";
import AddShareDialog from "../View/AddShareDialog";
import Helper from "../Utils/Helper";
import NetUtil from "../Net/NetUtil";
import FastImage from "react-native-fast-image";

/*
*设备设置
 */

export default class DeviceShare extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={'#FBFBFB'}
                    titleText={strings('device_share')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            shareArray: [],
            cancelShareDialog: false,
            addShareDialog: false,
            emailValue: '',
            deviceId: props.navigation.state.params.deviceId,
            token: props.navigation.state.params.token,
            cancelShareDeviceId: '',
            cancelShareUserId: '',
        }
    }

    componentWillMount() {
        this._getList(true)
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                }}>
                {
                    this.state.shareArray.length > 0 ?
                        <ScrollView>
                            {this._shareListView()}
                        </ScrollView>
                        :
                        this._noshareView()
                }


                {this._addShareBtnView()}
                {this._dialogView()}
            </View>
        );
    }

    _dialogView() {
        return (
            <View>
                <MessageDialog
                    onRequestClose={() => {
                        this.setState({cancelShareDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({cancelShareDialog: false})
                    }}
                    modalVisible={this.state.cancelShareDialog}
                    title={strings('dialog_cancel_share')}
                    message={strings('cancel_share_tips')}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({cancelShareDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    rightBtnTextColor={'#F77979'}
                    onRightBtnClick={() => {
                        //确认
                        this.setState({cancelShareDialog: false})
                        this._cancelShare(this.state.cancelShareDeviceId, this.state.cancelShareUserId)
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <AddShareDialog
                    onRequestClose={() => {
                        this.setState({addShareDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({addShareDialog: false})
                    }}
                    modalVisible={this.state.addShareDialog}
                    title={strings('add_share')}
                    leftBtnText={strings('send')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //发送
                        if (!Helper._ifValidEmail(this.state.emailValue)) {
                            this.onShowToast(strings('input_valid_email_account'))
                        } else {
                            this._sendShare(this.state.deviceId, this.state.token, this.state.emailValue)
                        }
                        this.setState({addShareDialog: false})

                    }}
                    emailValue={this.state.emailValue}
                    onChangeText={(text) => {
                        this.setState({emailValue: text})
                    }}
                    placeholder={strings('input_receiver_email')}
                    placeholderTextColor={'rgba(0,0,0,0.26)'}
                />
            </View>
        )
    }

    //发送设备分享
    _sendShare(deviceId, token, email) {
        NetUtil.shareDevice(token, deviceId, email, true)
            .then((res) => {
                this.onShowToast(strings('send_success'))
            })
            .catch((error) => {
                //this.onShowToast(strings('send_fail'))
            })
    }

    _noshareView() {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <Image
                    source={require('../../resources/noshare_ic.png')}/>

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000',
                        marginTop: 27
                    }}
                    text={strings('no_device_share')}/>
            </View>
        )
    }

    _shareListView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center'
                }}>
                {
                    this.state.shareArray.map((item, index) => {
                        return (this._shareListItemView(item, index))
                    })
                }
            </View>
        )
    }

    _shareListItemView(item, index) {
        return (
            <ShadowCardView
                key={index}
                style={{
                    height: 80,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingRight: 22,
                    paddingLeft: 22,
                    marginTop: 10,
                    marginBottom: index == this.state.shareArray.length - 1 ? 10 : 0
                }}>

                {
                    item.avatar ?
                        <View
                            style={{
                                width: this.mScreenWidth * 0.11,
                                height: this.mScreenWidth * 0.11,
                                borderRadius: (this.mScreenWidth * 0.11) / 2,
                                overflow: 'hidden',
                            }}>
                            <FastImage
                                style={{
                                    width: this.mScreenWidth * 0.11,
                                    height: this.mScreenWidth * 0.11,
                                }}
                                source={{
                                    uri: item.avatar,
                                    priority: FastImage.priority.normal,
                                }}
                                resizeMode={FastImage.resizeMode.cover}
                            />
                        </View>
                        :
                        <Image
                            style={{
                                width: this.mScreenWidth * 0.11,
                                height: this.mScreenWidth * 0.11,
                                resizeMode: 'contain'
                            }}
                            source={require('../../resources/ic_display_small.png')}/>
                }


                <View
                    style={{
                        marginLeft: 16
                    }}>
                    <CommonTextView
                        textSize={18}
                        style={{
                            color: '#000',
                        }}
                        text={item.nickName}/>

                    <CommonTextView
                        textSize={14}
                        style={{
                            color: '#B5BAC5',
                            marginTop: 5
                        }}
                        text={item.email}/>
                </View>

                {ViewHelper.getFlexView()}

                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        //取消分享
                        this.setState({
                            cancelShareDeviceId: item.deviceId,
                            cancelShareUserId: item.appuserId,
                            cancelShareDialog: true
                        })
                    }}
                    style={{
                        width: this.mScreenWidth * 0.2,
                        height: this.mScreenWidth * 0.07,
                        backgroundColor: '#FF8748'
                    }}
                    textStyle={{
                        fontSize: this.getSize(12)
                    }}
                    btnText={strings('cancel_share')}/>

            </ShadowCardView>
        )
    }

    _addShareBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //添加分享
                    this.setState({addShareDialog: true})
                }}
                style={{
                    width: this.mScreenWidth - 50,
                    marginTop: 20,
                    marginBottom: 24,
                    marginLeft: 25
                }}
                btnText={strings('add_share')}/>
        )
    }

    _getList(showLoading) {
        NetUtil.getCanCancelShareList(this.state.token, this.state.deviceId, showLoading)
            .then((res) => {
                var resArray = res.info
                if (resArray && resArray.length > 0) {
                    this.setState({shareArray: resArray})
                } else {
                    this.setState({shareArray: []})
                }
            }).catch((error) => {

        })
    }

    _cancelShare(deviceId, userId) {
        if (deviceId == '' || userId == '') {
            return
        }

        NetUtil.deviceOwnerCancalShare(this.state.token, deviceId, userId, true)
            .then((res) => {
                this.onShowToast('successed')
                this._getList(false)
            })
            .catch((error) => {

            })
    }
}