import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {DeviceEventEmitter, NativeEventEmitter, NativeModules, Platform, ScrollView, View} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings} from '../Language/I18n';
import ShadowCardView from "../View/ShadowCardView";
import MessageDialog from "../View/MessageDialog";
import ListItemView from "../View/ListItemView";
import CommonBtnView from "../View/CommonBtnView";
import DeviceSettingSlideView from "../View/DeviceSettingSlideView";
import DeviceSettingSlideView1 from "../View/DeviceSettingSlideView1";
import SpecUtil from "../Utils/SpecUtil";
import UpdateDeviceNameDialog from "../View/UpdateDeviceNameDialog";
import NetUtil from "../Net/NetUtil";
import EventUtil from "../Event/EventUtil";
import Helper from "../Utils/Helper";
import RouterUtil from "../Utils/RouterUtil";
import DeviceSettingSlideRangeView from "../View/DeviceSettingSlideRangeView";
import Bluetooth from "../Bluetooth/Bluetooth";
import MainDeviceList from "./MainDeviceList";
import StorageHelper from "../Utils/StorageHelper";
import LogUtil from "../Utils/LogUtil";

/*
*设备设置
 */

var acPower = 0
var etcMin = -1
var etcMax = -1
var pvPower = 0
var windPower = 0
var standbyTime = 0
var screenOffTime = 0
var acStandbyTime = 0

const specDelayTime = 5000

const powerUnit = 'w'
var GXRNManager = NativeModules.GXRNManager
var tempSpecDatas = null
export default class DeviceSetting extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={'#FBFBFB'}
                    titleText={strings('setting')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            isWifiConnect: !Helper.dataIsNull(props.navigation.state.params.isWifiConnect) ? props.navigation.state.params.isWifiConnect : false,
            beePower: props.navigation.state.params.beeSwitch,
            acChargePowerValue: !Helper.dataIsNull(props.navigation.state.params.acChargePower) ? props.navigation.state.params.acChargePower : 500,//AC充电功率设置  w
            acChargePowerVis: false,
            pvChargePowerValue: !Helper.dataIsNull(props.navigation.state.params.pvChargePower) ? props.navigation.state.params.pvChargePower : 0,//光伏充电功率
            pvChargePowerVis: false,
            fanChargePowerValue: !Helper.dataIsNull(props.navigation.state.params.fanChargePower) ? props.navigation.state.params.fanChargePower : 0,//风力充电功率
            fanChargePowerVis: false,
            deviceStandbyTimeValue: !Helper.dataIsNull(props.navigation.state.params.deviceStandByTime) ? props.navigation.state.params.deviceStandByTime : 0,//设备待机时间
            deviceStandbyTimeVis: false,
            screenOffTimeValue: !Helper.dataIsNull(props.navigation.state.params.screenOffTime) ? props.navigation.state.params.screenOffTime : 0,//息屏时间
            screenOffTimeVis: false,
            acStandbyTimeValue: !Helper.dataIsNull(props.navigation.state.params.acStandByTime) ? props.navigation.state.params.acStandByTime : 0,//交流待机时间
            acStandbyTimeVis: false,
            batteryProtectRangeMin: !Helper.dataIsNull(props.navigation.state.params.batteryProtectRangeMin) ? parseInt(props.navigation.state.params.batteryProtectRangeMin) : 0,//BMS电池保护下限值设置
            batteryProtectRangeMax: !Helper.dataIsNull(props.navigation.state.params.batteryProtectRangeMax) ? parseInt(props.navigation.state.params.batteryProtectRangeMax) : 100,//BMS电池保护上限值设置
            batteryProtectRangeVis: false,

            account: props.navigation.state.params.account,
            mac: props.navigation.state.params.mac,
            showMac: props.navigation.state.params.showMac,
            pk: props.navigation.state.params.pk,
            deviceName: props.navigation.state.params.deviceName,
            updateNameDialog: false,
            inputNameValue: props.navigation.state.params.deviceName,
            deviceId: props.navigation.state.params.deviceId,
            token: props.navigation.state.params.token,
            isWifiOnline: props.navigation.state.params.isWifiOnline,
            uType: props.navigation.state.params.uType,
            chargeStatus: props.navigation.state.params.chargeStatus,

            unbindDeviceDialog: false,
            specData: props.navigation.state.params.specData ? props.navigation.state.params.specData : '',


            acChargePowerValue1: !Helper.dataIsNull(props.navigation.state.params.acChargePower) ? props.navigation.state.params.acChargePower : 500,
            pvChargePowerValue1: !Helper.dataIsNull(props.navigation.state.params.pvChargePower) ? props.navigation.state.params.pvChargePower : 0,
            fanChargePowerValue1: !Helper.dataIsNull(props.navigation.state.params.fanChargePower) ? props.navigation.state.params.fanChargePower : 0,
            deviceStandbyTimeValue1: !Helper.dataIsNull(props.navigation.state.params.deviceStandByTime) ? props.navigation.state.params.deviceStandByTime : 0,
            screenOffTimeValue1: !Helper.dataIsNull(props.navigation.state.params.screenOffTime) ? props.navigation.state.params.screenOffTime : 0,
            acStandbyTimeValue1: !Helper.dataIsNull(props.navigation.state.params.acStandByTime) ? props.navigation.state.params.acStandByTime : 0,
            batteryProtectRangeMin1: !Helper.dataIsNull(props.navigation.state.params.batteryProtectRangeMin) ? parseInt(props.navigation.state.params.batteryProtectRangeMin) : 0,
            batteryProtectRangeMax1: !Helper.dataIsNull(props.navigation.state.params.batteryProtectRangeMax) ? parseInt(props.navigation.state.params.batteryProtectRangeMax) : 100,

            beePowerTime: 0,
            acChargePowerTime: 0,
            pvChargePowerTime: 0,
            fanChargePowerTime: 0,
            deviceStandbyTimeTime: 0,
            screenOffTimeTime: 0,
            acStandbyTimeTime: 0,
            batteryProtectRangeTime: 0,

            elcValue: props.navigation.state.params.elcValue,
        }
    }

    componentWillMount() {
        tempSpecDatas = null
        this.setState({
            token: StorageHelper.getTempToken()
        })

        if (this.state.isWifiConnect) {
            this._getDeviceProps(this.state.deviceId, this.state.mac)
        }
        this._resetTempData()
        if (this.state.isWifiConnect) {
            this._eventListenerAdd()
        } else {
            //蓝牙相关
            this._bleEvent()
        }
    }

    componentWillUnmount() {
        this._eventListenerRemove()
        this._bleDestory()
    }

    _eventListenerAdd() {
        if (Platform.OS === 'android') {
            this.awsDeviceStatusListListener = DeviceEventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                LogUtil.debugLog('设备设置页面------设备状态回调:' + JSON.stringify(value))
                //收到属性变化，重新请求获取属性接口
                //this._getDeviceProps(this.state.deviceId, this.state.mac)
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            eventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                LogUtil.debugLog('设备设置页面------设备状态回调:' + JSON.stringify(value))
                //收到属性变化，重新请求获取属性接口
                //this._getDeviceProps(this.state.deviceId, this.state.mac)
            });
        }

        this.devicePropsListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_PROPS, (resInfo) => {
            //收到设备属性数据
            if (resInfo) {
                this._resolveDevicePropsData(resInfo)
            }
        });
    }

    _eventListenerRemove() {
        this.awsDeviceStatusListListener && this.awsDeviceStatusListListener.remove()
        this.devicePropsListener && this.devicePropsListener.remove()
    }

    _resolveDevicePropsData(resInfo) {
        var specDatas = SpecUtil.resolveSpecs(resInfo)

        if (tempSpecDatas != specDatas) {
            tempSpecDatas = specDatas

            if (this.state.chargeStatus != specDatas.chargeStatus) {
                this.setState({
                    chargeStatus: !Helper.dataIsNull(specDatas.chargeStatus) ? specDatas.chargeStatus : this.state.chargeStatus,
                })
            }
            if (this.state.elcValue != specDatas.curRemaining) {
                this.setState({
                    elcValue: !Helper.dataIsNull(specDatas.curRemaining) ? specDatas.curRemaining : this.state.elcValue,
                })
            }

            if (this._canGetSpecData(this.state.beePowerTime) && this.state.beePower != specDatas.beeSwitch) {
                this.setState({
                    beePower: !Helper.dataIsNull(specDatas.beeSwitch) ? specDatas.beeSwitch : this.state.beePower,
                })
            }
            if (this._canGetSpecData(this.state.acChargePowerTime) && this.state.acChargePowerValue != specDatas.acInputPower) {
                this.setState({
                    acChargePowerValue: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue,
                })
            }
            if (this._canGetSpecData(this.state.pvChargePowerTime) && this.state.pvChargePowerValue != specDatas.pvChargePower) {
                this.setState({
                    pvChargePowerValue: !Helper.dataIsNull(specDatas.pvChargePower) ?
                        parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePowerValue
                        : this.state.pvChargePowerValue,
                })
            }
            if (this._canGetSpecData(this.state.fanChargePowerTime) && this.state.fanChargePowerValue != specDatas.fanChargePower) {
                this.setState({
                    fanChargePowerValue: !Helper.dataIsNull(specDatas.fanChargePower) ?
                        parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePowerValue
                        : this.state.fanChargePowerValue,
                })
            }
            if (this._canGetSpecData(this.state.deviceStandbyTimeTime) && this.state.deviceStandbyTimeValue != specDatas.deviceStandbyTime) {
                this.setState({
                    deviceStandbyTimeValue: !Helper.dataIsNull(specDatas.deviceStandbyTime) ?
                        parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandbyTimeValue
                        : this.state.deviceStandbyTimeValue,
                })
            }
            if (this._canGetSpecData(this.state.screenOffTimeTime) && this.state.screenOffTimeValue != specDatas.screenOffTime) {
                this.setState({
                    screenOffTimeValue: !Helper.dataIsNull(specDatas.screenOffTime) ?
                        parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTimeValue
                        : this.state.screenOffTimeValue,
                })
            }
            if (this._canGetSpecData(this.state.acStandbyTimeTime) && this.state.acStandbyTimeValue != specDatas.acStandbyTime) {
                this.setState({
                    acStandbyTimeValue: !Helper.dataIsNull(specDatas.acStandbyTime) ?
                        parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandbyTimeValue
                        : this.state.acStandbyTimeValue,
                })
            }
            if (this._canGetSpecData(this.state.batteryProtectRangeTime) && this.state.batteryProtectRangeMin != specDatas.batteryProtectRangeMin) {
                this.setState({
                    batteryProtectRangeMin: !Helper.dataIsNull(specDatas.batteryProtectRangeMin) ?
                        parseInt(specDatas.batteryProtectRangeMin) >= 0 ? (parseInt(specDatas.batteryProtectRangeMin)) : this.state.batteryProtectRangeMin
                        : this.state.batteryProtectRangeMin,
                })
            }
            if (this._canGetSpecData(this.state.batteryProtectRangeTime) && this.state.batteryProtectRangeMax != specDatas.batteryProtectRangeMax) {
                this.setState({
                    batteryProtectRangeMax: !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? (parseInt(specDatas.batteryProtectRangeMax)) : this.state.batteryProtectRangeMax,
                })
            }


            if (!this.state.acChargePowerVis && this._canGetSpecData(this.state.acChargePowerTime) && this.state.acChargePowerValue1 != specDatas.acInputPower) {
                this.setState({
                    acChargePowerValue1: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue1,
                })
                acPower = !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue1
            }
            if (!this.state.pvChargePowerVis && this._canGetSpecData(this.state.pvChargePowerTime) && this.state.pvChargePowerValue1 != specDatas.pvChargePower) {
                pvPower = !Helper.dataIsNull(specDatas.pvChargePower) ?
                    parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePowerValue1
                    : this.state.pvChargePowerValue1
                this.setState({
                    pvChargePowerValue1: pvPower,
                })
            }
            if (!this.state.fanChargePowerVis && this._canGetSpecData(this.state.fanChargePowerTime) && this.state.fanChargePowerValue1 != specDatas.fanChargePower) {
                windPower = !Helper.dataIsNull(specDatas.fanChargePower) ?
                    parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePowerValue1
                    : this.state.fanChargePowerValue1
                this.setState({
                    fanChargePowerValue1: windPower,
                })
            }
            if (!this.state.deviceStandbyTimeVis && this._canGetSpecData(this.state.deviceStandbyTimeTime) && this.state.deviceStandbyTimeValue1 != specDatas.deviceStandbyTime) {
                standbyTime = !Helper.dataIsNull(specDatas.deviceStandbyTime) ?
                    parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandbyTimeValue1
                    : this.state.deviceStandbyTimeValue1
                this.setState({
                    deviceStandbyTimeValue1: standbyTime,
                })
            }
            if (!this.state.screenOffTimeVis && this._canGetSpecData(this.state.screenOffTimeTime) && this.state.screenOffTimeValue1 != specDatas.screenOffTime) {
                screenOffTime = !Helper.dataIsNull(specDatas.screenOffTime) ?
                    parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTimeValue1
                    : this.state.screenOffTimeValue1
                this.setState({
                    screenOffTimeValue1: screenOffTime,
                })
            }
            if (!this.state.acStandbyTimeVis && this._canGetSpecData(this.state.acStandbyTimeTime) && this.state.acStandbyTimeValue1 != specDatas.acStandbyTime) {
                acStandbyTime = !Helper.dataIsNull(specDatas.acStandbyTime) ?
                    parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandbyTimeValue1
                    : this.state.acStandbyTimeValue1
                this.setState({
                    acStandbyTimeValue1: acStandbyTime,
                })
            }
            if (!this.state.batteryProtectRangeVis && this._canGetSpecData(this.state.batteryProtectRangeTime) && this.state.batteryProtectRangeMin1 != specDatas.batteryProtectRangeMin) {
                etcMin = !Helper.dataIsNull(specDatas.batteryProtectRangeMin) ?
                    parseInt(specDatas.batteryProtectRangeMin) >= 0 ? (parseInt(specDatas.batteryProtectRangeMin)) : this.state.batteryProtectRangeMin
                    : this.state.batteryProtectRangeMin
                this.setState({
                    batteryProtectRangeMin1: etcMin,
                })
            }
            if (!this.state.batteryProtectRangeVis && this._canGetSpecData(this.state.batteryProtectRangeTime) && this.state.batteryProtectRangeMax1 != specDatas.batteryProtectRangeMax) {
                etcMax = !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? parseInt(specDatas.batteryProtectRangeMax) : this.state.batteryProtectRangeMax
                this.setState({
                    batteryProtectRangeMax1: etcMax,
                })
            }
        }
    }

    _resetTempData() {
        acPower = this.state.acChargePowerValue
        etcMin = this.state.batteryProtectRangeMin
        etcMax = this.state.batteryProtectRangeMax
        pvPower = this.state.pvChargePowerValue
        windPower = this.state.fanChargePowerValue
        standbyTime = this.state.deviceStandbyTimeValue
        screenOffTime = this.state.screenOffTimeValue
        acStandbyTime = this.state.acStandbyTimeValue
    }

    //获取设备属性
    _getDeviceProps(deviceId, mac) {
        if (Helper.dataIsNull(deviceId)) {
            return
        }

        NetUtil.getDeviceProps(this.state.token, deviceId, false)
            .then((res) => {
                var specDatas = SpecUtil.resolveSpecs(res.info)

                this.setState({
                    chargeStatus: !Helper.dataIsNull(specDatas.chargeStatus) ? specDatas.chargeStatus : this.state.chargeStatus,
                    elcValue: !Helper.dataIsNull(specDatas.curRemaining) ? specDatas.curRemaining : this.state.elcValue,
                })
                if (this._canGetSpecData(this.state.beePowerTime)) {
                    this.setState({
                        beePower: !Helper.dataIsNull(specDatas.beeSwitch) ? specDatas.beeSwitch : this.state.beePower,
                    })
                }
                if (this._canGetSpecData(this.state.acChargePowerTime)) {
                    this.setState({
                        acChargePowerValue: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue,
                    })
                }
                if (this._canGetSpecData(this.state.pvChargePowerTime)) {
                    this.setState({
                        pvChargePowerValue: !Helper.dataIsNull(specDatas.pvChargePower) ?
                            parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePowerValue
                            : this.state.pvChargePowerValue,
                    })
                }
                if (this._canGetSpecData(this.state.fanChargePowerTime)) {
                    this.setState({
                        fanChargePowerValue: !Helper.dataIsNull(specDatas.fanChargePower) ?
                            parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePowerValue
                            : this.state.fanChargePowerValue,
                    })
                }
                if (this._canGetSpecData(this.state.deviceStandbyTimeTime)) {
                    this.setState({
                        deviceStandbyTimeValue: !Helper.dataIsNull(specDatas.deviceStandbyTime) ?
                            parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandbyTimeValue
                            : this.state.deviceStandbyTimeValue,
                    })
                }
                if (this._canGetSpecData(this.state.screenOffTimeTime)) {
                    this.setState({
                        screenOffTimeValue: !Helper.dataIsNull(specDatas.screenOffTime) ?
                            parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTimeValue
                            : this.state.screenOffTimeValue,
                    })
                }
                if (this._canGetSpecData(this.state.acStandbyTimeTime)) {
                    this.setState({
                        acStandbyTimeValue: !Helper.dataIsNull(specDatas.acStandbyTime) ?
                            parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandbyTimeValue
                            : this.state.acStandbyTimeValue,
                    })
                }
                if (this._canGetSpecData(this.state.batteryProtectRangeTime)) {
                    this.setState({
                        batteryProtectRangeMin: !Helper.dataIsNull(specDatas.batteryProtectRangeMin) ?
                            parseInt(specDatas.batteryProtectRangeMin) >= 0 ? (parseInt(specDatas.batteryProtectRangeMin)) : this.state.batteryProtectRangeMin
                            : this.state.batteryProtectRangeMin,
                        batteryProtectRangeMax: !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? (parseInt(specDatas.batteryProtectRangeMax)) : this.state.batteryProtectRangeMax,
                    })
                }


                if (!this.state.acChargePowerVis && this._canGetSpecData(this.state.acChargePowerTime)) {
                    this.setState({
                        acChargePowerValue1: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue1,
                    })
                    acPower = !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue1
                }
                if (!this.state.pvChargePowerVis && this._canGetSpecData(this.state.pvChargePowerTime)) {
                    pvPower = !Helper.dataIsNull(specDatas.pvChargePower) ?
                        parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePowerValue1
                        : this.state.pvChargePowerValue1
                    this.setState({
                        pvChargePowerValue1: pvPower,
                    })
                }
                if (!this.state.fanChargePowerVis && this._canGetSpecData(this.state.fanChargePowerTime)) {
                    windPower = !Helper.dataIsNull(specDatas.fanChargePower) ?
                        parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePowerValue1
                        : this.state.fanChargePowerValue1
                    this.setState({
                        fanChargePowerValue1: windPower,
                    })
                }
                if (!this.state.deviceStandbyTimeVis && this._canGetSpecData(this.state.deviceStandbyTimeTime)) {
                    standbyTime = !Helper.dataIsNull(specDatas.deviceStandbyTime) ?
                        parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandbyTimeValue1
                        : this.state.deviceStandbyTimeValue1
                    this.setState({
                        deviceStandbyTimeValue1: standbyTime,
                    })
                }
                if (!this.state.screenOffTimeVis && this._canGetSpecData(this.state.screenOffTimeTime)) {
                    screenOffTime = !Helper.dataIsNull(specDatas.screenOffTime) ?
                        parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTimeValue1
                        : this.state.screenOffTimeValue1
                    this.setState({
                        screenOffTimeValue1: screenOffTime,
                    })
                }
                if (!this.state.acStandbyTimeVis && this._canGetSpecData(this.state.acStandbyTimeTime)) {
                    acStandbyTime = !Helper.dataIsNull(specDatas.acStandbyTime) ?
                        parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandbyTimeValue1
                        : this.state.acStandbyTimeValue1
                    this.setState({
                        acStandbyTimeValue1: acStandbyTime,
                    })
                }
                if (!this.state.batteryProtectRangeVis && this._canGetSpecData(this.state.batteryProtectRangeTime)) {
                    etcMin = !Helper.dataIsNull(specDatas.batteryProtectRangeMin) ?
                        parseInt(specDatas.batteryProtectRangeMin) >= 0 ? (parseInt(specDatas.batteryProtectRangeMin)) : this.state.batteryProtectRangeMin
                        : this.state.batteryProtectRangeMin
                    etcMax = !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? parseInt(specDatas.batteryProtectRangeMax) : this.state.batteryProtectRangeMax
                    this.setState({
                        batteryProtectRangeMin1: etcMin,
                        batteryProtectRangeMax1: etcMax,
                    })
                }
            })
            .catch((error) => {

            })
    }

    _canGetSpecData(time) {
        var flag = true
        var curTime = new Date().getTime()
        if (curTime - time < specDelayTime) {
            flag = false
        }

        return flag
    }


    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                }}>
                <ScrollView>
                    <View
                        style={{
                            width: this.mScreenWidth,
                            alignItems: 'center'
                        }}>
                        {this.state.isWifiConnect || parseInt(this.state.uType) == -1 ? this._deviceName() : null}
                        {this._beePowerName()}
                        {this._view1()}
                        {this._view2()}
                        {this._view3()}
                        {this._view4()}
                        {this.state.isWifiConnect || parseInt(this.state.uType) == -1 ? this._unbindBtnView() : <View
                            style={{
                                height: 60,
                                width: 1
                            }}/>}
                    </View>
                </ScrollView>

                {this._dialogView()}
            </View>
        );
    }

    _dialogView() {
        return (
            <View>
                <DeviceSettingSlideView
                    step={1}
                    minimumValue={500}
                    maximumValue={this.state.chargeStatus == 3 ? 3000 :
                        this.state.chargeStatus == 2 ? 1800 : 1500}
                    onRequestClose={() => {
                        this.setState({acChargePowerVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({acChargePowerVis: false})
                    }}
                    modalVisible={this.state.acChargePowerVis}
                    title={strings('ac_charge_power_setting')}
                    sliderValue={parseInt(this.state.acChargePowerValue1)}
                    valueUnit={powerUnit}
                    onValueChange={(value) => {
                        acPower = value
                        this.setState({acChargePowerValue1: value})
                    }}
                    onSlidingComplete={(value) => {
                        acPower = value
                        this.setState({acChargePowerValue1: value})
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({acChargePowerVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({acChargePowerVis: false})
                        if (acPower) {
                            this.setState({
                                acChargePowerValue: acPower,
                                acChargePowerTime: new Date().getTime()
                            })
                            SpecUtil._sendAcChargePower(this.state.account, this.state.pk, this.state.mac, acPower)
                        }
                    }}/>

                <DeviceSettingSlideView
                    step={1}
                    minimumValue={0}
                    maximumValue={1500}
                    onRequestClose={() => {
                        this.setState({pvChargePowerVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({pvChargePowerVis: false})
                    }}
                    modalVisible={this.state.pvChargePowerVis}
                    title={strings('light_charge_power')}
                    sliderValue={parseInt(this.state.pvChargePowerValue1)}
                    valueUnit={powerUnit}
                    onValueChange={(value) => {
                        pvPower = value
                        this.setState({pvChargePowerValue1: value})
                    }}
                    onSlidingComplete={(value) => {
                        pvPower = value
                        this.setState({pvChargePowerValue1: value})
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({pvChargePowerVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({
                            pvChargePowerVis: false,
                            pvChargePowerTime: new Date().getTime()
                        })
                        if (pvPower >= 0) {
                            this.setState({pvChargePowerValue: pvPower})
                            SpecUtil._sendPvChargePower(this.state.account, this.state.pk, this.state.mac, pvPower)
                        }
                    }}/>

                <DeviceSettingSlideView
                    step={1}
                    minimumValue={0}
                    maximumValue={200}
                    onRequestClose={() => {
                        this.setState({fanChargePowerVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({fanChargePowerVis: false})
                    }}
                    modalVisible={this.state.fanChargePowerVis}
                    title={strings('wind_charge_power')}
                    sliderValue={parseInt(this.state.fanChargePowerValue1)}
                    valueUnit={powerUnit}
                    onValueChange={(value) => {
                        windPower = value
                        this.setState({fanChargePowerValue1: value})
                    }}
                    onSlidingComplete={(value) => {
                        windPower = value
                        this.setState({fanChargePowerValue1: value})
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({fanChargePowerVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({fanChargePowerVis: false})
                        if (windPower >= 0) {
                            this.setState({
                                fanChargePowerValue: windPower,
                                fanChargePowerTime: new Date().getTime()
                            })
                            SpecUtil._sendFanChargePower(this.state.account, this.state.pk, this.state.mac, windPower)
                        }
                    }}/>

                <DeviceSettingSlideView1
                    alwaysText={strings('always_on')}
                    isCheck={parseFloat(this.state.deviceStandbyTimeValue1) == parseFloat(0)}
                    onCheck={() => {
                        //常开
                        standbyTime = 0
                        this.setState({
                            deviceStandbyTimeValue1: 0,
                        })
                    }}
                    isCheckColor={'#FF8748'}
                    step={0.5}
                    minimumValue={0.5}
                    maximumValue={24}
                    onRequestClose={() => {
                        this.setState({deviceStandbyTimeVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({deviceStandbyTimeVis: false})
                    }}
                    modalVisible={this.state.deviceStandbyTimeVis}
                    title={strings('device_stand_by_time')}
                    sliderValue={parseFloat(this.state.deviceStandbyTimeValue1)}
                    valueUnit={strings('hour')}
                    onValueChange={(value) => {
                        standbyTime = value
                        this.setState({deviceStandbyTimeValue1: value})
                    }}
                    onSlidingComplete={(value) => {
                        standbyTime = value
                        this.setState({deviceStandbyTimeValue1: value})
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({deviceStandbyTimeVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({deviceStandbyTimeVis: false})
                        if (standbyTime != undefined) {
                            this.setState({
                                deviceStandbyTimeValue: standbyTime,
                                deviceStandbyTimeTime: new Date().getTime()
                            })
                            SpecUtil._sendDeviceStandbyTime(this.state.account, this.state.pk, this.state.mac, standbyTime)
                        }
                    }}/>

                <DeviceSettingSlideView1
                    alwaysText={strings('always_on')}
                    isCheck={parseFloat(this.state.screenOffTimeValue1) == parseFloat(0)}
                    onCheck={() => {
                        //常开
                        screenOffTime = 0
                        this.setState({
                            screenOffTimeValue1: 0,
                        })
                    }}
                    isCheckColor={'#FF8748'}
                    step={1}
                    minimumValue={1}
                    maximumValue={10}
                    onRequestClose={() => {
                        this.setState({screenOffTimeVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({screenOffTimeVis: false})
                    }}
                    modalVisible={this.state.screenOffTimeVis}
                    title={strings('screen_off_time')}
                    sliderValue={parseInt(this.state.screenOffTimeValue1)}
                    valueUnit={strings('min')}
                    onValueChange={(value) => {
                        screenOffTime = value
                        this.setState({screenOffTimeValue1: value})
                    }}
                    onSlidingComplete={(value) => {
                        screenOffTime = value
                        this.setState({screenOffTimeValue1: value})
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({screenOffTimeVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({
                            screenOffTimeVis: false,
                            screenOffTimeTime: new Date().getTime()
                        })
                        if (screenOffTime != undefined) {
                            this.setState({screenOffTimeValue: screenOffTime})
                            SpecUtil._sendScreenOffTime(this.state.account, this.state.pk, this.state.mac, screenOffTime)
                        }
                    }}/>

                <DeviceSettingSlideView1
                    alwaysText={strings('always_on')}
                    isCheck={parseFloat(this.state.acStandbyTimeValue1) == parseFloat(0)}
                    onCheck={() => {
                        //常开
                        acStandbyTime = 0
                        this.setState({
                            acStandbyTimeValue1: 0,
                        })
                    }}
                    isCheckColor={'#FF8748'}
                    step={0.5}
                    minimumValue={0.5}
                    maximumValue={24}
                    onRequestClose={() => {
                        this.setState({acStandbyTimeVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({acStandbyTimeVis: false})
                    }}
                    modalVisible={this.state.acStandbyTimeVis}
                    title={strings('ac_stand_by_time')}
                    sliderValue={parseFloat(this.state.acStandbyTimeValue1)}
                    valueUnit={strings('hour')}
                    onValueChange={(value) => {
                        acStandbyTime = value
                        this.setState({acStandbyTimeValue1: value})
                    }}
                    onSlidingComplete={(value) => {
                        acStandbyTime = value
                        this.setState({acStandbyTimeValue1: value})
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({acStandbyTimeVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({acStandbyTimeVis: false})
                        if (acStandbyTime != undefined) {
                            this.setState({
                                acStandbyTimeValue: acStandbyTime,
                                acStandbyTimeTime: new Date().getTime()
                            })
                            SpecUtil._sendAcStandbyTime(this.state.account, this.state.pk, this.state.mac, acStandbyTime)
                        }
                    }}/>

                <DeviceSettingSlideRangeView
                    minimumValue={0}
                    maximumValue={100}
                    sliderMinValue={parseInt(this.state.batteryProtectRangeMin1)}
                    sliderMaxValue={parseInt(this.state.batteryProtectRangeMax1)}
                    // onMinValueChange={(value) => {
                    //     this.setState({batteryProtectRangeMin: value})
                    // }}
                    // onMinSlidingComplete={(value) => {
                    //     this.setState({batteryProtectRangeMin: value})
                    //     SpecUtil._sendProtectRange(this.state.account, this.state.pk, this.state.mac, value, this.state.batteryProtectRangeMax)
                    // }}
                    // onMaxValueChange={(value) => {
                    //     this.setState({batteryProtectRangeMax: value})
                    // }}
                    // onMaxSlidingComplete={(value) => {
                    //     this.setState({batteryProtectRangeMax: value})
                    //     SpecUtil._sendProtectRange(this.state.account, this.state.pk, this.state.mac, this.state.batteryProtectRangeMin, value)
                    // }}

                    onSlider1ValueChange={(index) => {
                        etcMin = index
                        this.setState({batteryProtectRangeMin1: index})
                    }}
                    onSlider1ValueComplete={(index) => {
                        etcMin = index
                        this.setState({batteryProtectRangeMin1: index})
                    }}
                    onSlider2ValueChange={(index) => {
                        etcMax = index
                        this.setState({batteryProtectRangeMax1: index})
                    }}
                    onSlider2ValueComplete={(index) => {
                        etcMax = index
                        this.setState({batteryProtectRangeMax1: index})
                    }}
                    step={1}
                    onRequestClose={() => {
                        this.setState({batteryProtectRangeVis: false})
                    }}
                    overViewClick={() => {
                        this.setState({batteryProtectRangeVis: false})
                    }}
                    modalVisible={this.state.batteryProtectRangeVis}
                    title={strings('elc_protect_range')}
                    valueUnit={'%'}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({batteryProtectRangeVis: false})
                    }}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#FF8748'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({batteryProtectRangeVis: false})

                        if (etcMin >= parseInt(this.state.elcValue)) {
                            //低电量保护值不可大于等于当前电量！
                            this.onShowToast(strings('low_bat_set_tips'))
                        } else {
                            if (etcMin != -1 && etcMax != -1) {
                                this.setState({
                                    batteryProtectRangeMin: etcMin,
                                    batteryProtectRangeMax: etcMax,
                                    batteryProtectRangeTime: new Date().getTime()
                                })
                                SpecUtil._sendProtectRange(this.state.account, this.state.pk, this.state.mac, (etcMin), (etcMax))
                            }
                        }
                    }}/>

                <UpdateDeviceNameDialog
                    onRequestClose={() => {
                        this.setState({updateNameDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({updateNameDialog: false})
                    }}
                    modalVisible={this.state.updateNameDialog}
                    title={strings('update_device_nick_name')}
                    leftBtnText={strings('update')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //确定
                        if (this.state.inputNameValue == '') {
                            return
                        }

                        this.setState({updateNameDialog: false})

                        if (parseInt(this.state.uType) == -1) {
                            //蓝牙设备修改名称
                            this._updateBleDeviceName(this.state.inputNameValue.replace(/(^\s*)|(\s*$)/g, ""))
                        } else {
                            //去除左右两边空格
                            this._updateDeviceName(this.state.inputNameValue.replace(/(^\s*)|(\s*$)/g, ""), this.state.deviceId, this.state.token)
                        }
                    }}
                    value={this.state.inputNameValue}
                    onChangeText={(text) => {
                        this.setState({inputNameValue: text})
                    }}
                    placeholder={strings('device_nick_name')}
                    placeholderTextColor={'rgba(0,0,0,0.26)'}/>

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({unbindDeviceDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({unbindDeviceDialog: false})
                    }}
                    modalVisible={this.state.unbindDeviceDialog}
                    title={strings('unbind_device')}
                    message={strings('unbind_device_tips')}
                    leftBtnText={strings('comfire')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({unbindDeviceDialog: false})
                        if (parseInt(this.state.uType) == -1) {
                            //蓝牙设备解绑
                            this._bleDeviceUnbind()
                        } else {
                            this._unbindDevice(this.state.deviceId, this.state.token)
                        }
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>
            </View>
        )
    }

    _updateBleDeviceName(name) {
        var bleDevices = MainDeviceList.getBleDevices()
        var tempBleDevices = []
        bleDevices.map((item, index) => {
            if (item.mac == this.state.mac) {
                var itemData = item
                itemData.deviceName = name
                tempBleDevices.push(itemData)
            } else {
                tempBleDevices.push(item)
            }
        })
        var param = {
            bleDevice: tempBleDevices
        }
        if (Helper.dataIsNull(this.state.token)) {
            return
        }
        NetUtil.setUserParams(this.state.token, param, true)
            .then((res) => {
                MainDeviceList.setBleDevices(tempBleDevices)
                this.onShowToast(strings('update_success'))
                this.setState({deviceName: name})
                EventUtil.sendEvent(EventUtil.UPDATE_DEVICE_NAME_SUCCESS, name)
            })
    }

    _bleDeviceUnbind() {
        var bleDevices = MainDeviceList.getBleDevices()
        var tempBleDevices = []
        bleDevices.map((item, index) => {
            if (item.mac != this.state.mac) {
                tempBleDevices.push(item)
            }
        })
        var param = {
            bleDevice: tempBleDevices
        }
        if (Helper.dataIsNull(this.state.token)) {
            return
        }
        NetUtil.setUserParams(this.state.token, param, true)
            .then((res) => {
                RouterUtil.toMainDeviceList(this.state.token, true)
            })
    }

    //解绑设备
    _unbindDevice(deviceId, token) {
        if (deviceId == '' || token == '') {
            return
        }
        NetUtil.unbindDevice(token, deviceId, true)
            .then((res) => {
                RouterUtil.toMainDeviceList(token)
            })
            .catch((error) => {
                this.onShowToast(strings('failed'))
            })
    }

    //设备昵称
    _deviceName() {
        return (
            <ShadowCardView
                style={{
                    height: 56,
                    marginTop: 20
                }}>
                <ListItemView
                    title={strings('device_nick_name')}
                    value={this.state.deviceName}
                    onPress={() => {
                        //设备昵称
                        this.setState({
                            inputNameValue: this.state.deviceName,
                            updateNameDialog: true
                        })
                    }}/>
            </ShadowCardView>
        )
    }

    //修改设备名称
    _updateDeviceName(updateName, deviceId, token) {
        if (deviceId == '' || token == '' || updateName.toString().trim() == this.state.deviceName.toString().trim()) {
            return
        }

        NetUtil.updateDeviceName(updateName, token, deviceId, true)
            .then((res) => {
                this.onShowToast(strings('update_success'))
                this.setState({deviceName: updateName})
                EventUtil.sendEvent(EventUtil.UPDATE_DEVICE_NAME_SUCCESS, updateName)
            })
            .catch((error) => {
                this.onShowToast(strings('update_fail'))
            })

    }

    //蜂鸣开关
    _beePowerName() {
        return (
            <ShadowCardView
                style={{
                    height: 56,
                    marginTop: 15
                }}>
                <ListItemView
                    hideArrowIcon={true}
                    title={strings('bee_power')}
                    isCheck={this.state.beePower}
                    onCheck={() => {
                        //蜂鸣开关
                        if (!this.state.isWifiOnline) {
                            return
                        }

                        this.setState({
                            beePower: !this.state.beePower,
                            beePowerTime: new Date().getTime()
                        })
                        SpecUtil._sendBeeSwitch(this.state.account, this.state.pk, this.state.mac, !this.state.beePower)
                    }}/>
            </ShadowCardView>
        )
    }

    _getChargeTypeText(chargeStatus) {
        //充电状态优化为3个：0-慢充，1-快充1500w，2-快充1800w，3-快充3000w
        var chargeText = strings('slow_charge')
        switch (parseInt(chargeStatus)) {
            case 0:
                chargeText = strings('slow_charge')
                break
            case 1:
                chargeText = strings('quick_charge_a')
                break
            case 2:
                chargeText = strings('quick_charge_b')
                break
            case 3:
                chargeText = strings('quick_charge_c')
                break
        }
        return chargeText
    }

    _view1() {
        return (
            <ShadowCardView
                style={{
                    marginTop: 15
                }}>
                <ListItemView
                    rightPadding={true}
                    hideArrowIcon={true}
                    title={strings('charge_status')}
                    value={this._getChargeTypeText(this.state.chargeStatus)}
                />

                <ListItemView
                    rightPadding={true}
                    hideArrowIcon={this.state.chargeStatus == 0}
                    title={strings('ac_charge_power')}
                    value={(this.state.chargeStatus == 0 ? '500' : Helper.dataNanReset(this.state.acChargePowerValue)) + powerUnit}
                    onPress={() => {
                        //AC充电功率
                        if (!this.state.isWifiOnline || this.state.chargeStatus == 0) {
                            return
                        }

                        acPower = this.state.acChargePowerValue
                        this.setState({acChargePowerValue1: this.state.acChargePowerValue})

                        this.setState({acChargePowerVis: true})
                    }}/>
            </ShadowCardView>
        )
    }

    _view2() {
        return (
            <ShadowCardView
                style={{
                    marginTop: 15
                }}>
                <ListItemView
                    title={strings('elc_protect_range')}
                    value={Helper.dataNanReset(parseInt(parseInt(this.state.batteryProtectRangeMin))) + '%-' + Helper.dataNanReset(parseInt(parseInt(this.state.batteryProtectRangeMax))) + '%'}
                    onPress={() => {
                        //电池保护范围
                        if (!this.state.isWifiOnline) {
                            return
                        }
                        etcMin = this.state.batteryProtectRangeMin
                        etcMax = this.state.batteryProtectRangeMax
                        this.setState({
                            batteryProtectRangeMin1: this.state.batteryProtectRangeMin,
                            batteryProtectRangeMax1: this.state.batteryProtectRangeMax
                        })

                        this.setState({batteryProtectRangeVis: true})
                    }}/>
                {/*<ListItemView*/}
                {/*title={strings('light_charge_power')}*/}
                {/*value={this.state.pvChargePowerValue + powerUnit}*/}
                {/*onPress={() => {*/}
                {/*//光伏充电功率*/}
                {/*if (!this.state.isWifiOnline) {*/}
                {/*return*/}
                {/*}*/}

                {/*pvPower = this.state.pvChargePowerValue*/}
                {/*this.setState({pvChargePowerValue1: this.state.pvChargePowerValue})*/}

                {/*this.setState({pvChargePowerVis: true})*/}
                {/*}}/>*/}
                {/*<ListItemView*/}
                {/*title={strings('wind_charge_power')}*/}
                {/*value={this.state.fanChargePowerValue + powerUnit}*/}
                {/*onPress={() => {*/}
                {/*//风力充电功率*/}
                {/*if (!this.state.isWifiOnline) {*/}
                {/*return*/}
                {/*}*/}

                {/*windPower = this.state.fanChargePowerValue*/}
                {/*this.setState({fanChargePowerValue1: this.state.fanChargePowerValue})*/}

                {/*this.setState({fanChargePowerVis: true})*/}
                {/*}}/>*/}
            </ShadowCardView>
        )
    }

    _view3() {
        return (
            <ShadowCardView
                style={{
                    marginTop: 15
                }}>
                <ListItemView
                    title={strings('device_stand_by_time')}
                    value={this.state.deviceStandbyTimeValue == 0 ? 'N/O' : (Helper.dataNanReset(this.state.deviceStandbyTimeValue) + strings('hour'))}
                    onPress={() => {
                        //设备待机时间
                        if (!this.state.isWifiOnline) {
                            return
                        }

                        standbyTime = this.state.deviceStandbyTimeValue
                        this.setState({deviceStandbyTimeValue1: this.state.deviceStandbyTimeValue})

                        this.setState({deviceStandbyTimeVis: true})
                    }}/>
                <ListItemView
                    title={strings('screen_off_time')}
                    value={this.state.screenOffTimeValue == 0 ? 'N/O' : (Helper.dataNanReset(this.state.screenOffTimeValue) + strings('min'))}
                    onPress={() => {
                        //息屏时间
                        if (!this.state.isWifiOnline) {
                            return
                        }

                        screenOffTime = this.state.screenOffTimeValue
                        this.setState({screenOffTimeValue1: this.state.screenOffTimeValue})

                        this.setState({screenOffTimeVis: true})
                    }}/>
                <ListItemView
                    title={strings('ac_stand_by_time')}
                    value={this.state.acStandbyTimeValue == 0 ? 'N/O' : (Helper.dataNanReset(this.state.acStandbyTimeValue) + strings('hour'))}
                    onPress={() => {
                        //交流待机时间
                        if (!this.state.isWifiOnline) {
                            return
                        }

                        acStandbyTime = this.state.acStandbyTimeValue
                        this.setState({acStandbyTimeValue1: this.state.acStandbyTimeValue})

                        this.setState({acStandbyTimeVis: true})
                    }}/>
            </ShadowCardView>
        )
    }

    _view4() {
        return (
            <ShadowCardView
                style={{
                    marginTop: 15
                }}>
                {
                    this.state.uType != Helper.DEVICE_OWN_TYPE_MEMBER && this.state.isWifiConnect ?
                        <ListItemView
                            title={strings('device_share')}
                            onPress={() => {
                                //设备分享
                                this.props.navigation.navigate('DeviceShare', {
                                    deviceId: this.state.deviceId,
                                    token: this.state.token,
                                });
                            }}/> : null
                }

                {
                    this.state.isWifiConnect ?
                        <ListItemView
                            title={strings('fireware_update')}
                            onPress={() => {
                                //固件升级
                                if (!this.state.isWifiOnline) {
                                    return
                                }

                                this.props.navigation.navigate('DeviceFirewareUpdate', {
                                    token: this.state.token,
                                    deviceId: this.state.deviceId,
                                });
                            }}/> : null
                }


                <ListItemView
                    title={strings('use_help')}
                    onPress={() => {
                        //使用帮助
                        this.props.navigation.navigate('SelectDeviceList', {type: 0});
                    }}/>
                <ListItemView
                    title={strings('device_spec')}
                    onPress={() => {
                        //设备规格
                        this.props.navigation.navigate('DeviceSpecifications', {
                            specData: this.state.specData,
                            mac: this.state.mac,
                            showMac: this.state.showMac
                        });
                    }}/>
            </ShadowCardView>
        )
    }

    _unbindBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //解绑
                    this.setState({unbindDeviceDialog: true})
                }}
                style={{
                    marginBottom: 25,
                    width: this.mScreenWidth - 50,
                    marginTop: 30,
                    backgroundColor: '#FF8748'
                }}
                btnText={strings('unbind_device')}/>
        )
    }

    //蓝牙相关事件
    _bleEvent() {
        SpecUtil.initBleProps(false)
        this._bleListener()
    }

    _bleListener() {
        //蓝牙状态监听
        this.updateStateListener = BluetoothManager.addListener(
            "BleManagerDidUpdateState",
            this.handleUpdateState
        );

        if (Platform.OS === 'android') {
            //监听蓝牙数据上报
            this.bleNotifyDataListener = DeviceEventEmitter.addListener(EventUtil.BLE_NOTIFY_DATA, (res) => {
                BluetoothManager._bleLog("蓝牙数据上报:" + JSON.stringify(res));
                this._bleDataResolve(res.data)
            });
            //监听蓝牙通知状态
            this.bleNotifyListener = DeviceEventEmitter.addListener(EventUtil.BLE_NOTIFY_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙通知状态:" + JSON.stringify(res));
                if (res.status == 'onNotifySuccess') {
                    BluetoothManager._bleLog("onNotifySuccess蓝牙通知成功");
                } else if (res.status == 'onNotifyFailure') {
                    BluetoothManager._bleLog("onNotifyFailure蓝牙通知失败");
                }
            });
            //监听蓝牙连接状态
            this.bleConnectListener = DeviceEventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(res));
                if (res.status == 'onStartConnect') {
                    BluetoothManager._bleLog("onStartConnect开始连接");
                } else if (res.status == 'onConnectFail') {
                    BluetoothManager._bleLog("onConnectFail连接失败");

                } else if (res.status == 'onConnectSuccess') {
                    BluetoothManager._bleLog("onConnectSuccess连接成功");

                } else if (res.status == 'onDisConnected') {
                    BluetoothManager._bleLog("onDisConnected连接中断");

                }
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            //监听蓝牙数据上报
            eventEmitter.addListener(EventUtil.BLE_NOTIFY_DATA, (res) => {
                BluetoothManager._bleLog("蓝牙数据上报:" + JSON.stringify(res));
                this._bleDataResolve(res)
            });
            //监听蓝牙通知状态
            eventEmitter.addListener(EventUtil.BLE_NOTIFY_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙通知状态:" + JSON.stringify(res));
                if (res.status == 'onNotifySuccess') {
                    BluetoothManager._bleLog("onNotifySuccess蓝牙通知成功");
                } else if (res.status == 'onNotifyFailure') {
                    BluetoothManager._bleLog("onNotifyFailure蓝牙通知失败");
                }
            });
            //监听蓝牙连接状态
            eventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(res));
                if (res) {
                    BluetoothManager._bleLog("onConnectSuccess连接成功");
                } else {
                    BluetoothManager._bleLog("onConnectFail连接失败");
                }
            });
        }
    }

    _bleDestory() {
        this.updateStateListener && this.updateStateListener.remove();
        this.bleConnectListener && this.bleConnectListener.remove();
        this.bleNotifyListener && this.bleNotifyListener.remove();
        this.bleNotifyDataListener && this.bleNotifyDataListener.remove();
    }

    //蓝牙状态改变
    handleUpdateState = (args) => {
        BluetoothManager._bleLog("蓝牙状态改变:" + JSON.stringify(args));
        BluetoothManager.bluetoothState = args.state;
        if (args.state == "on") {
        } else if (args.state == "off") {

        }
    };

    _bleDataResolve(hexData) {
        try {
            if (hexData == '' || hexData == null || hexData == undefined) {
                return
            }
            var resInfo = Bluetooth._hex2Json(hexData)
            var specDatas = SpecUtil.resolveSpecs(resInfo, false)

            this.setState({
                chargeStatus: !Helper.dataIsNull(specDatas.chargeStatus) ? specDatas.chargeStatus : this.state.chargeStatus,
                elcValue: !Helper.dataIsNull(specDatas.curRemaining) ? specDatas.curRemaining : this.state.elcValue,
            })
            if (this._canGetSpecData(this.state.beePowerTime)) {
                this.setState({
                    beePower: !Helper.dataIsNull(specDatas.beeSwitch) ? specDatas.beeSwitch : this.state.beePower,
                })
            }
            if (this._canGetSpecData(this.state.acChargePowerTime)) {
                this.setState({
                    acChargePowerValue: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue,
                })
            }
            if (this._canGetSpecData(this.state.pvChargePowerTime)) {
                this.setState({
                    pvChargePowerValue: !Helper.dataIsNull(specDatas.pvChargePower) ?
                        parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePowerValue
                        : this.state.pvChargePowerValue,
                })
            }
            if (this._canGetSpecData(this.state.fanChargePowerTime)) {
                this.setState({
                    fanChargePowerValue: !Helper.dataIsNull(specDatas.fanChargePower) ?
                        parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePowerValue
                        : this.state.fanChargePowerValue,
                })
            }
            if (this._canGetSpecData(this.state.deviceStandbyTimeTime)) {
                this.setState({
                    deviceStandbyTimeValue: !Helper.dataIsNull(specDatas.deviceStandbyTime) ?
                        parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandbyTimeValue
                        : this.state.deviceStandbyTimeValue,
                })
            }
            if (this._canGetSpecData(this.state.screenOffTimeTime)) {
                this.setState({
                    screenOffTimeValue: !Helper.dataIsNull(specDatas.screenOffTime) ?
                        parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTimeValue
                        : this.state.screenOffTimeValue,
                })
            }
            if (this._canGetSpecData(this.state.acStandbyTimeTime)) {
                this.setState({
                    acStandbyTimeValue: !Helper.dataIsNull(specDatas.acStandbyTime) ?
                        parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandbyTimeValue
                        : this.state.acStandbyTimeValue,
                })
            }
            if (this._canGetSpecData(this.state.batteryProtectRangeTime)) {
                this.setState({
                    batteryProtectRangeMin: !Helper.dataIsNull(specDatas.batteryProtectRangeMin) ?
                        parseInt(specDatas.batteryProtectRangeMin) >= 0 ? (parseInt(specDatas.batteryProtectRangeMin)) : this.state.batteryProtectRangeMin
                        : this.state.batteryProtectRangeMin,
                    batteryProtectRangeMax: !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? (parseInt(specDatas.batteryProtectRangeMax)) : this.state.batteryProtectRangeMax,
                })
            }


            if (!this.state.acChargePowerVis && this._canGetSpecData(this.state.acChargePowerTime)) {
                this.setState({
                    acChargePowerValue1: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue1,
                })
                acPower = !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePowerValue1
            }
            if (!this.state.pvChargePowerVis && this._canGetSpecData(this.state.pvChargePowerTime)) {
                pvPower = !Helper.dataIsNull(specDatas.pvChargePower) ?
                    parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePowerValue1
                    : this.state.pvChargePowerValue1
                this.setState({
                    pvChargePowerValue1: pvPower,
                })
            }
            if (!this.state.fanChargePowerVis && this._canGetSpecData(this.state.fanChargePowerTime)) {
                windPower = !Helper.dataIsNull(specDatas.fanChargePower) ?
                    parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePowerValue1
                    : this.state.fanChargePowerValue1
                this.setState({
                    fanChargePowerValue1: windPower,
                })
            }
            if (!this.state.deviceStandbyTimeVis && this._canGetSpecData(this.state.deviceStandbyTimeTime)) {
                standbyTime = !Helper.dataIsNull(specDatas.deviceStandbyTime) ?
                    parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandbyTimeValue1
                    : this.state.deviceStandbyTimeValue1
                this.setState({
                    deviceStandbyTimeValue1: standbyTime,
                })
            }
            if (!this.state.screenOffTimeVis && this._canGetSpecData(this.state.screenOffTimeTime)) {
                screenOffTime = !Helper.dataIsNull(specDatas.screenOffTime) ?
                    parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTimeValue1
                    : this.state.screenOffTimeValue1
                this.setState({
                    screenOffTimeValue1: screenOffTime,
                })
            }
            if (!this.state.acStandbyTimeVis && this._canGetSpecData(this.state.acStandbyTimeTime)) {
                acStandbyTime = !Helper.dataIsNull(specDatas.acStandbyTime) ?
                    parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandbyTimeValue1
                    : this.state.acStandbyTimeValue1
                this.setState({
                    acStandbyTimeValue1: acStandbyTime,
                })
            }
            if (!this.state.batteryProtectRangeVis && this._canGetSpecData(this.state.batteryProtectRangeTime)) {
                etcMin = !Helper.dataIsNull(specDatas.batteryProtectRangeMin) ?
                    parseInt(specDatas.batteryProtectRangeMin) >= 0 ? (parseInt(specDatas.batteryProtectRangeMin)) : this.state.batteryProtectRangeMin
                    : this.state.batteryProtectRangeMin
                etcMax = !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? parseInt(specDatas.batteryProtectRangeMax) : this.state.batteryProtectRangeMax
                this.setState({
                    batteryProtectRangeMin1: etcMin,
                    batteryProtectRangeMax1: etcMax,
                })
            }
        } catch (e) {
            LogUtil.debugLog('_bleDataResolve_error:' + JSON.stringify(e))
        }
    }
}