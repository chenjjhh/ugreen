// 主页设备列表/添加设备/发现设备列表
import React from 'react';
import {
    Image,
    NativeModules,
    Platform,
    RefreshControl,
    ScrollView,
    StatusBar,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import CommonBtnView from '../View/CommonBtnView'

var GXRNManager = NativeModules.GXRNManager
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = (require('../../resources/greenUnion/nav_help_ic.png'))
const leftIcon = (require('../../resources/back_black_ic.png'))

export default class NewAddDevice extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
            <TitleBar
                    titleText={strings('add_device')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={leftIcon}
                    rightIcon={rightIcon}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    rightIconClick={() => {
                        // 客服   
                    }}

                />
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            data:[
                // {name:'G5600户外电源',deviceNo:'6A:G2:D6:3W:1D:79',deviceId:0},
                // {name:'G5600户外电源',deviceNo:'6A:G2:D6:3W:1D:79',deviceId:1},
                // {name:'G5600户外电源',deviceNo:'6A:G2:D6:3W:1D:79',deviceId:2},
                // {name:'G5600户外电源',deviceNo:'6A:G2:D6:3W:1D:79',deviceId:3},
            ],
            refreshing:false,
      }
    }

    // 手动添加
    addManully = () =>{

    }

    // 下拉刷新
    onRefresh = ()=>{

    }

    // 选择网络
    chooseNetwork=(item)=>{
        this.props.navigation.navigate('chooseNetwork')
    }
    
    render() {
        const { data,refreshing } = this.state
        // 手动添加按钮
        const addManullyBtnView = (
            <View style={{width:'100%',alignItems:'center'}}>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 手动添加
                        this.addManully()
                    }}
                    style={{
                        backgroundColor: '#18B34F',
                        width:(this.mScreenWidth-40)*0.9,
                        marginVertical:20
                    }}
                    btnText={strings('手动添加')}/>
            </View>)

        // 暂无设备视图
        const noDataVive = (
            <View style={{width:'100%',height:this.mScreenHeight/2,alignItems:'center',justifyContent:'center'}}>
                <Image source={require('../../resources/greenUnion/warning_ic.png')} />
                <Text style={{fontSize:13,color:'#BEBFC5',lineHeight:50}}>{strings('未发现设备')}</Text>
            </View>
        )
        return (
            <View style={{flex:1,backgroundColor:'#F7F7F7',height:this.mScreenHeight,width:this.mScreenWidth,justifyContent:'space-between'}}>
                <View style={{width:'100%',paddingLeft:20}}>
                    <View>
                        <Text style={{fontSize:22,color:'#2F2F2F'}}>{strings('find_device')}</Text>
                        <Image source={require('../../resources/greenUnion/connect_refresh_ic.png')} />
                    </View>
                    <Text style={{fontSize:14,color:'#999BA2'}}>{strings('find_device_tips')}</Text>
                </View>
                <ScrollView 
                    style={{flex:1,}} 
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />
                        }>
                    <View style={{flexDirection:'row',flexWrap:'wrap',marginHorizontal:20,paddingTop:20,}}>
                        {data?.map((item,index)=>(
                            <TouchableOpacity key={index} onPress={(item)=>this.chooseNetwork(item)} style={{width:(this.mScreenWidth-40)/3,alignItems:'center',marginBottom:20}}>
                                <Image source={require('../../resources/greenUnion/list_power_img.png')} />
                                <Text style={{fontSize:12,color:'#2F2F2F'}}>{item.name}</Text>
                                <Text style={{fontSize:8,color:'#808080'}}>{item.deviceNo}</Text>
                            </TouchableOpacity>
                        ))}
                        {data?.length ===0 ? (noDataVive) : null}
                        
                    </View>
                </ScrollView>
                {addManullyBtnView}
            </View>
        );
    }
}