// 电源详情/设备设置/电池健康
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Animated,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  DeviceEventEmitter,
  NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import ChooseDialog from "../View/ChooseDialog";
import SpecUtil from "../Utils/SpecUtil";
import StorageHelper from "../Utils/StorageHelper";

var GXRNManager = NativeModules.GXRNManager;
const { StatusBarManager } = NativeModules;
if (Platform.OS === "ios") {
  StatusBarManager.getHeight((height) => {
    statusBarHeight = height.height;
  });
} else {
  statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;
let keyData = [
  { key1: "chargingLimit", key2: "charging_limit" },
  { key1: "lowerLimit", key2: "lower_limit" },
];
export default class BatteryHealth extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          leftIcon={require("../../resources/back_black_ic.png")}
          leftIconClick={() => {
            navigation.goBack();
          }}
          backgroundColor={"#FBFBFB"}
          titleText={strings("电池健康")}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      deviceId: props.navigation?.state?.params?.deviceId,
      isWifiConnect: true, //是否连接WIFI
      chargingLimit: "", //当前充电上限
      lowerLimit: "", //当前放电下限
    };
    this.chargingLimitData = [
      { text: "50%", id: "0" },
      { text: "70%", id: "1" },
      { text: "85%", id: "2" },
      { text: "90%", id: "3" },
    ];
    this.lowerLimitData = [
      { text: "10%", id: "0" },
      { text: "20%", id: "1" },
      { text: "30%", id: "2" },
      { text: "45%", id: "3" },
    ];
  }

  componentWillMount() {
    const { deviceId } = this.state;
    this._getDeviceProps(deviceId, true, false);
  }

  //获取设备属性
  _getDeviceProps(deviceId, showLoading, hideErrorToast) {
    const { chargingLimit, lowerLimit } = this.state;
    if (Helper.dataIsNull(deviceId)) {
      return;
    }
    NetUtil.getDeviceProps(
      StorageHelper.getTempToken(),
      deviceId,
      showLoading,
      hideErrorToast
    ).then((res) => {
      var specDatas = SpecUtil.resolveSpecs(res.info, false);
      const { charging_limit, lower_limit } = specDatas || {};
      tempSpecDatas = specDatas;
      if (JSON.stringify(specDatas)) {
        // if (JSON.stringify(specDatas) != JSON.stringify(tempSpecDatas)) {
        //充电上限
        if (chargingLimit !== charging_limit) {
          this.setState({
            chargingLimit: !Helper.dataIsNull(charging_limit)
              ? charging_limit
              : chargingLimit,
          });
        }
        //放电下限
        if (lowerLimit !== lower_limit) {
          this.setState({
            lowerLimit: !Helper.dataIsNull(lower_limit)
              ? lower_limit
              : lowerLimit,
          });
        }
      }
    });
  }

  // 选择上下限
  onValueChange = (item, type) => {
    const { isWifiConnect, deviceId } = this.state;
    if (isWifiConnect) {
      NetUtil.deviceControlV3(
        StorageHelper.getTempToken(),
        deviceId,
        Helper.getKey(type,keyData),
        item?.id,
        false
      )
        .then((res) => {
            this.setState({
                [type]: item?.id,
              });
        })
        .catch((error) => {
          throw error;
        });
    }
  };

  // 根据枚举值匹配相应选项文字
  getText = (type, value) => {
    return this[type]?.find((element) => element.id === value)?.text;
  };

  render() {
    const { chargingLimit, lowerLimit } = this.state;
    // 充电上限
    const chargingLimitView = (
      <SwitchTabView
        type={"chargingLimit"}
        title={strings("充电上限")}
        data={this.chargingLimitData}
        currentSelect={this.getText("chargingLimitData", chargingLimit)}
        bottomText={strings("充电上限提示")}
        onPress={this.onValueChange}
      />
    );

    // 放电下限
    const lowerLimitView = (
      <SwitchTabView
        type={"lowerLimit"}
        title={strings("放电下限")}
        data={this.lowerLimitData}
        currentSelect={this.getText("lowerLimitData", lowerLimit)}
        bottomText={strings("放电下限提示")}
        onPress={this.onValueChange}
      />
    );

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#F7F7F7",
          alignItems: "center",
          height: this.mScreenHeight,
          width: this.mScreenWidth,
        }}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ width: this.mScreenWidth, alignItems: "center" }}>
            <View
              style={{
                width: this.mScreenWidth,
                paddingHorizontal: 20,
                alignItems: "center",
              }}
            >
              {chargingLimitView}
              {lowerLimitView}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export class SwitchTabView extends BaseComponent {
  render() {
    const { data, currentSelect, bottomText, title, type, onPress } =
      this.props;
    return (
      <View style={{ marginBottom: 30 }}>
        <View
          style={{
            backgroundColor: "#fff",
            borderRadius: 10,
            width: this.mScreenWidth - 40,
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <View style={{ width: "100%", paddingHorizontal: 15 }}>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text
                style={{
                  fontSize: this.getSize(14),
                  color: "#404040",
                  lineHeight: 55,
                }}
              >
                {title}
              </Text>
              <Text
                style={{
                  fontSize: this.getSize(14),
                  color: "#808080",
                  lineHeight: 55,
                }}
              >
                {currentSelect}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                height: 46,
                width: this.mScreenWidth - 70,
                backgroundColor: "#F1F2F6",
                borderRadius: 23,
                overflow: "hidden",
                paddingHorizontal: 4,
              }}
            >
              {data?.map((item, index) => (
                <TouchableOpacity
                  onPress={() => onPress?.(item,type)}
                  key={index}
                  style={{
                    flex: 1,
                    height: 39,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor:
                      currentSelect === item.text ? "#fff" : "#F1F2F6",
                    borderRadius: 23,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      color:
                        currentSelect === item.text ? "#404040" : "#C4C6CD",
                    }}
                  >
                    {item.text}
                  </Text>
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </View>
        <Text style={{ fontSize: 14, color: "#808080", lineHeight: 25 }}>
          {bottomText}
        </Text>
      </View>
    );
  }
}
