// 主页设备列表/添加设备/连接设备WIFI
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio,ScrollView,DeviceEventEmitter,NativeModules
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings, setLanguage} from '../Language/I18n';
import TextInputComponent from '../View/TextInputComponent'
import CommonBtnView from '../View/CommonBtnView'
// import  LottieView  from 'lottie-react-native';
var GXRNManager = NativeModules.GXRNManager
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
const leftIcon = (require('../../resources/back_black_ic.png'))
const wifiIcon = (require('../../resources/greenUnion/connect_hotspot_ic.png'))
const wifiBgIcon = (require('../../resources/greenUnion/hotspot.png'))

const noSelectIcon = (require('../../resources/greenUnion/unsel_ic.png'))
const selectIcon = (require('../../resources/greenUnion/sel_ic.png'))
const deleteIcon = (require('../../resources/greenUnion/input_deleteall_ic.png'))


export default class ConnectingDeviceWifi extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
            <TitleBar
            titleText={strings('添加设备')}
            backgroundColor={'#FBFBFB'}
            leftIcon={leftIcon}
            rightIcon={rightIcon}
            leftIconClick={() => {
                navigation.goBack();
            }}
        />
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            deviceWifi: 'WiFi-001',//设备热点
            passWord:'12345678',//密码
            mobilePhoneWifi:'WiFi-002',//手机当前连接wifi
      }
    }

    // 设备配网
    deviceDistributionNetwork = ()=>{
       

    }

    // 直接使用
    useDirectly = ()=>{
        
    }
    
    
    render() {
        const { deviceWifi, passWord, mobilePhoneWifi } = this.state
        const topView = (
            <View style={{width:'100%',alignItems:'center'}}>
                <View style={{width:'100%',marginBottom:40,alignItems:'center'}}>
                    <Image style={{height:60,width:60}} source={wifiIcon}/>
                    <Text style={{fontSize:18,color:'#404040',lineHeight:24,fontWeight:'bold'}}>{strings('连接设备WiFi')}</Text>
                </View>
                <Text style={{fontSize:14,color:'#999BA2',lineHeight:24}}>{strings('连接Wifi提示')}</Text>
                <View style={{width:'100%',borderRadius:11,overflow:'hidden',alignItems:'center',paddingVertical:10,marginVertical:20}}>
                    <Image resizeMode='contain' style={{width:this.mScreenWidth-50,height:(this.mScreenWidth-50)*0.38}} source={wifiBgIcon}/>
                    <View style={{position:'absolute',top:'50%',left:50}}>
                        <Text style={{fontSize:16,color:'#fff',lineHeight:22,fontWeight:'bold'}}>{deviceWifi}</Text>
                    </View>
                </View>
                <View style={{width:'100%',borderBottomColor:'rgba(0,0,0,0.07)',borderBottomWidth:0.5}}>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:14,color:'#999BA2',lineHeight:24,}}>{strings('设备热点')}</Text>
                        <View style={{flex:1,alignItems:'flex-end'}}>
                            <Text style={{fontSize:14,color:'#999BA2',lineHeight:24,}} numberOfLines={1}>{deviceWifi}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:7}}>
                        <Text style={{fontSize:14,color:'#999BA2',lineHeight:24,}}>{strings('密码')}</Text>
                        <View style={{flex:1,alignItems:'flex-end'}}>
                            <Text style={{fontSize:14,color:'#999BA2',lineHeight:24,}} numberOfLines={1}>{passWord}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:7}}>
                        <Text style={{fontSize:14,color:'#999BA2',lineHeight:34,}}>{strings('手机当前连接wifi')}</Text>
                        <View style={{flex:1,alignItems:'flex-end'}}>
                            <Text style={{fontSize:14,color:'#999BA2',lineHeight:34,}} numberOfLines={1}>{mobilePhoneWifi}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:7}}>
                        <Text style={{fontSize:14,color:'#999BA2',lineHeight:24,}}>{strings('未发现WiFi提示')}</Text>
                    </View>
                </View>
            </View>
        )

        const deviceDistributionNetworkBtnView = (
            <View>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        this.deviceDistributionNetwork()
                    }}
                    style={{
                        backgroundColor: '#F1F2F6',
                        width:(this.mScreenWidth-40)*0.9,
                        marginBottom:20
                    }}
                    textStyle={{color:'#808080'}}
                    btnText={strings('device_connecttion')}/>
            </View>
                )

        const useDirectlyBtnView = (
            <View>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        this.useDirectly()
                    }}
                    style={{
                        backgroundColor: '#18B34F',
                        width:(this.mScreenWidth-40)*0.9,
                    }}
                    btnText={strings('dir_use')}/>
            </View>
                )
        
        return (
            <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',height:this.mScreenHeight,width:this.mScreenWidth}}>
                <ScrollView style={{flex:1,}}>
                    <View style={{width:this.mScreenWidth,alignItems:'center'}}>
                        <View style={{width:this.mScreenWidth,paddingHorizontal:20,alignItems:'center'}}>
                            <View 
                                style={{
                                overflow: 'hidden',
                                width:this.mScreenWidth-40,
                                height:this.mScreenHeight,
                                paddingVertical:25,
                                justifyContent:'space-between',
                                alignItems:'center',
                                }}>
                                <View style={{width:'100%'}}>
                                    {topView}
                                </View>
                                <View style={{width:'100%',alignItems:'center'}}>
                                    {deviceDistributionNetworkBtnView}
                                    {useDirectlyBtnView}
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}