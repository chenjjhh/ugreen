import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput, PermissionsAndroid, AppState
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import PagerTitleView from "../View/PagerTitleView";
import DeviceCard from "../View/DeviceCard";
import Swiper from "react-native-swiper";
import ViewHelper from '../View/ViewHelper'
import AcceptDeviceShareDialog from "../View/AcceptDeviceShareDialog";
import StorageHelper from "../Utils/StorageHelper";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import ShadowCardView from "../View/ShadowCardView";
import ListItemView from "../View/ListItemView";
import EventUtil from "../Event/EventUtil";
import I18n from "react-native-i18n";
import BleModule from "../Bluetooth/BleModule";
import Bluetooth from "../Bluetooth/Bluetooth";
import MessageDialog from "../View/MessageDialog";
import LogUtil from "../Utils/LogUtil";

global.BluetoothManager = new BleModule();

/*
*添加设备
 */

const daySec = 3600 * 24
let that
var GXRNManager = NativeModules.GXRNManager
var tempDeviceShareInfo = null

export default class AddDevice extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null
        };
    }

    _checkPermission() {
        if (!this.state.bleOnOff) {
            //蓝牙未开启
            this.setState({bleTipsDialog: true})
            return
        }
        if (Platform.OS === 'android') {
            this._androidOpenGps()
        } else {
            this._toBleConnect()
        }
    }

    _toBleConnect() {
        this.props.navigation.navigate('BleAddDevice');
    }

    _androidOpenGps() {
        NativeModules.RNModule.isLocServiceEnable((result) => {
            if (!result) {
                //安卓未开启定位
                this.setState({gpsTipsDialog: true})
            } else {
                this.checkPermission()
            }
        });
    }

    async checkPermission() {
        const result = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        const result1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
        if (result && result1) {
            this._toBleConnect()
        } else {
            this.setState({locationTipsDialog: true})
        }
    }

    //安卓手机获取当前wifi名称需要获取定位权限
    async requestPermission() {
        try {
            PermissionsAndroid.requestMultiple(
                [
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION],
                {
                    'title': 'Wifi networks',
                    'message': 'We need your permission in order to find wifi networks'
                }
            ).then((result) => {
                if (result['android.permission.ACCESS_COARSE_LOCATION']
                    && result['android.permission.ACCESS_FINE_LOCATION'] === 'granted') {
                    LogUtil.debugLog("Thank you for your permission! :ACCESS_FINE_LOCATION)");
                    this._toBleConnect()
                } else if (result['android.permission.ACCESS_COARSE_LOCATION']
                    || result['android.permission.ACCESS_FINE_LOCATION'] === 'never_ask_again') {
                    this.onShowToast('Please Go into Settings -> Applications -> APP_NAME -> Permissions and Allow permissions to continue');
                }
            });
        } catch (err) {
            LogUtil.debugLog(err)
        }
    }

    constructor(props) {
        super(props);
        that = this
        this.state = {
            tabIndex: 0,
            bottomContainerViewHeight: 0,
            powerDeviceArray: [
                //[{deviceTypeName: strings('device_product_name')}]
            ],
            cleanDeviceArray: [],
            shareToMeDeviceArray: [],
            shareDialogVis: false,
            token: this.props.navigation.state.params.token,

            selectDeviceId: '',
            selectMac: '',
            selectUserName: 'Johnson',
            sharePushList: [],

            shareDeviceAlias: '',//分享的产品类型
            shareUserName: '',
            deviceName: '',
            shareDeviceImg: '',
            shareOrder: '',//分享口令
            productArray: [],
            bleOnOff: false,
            getFlag: false,

            bleTipsDialog: false,
            gpsTipsDialog: false,
            locationTipsDialog: false,
        }
    }

    componentWillMount() {
        tempDeviceShareInfo = null
        BluetoothManager.start(); //蓝牙初始化、检查蓝牙状态

        this._setTempData()
        this._getSharePushList()
        this._getProductList()

        //蓝牙状态监听
        this.updateStateListener = BluetoothManager.addListener(
            "BleManagerDidUpdateState",
            this.handleUpdateState
        );

        this.delayTimer = setTimeout(() => {
            //开始轮询
            //this._startTimerGetShareList()
        }, 3000);


        this.appStateListener = AppState.addEventListener('change', (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === 'active') { // 后台进入前台
                this._startTimerGetShareList()
            } else {
                this._stopTimerGetShareList()
            }
        });// 监听APP状态  前台、后台
        this.toLoginListener = DeviceEventEmitter.addListener(EventUtil.TO_LOGIN, (value) => {
            this._stopTimerGetShareList()
        });

        this._bleDisConnectDevice()

        this.routesListener = DeviceEventEmitter.addListener(EventUtil.ROUTES_LENGTH, (message) => {
            // 监听路由长度
            if (parseInt(message) == 2) {
                this._startTimerGetShareList()
            } else {
                this._stopTimerGetShareList()
            }
        });
    }

    _bleDisConnectDevice() {
        if (Platform.OS === "android") {
            NativeModules.RNBLEModule.disConnectDevice()
        } else {
            GXRNManager.disConnectDevice();
        }
    }

    _startTimerGetShareList() {
        this._stopTimerGetShareList()
        this.intervalGetShareList = setInterval(() => {
            this._getSharePushList()
        }, 3000)
    }

    _stopTimerGetShareList() {
        if (this.intervalGetShareList) {
            clearInterval(this.intervalGetShareList)
            this.intervalGetShareList = null
        }
    }

    //蓝牙状态改变
    handleUpdateState = (args) => {
        BluetoothManager._bleLog("蓝牙状态改变:" + JSON.stringify(args));
        BluetoothManager.bluetoothState = args.state;

        if (args.state == "unknow") {
            return
        }
        this.setState({
            bleOnOff: args.state == "on"
        })
    };

    componentWillUnmount() {
        this.delayTimer && clearTimeout(this.delayTimer);
        this.toLoginListener && this.toLoginListener.remove()
        this.updateStateListener && this.updateStateListener.remove();
        this.appStateListener && this.appStateListener.remove()
        this._stopTimerGetShareList()
        this.routesListener && this.routesListener.remove()
    }

    _getProductList() {
        if (this.state.token) {
            NetUtil.getProductList(this.state.token, true)
                .then((res) => {
                    if (res.list && res.list.length > 0) {
                        var tempArray = []
                        var resList = res.list
                        resList.map((item, index) => {
                            if (parseInt(item.status) == 1)
                                tempArray.push(item)
                        })
                        var productArray = Helper._resolveAllProductInfo(tempArray.sort(Helper.compare('createTime')))
                        if (productArray.length > 0) {
                            this.setState({
                                productArray: productArray,
                                getFlag: true
                            })
                        }
                    }
                })
                .catch((error) => {
                    this.setState({getFlag: true})
                })
        }
    }

    _setTempData() {
        var tempPowerDeviceArray = [
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT4'}]
        ]
        var tempCleanDeviceArray = [
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT4'}, {deviceTypeName: 'VOGEE XT5'}]
        ]
        var tempShareToMeDeviceArray = [
            [{deviceTypeName: 'VOGEE XT1', isNewShare: true}, {
                deviceTypeName: 'VOGEE XT1',
                isNewShare: false
            }, {deviceTypeName: 'VOGEE XT1', isNewShare: true}],
            [{deviceTypeName: 'VOGEE XT1', isNewShare: false}, {
                deviceTypeName: 'VOGEE XT1',
                isNewShare: false
            }, {deviceTypeName: 'VOGEE XT1', isNewShare: true}],
            [{deviceTypeName: 'VOGEE XT1', isNewShare: false}, {
                deviceTypeName: 'VOGEE XT1',
                isNewShare: false
            }, {deviceTypeName: 'VOGEE XT1', isNewShare: true}],
            [{deviceTypeName: 'VOGEE XT1', isNewShare: false}, {
                deviceTypeName: 'VOGEE XT1',
                isNewShare: false
            }, {deviceTypeName: 'VOGEE XT1', isNewShare: true}],
            [{deviceTypeName: 'VOGEE XT1', isNewShare: false}, {deviceTypeName: 'VOGEE XT1', isNewShare: false}]
        ]
        // this.setState({
        //     powerDeviceArray: tempPowerDeviceArray,
        //     cleanDeviceArray: tempCleanDeviceArray,
        //     //shareToMeDeviceArray: tempShareToMeDeviceArray,
        // })
    }


    _getSharePushList() {
        NetUtil.getDeviceSharePushList(StorageHelper.getTempToken(), false)
            .then((res) => {
                var deviceShareInfo = res.list

                if (JSON.stringify(deviceShareInfo) != JSON.stringify(tempDeviceShareInfo)) {
                    LogUtil.debugLog('_getSharePushList========>')
                    tempDeviceShareInfo = deviceShareInfo
                    if (deviceShareInfo && deviceShareInfo.length > 0) {
                        var tempArray = []
                        deviceShareInfo.map((item, index) => {
                            var auditStatus = item.auditStatus//消息处理状态 【t】已处理、【f】未处理
                            var isDeleted = (parseInt(item.deleted) == 1)//【0】未删除、【1】已删除
                            //是否失效，超过24小时失效
                            var isInvalid = (parseInt((new Date().getTime() - item.createTime) / 1000) > daySec)

                            if (!auditStatus && !isDeleted && !isInvalid) {
                                tempArray.push(item)
                            }
                        })
                        this.setState({
                            sharePushList: tempArray
                        })
                    }
                }
            })
            .catch((error) => {

            })
    }

    //获取分享给我的设备列表
    _getShareWithMeDeviceList() {
        NetUtil.shareWithMeDeviceList(this.state.token, false)
            .then((res) => {
                this._setShareWithMeDeviceInfo(res.info)
            })
            .catch((error) => {

            })
    }

    _setShareWithMeDeviceInfo(deviceInfo) {
        if (deviceInfo && deviceInfo.length > 0) {
            var tempArray = []
            deviceInfo.map((item, index) => {
                tempArray.push(
                    {
                        deviceTypeName: item.nickName,
                        time: item.time,
                        deviceId: item.deviceId,
                        mac: item.mac,
                        isNewShare: parseInt((new Date().getTime() - item.time) / 1000) <= daySec,
                    }
                )
            })

            this.setState({
                shareToMeDeviceArray: Helper._getArray3(tempArray)
            })
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF',
                }}>
                <TitleBar
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        this.props.navigation.goBack();
                    }}
                    rightIcon={require('../../resources/bluetooth_ic.png')}
                    rightIconClick={() => {
                        //蓝牙连接
                        that._checkPermission()
                    }}
                    rightText={strings('ble_connect')}
                    rightTextClick={() => {
                        //蓝牙连接
                        that._checkPermission()
                    }}
                />
                {this._titleView()}
                {this._tabView()}
                {this._bottomContainerView()}

                {this._accpetShareDialog()}
                {this._tipsDialogView()}
            </View>
        );
    }

    _tipsDialogView() {
        return (
            <View>
                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    modalVisible={this.state.bleTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({bleTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        if (Platform.OS === 'android') {
                            Bluetooth._enableBluetooth()
                        } else {
                            //ios跳转系统设置
                            GXRNManager.openSystemSetting()
                        }

                        this.setState({bleTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    modalVisible={this.state.gpsTipsDialog}
                    title={strings('warming_tips')}
                    message={strings('open_gps_tips')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({gpsTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        NativeModules.RNModule.openGps()
                        this.setState({gpsTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    modalVisible={this.state.locationTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({locationTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        this.requestPermission()
                        this.setState({locationTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

            </View>
        )
    }

    /*
    *接收分享设备弹窗
     */
    _accpetShareDialog() {
        return (
            <AcceptDeviceShareDialog
                deviceImage={this.state.shareDeviceImg}
                modalVisible={this.state.shareDialogVis}
                userName={this.state.shareUserName}
                deviceType={this.state.shareDeviceAlias}
                deviceName={this.state.deviceName}
                onModalClose={() => {
                    this.setState({shareDialogVis: false})
                }}
                onChangeText={(text) => {
                    var deviceName = text.toString().trim()
                    this.setState({deviceName: deviceName})
                    this.state.deviceName = deviceName
                }}
                clearNameTextInput={() => {
                    this.setState({deviceName: ''})
                    this.state.deviceName = ''
                }}
                onLeftBtnClick={() => {
                    //稍后
                    this.setState({shareDialogVis: false})
                }}
                onRightBtnClick={() => {
                    //接受
                    if (this.state.deviceName) {
                        this.setState({shareDialogVis: false})
                        this._accpteShareDevice(this.state.deviceName, this.state.shareOrder)
                    } else {
                        this.onShowToast(strings('mark_name_not_none'))
                    }
                }}/>)
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('add_device')}
            />
        )
    }

    _tabView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 36,
                    marginLeft: 18,
                    height: 46,
                    backgroundColor: '#F6F6F6',
                    borderRadius: 10,
                    padding: 5,
                    flexDirection: 'row',
                    marginTop: 23
                }}>
                {this._tabItemView(strings('wifi_add'), this.state.tabIndex == 0, () => {
                    if (this.state.tabIndex != 0) {
                        this.setState({tabIndex: 0})
                    }
                })}

                {this._tabItemView(strings('share_to_me'), this.state.tabIndex == 1, () => {
                    if (this.state.tabIndex != 1) {
                        this.setState({tabIndex: 1})
                    }
                })}
            </View>
        )
    }

    _tabItemView(text, isCheck, checkEvent) {
        return (
            <TouchableOpacity
                onPress={checkEvent}
                style={{
                    flex: 1,
                    height: 36,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: isCheck ? '#38579D' : 'rgba(0,0,0,0)',
                    borderRadius: 6
                }}>

                <CommonTextView
                    textSize={12}
                    text={text}
                    style={{
                        color: isCheck ? '#fff' : '#000'
                    }}/>
            </TouchableOpacity>
        )
    }

    _bottomContainerView() {
        return (
            <View
                style={{
                    flex: 1,
                }}
                onLayout={(event) => this.onLayout(event)}>
                <Swiper
                    index={this.state.tabIndex}
                    height={this.state.bottomContainerViewHeight}
                    width={this.mScreenWidth}
                    horizontal={true}
                    autoplay={false}
                    showsPagination={false}
                    loop={false}
                    onIndexChanged={(index) => {
                        this.setState({tabIndex: index})
                    }}
                >
                    {this._wifiConnectView()}
                    {this._shareToMeView()}
                </Swiper>
            </View>
        )
    }

    onLayout = (event) => { // 获取View的高度
        const viewHeight = event.nativeEvent.layout.height;
        const viewWidth = event.nativeEvent.layout.width;
        this.setState({bottomContainerViewHeight: viewHeight});
    }


    _wifiConnectView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 36,
                    height: this.state.bottomContainerViewHeight,
                    marginLeft: 18
                }}>
                {
                    this.state.productArray.length == 0 && this.state.getFlag ?
                        <View
                            style={{
                                width: this.mScreenWidth - 36,
                                height: this.state.bottomContainerViewHeight,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                            {this._shareWithMeNoneView()}
                        </View>
                        :
                        <ScrollView
                            showsVerticalScrollIndicator={false}>
                            {
                                this.state.productArray.map((item, index) => {
                                    return (
                                        this._connectItemView(index, item.name, item.data, false)
                                    )
                                })
                            }
                            {/*{this._connectItemView(strings('power_device'), this.state.powerDeviceArray, false)}*/}
                            {/*{this._connectItemView(strings('clean_device'), this.state.cleanDeviceArray, false)}*/}
                        </ScrollView>
                }
            </View>
        )
    }

    _shareToMeView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    height: this.state.bottomContainerViewHeight,
                    alignItems: 'center'
                }}>
                {
                    this.state.sharePushList.length == 0 ?
                        <View
                            style={{
                                width: this.mScreenWidth,
                                height: this.state.bottomContainerViewHeight,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                            {this._shareWithMeNoneView()}
                        </View> :
                        <ScrollView
                            style={{
                                width: this.mScreenWidth,
                                height: this.state.bottomContainerViewHeight,
                            }}
                            showsVerticalScrollIndicator={false}>
                            {/*{this._connectItemView('', this.state.shareToMeDeviceArray, true, 17)}*/}
                            {
                                this.state.sharePushList.map((item, index) => {
                                    return (this._sharePushItemView(item, index))
                                })
                            }
                        </ScrollView>
                }


                {/*{this._shareToMeTipsView()}*/}
            </View>
        )
    }

    _shareWithMeNoneView() {
        return (
            <CommonTextView
                textSize={14}
                text={strings('share_device_with_me_none_tips')}
                style={{
                    color: '#969AA1',
                }}/>
        )
    }

    _sharePushItemView(item, index) {
        return (
            <ShadowCardView
                onPress={() => {
                    this.setState({
                        shareDeviceAlias: item.alias,
                        shareUserName: item.sharerName,
                        deviceName: item.productName,
                        shareDeviceImg: item.img,
                        shareOrder: item.para,
                        shareDialogVis: true,
                    })
                }}
                key={index}
                style={{
                    width: this.mScreenWidth - 36,
                    marginTop: 15,
                    flexDirection: 'row',
                    paddingTop: 19,
                    paddingBottom: 19,
                    paddingLeft: 16,
                    paddingRight: 15,
                    marginLeft: 18,
                    marginBottom: index == this.state.sharePushList.length - 1 ? 18 : 0,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <Image
                    style={{
                        width: this.mScreenWidth * 0.15,
                        height: this._getHeightByWH(55, 52, this.mScreenWidth * 0.15)
                    }}
                    source={require('../../resources/list_dev_img.png')}/>

                <View
                    style={{
                        marginLeft: 15
                    }}>
                    <CommonTextView
                        textSize={14}
                        text={item.productName}
                        style={{
                            color: '#000'
                        }}/>

                    <CommonTextView
                        textSize={10}
                        text={strings('sharer') + item.sharerName}
                        style={{
                            color: '#B5BAC5',
                            marginTop: 5
                        }}/>

                    {
                        I18n.locale == 'zh' ?
                            <Text
                                style={{
                                    fontSize: this.getSize(10),
                                    color: '#B5BAC5',
                                    marginTop: 5
                                }}
                            >
                                分享将在
                                <Text
                                    style={{
                                        fontSize: this.getSize(10),
                                        color: '#FF8748',
                                    }}>
                                    {this._getShareExpiresTime(item.createTime)}
                                </Text>
                                后失效
                            </Text>
                            :
                            <Text
                                style={{
                                    fontSize: this.getSize(10),
                                    color: '#B5BAC5',
                                    marginTop: 5
                                }}
                            >
                                Share will expire after
                                <Text
                                    style={{
                                        fontSize: this.getSize(10),
                                        color: '#FF8748',
                                    }}>
                                    {this._getShareExpiresTime(item.createTime)}
                                </Text>
                            </Text>
                    }

                </View>

                {ViewHelper.getFlexView()}

                <CommonTextView
                    textSize={10}
                    text={strings('to_receive')}
                    style={{
                        color: '#B5BAC5',
                    }}/>

                <Image
                    source={require('../../resources/list_arrow_ic.png')}/>
            </ShadowCardView>
        )
    }

    _connectItemView(index, title, deviceArray, showShadow, marginTop) {
        return (
            <View
                key={index}>
                <CommonTextView
                    textSize={14}
                    text={title}
                    style={{
                        color: '#000000',
                        marginTop: marginTop ? marginTop : 30
                    }}/>

                {
                    deviceArray.map((item, index) => {
                        return (
                            <View
                                key={index}
                                style={{
                                    flexDirection: 'row'
                                }}>
                                {
                                    item.map((item1, index1) => {
                                        return (
                                            <DeviceCard
                                                imageUrl={item1.imgSmall ? {uri: item1.imgSmall} : require('../../resources/add_dev_dev_img.png')}
                                                onPress={() => {
                                                    if (showShadow) {
                                                        this.setState({
                                                            deviceName: item1.deviceTypeName,
                                                            selectDeviceId: item1.deviceId,
                                                            selectMac: item1.mac,
                                                            selectUserName: 'Johnson',
                                                            shareDialogVis: true
                                                        })
                                                    } else {
                                                        //wifi设备连接
                                                        this.props.navigation.navigate('WifiDeviceConnection', {
                                                            productId: item1.id,
                                                            token: this.state.token
                                                        });
                                                    }
                                                }}
                                                style={{
                                                    marginBottom: showShadow ? 10 : 0
                                                }}
                                                key={index1}
                                                showShadow={showShadow}
                                                isNewShare={item1.isNewShare ? item1.isNewShare : false}
                                                deviceTypeName={item1.name ? item1.name : item1.deviceTypeName}/>
                                        )
                                    })
                                }
                                {
                                    item.length == 1 ?
                                        ViewHelper.getFlex2View()
                                        : item.length == 2 ? ViewHelper.getFlexView()
                                        : null
                                }
                            </View>
                        )
                    })
                }
            </View>
        )
    }

    _shareToMeTipsView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 36,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 55,
                    marginTop: 10
                }}>
                <CommonTextView
                    textSize={14}
                    text={strings('share_to_me_tips')}
                    style={{
                        color: '#BBBEC1'
                    }}/>
            </View>
        )
    }

    //根据创建时间获取失效时间文案
    _getExpiresTime(createTime) {
        var timeText = ''
        var expiresSecond = daySec - parseInt((new Date().getTime() - createTime) / 1000)
        var hour = parseInt(expiresSecond / 3600)
        var min = parseInt((expiresSecond - hour * 3600) / 60)
        if (parseInt(expiresSecond) > 3600) {
            //小时
            timeText = strings('share_dismiss_after_hour')(hour)
        } else {
            //分钟
            timeText = strings('share_dismiss_after_min')(min)
        }
        return timeText
    }

    _getShareExpiresTime(createTime) {
        var time = ''
        var expiresSecond = daySec - parseInt((new Date().getTime() - createTime) / 1000)
        var hour = parseInt(expiresSecond / 3600)
        var min = parseInt((expiresSecond - hour * 3600) / 60)
        if (parseInt(expiresSecond) > 3600) {
            //小时
            time = hour + (I18n.locale == 'zh' ? '小时' : ' hours')
        } else {
            //分钟
            time = min + (I18n.locale == 'zh' ? '分钟' : ' mins')
        }
        return I18n.locale == 'zh' ? time : ' ' + time
    }

    _accpteShareDevice(deviceName, order) {
        if (order) {
            NetUtil.acceptShareDevice(this.state.token, deviceName, order, true)
                .then((res) => {
                    this._removeLocalShare(order)
                    this.onShowToast(strings('successed'))
                    EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY)
                    this._getSharePushList()
                }).then((error) => {

            })
        } else {
            this.onShowToast('order is null')
        }
    }

    _removeLocalShare(shareOrder) {
        StorageHelper.getShareIds()
            .catch((data) => {
                if (data && data.length > 0) {
                    var tempArray = []
                    data.map((item, index) => {
                        if (item != shareOrder) {
                            tempArray.push(item)
                        }
                    })
                    StorageHelper.saveShareIds(tempArray)
                }
            })
    }
}