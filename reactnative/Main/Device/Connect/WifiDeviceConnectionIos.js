import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {Image, View} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import ViewHelper from "../../View/ViewHelper";
import CommonTextView from "../../View/CommonTextView";
import PagerTitleView from "../../View/PagerTitleView";
import CommonBtnView from "../../View/CommonBtnView";
import ListItemView from "../../View/ListItemView";
import ShadowCardView from "../../View/ShadowCardView";

/*
*wifi设备配网
 */

export default class WifiDeviceConnectionIos extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff'
                }}>
                {this._titleView()}
                <View
                    style={{
                        width: this.mScreenWidth,
                        alignItems: 'center',
                        justifyContent: 'center',
                        flex: 1
                    }}>
                    {this._guideImageView()}
                    {this._goSettingView()}
                    {ViewHelper.getFixFlexView(0.7)}
                    {this._curConnectWifiView()}
                    {ViewHelper.getFlexView()}
                    {this._bottomBtnView()}
                </View>
            </View>
        );
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('device_connecttion')}
                style={{
                    marginLeft: 23
                }}
            />
        )
    }

    _guideImageView() {
        return (
            <Image
                style={{
                    width: this.mScreenWidth - 36,
                    resizeMode: 'contain',
                    marginTop: 53
                }}
                source={require('../../../resources/wifi_connect_img.png')}/>
        )
    }

    _goSettingView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 60,
                    flexDirection: 'row',
                    marginTop: 19,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#969AA1',
                    }}
                    text={strings('wifi_point_pd')}/>
                {ViewHelper.getFlexView()}
                <CommonTextView
                    onPress={() => {
                        //去设置
                    }}
                    textSize={14}
                    style={{
                        color: '#38579D',
                        marginRight: 4
                    }}
                    text={strings('go_setting')}/>

                <Image
                    source={require('../../../resources/arrow_blue_ic.png')}/>
            </View>
        )
    }

    _curConnectWifiView() {
        return (
            <View>
                <CommonTextView
                    onPress={() => {
                        //去设置
                    }}
                    textSize={16}
                    style={{
                        color: '#000000',
                    }}
                    text={strings('cur_connect_wifi')}/>
                <ShadowCardView
                    style={{
                        height: 56,
                        marginTop: 20
                    }}>
                    <ListItemView
                        titleStyle={{
                            color: '#969AA1'
                        }}
                        hideArrowIcon={true}
                        title={'abc123_2.4G'}
                        value={''}
                        onPress={() => {
                            //选择WiFi
                        }}/>
                </ShadowCardView>
            </View>
        )
    }

    _bottomBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.argeeCheck}
                onPress={() => {
                    //下一步
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginBottom: 44,
                }}
                btnText={strings('next')}/>
        )
    }
}