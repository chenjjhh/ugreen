import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    Animated,
    AppState,
    DeviceEventEmitter,
    Easing,
    Image,
    NativeModules,
    Platform,
    View
} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import CommonTextView from "../../View/CommonTextView";
import PagerTitleView from "../../View/PagerTitleView";
import NetUtil from "../../Net/NetUtil";
import StorageHelper from "../../Utils/StorageHelper";
import EventUtil from "../../Event/EventUtil";
import NetConstants from "../../Net/NetConstants";
import ViewHelper from "../../View/ViewHelper";
import ShadowCardView from "../../View/ShadowCardView";
import ListItemView from "../../View/ListItemView";
import TextInputView from "../../View/TextInputView";
import CommonBtnView from "../../View/CommonBtnView";
import Helper from "../../Utils/Helper";
import {NetworkInfo} from "react-native-network-info";
import MessageDialog from "../../View/MessageDialog";
import LogUtil from "../../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager
/*
*设备连接界面
*蓝牙设备连接、wifi设备连接
 */

const animatedDuration = 3000
const animatedDurationLoading = 1000

var deviceMac = ''
var bindWay = ''
var reBindTimes = 0//重新绑定的次数  5s一次，一共120秒  可以重新调用24次

const CONNECT_BLE_NAME = 'Gendome_'
const CONNECT_AP_NAME = 'Gendome_'
// const CONNECT_BLE_NAME = 'Granwin_AP_'
// const CONNECT_AP_NAME = 'Granwin_AP_'
export default class DeviceConnecting extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            imgRotate: new Animated.Value(0),
            animRuning: false,
            imgRotateLoading: new Animated.Value(0),
            animRuningLoading: false,
            deviceType: props.navigation.state.params.deviceType,//0蓝牙设备  1wifi设备
            wifiSSid: props.navigation.state.params.wifiSSid,
            wifiPwd: props.navigation.state.params.wifiPwd,
            wifiStep: 0,//wifi设备连接步骤 0连接设备热点 1设备配网
            longitude: props.navigation.state.params.longitude,
            latitude: props.navigation.state.params.latitude,
            isApConnect: false,//当前是否ap配网
            curWifiSSid:props.navigation.state.params.wifiSSid,

            gpsTipsDialog: false,
        }
        this.isGoing = false; // 为真旋转
        this.myAnimate = Animated.timing(this.state.imgRotate, {
            toValue: 1,
            duration: animatedDuration,
            easing: Easing.linear,
            useNativeDriver: true// 使用原生驱动动画
        });
        this.isGoingLoading = false; // 为真旋转
        this.myAnimateLoading = Animated.timing(this.state.imgRotateLoading, {
            toValue: 1,
            duration: animatedDurationLoading,
            easing: Easing.linear,
            useNativeDriver: true// 使用原生驱动动画
        });
    }

    componentWillMount() {
        deviceMac = ''
        bindWay = ''

        //this.props.navigation.replace('WifiDeviceConnectionIos');
        //this._startAnima(true);
        this._startAnimaLoading(true);

        /**
         * Android的配网成功的回调
         *
         * params.putString("way", "ble");
         * params.putString("status", "success");
         * params.putString("mac", setDeviceNetworkResultEntity.getMAC());
         * params.putString("pk", setDeviceNetworkResultEntity.getPK());
         */
        this.deviceNetWorkListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_NETWORK_KEY, (value) => {
            LogUtil.debugLog(value);
            this._setDeviceNetworkCallback(value)
        });


        this.appStateListener = AppState.addEventListener('change', (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === 'active') { // 后台进入前台
                this._getWifiSiidName()
            }
        });// 监听APP状态  前台、后台

        //开始连接设备
        this.startConfig();
    }

    //绑定设备
    _bindDevice(mac, way) {
        deviceMac = mac
        bindWay = way

        NetUtil.bindDevice(StorageHelper.getTempToken(), mac,  reBindTimes < 22)
            .then((res) => {
                //绑定设备成功
                reBindTimes = 0
                this._wifiToConnectSuccessResult()
                EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY)
            }).catch((error) => {
            //如果首次返回失败，可能是设备和云端未连接，需要多次调用绑定设备接口

            if (reBindTimes < 22) {
                //重新绑定
                reBindTimes = parseInt(reBindTimes + 1)
                this._startReBind()
            } else {
                this._wifiToConnectFailResult()
            }
        })
    }

    _startReBind() {
        this._stopReBind()
        this.intervalRebind = setInterval(() => {
            if (deviceMac != '') {
                this._bindDevice(deviceMac, bindWay)
            } else {
                //mac是空，则直接跳转失败页面
                this._wifiToConnectFailResult()
            }
            this._stopReBind()
        }, 5000)
    }

    _stopReBind() {
        if (this.intervalRebind) {
            clearInterval(this.intervalRebind)
        }
    }

    //设备配网回调
    _setDeviceNetworkCallback(msg) {
        if (msg) {
            var callbackStatus = msg.status
            var way = msg.way
            var mac = msg.mac
            var pk = msg.pk
            if (callbackStatus == 'success') {
                //配网成功，跳转成功界面
                this.setState({wifiStep: 2})
                this._bindDevice(mac, way)
            } else {
                //配网失败，跳转失败界面
                this._wifiToConnectFailResult()
            }
        }
    }

    _bleToConnectSuccessResult() {
        this.props.navigation.replace('DeviceConnectedResult', {
            isBle: true,
            connectedSuccessed: true,
            mac: deviceMac
        });
    }


    _bleToConnectFailResult() {
        this.props.navigation.replace('DeviceConnectedResult', {
            isBle: true,
            connectedSuccessed: false,
            mac: deviceMac

        });
    }


    _wifiToConnectSuccessResult() {
        this.setState({wifiStep: 3})
        setTimeout(() => {
            this.props.navigation.replace('DeviceConnectedResult', {
                isBle: false,
                connectedSuccessed: true,
                mac: deviceMac
            });
        }, 1000);
    }

    _wifiToConnectFailResult() {
        this.setState({wifiStep: 0})
        this.props.navigation.replace('DeviceConnectedResult', {
            curSiid: this.state.wifiSSid,
            isBle: false,
            connectedSuccessed: false,
            mac: deviceMac
        });
    }

    startConfig() {
        this.setState({wifiStep: 0})
        if (Platform.OS === 'android') {
            //android   '/device/certificate/get'
            NativeModules.RNModule.connectBLEDevice(CONNECT_BLE_NAME, (result) => {
                if (result == 'success') {
                    NativeModules.RNModule.setDeviceNetwork(true, this.state.curWifiSSid, this.state.wifiPwd, NetConstants.HTTP_URL)
                } else {
                    LogUtil.debugLog('===============》蓝牙配网失败')
                    this.setState({isApConnect: true})
                    //this._wifiToConnectFailResult()
                }
            });
        } else {
            /// 启动SDK
            GXRNManager.start();

            /// 蓝牙配网
            GXRNManager.bleSetDeviceNetwork(this.state.curWifiSSid,
                this.state.wifiPwd,
                CONNECT_BLE_NAME,
                NetConstants.HTTP_URL).then((datas) => {

                //蓝牙配网成功 开始绑定
                LogUtil.debugLog(datas);
                this.setState({wifiStep: 2})
                this._bindDevice(datas.MAC, 'ble')
            }).catch((err) => {
                LogUtil.debugLog(err);
                this.setState({isApConnect: true})
               // this._wifiToConnectFailResult()
            });
        }
    }

    componentWillUnmount() {
        this.appStateListener && this.appStateListener.remove()
        this._startAnima(false);
        this._startAnimaLoading(false);
        if (Platform.OS === 'android') {
            //停止连接设备
            NativeModules.RNModule.stopConnectDevice();
            //停止配网
            NativeModules.RNModule.stopSetDeviceNetwork();
            this.deviceNetWorkListener && this.deviceNetWorkListener.remove()
        } else {
            /// 停止配置WiFi
            GXRNManager.stopSetDeviceNetwork();
        }
        reBindTimes = 0
        this._stopReBind()
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF'
                }}>
                {this._titleView()}
                {
                    this.state.isApConnect ?
                        this._apConnectView()
                        :
                        <View
                            style={{
                                flex: 1
                            }}>
                            {this._centerImageView()}
                            {this._centerTipsView()}
                            {this._bottomStatusView()}
                        </View>
                }

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    modalVisible={this.state.gpsTipsDialog}
                    title={strings('warming_tips')}
                    message={strings('open_gps_tips')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({gpsTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        NativeModules.RNModule.openGps()
                        this.setState({gpsTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>
            </View>
        );
    }

    _apConnectView() {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    width: this.mScreenWidth,
                    paddingTop: 50
                }}>
                {this._apConnectTopImageView()}
                {this._apConnectToSettingView()}
                {this._apConnectCurSiidView()}
                {this._apConnectBtnView()}
            </View>
        )
    }

    _apConnectTopImageView() {
        return (
            <Image
                source={require('../../../resources/wifi_connect_img.png')}/>
        )
    }

    _apConnectToSettingView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 15,
                    width: this.mScreenWidth - 80,
                }}>
                <CommonTextView
                    textSize={14}
                    text={strings('waln_pd')}
                    style={{
                        color: '#969AA1',
                    }}
                />
                {ViewHelper.getFlexView()}
                <CommonTextView
                    onPress={() => {
                        if (Platform.OS === 'android') {
                            //安卓跳转wifi设置
                            NativeModules.RNModule.toWifiSetting()
                        } else {
                            //ios跳转系统设置
                            GXRNManager.openSystemSetting()
                        }
                    }}
                    textSize={14}
                    text={strings('to_setting')}
                    style={{
                        color: '#38579D',
                    }}
                />
                <Image
                    source={require('../../../resources/arrow_blue_ic.png')}/>
            </View>
        )
    }

    _apConnectCurSiidView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 80,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CommonTextView
                    textSize={16}
                    style={{
                        color: '#000000',
                        marginTop: 43,
                        width: this.mScreenWidth - 80,
                    }}
                    text={strings('cur_connect_wifi')}/>

                <ShadowCardView
                    style={{
                        height: 56,
                        marginTop: 20,
                        width: this.mScreenWidth - 80,
                    }}>
                    <ListItemView
                        titleStyle={{
                            color: '#969AA1'
                        }}
                        hideArrowIcon={true}
                        title={this.state.wifiSSid}
                        value={''}/>
                </ShadowCardView>
            </View>
        )
    }

    _apConnectBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.wifiSSid.indexOf(CONNECT_AP_NAME) != -1}
                onPress={() => {
                    //下一步 ap配网连接
                    this.setState({
                        isApConnect: false
                    })
                    this._apConnect()
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginBottom: 44,
                    marginTop: 80
                }}
                btnText={strings('next')}/>
        )
    }

    //ap配网
    _apConnect() {
        this.setState({wifiStep: 0})
        if (Platform.OS === 'android') {
            NativeModules.RNModule.connectDeviceHot(CONNECT_AP_NAME, (result) => {
                if (result == 'success') {
                    //连接设备热点成功
                    this.setState({wifiStep: 1})
                    NativeModules.RNModule.setDeviceNetwork(false, this.state.curWifiSSid, this.state.wifiPwd, NetConstants.HTTP_URL)
                } else {
                    //连接设备热点失败,跳转失败界面
                    this._wifiToConnectFailResult()
                }
            });
        } else {

            /// 连接到热点
            GXRNManager.connectDeviceHot(CONNECT_AP_NAME, '12345678',
                strings('提示'), strings('请手动连接到无线网络'), strings('确定')).then((datas) => {
                LogUtil.debugLog(datas);
                this.setState({wifiStep: 1})
                /// WiFi配网
                GXRNManager.wifiSetDeviceNetwork(this.state.curWifiSSid,
                    this.state.wifiPwd,
                    NetConstants.HTTP_URL, 30).then((datas) => {
                    this.setState({wifiStep: 2})
                    //wifi配网成功 开始绑定
                    LogUtil.debugLog(datas);
                    this._bindDevice(datas.MAC, 'wifi')
                }).catch((err) => {
                    LogUtil.debugLog(err);
                    this._wifiToConnectFailResult()
                });
            }).catch((err) => {
                LogUtil.debugLog(err);

                //连接设备热点失败,跳转失败界面
                this._wifiToConnectFailResult()
            });
        }
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('device_connect')}
                style={{
                    marginLeft: 23
                }}
            />
        )
    }

    _centerImageView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 34
                }}>
                <Animated.Image
                    style={{
                        transform: [
                            // 使用interpolate插值函数,实现了从数值单位的映//射转换,上面角度从0到1，这里把它变成0-360的变化
                            {
                                rotate: this.state.imgRotate.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: ['0deg', '360deg']
                                })
                            }
                        ]
                    }}
                    source={require('../../../resources/loading_ic.png')}/>
                <Image
                    style={{
                        position: 'absolute'
                    }}
                    source={this.state.deviceType == 0 ? require('../../../resources/loading_bluetooth_connect_ic.png') :
                        this.state.wifiStep == 0 ? require('../../../resources/loading_router_ic.png') : require('../../../resources/loading_wifi_connect_ic.png')}/>
            </View>
        )
    }

    _centerTipsView() {
        return (
            <CommonTextView
                textSize={14}
                text={this._getCentTipsText()}
                style={{
                    color: '#969AA1',
                    width: this.mScreenWidth - 80,
                    textAlign: 'center',
                    marginLeft: 40
                }}
            />
        )
    }

    _getCentTipsText() {
        var tipsText = ''
        if (this.state.deviceType == 0) {
            tipsText = strings('ble_on_close_to_device')
        } else {
            if (this.state.wifiStep == 0) {
                tipsText = strings('ble_on_close_to_device')
            } else {
                tipsText = strings('on_close_to_router')
            }
        }

        return tipsText
    }

    _bottomStatusView() {
        return (
            <View
                style={{
                    flex: 1,
                    marginLeft: 50,
                    marginRight: 50,
                    //alignItems: 'center',
                    justifyContent: 'center'
                }}>
                {this.state.wifiStep == 0 ? this._bottomStatusItemView(true, strings('phone_connecting_device')) : this._bottomStatusItemView(false, strings('link_device_success'))}
                {this.state.wifiStep == 1 ? this._bottomStatusItemView(true, strings('trans_info_to_device')) : null}
                {this.state.wifiStep < 2 ? null : this._bottomStatusItemView(false, strings('trans_info_to_device_success'))}
                {this.state.wifiStep == 2 ? this._bottomStatusItemView(true, strings('device_connecting_network')) : null}
                {this.state.wifiStep == 3 ? this._bottomStatusItemView(false, strings('device_connecting_network')) : null}
            </View>
        )
    }

    _bottomStatusItemView(isLoading, text) {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginBottom: 11
                }}>

                {
                    isLoading ?
                        <Animated.Image
                            style={{
                                transform: [
                                    // 使用interpolate插值函数,实现了从数值单位的映//射转换,上面角度从0到1，这里把它变成0-360的变化
                                    {
                                        rotate: this.state.imgRotateLoading.interpolate({
                                            inputRange: [0, 1],
                                            outputRange: ['0deg', '360deg']
                                        })
                                    }
                                ]
                            }}
                            source={require('../../../resources/loading_s_ic.png')}/>
                        : <Image
                            source={require('../../../resources/connect_done_ic.png')}/>
                }


                <CommonTextView
                    textSize={16}
                    text={text}
                    style={{
                        color: '#282A30',
                        marginLeft: 6,
                        marginTop: 2
                    }}
                />
            </View>
        )
    }


    // 运行动画
    _initAnima() {
        if (this.isGoing) {
            this.state.imgRotate.setValue(0);
            this.myAnimate.start(() => {
                this._initAnima();
            });
        }
    }

    _startAnima(flag) {
        this.isGoing = flag;

        if (this.isGoing) {
            if (!this.state.animRuning)
                this.myAnimate.start(() => {
                    this.myAnimate = Animated.timing(this.state.imgRotate, {
                        toValue: 1,
                        duration: animatedDuration,
                        easing: Easing.linear,
                        useNativeDriver: true// 使用原生驱动动画
                    });
                    this._initAnima();
                });
            this.setState({animRuning: true});
        } else {
            this.state.imgRotate.stopAnimation((oneTimeRotate) => {
                // 计算角度比例
                this.myAnimate = Animated.timing(this.state.imgRotate, {
                    toValue: 1,
                    duration: (1 - oneTimeRotate) * animatedDuration,
                    easing: Easing.linear,
                    useNativeDriver: true// 使用原生驱动动画
                });
            });
            this.setState({animRuning: false});
        }
    }

    // 运行动画
    _initAnimaLoading() {
        if (this.isGoingLoading) {
            this.state.imgRotateLoading.setValue(0);
            this.myAnimateLoading.start(() => {
                this._initAnimaLoading();
            });
        }
    }

    _startAnimaLoading(flag) {
        this.isGoingLoading = flag;

        if (this.isGoingLoading) {
            if (!this.state.animRuningLoading)
                this.myAnimateLoading.start(() => {
                    this.myAnimateLoading = Animated.timing(this.state.imgRotateLoading, {
                        toValue: 1,
                        duration: animatedDurationLoading,
                        easing: Easing.linear,
                        useNativeDriver: true// 使用原生驱动动画
                    });
                    this._initAnimaLoading();
                });
            this.setState({animRuningLoading: true});
        } else {
            this.state.imgRotateLoading.stopAnimation((oneTimeRotate) => {
                // 计算角度比例
                this.myAnimateLoading = Animated.timing(this.state.imgRotateLoading, {
                    toValue: 1,
                    duration: (1 - oneTimeRotate) * animatedDurationLoading,
                    easing: Easing.linear,
                    useNativeDriver: true// 使用原生驱动动画
                });
            });
            this.setState({animRuningLoading: false});
        }
    }


    _getWifiSiidName() {
        if (Platform.OS === 'android') {
            this._androidOpenGps()
        } else {
            this._iosGetNetWorkSiid()
        }
    }

    _androidOpenGps() {
        NativeModules.RNModule.isLocServiceEnable((result) => {
            if (!result) {
                //安卓未开启定位
                this.setState({gpsTipsDialog: true})
            } else {
                this._getNetWorkSiid()
            }
        });
    }

    _getNetWorkSiid() {
        NetworkInfo.getSSID().then(ssid => {
            this.setState({
                wifiSSid: ssid,
            })
        });
    }

    _iosGetNetWorkSiid() {
        GXRNManager.getWifiSSid().then((datas) => {
            this.setState({wifiSSid: datas})
        }).catch((err) => {
            LogUtil.debugLog('err', err);
        });
    }
}
