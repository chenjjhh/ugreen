import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {View, Image, NativeModules} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import ViewHelper from "../../View/ViewHelper";
import CommonTextView from "../../View/CommonTextView";
import PagerTitleView from "../../View/PagerTitleView";
import CommonBtnView from "../../View/CommonBtnView";
import CommonCheckView from "../../View/CommonCheckView";
import Geolocation from '@react-native-community/geolocation';
import NetUtil from "../../Net/NetUtil";
import Helper from "../../Utils/Helper";
import FastImage from "react-native-fast-image";

/*
*wifi设备配网
 */

export default class WifiDeviceConnection extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            argeeCheck: false,
            token: props.navigation.state.params.token,
            productId: props.navigation.state.params.productId,
            tipsInfo: '',
            tipsImgUrl: '',
            imageFlag: false
        }
    }

    componentWillMount() {
        NetUtil.getProductConnectNetTips(this.state.token, this.state.productId, true)
            .then((res) => {
                if (res && res.info) {
                    var langType = Helper.getLanguageType()
                    var resInfo = res.info
                    var tipsValue = (langType == 3 ? resInfo.remarkEn : resInfo.remarkCn)
                    this.setState({
                        tipsInfo: this._getTipsInfoValue(tipsValue),
                        tipsImgUrl: resInfo.imgNormal ? resInfo.imgNormal : (
                            resInfo.imgBig ? resInfo.imgBig : ''
                        ),
                    })
                } else {
                    this.setState({
                        tipsInfo: strings('device_connecttion_tips')
                    })
                }
                this.setState({imageFlag: true})
            })
            .catch((error) => {
                this.setState({
                    tipsInfo: strings('device_connecttion_tips'),
                    imageFlag: true
                })
            })
    }

    _getTipsInfoValue(value) {
        return value ? value : strings('device_connecttion_tips')
    }


    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff'
                }}>
                {this._titleView()}
                {this._tipsView()}
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    {
                        this.state.imageFlag ?
                            this.state.tipsImgUrl ?
                                <FastImage
                                    style={{width: this.mScreenWidth * (2 / 3), height: this.mScreenWidth * (2 / 3)}}
                                    source={{
                                        uri: this.state.tipsImgUrl,
                                        priority: FastImage.priority.normal,
                                    }}
                                    resizeMode={FastImage.resizeMode.contain}
                                />
                                :
                                <Image
                                    style={{
                                        width: this.mScreenWidth * (2 / 3),
                                        height: this.mScreenWidth * (2 / 3),
                                        resizeMode: 'contain'
                                    }}
                                    source={require('../../../resources/device_connection_ic.png')}/> : null
                    }

                </View>
                {this._agreeView()}
                {this._bottomBtnView()}
            </View>
        );
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('device_connecttion')}
                style={{
                    marginLeft: 23
                }}
            />
        )
    }

    _tipsView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 62,
                    marginLeft: 31,
                    marginTop: 31,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#969AA1',
                        lineHeight: 20,
                        textAlign: 'center'
                    }}
                    text={this.state.tipsInfo}/>
            </View>
        )
    }

    _agreeView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 120,
                    marginLeft: 60,
                    flexDirection: 'row'
                }}>
                <CommonCheckView
                    onCheck={(isCheck) => {
                        this.setState({argeeCheck: isCheck})
                    }}
                    isCheck={this.state.argeeCheck}/>

                <CommonTextView
                    textSize={11}
                    style={{
                        color: '#000000',
                        lineHeight: 16,
                        marginLeft: 10
                    }}
                    text={strings('device_connecttion_agree')}/>
            </View>
        )
    }

    _bottomBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.argeeCheck}
                onPress={() => {
                    //下一步
                    this.props.navigation.navigate('WifiDeviceConnectionAndroid');
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginBottom: 44,
                    marginLeft: 50,
                    marginTop: 80
                }}
                btnText={strings('next')}/>
        )
    }
}