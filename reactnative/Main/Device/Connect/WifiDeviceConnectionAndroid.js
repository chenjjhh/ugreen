import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {
    AppState,
    DeviceEventEmitter,
    Image,
    NativeModules,
    PanResponder,
    PermissionsAndroid,
    Platform,
    ScrollView,
    TouchableOpacity,
    View
} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import ViewHelper from "../../View/ViewHelper";
import CommonTextView from "../../View/CommonTextView";
import MessageDialog from "../../View/MessageDialog";
import PagerTitleView from "../../View/PagerTitleView";
import CommonBtnView from "../../View/CommonBtnView";
import ShadowCardView from "../../View/ShadowCardView";
import ListItemView from "../../View/ListItemView";
import TextInputView from "../../View/TextInputView";
import {NetworkInfo} from "react-native-network-info";
import Geolocation from '@react-native-community/geolocation';
import Helper from "../../Utils/Helper";
import EventUtil from "../../Event/EventUtil";
import StorageHelper from "../../Utils/StorageHelper";
import Bluetooth from "../../Bluetooth/Bluetooth";
import LogUtil from "../../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager
/*
*wifi设备配网
 */

var wifiInfoMap = new Map()
export default class WifiDeviceConnectionAndroid extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('选择网络')}
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            curWifiSiid: '',
            wifiSiid: '',
            wifiPd: '',
            longitude: 0,
            latitude: 0,
            showWifiList: false,
            wifiArray: [],
            wifiInfoArray: [],
            bleOnOff: false,
            macId: props.navigation.state.params.macId,
            showMac: props.navigation.state.params.showMac,

            bleTipsDialog: false,
            gpsTipsDialog: false,
            locationTipsDialog: false,
            wifiTipsDialog: false,
        }
    }

    componentWillMount() {
        this.appStateListener = AppState.addEventListener('change', (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === 'active') { // 后台进入前台
                // if (this.state.wifiSiid.indexOf('unknown') != -1 || this.state.wifiSiid == '' || this.state.wifiSiid == undefined) {
                //
                // }
                this._getWifiSiidName()
            }
        });// 监听APP状态  前台、后台
        this._getWifiSiidName()
        this.wifiListListener = DeviceEventEmitter.addListener(EventUtil.WIFI_LIST, (res) => {
            //wifi列表回调
            if (res && res.result) {
                var data = JSON.parse(res.data)
                var tempArray = []
                data.map((item, index) => {
                    //&& !Helper.is5GHzWifi(item.frequency)
                    if (this.state.wifiSiid != item.SSID) {
                        tempArray.push(item)
                    }
                })
                this.setState({
                    wifiArray: this._filterWifiList(tempArray)
                })
            }
        });

        this._panResponder()
        this._getLocalWifiInfo()

        BluetoothManager.start(); //蓝牙初始化、检查蓝牙状态
        //蓝牙状态监听
        this.updateStateListener = BluetoothManager.addListener(
            "BleManagerDidUpdateState",
            this.handleUpdateState
        );
    }

    _getLocalWifiInfo() {
        StorageHelper.getWifiInfo()
            .then((dataArray) => {
                if (!Helper.dataIsNull(dataArray) && dataArray.length > 0) {
                    var newMap = new Map()
                    dataArray.map((item, index) => {
                        newMap.set(item.key, {
                            key: item.key,
                            value: item.value,
                            time: item.time
                        })
                    })
                    wifiInfoMap = newMap

                    this.setState({
                        wifiInfoArray: dataArray.sort(Helper.compareDown('time'))
                    })
                }
            })
    }

    _panResponder() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => {
                return true;
            },
            onStartShouldSetPanResponderCapture: (evt, gestureState) => { // 表示，是否成为事件的劫持者，如果返回是，则不会把事件传递给它的子元素
                return false;
            },
            onPanResponderGrant: (evt, gestureState) => {
                this._hiddenWifiDropView()
            },
            onPanResponderMove: (evt, gestureState) => {

            },
            onPanResponderRelease: (evt, gestureState) => { // 释放

            },
            onPanResponderTerminate: (evt, gestureState) => {
            }
        });
    }

    _filterWifiList(wifiArray) {
        var tempArray = []
        var addFlag = false
        wifiArray.map((item, index) => {
            if (item.SSID) {
                if (this.state.curWifiSiid != item.SSID) {
                    tempArray.push(item)
                } else {
                    if (!addFlag) {
                        tempArray.push(item)
                        addFlag = true
                    }
                }
            }
        })

        return tempArray;
    }

    componentWillUnmount() {
        this.wifiListListener && this.wifiListListener.remove()
        this.appStateListener && this.appStateListener.remove()
        this.updateStateListener && this.updateStateListener.remove();
    }

    _getWifiSiidName() {
        if (Platform.OS === 'android') {
            this.checkPermission()
        } else {
            this._iosGetNetWorkSiid()
        }
    }

    async checkPermission() {
        const result = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        const result1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
        if (result && result1) {
            this._androidOpenGps()
        } else {
            this.setState({locationTipsDialog: true})
        }
    }

    _androidOpenGps() {
        NativeModules.RNModule.isLocServiceEnable((result) => {
            if (!result) {
                //安卓未开启定位
                this.setState({gpsTipsDialog: true})
            } else {
                this._getNetWorkSiid()
                this._getLocation()
                this._androidScanWifi()
            }
        });
    }

    _androidScanWifi() {
        NativeModules.RNModule.startScanWifi((res) => {
            if (res) {
                //扫描成功
            }
        })
    }

    _getNetWorkSiid() {
        NetworkInfo.getSSID().then(ssid => {
            this.setState({
                wifiSiid: ssid,
                curWifiSiid: ssid
            })
        });
    }

    _iosGetNetWorkSiid() {
        GXRNManager.getWifiSSid().then((datas) => {
            LogUtil.debugLog('data', datas);
            this.setState({wifiSiid: datas})
            this._getLocation()
        }).catch((err) => {
            LogUtil.debugLog('err', err);
        });
    }

    // 获取位置信息
    _getLocation() {
        Geolocation.getCurrentPosition(
            (location) => {
                LogUtil.debugLog('获取位置信息=======>' + JSON.stringify(location))
                var longitude = location.coords.longitude
                var latitude = location.coords.latitude
                this.setState({
                    longitude: longitude,
                    latitude: latitude,
                })
            },
            (error) => {
                LogUtil.debugLog('获取位置信息error=======>' + JSON.stringify(error))
            }
        );
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff'
                }}
                {...this._panResponder.panHandlers}>
                {this._titleView()}
                {this._tipsView()}
                {this._centerView()}
                {/*{this._wifiSelectView()}*/}
                {this._bottomBtnView()}

                {ViewHelper.getFlexView()}

                {this._cantNotFindView()}

                {this._tipsDialogView()}
            </View>
        );
    }

    _cantNotFindView() {
        return <View
            style={{
                width: this.mScreenWidth,
                alignItems: 'center'
            }}>
            <CommonTextView
                onPress={() => {

                }}
                textSize={12}
                text={strings('暂不联网直接进入')}
                style={{
                    color: '#18B34F',
                    marginBottom: 30
                }}
            />
        </View>
    }

    _tipsDialogView() {
        return (
            <View>
                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({wifiTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({wifiTipsDialog: false})
                    }}
                    modalVisible={this.state.wifiTipsDialog}
                    title={strings('WiFi名称异常')}
                    message={strings('WiFi名称异常提示')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({wifiTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        this._checkPermission()
                        this.setState({wifiTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({wifiPwdTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({wifiPwdTipsDialog: false})
                    }}
                    modalVisible={this.state.wifiPwdTipsDialog}
                    title={strings('WiFi名称异常')}
                    message={strings('WiFi密码异常提示')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({wifiPwdTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        this._checkPermission()
                        this.setState({wifiPwdTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    modalVisible={this.state.bleTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({bleTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        if (Platform.OS === 'android') {
                            Bluetooth._enableBluetooth()
                        } else {
                            //ios跳转系统设置
                            GXRNManager.openSystemSetting()
                        }
                        this.setState({bleTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    modalVisible={this.state.gpsTipsDialog}
                    title={strings('warming_tips')}
                    message={strings('open_gps_tips')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({gpsTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        NativeModules.RNModule.openGps()
                        this.setState({gpsTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    modalVisible={this.state.locationTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({locationTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        this._androidOpenGps()
                        this.setState({locationTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

            </View>
        )
    }

    _wifiSelectView() {
        return (
            <View
                style={{
                    marginTop: 15
                }}>
                <ScrollView
                    onScroll={(event) => {
                        this._hiddenWifiDropView()
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            this._hiddenWifiDropView()
                        }}
                        activeOpacity={1}>
                        {
                            this.state.wifiInfoArray.map((item, index) => {
                                return (this._wifiSelectItemView(item, index))
                            })
                        }
                    </TouchableOpacity>
                </ScrollView>
            </View>
        )
    }

    _hiddenWifiDropView() {
        if (this.state.showWifiList) {
            this.setState({showWifiList: false})
        }
    }

    _wifiSelectItemView(item, index) {
        return (
            <View
                key={index}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 36,
                    backgroundColor: '#F8F8F8',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: 24,
                    paddingRight: 11,
                    flexDirection: 'row',
                    marginLeft: 18,
                    borderRadius: 10,
                    marginBottom: 15
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000000',
                    }}
                    text={item.key}
                />
                {ViewHelper.getFlexView()}

                <TouchableOpacity
                    onPress={() => {
                        //删除
                        this._deleteWifiInfo(item.key)
                    }}>
                    <Image
                        source={require('../../../resources/delete_ic.png')}/>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => {
                        //填充
                        this.setState({
                            wifiSiid: item.key,
                            wifiPd: item.value
                        })
                    }}>
                    <Image
                        style={{
                            marginLeft: 24,
                        }}
                        source={require('../../../resources/select_ic.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('选择wifi_tips')}
                style={{
                    fontWeight: 'bold',
                    marginTop: 20,
                    marginLeft: 23
                }}
            />
        )
    }

    _tipsView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 46,
                    marginLeft: 23,
                    marginTop: 10,
                }}>
                <CommonTextView
                    textSize={16}
                    style={{
                        color: '#999BA2',
                    }}
                    text={strings('确保wifi_tips')}/>

            </View>
        )
    }

    _centerView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <Image
                    style={{
                        marginTop: 20,

                        marginLeft: 24,
                    }}
                    source={require('../../../resources/greenUnion/connect_wifi_tips_ic.png')}/>

                <ListItemView
                    style={{
                        marginTop: 20,
                        height: 56,
                        borderRadius: 27,
                        width: this.mScreenWidth - 36,
                        backgroundColor: '#F7F8FB'
                    }}
                    title={this.state.wifiSiid ? this.state.wifiSiid : strings('select_wifi')}
                    value={''}
                    onPress={() => {
                        //选择WiFi
                        if (Platform.OS === 'android') {
                            if (this.state.wifiSiid != '') {
                                this.setState({showWifiList: !this.state.showWifiList})
                            } else {
                                this._getWifiSiidName()
                            }
                        } else {
                            //ios跳转系统设置
                            GXRNManager.openSystemSetting()
                        }
                    }}/>

                <TextInputView
                    style={{
                        marginTop: 20,
                        height: 56,
                        width: this.mScreenWidth - 36,
                        backgroundColor: '#F7F8FB'
                    }}
                    isPassword={true}
                    onChangeText={(text) => {
                        const newText = text.replace(' ', '');
                        this.setState({wifiPd: newText})
                    }}
                    value={this.state.wifiPd}
                    placeholder={strings('input_wifi_pd')}
                    keyboardType={'default'}
                    placeholderTextColor={'rgba(0, 0, 0, 0.26)'}/>

                {this.state.showWifiList && Platform.OS === 'android' ? this._wifiListDropView() : null}

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#999BA2',
                        marginTop: 13,
                        lineHeight: 20
                    }}
                    text={strings('常见配网失败原因')}/>
            </View>
        )
    }

    _wifiListDropView() {
        return (
            <View
                style={{
                    position: 'absolute',
                    top: 130,
                    width: this.mScreenWidth - 36,
                    zIndex: 9999
                }}>
                <ShadowCardView
                    style={{
                        width: this.mScreenWidth - 56,
                        paddingRight: 5
                    }}>
                    <ScrollView
                        style={{
                            height: this.mScreenWidth / 2
                        }}>
                        {
                            this.state.wifiArray.length == 0 ?
                                <View
                                    style={{
                                        width: this.mScreenWidth - 56,
                                        height: this.mScreenWidth / 2,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                    <CommonTextView
                                        textSize={14}
                                        text={strings('wifi_list_none_tips')}
                                        style={{
                                            color: '#969AA1',
                                        }}/>
                                </View> :
                                this.state.wifiArray.map((item, index) => {
                                    return (this._wifiItemView(item, index))
                                })
                        }
                    </ScrollView>

                </ShadowCardView>

            </View>
        )
    }

    _wifiItemView(item, index) {
        return (
            <View
                key={index}>
                <ListItemView
                    onPress={() => {
                        this.setState({
                            wifiSiid: item.SSID,
                            wifiPd: '',
                            showWifiList: false
                        })
                        this._androidScanWifi()
                    }}
                    hideArrowIcon={true}
                    title={item.SSID}/>
            </View>
        )
    }

    _bottomBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.wifiSiid && (this.state.wifiPd.length >= 8 || this.state.wifiPd.length == 0)}
                onPress={() => {
                    //下一步
                    if (this.state.wifiSiid.indexOf('5G') != -1) {
                        //名称包含5G
                        this.setState({wifiTipsDialog: true})
                        return
                    }
                    if (this.state.wifiPd.length == 0) {
                        //未输入密码
                        this.setState({wifiPwdTipsDialog: true})
                        return
                    }
                    this._checkPermission()
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginLeft: 50,
                    marginTop: 40
                }}
                btnText={strings('next')}/>
        )
    }

    _deleteWifiInfo(wifiSiid) {
        var tempArray = []
        this.state.wifiInfoArray.map((item, index) => {
            if (wifiSiid != item.key) {
                tempArray.push(item)
            }
        })
        StorageHelper.saveWifiInfo(tempArray)
        this.setState({wifiInfoArray: tempArray})
    }

    _saveWifiInfoToLocal(wifiSiid, wifiPd) {
        wifiInfoMap.set(wifiSiid, {
            key: wifiSiid,
            value: wifiPd,
            time: new Date().getTime()
        })
        var tempArray = []
        wifiInfoMap.forEach(function (item, key) {
            tempArray.push({
                key: key,
                value: item.value,
                time: item.time
            })
        })
        StorageHelper.saveWifiInfo(tempArray)
    }

    //蓝牙状态改变
    handleUpdateState = (args) => {
        BluetoothManager._bleLog("蓝牙状态改变:" + JSON.stringify(args));
        BluetoothManager.bluetoothState = args.state;

        if (args.state == "unknow") {
            return
        }
        this.setState({
            bleOnOff: args.state == "on"
        })
    };

    _checkPermission() {
        if (!this.state.bleOnOff) {
            //蓝牙未开启
            this.setState({bleTipsDialog: true})
            return
        }
        if (Platform.OS === 'android') {
            this._androidOpenGps1()
        } else {
            this._toConnecting()
        }
    }

    _androidOpenGps1() {
        NativeModules.RNModule.isLocServiceEnable((result) => {
            if (!result) {
                //安卓未开启定位
                this.setState({gpsTipsDialog: true})
            } else {
                this._toConnecting()
            }
        });
    }

    _toConnecting() {
        /*  this.props.navigation.replace('DeviceConnecting', {
              deviceType: 1,
              wifiSSid: this.state.wifiSiid,
              wifiPwd: this.state.wifiPd,
              longitude: this.state.longitude,
              latitude: this.state.latitude,
              macId: this.state.macId,
              showMac:  this.state.showMac,
          });*/
        this.props.navigation.replace('BleDeviceConnecting', {
            macId: this.state.macId,
            showMac: this.state.showMac,
            wifiSSid: this.state.wifiSiid,
            wifiPwd: this.state.wifiPd,
        });

        this._saveWifiInfoToLocal(this.state.wifiSiid, this.state.wifiPd)
    }
}