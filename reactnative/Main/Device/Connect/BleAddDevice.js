import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {
    Animated,
    AppState,
    DeviceEventEmitter,
    Easing,
    FlatList,
    Image,
    NativeEventEmitter,
    NativeModules,
    PermissionsAndroid,
    Platform,
    View
} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import ViewHelper from "../../View/ViewHelper";
import CommonTextView from "../../View/CommonTextView";
import DeviceCard from "../../View/DeviceCard";
import MessageDialog from "../../View/MessageDialog";
import PagerTitleView from "../../View/PagerTitleView";
import Bluetooth from '../../Bluetooth/Bluetooth';
import EventUtil from "../../Event/EventUtil";
import BleModule from "../../Bluetooth/BleModule";
import BleProtocol from "../../Bluetooth/BleProtocol";
import LogUtil from "../../Utils/LogUtil";
import CommonBtnView from "../../View/CommonBtnView";
import DeviceListUtil from "../../Utils/DeviceListUtil";

var GXRNManager = NativeModules.GXRNManager
global.BluetoothManager = new BleModule();
global.BluetoothProtocol = new BleProtocol();

var dataMap = new Map()
const bleDeviceStartName = 'ugreen gs600'
const bleEarbudsT6Name = 'UGREEN HiTune T6'

//const bleDeviceStartName = 'iPhoned'

async function hasAndroidPermission() {
    const permission = PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION;
    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
        return true;
    }
    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
}

/*
蓝牙添加设备
 */

const animatedDuration = 3000
const scanTime = 30 * 1000
export default class BleAddDevice extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('select_device')}
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            findDeviceArray: [],

            curDeviceArray: [],
            canNotFindDialog: false,

            imgRotate: new Animated.Value(0),
            animRuning: false,
            onBleTime: 0,

            bleTipsDialog: false,
            gpsTipsDialog: false,
            locationTipsDialog: false,
        }
        this.isGoing = false; // 为真旋转
        this.myAnimate = Animated.timing(this.state.imgRotate, {
            toValue: 1,
            duration: animatedDuration,
            easing: Easing.linear,
            useNativeDriver: true// 使用原生驱动动画
        });
        this.scaning = false
    }

    componentWillMount() {
        this._getLocalDeviceList();
        this._bleDisConnectDevice()
        //this.startScaleConfig()
        this._eventListenerAdd()
    }

    _bleDisConnectDevice() {
        if (Platform.OS === "android") {
            NativeModules.RNBLEModule.disConnectDevice()
        } else {
            GXRNManager.disConnectDevice();
        }
    }

    _eventListenerAdd() {
        this.curRoutesListener = DeviceEventEmitter.addListener(EventUtil.CUR_ROUTE, (message) => {
            // 监听当前路由
            if (message == EventUtil.Route_BleAddDevice) {

            }
        });
    }

    _eventListenerRemove() {
        this.curRoutesListener && this.curRoutesListener.remove()
    }

    startScaleConfig() {
        //蓝牙 默认设备名字会是Granwin_BLE，后面怎么定义再说
        NativeModules.RNModule.connectBLEDevice('Granwin_BLE', (result) => {
            if (result == 'success') {
                //扫描、连接蓝牙设备成功
                this.props.navigation.replace('DeviceConnecting', {deviceType: 0});
            } else {

            }
        });
    }

    componentWillUnmount() {
        //Bluetooth.StopSearchBle()
        this._startAnima(false);
        this._eventListenerRemove()

        this._bleDestory()
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF'
                }}>
                {this._titleView()}
                {this._searchingView()}
                {/*  {this._centerImageView()}*/}
                {this._findDeviceView()}
                {ViewHelper.getFlexView()}
                {this._cantNotFindView()}

                {this._dialogView()}
                {this._tipsDialogView()}
            </View>
        );
    }

    _tipsDialogView() {
        return (
            <View>
                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    modalVisible={this.state.bleTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({bleTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        if (Platform.OS === 'android') {
                            Bluetooth._enableBluetooth()
                            this.setState({onBleTime: new Date().getTime()})
                        } else {
                            //ios跳转系统设置
                            GXRNManager.openSystemSetting()
                        }
                        this.setState({bleTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    modalVisible={this.state.gpsTipsDialog}
                    title={strings('warming_tips')}
                    message={strings('open_gps_tips')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({gpsTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        NativeModules.RNModule.openGps()
                        this.setState({gpsTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    modalVisible={this.state.locationTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({locationTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        this.requestPermission()
                        this.setState({locationTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

            </View>
        )
    }

    _dialogView() {
        return (
            <View>
                <MessageDialog
                    onRequestClose={() => {
                        this.setState({canNotFindDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({canNotFindDialog: false})
                    }}
                    modalVisible={this.state.canNotFindDialog}
                    title={strings('can_not_find_device')}
                    message={strings('can_not_find_device_tips1')
                    + '\n\n' + strings('can_not_find_device_tips2')
                    + '\n\n' + strings('can_not_find_device_tips3')
                    + '\n\n' + strings('can_not_find_device_tips4')}
                    leftBtnText={strings('ok')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({canNotFindDialog: false})
                    }}
                    messageStyle={{
                        lineHeight: 20,
                        fontWeight: 'normal'
                    }}/>
            </View>
        )
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('find_device')}
                style={{
                    marginLeft: 23
                }}
            />
        )
    }

    _searchingView() {
        return (
            <CommonTextView
                textSize={14}
                text={strings('find_device_tips')}
                style={{
                    color: '#969AA1',
                    marginLeft: 23,
                    marginTop: 15
                }}
            />
        )
    }

    _centerImageView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 34
                }}>
                <Animated.Image
                    style={{
                        transform: [
                            // 使用interpolate插值函数,实现了从数值单位的映//射转换,上面角度从0到1，这里把它变成0-360的变化
                            {
                                rotate: this.state.imgRotate.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: ['0deg', '360deg']
                                })
                            }
                        ]
                    }}
                    source={require('../../../resources/loading_ic.png')}/>
                <Image
                    style={{
                        position: 'absolute'
                    }}
                    source={require('../../../resources/loading_bluetooth_ic.png')}/>
            </View>
        )
    }

    _findDeviceView() {
        return (
            <FlatList
                style={{marginTop: 20}}
                renderItem={({item, index}) => {
                    return this._deviceItemView(item, index);
                }}
                key={'device'}
                showsVerticalScrollIndicator={false}
                data={this.state.findDeviceArray}
                numColumns={3}
                horizontal={false}
            />
        )
    }

    _deviceItemView(item, index) {
        return (
            <DeviceCard
                onPress={() => {
                    if (item.mac) {
                        this.props.navigation.navigate('WifiDeviceConnectionAndroid',
                            {
                                macId: item.mac,
                                showMac: item.showMac ? item.showMac : ''
                            });
                    }
                }}
                key={index}
                showShadow={true}
                isNewShare={true}
                name={(item.name)}
                deviceTypeName={item.mac}/>
        )
    }

    _cantNotFindView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center'
                }}>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        this.props.navigation.navigate("SelectProductListView");
                        //judy 跳转蓝牙耳机添加界面
                        /* if (Platform.OS === "android") {
                             NativeModules.RNModule.openScanEarbuds()
                         }*/
                    }}
                    style={{
                        marginBottom: 30,
                        height: 54,
                        backgroundColor: '#18B34F'
                    }}
                    btnText={strings('手动添加')}/>
                {/*<CommonTextView
                    onPress={() => {
                        this.setState({canNotFindDialog: true})
                    }}
                    textSize={12}
                    text={strings('can_not_find_device')}
                    style={{
                        color: '#38569B',
                        marginBottom: 30
                    }}
                />*/}
            </View>
        )
    }


    // 运行动画
    _initAnima() {
        if (this.isGoing) {
            this.state.imgRotate.setValue(0);
            this.myAnimate.start(() => {
                this._initAnima();
            });
        }
    }

    _startAnima(flag) {
        this.isGoing = flag;

        if (this.isGoing) {
            if (!this.state.animRuning)
                this.myAnimate.start(() => {
                    this.myAnimate = Animated.timing(this.state.imgRotate, {
                        toValue: 1,
                        duration: animatedDuration,
                        easing: Easing.linear,
                        useNativeDriver: true// 使用原生驱动动画
                    });
                    this._initAnima();
                });
            this.setState({animRuning: true});
        } else {
            this.state.imgRotate.stopAnimation((oneTimeRotate) => {
                // 计算角度比例
                this.myAnimate = Animated.timing(this.state.imgRotate, {
                    toValue: 1,
                    duration: (1 - oneTimeRotate) * animatedDuration,
                    easing: Easing.linear,
                    useNativeDriver: true// 使用原生驱动动画
                });
            });
            this.setState({animRuning: false});
        }
    }


    async componentDidMount() {
        BluetoothManager._bleLog('蓝牙初始化')
        BluetoothManager.start(); //蓝牙初始化、检查蓝牙状态

        if (Platform.OS === 'android') {
            this.checkPermission()
        } else {
            GXRNManager.startBluetoothControl();  // 蓝牙初始化
        }

        this._bleListener()
        this.appStateListener = AppState.addEventListener('change', (status) => {
            // 监听APP状态  前台、后台
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === 'active') { // 后台进入前台
                if (Platform.OS === 'android') {
                    this.checkPermission()
                } else {
                    this._ble()
                }
            }
        });
    }

    _androidOpenGps() {
        NativeModules.RNModule.isLocServiceEnable((result) => {
            if (!result) {
                //安卓未开启定位
                this.setState({gpsTipsDialog: true})
            } else {
                this._androidBlueToothAndroid12NewPermission()
                // this.checkPermission()
            }
        });
    }

    _androidBlueToothAndroid12NewPermission() {
        NativeModules.RNModule.buildVersionBigAndroid12((result) => {
            if (result) {
                //Android 版本大于等于 Android12时
                try {
                    PermissionsAndroid.requestMultiple(
                        [
                            PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN,
                            PermissionsAndroid.PERMISSIONS.BLUETOOTH_ADVERTISE,
                            PermissionsAndroid.PERMISSIONS.BLUETOOTH_CONNECT],
                        {
                            'title': strings('warming_tips'),
                            'message': strings('ble_off_tips')
                        }
                    ).then((result) => {
                        if (result['android.permission.BLUETOOTH_SCAN']
                            && result['android.permission.BLUETOOTH_ADVERTISE']
                            && result['android.permission.BLUETOOTH_CONNECT'] === 'granted') {
                            console.log("Thank you for your permission! :BLUETOOTH_SCAN)");

                            //判断蓝牙是否开启
                            BluetoothManager.start(); //蓝牙初始化、检查蓝牙状态
                        } else if (result['android.permission.BLUETOOTH_SCAN']
                            || result['android.permission.BLUETOOTH_ADVERTISE']
                            || result['android.permission.BLUETOOTH_CONNECT'] === 'never_ask_again') {
                            this.onShowToast('Please Go into Settings -> Applications -> APP_NAME -> Permissions and Allow permissions to continue');
                        }
                    });
                } catch (err) {
                    console.warn(err)
                }
            } else {
                //判断蓝牙是否开启
                BluetoothManager.start(); //蓝牙初始化、检查蓝牙状态
            }
        });
    }

    async checkPermission() {
        const result = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        const result1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
        if (result && result1) {
            this._androidOpenGps()
        } else {
            this.setState({locationTipsDialog: true})
        }
    }

    //安卓手机获取当前wifi名称需要获取定位权限
    async requestPermission() {
        try {
            PermissionsAndroid.requestMultiple(
                [
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION],
                {
                    'title': 'Wifi networks',
                    'message': 'We need your permission in order to find wifi networks'
                }
            ).then((result) => {
                if (result['android.permission.ACCESS_COARSE_LOCATION']
                    && result['android.permission.ACCESS_FINE_LOCATION'] === 'granted') {
                    LogUtil.debugLog("Thank you for your permission! :ACCESS_FINE_LOCATION)");
                    this._ble()
                } else if (result['android.permission.ACCESS_COARSE_LOCATION']
                    || result['android.permission.ACCESS_FINE_LOCATION'] === 'never_ask_again') {
                    this.onShowToast('Please Go into Settings -> Applications -> APP_NAME -> Permissions and Allow permissions to continue');
                }
            });
        } catch (err) {
            LogUtil.debugLog(err)
        }
    }

    async _ble() {
        if (Platform.OS === "android" && !(await hasAndroidPermission())) {
            return;
        }

        this._startAnima(true);
        this._bleScan()
    }

    _bleScan() {
        //扫描设备
        if (Platform.OS === "android") {
            //安卓
            NativeModules.RNBLEModule.disConnectDevice()
            NativeModules.RNBLEModule.scanDevices(bleDeviceStartName, (callback) => {
                if (callback == 'success') {
                    BluetoothManager._bleLog('开启扫描成功')
                } else {
                    BluetoothManager._bleLog('开启扫描失败')
                }
            })
        } else {
            BluetoothManager._bleLog('开始扫描')
            //ios
            GXRNManager.disConnectDevice();
            GXRNManager.scanDevices(bleDeviceStartName).then((datas) => {
                // 只是开启扫描，设备返回在通知里
            }).catch((err) => {
                BluetoothManager._bleLog("扫描蓝牙设备error:" + JSON.stringify(err));
            });


        }
    }

    _bleListener() {
        //蓝牙状态监听
        this.updateStateListener = BluetoothManager.addListener(
            "BleManagerDidUpdateState",
            this.handleUpdateState
        );

        if (Platform.OS === "android") {
            //监听扫描到符合规则的蓝牙设备
            this.bleScanListener = DeviceEventEmitter.addListener(EventUtil.BLE_SCAN, (res) => {
                BluetoothManager._bleLog("扫描到的蓝牙设备:" + JSON.stringify(res));

                this._setFindData(res)
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            eventEmitter.addListener(EventUtil.BLE_SCAN, (res) => {
                BluetoothManager._bleLog("iOS扫描到的蓝牙设备:" + JSON.stringify(res));
                this._setFindData(res);
            });

            eventEmitter.addListener(EventUtil.BLE_NOTIFY_DATA, (res) => {
                BluetoothManager._bleLog("接收到数据:" + JSON.stringify(res));
            });

            eventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("连接状态:" + JSON.stringify(res));
            });
        }
    }

    //获取本地缓存设备列表
    _getLocalDeviceList() {
        DeviceListUtil._getLocalDeviceList()
            .then((res) => {
                var deviceInfo = res
                this.setState({curDeviceArray: deviceInfo})
            })
            .catch((error) => {

            })
    }

    _isContainDev(mac) {
        let isContain = false;
        this.state.curDeviceArray.map((item, index) => {
            if (item.mac == mac) {
                isContain = true;
            }
        });
    }

    _setFindData(scanData) {
        if (this.state.findDeviceArray.length < 3) {
            dataMap.set(scanData.mac, scanData)
            var tempArray = []
            dataMap.forEach(function (item) {
                if (item.mac) {
                    tempArray.push(item)
                }
            })
            this.setState({
                findDeviceArray: tempArray.slice(0, 3)
            })
        }
    }

    _bleDestory() {
        this.updateStateListener && this.updateStateListener.remove();
        this.bleScanListener && this.bleScanListener.remove();
        this.appStateListener && this.appStateListener.remove();
        if (Platform.OS != "android") {
            GXRNManager.stopScanDevice();
        }
    }

    //蓝牙状态改变
    handleUpdateState = (args) => {
        BluetoothManager._bleLog("蓝牙状态改变:" + JSON.stringify(args));
        BluetoothManager.bluetoothState = args.state;
        if (args.state == "on") {
            //蓝牙打开时自动搜索
            this._ble()
        } else if (args.state == "off") {
            //蓝牙未打开
            if (new Date().getTime() - this.state.onBleTime > 5000) {
                this.setState({bleTipsDialog: true})
            }
        }
    };

}
