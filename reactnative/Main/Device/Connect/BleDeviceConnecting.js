import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {
    Animated,
    DeviceEventEmitter,
    Easing,
    Image,
    NativeEventEmitter,
    NativeModules,
    Platform,
    View
} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import CommonTextView from "../../View/CommonTextView";
import PagerTitleView from "../../View/PagerTitleView";
import NetUtil from "../../Net/NetUtil";
import StorageHelper from "../../Utils/StorageHelper";
import EventUtil from "../../Event/EventUtil";
import BleModule from "../../Bluetooth/BleModule";
import MainDeviceList from "../MainDeviceList";
import LogUtil from "../../Utils/LogUtil";
//import Bluetooth from "../../Bluetooth/Bluetooth";
global.BluetoothManager = new BleModule();
import {
    CRC
} from '../../Utils/CRC'

var GXRNManager = NativeModules.GXRNManager

/*
*设备连接界面
*蓝牙设备连接
 */

const animatedDuration = 3000
const animatedDurationLoading = 1000

var deviceMac = ''
var reBindTimes = 0//重新绑定的次数  5s一次，一共120秒  可以重新调用24次

export default class BleDeviceConnecting extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('绑定设备')}
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            imgRotate: new Animated.Value(0),
            animRuning: false,
            imgRotateLoading: new Animated.Value(0),
            animRuningLoading: false,
            bleStep: 0,
            macId: props.navigation.state.params.macId,
            showMac: props.navigation.state.params.showMac,
            wifiSSid: props.navigation.state.params.wifiSSid,
            wifiPwd: props.navigation.state.params.wifiPwd,
            configMac: '',

            isAutoDisconnect: false,
            isStartBind: false,
        }
        this.isGoing = false; // 为真旋转
        this.myAnimate = Animated.timing(this.state.imgRotate, {
            toValue: 1,
            duration: animatedDuration,
            easing: Easing.linear,
            useNativeDriver: true// 使用原生驱动动画
        });
        this.isGoingLoading = false; // 为真旋转
        this.myAnimateLoading = Animated.timing(this.state.imgRotateLoading, {
            toValue: 1,
            duration: animatedDurationLoading,
            easing: Easing.linear,
            useNativeDriver: true// 使用原生驱动动画
        });
    }

    componentWillMount() {
        this.setState({isStartBind: false})

        this._startAnimaLoading(true);
        //开始连接设备
        this._connectToDevice()
    }

    // 数据站buffer
    data2buffer(data) {
        var dataModbus = CRC.ToModbusCRC16('A1C0' + data)
        let dataAllStr = '5AA5A1C0' + data + dataModbus
        return dataAllStr
    }

    bytesToHexString(arrBytes) {
        var str = "";
        for (var i = 0; i < arrBytes.length; i++) {
            var tmp;
            var num = arrBytes[i];
            if (num < 0) {
                //此处填坑，当byte因为符合位导致数值为负时候，需要对数据进行处理
                tmp = (255 + num + 1).toString(16);
            } else {
                tmp = num.toString(16);
            }
            if (tmp.length == 1) {
                tmp = "0" + tmp;
            }
            str += tmp;
        }
        return str;
    }


    _bleToConnectSuccessResult() {
        const that = this
        if (this.state.isAutoDisconnect) {
            //查询配网状态
            that.sendInterval = setInterval(function () {
                let sendNumber = that.sendNumber
                if (sendNumber > 5) {
                    clearInterval(that.sendInterval)
                }
                // console.log('发送查询配网状态指令')
                that.sendNumber = sendNumber + 1
                if (Platform.OS === "android") {
                    LogUtil.debugLog('sendSimpleData:' + that.data2buffer('050000'))
                    NativeModules.RNBLEModule.sendSimpleDataBytes(that.data2buffer('050000'))
                } else {
                    GXRNManager.sendData(dataBuffer);
                }
            }, 1000)
        } else {
            //连接成功
            //向设备发送wifi 和 密码
            LogUtil.debugLog('向设备发送wifi 和 密码:' + this.state.wifiSSid + "_" + this.state.wifiPwd)

            let ssid = 'SSID:' + this.state.wifiSSid
            let pass = 'PASS:' + this.state.wifiPwd

            if (Platform.OS === "android") {
                NativeModules.RNBLEModule.sendSimpleData(ssid)
            } else {
                GXRNManager.sendData(ssid);
            }
            setTimeout(() => {
                if (Platform.OS === "android") {
                    NativeModules.RNBLEModule.sendSimpleData(pass)
                } else {
                    GXRNManager.sendData(pass);
                }
            }, 500);
        }
    }

    _saveBleDevice() {
        var bleDevices = MainDeviceList.getBleDevices()
        var tempBleDevices = []
        var time = new Date().getTime().toString()
        var saveFlag = true
        var itemData = {
            electricQuantity: 0,
            deviceType: "Gendome Home 3000",
            isWifiDevice: false,
            deviceName: "Gendome Home 3000",//用户设备名称
            sharer: '',//分享者编号 0 表示是自己配网绑定
            appuserId: '',//用户编号
            productId: '',//产品编号
            createTime: time,//创建时间
            uType: "-1",//1 所有者 2 管理员 3 成员
            updateTime: time,//更新时间
            productKey: '',//产品ID
            isOnline: false,//在线状态
            mac: this.state.macId,//设备mac
            deviceId: "",//设备id
            deviceInfo: null
        }
        bleDevices.map((item, index) => {
            if (item.mac == this.state.macId) {
                saveFlag = false
            } else {
                tempBleDevices.push(item)
            }

        })

        if (saveFlag) {
            tempBleDevices.push(itemData)

            var param = {
                bleDevice: tempBleDevices
            }
            NetUtil.setUserParams(StorageHelper.getTempToken(), param, true)
                .then((res) => {
                    MainDeviceList.setBleDevices(tempBleDevices)
                })
        }
    }


    _bleToConnectFailResult() {
        this.props.navigation.replace('DeviceConnectedResult', {
            isBle: true,
            connectedSuccessed: false,
            macId: this.state.macId,
            showMac: this.state.showMac,
            wifiSSid: this.state.wifiSiid,
            wifiPwd: this.state.wifiPd,
        });
    }

    componentWillUnmount() {
        this._bleDisConnectDevice();
        this._startAnima(false);
        this._startAnimaLoading(false);
        this._bleDestory()

        reBindTimes = 0
        this._stopReBind()
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF'
                }}>
                {this._centerImageView()}
                {this._centerTipsView()}
                {this._bottomStatusView()}
            </View>
        );
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('device_connect')}
                style={{
                    marginLeft: 23
                }}
            />
        )
    }

    _centerImageView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 34
                }}>
                <Animated.Image
                    style={{
                        transform: [
                            // 使用interpolate插值函数,实现了从数值单位的映//射转换,上面角度从0到1，这里把它变成0-360的变化
                            {
                                rotate: this.state.imgRotate.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: ['0deg', '360deg']
                                })
                            }
                        ]
                    }}
                    source={require('../../../resources/loading_ic.png')}/>
                <Image
                    style={{
                        position: 'absolute'
                    }}
                    source={require('../../../resources/loading_bluetooth_connect_ic.png')}/>
            </View>
        )
    }

    _centerTipsView() {
        return (
            /* <CommonTextView
                 textSize={14}
                 text={strings('ble_on_close_to_device')}
                 style={{
                     color: '#969AA1',
                     width: this.mScreenWidth - 80,
                     textAlign: 'center',
                     marginLeft: 40
                 }}
             />*/
            <CommonTextView
                textSize={14}
                text={
                    this.state.bleStep == 0 ? '40%'
                        : this.state.bleStep == 1 ? '60%'
                        : this.state.bleStep == 2 ? '80%' : '100%'
                }
                style={{
                    color: '#404040',
                    fontSize: 35,
                    width: this.mScreenWidth - 80,
                    textAlign: 'center',
                    marginLeft: 40
                }}
            />
        )
    }

    _bottomStatusView() {
        return (
            <View
                style={{
                    flex: 1,
                    marginLeft: 50,
                    marginRight: 50,
                    //alignItems: 'center',
                    justifyContent: 'center'
                }}>
                {
                    this.state.bleStep == 0 ?
                        <View
                            style={{
                                flexDirection: 'column',
                                marginBottom: 11
                            }}>

                            {this._bottomStatusItemView(true, strings('正在连接设备'))}
                            {this._bottomStatusItemView(false, strings('请将手机与设备尽量靠近'))}
                            {this._bottomStatusItemView(false, strings('向设备传输信息'))}
                            {this._bottomStatusItemView(false, strings('设备连接网络'))}

                        </View>
                        : this.state.bleStep == 1 ?
                        <View
                            style={{
                                flexDirection: 'column',
                                marginBottom: 11
                            }}>

                            {this._bottomStatusItemView(false, strings('正在连接设备'))}
                            {this._bottomStatusItemView(true, strings('向设备传输信息'))}
                            {this._bottomStatusItemView(false, strings('请勿关闭蓝牙或WiFi'))}
                            {this._bottomStatusItemView(false, strings('设备连接网络'))}

                        </View>
                        :
                        <View
                            style={{
                                flexDirection: 'column',
                                marginBottom: 11
                            }}>

                            {this._bottomStatusItemView(false, strings('正在连接设备'))}
                            {this._bottomStatusItemView(false, strings('向设备传输信息'))}
                            {this._bottomStatusItemView(true, strings('设备连接网络'))}
                            {this._bottomStatusItemView(false, strings('等待设备连接中'))}

                        </View>
                }
            </View>
        )
    }

    _bottomStatusItemView(isLoading, text) {
        return (

            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                <CommonTextView
                    textSize={isLoading ? 20 : 14}
                    text={text}
                    style={{
                        color: isLoading ? '#2F2F2F' : '#999BA2',
                        marginLeft: 6,
                        fontWeight: isLoading ? 'bold' : 'normal',
                        marginTop: 7
                    }}
                />
            </View>
        )
    }


// 运行动画
    _initAnima() {
        if (this.isGoing) {
            this.state.imgRotate.setValue(0);
            this.myAnimate.start(() => {
                this._initAnima();
            });
        }
    }

    _startAnima(flag) {
        this.isGoing = flag;

        if (this.isGoing) {
            if (!this.state.animRuning)
                this.myAnimate.start(() => {
                    this.myAnimate = Animated.timing(this.state.imgRotate, {
                        toValue: 1,
                        duration: animatedDuration,
                        easing: Easing.linear,
                        useNativeDriver: true// 使用原生驱动动画
                    });
                    this._initAnima();
                });
            this.setState({animRuning: true});
        } else {
            this.state.imgRotate.stopAnimation((oneTimeRotate) => {
                // 计算角度比例
                this.myAnimate = Animated.timing(this.state.imgRotate, {
                    toValue: 1,
                    duration: (1 - oneTimeRotate) * animatedDuration,
                    easing: Easing.linear,
                    useNativeDriver: true// 使用原生驱动动画
                });
            });
            this.setState({animRuning: false});
        }
    }

// 运行动画
    _initAnimaLoading() {
        if (this.isGoingLoading) {
            this.state.imgRotateLoading.setValue(0);
            this.myAnimateLoading.start(() => {
                this._initAnimaLoading();
            });
        }
    }

    _startAnimaLoading(flag) {
        this.isGoingLoading = flag;

        if (this.isGoingLoading) {
            if (!this.state.animRuningLoading)
                this.myAnimateLoading.start(() => {
                    this.myAnimateLoading = Animated.timing(this.state.imgRotateLoading, {
                        toValue: 1,
                        duration: animatedDurationLoading,
                        easing: Easing.linear,
                        useNativeDriver: true// 使用原生驱动动画
                    });
                    this._initAnimaLoading();
                });
            this.setState({animRuningLoading: true});
        } else {
            this.state.imgRotateLoading.stopAnimation((oneTimeRotate) => {
                // 计算角度比例
                this.myAnimateLoading = Animated.timing(this.state.imgRotateLoading, {
                    toValue: 1,
                    duration: (1 - oneTimeRotate) * animatedDurationLoading,
                    easing: Easing.linear,
                    useNativeDriver: true// 使用原生驱动动画
                });
            });
            this.setState({animRuningLoading: false});
        }
    }

    _connectToDevice() {
        this._bleListener()
        if (this.state.macId) {
            this.setState({isAutoDisconnect: false})
            this.connect(this.state.macId)
        }
    }

    _bleDisConnectDevice() {
        if (Platform.OS === "android") {
            NativeModules.RNBLEModule.disConnectDevice()
        } else {
            GXRNManager.disConnectDevice();
        }
    }

    connect(macId) {
        //连接蓝牙设备
        if (Platform.OS === 'android') {
            NativeModules.RNBLEModule.connectDevice(macId, (callback) => {

            })
        } else {
            GXRNManager.connectDevice(macId).then((datas) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(datas));
                this.setState({bleStep: 1})
                setTimeout(() => {
                    this.setState({bleStep: 2})
                    this._bleToConnectSuccessResult()
                }, 3000);
            }).catch((err) => {
                BluetoothManager._bleLog("蓝牙连接状态err:" + JSON.stringify(err));
                this.setState({bleStep: 0})
                this._bleToConnectFailResult()
            });
        }
    }

    _deviceRestartAction() {
        if (this.state.configMac == '') {
            //设备主动断开连接
            this.setState({isAutoDisconnect: true})
            if (this.state.macId) {
                this.connect(this.state.macId)
            }
        }
    }


    _wifiToConnectSuccessResult() {
        this.props.navigation.replace('DeviceConnectedResult', {
            isBle: false,
            connectedSuccessed: true,
            macId: this.state.macId,
            showMac: this.state.showMac,
            wifiSSid: this.state.wifiSiid,
            wifiPwd: this.state.wifiPd,
            wifiMac: deviceMac,
        });
    }

    _wifiToConnectFailResult() {
        this.props.navigation.replace('DeviceConnectedResult', {
            isBle: false,
            connectedSuccessed: false,
            macId: this.state.macId,
            showMac: this.state.showMac,
            wifiSSid: this.state.wifiSiid,
            wifiPwd: this.state.wifiPd,
            wifiMac: deviceMac,
        });
    }

    //绑定设备
    _bindDevice(mac) {
        deviceMac = mac

        BluetoothManager._bleLog("NetUtil.bindDevice:" + mac);
        NetUtil.bindDevice(StorageHelper.getTempToken(), mac, reBindTimes < 22)
            .then((res) => {
                //绑定设备成功
                reBindTimes = 0
                this._wifiToConnectSuccessResult()
                EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY)
            }).catch((error) => {
            //如果首次返回失败，可能是设备和云端未连接，需要多次调用绑定设备接口

            if (reBindTimes < 22) {
                //重新绑定
                reBindTimes = parseInt(reBindTimes + 1)
                this._startReBind()
            } else {
                this._wifiToConnectFailResult()
            }
        })
    }

    _startReBind() {
        this._stopReBind()
        this.intervalRebind = setInterval(() => {
            if (deviceMac != '') {
                this._bindDevice(deviceMac)
            } else {
                //mac是空，则直接跳转失败页面
                this._wifiToConnectFailResult()
            }
            this._stopReBind()
        }, 5000)
    }

    _stopReBind() {
        if (this.intervalRebind) {
            clearInterval(this.intervalRebind)
        }
    }

    int2ascll(hexCharCodeStr) {
        let hexCharCodeStrs = hexCharCodeStr
        var trimedStr = hexCharCodeStrs.trim();
        var rawStr = trimedStr.substr(0, 2).toLowerCase() === "0x" ? trimedStr.substr(2) : trimedStr;
        var len = rawStr.length;
        if (len % 2 !== 0) {
            return "";
        }
        var curCharCode;
        var resultStr = [];
        for (var i = 0; i < len; i = i + 2) {
            curCharCode = parseInt(rawStr.substr(i, 2), 16);
            resultStr.push(String.fromCharCode(curCharCode));
        }
        return resultStr.join("");
    }


    _bleListener() {
        //蓝牙状态监听
        this.updateStateListener = BluetoothManager.addListener(
            "BleManagerDidUpdateState",
            this.handleUpdateState
        );

        if (Platform.OS === 'android') {
            this.bleNotifyDataListener = DeviceEventEmitter.addListener(EventUtil.BLE_NOTIFY_DATA, (res) => {
                BluetoothManager._bleLog("蓝牙数据上报:" + JSON.stringify(res));
                let data = res.data;
                if (data.indexOf('5AA5C0A1051E00') != -1) {
                    let iotSnHex = data.substring(14, 44);
                    let iot = this.int2ascll(iotSnHex)

                    if (!this.state.isStartBind) {
                        //断开蓝牙
                        this._bleDisConnectDevice();
                        this._stopReBind()

                        this.setState({configMac: iot})
                        this.setState({isStartBind: true})
                        this._bindDevice(iot)
                    }
                }
            });
            //监听蓝牙通知状态
            this.bleNotifyListener = DeviceEventEmitter.addListener(EventUtil.BLE_NOTIFY_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙通知状态:" + JSON.stringify(res));
                if (res.status == 'onNotifySuccess') {
                    BluetoothManager._bleLog("onNotifySuccess蓝牙通知成功");
                } else if (res.status == 'onNotifyFailure') {
                    BluetoothManager._bleLog("onNotifyFailure蓝牙通知失败");
                }
            });
            //监听蓝牙连接状态
            this.bleConnectListener = DeviceEventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(res));
                if (res.status == 'onStartConnect') {
                    BluetoothManager._bleLog("onStartConnect开始连接");
                } else if (res.status == 'onConnectFail') {
                    BluetoothManager._bleLog("onConnectFail连接失败");
                    this._deviceRestartAction()
                    //this.setState({bleStep: 0})
                    //this._bleToConnectFailResult()
                } else if (res.status == 'onConnectSuccess') {
                    BluetoothManager._bleLog("onConnectSuccess连接成功");
                    this.setState({bleStep: 1})
                    setTimeout(() => {
                        this.setState({bleStep: 2})
                        this._bleToConnectSuccessResult()
                    }, 3000);
                } else if (res.status == 'onDisConnected') {
                    BluetoothManager._bleLog("onDisConnected连接中断");
                    this._deviceRestartAction()
                    //this.setState({bleStep: 0})
                    //this._bleToConnectFailResult()
                }
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            //监听蓝牙通知状态
            eventEmitter.addListener(EventUtil.BLE_NOTIFY_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙通知状态:" + JSON.stringify(res));
                if (res.status == 'onNotifySuccess') {
                    BluetoothManager._bleLog("onNotifySuccess蓝牙通知成功");
                } else if (res.status == 'onNotifyFailure') {
                    BluetoothManager._bleLog("onNotifyFailure蓝牙通知失败");
                }
            });
            //监听蓝牙连接状态
            eventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(res));
                if (res) {
                    BluetoothManager._bleLog("onConnectSuccess连接成功");
                    this.setState({bleStep: 1})
                    setTimeout(() => {
                        this.setState({bleStep: 2})
                        this._bleToConnectSuccessResult()
                    }, 3000);
                } else {
                    BluetoothManager._bleLog("onConnectFail连接失败");
                    this._deviceRestartAction()
                    //this.setState({bleStep: 0})
                    //this._bleToConnectFailResult()
                }
            });
        }

    }

    _bleDestory() {
        this.updateStateListener && this.updateStateListener.remove();
        this.bleConnectListener && this.bleConnectListener.remove();
        this.bleNotifyListener && this.bleNotifyListener.remove();
        //this.sendInterval && clearInterval(this.sendInterval)
    }


//蓝牙状态改变
    handleUpdateState = (args) => {
        BluetoothManager._bleLog("蓝牙状态改变:" + JSON.stringify(args));
        BluetoothManager.bluetoothState = args.state;
        if (args.state == "on") {
        } else if (args.state == "off") {

        }
    };
}