import BaseComponent from '../../../Main/Base/BaseComponent';
import React from 'react';
import {BackHandler, Image, Keyboard, NativeModules, Platform, TextInput, TouchableOpacity, View} from 'react-native';

import TitleBar from '../../View/TitleBar'
import {strings} from '../../Language/I18n';
import ViewHelper from "../../View/ViewHelper";
import CommonTextView from "../../View/CommonTextView";
import PagerTitleView from "../../View/PagerTitleView";
import CommonBtnView from "../../View/CommonBtnView";
import RouterUtil from "../../Utils/RouterUtil";
import StorageHelper from "../../Utils/StorageHelper";
import DeviceListUtil from "../../Utils/DeviceListUtil";
import NetUtil from "../../Net/NetUtil";
import LogUtil from "../../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager
let that

/*
*设备连接结果界面
* 连接成功、连接失败
 */
export default class DeviceConnectedResult extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings("add_device")}
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        that._bleDisConnectDevice()
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        that = this
        this.state = {
            isBle: this.props.navigation.state.params.isBle ? this.props.navigation.state.params.isBle : false,
            connectedSuccessed: this.props.navigation.state.params.connectedSuccessed ? this.props.navigation.state.params.connectedSuccessed : false,
            deviceName: 'GS600户外电源',
            curSiid: '',
            token: '',
            mac: this.props.navigation.state.params.mac ? this.props.navigation.state.params.mac : '',
            wifiMac: this.props.navigation.state.params.wifiMac ? this.props.navigation.state.params.wifiMac : '',
            keyboardHeight: 0,
            macId: this.props.navigation.state.params.macId ? this.props.navigation.state.params.macId : '',
            tempUnit: 1,
            showMac: this.props.navigation.state.params.showMac ? this.props.navigation.state.params.showMac : '',
        }
    }

    componentWillMount() {
        this._getLocalTempUnit()
        this._setPreData()

        this.setState({token: StorageHelper.getTempToken()})

        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (event) => {
            this.setState({keyboardHeight: event.endCoordinates.height})
        });
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (event) => {
            this.setState({keyboardHeight: 0})
        });

        if (Platform.OS === 'android') {
            this.backListener = BackHandler.addEventListener('hardwareBackPress', () => {
                this._bleDisConnectDevice()
            })
        }
    }

    _getLocalTempUnit() {
        StorageHelper.getTempUnit()
            .then((unit) => {
                var unitValue = 1
                unitValue = parseInt(unit) == 0 ? 2 : 1

                this.setState({
                    tempUnit: unitValue,
                })
            }).catch((error) => {
        })
    }

    componentWillUnmount() {
        this.keyboardDidShowListener && this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener && this.keyboardDidHideListener.remove()
        if (Platform.OS === 'android')
            this.backListener && this.backListener.remove();
    }

    _setPreData() {
        if (this.state.isBle) {
            //蓝牙设备
            if (this.state.connectedSuccessed) {
                //连接成功
            } else {
                //连接失败
            }
        } else {
            //wifi设备
            if (this.state.connectedSuccessed) {
                //连接成功
            } else {
                //连接失败
                this.setState({
                    curSiid: this.props.navigation.state.params.curSiid
                })
            }
        }
    }


    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF'
                }}>
                {this.state.isBle && this.state.connectedSuccessed ? ViewHelper.getFixFlexView(1.3) : null}
                {this.state.isBle && !this.state.connectedSuccessed ? ViewHelper.getFixFlexView(0.8) : null}
                {!this.state.isBle && this.state.connectedSuccessed ? ViewHelper.getFixFlexView(0.6) : null}
                {!this.state.isBle && !this.state.connectedSuccessed ? ViewHelper.getFixFlexView(1) : null}
                {this._imageView()}
                {!this.state.connectedSuccessed ? this._wifiConnectFailWifiNameView() : null}
                {this.state.isBle && this.state.connectedSuccessed ? ViewHelper.getFixFlexView(0.6) : null}
                {this.state.isBle && !this.state.connectedSuccessed ? ViewHelper.getFixFlexView(0.6) : null}
                {!this.state.isBle && this.state.connectedSuccessed ? ViewHelper.getFixFlexView(0.3) : null}

                {!this.state.isBle && !this.state.connectedSuccessed ? ViewHelper.getFixFlexView(1) : null}
                {this._connectTextView()}
                {!this.state.isBle && this.state.connectedSuccessed ? this._wifiConnectSuccessDeviceNameView() : null}
                {this.state.keyboardHeight == 0 ?
                    ViewHelper.getFlexView() :
                    <View
                        style={{
                            width: this.mScreenWidth,
                            height: this.state.keyboardHeight,
                        }}/>}
                {this.state.keyboardHeight == 0 ? this._bottomBtnView() : null}
            </View>
        );
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={this.state.connectedSuccessed ? strings('connected_successed') : strings('connected_failed')}
                style={{
                    marginLeft: 23
                }}
            />
        )
    }

    _imageView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <Image
                    source={this.state.connectedSuccessed ? require('../../../resources/greenUnion/connect_success_img.png') : require('../../../resources/greenUnion/connect_failure_ic.png')}/>
            </View>
        )
    }

    /*
    *连接结果文字提示
     */
    _connectTextView() {
        return (
            <View>
                {this.state.connectedSuccessed ? this._connectSuccessTextView() : this._connectFailTextView()}
            </View>
        )
    }

    /*
    *连接成功文字
     */
    _connectSuccessTextView() {
        return (
            <View>
                {this.state.isBle ? this._bleConnectSuccessTextView() : this._wifiConnectSuccessTextView()}
            </View>
        )
    }

    /*
   *连接失败文字
    */
    _connectFailTextView() {
        return (
            <View>
                {this._wifiConnectFailTextView()}
            </View>
        )
    }

    /*
    *蓝牙连接成功文字
     */
    _bleConnectSuccessTextView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 70,
                    marginLeft: 35,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CommonTextView
                    textSize={18}
                    style={{
                        color: '#282A30',
                        textAlign: 'center'
                    }}
                    text={strings('connected_successed_ble_tips1')}/>

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#969AA1',
                        marginTop: 6,
                        textAlign: 'center',
                        lineHeight: 22
                    }}
                    text={strings('connected_successed_ble_tips2')}/>


                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000',
                        marginTop: 36,
                        textAlign: 'center'
                    }}
                    text={'Gendome Home 3000'}/>
            </View>
        )
    }

    /*
   *蓝牙连接失败文字
    */
    _bleConnectFailTextView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 70,
                    marginLeft: 35,
                }}>

                <CommonTextView
                    textSize={12}
                    style={{
                        color: '#969AA1',
                        marginTop: 9,
                        lineHeight: 28
                    }}
                    text={strings('ble_connect_failed_tips1') + '\n'
                    + strings('ble_connect_failed_tips2')}/>
            </View>
        )
    }

    /*
    *wifi连接成功文字
     */
    _wifiConnectSuccessTextView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 70,
                    marginLeft: 35,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CommonTextView
                    textSize={24}
                    style={{
                        color: '#282A30',
                        fontWeight: 'bold',
                        textAlign: 'center'
                    }}
                    text={strings('设备添加成功')}/>

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#969AA1',
                        marginTop: 6,
                        textAlign: 'center',
                        lineHeight: 22
                    }}
                    text={strings('欢迎来到绿联的无线互联世界')}/>
            </View>
        )
    }

    /*
   *wifi连接失败文字
    */
    _wifiConnectFailTextView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 80,
                    marginLeft: 40,
                }}>

                <CommonTextView
                    textSize={13}
                    style={{
                        color: '#969AA1',
                        marginTop: 9,
                    }}
                    text={strings('wifi_connect_failed_tips1') + '\n'
                    + strings('wifi_connect_failed_tips2') + '\n'
                    + strings('wifi_connect_failed_tips3') + '\n\n\n'
                    + strings('wifi_connect_failed_tips4')}/>
            </View>
        )
    }


    /*
    *底部按钮
     */
    _bottomBtnView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingBottom: 44
                }}>
                {this.state.connectedSuccessed ? this._bottomSuccessBtnView() : this._bottomFailBtnView()}
            </View>
        )
    }

    _bottomSuccessBtnView() {
        return (
            <View>
                {this.state.isBle ? this._bleConnectSuccessBtnView() : this._wifiConnectSuccessBtnView()}
            </View>
        )
    }

    _bottomFailBtnView() {
        return (
            <View>
                {this.state.isBle ? this._bleConnectFailBtnView() : this._wifiConnectFailBtnView()}
            </View>
        )
    }

    _bleConnectSuccessBtnView() {
        return (
            <View>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        //蓝牙-WiFi连接
                        this._bleDisConnectDevice()
                        this.props.navigation.navigate('BleAddDevice', {token: this.state.token});
                        //  this.props.navigation.navigate('AddDevice', {token: this.state.token});
                    }}
                    style={{
                        width: this.mScreenWidth - 100,
                        backgroundColor: '#FF8748'
                    }}
                    btnText={strings('wifi_connect')}/>

                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        //蓝牙-直接使用
                        this.props.navigation.replace('DeviceDetail', {
                            itemData: null,
                            title: 'Gendome Home 3000',
                            isWifiConnect: false,
                            tempUnit: this.state.tempUnit,
                            mac: this.state.macId,
                            showMac: this.state.showMac,
                            uType: "-1"
                        });
                    }}
                    style={{
                        width: this.mScreenWidth - 100,
                        marginTop: 20
                    }}
                    btnText={strings('dir_use')}/>
            </View>
        )
    }

    _bleConnectFailBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //蓝牙-重试
                    if (this.state.macId) {
                        this.props.navigation.replace('BleDeviceConnecting', {macId: this.state.macId});
                    }
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginTop: 20
                }}
                btnText={strings('reconnect')}/>
        )
    }

    _wifiConnectSuccessBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //wifi-完成
                    this._wifiSuccessed()
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginTop: 20
                }}
                btnText={strings('开始使用')}/>
        )
    }

    _wifiConnectFailBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //wifi-重试   回到设备重置页面
                    this._bleDisConnectDevice()
                    this.props.navigation.navigate('BleAddDevice', {token: this.state.token});

                    //this.props.navigation.replace('AddDevice', {token: this.state.token});
                }}
                style={{
                    width: this.mScreenWidth - 100,
                    marginTop: 20
                }}
                btnText={strings('返回重试')}/>
        )
    }

    _bleDisConnectDevice() {
        if (Platform.OS === "android") {
            NativeModules.RNBLEModule.disConnectDevice()
        } else {
            GXRNManager.disConnectDevice();
        }
    }

    /*
    *wifi连接成功修改设备名称输入框
     */
    _wifiConnectSuccessDeviceNameView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center'
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 20,
                        height: 40,
                        backgroundColor: '#F7F8FB',
                        borderRadius: 27,
                        width: this.mScreenWidth * 0.64,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>

                    <TextInput
                        style={{
                            height: 40,
                            textAlign: 'left',
                            paddingLeft: 15,
                            paddingRight: 12,
                            flex: 1,
                            fontSize: this.getSize(14),
                            color: '#000',
                            fontFamily: 'DIN Alternate Bold'
                        }}
                        maxLength={32}
                        onChangeText={(text) => {
                            this.setState({deviceName: text.toString().trim()})
                        }}
                        value={this.state.deviceName}/>

                    {
                        this.state.deviceName ?
                            <TouchableOpacity
                                style={{
                                    padding: 10
                                }}
                                onPress={() => {
                                    this.setState({deviceName: ''})
                                    this.dissmissKeyboard()
                                }}>
                                <Image
                                    source={require('../../../resources/delete_all_ic.png')}/>
                            </TouchableOpacity> : null
                    }

                </View>
            </View>
        )
    }

    /*
    *wifi连接失败热点名称
     */
    _wifiConnectFailWifiNameView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 27
                }}>
                <CommonTextView
                    textSize={18}
                    style={{
                        fontWeight: 'bold',
                        color: '#282A30',
                        marginTop: 10
                    }}
                    text={strings('绑定设备失败')}/>

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#969AA1',
                    }}
                    text={strings('请排查以下问题后重试')}/>

            </View>
        )
    }

    _wifiSuccessed() {
        if (this.state.deviceName == '') {
            this.onShowToast(strings('设备名称不能为空'))
            return
        }
        if (this.state.mac == '') {
            LogUtil.debugLog('wifi设备绑定成功=========>mac null')
            this._toMain(null)
            return
        }

        //获取设备列表，拿到对应设备的设备id
        DeviceListUtil._getDeviceList(true, this.state.token, false)
            .then((res) => {
                var itemData = null;
                var deviceInfo = res.info
                var deviceId = ''
                if (deviceInfo) {
                    deviceInfo.map((item, index) => {
                        if (item.mac == this.state.wifiMac) {
                            deviceId = item.deviceId
                            itemData = item;
                        }
                    })
                    //修改设备名称
                    if (deviceId != '') {
                        NetUtil.updateDeviceName(this.state.deviceName, this.state.token, deviceId, true)
                            .then((res) => {
                                //修改成功
                                LogUtil.debugLog('wifi设备绑定成功=========>修改名称成功')
                                this._toMain(itemData)
                            })
                            .catch((error) => {
                                //修改失败
                                LogUtil.debugLog('wifi设备绑定成功=========>修改名称失败')
                                this._toMain(itemData)
                            })
                    }
                } else {
                    LogUtil.debugLog('wifi设备绑定成功=========>设备列表数据为空')
                    this._toMain(itemData)
                }
            })
            .catch((error) => {
                LogUtil.debugLog('wifi设备绑定成功=========>设备列表获取失败')
                this._toMain(null)
            })
    }

    _toMain(itemData) {
        RouterUtil.toMainDeviceList(this.state.token)

        if (itemData != null) {
            this.props.navigation.navigate('BatteryDetails', {
                itemData: itemData,
                deviceName: itemData.deviceName,
                isWifiConnect: true,
            });
        }
    }
}