// 电源设备设置
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    ScrollView,
    DeviceEventEmitter,
    NativeModules,
    NativeEventEmitter,
    AppState,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import ChooseDialog from "../View/ChooseDialog";
import {SwitchBtnView} from "./BatteryDetails";
import StorageHelper from "../Utils/StorageHelper";
import SpecUtil from "../Utils/SpecUtil";
import {setTimeout} from "react-timer-mixin";

var GXRNManager = NativeModules.GXRNManager;
const {StatusBarManager} = NativeModules;
if (Platform.OS === "ios") {
    StatusBarManager.getHeight((height) => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
const leftIcon = require("../../resources/back_black_ic.png");

let keyData = [
    {key1: "dcValue", key2: "car_charge_i_set"},
    {key1: "acValue", key2: "ac_freq_set"},
    {key1: "quietModel", key2: "low_sound_set"},
    {key1: "lowPowerPromptSetting", key2: "low_power_al_set"},
    {key1: "timingClose", key2: "timeoff_set"},
    {key1: "brightScreenTime", key2: "time_dis_shutdown"},
    {key1: "screenBrightness", key2: "display_bright_set"},
    {key1: "touchTone", key2: "bee_sound_set_key"},
    {key1: "alarm", key2: "bee_sound_set_warning"},
    {key1: "childrenSLock", key2: "switch_lock"},
];
var tempSpecDatas = null;
export default class BatterySetting extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={"#FBFBFB"}
                    titleText={strings("setting")}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        const {deviceName, deviceId, isWifiConnect, isOnline, mac} =
        props.navigation?.state?.params || {};
        this.state = {
            deviceName, //设备名称
            deviceId,
            mac,
            isWifiConnect, //是否连接WIFI
            isOnline, //是否连接网络
            dcValue: "", //DC充电电流设置
            acValue: "", //交流输出频率切换 0：50HZ 1：60Hz
            quietModel: "", //静享模式 0:低噪功能关 1:低噪功能开
            lowPowerPromptSetting: "", //低电量提示设置
            timingClose: "", //定时关闭 0关 1开
            brightScreenTime: "", //亮屏时间
            screenBrightness: "", //屏幕亮度 0：高亮 1：正常 2：低亮
            touchTone: "", //按键音 0：开 1：关
            alarm: "", //报警音 0：关 1：开
            childrenSLock: "", //童锁
            temperatureUnit: "", //温度单位切换
            updateDeviceNameDialog: false, //设备重命名弹窗
            temperatureUnitDiablog: false, //温度切换弹窗
            screenBrightnessDiablog: false, //屏幕亮度弹窗
            brightScreenTimeDiablog: false, //亮屏时间弹窗
            lowPowerPromptSettingDiablog: false, //低电量提示设置弹窗
            quietModelDiablog: false, //静享模式弹窗
            quietModelTipsDiablog: false, //静享模式提示弹窗
            acTipsDiablog: false, //交流电提示弹窗
            timingCloseTipsDiablog: false, //定时关闭提示弹窗
            childrenSLockTipsDiablog: false, //童锁提示弹窗
            dcDiablog: false, //DC充电电流设置
            dcTipsDiablog: false, //DC充电电流设置提示
            acDiablog: false, //DC充电电流设置
            shareDeviceDiablog: false, //设备分享弹窗
            shareDeviceAccount: "", //被分享人账号（设备分享弹窗输入框）
            firmwareUpgradeText: "", //固件升级是否有未升级固件的提示文案

            isFirstUseDC: true,
            isFirstUseAC: true,
            isFirstUseQuiet: true,
            isFirstUseTimerOff: true,
            isFirstUseChildLock: true
        };
        // DC充电电流
        this.dcData = [
            {text: "4A", id: "0"},
            {text: "6A", id: "1"},
            {text: "8A", id: "2"},
        ];

        // 交流输出频率切换
        this.acData = [
            {text: "50HZ", id: "1"},
            {text: "60HZ", id: "2"},
        ];

        // 亮屏时间
        this.brightScreenTimeData = [
            {text: strings("常亮"), id: "5"},
            {text: strings("10秒"), id: "0"},
            {text: strings("30秒"), id: "1"},
            {text: strings("1分钟"), id: "2"},
            {text: strings("5分钟"), id: "3"},
            {text: strings("30分钟"), id: "4"},
        ];

        // 低电量提示
        this.lowPowerPromptSettingData = [
            {text: "5%", id: "0"},
            {text: "10%", id: "1"},
            {text: "20%", id: "2"},
        ];

        // 静享模式
        this.quietModelData = [
            {text: strings("100W"), id: "0"},
            {text: strings("200W"), id: "1"},
            {text: strings("300W"), id: "2"},
            {text: strings("400W"), id: "3"},
            {text: strings("500W"), id: "4"},
            {text: strings("660W"), id: "5"},
        ];

        // 亮度
        this.screenBrightnessData = [
            {text: strings("低亮"), id: "2"},
            {text: strings("正常"), id: "1"},
            {text: strings("高亮"), id: "0"},
        ];

        // 温度
        this.temperatureUnitData = [
            {text: strings("摄氏度"), id: 0},
            {text: strings("华氏度"), id: 1},
        ];

        this.startTime = new Date().getTime(); //开始下发时间
        this.currentTime = new Date().getTime(); //下发成功时间
        this.allowTimer = true; //默认允许轮询
    }

    componentWillMount() {
        const {deviceId} = this.state;
        this._getLocalTempUnit();
        this._getDeviceProps(deviceId, true, false);
        this._eventListenerAdd();

        this._getIsFirstUseAC();
        this._getIsFirstUseDC();
        this._getIsFirstUseQuiet();
        this._getIsFirstUseTimerOff();
        this._getIsFirstUseChildLock();
    }

    // 获取是否首次使用DC
    _getIsFirstUseDC() {
        StorageHelper.getIsFirstUseDC()
            .then((flag) => {
                this.setState({
                    isFirstUseDC: flag,
                });
            })
            .catch((error) => {
            });
    }

    // 获取是否首次使用AC
    _getIsFirstUseAC() {
        StorageHelper.getIsFirstUseAC()
            .then((flag) => {
                this.setState({
                    isFirstUseAC: flag,
                });
            })
            .catch((error) => {
            });
    }

    // 获取是否首次使用静享
    _getIsFirstUseQuiet() {
        StorageHelper.getIsFirstUseQuiet()
            .then((flag) => {
                this.setState({
                    isFirstUseQuiet: flag,
                });
            })
            .catch((error) => {
            });
    }

    // 获取是否首次使用定时关闭
    _getIsFirstUseTimerOff() {
        StorageHelper.getIsFirstUseTimerOff()
            .then((flag) => {
                this.setState({
                    isFirstUseTimerOff: flag,
                });
            })
            .catch((error) => {
            });
    }

    // 获取是否首次使用童锁
    _getIsFirstUseChildLock() {
        StorageHelper.getIsFirstUseChildLock()
            .then((flag) => {
                this.setState({
                    isFirstUseChildLock: flag,
                });
            })
            .catch((error) => {
            });
    }

    componentWillUnmount() {
        this._eventListenerRemove();
        this._stopTimerGetDeviceProps();
    }

    _timerGetDeviceProps = () => {
        const {deviceId} = this.state;
        this._stopTimerGetDeviceProps();
        this.intervalGetDeviceProps = setInterval(() => {
            this.firmwareList();
            if (this.currentTime - this.startTime < 3000 && !this.allowTimer) {
                this.allowTimer = true;
                return;
            }
            this._getDeviceProps(deviceId, false, true);
        }, 4000);
    };

    _stopTimerGetDeviceProps() {
        if (this.intervalGetDeviceProps) {
            clearInterval(this.intervalGetDeviceProps);
            this.intervalGetDeviceProps = null;
        }
    }

    _eventListenerAdd = () => {
        const {deviceId} = this.state;
        // 温度单位变化监听
        this.tempUnitListener = DeviceEventEmitter.addListener(
            EventUtil.SEND_TEMP_UNIT,
            (unit) => {
                this.setState({
                    temperatureUnit: unit,
                });
            }
        );

        // 重新登录
        this.toLoginListener = DeviceEventEmitter.addListener(
            EventUtil.TO_LOGIN,
            (value) => {
                this._eventListenerRemove();
            }
        );

        // 监听设备离线状态变化
        this.deviceOfflineListener = DeviceEventEmitter.addListener(
            EventUtil.DEVICE_OFFLINE,
            (res) => {
                if (res) {
                    var did = res.deviceId;
                    if (did == deviceId) {
                        LogUtil.debugLog(
                            "offline======设备详情=====>设备离线：did" +
                            did +
                            ",deviceId:" +
                            deviceId
                        );
                        this.setState({
                            isOnline: false,
                        });
                    }
                }
            }
        );

        // 监听设备在线状态变化
        this.deviceOnlineListener = DeviceEventEmitter.addListener(
            EventUtil.DEVICE_ONLINE,
            (res) => {
                if (res) {
                    var did = res.deviceId;
                    if (did == deviceId) {
                        LogUtil.debugLog(
                            "online======设备详情=====>设备上线：did" +
                            did +
                            ",deviceId:" +
                            deviceId
                        );
                        this.setState({
                            isOnline: true,
                        });
                        this._getDeviceProps(deviceId, false, true);
                    }
                }
            }
        );

        // 监听应用程序状态的变化
        this.appStateListener = AppState.addEventListener("change", (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === "active") {
                // 后台进入前台
                this._timerGetDeviceProps();
                // if (blueConnectStatus == 1) {
                //     if (Platform.OS === 'android') {
                //         this._androidOpenGps()
                //     } else {
                //         this._ble()
                //     }
                // }
            } else {
                this._stopTimerGetDeviceProps();
            }
        }); // 监听APP状态  前台、后台

        // 定时设置页返回触发
        this.routesListener = DeviceEventEmitter.addListener(
            EventUtil.ROUTES_LENGTH,
            (message) => {
                if (message === 4) {
                    this._getDeviceProps(deviceId, false, true);
                }
            }
        );
    };

    _eventListenerRemove() {
        this.tempUnitListener?.remove();
        this.toLoginListener?.remove();
        this.deviceOfflineListener?.remove();
        this.deviceOnlineListener?.remove();
        this.appStateListener?.remove();
        this.routesListener?.remove();
    }

    // 获取温度单位 "0": 摄氏度, "1": 华氏度
    _getLocalTempUnit() {
        StorageHelper.getTempUnit()
            .then((unit) => {
                this.setState({
                    temperatureUnit: parseInt(unit),
                });
                if (unit == undefined) {
                    //设置默认温度单位
                    StorageHelper.saveTempUnit(0);
                }
            })
            .catch((error) => {
            });
    }

    //获取设备属性
    _getDeviceProps(deviceId, showLoading, hideErrorToast) {
        const {
            acIsSelected,
            dcValue,
            acValue,
            lowPowerPromptSetting,
            timingClose,
            quietModel,
            screenBrightness,
            touchTone,
            alarm,
            brightScreenTime,
            childrenSLock,
            isOnline,
        } = this.state;
        if (Helper.dataIsNull(deviceId)) {
            return;
        }
        NetUtil.getDeviceProps(
            StorageHelper.getTempToken(),
            deviceId,
            showLoading,
            hideErrorToast
        ).then((res) => {
            var specDatas = SpecUtil.resolveSpecs(res.info, false);
            const {
                switch_ac,
                car_charge_i_set,
                ac_freq_set,
                low_power_al_set,
                timeoff_set,
                display_bright_set,
                bee_sound_set_key,
                bee_sound_set_warning,
                low_sound_set,
                time_dis_shutdown,
                switch_lock,
            } = specDatas || {};
            tempSpecDatas = specDatas;
            console.log("specDatas: ", specDatas);
            if (JSON.stringify(specDatas)) {
                // if (JSON.stringify(specDatas) != JSON.stringify(tempSpecDatas)) {

                //ac交流
                if (acIsSelected !== switch_ac) {
                    this.setState({
                        acIsSelected: !Helper.dataIsNull(switch_ac)
                            ? switch_ac
                            : acIsSelected,
                    });
                }

                //DC充电电流
                if (dcValue !== car_charge_i_set) {
                    this.setState({
                        dcValue: !Helper.dataIsNull(car_charge_i_set)
                            ? car_charge_i_set
                            : dcValue,
                    });
                }
                //交流输出频率切换
                if (acValue !== ac_freq_set) {
                    this.setState({
                        acValue: !Helper.dataIsNull(ac_freq_set) ? ac_freq_set : acValue,
                    });
                }

                // 静享模式
                if (quietModel !== low_sound_set) {
                    this.setState({
                        quietModel: !Helper.dataIsNull(low_sound_set)
                            ? low_sound_set
                            : quietModel,
                    });
                }

                //低电量提示
                if (lowPowerPromptSetting !== low_power_al_set) {
                    this.setState({
                        lowPowerPromptSetting: !Helper.dataIsNull(low_power_al_set)
                            ? low_power_al_set
                            : lowPowerPromptSetting,
                    });
                }

                //亮屏时间
                if (brightScreenTime !== time_dis_shutdown) {
                    this.setState({
                        brightScreenTime: !Helper.dataIsNull(time_dis_shutdown)
                            ? time_dis_shutdown
                            : brightScreenTime,
                    });
                }

                // 定时关闭
                if (timingClose !== timeoff_set) {
                    this.setState({
                        timingClose: !Helper.dataIsNull(timeoff_set)
                            ? timeoff_set
                            : timingClose,
                    });
                }

                //屏幕亮度
                if (screenBrightness !== display_bright_set) {
                    this.setState({
                        screenBrightness: !Helper.dataIsNull(display_bright_set)
                            ? display_bright_set
                            : screenBrightness,
                    });
                }

                //按键音
                if (touchTone !== display_bright_set) {
                    this.setState({
                        touchTone: !Helper.dataIsNull(bee_sound_set_key)
                            ? bee_sound_set_key
                            : touchTone,
                    });
                }

                //报警音
                if (alarm !== bee_sound_set_warning) {
                    this.setState({
                        alarm: !Helper.dataIsNull(bee_sound_set_warning)
                            ? bee_sound_set_warning
                            : alarm,
                    });
                }

                // 童锁
                if (childrenSLock !== switch_lock) {
                    this.setState({
                        childrenSLock: !Helper.dataIsNull(switch_lock)
                            ? switch_lock
                            : childrenSLock,
                    });
                }
            }
            //开始轮询
            this._timerGetDeviceProps();
        });
    }

    // 开启或关闭switch
    onValueChange = (value, type) => {
        if (type == 'quietModel') {
            if (this.state.isFirstUseQuiet) {
                this._onShow("quietModelTipsDiablog");
                StorageHelper.saveIsFirstUseQuiet(false);
                this.setState({isFirstUseQuiet: false})
                return
            }
        } else if (type == 'timingClose') {
            if (this.state.isFirstUseTimerOff) {
                this._onShow("timingCloseTipsDiablog");
                StorageHelper.saveIsFirstUseTimerOff(false);
                this.setState({isFirstUseTimerOff: false})
                return
            }
        } else if (type == 'childrenSLock') {
            if (this.state.isFirstUseChildLock) {
                this._onShow("childrenSLockTipsDiablog");
                StorageHelper.saveIsFirstUseChildLock(false);
                this.setState({isFirstUseChildLock: false})
                return
            }
        }

        const {isWifiConnect, deviceId} = this.state;
        //获取当前时间毫秒数
        this.startTime = new Date().getTime();
        if (isWifiConnect) {
            NetUtil.deviceControlV3(
                StorageHelper.getTempToken(),
                deviceId,
                Helper.getKey(type, keyData),
                value,
                false
            )
                .then((res) => {
                    this.currentTime = new Date().getTime();
                    if (this.currentTime - this.startTime < 3000) {
                        this.allowTimer = false;
                    }
                    this.setState({
                        [type]: value,
                    });
                })
                .catch((error) => {
                    this.currentTime = new Date().getTime();
                    if (this.currentTime - this.startTime < 3000) {
                        this.allowTimer = false;
                    }
                    throw error;
                });
        }
    };

    // 获取固件列表
    firmwareList = () => {
        const {deviceId, mac} = this.state;
        let firmwareUpgradeText = "";
        // upgradeStatus	string	升级状态 UPGRADABLE-可升级 UPGRADED-升级完成(已是最新版本) UPGRADING-可升级
        NetUtil.firmwareList(StorageHelper.getTempToken(), deviceId, mac)
            .then((res) => {
                if (res?.list?.length > 0) {
                    res?.list?.map((item, index) => {
                        if (item?.upgradeStatus === "UPGRADABLE") {
                            firmwareUpgradeText = strings("可升级");
                        }
                    });
                    const allUpgraded = res?.list?.every(obj => obj.upgradeStatus === "UPGRADED");
                    if (allUpgraded) {
                        firmwareUpgradeText = strings("已是最新版本");
                    }
                }
                this.setState({
                    firmwareUpgradeText,
                });
            })
            .catch((error) => {
                throw error;
            });
    };

    // 选择弹窗
    _checkTab = (id, type, dialogType) => {
        const {isWifiConnect, deviceId} = this.state;
        //获取当前时间毫秒数
        this.startTime = new Date().getTime();
        if (isWifiConnect) {
            // 切换温度单位
            if (type === "temperatureUnit") {
                // 温度储存本地
                StorageHelper.saveTempUnit(id);
                EventUtil.sendEvent(EventUtil.SEND_TEMP_UNIT, id);
                this.setState({[type]: id, [dialogType]: false});
            } else {
                NetUtil.deviceControlV3(
                    StorageHelper.getTempToken(),
                    deviceId,
                    Helper.getKey(type, keyData),
                    id,
                    false
                )
                    .then((res) => {
                        this.currentTime = new Date().getTime();
                        if (this.currentTime - this.startTime < 3000) {
                            this.allowTimer = false;
                        }
                        this.setState({[type]: id, [dialogType]: false});
                    })
                    .catch((error) => {
                        this.currentTime = new Date().getTime();
                        if (this.currentTime - this.startTime < 3000) {
                            this.allowTimer = false;
                        }
                        throw error;
                    });
            }
        }
    };

    // 隐藏选择弹窗
    _onHide = (type) => {
        this.setState({
            [type]: false,
        });
    };

    // 隐藏弹窗
    _onShow = (type) => {
        this.setState({
            [type]: true,
        });
    };

    // 根据枚举值匹配相应选项文字
    getText = (type, value) => {
        return this[type]?.find((element) => element.id === value)?.text;
    };

    //修改设备名称
    _updateDeviceName(updateName, deviceId, token) {
        if (deviceId == "" || token == "") {
            return;
        }

        NetUtil.updateDeviceName(updateName, token, deviceId, true)
            .then((res) => {
                this.onShowToast(strings("update_success"));
                this.setState({deviceName: updateName});
                EventUtil.sendEvent(EventUtil.UPDATE_DEVICE_NAME_SUCCESS, updateName);
            })
            .catch((error) => {
                this.onShowToast(strings("update_fail"));
            });
    }

    // 分享设备
    _shareDevice = (shareDeviceAccount, deviceId, token) => {
        if (!shareDeviceAccount || !token || !deviceId) {
            return;
        }
        this.setState({shareDevice: false});
        NetUtil.shareDevice(token, deviceId, shareDeviceAccount, true)
            .then((res) => {
                this.setState({shareDeviceDiablog: false});
                this.onShowToast(strings("shareDeviceSuccessfully"));
            })
            .catch((error) => {
                this.setState({shareDeviceDiablog: false});
                // this.onShowToast(strings("shareDeviceFailed"));
            });
    };

    render() {
        const {
            deviceName,
            tempDeviceName,
            mac,
            dcValue,
            acValue,
            quietModel,
            lowPowerPromptSetting,
            timingClose,
            brightScreenTime,
            screenBrightness,
            touchTone,
            alarm,
            childrenSLock,
            temperatureUnit,
            updateDeviceNameDialog,
            screenBrightnessDiablog,
            temperatureUnitDiablog,
            brightScreenTimeDiablog,
            lowPowerPromptSettingDiablog,
            quietModelDiablog,
            quietModelTipsDiablog,
            acDiablog,
            acTipsDiablog,
            timingCloseTipsDiablog,
            childrenSLockTipsDiablog,
            dcDiablog,
            dcTipsDiablog,
            deviceId,
            shareDeviceAccount,
            shareDeviceDiablog,
            isWifiConnect,
            isOnline,
            firmwareUpgradeText,
        } = this.state;
        // 设备名称、设备分享
        const _deviceName = (
            <View
                style={[
                    styles.box,
                    {width: this.mScreenWidth - 40, marginBottom: 10},
                ]}
            >
                <ItemBtnView
                    onPress={() => {
                        this.setState({tempDeviceName:deviceName})
                        this._onShow("updateDeviceNameDialog");
                    }}
                    text={strings("设备名称")}
                    rightText={deviceName}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {
                        // this._onShow("shareDeviceDiablog");
                        this.props.navigation.navigate("DeviceShareMember", {
                            deviceData: {'deviceId': this.state.deviceId},
                        })
                    }}
                    text={strings("设备分享")}
                    rightIcon={rightIcon}
                    containerStyle={{borderBottomWidth: 0}}
                />
            </View>
        );
        // 电流设置
        const _currentSettings = (
            <View
                style={[
                    styles.box,
                    {width: this.mScreenWidth - 40, marginBottom: 10},
                ]}
            >
                <ItemBtnView
                    onPress={() => {
                        if (this.state.isFirstUseDC) {
                            this._onShow("dcTipsDiablog");
                            StorageHelper.saveIsFirstUseDC(false);
                            this.setState({isFirstUseDC: false})
                            return
                        }
                        this._onShow("dcDiablog");
                    }}
                    text={strings("DC充电电流设置")}
                    rightText={this.getText("dcData", dcValue)}
                    leftIcon2={require("../../resources/greenUnion/tips_ic.png")}
                    rightIcon={rightIcon}
                    onLeftIcon2Press={() => {
                        this._onShow("dcTipsDiablog");
                    }}
                />
                <ItemBtnView
                    disabled={Number(this.state.acIsSelected) == 1}
                    onPress={() => {
                        if (this.state.isFirstUseAC) {
                            this._onShow("acTipsDiablog");
                            StorageHelper.saveIsFirstUseAC(false);
                            this.setState({isFirstUseAC: false})
                            return
                        }
                        this._onShow("acDiablog");
                    }}
                    text={strings("交流输出频率切换")}
                    rightText={this.getText("acData", acValue)}
                    leftIcon2={require("../../resources/greenUnion/tips_ic.png")}
                    rightIcon={rightIcon}
                    onLeftIcon2Press={() => {
                        this._onShow("acTipsDiablog");
                    }}
                />
                {/* <ItemBtnView
          onPress={() => {
            this._onShow("quietModelDiablog");
          }}
          text={strings("静享模式")}
          rightText={this.getText("quietModelData", quietModel)}
          leftIcon2={require("../../resources/greenUnion/tips_ic.png")}
          rightIcon={rightIcon}
          onLeftIcon2Press={() => {
            this._onShow("quietModelTipsDiablog");
          }}
        /> */}
                <SwitchBtnView
                    title={strings("静享模式")}
                    onValueChange={this.onValueChange}
                    type={"quietModel"}
                    dialogType={"quietModelDiablog"}
                    value={Number(quietModel)}
                    tipsOnClick={() => this._onShow("quietModelTipsDiablog")}
                    style={{borderBottomWidth: 0}}
                />
            </View>
        );
        // 电池健康
        const _batteryHealth = (
            <View
                style={[
                    styles.box,
                    {width: this.mScreenWidth - 40, marginBottom: 10},
                ]}
            >
                {/* <ItemBtnView
          onPress={() => {
            this.props.navigation.navigate("BatteryHealth", { deviceId });
          }}
          text={strings("电池健康")}
          rightIcon={rightIcon}
        /> */}
                <ItemBtnView
                    onPress={() => {
                        this._onShow("lowPowerPromptSettingDiablog");
                    }}
                    text={strings("低电量提示设置")}
                    rightText={this.getText(
                        "lowPowerPromptSettingData",
                        lowPowerPromptSetting
                    )}
                    rightIcon={rightIcon}
                    onLeftIcon2Press={() => {
                        this._onShow("");
                    }}
                    containerStyle={{borderBottomWidth: 0}}
                />
            </View>
        );
        // 开关控制
        const _switchControl = (
            <View
                style={[
                    styles.box,
                    {width: this.mScreenWidth - 40, marginBottom: 10},
                ]}
            >
                <SwitchBtnView
                    title={strings("定时关闭")}
                    onValueChange={this.onValueChange}
                    type={"timingClose"}
                    dialogType={"timingCloseDialog"}
                    value={Number(timingClose)}
                    tipsOnClick={() => this._onShow("timingCloseTipsDiablog")}
                />
                <ItemBtnView
                    onPress={() => {
                        // 定时设置
                        this.props.navigation.navigate("RegularSettings", {
                            deviceId,
                            isWifiConnect,
                            isOnline,
                        });
                    }}
                    text={strings("定时设置")}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => this._onShow("brightScreenTimeDiablog")}
                    text={strings("亮屏时间")}
                    rightText={this.getText("brightScreenTimeData", brightScreenTime)}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => this._onShow("screenBrightnessDiablog")}
                    text={strings("屏幕亮度")}
                    rightText={this.getText("screenBrightnessData", screenBrightness)}
                    rightIcon={rightIcon}
                />
                <SwitchBtnView
                    title={strings("按键音")}
                    onValueChange={this.onValueChange}
                    type={"touchTone"}
                    value={Number(touchTone)}
                    contraryMode={true} //相反类型按键 0：开 1：关
                />
                <SwitchBtnView
                    title={strings("报警音")}
                    onValueChange={this.onValueChange}
                    type={"alarm"}
                    dialogType={"alarmDialog"}
                    value={Number(alarm)}
                />
                <SwitchBtnView
                    title={strings("童锁")}
                    onValueChange={this.onValueChange}
                    type={"childrenSLock"}
                    dialogType={"childrenSLockDialog"}
                    value={Number(childrenSLock)}
                    tipsOnClick={() => this._onShow("childrenSLockTipsDiablog")}
                />
                <ItemBtnView
                    onPress={() => this._onShow("temperatureUnitDiablog")}
                    text={strings("温度单位切换")}
                    rightText={this.getText("temperatureUnitData", temperatureUnit)}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {
                        this.props.navigation.navigate("IntelligentSelfExamination", {
                            deviceId,
                        });
                    }}
                    text={strings("智能自检")}
                    rightIcon={rightIcon}
                    containerStyle={{borderBottomWidth: 0}}
                />
            </View>
        );
        // 软件升级
        const applicationRelated = (
            <View
                style={[
                    styles.box,
                    {width: this.mScreenWidth - 40, marginBottom: 10},
                ]}
            >
                <ItemBtnView
                    onPress={() => {
                        this.props.navigation.navigate("AboutDevice", {
                            itemData: this.props.navigation.state?.params?.deviceData,
                        });
                    }}
                    text={strings("关于本机")}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {
                        this.props.navigation.navigate(
                            "NewUseHelpDeviceDetails",
                            {title: 'GS600', productId: '840126776388972500'}
                        );
                        // this.props.navigation.navigate('NewUseHelp');
                    }}
                    text={strings("使用帮助")}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {
                        this.props.navigation.navigate("DeviceUpgrade", {deviceId, mac, isOnline, isWifiConnect});
                    }}
                    text={strings("固件升级")}
                    rightText={firmwareUpgradeText}
                    rightIcon={rightIcon}
                    containerStyle={{borderBottomWidth: 0}}
                />
            </View>
        );
        // 弹窗
        const _dialogView = (
            <View>
                {/* 重命名 */}
                <CommonMessageDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    children={
                        <View
                            style={{
                                flexDirection: "row",
                                height: 45,
                                width: "100%",
                                alignItems: "center",
                                justifyContent: "center",
                                marginVertical: 30,
                            }}
                        >
                            <TextInput
                                style={[
                                    {
                                        height: 55,
                                        textAlign: "left",
                                        paddingHorizontal: 15,
                                        fontSize: this.getSize(14),
                                        color: "#404040",
                                        backgroundColor: "#EBEEF0",
                                        width: "100%",
                                        borderRadius: 40,
                                    },
                                ]}
                                maxLength={30}
                                onChangeText={(text) => {
                                    this.setState({
                                        tempDeviceName: text,
                                    });
                                }}
                                defaultValue={tempDeviceName}
                                // placeholderTextColor={}
                                // placeholder={}
                            />
                        </View>
                    }
                    onRequestClose={() => {
                        this.setState({updateDeviceNameDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({updateDeviceNameDialog: false});
                    }}
                    modalVisible={updateDeviceNameDialog}
                    title={strings("naming")}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({updateDeviceNameDialog: false});
                    }}
                    rightBtnText={strings("comfire")}
                    onRightBtnClick={() => {
                        //确定
                        if (this.state.tempDeviceName.length < 2) {
                            return
                        }
                        this.setState({updateDeviceNameDialog: false});

                        this.setState({
                            deviceName: this.state.tempDeviceName.replace(/(^\s*)|(\s*$)/g, ""),
                        });
                        this._updateDeviceName(
                            this.state.tempDeviceName.replace(/(^\s*)|(\s*$)/g, ""),
                            this.state.deviceId,
                            StorageHelper.getTempToken()
                        );
                        //this._updateUserName(deviceName.replace(/(^\s*)|(\s*$)/g, ""), this.state.token)
                    }}
                />
                {/*设备分享 */}
                <CommonMessageDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    children={
                        <View
                            style={{
                                flexDirection: "row",
                                height: 45,
                                width: "100%",
                                alignItems: "center",
                                justifyContent: "center",
                                marginVertical: 30,
                            }}
                        >
                            <TextInput
                                style={[
                                    {
                                        height: 45,
                                        textAlign: "left",
                                        paddingHorizontal: 15,
                                        fontSize: this.getSize(14),
                                        color: "#404040",
                                        backgroundColor: "#EBEEF0",
                                        width: "100%",
                                        borderRadius: 40,
                                    },
                                ]}
                                maxLength={20}
                                onChangeText={(text) => {
                                    this.setState({
                                        shareDeviceAccount: text,
                                    });
                                }}
                                defaultValue={shareDeviceAccount}
                                // placeholderTextColor={}
                                placeholder={strings('请输入接收者账号')}
                            />
                        </View>
                    }
                    onRequestClose={() => {
                        this.setState({shareDeviceDiablog: false});
                    }}
                    overViewClick={() => {
                        this.setState({shareDeviceDiablog: false});
                    }}
                    modalVisible={shareDeviceDiablog}
                    title={strings("sharing_equipment")}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({shareDeviceDiablog: false});
                    }}
                    rightBtnText={strings("comfire")}
                    onRightBtnClick={() => {
                        //确定
                        this._shareDevice(
                            shareDeviceAccount.replace(/(^\s*)|(\s*$)/g, ""),
                            deviceId,
                            StorageHelper.getTempToken()
                        );
                    }}
                />

                {/* dc充电电流设置 */}
                <ChooseDialog
                    title={strings("DC充电电流")}
                    show={dcDiablog}
                    data={this.dcData}
                    currentId={dcValue}
                    _checkTab={(data) => {
                        this._checkTab(data.id, "dcValue", "dcDiablog");
                    }}
                    _onHide={() => this._onHide("dcDiablog")}
                />

                {/* dc充电电流设置提示弹窗 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("dcTipsDiablog");
                    }}
                    overViewClick={() => {
                        this._onHide("dcTipsDiablog");
                    }}
                    modalVisible={dcTipsDiablog}
                    title={strings("DC充电电流设置")}
                    message={""}
                    rightBtnText={strings("我知道了")}
                    onRightBtnClick={() => {
                        //确定
                        this._onHide("dcTipsDiablog");
                    }}
                />

                {/* 温度单位切换 */}
                <ChooseDialog
                    title={strings("温度单位切换")}
                    show={temperatureUnitDiablog}
                    data={this.temperatureUnitData}
                    currentId={temperatureUnit}
                    _checkTab={(data) => {
                        this._checkTab(
                            data.id,
                            "temperatureUnit",
                            "temperatureUnitDiablog"
                        );
                    }}
                    _onHide={() => this._onHide("temperatureUnitDiablog")}
                />
                {/* 屏幕亮度 */}
                <ChooseDialog
                    title={strings("屏幕亮度")}
                    show={screenBrightnessDiablog}
                    data={this.screenBrightnessData}
                    currentId={screenBrightness}
                    _checkTab={(data) => {
                        this._checkTab(
                            data.id,
                            "screenBrightness",
                            "screenBrightnessDiablog"
                        );
                    }}
                    _onHide={() => this._onHide("screenBrightnessDiablog")}
                />
                {/* 亮屏时间 */}
                <ChooseDialog
                    title={strings("亮屏时间")}
                    show={brightScreenTimeDiablog}
                    data={this.brightScreenTimeData}
                    currentId={brightScreenTime}
                    _checkTab={(data) => {
                        this._checkTab(
                            data.id,
                            "brightScreenTime",
                            "brightScreenTimeDiablog"
                        );
                    }}
                    _onHide={() => this._onHide("brightScreenTimeDiablog")}
                />
                {/* 低电量设置 */}
                <ChooseDialog
                    title={strings("低电量提示设置")}
                    show={lowPowerPromptSettingDiablog}
                    data={this.lowPowerPromptSettingData}
                    currentId={lowPowerPromptSetting}
                    _checkTab={(data) => {
                        this._checkTab(
                            data.id,
                            "lowPowerPromptSetting",
                            "lowPowerPromptSettingDiablog"
                        );
                    }}
                    _onHide={() => this._onHide("lowPowerPromptSettingDiablog")}
                />
                {/* 静享模式 */}
                <ChooseDialog
                    title={strings("静享模式")}
                    show={quietModelDiablog}
                    data={this.quietModelData}
                    currentId={quietModel}
                    _checkTab={(data) => {
                        this._checkTab(data.id, "quietModel", "quietModelDiablog");
                    }}
                    _onHide={() => this._onHide("quietModelDiablog")}
                />

                {/* ac交流电弹窗 */}
                <ChooseDialog
                    title={strings("交流输出频率切换")}
                    show={acDiablog}
                    data={this.acData}
                    currentId={acValue}
                    _checkTab={(data) => {
                        this._checkTab(data.id, "acValue", "acDiablog");
                    }}
                    _onHide={() => this._onHide("acDiablog")}
                />

                {/* ac交流电提示弹窗 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("acTipsDiablog");
                    }}
                    overViewClick={() => {
                        this._onHide("acTipsDiablog");
                    }}
                    modalVisible={acTipsDiablog}
                    title={strings("交流输出频率切换")}
                    message={strings("交流电提示")}
                    rightBtnText={strings("我知道了")}
                    onRightBtnClick={() => {
                        //确定
                        this._onHide("acTipsDiablog");
                    }}
                />
                {/* 静享模式提示弹窗 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("quietModelTipsDiablog");
                    }}
                    overViewClick={() => {
                        this._onHide("quietModelTipsDiablog");
                    }}
                    modalVisible={quietModelTipsDiablog}
                    title={strings("静享模式")}
                    message={strings("静享模式提示")}
                    rightBtnText={strings("我知道了")}
                    onRightBtnClick={() => {
                        //确定
                        this._onHide("quietModelTipsDiablog");
                    }}
                />
                {/* 定时关闭提示弹窗 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("timingCloseTipsDiablog");
                    }}
                    overViewClick={() => {
                        this._onHide("timingCloseTipsDiablog");
                    }}
                    modalVisible={timingCloseTipsDiablog}
                    title={strings("定时关闭")}
                    message={strings("定时关闭提示")}
                    rightBtnText={strings("我知道了")}
                    onRightBtnClick={() => {
                        //确定
                        this._onHide("timingCloseTipsDiablog");
                    }}
                />
                {/* 童锁提示弹窗 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("childrenSLockTipsDiablog");
                    }}
                    overViewClick={() => {
                        this._onHide("childrenSLockTipsDiablog");
                    }}
                    modalVisible={childrenSLockTipsDiablog}
                    title={strings("童锁")}
                    message={strings("童锁提示")}
                    rightBtnText={strings("我知道了")}
                    onRightBtnClick={() => {
                        //确定
                        this._onHide("childrenSLockTipsDiablog");
                    }}
                />
            </View>
        );

        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F7F7F7",
                    alignItems: "center",
                    height: this.mScreenHeight,
                    width: this.mScreenWidth,
                }}
            >
                <ScrollView style={{flex: 1}}>
                    <View style={{width: this.mScreenWidth, alignItems: "center"}}>
                        <View
                            style={{
                                width: this.mScreenWidth,
                                paddingHorizontal: 20,
                                alignItems: "center",
                            }}
                        >
                            {_deviceName}
                            {_currentSettings}
                            {_batteryHealth}
                            {_switchControl}
                            {applicationRelated}
                        </View>
                    </View>
                </ScrollView>
                {_dialogView}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    box: {backgroundColor: "#fff", borderRadius: 15, overflow: "hidden"},
});
