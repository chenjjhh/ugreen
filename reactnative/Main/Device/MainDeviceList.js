import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    AppState,
    BackHandler,
    DeviceEventEmitter,
    Image,
    NativeEventEmitter,
    NativeModules,
    Platform,
    ScrollView,
    StatusBar,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import {strings} from '../Language/I18n';
import ListDeviceCard from "../View/ListDeviceCard";
import ListEarbudsCard from "../View/ListEarbudsCard";
import AcceptDeviceShareDialog from "../View/AcceptDeviceShareDialog";
import CommonTextView from "../View/CommonTextView";
import StorageHelper from "../Utils/StorageHelper";
import NetUtil from "../Net/NetUtil";
import ScrollViewPull from "../View/ScrollViewPull";
import DeviceListUtil from "../Utils/DeviceListUtil";
import EventUtil from "../Event/EventUtil";
import SpecUtil from "../Utils/SpecUtil";
import {NetworkInfo} from "react-native-network-info";
import SplashScreen from "react-native-splash-screen";
import {Amplify} from 'aws-amplify';
import PushNotification from '@aws-amplify/pushnotification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import BannerView from "../View/BannerView";
import NetConstants from "../Net/NetConstants";
import Helper from "../Utils/Helper";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import CommonMessageDialog from "../View/CommonMessageDialog";
import CommonBtnView from '../View/CommonBtnView'
import ChooseListDialog from '../View/ChooseListDialog'
import {
    CRC
} from '../Utils/CRC'

/*
*主页、设备列表页
 */

//ui上banner的宽高
const bannerUIWidth = 336
const bannerUIHeight = 150

let that

var firstClick = 0;
var GXRNManager = NativeModules.GXRNManager
var pushFlag = true
var bleDevices = []
let userToken = ''

const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;
export default class MainDeviceList extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
            /*<TitleBar
                style={{
                    marginLeft: 23,
                    marginRight: 19,
                }}
                backgroundColor={'#FFFFFF'}
                showLogo={true}
                leftIcon={require('../../resources/ic_display_small.png')}
                leftIconClick={() => {
                    navigation.navigate('Mine', {
                        userName: that.state.userName,
                        avatarUrl: that.state.avatarUrl,
                        bgUrl: that.state.bgUrl
                    });
                }}
                rightIcon={require('../../resources/add_ic.png')}
                rightIconClick={() => {
                    that._toAddDevicePager()
                }}
                leftIconUrl={navigation.state.params.leftIconUrl ? navigation.state.params.leftIconUrl : ''}
            />*/
                null
        };
    }

    //设置顶部头像
    _setTopAvatar(url) {
        if (!Helper.dataIsNull(url)) {
            this.props.navigation.setParams({leftIconUrl: url});
        }
    }

    _setThirdAvatar() {
        StorageHelper.getThirdAvatar()
            .then((res) => {
                if (res) {
                    this._setTopAvatar(res)
                    this.setState({avatarUrl: res})
                }

            })
    }

    constructor(props) {
        super(props);
        that = this
        this.state = {
            bannerData: [],
            deviceList: [],
            earbudsList: [],//蓝牙耳机
            shareDialogVis: false,
            token: '',
            // getUserFlag: this.props.navigation?.state?.params?.getUserFlag || false,
            deviceInfoRefreshing: false,
            scrollContainerViewHeight: 0,
            userName: '',
            routesLength: 1,
            deviceName: '',
            account: '',
            avatarUrl: '',
            bgUrl: '',
            getShareInfoFlag: false,
            tempUnit: 1,
            openNotifitionTime: 0,
            getFlag: false,
            deleteDialog: false,//删除设备弹窗
            newDeviceDialog: false,//发现新设备弹窗
            currentId: '',//保存选择列表项
            show: false,//保存列表选择弹窗显示或隐藏状态
            resetDeviceName: false,//重命名弹窗状态
            shareDevice: false,//设备分享弹窗
            shareDeviceAccount: '',//被分享人账号（设备分享弹窗输入框）
            itemData: {},//记录长按选中列表当前设备
            isHasNotRead: false,//是否未读消息
            FindNewDevice: '',//发现新设备设备名称
            FindNewDeviceId: '',//发现新设备设备Id
            accpetShareDeviceData: {},//接受分享设备数据


        }
        this.tabData = [
            {text: strings('naming'), id: 0},
            {text: strings('sharing_equipment'), id: 1},
            {text: strings('delete_device'), id: 2},
        ]
        this.handleBack = this.handleBack.bind(this);
    }


    componentWillMount() {
        this.setState({
            token: StorageHelper.getTempToken()
        }, () => {
            userToken = StorageHelper.getTempToken()
            LogUtil.AndroidLogWrite('==============main_componentWillMount=========>state.token:' + this.state.token)
            LogUtil.AndroidLogWrite('==============main_componentWillMount=========>userToken:' + userToken)
        })
        // if (this.state.getUserFlag) {
        this._getUserInfo(false)
        // }

        pushFlag = true
        //倒计时
        setTimeout(() => {
            SplashScreen.hide()
        }, 2000);

        this._getLocalUserInfo()
        this._getLocalTempUnit()

        //接口没有返回aws参数 先不进行aws登录
        //this._awsLogin()


        //this._getLocalDeviceList()
        this._mainGetData(true)
        this._eventListenerAdd()
        this._blueTest()
        this.getMsgCount()

        //this._initPush()

        setTimeout(() => {
            NetUtil._setCanToLoginFlag(true)
        }, 5000);


        this.EarbudsListListener = DeviceEventEmitter.addListener(EventUtil.EARBUDS_LIST_UPDATE, (message) => {
            //耳机列表刷新
            LogUtil.debugLog('蓝牙耳机列表发生改变======》' + JSON.stringify(message))
            if (message) {
                this.setState({earbudsList: JSON.parse(message.ugDevices)});
            }
        });

        //judy 查询蓝牙耳机列表
        if (Platform.OS === 'android') {
            NativeModules.RNModule.queryEarbudsList()
        }
    }

    // 获取消息数量
    getMsgCount = () => {
        NetUtil.getMsgCount(StorageHelper.getTempToken())
            .then((res) => {
                if (res?.list) {
                    let isHasNotRead;
                    res?.list.forEach(element => {
                        if (element?.unReadCnt) {
                            isHasNotRead = true;
                        }
                    });
                    this.setState({
                        isHasNotRead
                    })
                }
            }).catch((error) => {
            console.error(error);
        })

    }

    _blueTest() {
        var tempArray = [
            {
                specId: 2,
                specValue: 1
            }, {
                specId: 3,
                specValue: 2
            }, {
                specId: 4,
                specValue: 3
            }
        ]
        //alert(Bluetooth._singleSpec2Hex(2, -20))
        //alert(JSON.stringify(Bluetooth._hex2Json('00020001801400a800ff')))
    }

    componentWillUnmount() {
        this._eventListenerRemove()
    }

    _eventListenerAdd() {
        // 监听消息已读
        this.toReadListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_ALL_READ, (value) => {
            this.getMsgCount()
        });
        this.toLoginListener = DeviceEventEmitter.addListener(EventUtil.TO_LOGIN, (value) => {
            this._stopTimerGetDeviceList()
        });
        this.refreshListListener = DeviceEventEmitter.addListener(EventUtil.REFRESH_DEVICE_LIST_KEY, (value) => {
            //收到通知，刷新设备列表
            this._getDeviceList(false, true)
        });
        this.updateDeviceNameListener = DeviceEventEmitter.addListener(EventUtil.UPDATE_DEVICE_NAME_SUCCESS, (value) => {
            //修改设备名称成功，收到通知，刷新设备列表
            this._mainGetData(false)
        });
        this.updateUserinfoDeviceNameListener = DeviceEventEmitter.addListener(EventUtil.UPDATE_USER_INFO, (value) => {
            //收到通知，获取用户信息
            this._getUserInfo(false)
        })

        this.routesListener = DeviceEventEmitter.addListener(EventUtil.ROUTES_LENGTH, (message) => {
            // 监听路由长度
            if (parseInt(message) == 1) {
                this._timerGetDeviceList()

                if (parseInt(this.state.routesLength) > parseInt(message)) {
                    this._getDeviceList(false, true)
                }
            } else {
                this._stopTimerGetDeviceList()
            }

            this.setState({
                routesLength: message,
            });
        });
        this.tempUnitListener = DeviceEventEmitter.addListener(EventUtil.SEND_TEMP_UNIT, (unit) => {
            //温度单位
            var unitValue = parseInt(unit) == 0 ? 2 : 1
            this.setState({
                tempUnit: unitValue,
            })
        });

        if (Platform.OS === 'android')
            BackHandler.addEventListener('hardwareBackPress', this.handleBack)

        this.awsMessageListListener = DeviceEventEmitter.addListener(EventUtil.AWS_MSG, (message) => {
            //aws 消息回调
            LogUtil.debugLog('aws消息===Main===》' + JSON.stringify(message))
        });

        if (Platform.OS === 'ios') {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            eventEmitter.addListener(EventUtil.AWS_NOTIFICATION_IOS_TOKEN, (token) => {
                //aws ios推送token
                LogUtil.debugLog('ios推送token===Main===》' + JSON.stringify(token))
                if (token) {
                    this._setPushTokenToCloud(token)
                }
            });

            eventEmitter.addListener(EventUtil.AWS_MSG, (message) => {
                //aws 消息回调
                LogUtil.debugLog('aws消息===Main===》' + JSON.stringify(message))
            });
            eventEmitter.addListener(EventUtil.AWS_LOGIN_STATUS_KEY, (value) => {
                //aws 登录回调，拿到connected 则是连接成功
                LogUtil.debugLog('aws 登录回调，拿到connected 则是连接成功=====>' + JSON.stringify(value))
            });
            eventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                this._getDeviceList(false)
            });
        } else {
            this.awsLoginListListener = DeviceEventEmitter.addListener(EventUtil.AWS_LOGIN_STATUS_KEY, (value) => {
                //aws 登录回调，拿到connected 则是连接成功
                LogUtil.debugLog('aws 登录回调，拿到connected 则是连接成功=====>' + JSON.stringify(value))
            });
            this.awsDeviceStatusListListener = DeviceEventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                this._getDeviceList(false)
            });
        }
        /*
                this.awsIosNotificationTokenListener = DeviceEventEmitter.addListener(EventUtil.AWS_NOTIFICATION_IOS_TOKEN, (token) => {
                    //aws ios推送token
                    LogUtil.debugLog('ios推送token===Main===》' + JSON.stringify(token))
                    if(token){
                        this._setPushTokenToCloud(token)
                    }
                });*/

        this.appStateListener = AppState.addEventListener('change', (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === 'active') { // 后台进入前台
                this._timerGetDeviceList()
            } else {
                this._stopTimerGetDeviceList()
            }
        });// 监听APP状态  前台、后台
    }

    _timerGetDeviceList() {
        this._stopTimerGetDeviceList()
        this.intervalGetDeviceList = setInterval(() => {
            this._getDeviceList(false, true)
        }, 5000)
    }

    _stopTimerGetDeviceList() {
        if (this.intervalGetDeviceList) {
            clearInterval(this.intervalGetDeviceList)
            this.intervalGetDeviceList = null
        }
    }

    _eventListenerRemove() {
        this.EarbudsListListener && this.EarbudsListListener.remove()

        this.toReadListener?.remove()
        this.toLoginListener && this.toLoginListener.remove()
        this.refreshListListener && this.refreshListListener.remove()
        this.awsLoginListListener && this.awsLoginListListener.remove()
        this.awsDeviceStatusListListener && this.awsDeviceStatusListListener.remove()
        this.updateDeviceNameListener && this.updateDeviceNameListener.remove()
        this.updateUserinfoDeviceNameListener && this.updateUserinfoDeviceNameListener.remove()
        this.routesListener && this.routesListener.remove()
        this.tempUnitListener && this.tempUnitListener.remove()
        this.awsMessageListListener && this.awsMessageListListener.remove()
        //this.awsIosNotificationTokenListener && this.awsIosNotificationTokenListener.remove()

        if (Platform.OS === 'android')
            BackHandler.removeEventListener('hardwareBackPress', this.handleBack)
        this._stopTimerGetDeviceList()
        this.appStateListener && this.appStateListener.remove()
    }

    handleBack = () => {
        var timestamp = (new Date()).valueOf();
        if (this.state.routesLength == 1) {
            if (timestamp - firstClick > 2000) {
                firstClick = timestamp;
                this.onShowToast(strings('press_out'))
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    _awsLogin() {
        //aws SDK登录
        StorageHelper.getLoginInfo()
            .then((res) => {
                if (res) {
                    if (Platform.OS === 'android') {
                        //aws登录  监听Granwin_AWS_status  如果拿到connected 则是连接成功
                        NativeModules.RNModule.loginAWS(res.info.pool.identityId,
                            res.info.endpoint, res.info.pool.token, res.info.pool.identifier, res.info.pool.identityPoolId,
                            res.info.region);
                    } else {
                        GXRNManager.setupAwsIotClient(res.info.pool.identityId, res.info.endpoint,
                            res.info.pool.token, res.info.pool.identityId, res.info.pool.identityPoolId,
                            res.info.region);
                    }
                }
            })
    }

    _mainGetData(showLoading) {
        this._getUserInfo(showLoading, true)
    }

    _goToMine = () => {
        this.props.navigation.navigate('NewMine');
    }

    //修改设备名称
    _updateDeviceName(updateName, deviceId, token) {
        if (deviceId == '' || token == '' || updateName.toString().trim() == this.state.itemData?.deviceName.toString().trim()) {
            return
        }
        this.setState({resetDeviceName: false})
        NetUtil.updateDeviceName(updateName, token, deviceId, true)
            .then((res) => {
                this.onShowToast(strings('update_success'))
                EventUtil.sendEvent(EventUtil.UPDATE_DEVICE_NAME_SUCCESS, updateName)
            })
            .catch((error) => {
                this.onShowToast(strings('update_fail'))
            })

    }

    // 分享设备
    _shareDevice = (shareDeviceAccount, deviceId, token) => {
        if (!shareDeviceAccount || !token || !deviceId) {
            return
        }
        this.setState({shareDevice: false})
        NetUtil.shareDevice(token, deviceId, shareDeviceAccount, true)
            .then((res) => {
                this.onShowToast(strings('shareDeviceSuccessfully'))
            })
            .catch((error) => {
                // this.onShowToast(strings('shareDeviceFailed'))
            })
    }


    render() {
        // 顶部bar标题
        const TitleView = (
            <View style={{
                flexDirection: 'row',
                height: 55,
                marginTop: statusBarHeight,
                alignItems: 'center',
                paddingHorizontal: 12
            }}>
                <TouchableOpacity
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 10,
                        overflow: 'hidden',
                        marginRight: 11
                    }}
                    onPress={() => {
                        // this._goToMine()
                    }}>
                    <Image
                        style={{
                            position: 'absolute',
                            width: 30,
                            height: 30,
                        }}
                        source={require('../../resources/ic_display_small.png')}/>
                    <FastImage
                        style={{width: this.mScreenWidth * 0.07, height: this.mScreenWidth * 0.07}}
                        source={{
                            uri: this.props.navigation?.state?.params?.leftIconUrl || '',
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                </TouchableOpacity>
                <View style={{flex: 1}}>
                    <Text
                        numberOfLines={1}
                        style={{
                            color: '#2F2F2F',
                            fontSize: this.getSize(17),
                            fontWeight: 'bold'
                        }}>
                        {this.state.userName}
                    </Text>
                </View>
                <TouchableOpacity
                    style={{marginRight: 22}}
                    onPress={() => {
                        this.props.navigation.navigate('MessegeCenter')
                    }}>
                    <Image
                        source={this.state.isHasNotRead ? require('../../resources/greenUnion/top_msg_new_ic.png') : require('../../resources/greenUnion/top_msg_ic.png')}/>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        that._toAddDevicePager()
                    }}>
                    <Image style={{height: 26, width: 26}}
                           source={require('../../resources/greenUnion/top_add_ic.png')}/>
                </TouchableOpacity>
            </View>
        )
        // 列表无数据
        const _noDeviceView = (<View
            style={{
                alignItems: 'center',
                width: this.mScreenWidth,
                height: this.state.scrollContainerViewHeight,
            }}>
            <View style={{marginTop: 150}}>
                <Image
                    style={{
                        resizeMode: 'contain'
                    }}
                    source={require('../../resources/greenUnion/home_nodev_ic.png')}/>
            </View>
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //添加设备
                    this._toAddDevicePager()
                }}
                style={{
                    height: 54,
                    backgroundColor: '#18B34F',
                    marginTop: 60
                }}
                btnText={strings('add_device')}/>
        </View>)
        // 删除弹窗
        const DeleteDeviceView = (
            <CommonMessageDialog
                leftBtnTextColor={'#808080'}
                onLeftBackgroundColor={'#F1F2F6'}
                rightBtnTextColor={'#ffffff'}
                onRightBackgroundColor={'#18B34F'}
                onRequestClose={() => {
                    this.setState({deleteDialog: false})
                }}
                overViewClick={() => {
                    this.setState({deleteDialog: false})
                }}
                modalVisible={this.state.deleteDialog}
                title={strings('delete_device')}
                message={strings('delete_prompt')}
                leftBtnText={strings('cancel')}
                onLeftBtnClick={() => {
                    //取消
                    this.setState({deleteDialog: false})
                }}
                rightBtnText={strings('confirm')}
                onRightBtnClick={() => {
                    //确认
                    this.deleteDevice()
                }}/>)
        // 重名名弹窗
        const resetDeviceName = (
            <CommonMessageDialog
                leftBtnTextColor={'#808080'}
                onLeftBackgroundColor={'#F1F2F6'}
                rightBtnTextColor={'#ffffff'}
                onRightBackgroundColor={'#18B34F'}
                children={(<View
                    style={{
                        flexDirection: 'row',
                        height: 45,
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginVertical: 30
                    }}>
                    <TextInput
                        style={[
                            {
                                height: 45,
                                textAlign: 'left',
                                paddingHorizontal: 15,
                                fontSize: this.getSize(14),
                                color: '#404040',
                                backgroundColor: '#EBEEF0',
                                width: '100%',
                                borderRadius: 40,
                            }
                        ]}
                        maxLength={20}
                        onChangeText={(text) => {
                            this.setState({
                                deviceName: text
                            })
                        }}
                        defaultValue={this.state.deviceName}
                        // placeholderTextColor={}
                        // placeholder={}
                    />
                </View>)}
                onRequestClose={() => {
                    this.setState({resetDeviceName: false})
                }}
                overViewClick={() => {
                    this.setState({resetDeviceName: false})
                }}
                modalVisible={this.state.resetDeviceName}
                title={strings('naming')}
                leftBtnText={strings('cancel')}
                onLeftBtnClick={() => {
                    //取消
                    this.setState({resetDeviceName: false})
                }}
                rightBtnText={strings('comfire')}
                onRightBtnClick={() => {
                    //确定
                    this._updateDeviceName(this.state.deviceName.replace(/(^\s*)|(\s*$)/g, ""), this.state.itemData?.deviceId, this.state.token)
                }}/>)
        // 分享设备
        const shareDeviceView = (
            <CommonMessageDialog
                leftBtnTextColor={'#808080'}
                onLeftBackgroundColor={'#F1F2F6'}
                rightBtnTextColor={'#ffffff'}
                onRightBackgroundColor={'#18B34F'}
                children={(<View
                    style={{
                        flexDirection: 'row',
                        height: 45,
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginVertical: 30
                    }}>
                    <TextInput
                        style={[
                            {
                                height: 45,
                                textAlign: 'left',
                                paddingHorizontal: 15,
                                fontSize: this.getSize(14),
                                color: '#404040',
                                backgroundColor: '#EBEEF0',
                                width: '100%',
                                borderRadius: 40,
                            }
                        ]}
                        maxLength={20}
                        onChangeText={(text) => {
                            this.setState({
                                shareDeviceAccount: text
                            })
                        }}
                        defaultValue={this.state.shareDeviceAccount}
                        // placeholderTextColor={}
                        placeholder={strings('请输入接收者账号')}
                    />
                </View>)}
                onRequestClose={() => {
                    this.setState({shareDevice: false})
                }}
                overViewClick={() => {
                    this.setState({shareDevice: false})
                }}
                modalVisible={this.state.shareDevice}
                title={strings('sharing_equipment')}
                leftBtnText={strings('cancel')}
                onLeftBtnClick={() => {
                    //取消
                    this.setState({shareDevice: false})
                }}
                rightBtnText={strings('comfire')}
                onRightBtnClick={() => {
                    //确定
                    this._shareDevice(this.state.shareDeviceAccount.replace(/(^\s*)|(\s*$)/g, ""), this.state.itemData?.deviceId, this.state.token)
                }}/>)

        // 列表item项
        const _deviceListView = ((
            <ScrollView
                style={{
                    marginTop: this.state.bannerData.length > 1 ? 27 : 10
                }}>
                {this.state.deviceList.map((item, index) => {
                    return (
                        <ListDeviceCard
                            onPress={() => {
                                //电池设备详情
                                this.props.navigation.navigate('BatteryDetails', {
                                    itemData: item,
                                    deviceName: item.deviceName,
                                    isWifiConnect: true,
                                    tempUnit: this.state.tempUnit,
                                    uType: item.uType
                                });
                            }}
                            onLongPress={() => {
                                this.setState({
                                    show: true,
                                    itemData: item,
                                })
                            }}
                            key={index}
                            data={item}
                        />
                    )
                })}

                {this.state.earbudsList.map((item, index) => {
                    return (
                        <ListEarbudsCard
                            onPress={() => {
                                //judy 跳转蓝牙耳机设备详情
                                if (Platform.OS === "android") {
                                    NativeModules.RNModule.openEarbuds(item.macAddress)
                                }
                            }}
                            onLongPress={() => {
                                this.setState({
                                    show: true,
                                    itemData: item,
                                })
                            }}
                            key={index}
                            data={item}
                        />
                    )
                })}
            </ScrollView>
        ))
        // 发现新设备弹窗
        const FindNewDeviceView = (
            <CommonMessageDialog
                leftBtnTextColor={'#808080'}
                onLeftBackgroundColor={'#F1F2F6'}
                rightBtnTextColor={'#ffffff'}
                onRightBackgroundColor={'#18B34F'}
                children={(<View style={{marginBottom: 30, alignItems: 'center'}}>
                    <Image style={{marginVertical: 20}}
                           source={require('../../resources/greenUnion/new_popup_dev_img.png')}/>
                    <CommonTextView
                        textSize={14}
                        text={this.state.FindNewDevice}
                        style={[{color: '#2F2F2F'}]}/>
                    <CommonTextView
                        textSize={12}
                        text={this.state.FindNewDeviceId}
                        style={[{color: '#2F2F2F'}]}/>
                </View>)}
                onRequestClose={() => {
                    this.setState({newDeviceDialog: false})
                }}
                overViewClick={() => {
                    this.setState({newDeviceDialog: false})
                }}
                modalVisible={this.state.newDeviceDialog}
                title={strings('discover_new_equipment')}
                message={strings('delete_prompt')}
                leftBtnText={strings('neglect')}
                onLeftBtnClick={() => {
                    //忽略
                    this.setState({newDeviceDialog: false})
                }}
                rightBtnText={strings('add_to')}
                onRightBtnClick={() => {
                    //添加
                    if (this.state.FindNewDevice) {
                        this.setState({newDeviceDialog: false})
                        // 发现新设备接口：

                    } else {
                        this.onShowToast(strings('mark_name_not_none'))
                    }
                }}/>)
        /*
        *接收分享设备弹窗
        */
        const _accpetShareDialog = (
            <AcceptDeviceShareDialog
                deviceImage={this.state.accpetShareDeviceData?.imgUrl}
                modalVisible={this.state.shareDialogVis}
                userName={this.state.accpetShareDeviceData?.nickName}
                deviceType={this.state.accpetShareDeviceData?.productTypeName}
                deviceName={this.state.accpetShareDeviceData?.productTypeName}
                onModalClose={() => {
                    this.setState({shareDialogVis: false})
                }}
                onChangeText={(text) => {
                    var accpetShareDeviceName = text.toString().trim()
                    this.setState({accpetShareDeviceData: {...this.state.accpetShareDeviceData, accpetShareDeviceName}})
                }}
                onLeftBtnClick={() => {
                    //稍后
                    this.setState({shareDialogVis: false})
                    this._accpteShareDevice(this.state.accpetShareDeviceData, 2)
                }}
                onRightBtnClick={() => {
                    //接受
                    if (this.state.accpetShareDeviceData?.accpetShareDeviceName) {
                        this.setState({shareDialogVis: false})
                        this._accpteShareDevice(this.state.accpetShareDeviceData, 1)
                    } else {
                        this.setState({shareDialogVis: false})
                        this.onShowToast(strings('设备名称不能为空'))
                    }
                }}/>)
        // 长按显示弹窗
        const _ChooseListDialog = (
            <ChooseListDialog
                show={this.state.show}
                data={this.tabData}
                currentId={this.state.currentId}
                _checkTab={this._checkTab}
                _onHide={this._onHide}
            />
        )

        const _newBannerView = (
            <BannerView
                onItemClick={(index) => {
                    //banner点击事件
                    if (this.state.bannerData.length > 0) {
                        var toUrl = this.state.bannerData[index].linkUrl
                        if (toUrl) {
                            this.props.navigation.navigate('UrlWebView', {url: toUrl});
                        }
                    }
                }}
                style={{borderRadius: 12, marginVertical: 8}}
                height={this._getHeightByWH(bannerUIWidth, bannerUIHeight, this.mScreenWidth - 24)}
                width={this.mScreenWidth - 24}
                images={this.state.bannerData}/>
        )
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#F1F2F6',
                    alignItems: 'center'
                }}>
                {TitleView}
                {this.state.bannerData.length > 0 ? _newBannerView : null}
                <View
                    style={{
                        flex: 1,
                        width: this.mScreenWidth,
                    }}
                    onLayout={(event) => this.onLayout(event)}>
                    <ScrollViewPull
                        refreshing={this.state.deviceInfoRefreshing}
                        refreshBegin={() => {
                            this.setState({deviceInfoRefreshing: true})
                            this._mainGetData(false)
                        }}
                        scrollEnd={() => {

                        }}>
                        {this.state.deviceList.length == 0 && this.state.earbudsList.length == 0 ? _noDeviceView : _deviceListView}
                    </ScrollViewPull>
                </View>
                {FindNewDeviceView}
                {DeleteDeviceView}
                {resetDeviceName}
                {shareDeviceView}
                {_accpetShareDialog}
                {_ChooseListDialog}
            </View>
        );
    }

    onLayout = (event) => { // 获取View的高度
        const viewHeight = event.nativeEvent.layout.height;
        this.setState({scrollContainerViewHeight: viewHeight});
    }

    // _bannerView() {
    //     return (
    //         <MainBannerView
    //             bannerData={this.state.bannerData}
    //             bannerHeight={this._getHeightByWH(bannerUIWidth, bannerUIHeight, this.mScreenWidth - 36)}
    //             onItemClick={(index) => {
    //                 //banner点击事件
    //                 if (this.state.bannerData.length > 0) {
    //                     var toUrl = this.state.bannerData[index].linkUrl
    //                     if (toUrl) {
    //                         this.props.navigation.navigate('UrlWebView', {url: toUrl});
    //                     }
    //                 }
    //             }}
    //         />
    //     )
    // }

    _checkTab = (data) => {
        const {itemData} = this.state;
        // 0:重命名 1:分享设备 2:删除设备
        this.setState({currentId: data.id, show: false}, () => {
            switch (data.id) {
                case 0:
                    this.setState({
                        resetDeviceName: true,
                        show: false,
                        deviceName: itemData?.deviceName,//重命名弹窗展开后显示当前选中设备名
                    })
                    break;
                case 1:
                    this.setState({
                        shareDevice: true
                    })
                    break;
                case 2:
                    this.setState({
                        deleteDialog: true,
                        show: false,
                    })
                    break;
                default:
                    break;
            }
        })
    }

    // 删除设备
    deleteDevice = () => {
        const {itemData, deviceList} = this.state;
        let NewDeviceList = deviceList?.filter((element) => (itemData.deviceId !== element.deviceId))
        NetUtil.delDevice(itemData?.deviceId, StorageHelper.getTempToken(), true)
            .then((res) => {
                this.setState({
                    deviceList: NewDeviceList,
                    deleteDialog: false,
                }, () => {
                    this.onShowToast(strings('successed'))
                    EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY)
                })
            })
            .catch((error) => {
                this.onShowToast(strings('failed'))
            })
    }

    _onHide = () => {
        this.setState({
            show: false
        })
    }

    _setUserParam() {
        var param = {
            bleDevice: []
        }
        NetUtil.setUserParams(StorageHelper.getTempToken(), param, true)
            .then((res) => {

            })
    }

    // 接受或者稍后处理接受分享
    _accpteShareDevice({deviceShareId, accpetShareDeviceName}, receive) {
        NetUtil.deviceReceveDetails(StorageHelper.getTempToken(), deviceShareId, receive, accpetShareDeviceName, true)
            .then((res) => {
                this.onShowToast(strings('successed'))
                this._getDeviceList(false, true)
            }).catch((error) => {

        })
    }

    int2ascll(hexCharCodeStr) {
        let hexCharCodeStrs = Number(hexCharCodeStr).toString(16)
        var trimedStr = hexCharCodeStrs.trim();
        var rawStr = trimedStr.substr(0, 2).toLowerCase() === "0x" ? trimedStr.substr(2) : trimedStr;
        var len = rawStr.length;
        if (len % 2 !== 0) {
            return "";
        }
        var curCharCode;
        var resultStr = [];
        for (var i = 0; i < len; i = i + 2) {
            curCharCode = parseInt(rawStr.substr(i, 2), 16);
            resultStr.push(String.fromCharCode(curCharCode));
        }
        return resultStr.join("");
    }

    hex2bytes = (hex) => {
        let pos = 0, len = hex.length;
        if (len % 2 != 0) {
            return null
        }
        len / 2;
        let bytes = new Array();
        for (let i = 0; i < len; i++) {
            let s = hex.substr(pos, 2);
            let v = parseInt(s, 16);
            bytes.push(v);
            pos += 2;
        }
        return bytes
    }

    _toAddDevicePager() {
        /*NetUtil.bindDevice(StorageHelper.getTempToken(), 'G06FC2212140609', false)
            .then((res) => {
                //绑定设备成功
            }).catch((error) => {
            //如果首次返回失败，可能是设备和云端未连接，需要多次调用绑定设备接口
        })*/

        /* let dataArray = this.hex2bytes('47 30 36 42 43 30 31 32 32 31 33')
         let dataIntArray = []
         for (let dataItem of dataArray) {
             let hex2int = CRC.strToHex(dataItem)
             dataIntArray.push(hex2int[0])
         }
         let sn = ''
         dataIntArray.forEach(function (value, index) {
             sn += this.int2ascll(value)
         });
         console.log('sn长度', sn)
         if (sn.length > 15) {
             let IotDeviceSn = sn.substr(0, 15)
             let deviceNameSn = sn.substr(15, 15)
             console.log('IotDeviceSn号：' + IotDeviceSn)
             console.log('sn号：' + deviceNameSn)
         } else {
             // console.log(sn);
             let deviceNameSn = sn.substr(0, 15)
             console.log('sn号：' + deviceNameSn)
         }*/

        this.props.navigation.navigate('BleAddDevice', {token: StorageHelper.getTempToken()});

        /*this.props.navigation.navigate('WifiDeviceConnectionAndroid',
            {
                macId: '123',
                showMac: ''
            });*/
    }

    //获取banner数据
    _getBannerData() {
        if (StorageHelper.getTempToken()) {
            NetUtil.getBannerAd(StorageHelper.getTempToken())
                .then((res) => {
                    var bannerData = res.list
                    this.setState({bannerData: bannerData.length > 0 ? bannerData : []})
                })
                .catch((error) => {
                    this.setState({deviceInfoRefreshing: false})
                })
        }
    }

    _getLocalUserInfo() {
        StorageHelper.getUserInfo()
            .then((userInfo) => {
                    if (userInfo) {
                        this._setUserInfoParam(userInfo)

                        this.setState({
                            userName: userInfo.name,
                            account: userInfo.account ? userInfo.account : userInfo.email,
                            avatarUrl: userInfo.avatar,
                            bgUrl: userInfo.background
                        })
                        if (userInfo.avatar)
                            this._setTopAvatar(userInfo.avatar)
                        else
                            this._setThirdAvatar()
                    } else {
                        this._setThirdAvatar()
                    }
                }
            ).catch((error) => {
            this._setThirdAvatar()
        })
    }

    _setUserInfoParam(userInfo) {
        try {
            var params = JSON.parse(userInfo.param)
            if (!Helper.dataIsNull(params) && params.bleDevice.length > 0) {
                bleDevices = params.bleDevice
            } else {
                bleDevices = []
            }
        } catch (e) {

        }
    }

    static getBleDevices() {
        return bleDevices
    }

    static setBleDevices(data, getUserInfoFlag) {
        bleDevices = data

        // if(getUserInfoFlag){
        //     this._getUserInfo(false)
        // }
    }

    _setBleDataToDeviceList(deviceList) {
        //蓝牙相关注释
        // var tempArray = deviceList
        // if (bleDevices.length > 0) {
        //     bleDevices.map((item, index) => {
        //         tempArray.push(item)
        //     })
        //     this.setState({
        //         devicelist: tempArray
        //     })
        // }
    }

    _getLocalTempUnit() {
        StorageHelper.getTempUnit()
            .then((unit) => {
                var unitValue = parseInt(unit) == 0 ? 2 : 1

                this.setState({
                    tempUnit: unitValue,
                })
                if (unit == undefined) {
                    //设置默认温度单位
                    StorageHelper.saveTempUnit(0)
                }
            }).catch((error) => {
        })
    }

    //获取用户信息
    _getUserInfo(showLoading, getFlag) {
        if (StorageHelper.getTempToken()) {
            NetUtil.getUserInfo(StorageHelper.getTempToken(), showLoading)
                .then((res) => {
                    var userInfo = res.info
                    if (userInfo) {
                        this._setUserInfoParam(userInfo)

                        StorageHelper.saveUserInfo(userInfo)
                        this.setState({
                            userName: userInfo.name,
                            account: userInfo.account,
                            avatarUrl: userInfo.avatar,
                            bgUrl: userInfo.background
                        })
                        if (userInfo.avatar)
                            this._setTopAvatar(userInfo.avatar)
                        else
                            this._setThirdAvatar()
                    } else {
                        this._setThirdAvatar()
                    }

                    if (getFlag) {
                        this._getBannerData()
                        this._getDeviceList(showLoading, !showLoading)
                        this._getDeviceSharePushList()
                    }
                })
                .catch((error) => {
                    // if (getFlag) {
                    //     this._getBannerData()
                    //     this._getDeviceList(showLoading)
                    //     this._getDeviceSharePushList()
                    // }
                    this._setThirdAvatar()
                    this.setState({deviceInfoRefreshing: false})
                })
        } else {
            console.log('not token')
        }
    }

    //获取设备列表
    _getDeviceList(showLoading, hideErrorToast) {
        if (StorageHelper.getTempToken()) {
            DeviceListUtil._getDeviceList(showLoading, StorageHelper.getTempToken(), hideErrorToast)
                .then((res) => {
                    var deviceInfo = res.list
                    if (deviceInfo) {
                        this._dealDeviceInfo(deviceInfo, true)
                    }
                    this.setState({deviceInfoRefreshing: false})

                    if (showLoading) {
                        //开始轮询
                        this._timerGetDeviceList()
                    }
                    if (!this.state.getFlag) {
                        LogUtil.AndroidLogWrite('getDeviceList success,size:' + deviceInfo.length)
                    }
                })
                .catch((error) => {
                    if (!this.state.getFlag) {
                        LogUtil.AndroidLogWrite('getDeviceList fail')
                    }
                    this.setState({
                        deviceInfoRefreshing: false,
                        getFlag: true
                    })
                    //this._stopTimerGetDeviceList()
                })
        }
    }

    //获取设备分享推送的消息列表
    _getDeviceSharePushList() {
        if (this.state.getShareInfoFlag)
            return
        NetUtil.deviceShare(StorageHelper.getTempToken(), 1, 10, false)
            .then((res) => {
                if (res?.list?.[0]) {
                    // deviceShareId	string	设备分享消息id
                    // createTime	string	创建时间
                    // auditStatus	string	t-已处理 f-未处理
                    const {createTime, auditStatus, deviceShareId} = res?.list?.[0];
                    //是否失效，超过24小时失效
                    var isInvalid = (parseInt((new Date().getTime() - createTime) / 1000) > (3600 * 24))
                    StorageHelper.getShareIds()
                        .then((data) => {
                            // 判断是否已存在
                            let boolValue = false
                            data?.forEach(element => {
                                if (element === deviceShareId) {
                                    boolValue = true
                                }
                            });
                            if (!auditStatus && !isInvalid && !boolValue) {
                                this.setState({
                                    accpetShareDeviceData: res?.list?.[0] || {},
                                    shareDialogVis: true
                                })
                                // 如果本地不存在，则保存
                                if (!data) {
                                    StorageHelper.saveShareIds([deviceShareId])
                                } else {
                                    // 存在，直接合并至本地
                                    StorageHelper.saveShareIds([...data, deviceShareId])
                                }
                            }
                        }).catch((error) => {
                        // console.log('error: ', error);
                    })
                }
            }).catch((error) => {
            this.setState({deviceInfoRefreshing: false})
        })
    }

    //根据id获取设备分享详情
    _getDeviceSharePushDetail(id) {
        NetUtil.getDeviceSharePushDetail(StorageHelper.getTempToken(), id, false)
            .then((res) => {
                this.setState({shareDialogVis: true})
            })
            .catch((error) => {

            })
    }

    //获取本地缓存设备列表
    _getLocalDeviceList() {
        DeviceListUtil._getLocalDeviceList()
            .then((res) => {
                var deviceInfo = res
                if (deviceInfo) {
                    this._dealDeviceInfo(deviceInfo)
                }
            })
            .catch((error) => {

            })
    }

    //解析设备列表接口返回的数据
    _dealDeviceInfo(deviceInfo, getSpecsFlag) {
        var tempDeviceArray = []
        var tempDeviceIds = []
        deviceInfo.map((item, index) => {
            tempDeviceArray.push({
                electricQuantity: 0,
                deviceType: item.alias,
                isWifiDevice: true,
                deviceName: item.deviceName,//用户设备名称
                sharer: item.sharer,//分享者编号 0 表示是自己配网绑定
                appuserId: item.appuserId,//用户编号
                productId: item.productId,//产品编号
                createTime: item.createTime,//创建时间
                uType: item.uType,//1 所有者 2 管理员 3 成员
                updateTime: item.updateTime,//更新时间
                productKey: item.productKey,//产品ID
                isOnline: item.onlineStatus,//在线状态
                mac: item.mac,//设备mac
                deviceId: item.deviceId,//设备id
                deviceInfo: null
            })
            tempDeviceIds.push(item.mac)

            if (!item.connect) {
                this.state.deviceList.map((item1, index1) => {
                    if (item1.deviceId == item.deviceId && item1.isOnline) {
                        LogUtil.debugLog('offline===========>设备离线：deviceId' + item.deviceId + ',mac:' + item.mac + ',deviceName:' + item.deviceNickname)
                        EventUtil.sendEvent(EventUtil.DEVICE_OFFLINE, {
                            deviceId: item.deviceId
                        })
                    }
                })
            }
            if (item.connect) {
                this.state.deviceList.map((item1, index1) => {
                    if (item1.deviceId == item.deviceId && !item1.isOnline) {
                        LogUtil.debugLog('online===========>设备上线：deviceId' + item.deviceId + ',mac:' + item.mac + ',deviceName:' + item.deviceNickname)
                        EventUtil.sendEvent(EventUtil.DEVICE_ONLINE, {
                            deviceId: item.deviceId
                        })
                    }
                })
            }
        })

        var deviceIds = JSON.stringify(tempDeviceIds)
        if (getSpecsFlag && deviceIds) {
            //this._setBleDataToDeviceList(array)
            //批量请求属性
            this._getSpecsById(deviceIds, tempDeviceArray)
        } else {
            this.setState({
                deviceList: tempDeviceArray,
                getFlag: true
            })
            this._setBleDataToDeviceList(tempDeviceArray)
        }
    }

    //批量获取设备属性
    _getSpecsById(devices, deviceArray) {
        // 修改列表提交号：0ad50a8e7125440f27df93309a4b57af262f774b
        var tempDeviceArray = deviceArray
        NetUtil.getSpecsByDeviceId(StorageHelper.getTempToken(), devices, false)
            .then((res) => {
                var info = res.info
                tempDeviceArray.map((item, index) => {
                    tempDeviceArray[index].deviceInfo = SpecUtil.resolveSpecs(info[item.mac], false)
                })
                if ((JSON.stringify(tempDeviceArray) != JSON.stringify(this.state.deviceList))) {
                    this.setState({
                        deviceList: tempDeviceArray,
                        getFlag: true
                    })
                }
                this._setBleDataToDeviceList(tempDeviceArray)
            })
            .catch((error) => {
                if (!this.state.getFlag) {
                    LogUtil.AndroidLogWrite('getSpecsById fail')
                }
                this.setState({
                    deviceList: tempDeviceArray,
                    getFlag: true
                })
                this._setBleDataToDeviceList(tempDeviceArray)
            })
    }

    _setTempUnitToDevice(unit) {
        var unitValue = 1
        if (unit) {
            unitValue = parseInt(unit) == 0 ? 1 : 2
        }

        this.state.deviceList.map((item, index) => {
            //下发设备温度单位
            SpecUtil._sendTempUnitByParams(StorageHelper.getTempToken(), item.deviceId, this.state.account, item.productKey, item.mac, unitValue)
        })
    }

    _getLocationByIp() {
        //120.238.191.116
        NetworkInfo.getIPV4Address().then(ip => {
            // NetUtil.getLocationByIp(ip, false)
            alert(ip)
        });
    }

    _initPush() {
        Amplify.configure({
            PushNotification: {
                appId: Platform.OS === 'android' ? NetConstants.GOOGLE_ANDROID_CLIENTID : NetConstants.GOOGLE_IOS_CLIENTID,
                requestIOSPermissions: false
            }
        });

        // get the notification data when notification is received
        PushNotification.onNotification((notification) => {
            // Note that the notification object structure is different from Android and IOS
            LogUtil.debugLog('in app notification', JSON.stringify(notification));

            if (Platform.OS === 'android') {

            } else {
                // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/push-notification-ios#finish)
                notification.finish(PushNotificationIOS.FetchResult.NoData);
            }
        });

// get the registration token
// This will only be triggered when the token is generated or updated.
        PushNotification.onRegister((token) => {
            LogUtil.debugLog('notification in app registration', token);
            if (token) {
                this._setPushTokenToCloud(token)
            }
        });

// get the notification data when notification is opened
        PushNotification.onNotificationOpened((notification) => {
            LogUtil.debugLog('the notification is opened', JSON.stringify(notification));
            if (notification) {
                //防止短时间内多次回调
                if (new Date().getTime() - this.state.openNotifitionTime < 5000) {
                    return
                }

                this.setState({openNotifitionTime: new Date().getTime()})
                if (Platform.OS === 'android') {
                    this._pushMessageClikeTo(notification.data)
                } else {
                    this._pushMessageIosClikeTo(notification)
                }
            }
        });

        if (Platform.OS === 'android') {
            NativeModules.RNPushNotification.getToken(
                token => {
                    LogUtil.debugLog('notification_token=====>' + token)
                    if (token) {
                        this._setPushTokenToCloud(token)
                    }
                },
                error => {
                    LogUtil.debugLog('notification_token_error=====>' + error)
                }
            );
        } else {
            //iOS获取设备推送token
            LogUtil.debugLog('GXRNManager.getDeviceToken()')
            GXRNManager.getDeviceToken();
        }
    }

    _setPushTokenToCloud(token) {
        if (!pushFlag) {
            return
        }
        LogUtil.AndroidLogWrite('==============main__setPushTokenToCloud=========>' + this.state.token)
        LogUtil.AndroidLogWrite('==============main__setPushTokenToCloud=========>userToken:' + userToken)
        pushFlag = false
        if (token && StorageHelper.getTempToken()) {
            NetUtil.setUserUp(StorageHelper.getTempToken(), token, 1, false, true)
                .then((res) => {
                }).catch((error) => {
                pushFlag = true
            })
        }
    }

    /*
    *消息类型Key：pushMessageType

    用户分享设备消息：userShareDevice
    设备故障消息：deviceAlarm
    设备规则消息：deviceRule
    家庭智能消息：familyWisdom
    家庭消息：familyMsg

    常规推送：appPush
    注：由于aws批量推送无法设置自定义，所以默认没有推送类型
     */

    //推送消息点击跳转
    _pushMessageClikeTo(pushData) {
        try {
            if (pushData && pushData['pinpoint.jsonBody']) {
                var jsonBody = JSON.parse(pushData['pinpoint.jsonBody'])
                var pushMessageType = jsonBody.pushMessageType
                this._goByPushMessageType(pushMessageType)
            }
        } catch (e) {
            LogUtil.debugLog('_pushMessageClikeTo========>error:' + JSON.stringify(e))
        }
    }

    _pushMessageIosClikeTo(pushData) {
        try {
            var jsonNotifition = JSON.parse(JSON.stringify(pushData))
            var jsonBody = jsonNotifition._data.data['jsonBody']
            var pushMessageType = jsonBody.pushMessageType
            this._goByPushMessageType(pushMessageType)


            // var jsonMessageData = JSON.parse(jsonBody.data)
            // alert(jsonMessageData.mac)
        } catch (e) {
            LogUtil.debugLog('_pushMessageClikeTo========>ios_error:' + JSON.stringify(e))
        }
    }

    _goByPushMessageType(pushMessageType) {
        if (pushMessageType == 'userShareDevice') {//用户分享设备消息
            this.state.getShareInfoFlag = false
            this.setState({getShareInfoFlag: false})
            this._getDeviceSharePushList()
        } else if (pushMessageType == 'deviceAlarm') {//设备故障消息

        } else if (pushMessageType == 'deviceRule') {//设备规则消息

        } else if (pushMessageType == 'familyWisdom') {//家庭智能消息

        } else if (pushMessageType == 'familyMsg') {//家庭消息

        }
    }
}