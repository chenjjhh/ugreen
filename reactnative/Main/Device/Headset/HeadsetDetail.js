// 耳机详情
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio, ScrollView, DeviceEventEmitter, NativeModules
} from 'react-native';
import BaseComponent from "../../Base/BaseComponent";
import TitleBar from "../../View/TitleBar";
import {strings} from "../../Language/I18n";
import ViewHelper from "../../View/ViewHelper";
import CommonTextView from "../../View/CommonTextView";
import DialogBottomBtnView from "../../View/DialogBottomBtnView";
import DialogContainerView from "../../View/DialogContainerView";
import CommonCheckView from "../../View/CommonCheckView";

export default class HeadsetDetail extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    leftIcon={require('../../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={'#F1F2F6'}
                    titleText={'UGREEN HiTune T6'}/>,
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            noiceControl: 0,//噪声控制
            tabIndex: 0,//0设备 1耳机操控 2更多
            noiseReductionWay: 0,//降噪方式
            equalizerValue: 0,//均衡器
            gameMode: false,
            noiseReductionDialog: false,
            noiseReductionWayData: [
                {title: strings('深度'), subTitle: strings('深度tips')},
                {title: strings('中度'), subTitle: strings('中度tips')},
                {title: strings('轻度'), subTitle: strings('轻度tips')}]
        }
    }

    render() {
        return (
            <View
                style={{
                    backgroundColor: '#F1F2F6',
                    flex: 1
                }}>
                {this._mainContainer()}
                {this._tabView()}
                {this._dialogView()}
            </View>
        );
    }

    _mainContainer() {
        return (
            <View
                style={{
                    flex: 1
                }}>
                {this.state.tabIndex == 1 ? this._headsetControlView() :
                    this.state.tabIndex == 2 ? this._moreView() : this._deviceView()}
            </View>
        )
    }

    //底部导航栏
    _tabView() {
        return (
            <View
                style={{
                    flexDirection: 'row'
                }}>
                {this._tabItemView(require('../../../resources/greenUnion/detail_nav_dev_ic.png'), require('../../../resources/greenUnion/detail_nav_dev_sel_ic.png'), this.state.tabIndex == 0, strings('设备'), () => {
                    //设备
                    if (this.state.tabIndex != 0) {
                        this.setState({
                            tabIndex: 0
                        })
                    }
                })}
                {this._tabItemView(require('../../../resources/greenUnion/detail_nav_ctl_ic.png'), require('../../../resources/greenUnion/detail_nav_ctl_sel_ic.png'), this.state.tabIndex == 1, strings('耳机操控'), () => {
                    //耳机操控
                    if (this.state.tabIndex != 1) {
                        this.setState({
                            tabIndex: 1
                        })
                    }
                })}
                {this._tabItemView(require('../../../resources/greenUnion/detail_nav_more_ic.png'), require('../../../resources/greenUnion/detail_nav_more_sel_ic.png'), this.state.tabIndex == 2, strings('更多'), () => {
                    //更多
                    if (this.state.tabIndex != 2) {
                        this.setState({
                            tabIndex: 2
                        })
                    }
                })}
            </View>
        )
    }

    _tabItemView(icon, checkIcon, isCheck, text, clickEvent) {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingTop: 12,
                    paddingBottom: 5
                }}
                onPress={clickEvent}>
                <Image
                    source={isCheck ? checkIcon : icon}/>

                <Text style={{
                    color: isCheck ? '#18B34F' : '#999BA2',
                    fontSize: 10,
                    marginTop: 10
                }}>{text}</Text>
            </TouchableOpacity>
        )
    }

    //弹窗
    _dialogView() {
        return (
            <View>
                {this._noiseReductionDialog()}
            </View>
        )
    }

    ////////////////////////////////////////////////设备//////////////////////////////////////////////////
    _deviceView() {
        return (
            <ScrollView>
                {this._topImageView()}
                {this._batteryView()}
                {this._noiceControlView()}
                {this._centerView()}
                {this._bottomView()}
            </ScrollView>
        )
    }

    //耳机图标
    _topImageView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingTop: 30,
                    paddingBottom: 40
                }}>
                <Image
                    source={require('../../../resources/greenUnion/detail_headphone_img.png')}/>
            </View>
        )
    }

    //电池
    _batteryView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.mScreenWidth - this.getSize(24),
                    marginLeft: this.getSize(12),
                    backgroundColor: '#fff',
                    borderRadius: this.getSize(12),
                    paddingHorizontal: this.getSize(30),
                    paddingVertical: this.getSize(20),
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                {this._batteryItemView(require('../../../resources/greenUnion/detail_battery_100_ic.png'), 100, require('../../../resources/greenUnion/detail_left_ic.png'))}

                {this._batteryItemView(require('../../../resources/greenUnion/detail_battery_100_ic.png'), 100, require('../../../resources/greenUnion/detail_right_ic.png'))}

                {this._batteryItemView(require('../../../resources/greenUnion/detail_battery_100_ic.png'), 100, require('../../../resources/greenUnion/detail_box_ic.png'))}
            </View>
        )
    }

    _batteryItemView(batteryImg, batteryValue, indexImg) {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={batteryImg}/>

                    <Text style={{
                        color: '#383838',
                        fontSize: 16,
                        marginLeft: 6
                    }}>{batteryValue}%</Text>
                </View>

                <Image
                    style={{
                        marginTop: 6
                    }}
                    source={indexImg}/>
            </View>
        )
    }


    //连接状态
    _connectStatusView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.mScreenWidth - this.getSize(24),
                    marginLeft: this.getSize(12),
                    backgroundColor: '#fff',
                    borderRadius: this.getSize(12),
                    paddingHorizontal: this.getSize(30),
                    paddingVertical: this.getSize(20),
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <Text style={{
                    color: '#383838',
                    fontSize: 16,
                }}>未连接</Text>

                {ViewHelper.getFlexView()}

                <Text style={{
                    color: '#17B34F',
                    fontSize: 16,
                }}>正在连接</Text>
            </View>
        )
    }

    //噪声控制
    _noiceControlView() {
        return (
            <View
                style={{
                    marginTop: this.getSize(11),
                    width: this.mScreenWidth - this.getSize(24),
                    borderRadius: this.getSize(12),
                    backgroundColor: '#fff',
                    paddingTop: this.getSize(17),
                    paddingHorizontal: this.getSize(19),
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: this.getSize(12)
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        paddingHorizontal: this.getSize(27),
                        paddingBottom: this.getSize(25)
                    }}>
                    {this._noiceControlItemView(require('../../../resources/greenUnion/detail_reduction_ic.png'), require('../../../resources/greenUnion/detail_reduction_sel_ic.png'), strings('降噪'), this.state.noiceControl == 0, () => {
                        if (this.state.noiceControl != 0) {
                            this.setState({
                                noiceControl: 0
                            })
                        }
                    })}

                    {this._noiceControlItemView(require('../../../resources/greenUnion/detail_off_ic.png'), require('../../../resources/greenUnion/detail_off_sel_ic.png'), strings('关闭'), this.state.noiceControl == 1, () => {
                        if (this.state.noiceControl != 1) {
                            this.setState({
                                noiceControl: 1
                            })
                        }
                    })}

                    {this._noiceControlItemView(require('../../../resources/greenUnion/detail_transparent_ic.png'), require('../../../resources/greenUnion/detail_transparent_sel_ic.png'), strings('通透'), this.state.noiceControl == 2, () => {
                        if (this.state.noiceControl != 2) {
                            this.setState({
                                noiceControl: 2
                            })
                        }
                    })}
                </View>

                {this.state.noiceControl == 0 ? this._noiceControlWayView() : null}
            </View>
        )
    }

    _noiceControlItemView(icon, checkIcon, text, isCheck, clickEvent) {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
                onPress={clickEvent}>
                <Image
                    source={isCheck ? checkIcon : icon}/>

                <Text style={{
                    color: '#808080',
                    fontSize: 12,
                    marginTop: 7
                }}>{text}</Text>
            </TouchableOpacity>
        )
    }

    //降噪方式
    _noiceControlWayView() {
        var noiseWayText = this._getNoiseReductionWayData()
        return (
            <View
                style={{
                    width: this.mScreenWidth - this.getSize(60),
                    paddingVertical: this.getSize(18)
                }}>
                <View
                    style={{
                        backgroundColor: '#F1F2F6',
                        height: this.getSize(0.5),
                        width: '100%'
                    }}/>

                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                    onPress={() => {
                        //降噪方式
                        this.setState({
                            noiseReductionDialog: true
                        })
                    }}>
                    <Text style={{
                        color: '#2F2F2F',
                        fontSize: 14,
                    }}>{strings('降噪方式')}</Text>

                    {ViewHelper.getFlexView()}

                    <Text style={{
                        color: '#808080',
                        fontSize: 14,
                    }}>{noiseWayText[0]}</Text>

                    <Image
                        source={require('../../../resources/greenUnion/list_arrow_ic.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    _getNoiseReductionWayData() {
        var wayData = [strings('深度'), strings('深度tips')]
        switch (parseInt(this.state.noiseReductionWay)) {
            case 0:
                wayData = [strings('深度'), strings('深度tips')]
                break
            case 1:
                wayData = [strings('中度'), strings('中度tips')]
                break
            case 2:
                wayData = [strings('轻度'), strings('轻度tips')]
                break
        }
        return wayData
    }

    //降噪弹窗
    _noiseReductionDialog() {
        return (
            <DialogContainerView
                cardStyle={{
                    alignItems: 'center',
                    with: this.mScreenWidth,
                    paddingHorizontal: this.getSize(30)
                }}
                overViewClick={() => {
                    this.setState({noiseReductionDialog: false})
                }}
                onRequestClose={() => {
                    this.setState({noiseReductionDialog: false})
                }}
                modalVisible={this.state.noiseReductionDialog}>

                {this.state.noiseReductionWayData.map((item, index) => {
                    return (this._noiseReductionItemView(item, index, this.state.noiseReductionWay == index, () => {
                        this._setNoiseReductionWay(index)
                    }))
                })}

            </DialogContainerView>
        )
    }

    _noiseReductionItemView(data, index, isCheck, clickEvent) {
        return (
            <TouchableOpacity
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
                key={index}
                onPress={clickEvent}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingVertical: this.getSize(18)
                    }}>
                    <View>
                        <CommonTextView
                            textSize={18}
                            text={data.title}
                            style={{
                                color: '#2F2F2F',
                                fontWeight: 'bold'
                            }}/>

                        <CommonTextView
                            textSize={14}
                            text={data.subTitle}
                            style={{
                                color: '#808080',
                                marginTop: 3
                            }}/>
                    </View>

                    {ViewHelper.getFlexView()}

                    <Image
                        style={{
                            width: this.getSize(25),
                            height: this.getSize(25),
                            resizeMode: 'contain'
                        }}
                        source={isCheck ? require('../../../resources/greenUnion/sel_ic.png') : require('../../../resources/greenUnion/unsel_ic.png')}/>
                </View>

                {this._lineView()}
            </TouchableOpacity>
        )
    }

    _setNoiseReductionWay(index) {
        this.setState({
            noiseReductionWay: index
        })
    }

    _centerView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.mScreenWidth - this.getSize(24),
                    marginLeft: this.getSize(12),
                    marginTop: this.getSize(12)
                }}>
                {this._centerItemView(strings('均衡器'), this._getEqualizerText(), () => {
                    //均衡器
                })}

                <View
                    style={{
                        width: this.getSize(12),
                        height: 1
                    }}/>

                {this._centerItemView(strings('游戏模式'), this.state.gameMode ? strings('开启') : strings('关闭'), () => {
                    //游戏模式
                })}
            </View>
        )
    }

    _getEqualizerText() {
        var data = strings('经典均衡')
        switch (parseInt(this.state.equalizerValue)) {
            case 0:
                data = strings('经典均衡')
                break
            case 1:
                data = strings('动感低音')
                break
            case 2:
                data = strings('悠扬人声')
                break
        }
        return data
    }

    _centerItemView(text, value, clcikEvent) {
        return (
            <TouchableOpacity
                style={{
                    flex: 1,
                    padding: this.getSize(20),
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'row',
                    backgroundColor: '#FFFFFF',
                    borderRadius: this.getSize(12)
                }}
                onPress={clcikEvent}>
                <View>
                    <Text style={{
                        color: '#2F2F2F',
                        fontSize: 16,
                        fontWeight: 'bold'
                    }}>{text}</Text>

                    <Text style={{
                        marginTop: 3,
                        color: '#808080',
                        fontSize: 18
                    }}>{value}</Text>
                </View>

                {ViewHelper.getFlexView()}

                <Image
                    source={require('../../../resources/greenUnion/list_arrow_ic.png')}/>
            </TouchableOpacity>
        )
    }

    _bottomView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - this.getSize(24),
                    marginLeft: this.getSize(12),
                    marginTop: this.getSize(12),
                    backgroundColor: '#FFFFFF',
                    borderRadius: this.getSize(12),
                    paddingHorizontal: this.getSize(20)
                }}>
                {this._bottomItemView(strings('空间音效'), () => {
                    //空间音效
                })}
                {this._lineView()}
                {this._bottomItemView(strings('设备双连'), () => {
                    //设备双连
                })}
                {this._lineView()}
                {this._bottomItemView(strings('查找耳机'), () => {
                    //查找耳机
                })}
                {this._lineView()}
                {this._bottomItemView(strings('高音质解码'), () => {
                    //高音质解码
                })}
            </View>
        )
    }

    _bottomItemView(text, clcikEvent) {
        return (
            <TouchableOpacity
                onPress={clcikEvent}
                style={{
                    flexDirection: 'row',
                    paddingVertical: this.getSize(20),
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <Text style={{
                    color: '#2F2F2F',
                    fontSize: 16,
                    fontWeight: 'bold'
                }}>{text}</Text>

                {ViewHelper.getFlexView()}

                <Image
                    source={require('../../../resources/greenUnion/list_arrow_ic.png')}/>
            </TouchableOpacity>
        )
    }

    _lineView() {
        return (
            <View
                style={{
                    height: this.getSize(0.5),
                    backgroundColor: '#F1F2F6',
                    width: '100%'
                }}/>
        )
    }

    ////////////////////////////////////////////////设备//////////////////////////////////////////////////

    ////////////////////////////////////////////////耳机操控//////////////////////////////////////////////////
    _headsetControlView() {
        return (
            <View/>
        )
    }

    ////////////////////////////////////////////////耳机操控//////////////////////////////////////////////////

    ////////////////////////////////////////////////更多//////////////////////////////////////////////////
    _moreView() {
        return (
            <View/>
        )
    }

    ////////////////////////////////////////////////更多//////////////////////////////////////////////////
}