import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    ImageBackground,
    View, BackHandler, Clipboard, TextInput, PixelRatio, NativeEventEmitter, AppState, PermissionsAndroid
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import ViewHelper from '../View/ViewHelper'
import AnimatedCircularProgress from '../View/CircularProgress/AnimatedCircularProgress'
import CircleView from '../View/CircularProgress/CircleView'
import ShadowCardView from "../View/ShadowCardView";
import MessageDialog from "../View/MessageDialog";
import DetailLightControlDialog from "../View/DetailLightControlDialog";
import SwitchView from "../View/SwitchView";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";
import SpecUtil from "../Utils/SpecUtil";
import Helper from "../Utils/Helper";
import I18n from "react-native-i18n";
import ChartView from "../View/ChartView";
import Bluetooth from "../Bluetooth/Bluetooth";
import SplashScreen from "react-native-splash-screen";
import DeviceSettingSlideView from "../View/DeviceSettingSlideView";
import LogUtil from "../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager

/*
*添加设备
 */

let that
let sendUnitFlag = false
const powerUnit = 'w'
const specDelayTime = 5000
const gifImgWidth = 448
const gifImgHeight = 321
const iconFlashTime = 300
const bleDeviceStartName = 'Gendome'
var dataMap = new Map()
var tempSpecDatas = null

async function hasAndroidPermission() {
    const permission = PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION;
    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
        return true;
    }
    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
}

export default class DeviceDetail extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null
        };
    }

    _toDeviceSetting() {
        this.props.navigation.navigate('DeviceSetting', {
            account: this.state.account,
            mac: this.state.mac,
            showMac: this.state.showMac,
            pk: this.state.pk,
            beeSwitch: this.state.beeSwitch,
            acChargePower: this.state.acChargePower,
            pvChargePower: this.state.pvChargePower,
            fanChargePower: this.state.fanChargePower,
            deviceStandByTime: this.state.deviceStandByTime,
            screenOffTime: this.state.screenOffTime,
            acStandByTime: this.state.acStandByTime,
            deviceName: this.state.deviceName,
            deviceId: this.state.deviceId,
            token: this.state.token,
            isWifiOnline: this.state.isWifiOnline,
            uType: this.state.uType,
            batteryProtectRangeMin: this.state.batteryProtectRangeMin,
            batteryProtectRangeMax: this.state.batteryProtectRangeMax,
            chargeStatus: this.state.chargeStatus,
            isWifiConnect: this.state.isWifiConnect,
            elcValue: this.state.elcValue,

            specData: {
                capacity: this.state.capacity,
                weight1: this.state.weight1,
                weight2: this.state.weight2,
                cycleCharge1: this.state.cycleCharge1,
                cycleCharge2: this.state.cycleCharge2,
                chargeTempMin: this.state.chargeTempMin,
                chargeTempMax: this.state.chargeTempMax,
                chargeTempT: this.state.chargeTempT,
                disChargeTempMin: this.state.disChargeTempMin,
                disChargeTempMax: this.state.disChargeTempMax,
                disChargeTempT: this.state.disChargeTempT,
                inputAcMin: this.state.inputAcMin,
                inputAcMax: this.state.inputAcMax,
                inputPvMax: this.state.inputPvMax,
                inputWindElcMax: this.state.inputWindElcMax,
                batteryPackNum: this.state.batteryPackNum,
                acOutputMaxPower: this.state.acOutputMaxPower,
                socket20ANum: this.state.socket20ANum,
                socket30ANum: this.state.socket30ANum,
                dcOutputMaxPower: this.state.dcOutputMaxPower,
                usbANum: this.state.usbANum,
                usbCNum: this.state.usbCNum,
                wirelessChargeNum: this.state.wirelessChargeNum,
                cigarette12vNum: this.state.cigarette12vNum,
                dc12VNum: this.state.dc12VNum,
                xt60Num: this.state.xt60Num,
                deviceType: this.state.deviceType
            }
        });
    }

    constructor(props) {
        super(props);
        that = this
        this.state = {
            routesLength: 2,
            isWifiConnect: props.navigation.state.params.isWifiConnect,
            tempValue: 0,//设备环境温度
            elcValue: 0,//电量
            tabIndex: 0,
            deviceOffLineDialog: false,
            deviceLowElcDialog: false,
            lightControlDialog: false,
            outputACPowerSwitch: false,//输出ac
            outputDCPowerSwitch: false,//输出dc 12v
            outputACPowerSwitchTime: 0,
            outputDCPowerSwitchTime: 0,

            deviceData: props.navigation.state.params.itemData,
            deviceType: '——',
            isOnline: false,//是否wifi在线
            isCharging: false,//是否充电中
            inputPvPower: 0,//太阳能功率
            inputFanPower: 0,//风能功率
            inputAcPower: 0,//交流电输入功率
            outputACPower: 0,//交流电输出功率
            outputDCPower: 0,//直流电输出功率12v
            usbA1Power: 0,
            usbA2Power: 0,
            usbA3Power: 0,
            usbA4Power: 0,
            typeC1Power: 0,
            typeC2Power: 0,

            brightness: 0,//屏幕亮度
            lightBrightness: 0,//灯带亮度
            topLight: 255,
            bottomLight: 255,
            moodLight: 255,
            topBottomLight: 0,

            beeSwitch: false,//蜂鸣器
            acChargePower: 0,//ac充电功率
            pvChargePower: 0,//光伏充电功率
            fanChargePower: 0,//风力充电功率
            deviceStandByTime: 0,//设备待机时间
            screenOffTime: 0,//息屏时间
            acStandByTime: 0,//交流待机时间

            chargeRemindTime: 0,//充电剩余时长
            disChargeRemindTime: 0,//放电剩余时长

            account: '',
            mac: '',
            pk: '',
            showMac: '',

            isWifiOnline: false,
            deviceName: props.navigation.state.params.title,
            deviceId: '',
            token: '',
            uType: props.navigation.state.params.uType,//1 所有者 2 管理员 3 成员
            inputTotalPower: 0,//输入总功率
            outputTotalPower: 0,//输出总功率
            package1BatteryPower: 0,//电池包1电池功率
            package1ElcRemind: 0,//电池包1电池剩余电量
            package2BatteryPower: 0,//电池包2电池功率
            package2ElcRemind: 0,//电池包2电池剩余电量

            batteryProtectRangeMin: 0,//BMS电池保护下限值设置
            batteryProtectRangeMax: 0,//BMS电池保护上限值设置
            chargeStatus: 0,//充电状态

            progressViewSize: this.mScreenWidth * 0.57,
            topBgWH: this.mScreenWidth * 0.47,


            capacity: 0,
            weight1: 0,
            weight2: 0,
            cycleCharge1: 0,
            cycleCharge2: 0,
            chargeTempMin: 0,
            chargeTempMax: 0,
            chargeTempT: 0,
            disChargeTempMin: 0,
            disChargeTempMax: 0,
            disChargeTempT: 0,
            inputAcMin: 0,
            inputAcMax: 0,
            inputPvMax: 0,
            inputWindElcMax: 0,
            batteryPackNum: 0,
            acOutputMaxPower: 0,
            socket20ANum: 0,
            socket30ANum: 0,
            dcOutputMaxPower: 0,
            usbANum: 0,
            usbCNum: 0,
            wirelessChargeNum: 0,
            cigarette12vNum: 0,
            dc12VNum: 0,
            xt60Num: 0,

            pvChartYAllArray: [],
            pvChartDataArray: [],
            windChartYAllArray: [],
            windChartDataArray: [],
            acChartYAllArray: [],
            acChartDataArray: [],

            errorTitle: '',
            errorTips: '',
            errorArray: [],
            errorTipsDialog: false,
            tempUnit: props.navigation.state.params.tempUnit ? props.navigation.state.params.tempUnit : 1,

            sendScreenTime: 0,
            sendLightTime: 0,
            sendTopLightTime: 0,
            sendBottomLightTime: 0,
            sendTopBottomLightTime: 0,
            sendTempUnitTime: 0,
            dcAllImgWidth: 0,
            getDataFlag: false,

            dcAll: 0,//12v直流输出功率+无线充输出总功率+房车RV接口输出功率
            dc12VAllValue: 0,
            outputAllValue: 0,
            showBleIcon: true,
            bleConnectStatus: 0,//0未连接，1连接中，2连接成功，3连接中
            onBleTime: 0,
            acInverterPower: 0,

            dc12vOutputPower: 0,
            houseCarPower: 0,
            wirelessOutputPower: 0,

            bleTipsDialog: false,
            gpsTipsDialog: false,
            locationTipsDialog: false,

            getLineTime: 0,
        }
    }

    componentWillMount() {
        tempSpecDatas = null
        //this._bleDataResolve('000200B1000000B20000')
        this._getLocalTempUnit()
        sendUnitFlag = false
        //this.setState({deviceLowElcDialog: true})
        this._getUserInfo()
        var deviceData = this.state.deviceData
        if (deviceData) {
            if (deviceData.deviceInfo) {
                this._resolveSpecData(deviceData.deviceInfo)
            }
            this.setState({
                mac: deviceData.mac,
                pk: deviceData.productKey,
                isWifiOnline: deviceData.isOnline,
                deviceId: deviceData.deviceId,
                uType: deviceData.uType,
                deviceType: deviceData.deviceType,
            })
            this._setBleConnectStatus(0)
            this._getDeviceProps(deviceData.deviceId, deviceData.mac, true, false)
            this._setParams(deviceData)

            if (!deviceData.isOnline) {
                this.setState({deviceOffLineDialog: true})
                this._bleEvent()
            }
        }

        if (!this.state.isWifiConnect) {
            //非WiFi连接，初始化蓝牙相关事件
            this._bleSuccessSetData(this.props.navigation.state.params.mac)
            this._bleEvent()
        } else {
            this._eventListenerAdd()
        }
    }

    _bleSuccessSetData(mac) {
        this.setState({
            isOnline: true,
            isWifiOnline: true,
            mac: mac,
            getDataFlag: true,
            isWifiConnect: false,
            showMac: this.props.navigation.state.params.showMac
        })
        this._setBleConnectStatus(2)
        SpecUtil.initBleProps(false)
        this._stopScanTimer()
    }

    _startBleQueryCode() {
        this._stopBleQueryCode()
        this.intervalBleQueryCode = setInterval(() => {
            SpecUtil._bleSendQueryCode()
        }, 5000)
    }

    _stopBleQueryCode() {
        if (this.intervalBleQueryCode) {
            clearInterval(this.intervalBleQueryCode)
            this.intervalBleQueryCode = null
        }
    }

    componentWillUnmount() {
        this._eventListenerRemove()
        this._bleDestory()
        this._stopBleIconAnimated()
        this._stopTimerGetDeviceProps()
        this._stopScanTimer()
        this._stopBleQueryCode()
    }

    _setParams(deviceData) {
        this.setState({isOnline: deviceData.isOnline})
    }

    _getUserInfo() {
        StorageHelper.getUserInfo()
            .then((res) => {
                if (res) {
                    this.setState({
                        account: res.account ? res.account : res.email,
                    })
                }
            })
            .catch((error) => {

            })
    }

    _getLocalTempUnit() {
        StorageHelper.getTempUnit()
            .then((unit) => {
                var unitValue = 1
                unitValue = parseInt(unit) == 0 ? 2 : 1

                //SpecUtil._sendTempUnit(this.state.account, this.state.pk, this.state.mac, unitValue)
                sendUnitFlag = true
                this.setState({
                    tempUnit: unitValue,
                    sendTempUnitTime: new Date().getTime()
                })
            }).catch((error) => {
        })
    }

    //获取设备属性
    _getDeviceProps(deviceId, mac, showLoading, hideErrorToast) {
        if (Helper.dataIsNull(deviceId)) {
            return
        }

        this.setState({token: StorageHelper.getTempToken()})
        SpecUtil.initProps(StorageHelper.getTempToken(), deviceId, this.state.isWifiConnect)
        // if (!sendUnitFlag) {
        //     this._getLocalTempUnit()
        // }
        NetUtil.getDeviceProps(StorageHelper.getTempToken(), deviceId, showLoading, hideErrorToast)
            .then((res) => {
                var specDatas = SpecUtil.resolveSpecs(res.info, false)

                if (JSON.stringify(specDatas) != JSON.stringify(tempSpecDatas) && parseInt(this.state.routesLength) == 2) {
                    tempSpecDatas = specDatas

                    if (this._canGetSpecData(this.state.sendScreenTime) && this.state.brightness != specDatas.deviceScreenLight) {
                        this.setState({
                            brightness: !Helper.dataIsNull(specDatas.deviceScreenLight) ? specDatas.deviceScreenLight : this.state.brightness,
                        })
                    }
                    if (this._canGetSpecData(this.state.sendLightTime) && this.state.lightBrightness != specDatas.lightSet) {
                        this.setState({
                            lightBrightness: !Helper.dataIsNull(specDatas.lightSet) ? specDatas.lightSet : this.state.lightBrightness,
                        })
                    }
                    var tempTopLight = !Helper.dataIsNull(specDatas.topLight) ? specDatas.topLight : this.state.topLight
                    var tempBottomLight = !Helper.dataIsNull(specDatas.bottomLight) ? specDatas.bottomLight : this.state.bottomLight
                    if (this._canGetSpecData(this.state.sendTopBottomLightTime) && this.state.topLight != tempTopLight) {
                        this.setState({
                            topLight: tempTopLight,
                        })
                    }
                    if (this._canGetSpecData(this.state.sendTopBottomLightTime) && this.state.bottomLight != tempBottomLight) {
                        this.setState({
                            bottomLight: tempBottomLight,
                        })
                    }
                    this._setModeLight(tempTopLight, tempBottomLight)


                    if (!this.state.lightControlDialog) {
                        if (this._canGetSpecData(this.state.outputACPowerSwitchTime) && this.state.outputACPowerSwitch != specDatas.acOutputSwitch) {
                            if (!Helper.dataIsNull(specDatas.acOutputSwitch)) {
                                this.setState({
                                    outputACPowerSwitch: specDatas.acOutputSwitch,
                                })
                            }
                        }
                        if (this._canGetSpecData(this.state.outputDCPowerSwitchTime) && this.state.outputDCPowerSwitch != specDatas.dcOutputSwitch) {
                            if (!Helper.dataIsNull(specDatas.dcOutputSwitch)) {
                                this.setState({
                                    outputDCPowerSwitch: specDatas.dcOutputSwitch,
                                })
                            }
                        }
                        if (this.state.acInverterPower != specDatas.inverterPower) {
                            this.setState({
                                acInverterPower: !Helper.dataIsNull(specDatas.inverterPower) ? specDatas.inverterPower : this.state.acInverterPower,
                            })
                        }
                        if (this.state.tempValue != specDatas.enirTemp) {
                            this.setState({
                                tempValue: !Helper.dataIsNull(specDatas.enirTemp) ? specDatas.enirTemp : this.state.tempValue,
                            })
                        }
                        if (this.state.elcValue != specDatas.curRemaining) {
                            this.setState({
                                elcValue: !Helper.dataIsNull(specDatas.curRemaining) ? specDatas.curRemaining : this.state.elcValue,
                            })
                        }
                        if (this.state.inputPvPower != specDatas.pvInputPower) {
                            this.setState({
                                inputPvPower: !Helper.dataIsNull(specDatas.pvInputPower) ? specDatas.pvInputPower : this.state.inputPvPower,
                            })
                        }
                        if (this.state.inputFanPower != specDatas.fanInputPower) {
                            this.setState({
                                inputFanPower: !Helper.dataIsNull(specDatas.fanInputPower) ? specDatas.fanInputPower : this.state.inputFanPower,
                            })
                        }
                        if (this.state.inputAcPower != specDatas.acInputPower) {
                            this.setState({
                                inputAcPower: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.inputAcPower,
                            })
                        }
                        if (this.state.outputACPower != specDatas.acOutputTotalPower) {
                            this.setState({
                                outputACPower: !Helper.dataIsNull(specDatas.acOutputTotalPower) ? specDatas.acOutputTotalPower : this.state.outputACPower,
                            })
                        }
                        if (this.state.outputDCPower != specDatas.dcOutputPower) {
                            this.setState({
                                outputDCPower: !Helper.dataIsNull(specDatas.dcOutputPower) ? specDatas.dcOutputPower : this.state.outputDCPower,
                            })
                        }
                        if (this.state.usbA1Power != specDatas.usbA1Power) {
                            this.setState({
                                usbA1Power: !Helper.dataIsNull(specDatas.usbA1Power) ? specDatas.usbA1Power : this.state.usbA1Power,
                            })
                        }
                        if (this.state.usbA2Power != specDatas.usbA2Power) {
                            this.setState({
                                usbA2Power: !Helper.dataIsNull(specDatas.usbA2Power) ? specDatas.usbA2Power : this.state.usbA2Power,
                            })
                        }
                        if (this.state.usbA3Power != specDatas.usbA3Power) {
                            this.setState({
                                usbA3Power: !Helper.dataIsNull(specDatas.usbA3Power) ? specDatas.usbA3Power : this.state.usbA3Power,
                            })
                        }
                        if (this.state.usbA4Power != specDatas.usbA4Power) {
                            this.setState({
                                usbA4Power: !Helper.dataIsNull(specDatas.usbA4Power) ? specDatas.usbA4Power : this.state.usbA4Power,
                            })
                        }
                        if (this.state.typeC1Power != specDatas.typeC1Power) {
                            this.setState({
                                typeC1Power: !Helper.dataIsNull(specDatas.typeC1Power) ? specDatas.typeC1Power : this.state.typeC1Power,
                            })
                        }
                        if (this.state.typeC2Power != specDatas.typeC2Power) {
                            this.setState({
                                typeC2Power: !Helper.dataIsNull(specDatas.typeC2Power) ? specDatas.typeC2Power : this.state.typeC2Power,
                            })
                        }
                        if (this.state.beeSwitch != specDatas.beeSwitch) {
                            this.setState({
                                beeSwitch: !Helper.dataIsNull(specDatas.beeSwitch) ? specDatas.beeSwitch : this.state.beeSwitch,
                            })
                        }
                        if (this.state.acChargePower != specDatas.acInputPower) {
                            this.setState({
                                acChargePower: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePower,
                            })
                        }
                        if (this.state.pvChargePower != specDatas.pvChargePower) {
                            this.setState({
                                pvChargePower: parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePower,
                            })
                        }
                        if (this.state.fanChargePower != specDatas.fanChargePower) {
                            this.setState({
                                fanChargePower: parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePower,
                            })
                        }
                        if (this.state.deviceStandByTime != specDatas.deviceStandbyTime) {
                            this.setState({
                                deviceStandByTime: parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandByTime,
                            })
                        }
                        if (this.state.screenOffTime != specDatas.screenOffTime) {
                            this.setState({
                                screenOffTime: parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTime,
                            })
                        }
                        if (this.state.acStandByTime != specDatas.acStandbyTime) {
                            this.setState({
                                acStandByTime: parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandByTime,
                            })
                        }
                        if (this.state.chargeRemindTime != specDatas.chargeRemindTime) {
                            this.setState({
                                chargeRemindTime: !Helper.dataIsNull(specDatas.chargeRemindTime) ? specDatas.chargeRemindTime : this.state.chargeRemindTime,
                            })
                        }
                        if (this.state.disChargeRemindTime != specDatas.disChargeRemindTime) {
                            this.setState({
                                disChargeRemindTime: !Helper.dataIsNull(specDatas.disChargeRemindTime) ? specDatas.disChargeRemindTime : this.state.disChargeRemindTime,
                            })
                        }
                        if (this.state.inputTotalPower != specDatas.inputTotalPower) {
                            this.setState({
                                inputTotalPower: !Helper.dataIsNull(specDatas.inputTotalPower) ? specDatas.inputTotalPower : this.state.inputTotalPower,
                            })
                        }
                        if (this.state.outputTotalPower != specDatas.outputTotalPower) {
                            this.setState({
                                outputTotalPower: !Helper.dataIsNull(specDatas.outputTotalPower) ? specDatas.outputTotalPower : this.state.outputTotalPower,
                            })
                        }
                        if (this.state.package1BatteryPower != specDatas.packInputPower1) {
                            this.setState({
                                package1BatteryPower: !Helper.dataIsNull(specDatas.packInputPower1) ? specDatas.packInputPower1 : this.state.package1BatteryPower,
                            })
                        }
                        if (this.state.package1ElcRemind != specDatas.packRemind1) {
                            this.setState({
                                package1ElcRemind: !Helper.dataIsNull(specDatas.packRemind1) ? specDatas.packRemind1 : this.state.package1ElcRemind,
                            })
                        }
                        if (this.state.package2BatteryPower != specDatas.packInputPower2) {
                            this.setState({
                                package2BatteryPower: !Helper.dataIsNull(specDatas.packInputPower2) ? specDatas.packInputPower2 : this.state.package2BatteryPower,
                            })
                        }
                        if (this.state.package2ElcRemind != specDatas.packRemind2) {
                            this.setState({
                                package2ElcRemind: !Helper.dataIsNull(specDatas.packRemind2) ? specDatas.packRemind2 : this.state.package2ElcRemind,
                            })
                        }
                        if (this.state.batteryProtectRangeMin != specDatas.batteryProtectRangeMin) {
                            this.setState({
                                batteryProtectRangeMin: parseInt(specDatas.batteryProtectRangeMin) >= 0 ? specDatas.batteryProtectRangeMin : this.state.batteryProtectRangeMin,
                            })
                        }
                        if (this.state.batteryProtectRangeMax != specDatas.batteryProtectRangeMax) {
                            this.setState({
                                batteryProtectRangeMax: !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? specDatas.batteryProtectRangeMax : this.state.batteryProtectRangeMax,
                            })
                        }
                        if (this.state.chargeStatus != specDatas.chargeStatus) {
                            this.setState({
                                chargeStatus: !Helper.dataIsNull(specDatas.chargeStatus) ? specDatas.chargeStatus : this.state.chargeStatus,
                            })
                        }
                        if (this.state.dc12vOutputPower != specDatas.dc12vOutputPower) {
                            this.setState({
                                dc12vOutputPower: !Helper.dataIsNull(specDatas.dc12vOutputPower) ? specDatas.dc12vOutputPower : this.state.dc12vOutputPower,
                            })
                        }
                        if (this.state.houseCarPower != specDatas.houseCarPower) {
                            this.setState({
                                houseCarPower: !Helper.dataIsNull(specDatas.houseCarPower) ? specDatas.houseCarPower : this.state.houseCarPower,
                            })
                        }
                        if (this.state.wirelessOutputPower != specDatas.wirelessOutputPower) {
                            this.setState({
                                wirelessOutputPower: !Helper.dataIsNull(specDatas.wirelessOutputPower) ? specDatas.wirelessOutputPower : this.state.wirelessOutputPower,
                            })
                        }
                        if (!this.state.getDataFlag) {
                            this.setState({
                                getDataFlag: true
                            })
                        }
                        // this.setState({
                        //     // capacity: !Helper.dataIsNull(specDatas.capacity) ? specDatas.capacity : this.state.capacity,
                        //     // weight1: !Helper.dataIsNull(specDatas.weight1) ? specDatas.weight1 : this.state.weight1,
                        //     // weight2: !Helper.dataIsNull(specDatas.weight2) ? specDatas.weight2 : this.state.weight2,
                        //     // cycleCharge1: !Helper.dataIsNull(specDatas.cycleCharge1) ? specDatas.cycleCharge1 : this.state.cycleCharge1,
                        //     // cycleCharge2: !Helper.dataIsNull(specDatas.cycleCharge2) ? specDatas.cycleCharge2 : this.state.cycleCharge2,
                        //     // chargeTempMin: !Helper.dataIsNull(specDatas.chargeTempMin) ? specDatas.chargeTempMin : this.state.chargeTempMin,
                        //     // chargeTempMax: !Helper.dataIsNull(specDatas.chargeTempMax) ? specDatas.chargeTempMax : this.state.chargeTempMax,
                        //     // chargeTempT: !Helper.dataIsNull(specDatas.chargeTempT) ? specDatas.chargeTempT : this.state.chargeTempT,
                        //     // disChargeTempMin: !Helper.dataIsNull(specDatas.disChargeTempMin) ? specDatas.disChargeTempMin : this.state.disChargeTempMin,
                        //     // disChargeTempMax: !Helper.dataIsNull(specDatas.disChargeTempMax) ? specDatas.disChargeTempMax : this.state.disChargeTempMax,
                        //     // disChargeTempT: !Helper.dataIsNull(specDatas.disChargeTempT) ? specDatas.disChargeTempT : this.state.disChargeTempT,
                        //     // inputAcMin: !Helper.dataIsNull(specDatas.inputAcMin) ? specDatas.inputAcMin : this.state.inputAcMin,
                        //     // inputAcMax: !Helper.dataIsNull(specDatas.inputAcMax) ? specDatas.inputAcMax : this.state.inputAcMax,
                        //     // inputPvMax: !Helper.dataIsNull(specDatas.inputPvMax) ? specDatas.inputPvMax : this.state.inputPvMax,
                        //     // inputWindElcMax: !Helper.dataIsNull(specDatas.inputWindElcMax) ? specDatas.inputWindElcMax : this.state.inputWindElcMax,
                        //     // batteryPackNum: !Helper.dataIsNull(specDatas.batteryPackNum) ? specDatas.batteryPackNum : this.state.batteryPackNum,
                        //     // acOutputMaxPower: !Helper.dataIsNull(specDatas.acOutputMaxPower) ? specDatas.acOutputMaxPower : this.state.acOutputMaxPower,
                        //     // socket20ANum: !Helper.dataIsNull(specDatas.socket20ANum) ? specDatas.socket20ANum : this.state.socket20ANum,
                        //     // socket30ANum: !Helper.dataIsNull(specDatas.socket30ANum) ? specDatas.socket30ANum : this.state.socket30ANum,
                        //     // dcOutputMaxPower: !Helper.dataIsNull(specDatas.dcOutputMaxPower) ? specDatas.dcOutputMaxPower : this.state.dcOutputMaxPower,
                        //     // usbANum: !Helper.dataIsNull(specDatas.usbANum) ? specDatas.usbANum : this.state.usbANum,
                        //     // usbCNum: !Helper.dataIsNull(specDatas.usbCNum) ? specDatas.usbCNum : this.state.usbCNum,
                        //     // wirelessChargeNum: !Helper.dataIsNull(specDatas.wirelessChargeNum) ? specDatas.wirelessChargeNum : this.state.wirelessChargeNum,
                        //     // cigarette12vNum: !Helper.dataIsNull(specDatas.cigarette12vNum) ? specDatas.cigarette12vNum : this.state.cigarette12vNum,
                        //     // dc12VNum: !Helper.dataIsNull(specDatas.dc12VNum) ? specDatas.dc12VNum : this.state.dc12VNum,
                        //     // xt60Num: !Helper.dataIsNull(specDatas.xt60Num) ? specDatas.xt60Num : this.state.xt60Num,
                        // })

                        this.state.batteryProtectRangeMin = parseInt(specDatas.batteryProtectRangeMin) >= 0 ? specDatas.batteryProtectRangeMin : this.state.batteryProtectRangeMin
                        this.state.batteryProtectRangeMax = !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? specDatas.batteryProtectRangeMax : this.state.batteryProtectRangeMax
                        this._setOutputValue(
                            !Helper.dataIsNull(specDatas.dc12vOutputPower) ? specDatas.dc12vOutputPower : this.state.dc12vOutputPower,
                            !Helper.dataIsNull(specDatas.houseCarPower) ? specDatas.houseCarPower : this.state.houseCarPower,
                            !Helper.dataIsNull(specDatas.wirelessOutputPower) ? specDatas.wirelessOutputPower : this.state.wirelessOutputPower,
                            !Helper.dataIsNull(specDatas.usbA1Power) ? specDatas.usbA1Power : this.state.usbA1Power,
                            !Helper.dataIsNull(specDatas.usbA2Power) ? specDatas.usbA2Power : this.state.usbA2Power,
                            !Helper.dataIsNull(specDatas.usbA3Power) ? specDatas.usbA3Power : this.state.usbA3Power,
                            !Helper.dataIsNull(specDatas.usbA4Power) ? specDatas.usbA4Power : this.state.usbA4Power,
                            !Helper.dataIsNull(specDatas.typeC1Power) ? specDatas.typeC1Power : this.state.typeC1Power,
                            !Helper.dataIsNull(specDatas.typeC2Power) ? specDatas.typeC2Power : this.state.typeC2Power,
                            !Helper.dataIsNull(specDatas.acOutputTotalPower) ? specDatas.acOutputTotalPower : this.state.outputACPower)

                        var tempErrorArray = []
                        var error176_3 = !Helper.dataIsNull(specDatas.error176_3) ? specDatas.error176_3 : false
                        var error168_10 = !Helper.dataIsNull(specDatas.error168_10) ? specDatas.error168_10 : false
                        var error170_4 = !Helper.dataIsNull(specDatas.error170_4) ? specDatas.error170_4 : false
                        var error170_5 = !Helper.dataIsNull(specDatas.error170_5) ? specDatas.error170_5 : false
                        var error170_8 = !Helper.dataIsNull(specDatas.error170_8) ? specDatas.error170_8 : false
                        var error170_9 = !Helper.dataIsNull(specDatas.error170_9) ? specDatas.error170_9 : false
                        var error170_10 = !Helper.dataIsNull(specDatas.error170_10) ? specDatas.error170_10 : false
                        var error170_11 = !Helper.dataIsNull(specDatas.error170_11) ? specDatas.error170_11 : false
                        var error170_3 = !Helper.dataIsNull(specDatas.error170_3) ? specDatas.error170_3 : false


                        if (error176_3) {
                            tempErrorArray.push(Helper.type_error_176_3)
                        }
                        if (error168_10) {
                            tempErrorArray.push(Helper.type_error_168_10)
                        }
                        if (error170_4) {
                            tempErrorArray.push(Helper.type_error_170_4)
                        }
                        if (error170_5) {
                            tempErrorArray.push(Helper.type_error_170_5)
                        }
                        if (error170_8 || error170_9 || error170_10 || error170_11) {
                            tempErrorArray.push(Helper.type_error_170_8)
                        }
                        if (error170_3) {
                            tempErrorArray.push(Helper.type_error_170_3)
                        }

                        if (JSON.stringify(this.state.errorArray) != JSON.stringify(tempErrorArray)) {
                            this.setState({errorArray: tempErrorArray})
                            this._showErrorTipsDialog(tempErrorArray)
                        }
                    }
                }


                if (new Date().getTime() - this.state.getLineTime > 1000 * 60 * 10) {
                    this._getSolarInputPowerLog(StorageHelper.getTempToken(), deviceId)
                }

                if (parseInt(this.state.routesLength) == 3) {
                    //发送请求到的设备属性数据
                    EventUtil.sendEvent(EventUtil.DEVICE_PROPS, res.info)
                }
                if (showLoading && this.state.isWifiOnline) {
                    //开始轮询
                    this._timerGetDeviceProps()
                }
            })
            .catch((error) => {
                //this._stopTimerGetDeviceProps()
            })

        //查询设备状态  会在事件EventUtil.AWS_DEVICE_SHADOW_KEY中回调
        //NativeModules.RNModule.queryDevStatus(mac);
    }

    _timerGetDeviceProps() {
        this._stopTimerGetDeviceProps()
        this.intervalGetDeviceProps = setInterval(() => {
            this._getDeviceProps(this.state.deviceId, this.state.mac, false, true)
        }, 4000)
    }

    _stopTimerGetDeviceProps() {
        if (this.intervalGetDeviceProps) {
            clearInterval(this.intervalGetDeviceProps)
            this.intervalGetDeviceProps = null
        }
    }

    _setOutputValue(dc12vOutputPower, houseCarPower, wirelessOutputPower,
                    usbA1Power, usbA2Power, usbA3Power, usbA4Power, typeC1Power, typeC2Power,
                    acOutputTotalPower) {
        //dcAll=12v直流输出功率+无线充输出总功率+房车RV接口输出功率
        var dcAll = parseInt(this._getPowerTruthValue(dc12vOutputPower)) + parseInt(this._getPowerTruthValue(houseCarPower)) + parseInt(this._getPowerTruthValue(wirelessOutputPower))
        //dc12VAllValue =4个USBA+2个USBC+dcAll
        var dc12VAllValue = parseInt(this._getPowerTruthValue(usbA1Power)) + parseInt(this._getPowerTruthValue(usbA2Power)) + parseInt(this._getPowerTruthValue(usbA3Power)) +
            parseInt(this._getPowerTruthValue(usbA4Power)) + parseInt(this._getPowerTruthValue(typeC1Power)) + parseInt(this._getPowerTruthValue(typeC2Power)) + parseInt(dcAll)
        var outputAllValue = parseInt(this._getPowerTruthValue(acOutputTotalPower)) + dc12VAllValue
        if (this.state.dcAll != dcAll) {
            this.setState({
                dcAll: dcAll,
            })
        }
        if (this.state.dc12VAllValue != dc12VAllValue) {
            this.setState({
                dc12VAllValue: dc12VAllValue,
            })
        }
        if (this.state.outputAllValue != outputAllValue) {
            this.setState({
                outputAllValue: outputAllValue
            })
        }
    }

    _getPowerTruthValue(power) {
        return !Helper.dataIsNull(power) ? power : 0
    }

    _canGetSpecData(time) {
        var flag = true
        var curTime = new Date().getTime()
        if (curTime - time < specDelayTime) {
            flag = false
        }

        return flag
    }

    _eventListenerAdd() {
        if (Platform.OS === 'android') {
            this.awsDeviceStatusListListener = DeviceEventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                LogUtil.debugLog('设备状态回调:' + JSON.stringify(value))
                //收到属性变化，重新请求获取属性接口
                //this._getDeviceProps(this.state.deviceId, this.state.mac, false)
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            eventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                LogUtil.debugLog('设备状态回调:' + JSON.stringify(value))
                //收到属性变化，重新请求获取属性接口
                //this._getDeviceProps(this.state.deviceId, this.state.mac, false)
            });
        }

        this.updateDeviceNameListener = DeviceEventEmitter.addListener(EventUtil.UPDATE_DEVICE_NAME_SUCCESS, (value) => {
            //修改设备名称成功，收到通知
            if (value) {
                this._setNarTitle(value)
                this.setState({deviceName: value})
            }
        });

        this.appStateListener = AppState.addEventListener('change', (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === 'active' && this.state.isWifiOnline) { // 后台进入前台
                this._timerGetDeviceProps()

                if (this.state.bleConnectStatus == 1) {
                    if (Platform.OS === 'android') {
                        this._androidOpenGps()
                    } else {
                        this._ble()
                    }
                }
            } else {
                this._stopTimerGetDeviceProps()
            }
        });// 监听APP状态  前台、后台

        this.toLoginListener = DeviceEventEmitter.addListener(EventUtil.TO_LOGIN, (value) => {
            this._stopTimerGetDeviceProps()
        });
        this.deviceOfflineListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_OFFLINE, (res) => {
            if (res) {
                var did = res.deviceId
                if (did == this.state.deviceId) {
                    LogUtil.debugLog('offline======设备详情=====>设备离线：did' + did + ',deviceId:' + this.state.deviceId)
                    this.setState({
                        isOnline: false,
                        isWifiOnline: false,
                    })
                }
            }
        });
        this.deviceOnlineListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_ONLINE, (res) => {
            if (res) {
                var did = res.deviceId
                if (did == this.state.deviceId) {
                    LogUtil.debugLog('online======设备详情=====>设备上线：did' + did + ',deviceId:' + this.state.deviceId)
                    this.setState({
                        isOnline: true,
                        isWifiOnline: true,
                    })
                    this._getDeviceProps(this.state.deviceId, this.state.mac, false, true)
                }
            }
        });
        this.routesListener = DeviceEventEmitter.addListener(EventUtil.ROUTES_LENGTH, (message) => {
            // 监听路由长度
            this.setState({
                routesLength: message,
            });
        });
    }

    //设置顶部标题
    _setNarTitle(value) {
        this.props.navigation.setParams({title: value});
    }

    _eventListenerRemove() {
        this.awsDeviceStatusListListener && this.awsDeviceStatusListListener.remove()
        this.updateDeviceNameListener && this.updateDeviceNameListener.remove()
        this.appStateListener && this.appStateListener.remove()
        this.toLoginListener && this.toLoginListener.remove()
        this.deviceOfflineListener && this.deviceOfflineListener.remove()
        this.deviceOnlineListener && this.deviceOnlineListener.remove()
        this.routesListener && this.routesListener.remove()
    }


    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#F5F5F5',
                    alignItems: 'center'
                }}>
                <TitleBar
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        this.props.navigation.goBack();
                    }}
                    backgroundColor={'#F5F5F5'}
                    rightIcon={require('../../resources/setting_ic.png')}
                    rightIconClick={() => {
                        //设置
                        that._toDeviceSetting()
                    }}
                    titleText={this.props.navigation.state.params.title ? this.props.navigation.state.params.title : ''}
                />
                {/*{this._subTitleView()}*/}

                <ScrollView
                    showsVerticalScrollIndicator={false}>
                    {this._topView()}
                    {this._tabView()}
                    {this._topView1()}
                    {this._mainView()}
                </ScrollView>

                {this._dialogView()}
                {this._tipsDialogView()}
            </View>
        );
    }

    _tipsDialogView() {
        return (
            <View>
                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({bleTipsDialog: false})
                    }}
                    modalVisible={this.state.bleTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this._setBleConnectStatus(0)
                        this.setState({bleTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        if (Platform.OS === 'android') {
                            Bluetooth._enableBluetooth()
                            this.setState({onBleTime: new Date().getTime()})
                        } else {
                            //ios跳转系统设置
                            GXRNManager.openSystemSetting()
                        }
                        this.setState({bleTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({gpsTipsDialog: false})
                    }}
                    modalVisible={this.state.gpsTipsDialog}
                    title={strings('warming_tips')}
                    message={strings('open_gps_tips')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this._setBleConnectStatus(0)
                        this.setState({gpsTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        NativeModules.RNModule.openGps()
                        this.setState({gpsTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    leftBtnTextColor={'#000000'}
                    rightBtnTextColor={'#38579D'}
                    onRequestClose={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({locationTipsDialog: false})
                    }}
                    modalVisible={this.state.locationTipsDialog}
                    title={strings('location_title_tips')}
                    message={strings('location_content')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({locationTipsDialog: false})
                    }}
                    rightBtnText={strings('confirm')}
                    onRightBtnClick={() => {
                        //确认
                        this.requestPermission()
                        this.setState({locationTipsDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

            </View>
        )
    }

    /*
    *弹窗
     */
    _dialogView() {
        return (
            <View>
                <MessageDialog
                    onRequestClose={() => {
                        this.setState({deviceOffLineDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({deviceOffLineDialog: false})
                    }}
                    modalVisible={this.state.deviceOffLineDialog}
                    title={strings('device_off_line')}
                    message={
                        strings('device_off_line_tips1') + '\n'
                        + strings('device_off_line_tips2') + '\n'
                        + strings('device_off_line_tips3') + '\n'
                        + strings('device_off_line_tips4')}
                    leftBtnText={strings('ok')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({deviceOffLineDialog: false})
                    }}
                />

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({deviceLowElcDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({deviceLowElcDialog: false})
                    }}
                    modalVisible={this.state.deviceLowElcDialog}
                    title={strings('warming')}
                    message={
                        strings('device_low_elc_tips')}
                    leftBtnText={strings('ok')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({deviceLowElcDialog: false})
                    }}/>

                <DetailLightControlDialog
                    closeClickEvent={() => {
                        this.setState({lightControlDialog: false})
                    }}
                    onRequestClose={() => {
                        this.setState({lightControlDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({lightControlDialog: false})
                    }}
                    modalVisible={this.state.lightControlDialog}

                    brightness={parseInt(this.state.brightness)}
                    onValueChange={(value) => {
                        //屏幕亮度滑动条回调
                        this.setState({brightness: value})
                    }}
                    onSlidingComplete={(value) => {
                        //屏幕亮度滑动条回调
                        this._sendScreen(value)
                    }}
                    lightBrightness={parseInt(this.state.lightBrightness)}
                    onLightBrightnessValueChange={(value) => {
                        //灯带亮度滑动条回调
                        this.setState({lightBrightness: value})
                    }}
                    onLightBrightnessSlidingComplete={(value) => {
                        //灯带亮度滑动条回调
                        this._sendLight(value)
                    }}
                    topLightSwitchValue={parseInt(this.state.topLight) != 255}
                    topLightSwitchClick={() => {
                        //顶灯开关回调
                        if (parseInt(this.state.topLight) == 255 && parseInt(this.state.bottomLight) == 255) {
                            // ①当顶灯和底灯均关闭时，单击其一开关开灯，默认下发休闲模式至对应灯亮起；
                            this._sendTopLight(0)
                        } else if (parseInt(this.state.topLight) == 255 && parseInt(this.state.bottomLight) != 255) {
                            // ②当其一灯以模式A亮起，另一灯为关闭状态下，单击另一灯开关开灯，下发模式A至另一灯亮起；
                            this._sendTopLight(this.state.bottomLight)
                        } else if (parseInt(this.state.topLight) != 255) {
                            // ③当灯开启时，单击开关按钮向对应灯下发关灯指令。
                            this._sendTopLight(255)
                        }
                    }}
                    bottomLightSwitchValue={parseInt(this.state.bottomLight) != 255}
                    bottomLightSwitchClick={() => {
                        //底灯开关回调
                        if (parseInt(this.state.bottomLight) == 255 && parseInt(this.state.topLight) == 255) {
                            // ①当顶灯和底灯均关闭时，单击其一开关开灯，默认下发休闲模式至对应灯亮起；
                            this._sendBottomLight(0)
                        } else if (parseInt(this.state.bottomLight) == 255 && parseInt(this.state.topLight) != 255) {
                            // ②当其一灯以模式A亮起，另一灯为关闭状态下，单击另一灯开关开灯，下发模式A至另一灯亮起；
                            this._sendBottomLight(this.state.topLight)
                        } else if (parseInt(this.state.bottomLight) != 255) {
                            // ③当灯开启时，单击开关按钮向对应灯下发关灯指令。
                            this._sendBottomLight(255)
                        }
                    }}
                    lightSwitchValue={parseInt(this.state.topLight) != 255 && parseInt(this.state.bottomLight) != 255}
                    lightSwitchClick={() => {
                        if ((parseInt(this.state.bottomLight) == 255 && parseInt(this.state.topLight) == 255)
                            || (parseInt(this.state.bottomLight) != 255 && parseInt(this.state.topLight) == 255)
                            || (parseInt(this.state.bottomLight) == 255 && parseInt(this.state.topLight) != 255)) {
                            //开
                            var value = 0
                            SpecUtil._sendTopBottomLight(this.state.account, this.state.pk, this.state.mac, value, value)
                            this.setState({
                                moodLight: value,
                            })
                            this.setState({
                                topLight: value,
                                bottomLight: value,
                                sendTopBottomLightTime: new Date().getTime()
                            })

                        } else {
                            //关
                            var value = 255
                            SpecUtil._sendTopBottomLight(this.state.account, this.state.pk, this.state.mac, value, value)
                            this.setState({
                                moodLight: value,
                            })
                            this.setState({
                                topLight: value,
                                bottomLight: value,
                                sendTopBottomLightTime: new Date().getTime()
                            })
                        }
                    }}
                    topLight={this.state.topLight}
                    bottomLight={this.state.bottomLight}


                    moodLight={parseInt(this.state.moodLight)}
                    onMoodLightEvent={(value) => {
                        //氛围灯点击事件回调

                        // ④顶灯和底灯均关闭状态下，模式不可点击；


                        if (parseInt(this.state.bottomLight) != 255 && parseInt(this.state.topLight) != 255) {
                            // ②顶灯和底灯同时亮起状态下，单击模式可向顶灯和底灯同时下发对应模式；
                            // this._sendTopLight(value)
                            // setTimeout(() => {
                            //     this._sendBottomLight(value)
                            // }, 500);

                            SpecUtil._sendTopBottomLight(this.state.account, this.state.pk, this.state.mac, value, value)
                            this.setState({
                                moodLight: (value == 255 && this.state.bottomLight == 255) ? 255 : (value == 255 ? this.state.moodLight : value),
                            })
                            this.setState({
                                moodLight: (value == 255 && this.state.topLight == 255) ? 255 : (value == 255 ? this.state.moodLight : value),
                            })
                            this.setState({
                                topLight: value,
                                bottomLight: value,
                                sendTopBottomLightTime: new Date().getTime()
                            })
                        } else if (parseInt(this.state.topLight) != 255 && parseInt(this.state.bottomLight) == 255) {
                            // ③顶灯和底灯其一亮起状态下，单击模式仅向开启的灯下发对应模式；
                            this._sendTopLight(value)
                        } else if (parseInt(this.state.topLight) == 255 && parseInt(this.state.bottomLight) != 255) {
                            // ③顶灯和底灯其一亮起状态下，单击模式仅向开启的灯下发对应模式；
                            this._sendBottomLight(value)
                        }
                        // if (parseInt(this.state.topBottomLight) == 0) {
                        //     //底灯
                        //     if (parseInt(value) != this.state.bottomLight) {
                        //         SpecUtil._sendBottomLight(this.state.account, this.state.pk, this.state.mac, value)
                        //         this.setState({bottomLight: value})
                        //     } else {
                        //         SpecUtil._sendBottomLight(this.state.account, this.state.pk, this.state.mac, 255)
                        //         this.setState({
                        //             bottomLight: 255,
                        //             moodLight: 255
                        //         })
                        //     }
                        // } else {
                        //     //顶灯
                        //     if (parseInt(value) != this.state.topLight) {
                        //         SpecUtil._sendLcdLight(this.state.account, this.state.pk, this.state.mac, value)
                        //         this.setState({topLight: value})
                        //     } else {
                        //         SpecUtil._sendLcdLight(this.state.account, this.state.pk, this.state.mac, 255)
                        //         this.setState({
                        //             topLight: 255,
                        //             moodLight: 255
                        //         })
                        //     }
                        // }
                    }}


                    // topBottomLight={parseInt(this.state.topBottomLight)}
                    // onMoodLightTopBottomEvent={(value) => {
                    //     //底灯、顶灯点击事件回调
                    //     this.setState({topBottomLight: value})
                    //
                    //     if (parseInt(value) == 0) {
                    //         //底灯
                    //         this.setState({moodLight: this.state.bottomLight})
                    //     } else {
                    //         //顶灯
                    //         this.setState({moodLight: this.state.topLight})
                    //     }
                    // }}
                />

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({errorTipsDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({errorTipsDialog: false})
                    }}
                    modalVisible={this.state.errorTipsDialog}
                    title={this.state.errorTitle}
                    message={this.state.errorTips}
                    leftBtnText={strings('ok')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({errorTipsDialog: false})
                        this._showErrorTipsDialog(this.state.errorArray)
                    }}/>
            </View>
        )
    }

    _sendScreen(value) {
        this.setState({
            brightness: value,
            sendScreenTime: new Date().getTime(),
        })
        SpecUtil._sendScreenLight(this.state.account, this.state.pk, this.state.mac, value)
    }

    _sendLight(value) {
        this.setState({
            lightBrightness: value,
            sendLightTime: new Date().getTime(),
        })
        SpecUtil._sendLightBrightness(this.state.account, this.state.pk, this.state.mac, value)
    }

    _sendTopLight(value) {
        SpecUtil._sendLcdLight(this.state.account, this.state.pk, this.state.mac, value)
        this.setState({
            moodLight: (value == 255 && this.state.bottomLight == 255) ? 255 : (value == 255 ? this.state.moodLight : value),
            topLight: value,
            sendTopBottomLightTime: new Date().getTime(),
        })
    }

    _sendBottomLight(value) {
        SpecUtil._sendBottomLight(this.state.account, this.state.pk, this.state.mac, value)
        this.setState({
            moodLight: (value == 255 && this.state.topLight == 255) ? 255 : (value == 255 ? this.state.moodLight : value),
            bottomLight: value,
            sendTopBottomLightTime: new Date().getTime()
        })
    }

    _setModeLight(topLight, bottomLight) {
        if (!this._canGetSpecData(this.state.sendTopBottomLightTime)) {
            return
        }
        // if (topLight != 255 && bottomLight != 255) {
        //     this.setState({moodLight: topLight})
        // } else if (topLight != 255) {
        //     this.setState({moodLight: topLight})
        // } else if (bottomLight != 255) {
        //     this.setState({moodLight: bottomLight})
        // }
        var value = (topLight == 255 && bottomLight == 255) ? 255 : (topLight == 255 ? bottomLight : topLight)
        if (this.state.moodLight != value) {
            this.setState({
                moodLight: value,
            })
        }
    }

    _showErrorTipsDialog(errorArray) {
        if (!this.state.isWifiOnline) {
            return
        }
        if (errorArray.length > 0) {
            var value = parseInt(errorArray[0]) == Helper.type_error_170_4 ? this.state.batteryProtectRangeMax
                : parseInt(errorArray[0]) == Helper.type_error_170_3 ? this.state.batteryProtectRangeMin
                    : ''
            this.setState({
                errorTitle: Helper.getTextErrorTitle(errorArray[0]),
                errorTips: Helper.getTextErrorTips(errorArray[0], value),
                errorTipsDialog: true,
            })
            var tempErrorArray = []
            this.state.errorArray.map((item, index) => {
                if (index != 0) {
                    tempErrorArray.push(item)
                }
            })
            this.setState({errorArray: tempErrorArray})
        }
    }

    _subTitleView() {
        return (
            <CommonTextView
                textSize={14}
                style={{
                    color: '#9A9DA4'
                }}
                text={this.state.deviceType}/>
        )
    }

    _topView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 37,
                }}>

                <AnimatedCircularProgress
                    size={this.state.progressViewSize}
                    width={16}
                    fill={100}
                    prefill={100}
                    tintColor="#000"
                    backgroundColor="#E3E3E3"
                    linecap="round"
                    leftFill={this.state.isWifiOnline ? (this.state.tempValue != null && this.state.tempValue != undefined ? this._getTempValueToCircular(this.state.tempUnit, this.state.tempValue) : 0) : 0}
                    rightFill={this.state.isWifiOnline ? (parseInt(this.state.elcValue) > 0 ? this.state.elcValue : 0) : 0}
                >
                </AnimatedCircularProgress>


                <Image
                    style={{
                        width: this.state.topBgWH,
                        height: this.state.topBgWH,
                        position: 'absolute',
                    }}
                    source={require('../../resources/detail_circle_img.png')}/>
            </View>
        )
    }

    _getTempValueToCircular(tempUnit, tempValue) {
        if (!this.state.getDataFlag) {
            return 0
        }
        var sMinTemp = -10
        var sMaxTemp = 60
        var hMinTemp = Helper.getFTempVlaue(-10)
        var hMaxTemp = Helper.getFTempVlaue(60)

        var totalTemp = (sMaxTemp - sMinTemp)
        var ttValue = tempUnit == 1 ? Helper.getFTempVlaue(tempValue) : tempValue
        var tempTempValue = 0
        if (parseInt(ttValue) < sMinTemp) {
            tempTempValue = sMinTemp
        } else if (parseInt(ttValue) > sMaxTemp) {
            tempTempValue = sMaxTemp
        } else {
            tempTempValue = parseInt(ttValue)
        }
        tempTempValue = tempTempValue + 10

        return parseInt((tempTempValue / totalTemp) * 100)
    }

    _topView1() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 37,
                    position: 'absolute',
                }}>

                <View
                    style={{
                        width: this.state.progressViewSize,
                        height: this.state.progressViewSize
                    }}/>

                <Image
                    style={{
                        width: this.state.topBgWH,
                        height: this.state.topBgWH,
                        position: 'absolute',
                    }}
                    source={require('../../resources/detail_dev_panel_img.png')}/>

                <View
                    style={{
                        width: this.state.topBgWH,
                        height: this.state.topBgWH,
                        position: 'absolute',
                    }}>
                    <CommonTextView
                        textSize={12}
                        text={'H'}
                        style={{
                            color: '#B0B7C3',
                            position: 'absolute',
                            left: 5,
                        }}/>

                    <CommonTextView
                        textSize={12}
                        text={'C'}
                        style={{
                            color: '#B0B7C3',
                            position: 'absolute',
                            left: 5,
                            bottom: 0
                        }}/>

                    <CommonTextView
                        textSize={12}
                        text={'F'}
                        style={{
                            color: '#B0B7C3',
                            position: 'absolute',
                            right: 5
                        }}/>

                    <CommonTextView
                        textSize={12}
                        text={'E'}
                        style={{
                            color: '#B0B7C3',
                            position: 'absolute',
                            right: 5,
                            bottom: 0
                        }}/>
                </View>

                {this._topCenterView()}
                {this._tempElcValue()}


                {this._newLightBtnView()}
                {/*蓝牙相关注释*/}
                {/*{!this.state.isWifiOnline && (this.state.bleConnectStatus == 0 || this.state.bleConnectStatus == 1) ? this._bleConnectBtnView() : null}*/}
            </View>
        )
    }

    _topCenterView() {
        return (
            <View
                style={{
                    width: this.state.topBgWH,
                    height: this.state.topBgWH,
                    position: 'absolute',
                    //alignItems: 'center',
                    //justifyContent: 'center',
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: this.state.topBgWH * 0.21,
                        paddingRight: this.state.topBgWH * 0.18,
                    }}>
                    {ViewHelper.getFlexView()}
                    <Image
                        style={{
                            width: this.state.topBgWH * 0.097,
                            height: this.state.topBgWH * 0.131
                        }}
                        source={
                            this.state.tabIndex == 0 ?
                                this.state.isWifiOnline ? (this.state.inputTotalPower > 0 ? require('../../resources/detail_charge_ic.png') : require('../../resources/detail_charge_off_ic.png')) : require('../../resources/detail_charge_off_ic.png')
                                :
                                this.state.isWifiOnline ? (this.state.outputAllValue > 0 ? require('../../resources/detail_charge_ic.png') : require('../../resources/detail_charge_off_ic.png')) : require('../../resources/detail_charge_off_ic.png')
                        }/>
                    <Image
                        style={{
                            width: this.state.topBgWH * 0.154,
                            height: this.state.topBgWH * 0.131
                        }}
                        source={this.state.isWifiConnect ? (this.state.isWifiOnline ? require('../../resources/detail_wifi_ic.png') : require('../../resources/detail_wifi_offline_ic.png'))
                            :
                            this.state.bleConnectStatus == 2 ? require('../../resources/detail_bluetooth_ic.png')
                                : require('../../resources/detail_wifi_offline_ic.png')}/>
                </View>

                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: this.state.topBgWH * 0.1,
                    }}>
                    <CommonTextView
                        textSize={I18n.locale == 'zh' ? 11 : 10}
                        style={{
                            color: '#fff'
                        }}
                        text={this.state.isOnline ? (this.state.tabIndex == 0 ?
                            strings('fully_charge_after') :
                            strings('available_after'))
                            : strings('device_is')}
                    />

                    <CommonTextView
                        textSize={I18n.locale == 'zh' ? 16 : 22}
                        style={{
                            color: '#fff',
                            marginTop: 3
                        }}
                        text={this.state.isOnline ? (this.state.tabIndex == 0 ?
                            parseInt(this.state.chargeRemindTime) == 0 || this.state.inputTotalPower == 0 ? strings('hour_min_value')('-- ', ' -- ') : Helper.dateCount(this.state.chargeRemindTime) :
                            parseInt(this.state.disChargeRemindTime) == 0 || this.state.outputAllValue == 0 ? strings('hour_min_value')('-- ', ' -- ') : Helper.dateCount(this.state.disChargeRemindTime))
                            : strings('offline')}
                    />
                </View>

            </View>
        )
    }


    /*
    *温度，电量  图标、文案
     */
    _tempElcValue() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    position: 'absolute',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingLeft: this.mScreenWidth * 0.029,
                    paddingRight: this.mScreenWidth * 0.05
                }}>
                <Image
                    style={{
                        width: this.state.topBgWH * 0.114,
                        height: this.state.topBgWH * 0.114
                    }}
                    source={require('../../resources/detail_temp_ic.png')}/>

                <CommonTextView
                    textSize={16}
                    style={{
                        color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                        marginLeft: this.mScreenWidth * 0.01
                    }}
                    text={(this.state.isWifiOnline ? (this.state.getDataFlag ?
                        (this.state.tempUnit == 1 ? Helper.getFTempVlaue(this.state.tempValue) : this.state.tempValue) : 0) : '- ')
                    + (this.state.tempUnit == 2 ? '℃' : '℉')}/>

                {ViewHelper.getFlexView()}

                <CommonTextView
                    textSize={16}
                    style={{
                        color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                        marginRight: this.mScreenWidth * 0.01
                    }}
                    text={(this.state.isWifiOnline ? this.state.elcValue : '- ') + '%'}/>

                <Image
                    style={{
                        width: this.state.topBgWH * 0.114,
                        height: this.state.topBgWH * 0.114
                    }}
                    source={require('../../resources/detail_battery_ic.png')}/>
            </View>
        )
    }

    _getTempValue(tempValue, localTempUnit, tempUnit) {
        var tempData = 0
        if (parseInt(localTempUnit) == 0) {
            //显示摄氏度
            if (parseInt(tempUnit) == 1) {
                //设备的温度是摄氏度
                tempData = tempValue
            } else {
                //设备的温度是华氏度
                tempData = Helper.getSTempVlaue(tempValue)
            }
        } else {
            //显示华氏度
            if (parseInt(tempUnit) == 1) {
                //设备的温度是摄氏度
                tempData = Helper.getFTempVlaue(tempValue)
            } else {
                //设备的温度是华氏度
                tempData = tempValue
            }
        }
        return tempData
    }


    /*
    *灯光按钮
     */
    _lightBtnView() {
        return (
            <View
                style={{
                    position: 'absolute',
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    bottom: (this.state.topBgWH * 0.33) / 4,
                    //backgroundColor: 'rgba(0,0,0,0.3)',
                    height: this.state.topBgWH * 0.22
                }}>
                <View
                    style={{
                        height: this.state.topBgWH * 0.22,
                        backgroundColor: '#39589D',
                        width: 2 / PixelRatio.get(),
                        position: 'absolute'
                    }}/>
                <TouchableOpacity
                    onPress={() => {
                        //灯光按钮
                        if (!this.state.isWifiOnline)
                            return

                        this.setState({lightControlDialog: true})
                    }}>
                    <Image
                        style={{
                            width: this.state.topBgWH * 0.33,
                            height: (this.state.topBgWH * 0.33) / 2
                        }}
                        source={require('../../resources/detail_light_btn.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    _newLightBtnView() {
        return (
            <View
                style={{
                    position: 'absolute',
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    bottom: -13
                }}>
                <TouchableOpacity
                    onPress={() => {
                        //灯光按钮
                        if (!this.state.isWifiOnline) {
                            this.onShowToast(strings('device_offline_enable_control'))
                            return
                        }

                        this.setState({lightControlDialog: true})
                    }}>
                    <Image
                        source={require('../../resources/detail_light_btn.png')}/>
                </TouchableOpacity>
            </View>
        )
    }

    _bleConnectBtnView() {
        return (
            <View
                style={{
                    position: 'absolute',
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    top: 0
                }}>
                <TouchableOpacity
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 60,
                        height: 28,
                        backgroundColor: '#38579D',
                        borderRadius: 14
                    }}
                    onPress={() => {
                        //蓝牙连接
                        if (this.state.bleConnectStatus == 0)
                            this._startConnectBle()
                    }}>
                    {
                        this.state.showBleIcon ?
                            <Image
                                source={require('../../resources/detail_bluetooth_ic.png')}/>
                            : null
                    }
                </TouchableOpacity>
            </View>
        )
    }

    _startBleIconAnimated() {
        this._stopBleIconAnimated()
        this.intervalBleIcon = setInterval(() => {
            this.setState({
                showBleIcon: !this.state.showBleIcon
            })
        }, iconFlashTime)
    }

    _stopBleIconAnimated() {
        if (this.intervalBleIcon) {
            clearInterval(this.intervalBleIcon)
            this.intervalBleIcon = null
            this.setState({
                showBleIcon: true
            })
        }
    }

    _startScanTimer() {
        this._stopScanTimer()
        this.intervalScanTime30 = setInterval(() => {
            this._stopScanTimer()
            this._bleConnectFail()
            if (Platform.OS != "android") {
                GXRNManager.stopScanDevice();
            } else {
                NativeModules.RNBLEModule.stopScanDevice()
            }
            BluetoothManager._bleLog('=========>stopScan======>30')
        }, 30000)
    }

    _stopScanTimer() {
        if (this.intervalScanTime30) {
            clearInterval(this.intervalScanTime30)
            this.intervalScanTime30 = null
        }
    }

    _tabView() {
        return (
            <View
                style={{
                    marginTop: -75
                }}>
                {this._newTabLineView()}
                {this._tabBtnView()}
            </View>
        )
    }

    _tabLineView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: -(this.state.topBgWH * 0.33) / 4,
                    //backgroundColor: '#ccc'
                }}>
                <View
                    style={{
                        height: 40,
                        backgroundColor: '#39589D',
                        width: 2 / PixelRatio.get(),
                    }}/>
                <View
                    style={{
                        flexDirection: 'row',
                        width: this.mScreenWidth * 0.53,
                    }}>
                    <View
                        style={{
                            flex: 1
                        }}>
                        <View
                            style={{
                                height: 2 / PixelRatio.get(),
                                backgroundColor: this.state.tabIndex == 0 ? '#39589D' : '#D4D4D4',
                                width: (this.mScreenWidth * 0.53) / 2
                            }}/>
                        <View
                            style={{
                                width: 2 / PixelRatio.get(),
                                height: 20,
                                backgroundColor: this.state.tabIndex == 0 ? '#39589D' : '#D4D4D4',
                            }}/>
                    </View>

                    <View
                        style={{
                            flex: 1
                        }}>
                        <View
                            style={{
                                height: 2 / PixelRatio.get(),
                                backgroundColor: this.state.tabIndex == 1 ? '#39589D' : '#D4D4D4',
                                width: (this.mScreenWidth * 0.53) / 2
                            }}/>
                        <View
                            style={{
                                width: 2 / PixelRatio.get(),
                                height: 20,
                                backgroundColor: this.state.tabIndex == 1 ? '#39589D' : '#D4D4D4',
                                position: 'absolute',
                                right: 0
                            }}/>
                    </View>
                </View>
            </View>
        )
    }

    _tabBtnView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 80,
                    flexDirection: 'row',
                    marginLeft: 40,
                    height: 68,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 68 / 2,
                    backgroundColor: '#EBEBEB',
                    paddingLeft: 9,
                    paddingRight: 9,
                    marginTop: -5
                }}>
                {this._tabBtnItemView(strings('input'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.inputTotalPower) : '- ') + powerUnit, this.state.tabIndex == 0, () => {
                    this.setState({tabIndex: 0})
                })}

                {this._tabBtnItemView(strings('output'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.outputAllValue) : '- ') + powerUnit, this.state.tabIndex == 1, () => {
                    this.setState({tabIndex: 1})
                })}
            </View>
        )
    }

    _newTabLineView() {
        return (
            <View
                style={{
                    alignItems: 'center'
                }}>
                {
                    !this.state.isWifiOnline ?
                        this.state.tabIndex == 0 ?
                            <ImageBackground
                                style={{
                                    width: this.mScreenWidth * 0.53,
                                    height: this._getHeightByWH(gifImgWidth, gifImgHeight, this.mScreenWidth * 0.53),
                                    resizeMode: 'contain',
                                }}
                                source={require('../../resources/detail_input_offline.png')}/>
                            :
                            <ImageBackground
                                style={{
                                    width: this.mScreenWidth * 0.53,
                                    height: this._getHeightByWH(gifImgWidth, gifImgHeight, this.mScreenWidth * 0.53),
                                    resizeMode: 'contain',
                                }}
                                source={require('../../resources/detail_output_offline.png')}/>
                        :
                        this.state.tabIndex == 0 ?
                            this.state.inputTotalPower != 0 ?
                                <Image
                                    style={{
                                        width: this.mScreenWidth * 0.53,
                                        height: this._getHeightByWH(gifImgWidth, gifImgHeight, this.mScreenWidth * 0.53),
                                        resizeMode: 'contain',
                                    }}
                                    source={require('../../resources/detail_input.gif')}/>
                                :
                                <ImageBackground
                                    style={{
                                        width: this.mScreenWidth * 0.53,
                                        height: this._getHeightByWH(gifImgWidth, gifImgHeight, this.mScreenWidth * 0.53),
                                        resizeMode: 'contain',
                                    }}
                                    source={require('../../resources/detail_input_offline.png')}/>
                            :
                            this.state.outputAllValue != 0 ?
                                <Image
                                    style={{
                                        width: this.mScreenWidth * 0.53,
                                        height: this._getHeightByWH(gifImgWidth, gifImgHeight, this.mScreenWidth * 0.53),
                                        resizeMode: 'contain',
                                    }}
                                    source={require('../../resources/detail_output.gif')}/>
                                :
                                <ImageBackground
                                    style={{
                                        width: this.mScreenWidth * 0.53,
                                        height: this._getHeightByWH(gifImgWidth, gifImgHeight, this.mScreenWidth * 0.53),
                                        resizeMode: 'contain',
                                    }}
                                    source={require('../../resources/detail_output_offline.png')}/>
                }
            </View>
        )
    }

    _tabBtnItemView(text, value, isCheck, clickEvent) {
        return (
            <TouchableOpacity
                onPress={clickEvent}
                style={{
                    flex: 1,
                    height: 54,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: isCheck ? '#FFFFFF' : 'rgba(0,0,0,0)',
                    borderRadius: 54 / 2,
                    elevation: isCheck ? (Platform.OS === 'android' ? 5 : 1) : 0.0001, // 设置阴影角度，通过这个设置有无阴影（这个是最重要的，决定有没有阴影）
                    shadowColor: isCheck ? '#000' : 'rgba(0,0,0,0)', // 阴影颜色
                    shadowOffset: {width: 0, height: 0}, // 阴影偏移
                    shadowOpacity: Platform.OS === 'android' ? 1 : 0.1, // 阴影不透明度
                    shadowRadius: (54 / 2) + 2, // 圆角
                }}>

                <CommonTextView
                    textSize={12}
                    style={{
                        color: '#000000',
                    }}
                    text={text}/>

                <CommonTextView
                    textSize={18}
                    style={{
                        color: this.state.isWifiOnline ? (isCheck ? '#39589D' : '#9A9DA4') : '#9A9DA4',
                        marginTop: 0
                    }}
                    text={value}/>
            </TouchableOpacity>
        )
    }

    _mainView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 19
                }}>
                {this.state.tabIndex == 0 ?
                    this._inputView() : this._outputView()}
            </View>
        )
    }

    ////////////////////////////////////////////输入视图////////////////////////////////
    _inputView() {
        return (
            <View>
                {this._inputSolarView()}
                {this._inputWindView()}
                {this._inputACView()}
                {/*{this._inputBatView()}*/}
            </View>
        )
    }

    //输入太阳能
    _inputSolarView() {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                    marginBottom: 11
                }}>
                {this._titleView(require('../../resources/solar_ic.png'), strings('solar_energy'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.inputPvPower) : '- '), powerUnit)}
                {this.state.isWifiOnline && this.state.pvChartDataArray.length > 0 ? this._solarChartView() : null}
            </ShadowCardView>
        )
    }

    //输入风能
    _inputWindView() {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                    marginBottom: 11
                }}>
                {this._titleView(require('../../resources/wind_ic.png'), strings('wind_energy'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.inputFanPower) : '- '), powerUnit)}
                {this.state.isWifiOnline && this.state.windChartDataArray.length > 0 ? this._windChartView() : null}
            </ShadowCardView>
        )
    }

    //输入ac
    _inputACView() {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                    marginBottom: 11
                }}>
                {this._titleView(require('../../resources/ac_ic.png'), strings('ac'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.acInverterPower) : '- '), powerUnit)}
            </ShadowCardView>
        )
    }

    //输入加电包
    _inputBatView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginBottom: 29
                }}>

                {this._batItemView(strings('bat01'), (this.state.isWifiOnline ?
                    parseFloat(this.state.package1BatteryPower) > 0 ? Math.abs(this.state.package1BatteryPower) : 0
                    : '- '), (this.state.isWifiOnline ?
                    this.state.package1ElcRemind
                    : '- '))}
                <View
                    style={{
                        width: 10,
                        height: 1
                    }}/>
                {this._batItemView(strings('bat02'), (this.state.isWifiOnline ?
                    parseFloat(this.state.package2BatteryPower) > 0 ? Math.abs(this.state.package2BatteryPower) : 0
                    : '- '), (this.state.isWifiOnline ?
                    this.state.package2ElcRemind
                    : '- '))}
            </View>
        )
    }

    ////////////////////////////////////////////输入视图////////////////////////////////

    ////////////////////////////////////////////输出视图////////////////////////////////
    _outputView() {
        return (
            <View>
                {this._outputACView()}
                {this._outputDCView()}
                {/*{this._outputInterfaceView()}*/}
                {/*{this._outputBatView()}*/}
            </View>
        )
    }

    //输出ac
    _outputACView() {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                    marginBottom: 11
                }}>
                {this._titleView(require('../../resources/ac_ic.png'), strings('ac'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.outputACPower) : '- '), powerUnit, (this.state.isWifiOnline ? this.state.outputACPowerSwitch : false), () => {
                    //交流电输出开关
                    if (!this.state.isWifiOnline) {
                        return
                    }

                    this.setState({
                        outputACPowerSwitch: !this.state.outputACPowerSwitch,
                        outputACPowerSwitchTime: new Date().getTime()
                    })
                    SpecUtil._sendAcOutputPowerSwitch(this.state.account, this.state.pk, this.state.mac, !this.state.outputACPowerSwitch)
                })}

                {this.state.isWifiOnline && this.state.acChartDataArray.length > 0 ? this._acChartView() : null}
            </ShadowCardView>
        )
    }

    //输出dc
    _outputDCView() {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                    marginBottom: 11
                }}>
                {this._titleView(require('../../resources/dc_ic.png'), strings('dc_12'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.dc12VAllValue) : '- '), powerUnit, (this.state.isWifiOnline ? this.state.outputDCPowerSwitch : false), () => {
                    //直流输出总开关
                    if (!this.state.isWifiOnline) {
                        return
                    }

                    this.setState({
                        outputDCPowerSwitch: !this.state.outputDCPowerSwitch,
                        outputDCPowerSwitchTime: new Date().getTime()
                    })
                    SpecUtil._sendDcOutputPowerSwitch(this.state.account, this.state.pk, this.state.mac, !this.state.outputDCPowerSwitch)
                })}

                {this._newOutputInterfaceView()}
            </ShadowCardView>
        )
    }

    _newOutputInterfaceView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: this.mScreenWidth - 90,
                        height: 2 / PixelRatio.get(),
                        backgroundColor: '#DDDDDD'
                    }}/>

                <View
                    style={{
                        width: this.mScreenWidth - 56,
                        flexDirection: 'row',
                        marginTop: 12,
                        marginBottom: 20,
                        paddingLeft: 17
                    }}>
                    <View>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginBottom: 20
                            }}>
                            {this._newOutputInterfaceItemView(require('../../resources/typec_ic.png'), strings('type_c1'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.typeC1Power) : '- '))}
                            {this._newOutputInterfaceItemView(require('../../resources/typec_ic.png'), strings('type_c2'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.typeC2Power) : '- '))}
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                marginBottom: 20
                            }}>
                            {this._newOutputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a1'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.usbA2Power) : '- '))}
                            {this._newOutputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a2'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.usbA3Power) : '- '))}
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                            }}>
                            {this._newOutputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a3'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.usbA1Power) : '- '))}
                            {this._newOutputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a4'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.usbA4Power) : '- '))}
                        </View>
                    </View>

                    <View
                        style={{
                            width: 2 / PixelRatio.get(),
                            height: '100%',
                            backgroundColor: '#DDDDDD',
                            marginBottom: 20
                        }}/>

                    {/*{ViewHelper.getFlexView()}*/}

                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center'
                        }}>
                        <Image
                            onLayout={(event) => this.onLayout(event)}
                            style={{
                                resizeMode: 'contain'
                            }}
                            source={this.state.isWifiOnline ? (this.state.outputDCPowerSwitch ? require('../../resources/dc_on_ic.png') : require('../../resources/dc_off_ic.png'))
                                : require('../../resources/dc_off_ic.png')}/>
                        {ViewHelper.getFlexView()}
                        <View
                            style={{
                                width: this.state.dcAllImgWidth
                            }}>
                            {this._newOutputInterfaceItemView(null, strings('dc_all'), (this.state.isWifiOnline ? Helper.dataNanReset(this.state.dcAll) : '- '))}
                        </View>
                    </View>
                </View>


            </View>
        )
    }

    onLayout = (event) => { // 获取View的高度
        const viewWidth = event.nativeEvent.layout.width;
        this.setState({dcAllImgWidth: viewWidth});
    }


    _newOutputInterfaceItemView(icon, text, value) {
        return (
            <View
                style={{
                    width: (this.mScreenWidth - 146) / 3,
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                    {icon ?
                        <Image
                            source={icon}/> : null}


                    <CommonTextView
                        textSize={12}
                        style={{
                            color: '#9A9DA4',
                            marginLeft: icon ? 4 : 0,
                        }}
                        text={text}/>
                </View>


                <View
                    style={{
                        flexDirection: 'row',
                    }}>
                    <CommonTextView
                        textSize={20}
                        style={{
                            color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                        }}
                        text={value}/>

                    <View>
                        {ViewHelper.getFlexView()}
                        <CommonTextView
                            textSize={12}
                            style={{
                                color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                                marginBottom: 3
                            }}
                            text={powerUnit}/>
                    </View>
                </View>
            </View>
        )
    }

    //输出加电包
    _outputBatView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginBottom: 29
                }}>

                {this._batItemView(strings('bat01'), (this.state.isWifiOnline ?
                    parseFloat(this.state.package1BatteryPower) < 0 ? Math.abs(this.state.package1BatteryPower) : 0
                    : '- '), (this.state.isWifiOnline ?
                    this.state.package1ElcRemind
                    : '- '))}
                <View
                    style={{
                        width: 10,
                        height: 1
                    }}/>
                {this._batItemView(strings('bat02'), (this.state.isWifiOnline ?
                    parseFloat(this.state.package2BatteryPower) < 0 ? Math.abs(this.state.package2BatteryPower) : 0
                    : '- '), (this.state.isWifiOnline ?
                    this.state.package2ElcRemind
                    : '- '))}
            </View>
        )
    }

    _outputInterfaceView() {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth - 56,
                    alignItems: 'center',
                    marginBottom: 11,
                    padding: 15
                }}>
                <View
                    style={{
                        flexDirection: 'row'
                    }}>
                    {this._outputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a1'), (this.state.isWifiOnline ? this.state.usbA1Power : '- '))}
                    {ViewHelper.getFlexView()}
                    {this._outputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a2'), (this.state.isWifiOnline ? this.state.usbA2Power : '- '))}
                    {ViewHelper.getFlexView()}
                    {this._outputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a3'), (this.state.isWifiOnline ? this.state.usbA3Power : '- '))}
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 20
                    }}>
                    {this._outputInterfaceItemView(require('../../resources/usb_ic.png'), strings('usb_a4'), (this.state.isWifiOnline ? this.state.usbA4Power : '- '))}
                    {ViewHelper.getFlexView()}
                    {this._outputInterfaceItemView(require('../../resources/typec_ic.png'), strings('type_c1'), (this.state.isWifiOnline ? this.state.typeC1Power : '- '))}
                    {ViewHelper.getFlexView()}
                    {this._outputInterfaceItemView(require('../../resources/typec_ic.png'), strings('type_c2'), (this.state.isWifiOnline ? this.state.typeC2Power : '- '))}
                </View>
            </ShadowCardView>
        )
    }

    _outputInterfaceItemView(icon, text, value) {
        return (
            <View
                style={{
                    width: (this.mScreenWidth - 146) / 3,
                    //backgroundColor: '#ccc'
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                    <Image
                        source={icon}/>

                    <CommonTextView
                        textSize={12}
                        style={{
                            color: '#9A9DA4',
                            marginLeft: 4,
                        }}
                        text={text}/>
                </View>


                <View
                    style={{
                        flexDirection: 'row',
                        marginTop: 10
                    }}>
                    <CommonTextView
                        textSize={20}
                        style={{
                            color: '#000000',
                        }}
                        text={value}/>

                    <View>
                        {ViewHelper.getFlexView()}
                        <CommonTextView
                            textSize={12}
                            style={{
                                color: '#000000',
                                marginBottom: 3
                            }}
                            text={powerUnit}/>
                    </View>
                </View>
            </View>
        )
    }

    ////////////////////////////////////////////输出视图////////////////////////////////

    _titleView(icon, text, value, unit, switchValue, onCheck) {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    width: this.mScreenWidth - 80,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 40
                }}>
                <Image
                    source={icon}/>

                <CommonTextView
                    textSize={12}
                    style={{
                        color: '#9A9DA4',
                        marginLeft: 9,
                    }}
                    text={text}/>

                {ViewHelper.getFlexView()}

                <View
                    style={{
                        flexDirection: 'row'
                    }}>
                    <CommonTextView
                        textSize={20}
                        style={{
                            color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                        }}
                        text={value}/>

                    <View>
                        {ViewHelper.getFlexView()}
                        <CommonTextView
                            textSize={12}
                            style={{
                                color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                                marginBottom: 3
                            }}
                            text={unit}/>
                    </View>
                </View>

                {
                    onCheck ?
                        <SwitchView
                            isCheck={switchValue}
                            onCheck={onCheck}
                            style={{
                                marginLeft: 17,
                                backgroundColor: switchValue ? '#15AEC6' : '#D9DCE1',
                            }}/>
                        : null
                }
            </View>
        )
    }

    //充电包子视图
    _batItemView(text, power, pec) {
        return (
            <ShadowCardView
                style={{
                    width: (this.mScreenWidth - 66) / 2,
                    paddingTop: 9,
                    paddingLeft: 13,
                    paddingRight: 13,
                    paddingBottom: 13
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                    <Image
                        source={require('../../resources/bat_ic.png')}/>

                    <CommonTextView
                        textSize={12}
                        style={{
                            color: '#9A9DA4',
                            marginLeft: 9,
                        }}
                        text={text}/>
                </View>

                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 13
                    }}>

                    <View
                        style={{
                            flexDirection: 'row'
                        }}>
                        <CommonTextView
                            textSize={20}
                            style={{
                                color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                            }}
                            text={power}/>

                        <View>
                            {ViewHelper.getFlexView()}
                            <CommonTextView
                                textSize={12}
                                style={{
                                    color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                                    marginBottom: 3
                                }}
                                text={powerUnit}/>
                        </View>
                    </View>

                    {ViewHelper.getFlexView()}

                    <CommonTextView
                        textSize={12}
                        style={{
                            color: this.state.isWifiOnline ? '#000000' : '#9A9DA4',
                            marginRight: 9
                        }}
                        text={pec + '%'}/>

                    <Image
                        source={require('../../resources/detail_battery_grey_ic.png')}/>
                </View>
            </ShadowCardView>
        )
    }


    _solarChartView() {
        return (
            <ChartView
                scaleAble={true}
                yAllArray={this.state.pvChartYAllArray}
                data={this.state.pvChartDataArray}
                lineColor={'#FF8748'}
            />
        )
    }

    _windChartView() {
        return (
            <ChartView
                scaleAble={true}
                yAllArray={this.state.windChartYAllArray}
                data={this.state.windChartDataArray}
                lineColor={'#3A599E'}
            />
        )
    }

    _acChartView() {
        return (
            <ChartView
                scaleAble={true}
                yAllArray={this.state.acChartYAllArray}
                data={this.state.acChartDataArray}
                lineColor={'#15AEC6'}
            />
        )
    }

    //获取太阳能输入功率日志
    _getSolarInputPowerLog(token, deviceId) {
        // if (this.state.pvChartDataArray.length > 0)
        //     return
        this._getDeviceSpecLog(token, deviceId, SpecUtil.key_pv_input_power)
    }

    //获取风能输入功率日志
    _getWindInputPowerLog(token, deviceId) {
        // if (this.state.windChartDataArray.length > 0)
        //     return
        this._getDeviceSpecLog(token, deviceId, SpecUtil.key_wind_elc_input_power)
    }

    //获取交流电输出功率日志
    _getAcOutputPowerLog(token, deviceId) {
        // if (this.state.acChartDataArray.length > 0)
        //     return
        this._getDeviceSpecLog(token, deviceId, SpecUtil.key_ac_output_total_power)
    }

    _getDeviceSpecLog(token, deviceId, attribute) {
        var endTime = parseInt(new Date().getTime())
        var startTime = parseInt(endTime - (3600 * 24 * 1000))
        // NetUtil.getDeviceSpecLog(token, deviceId, attribute, null, parseInt(startTime / 1000), parseInt(endTime / 1000), false)
        //     .then((res) => {
        //         var tempChartData = this._resolveChartData(res.info)
        //         if (attribute == SpecUtil.key_pv_input_power) {
        //             //获取太阳能属性日志返回
        //             this._getWindInputPowerLog(token, deviceId)
        //
        //             this.setState({
        //                 pvChartYAllArray: tempChartData[0],
        //                 pvChartDataArray: tempChartData[1],
        //             })
        //         } else if (attribute == SpecUtil.key_wind_elc_input_power) {
        //             //获取风能属性日志返回
        //             this._getAcOutputPowerLog(token, deviceId)
        //
        //             this.setState({
        //                 windChartYAllArray: tempChartData[0],
        //                 windChartDataArray: tempChartData[1],
        //             })
        //         } else if (attribute == SpecUtil.key_ac_output_total_power) {
        //             //获取交流电属性日志返回
        //             this.setState({
        //                 acChartYAllArray: tempChartData[0],
        //                 acChartDataArray: tempChartData[1],
        //             })
        //
        //         }
        //     })
        //     .catch((error) => {
        //
        //     })

        //7CDFA1D0BC40
        NetUtil.getLineData(token, '', deviceId, attribute, false)
            .then((res) => {
                var tempChartData = this._resolveChartData(res.info)
                if (attribute == SpecUtil.key_pv_input_power) {
                    //获取太阳能属性日志返回
                    this._getWindInputPowerLog(token, deviceId)

                    this.setState({
                        pvChartYAllArray: tempChartData[0],
                        pvChartDataArray: tempChartData[1],
                    })
                } else if (attribute == SpecUtil.key_wind_elc_input_power) {
                    //获取风能属性日志返回
                    this._getAcOutputPowerLog(token, deviceId)

                    this.setState({
                        windChartYAllArray: tempChartData[0],
                        windChartDataArray: tempChartData[1],
                    })
                } else if (attribute == SpecUtil.key_ac_output_total_power) {
                    //获取交流电属性日志返回
                    this.setState({
                        acChartYAllArray: tempChartData[0],
                        acChartDataArray: tempChartData[1],
                        getLineTime: new Date().getTime()
                    })
                }
            }).catch((error) => {
            this.setState({
                getLineTime: 0
            })
        })
    }

    //解析折线图数据
    _resolveChartData(resInfo) {
        var tempYAllArray = []
        var tempDataArray = []

        if (resInfo) {
            var resultArray = resInfo

            if (resultArray && resultArray.length > 0) {
                resultArray.map((item, index) => {
                    tempYAllArray.push({
                        timestamp: item.time,
                        time: ''
                    })
                    tempDataArray.push({
                        value: this._resolveObjectData(item),
                        time: item.time
                    })
                })
            }
        }

        return [tempYAllArray, tempDataArray]
    }

    _resolveObjectData(value) {
        var resultValue = 0
        Object.keys(value).map((key, index) => {
            var specValue = value[key]

            if (key == SpecUtil.key_pv_input_power || key == SpecUtil.key_wind_elc_input_power || key == SpecUtil.key_ac_output_total_power) {
                resultValue = specValue
            }
        });


        return resultValue
    }

    //蓝牙相关事件
    _bleEvent() {
        SpecUtil.initBleProps(false)
        this._bleListener()
        //this._bleDataResolve('00740020000100210002002200000023000400240005002A000000300011003100120032001300330014003400000035000000360017003700180038001900390000003A0000003B00010040002100410022004200230043002400440025004500260046002700470028004800290049002A004A002B004B002C004C002D004D002E004E002F004F00300080003100810032008200330083003400840035008500360086003700870038008800390089003A008A003B008B003C008C003D008D003E008E003F008F00400090004100910042009200430093004400940045009500460096004700970048009800490099004A009A004B009B004C009C004D009D004E009E004F009F00500050002E00510034005200530053005400540055005500560056005700570058005800590059005A005A005B005B005C005C005D005D005E005E005F00630000006400000065000000660000006700000068000000690000006A0000006B006C006C006D006D006E006E006F006F0070007000710072007300730074007400750075007600760077007700780078007A0079007B007A007C007B007D007C007E007D007F007E0081007F008200A0008300A1008400A3017C00A4000000A5000000A6000000B70000')
        //this._bleDataResolve('00740020000100210002002200000023000400240005002A000000300011003100120032001300330014003400000035000000360017003700180038001900390000003A0000003B003C0040002100410022004200230043002400440025004500260046002700470028004800290049002A004A002B004B002C004C002D004D002E004E002F004F00300080003100810032008200330083003400840035008500360086003700870038008800390089003A008A003B008B003C008C003D008D003E008E003F008F00400090004100910042009200430093004400940045009500460096004700970048009800490099004A009A004B009B004C009C004D009D004E009E004F009F00500050000600510034005200530053005400540055005500560056005700570058005800590059005A005A005B005B005C005C005D005D005E005E005F0063000500640000006500000066000000670000006800000069000F006A0050006B006C006C006D006D006E006E006F006F0070007000710072007300730074007400750075007600760077007700780078007A0079007B007A007C007B007D007C007E007D007F007E0081007F008200A0008300A1008400A3017C00A4000000A5000000A6505000B70000')
    }

    _bleListener() {
        //蓝牙状态监听
        this.updateStateListener = BluetoothManager.addListener(
            "BleManagerDidUpdateState",
            this.handleUpdateState
        );


        if (Platform.OS === 'android') {
            //监听蓝牙数据上报
            this.bleNotifyDataListener = DeviceEventEmitter.addListener(EventUtil.BLE_NOTIFY_DATA, (res) => {
                BluetoothManager._bleLog("蓝牙数据上报:" + JSON.stringify(res));
                this._bleDataResolve(res.data)
            });
            //监听蓝牙通知状态
            this.bleNotifyListener = DeviceEventEmitter.addListener(EventUtil.BLE_NOTIFY_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙通知状态:" + JSON.stringify(res));
                if (res.status == 'onNotifySuccess') {
                    BluetoothManager._bleLog("onNotifySuccess蓝牙通知成功");
                } else if (res.status == 'onNotifyFailure') {
                    BluetoothManager._bleLog("onNotifyFailure蓝牙通知失败");
                }
            });
            //监听蓝牙连接状态
            this.bleConnectListener = DeviceEventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(res));
                if (res.status == 'onStartConnect') {
                    BluetoothManager._bleLog("onStartConnect开始连接");
                } else if (res.status == 'onConnectFail') {
                    BluetoothManager._bleLog("onConnectFail连接失败");
                    this._bleConnectFail()
                } else if (res.status == 'onConnectSuccess') {
                    BluetoothManager._bleLog("onConnectSuccess连接成功");
                    this._bleSuccessSetData(this.state.mac)
                } else if (res.status == 'onDisConnected') {
                    BluetoothManager._bleLog("onDisConnected连接中断");
                    this._bleConnectFail()
                }
            });
            //监听扫描到符合规则的蓝牙设备
            this.bleScanListener = DeviceEventEmitter.addListener(EventUtil.BLE_SCAN, (res) => {
                BluetoothManager._bleLog("扫描到的蓝牙设备:" + JSON.stringify(res));
                this._setFindData(res)
            });
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            //监听蓝牙数据上报
            eventEmitter.addListener(EventUtil.BLE_NOTIFY_DATA, (res) => {
                BluetoothManager._bleLog("蓝牙数据上报:" + JSON.stringify(res));
                this._bleDataResolve(res)
            });
            //监听蓝牙通知状态
            eventEmitter.addListener(EventUtil.BLE_NOTIFY_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙通知状态:" + JSON.stringify(res));
                if (res.status == 'onNotifySuccess') {
                    BluetoothManager._bleLog("onNotifySuccess蓝牙通知成功");
                } else if (res.status == 'onNotifyFailure') {
                    BluetoothManager._bleLog("onNotifyFailure蓝牙通知失败");
                }
            });
            //监听蓝牙连接状态
            eventEmitter.addListener(EventUtil.BLE_CONNECT_STATUS, (res) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(res));
                if (res) {
                    BluetoothManager._bleLog("onConnectSuccess连接成功");
                    this._bleSuccessSetData(this.state.mac)
                } else {
                    BluetoothManager._bleLog("onConnectFail连接失败");
                    this._bleConnectFail()
                }
            });
            eventEmitter.addListener(EventUtil.BLE_SCAN, (res) => {
                BluetoothManager._bleLog("iOS扫描到的蓝牙设备:" + JSON.stringify(res));
                this._setFindData(res)
            });
        }
    }


    _bleConnectFail() {
        if (this.state.bleConnectStatus == 1) {
            //手动触发连接失败
            this._connectBleFailStop()
        } else {
            this._setBleConnectStatus(0)
            this._startConnectBle()
        }
        this.setState({
            isOnline: false,
            isWifiOnline: false,
            mac: this.state.mac,
            getDataFlag: false,
            isWifiConnect: false,
        })
        this._stopScanTimer()
    }

    _bleDestory() {
        this.bleScanListener && this.bleScanListener.remove();
        this.updateStateListener && this.updateStateListener.remove();
        this.bleConnectListener && this.bleConnectListener.remove();
        this.bleNotifyListener && this.bleNotifyListener.remove();
        this.bleNotifyDataListener && this.bleNotifyDataListener.remove();
        if (!this.state.isWifiConnect) {
            if (Platform.OS === "android") {
                NativeModules.RNBLEModule.disConnectDevice()
            } else {
                GXRNManager.disConnectDevice();
            }
        }
    }

    //蓝牙状态改变
    handleUpdateState = (args) => {
        BluetoothManager._bleLog("蓝牙状态改变:" + JSON.stringify(args));
        BluetoothManager.bluetoothState = args.state;
        if (args.state == "on") {
        } else if (args.state == "off") {
            this._bleConnectFail()
        }

        if (this.state.bleConnectStatus == 1) {
            if (args.state == "on") {
                //蓝牙打开时自动搜索
                if (Platform.OS === 'android') {
                    this._androidOpenGps()
                } else {
                    this._ble()
                }
            } else if (args.state == "off") {
                //蓝牙未打开
                if (new Date().getTime() - this.state.onBleTime > 5000) {
                    this.setState({bleTipsDialog: true})
                }
            }
        }
    };

    _bleDataResolve(hexData) {
        try {
            if (hexData == '' || hexData == null || hexData == undefined) {
                return
            }
            var resInfo = Bluetooth._hex2Json(hexData)
            var specDatas = SpecUtil.resolveSpecs(resInfo, false)
            LogUtil.debugLog('BleModule_log_解析:' + JSON.stringify(resInfo))

            if (this._canGetSpecData(this.state.outputACPowerSwitchTime)) {
                if (!Helper.dataIsNull(specDatas.acOutputSwitch)) {
                    this.setState({
                        outputACPowerSwitch: specDatas.acOutputSwitch,
                    })
                }
            }
            if (this._canGetSpecData(this.state.outputDCPowerSwitchTime)) {
                if (!Helper.dataIsNull(specDatas.dcOutputSwitch)) {
                    this.setState({
                        outputDCPowerSwitch: specDatas.dcOutputSwitch,
                    })
                }
            }
            if (this._canGetSpecData(this.state.sendScreenTime)) {
                this.setState({
                    brightness: !Helper.dataIsNull(specDatas.deviceScreenLight) ? specDatas.deviceScreenLight : this.state.brightness,
                })
            }
            if (this._canGetSpecData(this.state.sendLightTime)) {
                this.setState({
                    lightBrightness: !Helper.dataIsNull(specDatas.lightSet) ? specDatas.lightSet : this.state.lightBrightness,
                })
            }
            var tempTopLight = !Helper.dataIsNull(specDatas.topLight) ? specDatas.topLight : this.state.topLight
            var tempBottomLight = !Helper.dataIsNull(specDatas.bottomLight) ? specDatas.bottomLight : this.state.bottomLight
            if (this._canGetSpecData(this.state.sendTopBottomLightTime)) {
                this.setState({
                    topLight: tempTopLight,
                })
            }
            if (this._canGetSpecData(this.state.sendTopBottomLightTime)) {
                this.setState({
                    bottomLight: tempBottomLight,
                })
            }
            this._setModeLight(tempTopLight, tempBottomLight)

            this.setState({
                acInverterPower: !Helper.dataIsNull(specDatas.inverterPower) ? specDatas.inverterPower : this.state.acInverterPower,
                tempValue: !Helper.dataIsNull(specDatas.enirTemp) ? specDatas.enirTemp : this.state.tempValue,
                elcValue: !Helper.dataIsNull(specDatas.curRemaining) ? specDatas.curRemaining : this.state.elcValue,
                inputPvPower: !Helper.dataIsNull(specDatas.pvInputPower) ? specDatas.pvInputPower : this.state.inputPvPower,
                inputFanPower: !Helper.dataIsNull(specDatas.fanInputPower) ? specDatas.fanInputPower : this.state.inputFanPower,
                inputAcPower: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.inputAcPower,
                outputACPower: !Helper.dataIsNull(specDatas.acOutputTotalPower) ? specDatas.acOutputTotalPower : this.state.outputACPower,
                outputDCPower: !Helper.dataIsNull(specDatas.dcOutputPower) ? specDatas.dcOutputPower : this.state.outputDCPower,
                usbA1Power: !Helper.dataIsNull(specDatas.usbA1Power) ? specDatas.usbA1Power : this.state.usbA1Power,
                usbA2Power: !Helper.dataIsNull(specDatas.usbA2Power) ? specDatas.usbA2Power : this.state.usbA2Power,
                usbA3Power: !Helper.dataIsNull(specDatas.usbA3Power) ? specDatas.usbA3Power : this.state.usbA3Power,
                usbA4Power: !Helper.dataIsNull(specDatas.usbA4Power) ? specDatas.usbA4Power : this.state.usbA4Power,
                typeC1Power: !Helper.dataIsNull(specDatas.typeC1Power) ? specDatas.typeC1Power : this.state.typeC1Power,
                typeC2Power: !Helper.dataIsNull(specDatas.typeC2Power) ? specDatas.typeC2Power : this.state.typeC2Power,

                beeSwitch: !Helper.dataIsNull(specDatas.beeSwitch) ? specDatas.beeSwitch : this.state.beeSwitch,
                acChargePower: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePower,
                pvChargePower: parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePower,
                fanChargePower: parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePower,
                deviceStandByTime: parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandByTime,
                screenOffTime: parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTime,
                acStandByTime: parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandByTime,

                chargeRemindTime: !Helper.dataIsNull(specDatas.chargeRemindTime) ? specDatas.chargeRemindTime : this.state.chargeRemindTime,
                disChargeRemindTime: !Helper.dataIsNull(specDatas.disChargeRemindTime) ? specDatas.disChargeRemindTime : this.state.disChargeRemindTime,

                inputTotalPower: !Helper.dataIsNull(specDatas.inputTotalPower) ? specDatas.inputTotalPower : this.state.inputTotalPower,
                outputTotalPower: !Helper.dataIsNull(specDatas.outputTotalPower) ? specDatas.outputTotalPower : this.state.outputTotalPower,

                package1BatteryPower: !Helper.dataIsNull(specDatas.packInputPower1) ? specDatas.packInputPower1 : this.state.package1BatteryPower,
                package1ElcRemind: !Helper.dataIsNull(specDatas.packRemind1) ? specDatas.packRemind1 : this.state.package1ElcRemind,
                package2BatteryPower: !Helper.dataIsNull(specDatas.packInputPower2) ? specDatas.packInputPower2 : this.state.package2BatteryPower,
                package2ElcRemind: !Helper.dataIsNull(specDatas.packRemind2) ? specDatas.packRemind2 : this.state.package2ElcRemind,

                batteryProtectRangeMin: parseInt(specDatas.batteryProtectRangeMin) >= 0 ? specDatas.batteryProtectRangeMin : this.state.batteryProtectRangeMin,
                batteryProtectRangeMax: !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? specDatas.batteryProtectRangeMax : this.state.batteryProtectRangeMax,

                chargeStatus: !Helper.dataIsNull(specDatas.chargeStatus) ? specDatas.chargeStatus : this.state.chargeStatus,


                // capacity: !Helper.dataIsNull(specDatas.capacity) ? specDatas.capacity : this.state.capacity,
                // weight1: !Helper.dataIsNull(specDatas.weight1) ? specDatas.weight1 : this.state.weight1,
                // weight2: !Helper.dataIsNull(specDatas.weight2) ? specDatas.weight2 : this.state.weight2,
                // cycleCharge1: !Helper.dataIsNull(specDatas.cycleCharge1) ? specDatas.cycleCharge1 : this.state.cycleCharge1,
                // cycleCharge2: !Helper.dataIsNull(specDatas.cycleCharge2) ? specDatas.cycleCharge2 : this.state.cycleCharge2,
                // chargeTempMin: !Helper.dataIsNull(specDatas.chargeTempMin) ? specDatas.chargeTempMin : this.state.chargeTempMin,
                // chargeTempMax: !Helper.dataIsNull(specDatas.chargeTempMax) ? specDatas.chargeTempMax : this.state.chargeTempMax,
                // chargeTempT: !Helper.dataIsNull(specDatas.chargeTempT) ? specDatas.chargeTempT : this.state.chargeTempT,
                // disChargeTempMin: !Helper.dataIsNull(specDatas.disChargeTempMin) ? specDatas.disChargeTempMin : this.state.disChargeTempMin,
                // disChargeTempMax: !Helper.dataIsNull(specDatas.disChargeTempMax) ? specDatas.disChargeTempMax : this.state.disChargeTempMax,
                // disChargeTempT: !Helper.dataIsNull(specDatas.disChargeTempT) ? specDatas.disChargeTempT : this.state.disChargeTempT,
                // inputAcMin: !Helper.dataIsNull(specDatas.inputAcMin) ? specDatas.inputAcMin : this.state.inputAcMin,
                // inputAcMax: !Helper.dataIsNull(specDatas.inputAcMax) ? specDatas.inputAcMax : this.state.inputAcMax,
                // inputPvMax: !Helper.dataIsNull(specDatas.inputPvMax) ? specDatas.inputPvMax : this.state.inputPvMax,
                // inputWindElcMax: !Helper.dataIsNull(specDatas.inputWindElcMax) ? specDatas.inputWindElcMax : this.state.inputWindElcMax,
                // batteryPackNum: !Helper.dataIsNull(specDatas.batteryPackNum) ? specDatas.batteryPackNum : this.state.batteryPackNum,
                // acOutputMaxPower: !Helper.dataIsNull(specDatas.acOutputMaxPower) ? specDatas.acOutputMaxPower : this.state.acOutputMaxPower,
                // socket20ANum: !Helper.dataIsNull(specDatas.socket20ANum) ? specDatas.socket20ANum : this.state.socket20ANum,
                // socket30ANum: !Helper.dataIsNull(specDatas.socket30ANum) ? specDatas.socket30ANum : this.state.socket30ANum,
                // dcOutputMaxPower: !Helper.dataIsNull(specDatas.dcOutputMaxPower) ? specDatas.dcOutputMaxPower : this.state.dcOutputMaxPower,
                // usbANum: !Helper.dataIsNull(specDatas.usbANum) ? specDatas.usbANum : this.state.usbANum,
                // usbCNum: !Helper.dataIsNull(specDatas.usbCNum) ? specDatas.usbCNum : this.state.usbCNum,
                // wirelessChargeNum: !Helper.dataIsNull(specDatas.wirelessChargeNum) ? specDatas.wirelessChargeNum : this.state.wirelessChargeNum,
                // cigarette12vNum: !Helper.dataIsNull(specDatas.cigarette12vNum) ? specDatas.cigarette12vNum : this.state.cigarette12vNum,
                // dc12VNum: !Helper.dataIsNull(specDatas.dc12VNum) ? specDatas.dc12VNum : this.state.dc12VNum,
                // xt60Num: !Helper.dataIsNull(specDatas.xt60Num) ? specDatas.xt60Num : this.state.xt60Num,

                dc12vOutputPower: !Helper.dataIsNull(specDatas.dc12vOutputPower) ? specDatas.dc12vOutputPower : this.state.dc12vOutputPower,
                houseCarPower: !Helper.dataIsNull(specDatas.houseCarPower) ? specDatas.houseCarPower : this.state.houseCarPower,
                wirelessOutputPower: !Helper.dataIsNull(specDatas.wirelessOutputPower) ? specDatas.wirelessOutputPower : this.state.wirelessOutputPower,

                getDataFlag: true,
            })
            this.state.batteryProtectRangeMin = parseInt(specDatas.batteryProtectRangeMin) >= 0 ? specDatas.batteryProtectRangeMin : this.state.batteryProtectRangeMin
            this.state.batteryProtectRangeMax = !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? specDatas.batteryProtectRangeMax : this.state.batteryProtectRangeMax
            this._setOutputValue(
                !Helper.dataIsNull(specDatas.dc12vOutputPower) ? specDatas.dc12vOutputPower : this.state.dc12vOutputPower,
                !Helper.dataIsNull(specDatas.houseCarPower) ? specDatas.houseCarPower : this.state.houseCarPower,
                !Helper.dataIsNull(specDatas.wirelessOutputPower) ? specDatas.wirelessOutputPower : this.state.wirelessOutputPower,
                !Helper.dataIsNull(specDatas.usbA1Power) ? specDatas.usbA1Power : this.state.usbA1Power,
                !Helper.dataIsNull(specDatas.usbA2Power) ? specDatas.usbA2Power : this.state.usbA2Power,
                !Helper.dataIsNull(specDatas.usbA3Power) ? specDatas.usbA3Power : this.state.usbA3Power,
                !Helper.dataIsNull(specDatas.usbA4Power) ? specDatas.usbA4Power : this.state.usbA4Power,
                !Helper.dataIsNull(specDatas.typeC1Power) ? specDatas.typeC1Power : this.state.typeC1Power,
                !Helper.dataIsNull(specDatas.typeC2Power) ? specDatas.typeC2Power : this.state.typeC2Power,
                !Helper.dataIsNull(specDatas.acOutputTotalPower) ? specDatas.acOutputTotalPower : this.state.outputACPower)

            var tempErrorArray = []
            var error176_3 = !Helper.dataIsNull(specDatas.error176_3) ? specDatas.error176_3 : false
            var error168_10 = !Helper.dataIsNull(specDatas.error168_10) ? specDatas.error168_10 : false
            var error170_4 = !Helper.dataIsNull(specDatas.error170_4) ? specDatas.error170_4 : false
            var error170_5 = !Helper.dataIsNull(specDatas.error170_5) ? specDatas.error170_5 : false
            var error170_8 = !Helper.dataIsNull(specDatas.error170_8) ? specDatas.error170_8 : false
            var error170_9 = !Helper.dataIsNull(specDatas.error170_9) ? specDatas.error170_9 : false
            var error170_10 = !Helper.dataIsNull(specDatas.error170_10) ? specDatas.error170_10 : false
            var error170_11 = !Helper.dataIsNull(specDatas.error170_11) ? specDatas.error170_11 : false
            var error170_3 = !Helper.dataIsNull(specDatas.error170_3) ? specDatas.error170_3 : false


            if (error176_3) {
                tempErrorArray.push(Helper.type_error_176_3)
            }
            if (error168_10) {
                tempErrorArray.push(Helper.type_error_168_10)
            }
            if (error170_4) {
                tempErrorArray.push(Helper.type_error_170_4)
            }
            if (error170_5) {
                tempErrorArray.push(Helper.type_error_170_5)
            }
            if (error170_8 || error170_9 || error170_10 || error170_11) {
                tempErrorArray.push(Helper.type_error_170_8)
            }
            if (error170_3) {
                tempErrorArray.push(Helper.type_error_170_3)
            }
            this.setState({errorArray: tempErrorArray})
            this._showErrorTipsDialog(tempErrorArray)
        } catch (e) {
            LogUtil.debugLog('_bleDataResolve_error:' + JSON.stringify(e))
        }
    }


    _resolveSpecData(resInfo) {
        var specDatas = SpecUtil.resolveSpecs(resInfo, false)

        if (this._canGetSpecData(this.state.outputACPowerSwitchTime)) {
            if (!Helper.dataIsNull(specDatas.acOutputSwitch)) {
                this.setState({
                    outputACPowerSwitch: specDatas.acOutputSwitch,
                })
            }
        }
        if (this._canGetSpecData(this.state.outputDCPowerSwitchTime)) {
            if (!Helper.dataIsNull(specDatas.dcOutputSwitch)) {
                this.setState({
                    outputDCPowerSwitch: specDatas.dcOutputSwitch,
                })
            }
        }
        if (this._canGetSpecData(this.state.sendScreenTime)) {
            this.setState({
                brightness: !Helper.dataIsNull(specDatas.deviceScreenLight) ? specDatas.deviceScreenLight : this.state.brightness,
            })
        }
        if (this._canGetSpecData(this.state.sendLightTime)) {
            this.setState({
                lightBrightness: !Helper.dataIsNull(specDatas.lightSet) ? specDatas.lightSet : this.state.lightBrightness,
            })
        }
        var tempTopLight = !Helper.dataIsNull(specDatas.topLight) ? specDatas.topLight : this.state.topLight
        var tempBottomLight = !Helper.dataIsNull(specDatas.bottomLight) ? specDatas.bottomLight : this.state.bottomLight
        if (this._canGetSpecData(this.state.sendTopBottomLightTime)) {
            this.setState({
                topLight: tempTopLight,
            })
        }
        if (this._canGetSpecData(this.state.sendTopBottomLightTime)) {
            this.setState({
                bottomLight: tempBottomLight,
            })
        }
        this._setModeLight(tempTopLight, tempBottomLight)
        this.setState({
            acInverterPower: !Helper.dataIsNull(specDatas.inverterPower) ? specDatas.inverterPower : this.state.acInverterPower,
            tempValue: !Helper.dataIsNull(specDatas.enirTemp) ? specDatas.enirTemp : this.state.tempValue,
            elcValue: !Helper.dataIsNull(specDatas.curRemaining) ? specDatas.curRemaining : this.state.elcValue,
            inputPvPower: !Helper.dataIsNull(specDatas.pvInputPower) ? specDatas.pvInputPower : this.state.inputPvPower,
            inputFanPower: !Helper.dataIsNull(specDatas.fanInputPower) ? specDatas.fanInputPower : this.state.inputFanPower,
            inputAcPower: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.inputAcPower,
            outputACPower: !Helper.dataIsNull(specDatas.acOutputTotalPower) ? specDatas.acOutputTotalPower : this.state.outputACPower,
            outputDCPower: !Helper.dataIsNull(specDatas.dcOutputPower) ? specDatas.dcOutputPower : this.state.outputDCPower,
            usbA1Power: !Helper.dataIsNull(specDatas.usbA1Power) ? specDatas.usbA1Power : this.state.usbA1Power,
            usbA2Power: !Helper.dataIsNull(specDatas.usbA2Power) ? specDatas.usbA2Power : this.state.usbA2Power,
            usbA3Power: !Helper.dataIsNull(specDatas.usbA3Power) ? specDatas.usbA3Power : this.state.usbA3Power,
            usbA4Power: !Helper.dataIsNull(specDatas.usbA4Power) ? specDatas.usbA4Power : this.state.usbA4Power,
            typeC1Power: !Helper.dataIsNull(specDatas.typeC1Power) ? specDatas.typeC1Power : this.state.typeC1Power,
            typeC2Power: !Helper.dataIsNull(specDatas.typeC2Power) ? specDatas.typeC2Power : this.state.typeC2Power,

            beeSwitch: !Helper.dataIsNull(specDatas.beeSwitch) ? specDatas.beeSwitch : this.state.beeSwitch,
            acChargePower: !Helper.dataIsNull(specDatas.acInputPower) ? specDatas.acInputPower : this.state.acChargePower,
            pvChargePower: parseInt(specDatas.pvChargePower) >= 0 ? specDatas.pvChargePower : this.state.pvChargePower,
            fanChargePower: parseInt(specDatas.fanChargePower) >= 0 ? specDatas.fanChargePower : this.state.fanChargePower,
            deviceStandByTime: parseInt(specDatas.deviceStandbyTime) >= 0 ? specDatas.deviceStandbyTime : this.state.deviceStandByTime,
            screenOffTime: parseInt(specDatas.screenOffTime) >= 0 ? specDatas.screenOffTime : this.state.screenOffTime,
            acStandByTime: parseInt(specDatas.acStandbyTime) >= 0 ? specDatas.acStandbyTime : this.state.acStandByTime,

            chargeRemindTime: !Helper.dataIsNull(specDatas.chargeRemindTime) ? specDatas.chargeRemindTime : this.state.chargeRemindTime,
            disChargeRemindTime: !Helper.dataIsNull(specDatas.disChargeRemindTime) ? specDatas.disChargeRemindTime : this.state.disChargeRemindTime,

            inputTotalPower: !Helper.dataIsNull(specDatas.inputTotalPower) ? specDatas.inputTotalPower : this.state.inputTotalPower,
            outputTotalPower: !Helper.dataIsNull(specDatas.outputTotalPower) ? specDatas.outputTotalPower : this.state.outputTotalPower,

            package1BatteryPower: !Helper.dataIsNull(specDatas.packInputPower1) ? specDatas.packInputPower1 : this.state.package1BatteryPower,
            package1ElcRemind: !Helper.dataIsNull(specDatas.packRemind1) ? specDatas.packRemind1 : this.state.package1ElcRemind,
            package2BatteryPower: !Helper.dataIsNull(specDatas.packInputPower2) ? specDatas.packInputPower2 : this.state.package2BatteryPower,
            package2ElcRemind: !Helper.dataIsNull(specDatas.packRemind2) ? specDatas.packRemind2 : this.state.package2ElcRemind,

            batteryProtectRangeMin: parseInt(specDatas.batteryProtectRangeMin) >= 0 ? specDatas.batteryProtectRangeMin : this.state.batteryProtectRangeMin,
            batteryProtectRangeMax: !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? specDatas.batteryProtectRangeMax : this.state.batteryProtectRangeMax,

            chargeStatus: !Helper.dataIsNull(specDatas.chargeStatus) ? specDatas.chargeStatus : this.state.chargeStatus,


            // capacity: !Helper.dataIsNull(specDatas.capacity) ? specDatas.capacity : this.state.capacity,
            // weight1: !Helper.dataIsNull(specDatas.weight1) ? specDatas.weight1 : this.state.weight1,
            // weight2: !Helper.dataIsNull(specDatas.weight2) ? specDatas.weight2 : this.state.weight2,
            // cycleCharge1: !Helper.dataIsNull(specDatas.cycleCharge1) ? specDatas.cycleCharge1 : this.state.cycleCharge1,
            // cycleCharge2: !Helper.dataIsNull(specDatas.cycleCharge2) ? specDatas.cycleCharge2 : this.state.cycleCharge2,
            // chargeTempMin: !Helper.dataIsNull(specDatas.chargeTempMin) ? specDatas.chargeTempMin : this.state.chargeTempMin,
            // chargeTempMax: !Helper.dataIsNull(specDatas.chargeTempMax) ? specDatas.chargeTempMax : this.state.chargeTempMax,
            // chargeTempT: !Helper.dataIsNull(specDatas.chargeTempT) ? specDatas.chargeTempT : this.state.chargeTempT,
            // disChargeTempMin: !Helper.dataIsNull(specDatas.disChargeTempMin) ? specDatas.disChargeTempMin : this.state.disChargeTempMin,
            // disChargeTempMax: !Helper.dataIsNull(specDatas.disChargeTempMax) ? specDatas.disChargeTempMax : this.state.disChargeTempMax,
            // disChargeTempT: !Helper.dataIsNull(specDatas.disChargeTempT) ? specDatas.disChargeTempT : this.state.disChargeTempT,
            // inputAcMin: !Helper.dataIsNull(specDatas.inputAcMin) ? specDatas.inputAcMin : this.state.inputAcMin,
            // inputAcMax: !Helper.dataIsNull(specDatas.inputAcMax) ? specDatas.inputAcMax : this.state.inputAcMax,
            // inputPvMax: !Helper.dataIsNull(specDatas.inputPvMax) ? specDatas.inputPvMax : this.state.inputPvMax,
            // inputWindElcMax: !Helper.dataIsNull(specDatas.inputWindElcMax) ? specDatas.inputWindElcMax : this.state.inputWindElcMax,
            // batteryPackNum: !Helper.dataIsNull(specDatas.batteryPackNum) ? specDatas.batteryPackNum : this.state.batteryPackNum,
            // acOutputMaxPower: !Helper.dataIsNull(specDatas.acOutputMaxPower) ? specDatas.acOutputMaxPower : this.state.acOutputMaxPower,
            // socket20ANum: !Helper.dataIsNull(specDatas.socket20ANum) ? specDatas.socket20ANum : this.state.socket20ANum,
            // socket30ANum: !Helper.dataIsNull(specDatas.socket30ANum) ? specDatas.socket30ANum : this.state.socket30ANum,
            // dcOutputMaxPower: !Helper.dataIsNull(specDatas.dcOutputMaxPower) ? specDatas.dcOutputMaxPower : this.state.dcOutputMaxPower,
            // usbANum: !Helper.dataIsNull(specDatas.usbANum) ? specDatas.usbANum : this.state.usbANum,
            // usbCNum: !Helper.dataIsNull(specDatas.usbCNum) ? specDatas.usbCNum : this.state.usbCNum,
            // wirelessChargeNum: !Helper.dataIsNull(specDatas.wirelessChargeNum) ? specDatas.wirelessChargeNum : this.state.wirelessChargeNum,
            // cigarette12vNum: !Helper.dataIsNull(specDatas.cigarette12vNum) ? specDatas.cigarette12vNum : this.state.cigarette12vNum,
            // dc12VNum: !Helper.dataIsNull(specDatas.dc12VNum) ? specDatas.dc12VNum : this.state.dc12VNum,
            // xt60Num: !Helper.dataIsNull(specDatas.xt60Num) ? specDatas.xt60Num : this.state.xt60Num,

            dc12vOutputPower: !Helper.dataIsNull(specDatas.dc12vOutputPower) ? specDatas.dc12vOutputPower : this.state.dc12vOutputPower,
            houseCarPower: !Helper.dataIsNull(specDatas.houseCarPower) ? specDatas.houseCarPower : this.state.houseCarPower,
            wirelessOutputPower: !Helper.dataIsNull(specDatas.wirelessOutputPower) ? specDatas.wirelessOutputPower : this.state.wirelessOutputPower,

            getDataFlag: true
        })
        this.state.batteryProtectRangeMin = parseInt(specDatas.batteryProtectRangeMin) >= 0 ? specDatas.batteryProtectRangeMin : this.state.batteryProtectRangeMin
        this.state.batteryProtectRangeMax = !Helper.dataIsNull(specDatas.batteryProtectRangeMax) ? specDatas.batteryProtectRangeMax : this.state.batteryProtectRangeMax
        this._setOutputValue(
            !Helper.dataIsNull(specDatas.dc12vOutputPower) ? specDatas.dc12vOutputPower : this.state.dc12vOutputPower,
            !Helper.dataIsNull(specDatas.houseCarPower) ? specDatas.houseCarPower : this.state.houseCarPower,
            !Helper.dataIsNull(specDatas.wirelessOutputPower) ? specDatas.wirelessOutputPower : this.state.wirelessOutputPower,
            !Helper.dataIsNull(specDatas.usbA1Power) ? specDatas.usbA1Power : this.state.usbA1Power,
            !Helper.dataIsNull(specDatas.usbA2Power) ? specDatas.usbA2Power : this.state.usbA2Power,
            !Helper.dataIsNull(specDatas.usbA3Power) ? specDatas.usbA3Power : this.state.usbA3Power,
            !Helper.dataIsNull(specDatas.usbA4Power) ? specDatas.usbA4Power : this.state.usbA4Power,
            !Helper.dataIsNull(specDatas.typeC1Power) ? specDatas.typeC1Power : this.state.typeC1Power,
            !Helper.dataIsNull(specDatas.typeC2Power) ? specDatas.typeC2Power : this.state.typeC2Power,
            !Helper.dataIsNull(specDatas.acOutputTotalPower) ? specDatas.acOutputTotalPower : this.state.outputACPower)

    }

    //开始连接蓝牙设备
    _startConnectBle() {
        this.setState({
            isWifiConnect: false
        })
        this._setBleConnectStatus(1)
        this.connect(this.state.mac)
    }

    _connectBleFailStop() {
        if (this.state.bleConnectStatus == 1 && this.intervalBleIcon) {
            this._stopBleIconAnimated()
            this._setBleConnectStatus(0)
        }
    }

    _setBleConnectStatus(status) {
        this.state.bleConnectStatus = status
        this.setState({
            bleConnectStatus: this.state.bleConnectStatus
        })
        if (status == 2) {
            setTimeout(() => {
                SpecUtil._bleSendQueryCode()
            }, 3000);
            //this._startBleQueryCode()
        } else {
            this._stopBleQueryCode()
        }
    }

    connect(macId) {
        BluetoothManager._bleLog("=====》蓝牙连接");
        //检查权限===》开启扫描===》扫描到设备===》连接设备

        BluetoothManager.start(); //蓝牙初始化、检查蓝牙状态
        if (Platform.OS === "ios") {
            GXRNManager.startBluetoothControl();  // 蓝牙初始化
        }
    }

    _androidOpenGps() {
        NativeModules.RNModule.isLocServiceEnable((result) => {
            if (!result) {
                //安卓未开启定位
                this.setState({gpsTipsDialog: true})
            } else {
                this.checkPermission()
            }
        });
    }

    async checkPermission() {
        const result = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        const result1 = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
        if (result && result1) {
            this._ble()
        } else {
            this.setState({locationTipsDialog: true})
        }
    }

    //安卓手机获取当前wifi名称需要获取定位权限
    async requestPermission() {
        try {
            PermissionsAndroid.requestMultiple(
                [
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION],
                {
                    'title': 'Wifi networks',
                    'message': 'We need your permission in order to find wifi networks'
                }
            ).then((result) => {
                if (result['android.permission.ACCESS_COARSE_LOCATION']
                    && result['android.permission.ACCESS_FINE_LOCATION'] === 'granted') {
                    LogUtil.debugLog("Thank you for your permission! :ACCESS_FINE_LOCATION)");
                    this._ble()
                } else if (result['android.permission.ACCESS_COARSE_LOCATION']
                    || result['android.permission.ACCESS_FINE_LOCATION'] === 'never_ask_again') {
                    this.onShowToast('Please Go into Settings -> Applications -> APP_NAME -> Permissions and Allow permissions to continue');
                }
            });
        } catch (err) {
            LogUtil.debugLog(err)
        }
    }

    async _ble() {
        if (Platform.OS === "android" && !(await hasAndroidPermission())) {
            return;
        }

        this._bleScan()
    }

    _bleScan() {
        if (this.state.bleConnectStatus != 1) {
            return
        }

        this._startScanTimer()
        this._startBleIconAnimated()

        //扫描设备
        if (Platform.OS === "android") {
            //安卓
            NativeModules.RNBLEModule.disConnectDevice()
            NativeModules.RNBLEModule.scanDevices(bleDeviceStartName, (callback) => {
                if (callback == 'success') {
                    BluetoothManager._bleLog('开启扫描成功')
                } else {
                    BluetoothManager._bleLog('开启扫描失败')
                    this._bleConnectFail()
                }
            })
        } else {
            BluetoothManager._bleLog('开始扫描')
            //ios
            GXRNManager.disConnectDevice();
            GXRNManager.scanDevices(bleDeviceStartName).then((datas) => {
                // 只是开启扫描，设备返回在通知里
            }).catch((err) => {
                BluetoothManager._bleLog("扫描蓝牙设备error:" + JSON.stringify(err));
                this._bleConnectFail()
            });
        }
    }

    _setFindData(scanData) {
        dataMap.set(scanData.id, scanData)
        var tempArray = []
        dataMap.forEach(function (item) {
            tempArray.push(item)
        })

        tempArray.map((item, index) => {
            if (this.state.mac == item.mac && this.state.bleConnectStatus == 1) {
                this._setBleConnectStatus(3)
                this.connectToBleDevice(item.mac)
            }
        })
    }

    connectToBleDevice(macId) {
        //连接蓝牙设备
        if (Platform.OS === 'android') {
            NativeModules.RNBLEModule.connectDevice(macId, (callback) => {

            })
        } else {
            GXRNManager.connectDevice(macId).then((datas) => {
                BluetoothManager._bleLog("蓝牙连接状态:" + JSON.stringify(datas));
                this._bleSuccessSetData(this.state.mac)
            }).catch((err) => {
                BluetoothManager._bleLog("蓝牙连接状态err:" + JSON.stringify(err));
                this._bleConnectFail()
            });
        }
    }
}