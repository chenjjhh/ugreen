// 电源详情/设置/智能自检
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    ScrollView,
    DeviceEventEmitter,
    NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import StorageHelper from "../Utils/StorageHelper";
import SpecUtil from "../Utils/SpecUtil";
// import  LottieView  from 'lottie-react-native';

var GXRNManager = NativeModules.GXRNManager;
const {StatusBarManager} = NativeModules;
if (Platform.OS === "ios") {
    StatusBarManager.getHeight((height) => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
const leftIcon = require("../../resources/back_black_ic.png");
export default class IntelligentSelfExamination extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("智能自检")}
                    backgroundColor={"#FBFBFB"}
                    leftIcon={leftIcon}
                    rightIcon={rightIcon}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            deviceId: props.navigation?.state?.params?.deviceId,
            SelfTestState: '0', //升级状态 1:自检中 0:自检完成
        };
    }

    componentWillUnmount() {
        this.intervalId && clearInterval(this.intervalId);
    }

    getStatus = () => {
        const {deviceId} = this.state;
        NetUtil.getDeviceProps(StorageHelper.getTempToken(), deviceId, false, false)
            .then((res) => {
                var specDatas = SpecUtil.resolveSpecs(res.info, false);
                const {self_check} = specDatas || {};
                console.log('self_check: ', self_check);
                if (self_check === "0") {
                    // 当self_check是0时，表示已经自检完成
                    this.setState({
                        SelfTestState: self_check,
                    });
                    this.intervalId && clearInterval(this.intervalId);
                }
            })
            .catch((error) => {
                throw error;
            });
    };

    // 开启自检
    startSelfTest = () => {
        const {deviceId} = this.state;
        this.setState({
            SelfTestState: "1",
        });
        NetUtil.deviceControlV3(
            StorageHelper.getTempToken(),
            deviceId,
            "self_check",
            "1",
            false
        )
            .then((res) => {
                // 开始质检后，启动轮询查询，知道查询到self_check为0则停止
                this.intervalId = setInterval(() => {
                    this.getStatus();
                }, 500);
            })
            .catch((error) => {
                // 下发失败后重置空状态
                this.setState({
                    SelfTestState: "",
                });
            });
    };

    // 获取状态
    getSelfTestStateText = () => {
        const {SelfTestState} = this.state;
        let text;
        switch (SelfTestState) {
            case "0":
                text = strings("设备正常");
                break;
            case "1":
                text = strings("智能自检中");
                break;
            default:
                text = "";
                break;
        }
        return text;
    };

    render() {
        const {SelfTestState} = this.state;
        const selfTestView = (
            <>
                <View style={{paddingHorizontal: 20}}>
                    <View style={{alignItems: "center", marginTop: 50}}>
                        <View
                            style={{
                                backgroundColor: "rgba(24, 179, 79, 0.1)",
                                borderRadius: 58,
                                width: 118,
                                height: 118,
                                marginBottom: 80,
                            }}
                        >
                            {/* <LottieView source={require('../../Lottie/selfInspectionAnimation/selfInspectionAnimation.json')} loop={true} /> */}
                        </View>
                        <Text style={{color: "#404040", fontSize: 16, fontWeight: 'bold', lineHeight: 25}}>
                            {this.getSelfTestStateText()}
                        </Text>
                    </View>
                    <View style={{marginBottom: 30, marginTop: 10}}>
                        <Text style={{color: "#808080", fontSize: 14, lineHeight: 25}}>
                            {strings("自检提示语")}
                        </Text>
                    </View>
                </View>
                <View style={{alignItems: "center"}}>
                    {SelfTestState !== "1" ? (
                        <CommonBtnView
                            clickAble={true}
                            onPress={() => {
                                // 开始升级
                                this.startSelfTest();
                            }}
                            style={{
                                backgroundColor: "#18B34F",
                                width: (this.mScreenWidth - 40) * 0.9,
                            }}
                            btnText={strings("开启自检")}
                        />
                    ) : null}
                </View>
            </>
        );

        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F7F7F7",
                    alignItems: "center",
                    width: this.mScreenWidth,
                }}
            >
                <View
                    style={{
                        flex: 1,
                        width: this.mScreenWidth,
                        paddingHorizontal: 20,
                        alignItems: "center",
                    }}
                >
                    <View
                        style={{
                            overflow: "hidden",
                            width: this.mScreenWidth - 40,
                            height: this.mScreenHeight,
                            paddingVertical: 25,
                            justifyContent: "space-between",
                            alignItems: "center",
                            flex: 1,
                        }}
                    >
                        {selfTestView}
                    </View>
                </View>
            </View>
        );
    }
}
