// 电源详情/设备设置/定时设置
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Animated,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  DeviceEventEmitter,
  NativeModules,
  AppState
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import ChooseDialog from "../View/ChooseDialog";
import StorageHelper from "../Utils/StorageHelper";
import SpecUtil from "../Utils/SpecUtil";
var GXRNManager = NativeModules.GXRNManager;

const { StatusBarManager } = NativeModules;
if (Platform.OS === "ios") {
  StatusBarManager.getHeight((height) => {
    statusBarHeight = height.height;
  });
} else {
  statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;
const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
const leftIcon = require("../../resources/back_black_ic.png");
const timer_ac_btn = require("../../resources/greenUnion/timer_ac_btn.png");
const timer_ac_sel_btn = require("../../resources/greenUnion/timer_ac_sel_btn.png");

const timer_dc_btn = require("../../resources/greenUnion/timer_dc_btn.png");
const timer_dc_sel_btn = require("../../resources/greenUnion/timer_dc_sel_btn.png");

const timer_usb_btn = require("../../resources/greenUnion/timer_usb_btn.png");
const timer_usb_sel_btn = require("../../resources/greenUnion/timer_usb_sel_btn.png");

const timer_light_btn = require("../../resources/greenUnion/timer_light_btn.png");
const timer_light_sel_btn = require("../../resources/greenUnion/timer_light_sel_btn.png");

export default class RegularSettings extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          leftIcon={leftIcon}
          leftIconClick={() => {
            navigation.goBack();
          }}
          backgroundColor={"#FBFBFB"}
          titleText={strings("定时设置")}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    const { deviceId, isWifiConnect, isOnline} =
    props.navigation?.state?.params || {};
    this.state = {
      deviceId,
      isWifiConnect,
      isOnline,
      ctimingArea: [
        {
          type: "ac_sw",
          isSelected: false,
          selectedIcon: timer_ac_sel_btn,
          noSelectedIcon: timer_ac_btn,
        },
        {
          type: "dc_sw",
          isSelected: false,
          selectedIcon: timer_dc_sel_btn,
          noSelectedIcon: timer_dc_btn,
        },
        {
          type: "lighting_lantern",
          isSelected: false,
          selectedIcon: timer_light_sel_btn,
          noSelectedIcon: timer_light_btn,
        },
        {
          type: "usb_sw",
          isSelected: false,
          selectedIcon: timer_usb_sel_btn,
          noSelectedIcon: timer_usb_btn,
        },
      ],
      timingDuration: "", //计时时长
      limitCapacity: "", //限制容量
      timingDurationDiablog: false, //计时时长弹窗
      limitCapacityDiablog: false, //限制容量弹窗
    };

    this.timingDurationData = [
      { text: strings("不设置"), id: "0" },
      { text: strings("30分钟"), id: "1" },
      { text: strings("1小时"), id: "2" },
      { text: strings("2小时"), id: "3" },
      { text: strings("4小时"), id: "4" },
      { text: strings("6小时"), id: "5" },
      { text: strings("12小时"), id: "6" },
    ];
    this.limitCapacityData = [
      { text: strings("不设置"), id: "0" },
      { text: "10%", id: "1" },
      { text: "20%", id: "2" },
      { text: "30%", id: "3" },
      { text: "50%", id: "4" },
    ];
    this.startTime = new Date().getTime();//开始下发时间
    this.currentTime = new Date().getTime();//下发成功时间
    this.allowTimer = true //默认允许轮询
  }

  componentDidMount() {
    const { deviceId } = this.state;
    this._getDeviceProps(deviceId, true, false);
    this._eventListenerAdd();
  }

  componentWillUnmount(){
    this._eventListenerRemove()
    this._stopTimerGetDeviceProps();
  }
  
  //获取设备属性
  _getDeviceProps = (deviceId, showLoading, hideErrorToast) => {
    const {
      timingDuration,
      limitCapacity,
      ctimingArea,
      isOnline
    } = this.state;
    if (Helper.dataIsNull(deviceId)) {
      return;
    }
    NetUtil.getDeviceProps(
      StorageHelper.getTempToken(),
      deviceId,
      showLoading,
      hideErrorToast
    ).then((res) => {
      var specDatas = SpecUtil.resolveSpecs(res.info, false);
      const {
        time_shutdown,
        timeoff_cap,
        timeoff_zoom,
      } = specDatas || {};
      if (JSON.stringify(specDatas)) {
        // 定时区域
        let timeoffZoomStr = this.formatString(timeoff_zoom);
        let newCtimingArea = [...ctimingArea];
        timeoffZoomStr?.split('')?.forEach((element,index) => {
          newCtimingArea[index]['isSelected'] = Boolean(Number(element))
        });
        this.setState({
          ctimingArea: [...ctimingArea],
        });

        // 定时关机时间
        if (timingDuration !== time_shutdown) {
          this.setState({
            timingDuration: !Helper.dataIsNull(time_shutdown)
              ? time_shutdown
              : timingDuration,
          });
        }

        // 定时关机容量
        if (limitCapacity !== timeoff_cap) {
          this.setState({
            limitCapacity: !Helper.dataIsNull(timeoff_cap)
              ? timeoff_cap
              : limitCapacity,
          });
        }
      }
    });
  }

  _timerGetDeviceProps() {
    const { deviceId } = this.state;
    this._stopTimerGetDeviceProps();
    this.intervalGetDeviceProps = setInterval(() => {
      if(this.currentTime - this.startTime < 3000 && !this.allowTimer) {
        this.allowTimer = true;
        return;
      };
      this._getDeviceProps(deviceId, false, true);
    }, 4000);
  }

  _stopTimerGetDeviceProps() {
    if (this.intervalGetDeviceProps) {
      clearInterval(this.intervalGetDeviceProps);
      this.intervalGetDeviceProps = null;
    }
  }

  _eventListenerAdd = () => {
    const { deviceId } = this.state;
    // 温度单位变化监听
    this.tempUnitListener = DeviceEventEmitter.addListener(
      EventUtil.SEND_TEMP_UNIT,
      (unit) => {
        this.setState({
          temperatureUnit: unit,
        });
      }
    );

    // 重新登录
    this.toLoginListener = DeviceEventEmitter.addListener(
      EventUtil.TO_LOGIN,
      (value) => {
        this._eventListenerRemove();
      }
    );

    // 监听设备离线状态变化
    this.deviceOfflineListener = DeviceEventEmitter.addListener(
      EventUtil.DEVICE_OFFLINE,
      (res) => {
        if (res) {
          var did = res.deviceId;
          if (did == deviceId) {
            LogUtil.debugLog(
              "offline======设备详情=====>设备离线：did" +
                did +
                ",deviceId:" +
                deviceId
            );
            this.setState({
              isOnline: false,
            });
          }
        }
      }
    );

    // 监听设备在线状态变化
    this.deviceOnlineListener = DeviceEventEmitter.addListener(
      EventUtil.DEVICE_ONLINE,
      (res) => {
        if (res) {
          var did = res.deviceId;
          if (did == deviceId) {
            LogUtil.debugLog(
              "online======设备详情=====>设备上线：did" +
                did +
                ",deviceId:" +
                deviceId
            );
            this.setState({
              isOnline: true,
            });
            this._getDeviceProps(deviceId, false, true);
          }
        }
      }
    );

  };

  _eventListenerRemove = ()=>{
    this.tempUnitListener?.remove();
    this.toLoginListener?.remove();
    this.deviceOfflineListener?.remove();
    this.deviceOnlineListener?.remove();
  }

  // 格式化定时区域字符
  formatString = (str) => {  
    if (str === '0' || str === '') {  
      return '0000';  
    } else if (str?.length < 4) {  
      const padding = '0'.repeat(4 - str.length);  
      return `${padding}${str}`;  
    } else {  
      return str;  
    }  
  }

  // 选择定时区域
  selectcTimingArea = (currentItem, index) => {
    const { ctimingArea } = this.state;
    const newCtimingArea = ctimingArea.map((item, index) => {
      if (item.type === currentItem.type) {
        item.isSelected = !item.isSelected;
      }
      return item;
    });
    this.setState({
      ctimingArea: newCtimingArea,
    });
  };

  // 全选/取消全选
  handleAllSelection = (type) => {
    const { ctimingArea } = this.state;
    const newCtimingArea = ctimingArea.map((item, index) => {
      item.isSelected = type;
      return item;
    });
    this.setState({
      ctimingArea: newCtimingArea,
    });
  };

  // 判断显示全选还是取消全选
  isAllSeclect = () => {
    const { ctimingArea } = this.state;
    let isAllSelected = true;
    ctimingArea?.map((item, index) => {
      if (!item.isSelected) {
        isAllSelected = false;
      }
    });
    return isAllSelected;
  };

  // 隐藏选择弹窗
  _onHide = (type) => {
    this.setState({
      [type]: false,
    });
  };

  // 隐藏弹窗
  _onShow = (type) => {
    this.setState({
      [type]: true,
    });
  };

  _turnOnTime = () => {
    const {
      isWifiConnect,
      deviceId,
      ctimingArea,
      timingDuration,
      limitCapacity,
    } = this.state;
    //获取当前时间毫秒数
    this.startTime = new Date().getTime(); 
     // 定时关机容量对应限制容量，选择区域对应定时区域。这里传输的数据需要注意全选下发数据是 1111;AC 不选下发 0111:AC+DC 不选下发 0011，依次类推
    let ctimingAreaArray = [];
    ctimingArea?.forEach((element) => {
      ctimingAreaArray.push(Number(element.isSelected).toString());
    });
    let timeoff_zoom = ctimingAreaArray.join('');
    let param = {
      time_shutdown: timingDuration,
      timeoff_cap: limitCapacity,
      timeoff_zoom,
    };
    // console.log('param: ', param);
    if (isWifiConnect) {
      NetUtil.deviceControlV3(
        StorageHelper.getTempToken(),
        deviceId,
        param,
        null,
        false,
        true
      )
        .then((res) => {
          console.log('res？？？？ ', res);
          this.currentTime = new Date().getTime();
          if(this.currentTime - this.startTime < 3000 ) {
            this.allowTimer = false;
          }
          // 开启定时成功后返回
          this.props.navigation?.goBack();
        })
        .catch((error) => {
          console.log('error: ', error);
          throw error;
        });
    }
  };

  // 选择弹窗
  _checkTab = (id, type, dialogType) => {
    this.setState({ [type]: id, [dialogType]: false });
  };

  // 根据枚举值匹配相应选项文字
  getText = (type, value) => {
    return this[type]?.find((element) => element.id === value)?.text;
  };

  render() {
    const {
      ctimingArea,
      timingDuration,
      limitCapacity,
      timingDurationDiablog,
      limitCapacityDiablog,
    } = this.state;
    // 定时区域
    const ctimingAreaView = (
      <View style={{ marginBottom: 0 }}>
        <View
          style={{
            backgroundColor: "#fff",
            borderRadius: 10,
            width: this.mScreenWidth - 40,
            paddingBottom: 25,
            marginVertical: 10,
          }}
        >
          <View style={{ width: "100%", paddingHorizontal: 15 }}>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text
                style={{
                  fontSize: this.getSize(14),
                  color: "#404040",
                  lineHeight: 55,
                }}
              >
                {strings("定时区域")}
              </Text>
              <TouchableOpacity
                onPress={() => this.handleAllSelection(!this.isAllSeclect())}
              >
                <Text
                  style={{
                    fontSize: this.getSize(14),
                    color: "#808080",
                    lineHeight: 55,
                  }}
                >
                  {this.isAllSeclect() ? strings("取消全选") : strings("全选")}
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                width: this.mScreenWidth - 70,
                overflow: "hidden",
                paddingHorizontal: 4,
              }}
            >
              {ctimingArea?.map((item, index) => (
                <TouchableOpacity
                  onPress={() => this.selectcTimingArea(item, index)}
                  key={index}
                  style={{
                    width: 50,
                    height: 50,
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 23,
                  }}
                >
                  <Image
                    source={
                      item.isSelected ? item.selectedIcon : item.noSelectedIcon
                    }
                  />
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </View>
      </View>
    );

    //计时时长
    const timingDurationView = (
      <ItemBtnView
        style={{ borderRadius: 10, marginBottom: 10 }}
        onPress={() => {
          this._onShow("timingDurationDiablog");
        }}
        rightText={this.getText("timingDurationData", timingDuration)}
        text={strings("计时时长")}
        rightIcon={rightIcon}
      />
    );

    //限制容量
    const limitCapacityView = (
      <ItemBtnView
        style={{ borderRadius: 10, marginBottom: 10 }}
        onPress={() => {
          this._onShow("limitCapacityDiablog");
        }}
        text={strings("限制容量")}
        rightText={this.getText("limitCapacityData", limitCapacity)}
        rightIcon={rightIcon}
      />
    );

    const dialogView = (
      <View>
        <ChooseDialog
          title={strings("计时时长")}
          show={timingDurationDiablog}
          data={this.timingDurationData}
          currentId={timingDuration}
          _checkTab={(data) => {
            this._checkTab(data.id, "timingDuration", "timingDurationDiablog");
          }}
          _onHide={() => this._onHide("timingDurationDiablog")}
        />
        <ChooseDialog
          title={strings("限制容量")}
          show={limitCapacityDiablog}
          data={this.limitCapacityData}
          currentId={limitCapacity}
          _checkTab={(data) => {
            this._checkTab(data.id, "limitCapacity", "limitCapacityDiablog");
          }}
          _onHide={() => this._onHide("limitCapacityDiablog")}
        />
      </View>
    );

    const turnOnTimeView = (
      <View
        style={{
          flexDirection: "row",
          marginVertical: 20,
          justifyContent: "center",
        }}
      >
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            // 开启定时
            this._turnOnTime();
          }}
          style={{
            backgroundColor: "#18B34F",
            width: (this.mScreenWidth - 40) * 0.9,
          }}
          btnText={strings("开启定时")}
        />
      </View>
    );

    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#F7F7F7",
          alignItems: "center",
          height: this.mScreenHeight,
          width: this.mScreenWidth,
        }}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ width: this.mScreenWidth, alignItems: "center" }}>
            <View
              style={{
                width: this.mScreenWidth,
                paddingHorizontal: 20,
                alignItems: "center",
              }}
            >
              {ctimingAreaView}
              {timingDurationView}
              {limitCapacityView}
              {dialogView}
              <Text style={{ fontSize: 14, color: "#808080", lineHeight: 25 }}>
                {strings("限制容量提示")}
              </Text>
            </View>
          </View>
        </ScrollView>
        {turnOnTimeView}
      </View>
    );
  }
}
