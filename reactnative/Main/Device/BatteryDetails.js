// 电源详情
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    ScrollView,
    DeviceEventEmitter,
    NativeModules,
    AppState,
    NativeEventEmitter,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import DialogConnect from "../View/DialogConnect";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import ChooseDialog from "../View/ChooseDialog";
import StorageHelper from "../Utils/StorageHelper";
import SpecUtil from "../Utils/SpecUtil";

var GXRNManager = NativeModules.GXRNManager;
const {StatusBarManager} = NativeModules;
if (Platform.OS === "ios") {
    StatusBarManager.getHeight((height) => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
const leftIcon = require("../../resources/back_black_ic.png");

var dataMap = new Map();
var tempSpecDatas = null;
const specDelayTime = 5000;
let keyData = [
    {key1: "acIsSelected", key2: "switch_ac"},
    {key1: "dcIsSelected", key2: "switch_dc"},
    {key1: "usbIsSelected", key2: "usb_sw"},
    {key1: "UtIsSelected", key2: "switch_conpower"},
    {key1: "lightingLantern", key2: "lamp_sw"},
    {key1: "closeWithOneClick", key2: "switch_all"},
    {key1: "operatingValue", key2: "work_mode"},
];
export default class BatteryDetails extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null,
        };
    };

    constructor(props) {
        super(props);
        const {deviceName, itemData, isWifiConnect} =
        props.navigation?.state?.params || {};
        this.state = {
            deviceName: deviceName || "", //设备名称
            deviceId: itemData?.deviceId || "", //设备ID
            isOnline: itemData?.isOnline || "", //是否WIFI在线
            isWifiConnect, //是否WIFI连接
            mac: itemData?.mac || "",
            temperature: "", //温度
            electricity: "", //剩余电量
            totalInput: "", //总输入
            totalOutput: "", //总输出
            dischargeTime: "", //放电剩余时间
            chargingTime: "", //充电剩余时间
            equipmentManagementIsShow: false, //设备管理是否展开
            acIsSelected: "", //  AC交流开关是否打开 '0':关 '1'：开
            acDischargePow: "", //  AC放电功率
            UtIsSelected: "", //  UT交流开关是否打开
            dcIsSelected: "", // DC直流
            usbIsSelected: "", // USB开关是否打开
            usb1: undefined, //usb1输出功率
            usb2: undefined, //usb2输出功率
            usb3: undefined, //usb3输出功率
            usb4: undefined, //usb4输出功率
            usbc1: undefined, //typec1输出功率
            usbc2: undefined, //typec2输出功率
            usbc3: undefined, //typec3输出功率
            usbc4: undefined, //typec4输出功率
            lightingLantern: "", //照明灯选项 0关闭 1:低亮 2：高亮 3：爆闪 4：SOS
            lightingLanternData: [
                {title: strings("低亮"), key: "1"},
                {title: strings("高亮"), key: "2"},
                {title: strings("爆闪"), key: "3"},
                {title: "SOS", key: "4"},
            ],
            operatingModeTipsDialog: false, //工作模式提示弹窗
            operatingModeDialog: false, //工作模式选择弹窗
            operatingValue: "", //当前工作模式
            utTipsDialog: false, //UT提示弹窗
            upgradePromptDialog: false, //升级提示弹窗
            size: "1.0M", //升级固件大小
            closeWithOneClick: "", //是否一键关闭
            batteryStatus: 1, //设备是否在线 0：离线 1：在线
            DialogConnectDialog: false, //连接提示弹窗
            blueConnectStatus: 1, //蓝牙连接状态 0:未开始 1：连接中 2：连接成功 3:连接失败
            deviceData: itemData,
            dcValue: "", //DC直流功率,

            isFirstUseUTurbo: true,
            isFirstUseWorkMode: true
        };
        this.operatingModeData = [
            {text: strings("标准模式"), id: "0"},
            {text: strings("节能模式"), id: "1"},
            {text: strings("常开模式"), id: "3"},
        ];
        this.startTime = new Date().getTime();//开始下发时间
        this.currentTime = new Date().getTime();//下发成功时间
        this.allowTimer = true //默认允许轮询
    }

    componentWillMount() {
        const {deviceData, deviceId, isOnline, isWifiConnect} = this.state;
        this._getUserInfo();
        if (deviceData) {
            this._getDeviceProps(deviceId, true, false);
            if (!isOnline) {
                //   离线
            }
        }
        if (!isWifiConnect) {
            //非WiFi连接，初始化蓝牙相关事件
        } else {
            this._eventListenerAdd();
        }
        // 获取本地温度单位
        this._getLocalTempUnit();
        // 温度单位变化监听
        this.tempUnitListener = DeviceEventEmitter.addListener(
            EventUtil.SEND_TEMP_UNIT,
            (unit) => {
                this.setState({
                    temperatureUnit: unit,
                });
            }
        );

        this._getIsFirstUseUTurbo();
        this._getIsFirstUseWorkMode();
    }

    // 获取是否首次使用u-turbo
    _getIsFirstUseUTurbo() {
        StorageHelper.getIsFirstUseUTurbo()
            .then((flag) => {
                this.setState({
                    isFirstUseUTurbo: flag,
                });
            })
            .catch((error) => {
            });
    }

    // 获取是否首次使用工作模式
    _getIsFirstUseWorkMode() {
        StorageHelper.getIsFirstUseWorkMode()
            .then((flag) => {
                this.setState({
                    isFirstUseWorkMode: flag,
                });
            })
            .catch((error) => {
            });
    }

    // 获取温度单位 "0": 摄氏度, "1": 华氏度
    _getLocalTempUnit() {
        StorageHelper.getTempUnit()
            .then((unit) => {
                this.setState({
                    temperatureUnit: parseInt(unit),
                });
                if (unit == undefined) {
                    //设置默认温度单位
                    StorageHelper.saveTempUnit(0);
                }
            })
            .catch((error) => {
            });
    }

    _eventListenerAdd() {
        const {deviceId, isOnline} = this.state;
        if (Platform.OS === "android") {
            this.awsDeviceStatusListListener = DeviceEventEmitter.addListener(
                EventUtil.AWS_DEVICE_SHADOW_KEY,
                (value) => {
                    //aws 设备状态回调
                    // LogUtil.debugLog("设备状态回调:" + JSON.stringify(value));
                    //收到属性变化，重新请求获取属性接口
                    this._getDeviceProps(deviceId, false, true);
                }
            );
        } else {
            const eventEmitter = new NativeEventEmitter(GXRNManager);
            eventEmitter.addListener(EventUtil.AWS_DEVICE_SHADOW_KEY, (value) => {
                //aws 设备状态回调
                // LogUtil.debugLog("设备状态回调:" + JSON.stringify(value));
                //收到属性变化，重新请求获取属性接口
                this._getDeviceProps(deviceId, false, true);
            });
        }

        // 更新设备名称
        this.updateDeviceNameListener = DeviceEventEmitter.addListener(
            EventUtil.UPDATE_DEVICE_NAME_SUCCESS,
            (value) => {
                //修改设备名称成功，收到通知
                if (value) {
                    this.setState({deviceName: value});
                }
            }
        );

        // 监听应用程序状态的变化
        this.appStateListener = AppState.addEventListener("change", (status) => {
            // 监听第一次改变后, 可以取消监听.或者在componentUnmount中取消监听
            if (status != null && status === "active" && isOnline) {
                // 后台进入前台
                this._timerGetDeviceProps();
                // if (blueConnectStatus == 1) {
                //     if (Platform.OS === 'android') {
                //         this._androidOpenGps()
                //     } else {
                //         this._ble()
                //     }
                // }
            } else {
                this._stopTimerGetDeviceProps();
            }
        }); // 监听APP状态  前台、后台

        // 重新登录
        this.toLoginListener = DeviceEventEmitter.addListener(
            EventUtil.TO_LOGIN,
            (value) => {
                this._stopTimerGetDeviceProps();
                this._eventListenerRemove();
            }
        );
        this.deviceOfflineListener = DeviceEventEmitter.addListener(
            EventUtil.DEVICE_OFFLINE,
            (res) => {
                if (res) {
                    var did = res.deviceId;
                    if (did == deviceId) {
                        LogUtil.debugLog(
                            "offline======设备详情=====>设备离线：did" +
                            did +
                            ",deviceId:" +
                            deviceId
                        );
                        this.setState({
                            isOnline: false,
                        });
                    }
                }
            }
        );

        // 监听设备在线状态变化
        this.deviceOnlineListener = DeviceEventEmitter.addListener(
            EventUtil.DEVICE_ONLINE,
            (res) => {
                if (res) {
                    var did = res.deviceId;
                    if (did == deviceId) {
                        LogUtil.debugLog(
                            "online======设备详情=====>设备上线：did" +
                            did +
                            ",deviceId:" +
                            deviceId
                        );
                        this.setState({
                            isOnline: true,
                        });
                        this._getDeviceProps(deviceId, false, true);
                    }
                }
            }
        );

        // 监听路由长度变化
        this.routesListener = DeviceEventEmitter.addListener(EventUtil.ROUTES_LENGTH, (message) => {
            if (message === 3) {
                this._getDeviceProps(deviceId, false, true);
            }
        });
    }

    componentWillUnmount() {
        this._eventListenerRemove();
        this._stopTimerGetDeviceProps();
        this.tempUnitListener?.remove();
    }

    _eventListenerRemove() {
        this.awsDeviceStatusListListener?.remove();
        this.updateDeviceNameListener?.remove();
        this.appStateListener?.remove();
        this.toLoginListener?.remove();
        this.deviceOfflineListener?.remove();
        this.deviceOnlineListener?.remove();
        this.routesListener.remove();
    }

    _getUserInfo() {
        StorageHelper.getUserInfo()
            .then((res) => {
                if (res) {
                    this.setState({
                        account: res.account ? res.account : res.email,
                    });
                }
            })
            .catch((error) => {
            });
    }

    //获取设备属性
    _getDeviceProps(deviceId, showLoading, hideErrorToast) {
        const {
            isWifiConnect,
            temperature,
            electricity,
            totalInput,
            totalOutput,
            dischargeTime,
            chargingTime,
            acIsSelected,
            acDischargePow,
            UtIsSelected,
            dcIsSelected,
            usbIsSelected,
            lightingLantern,
            operatingValue,
            dcValue,
            usb1,
            usb2,
            usb3,
            usb4,
            usbc1,
            usbc2,
            usbc3,
            usbc4,
            isOnline,
        } = this.state;
        if (Helper.dataIsNull(deviceId)) {
            return;
        }

        SpecUtil.initProps(StorageHelper.getTempToken(), deviceId, isWifiConnect);
        NetUtil.getDeviceProps(
            StorageHelper.getTempToken(),
            deviceId,
            showLoading,
            hideErrorToast
        )
            .then((res) => {
                console.log('res: ', res);
                var specDatas = SpecUtil.resolveSpecs(res.info, false);
                const {
                    bat_temp1,
                    battery_percentage,
                    charge_power_all,
                    discharge_pow,
                    charge_remain_time,
                    discharge_remain_time,
                    ac_discharge_pow,
                    switch_ac,
                    switch_conpower,
                    switch_dc,
                    usb_sw,
                    lamp_sw,
                    work_mode,
                    dc_vol,
                    usb1_vol,
                    usb2_vol,
                    usb3_vol,
                    usb4_vol,
                    type_c1_vol,
                    type_c2_vol,
                    type_c3_vol,
                    type_c4_vol,
                } = specDatas || {};
                if (JSON.stringify(specDatas)) {
                    // if (JSON.stringify(specDatas) != JSON.stringify(tempSpecDatas)) {
                    tempSpecDatas = specDatas;
                    //电池温度
                    if (temperature != bat_temp1) {
                        this.setState({
                            temperature: !Helper.dataIsNull(bat_temp1)
                                ? bat_temp1
                                : temperature,
                        });
                    }
                    //电池电量百分比
                    if (electricity != battery_percentage) {
                        this.setState({
                            electricity: !Helper.dataIsNull(battery_percentage)
                                ? battery_percentage
                                : electricity,
                        });
                    }

                    //总输入
                    console.log('charge_power_all: ', charge_power_all);
                    if (totalInput != charge_power_all) {
                        this.setState({
                            totalInput: !Helper.dataIsNull(charge_power_all)
                                ? charge_power_all
                                : totalInput,
                        });
                    }

                    //总输出
                    if (totalOutput != discharge_pow) {
                        this.setState({
                            totalOutput: !Helper.dataIsNull(discharge_pow)
                                ? discharge_pow
                                : totalOutput,
                        });
                    }

                    //放电剩余时间
                    if (dischargeTime != discharge_remain_time) {
                        this.setState({
                            dischargeTime: !Helper.dataIsNull(discharge_remain_time)
                                ? discharge_remain_time
                                : dischargeTime,
                        });
                    }

                    //充电剩余时间
                    if (chargingTime != charge_remain_time) {
                        this.setState({
                            chargingTime: !Helper.dataIsNull(charge_remain_time)
                                ? charge_remain_time
                                : chargingTime,
                        });
                    }

                    //ac交流
                    if (acIsSelected !== switch_ac) {
                        this.setState({
                            acIsSelected: !Helper.dataIsNull(switch_ac)
                                ? switch_ac
                                : acIsSelected,
                        });
                    }

                    //ac放电功率
                    if (acDischargePow !== ac_discharge_pow) {
                        this.setState({
                            acDischargePow: !Helper.dataIsNull(ac_discharge_pow)
                                ? ac_discharge_pow
                                : acDischargePow,
                        });
                    }

                    //u-turbo
                    if (UtIsSelected !== switch_conpower) {
                        this.setState({
                            UtIsSelected: !Helper.dataIsNull(switch_conpower)
                                ? switch_conpower
                                : UtIsSelected,
                        });
                    }

                    //dc直流功率
                    if (dcValue !== dc_vol) {
                        this.setState({
                            dcValue: !Helper.dataIsNull(dc_vol) ? dc_vol : dcValue,
                        });
                    }

                    //dc交流
                    if (dcIsSelected !== switch_dc) {
                        this.setState({
                            dcIsSelected: !Helper.dataIsNull(switch_dc)
                                ? switch_dc
                                : dcIsSelected,
                        });
                    }

                    //usb开关
                    if (usbIsSelected !== usb_sw) {
                        this.setState({
                            usbIsSelected: !Helper.dataIsNull(usb_sw)
                                ? usb_sw
                                : usbIsSelected,
                        });
                    }

                    //usb1
                    if (usb1 !== usb1_vol) {
                        this.setState({
                            usb1: !Helper.dataIsNull(usb1_vol) ? usb1_vol : usb1,
                        });
                    }

                    //usb2
                    if (usb2 !== usb2_vol) {
                        this.setState({
                            usb2: !Helper.dataIsNull(usb2_vol) ? usb2_vol : usb2,
                        });
                    }

                    //usb3
                    if (usb3 !== usb3_vol) {
                        this.setState({
                            usb3: !Helper.dataIsNull(usb3_vol) ? usb3_vol : usb3,
                        });
                    }

                    //usb4
                    if (usb4 !== usb4_vol) {
                        this.setState({
                            usb4: !Helper.dataIsNull(usb4_vol) ? usb4_vol : usb4,
                        });
                    }

                    //typec1
                    if (usbc1 !== type_c1_vol) {
                        this.setState({
                            usbc1: !Helper.dataIsNull(type_c1_vol) ? type_c1_vol : usbc1,
                        });
                    }

                    //typec2
                    if (usbc2 !== type_c2_vol) {
                        this.setState({
                            usbc2: !Helper.dataIsNull(type_c2_vol) ? type_c2_vol : usbc2,
                        });
                    }

                    //typec3
                    if (usbc3 !== type_c3_vol) {
                        this.setState({
                            usbc3: !Helper.dataIsNull(type_c3_vol) ? type_c3_vol : usbc3,
                        });
                    }

                    //typec4
                    if (usbc4 !== type_c4_vol) {
                        this.setState({
                            usbc4: !Helper.dataIsNull(type_c4_vol) ? type_c4_vol : usbc4,
                        });
                    }

                    //照明
                    if (lightingLantern !== lamp_sw) {
                        this.setState({
                            lightingLantern: !Helper.dataIsNull(lamp_sw)
                                ? lamp_sw
                                : lightingLantern,
                        });
                    }

                    //工作模式
                    if (operatingValue !== work_mode) {
                        this.setState({
                            operatingValue: !Helper.dataIsNull(work_mode)
                                ? work_mode
                                : operatingValue,
                        });
                    }
                }

                if (showLoading && isOnline) {
                    //开始轮询
                    this._timerGetDeviceProps();
                }
            })
            .catch((error) => {
                this._stopTimerGetDeviceProps();
            });

        //查询设备状态  会在事件EventUtil.AWS_DEVICE_SHADOW_KEY中回调
        //NativeModules.RNModule.queryDevStatus(mac);
    }

    componentWillUnmount() {
        this._stopTimerGetDeviceProps();
    }

    _timerGetDeviceProps() {
        this._stopTimerGetDeviceProps();
        this.intervalGetDeviceProps = setInterval(() => {
            if (this.currentTime - this.startTime < 3000 && !this.allowTimer) {
                this.allowTimer = true;
                return;
            }
            this._getDeviceProps(this.state.deviceId, false, true);
        }, 3000);
    }

    _stopTimerGetDeviceProps() {
        if (this.intervalGetDeviceProps) {
            clearInterval(this.intervalGetDeviceProps);
            this.intervalGetDeviceProps = null;
        }
    }

    // 开启或关闭switch
    onValueChange = (value, type) => {
        if (type == 'UtIsSelected') {
            if (this.state.isFirstUseUTurbo) {
                //u turbo是否第一次使用是的话 先弹窗
                this._onShow("utTipsDialog")
                StorageHelper.saveIsFirstUseUTurbo(false);
                this.setState({isFirstUseUTurbo: false})
                return
            }
        }
        const {isWifiConnect, deviceId} = this.state;
        //获取当前时间毫秒数
        this.startTime = new Date().getTime();
        if (isWifiConnect) {
            NetUtil.deviceControlV3(
                StorageHelper.getTempToken(),
                deviceId,
                Helper.getKey(type, keyData),
                value,
                false
            )
                .then((res) => {
                    this.currentTime = new Date().getTime();
                    if (this.currentTime - this.startTime < 3000) {
                        this.allowTimer = false;
                    }
                    this.setState({
                        [type]: value,
                    });

                })
                .catch((error) => {
                    this.currentTime = new Date().getTime();
                    if (this.currentTime - this.startTime < 3000) {
                        this.allowTimer = false;
                    }
                    throw error;
                });
        }
    };

    // 选择弹窗
    _checkTab = (id, type, dialogType) => {
        const {isWifiConnect, deviceId} = this.state;
        //获取当前时间毫秒数
        this.startTime = new Date().getTime();
        if (isWifiConnect) {
            NetUtil.deviceControlV3(
                StorageHelper.getTempToken(),
                deviceId,
                Helper.getKey(type, keyData),
                id,
                false
            )
                .then((res) => {
                    this.currentTime = new Date().getTime();
                    if (this.currentTime - this.startTime < 3000) {
                        this.allowTimer = false;
                    }
                    this.setState({[type]: id, [dialogType]: false});
                })
                .catch((error) => {
                    this.currentTime = new Date().getTime();
                    if (this.currentTime - this.startTime < 3000) {
                        this.allowTimer = false;
                    }
                    throw error;
                });
        }
    };

    // 隐藏弹窗
    _onHide = (type) => {
        this.setState({
            [type]: false,
        });
    };

    // 隐藏弹窗
    _onShow = (type) => {
        this.setState({
            [type]: true,
        });
    };

    // 根据枚举值匹配相应选项文字
    getText = (type, value) => {
        return this[type]?.find((element) => element.id === value)?.text;
    };

    // 重试（重新连接蓝牙）
    retry = () => {
    };

    //照明灯开关
    checkLighting = (item, type) => {
        const {isWifiConnect, deviceId, lightingLantern} = this.state;
        let itemKey = lightingLantern === item.key ? "0" : item.key;
        if (isWifiConnect) {
            NetUtil.deviceControlV3(
                StorageHelper.getTempToken(),
                deviceId,
                type,
                itemKey,
                false
            )
                .then((res) => {
                    this.setState({
                        lightingLantern: itemKey,
                    });
                })
                .catch((error) => {
                    throw error;
                });
        }
    };

    // 格式化可用时间
    formatTime = (timeInMinutes = 0, type) => {
        let obj = {};
        if (timeInMinutes < 60) {
            obj["value"] = timeInMinutes;
            obj["unit"] = "分钟";
        } else {
            let hours = (timeInMinutes / 60)?.toFixed(1);
            // 剩余可用时长最多显示99.9H
            if (type === 'dischargeTime') {
                obj["value"] = hours > 99.9 ? 99.9 : hours;
            } else {
                obj["value"] = hours;
            }
            obj["unit"] = "小时";
        }
        return obj;
    };

    getBatteryImageView = () => {
        const {electricity, isOnline} = this.state;
        let ImageView;
        if (!isOnline) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/battery_offline_icon.png")}
                />
            );
        } else if (Number(electricity) === 0) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/battery_close_icon.png")}
                />
            );
        } else if (0 < Number(electricity) && Number(electricity) <= 10) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/10_battery_power_icon.png")}
                />
            );
        } else if (10 < Number(electricity) && Number(electricity) <= 20) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/20_battery_power_icon.png")}
                />
            );
        } else if (20 < Number(electricity) && Number(electricity) <= 30) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/30_battery_power_icon.png")}
                />
            );
        } else if (30 < Number(electricity) && Number(electricity) <= 40) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/40_battery_power_icon.png")}
                />
            );
        } else if (40 < Number(electricity) && Number(electricity) <= 50) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/50_battery_power_icon.png")}
                />
            );
        } else if (50 < Number(electricity) && Number(electricity) <= 60) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/60_battery_power_icon.png")}
                />
            );
        } else if (60 < Number(electricity) && Number(electricity) <= 70) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/70_battery_power_icon.png")}
                />
            );
        } else if (70 < Number(electricity) && Number(electricity) <= 80) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/80_battery_power_icon.png")}
                />
            );
        } else if (80 < Number(electricity) && Number(electricity) <= 90) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/90_battery_power_icon.png")}
                />
            );
        } else if (90 < Number(electricity) && Number(electricity) <= 100) {
            ImageView = (
                <Image
                    source={require("../../resources/greenUnion/100_battery_power_icon.png")}
                />
            );
        }
        return ImageView;
    };

    // 获取WIfi或蓝牙连接的对应图标
    getConnectionMethodIcon = () => {
        const {isWifiConnect, isOnline, blueConnectStatus} = this.state;
        let icon = (
            <Image
                source={
                    isWifiConnect
                        ? isOnline
                        ? require("../../resources/greenUnion/online_ic.png")
                        : require("../../resources/greenUnion/offline_ic.png")
                        : null
                }
                // source={
                //   isWifiConnect
                //     ? isOnline
                //       ? require("../../resources/greenUnion/online_ic.png")
                //       : require("../../resources/greenUnion/offline_ic.png")
                //     : blueConnectStatus == 2
                //     ? require("../../resources/greenUnion/bluetooth_ic.png")
                //     : require("../../resources/greenUnion/offline_ic.png")
                // }
            />
        );
        return icon;
    };

    getBlueStatusView = () => {
        const {blueConnectStatus} = this.state;
        return (
            <View
                style={{
                    backgroundColor: "#fff",
                    width: this.mScreenWidth - 24,
                    height: 54,
                    borderRadius: 12,
                    flexDirection: "row",
                    alignItems: "center",
                    paddingHorizontal: 20,
                }}
            >
                <View style={{flex: 1, flexDirection: "row"}}>
                    <Image
                        style={{marginRight: 10}}
                        source={
                            blueConnectStatus === 1
                                ? require("../../resources/greenUnion/connetingIcon.png")
                                : blueConnectStatus === 3
                                ? require("../../resources/greenUnion/failureIcon.png")
                                : null
                        }
                    />
                    <Text
                        style={{
                            fontSize: 16,
                            color: "#404040",
                        }}
                    >
                        {blueConnectStatus === 1
                            ? strings("连接中")
                            : blueConnectStatus === 3
                                ? strings("蓝牙连接失败")
                                : ""}
                    </Text>
                </View>
                {blueConnectStatus === 3 ? (
                    <TouchableOpacity onPress={() => this.retry()}>
                        <Text style={{fontSize: 16, color: "#FF9F18"}}>
                            {strings("重试")}
                        </Text>
                    </TouchableOpacity>
                ) : null}
            </View>
        );
    };

    render() {
        const {
            deviceId,
            mac,
            deviceName,
            temperatureUnit,
            temperature,
            DialogConnectDialog,
            electricity,
            closeWithOneClick,
            operatingValue,
            operatingModeTipsDialog,
            utTipsDialog,
            upgradePromptDialog,
            operatingModeDialog,
            lightingLanternData,
            totalInput,
            totalOutput,
            chargingTime,
            dischargeTime,
            acDischargePow,
            equipmentManagementIsShow,
            acIsSelected,
            UtIsSelected,
            dcIsSelected,
            usbIsSelected,
            lightingLantern,
            size,
            dcValue,
            usb1,
            usb2,
            usb3,
            usb4,
            usbc1,
            usbc2,
            usbc3,
            usbc4,
            isWifiConnect,
            blueConnectStatus,
            isOnline,
            deviceData,
        } = this.state;
        console.log('Number(totalInput)', Number(totalInput));
        // 判断是否为离线禁用状态
        let offlineDisableMode =
            (isWifiConnect && !isOnline) || (!isWifiConnect && blueConnectStatus == 3)
                ? true
                : false;

        // 顶部bar标题
        const _titleView = (
            <View
                style={{
                    flexDirection: "row",
                    height: 55,
                    marginTop: statusBarHeight,
                    alignItems: "center",
                    paddingHorizontal: 12,
                }}
            >
                <TouchableOpacity
                    style={{
                        width: 30,
                        height: 30,
                        borderRadius: 10,
                        overflow: "hidden",
                        marginRight: 11,
                    }}
                    onPress={() => {
                        this.props.navigation.goBack();
                    }}
                >
                    <Image
                        style={{
                            width: 30,
                            height: 30,
                        }}
                        source={leftIcon}
                    />
                </TouchableOpacity>
                <View style={{flex: 1, alignItems: "center"}}>
                    <Text
                        numberOfLines={1}
                        style={{
                            color: "#2F2F2F",
                            fontSize: this.getSize(17),
                            fontWeight: "bold",
                        }}
                    >
                        {deviceName}
                    </Text>
                </View>
                {/* todo */}
                <TouchableOpacity
                    style={{marginRight: 22}}
                    onPress={() => {
                        // 跳转客服页面
                        this.props.navigation.navigate("TechnicalSupport");
                    }}
                >
                    <Image
                        style={{height: 23, width: 30}}
                        source={require("../../resources/greenUnion/nav_help_ic.png")}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        // 跳转设置页面
                        this.props.navigation.navigate("BatterySetting", {
                            deviceData,
                            deviceName,
                            deviceId,
                            isOnline,
                            isWifiConnect,
                            mac
                        });
                    }}
                >
                    <Image
                        style={{height: 26, width: 26}}
                        source={require("../../resources/greenUnion/top_setting_ic.png")}
                    />
                </TouchableOpacity>
            </View>
        );
        // 弹窗
        const _dialogView = (
            <View>
                {/* 连接提示 */}
                <DialogConnect
                    cardStyle={{justifyContent: "center", alignItems: "center"}}
                    modalVisible={DialogConnectDialog}
                    onRequestClose={() => {
                        this.setState({DialogConnectDialog: false});
                    }}
                    // overViewClick={() => {
                    //     this.setState({DialogConnectDialog: false})
                    // }}
                    children={this.getBlueStatusView()}
                />
                {/* 升级提示 */}
                <CommonMessageDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({upgradePromptDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({upgradePromptDialog: false});
                    }}
                    title={strings("升级提示")}
                    children={
                        <View style={{alignItems: "center", marginVertical: 25}}>
                            <Text
                                style={{
                                    color: "#18B34F",
                                    fontSize: 16,
                                    textAlign: "center",
                                    lineHeight: 25,
                                }}
                            >
                                {strings("询问升级提示语")}
                            </Text>
                            <Text
                                style={{
                                    color: "#808080",
                                    fontSize: 16,
                                    textAlign: "center",
                                    lineHeight: 25,
                                }}
                            >
                                {strings("大小") + "：" + size}
                            </Text>
                        </View>
                    }
                    style={{paddingTop: 30}}
                    modalVisible={upgradePromptDialog}
                    leftBtnText={strings("neglect")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({upgradePromptDialog: false});
                    }}
                    rightBtnText={strings("升级")}
                    onRightBtnClick={() => {
                        //确定
                        this.setState({upgradePromptDialog: false});
                    }}
                />
                {/* UT超能模式提示 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("utTipsDialog");
                    }}
                    overViewClick={() => {
                        this._onHide("utTipsDialog");
                    }}
                    modalVisible={utTipsDialog}
                    children={
                        <View
                            style={{
                                width: this.mScreenWidth,
                                alignItems: "center",
                                marginVertical: 25,
                                paddingHorizontal: 20,
                            }}
                        >
                            <Text
                                style={{
                                    color: "#969AA1",
                                    marginTop: 35,
                                    marginBottom: 35,
                                    lineHeight: 25,
                                    fontWeight: "bold",
                                    fontSize: 14,
                                }}
                            >
                                {strings("UT超能模式提示")}
                                <Text
                                    style={{textDecorationLine: "underline"}}
                                    onPress={() => {
                                        this._onHide("utTipsDialog");
                                        // 关闭弹窗后跳转至说明书页面
                                    }}
                                >
                                    {strings("查阅说明书")}
                                </Text>
                                {strings("。")}
                            </Text>
                        </View>
                    }
                    title={strings("UT超能模式")}
                    message={strings("UT超能模式提示")}
                    rightBtnText={strings("我知道了")}
                    onRightBtnClick={() => {
                        //确定
                        this._onHide("utTipsDialog");
                    }}
                />
                {/* 工作模式选择弹窗 */}
                <ChooseDialog
                    title={strings("工作模式")}
                    show={operatingModeDialog}
                    data={this.operatingModeData}
                    currentId={operatingValue}
                    _checkTab={(data) => {
                        if (this.state.isFirstUseWorkMode) {
                            //workmode是否第一次使用是的话 先弹窗
                            this._onShow("operatingModeTipsDialog")
                            StorageHelper.saveIsFirstUseWorkMode(false);
                            this.setState({isFirstUseWorkMode: false})
                            return
                        }
                        this._checkTab(data.id, "operatingValue", "operatingModeDialog");
                    }}
                    _onHide={() => this._onHide("operatingModeDialog")}
                />
                {/* 工作模式提示 */}
                <CommonMessageDialog
                    leftBtnTextColor={"#ffffff"}
                    onLeftBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this._onHide("operatingModeTipsDialog");
                    }}
                    overViewClick={() => {
                        this._onHide("operatingModeTipsDialog");
                    }}
                    title={strings("设备工作模式")}
                    children={
                        <View
                            style={{
                                width: this.mScreenWidth,
                                alignItems: "center",
                                marginVertical: 25,
                                paddingHorizontal: 20,
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    width: "100%",
                                    marginBottom: 20,
                                }}
                            >
                                <Image
                                    style={{marginRight: 15, width: 32, height: 32}}
                                    source={require("../../resources/greenUnion/standardModeIcon.png")}
                                />
                                <View style={{flex: 1}}>
                                    <Text
                                        style={{
                                            color: "#404040",
                                            fontSize: 18,
                                            fontWeight: "bold",
                                        }}
                                    >
                                        {strings("标准模式")}
                                    </Text>
                                    <Text
                                        style={{
                                            color: "#808080",
                                            fontSize: 14,
                                        }}
                                    >
                                        {strings("标准模式提示")}
                                    </Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    width: "100%",
                                    marginBottom: 20,
                                }}
                            >
                                <Image
                                    style={{marginRight: 15, width: 32, height: 32}}
                                    source={require("../../resources/greenUnion/energySavingModeIcon.png")}
                                />
                                <View style={{flex: 1}}>
                                    <Text
                                        style={{
                                            color: "#404040",
                                            fontSize: 18,
                                            fontWeight: "bold",
                                        }}
                                    >
                                        {strings("节能模式")}
                                    </Text>
                                    <Text
                                        style={{
                                            color: "#808080",
                                            fontSize: 14,
                                        }}
                                    >
                                        {strings("节能模式提示")}
                                    </Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    width: "100%",
                                    marginBottom: 20,
                                }}
                            >
                                <Image
                                    style={{marginRight: 15, width: 32, height: 32}}
                                    source={require("../../resources/greenUnion/frequentlyOpenModeIcon.png")}
                                />
                                <View style={{flex: 1}}>
                                    <Text
                                        style={{
                                            color: "#404040",
                                            fontSize: 18,
                                            fontWeight: "bold",
                                        }}
                                    >
                                        {strings("常开模式")}
                                    </Text>
                                    <Text
                                        style={{
                                            color: "#808080",
                                            fontSize: 14,
                                        }}
                                    >
                                        {strings("常开模式提示")}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    }
                    style={{paddingTop: 30}}
                    modalVisible={operatingModeTipsDialog}
                    leftBtnText={strings("我知道了")}
                    onLeftBtnClick={() => {
                        //确定
                        this._onHide("operatingModeTipsDialog");
                    }}
                />
            </View>
        );
        // 功率参数
        const _batteryShowView = (
            <>
                <View
                    style={{
                        flexDirection: "row",
                        height: 20,
                        width: "100%",
                        alignItems: "center",
                    }}
                >
                    {this.getConnectionMethodIcon()}
                    <Text
                        style={{
                            fontSize: 16,
                            fontWeight: "bold",
                            color: offlineDisableMode ? "#ccc" : "#404040",
                        }}
                    >
                        {"  |  "}
                        {Helper.temperatureUnitConvert(offlineDisableMode, temperatureUnit, temperature)}
                    </Text>
                </View>
                <View
                    style={{
                        alignItems: "center",
                        height: this.mScreenHeight * 0.7,
                        width: "100%",
                        justifyContent: "center",
                    }}
                >
                    <Text
                        style={{
                            fontSize: 28,
                            fontWeight: "bold",
                            color: "#222222",
                            lineHeight: 50,
                        }}
                    >
                        {electricity && !offlineDisableMode ? electricity + "%" : ""}
                    </Text>
                    {this.getBatteryImageView()}
                </View>
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "space-around",
                        backgroundColor: "#fff",
                        borderRadius: 10,
                        paddingVertical: 15,
                        marginBottom: 10,
                    }}
                >
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <View style={{height: "100%", alignItems: "center"}}>
                            <View
                                style={{
                                    justifyContent: "flex-end",
                                    height: 40,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 26,
                                        color: offlineDisableMode ? "#ccc" : "#404040",
                                    }}
                                >
                                    {offlineDisableMode ? "- " : Number(totalInput)}
                                </Text>
                            </View>
                            <Text
                                style={{
                                    fontSize: 12,
                                    color: offlineDisableMode ? "#ccc" : "#808080",
                                    lineHeight: 30,
                                }}
                            >
                                {strings("总输入")}
                            </Text>
                        </View>
                        <View style={{height: "100%", alignItems: "center"}}>
                            <View
                                style={{
                                    justifyContent: "flex-end",
                                    height: 37,
                                    marginLeft: 3,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 12,
                                        color: offlineDisableMode ? "#ccc" : "#404040",
                                    }}
                                >
                                    w
                                </Text>
                            </View>
                        </View>
                    </View>


                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <View style={{height: "100%", alignItems: "center"}}>
                            <View
                                style={{
                                    justifyContent: "flex-end",
                                    height: 40,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 26,
                                        color: offlineDisableMode ? "#ccc" : "#404040",
                                    }}
                                >
                                    {offlineDisableMode ? "- " : Number(totalOutput)}
                                </Text>
                            </View>
                            <Text
                                style={{
                                    fontSize: 12,
                                    color: offlineDisableMode ? "#ccc" : "#808080",
                                    lineHeight: 30,
                                }}
                            >
                                {strings("总输出")}
                            </Text>
                        </View>
                        <View style={{height: "100%", alignItems: "center"}}>
                            <View
                                style={{
                                    justifyContent: "flex-end",
                                    height: 37,
                                    marginLeft: 3,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 12,
                                        color: offlineDisableMode ? "#ccc" : "#404040",
                                    }}
                                >
                                    w
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                        }}
                    >
                        <View style={{height: "100%", alignItems: "center"}}>
                            <View
                                style={{
                                    justifyContent: "flex-end",
                                    height: 40,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 26,
                                        color: offlineDisableMode ? "#ccc" : "#404040",
                                    }}
                                >
                                    {offlineDisableMode
                                        ? "- "
                                        : Number(totalInput) > 0
                                            ? this.formatTime(Number(chargingTime)).value
                                            : this.formatTime(Number(dischargeTime), 'dischargeTime').value}
                                </Text>
                            </View>
                            <Text
                                style={{
                                    fontSize: 12,
                                    color: offlineDisableMode ? "#ccc" : "#808080",
                                    lineHeight: 30,
                                }}
                            >
                                {Number(totalInput) > 0 ? strings('充电时间') : strings('可用时间')}
                            </Text>
                        </View>
                        <View style={{height: "100%", alignItems: "center"}}>
                            <View
                                style={{
                                    justifyContent: "flex-end",
                                    height: 37,
                                    marginLeft: 3,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 12,
                                        color: offlineDisableMode ? "#ccc" : "#404040",
                                    }}
                                >
                                    {strings(
                                        Number(totalInput) > 0
                                            ? this.formatTime(Number(chargingTime)).unit
                                            : this.formatTime(Number(dischargeTime)).unit
                                    )}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </>
        );

        // 设备管理选项标题
        const _equipmentManagement = (
            <ShowOrHideBtnView
                boxStyle={{borderBottomWidth: 0}}
                containerStyle={{borderBottomWidth: 0}}
                onPress={() => {
                    this.setState({
                        equipmentManagementIsShow: !equipmentManagementIsShow,
                    });
                }}
                text={strings("设备管理")}
                rightIcon={rightIcon}
                disabled={offlineDisableMode}
            />
        );

        // AC交流
        const _acView = (
            <View
                style={{
                    backgroundColor: "#fff",
                    borderRadius: 10,
                    width: this.mScreenWidth - 40,
                    paddingVertical: 5,
                    marginBottom: 10,
                }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "space-between",
                        alignItems: "center",
                        paddingHorizontal: 15,
                        height: 64,
                        borderBottomColor: "#EBEBEB",
                        borderBottomWidth: 0.5,
                    }}
                >
                    <View>
                        <Text style={{fontSize: this.getSize(14), color: "#404040"}}>
                            {strings("AC交流")}
                        </Text>
                        <Text style={{fontSize: 26, color: "#404040"}}>
                            {acDischargePow}
                            <Text style={{fontSize: 12}}> w</Text>
                        </Text>
                    </View>
                    <SwitchBtn
                        key={"acIsSelected"}
                        type={"acIsSelected"}
                        value={Number(acIsSelected)}
                        onValueChange={this.onValueChange}
                    />
                </View>
                <SwitchBtnView
                    title={strings("UT")}
                    onValueChange={this.onValueChange}
                    type={"UtIsSelected"}
                    dialogType={"utTipsDialog"}
                    value={Number(UtIsSelected)}
                    tipsOnClick={this._onShow}
                    style={{borderBottomWidth: 0}}
                />
            </View>
        );
        // DC直流
        const _dcView = (
            <View
                style={{
                    backgroundColor: "#fff",
                    borderRadius: 10,
                    width: this.mScreenWidth - 40,
                    paddingVertical: 5,
                    marginBottom: 10,
                }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "space-between",
                        alignItems: "center",
                        paddingHorizontal: 15,
                        height: 64,

                    }}
                >
                    <View>
                        <Text style={{fontSize: this.getSize(14), color: "#404040"}}>
                            {strings("DC直流")}
                        </Text>
                        <Text style={{fontSize: 26, color: "#404040"}}>
                            {dcValue}
                            <Text style={{fontSize: 12}}> w</Text>
                        </Text>
                    </View>
                    <SwitchBtn
                        key={"dcIsSelected"}
                        type={"dcIsSelected"}
                        value={Number(dcIsSelected)}
                        onValueChange={this.onValueChange}
                    />
                </View>
            </View>
        );
        // USB
        const _usbView = (
            <View
                style={{
                    backgroundColor: "#fff",
                    borderRadius: 10,
                    width: this.mScreenWidth - 40,
                    marginBottom: 10,
                    paddingVertical: 15,
                }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "space-between",
                        alignItems: "center",
                        paddingHorizontal: 15,
                    }}
                >
                    <View style={{width: "100%"}}>
                        <View
                            style={{
                                flexDirection: "row",
                                width: "100%",
                                justifyContent: "space-between",
                                marginBottom: 10,
                            }}
                        >
                            <Text style={{fontSize: this.getSize(14), color: "#404040"}}>
                                USB
                            </Text>
                            <SwitchBtn
                                key={"usbIsSelected"}
                                type={"usbIsSelected"}
                                value={Number(usbIsSelected)}
                                onValueChange={this.onValueChange}
                            />
                        </View>

                        <View
                            style={{
                                flexDirection: "row",
                                width: "100%",
                                flexWrap: "wrap",
                            }}
                        >
                            {usbc1 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USBC.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usbc1) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usbc1)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                            {usbc2 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USBC.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usbc2) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usbc2)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                            {usbc3 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USBC.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usbc3) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usbc3)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                            {usbc4 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USBC.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usbc4) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usbc4)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}

                            {usb1 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USB.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usb1) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usb1)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                            {usb2 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USB.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usb2) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usb2)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                            {usb3 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USB.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usb3) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usb3)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                            {usb4 !== undefined ? (
                                <View
                                    style={{
                                        alignItems: "center",
                                        width: (this.mScreenWidth - 70) / 4,
                                    }}
                                >
                                    <Image
                                        source={require("../../resources/greenUnion/USB.png")}
                                    />
                                    <Text
                                        numberOfLines={1}
                                        style={{
                                            fontSize: 26,
                                            color: Number(usb4) ? "#404040" : "#C4C6CD",
                                        }}
                                    >
                                        {Number(usb4)}
                                        <Text style={{fontSize: 12}}> w</Text>
                                    </Text>
                                </View>
                            ) : null}
                        </View>
                        <View
                            style={{
                                flexDirection: "row",
                                width: "100%",
                                justifyContent: "space-around",
                            }}
                        ></View>
                    </View>
                </View>
            </View>
        );

        // 照明灯
        const lightingLanternView = (
            <View
                style={{
                    backgroundColor: "#fff",
                    borderRadius: 10,
                    width: this.mScreenWidth - 40,
                    paddingBottom: 15,
                    marginBottom: 10,
                }}
            >
                <View style={{width: "100%", paddingHorizontal: 15}}>
                    <Text
                        style={{
                            fontSize: this.getSize(14),
                            color: "#404040",
                            lineHeight: 50,
                        }}
                    >
                        {strings("照明灯")}
                    </Text>
                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                            height: 46,
                            width: this.mScreenWidth - 70,
                            backgroundColor: "#F1F2F6",
                            borderRadius: 23,
                            overflow: "hidden",
                            paddingHorizontal: 4,
                        }}
                    >
                        {lightingLanternData?.map((item, index) => (
                            <TouchableOpacity
                                onPress={() => {
                                    this.checkLighting(item, "lamp_sw");
                                }}
                                key={index}
                                style={{
                                    flex: 1,
                                    height: 39,
                                    alignItems: "center",
                                    justifyContent: "center",
                                    backgroundColor:
                                        lightingLantern === item.key ? "#fff" : "#F1F2F6",
                                    borderRadius: 23,
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 14,
                                        color: lightingLantern === item.key ? "#404040" : "#C4C6CD",
                                    }}
                                >
                                    {item.title}
                                </Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>
            </View>
        );
        // 一键关闭
        const _oneButtonClosure = (
            <ItemBtnView
                style={{borderRadius: 10, marginBottom: 10}}
                rightIconContainerStyle={{
                    borderRadius: 20,
                    backgroundColor: "#F1F2F6",
                    width: 38,
                    height: 38,
                    justifyContent: "center",
                    alignItems: "center",
                }}
                onPress={() => {
                    this.onValueChange(
                        "1",
                        "closeWithOneClick"
                    );
                }}
                text={strings("一键关闭")}
                rightIcon={
                    Number(closeWithOneClick)
                        ? require("../../resources/greenUnion/closeTheIconWithOneClickSel.png")
                        : require("../../resources/greenUnion/closeTheIconWithOneClick.png")
                }
            />
        );

        // 工作模式
        const _operatingModeView = (
            <ItemBtnView
                style={{borderRadius: 10, marginBottom: 10}}
                onPress={() => {
                    this._onShow("operatingModeDialog");
                }}
                disabled={offlineDisableMode}
                text={strings("工作模式")}
                leftIcon2={require("../../resources/greenUnion/tips_ic.png")}
                rightText={this.getText("operatingModeData", operatingValue)}
                rightIcon={rightIcon}
                onLeftIcon2Press={() => {
                    this._onShow("operatingModeTipsDialog");
                }}
            />
        );

        // 加电包
        const _chargingPackageView = (
            <ItemBtnView
                style={{borderRadius: 10, marginBottom: 10}}
                onPress={() => {
                    this.props.navigation.navigate("ChargingPackage");
                }}
                text={strings("加电包")}
                rightIcon={rightIcon}
            />
        );

        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F7F7F7",
                    alignItems: "center",
                    height: this.mScreenHeight,
                    width: this.mScreenWidth,
                }}
            >
                {_titleView}
                <ScrollView style={{flex: 1}}>
                    <View style={{width: this.mScreenWidth, alignItems: "center"}}>
                        <View
                            style={{
                                width: this.mScreenWidth,
                                paddingHorizontal: 20,
                                alignItems: "center",
                            }}
                        >
                            {_batteryShowView}
                            {_equipmentManagement}
                            {equipmentManagementIsShow ? (
                                <>
                                    {_acView}
                                    {_dcView}
                                    {_usbView}
                                    {lightingLanternView}
                                    {_oneButtonClosure}
                                </>
                            ) : null}
                            {_operatingModeView}
                            {/* {_chargingPackageView} */}
                        </View>
                    </View>
                </ScrollView>
                {_dialogView}
            </View>
        );
    }
}

export class SwitchBtnView extends BaseComponent {
    render() {
        const {
            onValueChange,
            type,
            value,
            tipsOnClick,
            dialogType,
            title,
            contraryMode,
            style
        } = this.props;
        return (
            <View
                style={[{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    borderBottomColor: "#EBEBEB",
                    borderBottomWidth: 0.5,
                    alignItems: "center",
                    marginHorizontal: 15,
                    height: 64,
                }, style]}
            >
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Text style={{fontSize: this.getSize(14), color: "#404040"}}>
                        {title}
                    </Text>
                    {tipsOnClick ? (
                        <TouchableOpacity onPress={() => tipsOnClick?.(dialogType)}>
                            <Image
                                style={{marginHorizontal: 7}}
                                source={require("../../resources/greenUnion/tips_ic.png")}
                            />
                        </TouchableOpacity>
                    ) : null}
                </View>
                <SwitchBtn
                    type={type}
                    value={value}
                    onValueChange={onValueChange}
                    contraryMode={contraryMode}
                />
            </View>
        );
    }
}
