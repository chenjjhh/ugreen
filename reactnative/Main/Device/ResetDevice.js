// 主页设备列表/添加设备/重置设备
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio,ScrollView,DeviceEventEmitter,NativeModules
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from '../View/TitleBar'
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from '../View/ItemBtnView'
import TextInputComponent from '../View/TextInputComponent'
import CommonBtnView from '../View/CommonBtnView'
import UpLoadView from '../View/UpLoadView'
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";

var GXRNManager = NativeModules.GXRNManager
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
const leftIcon = (require('../../resources/back_black_ic.png'))
const deviceIcon = (require('../../resources/greenUnion/connect_network_icon.png'))
const noSelectIcon = (require('../../resources/greenUnion/unsel_ic.png'))
const selectIcon = (require('../../resources/greenUnion/sel_ic.png'))
export default class ResetDevice extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
            <TitleBar
                    titleText={strings('重置设备')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={leftIcon}
                    rightIcon={rightIcon}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            confirmDistributionNetworkStatus: false,//是否确认配网状态
      }
    }

    // 下一步
    nextStep = ()=>{
        const { confirmDistributionNetworkStatus} = this.state;
        if(!confirmDistributionNetworkStatus){
            this.onShowToast(strings('请确认设备处于配网状态'))
        }

    }
    
    
    render() {
        const { confirmDistributionNetworkStatus } = this.state
        const topView = (
            <View style={{width:'100%',paddingLeft:20,marginTop:20,alignItems:'center'}}>
                <View style={{width:'100%',marginBottom:30}}>
                    <Text style={{fontSize:22,color:'#2F2F2F',lineHeight:40}}>{strings('重置引导')}</Text>
                    <Text style={{fontSize:14,color:'#999BA2'}}>{strings('find_device_tips')}</Text>
                </View>
                <Image  source={deviceIcon}/>
            </View>
        )
        const nextSteptBtnView = (
            <View>
                <TouchableOpacity  
                    onPress={()=>{
                        this.setState({
                        confirmDistributionNetworkStatus:!confirmDistributionNetworkStatus
                    })}} 
                    style={{flexDirection:'row',alignItems:'center',justifyContent:'center',marginBottom:20}}>
                    <Image style={{marginHorizontal:10}} source={confirmDistributionNetworkStatus ? selectIcon : noSelectIcon}/>
                    <Text style={{fontSize:14,color:'#999BA2'}}>{strings('重置提示')}</Text>
                </TouchableOpacity>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 下一步
                        this.nextStep()
                    }}
                    style={{
                        backgroundColor: '#18B34F',
                        width:(this.mScreenWidth-40)*0.9,
                    }}
                    btnText={strings('next')}/>
            </View>
                )
        
        return (
            <View style={{flex:1,backgroundColor:'#fff',alignItems:'center',height:this.mScreenHeight,width:this.mScreenWidth}}>
                <ScrollView style={{flex:1,}}>
                    <View style={{width:this.mScreenWidth,alignItems:'center'}}>
                        <View style={{width:this.mScreenWidth,paddingHorizontal:20,alignItems:'center'}}>
                            <View 
                                style={{
                                overflow: 'hidden',
                                width:this.mScreenWidth-40,
                                height:this.mScreenHeight,
                                paddingVertical:25,
                                justifyContent:'space-between',
                                alignItems:'center',
                                }}>
                                {topView}
                                {nextSteptBtnView}
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}