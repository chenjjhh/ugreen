// 主页设备列表/添加设备/发现设备列表/选择网络（登录WIFI）
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    RefreshControl,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio,ScrollView,DeviceEventEmitter,NativeModules
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import TextInputView from '../View/TextInputView'
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from '../View/TitleBar'
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from '../View/ItemBtnView'
import TextInputComponent from '../View/TextInputComponent'
import CommonBtnView from '../View/CommonBtnView'
import UpLoadView from '../View/UpLoadView'
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";

var GXRNManager = NativeModules.GXRNManager
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
const connectWifiIcon = (require('../../resources/greenUnion/connect_wifi_ic.png'))
const leftIcon = (require('../../resources/back_black_ic.png'))

export default class ChooseNetwork extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
            <TitleBar
                    titleText={strings('选择网络')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={leftIcon}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }
    constructor(props) {
        super(props);
        this.state = {
            passwordText:'',//密码
            WifiDialog:true,//waifi列表弹窗
            selectedData:{},//当前选中网络数据
      };
      this.WifiData = [
        {text: 'UGREEN_WiFi_01', status:0, id: 0},
        {text: 'UGREEN_WiFi_02', status:1, id: 1},
        {text: 'UGREEN_WiFi_03', status:1, id: 2},
        {text: 'UGREEN_WiFi_04', status:1, id: 3},
        {text: 'UGREEN_WiFi_05', status:1, id: 4},
    ];
    }

    // 手动添加
    nextStep = () =>{

    }

    // 直接进入
    enterDirectly = ()=>{

    }


    // 选择网络
    _checkTab = (selectedData)=>{
        this.setState({
            selectedData,
            WifiDialog:false
        })
    }

    // 隐藏弹窗
    onHide=(type)=>{
        this.setState({
            [type]: false
        })
    }

    // 显示弹窗
    onShow=(type)=>{
        this.setState({
            [type]: true
        })
    }

    
    render() {
        const { passwordText, Wifi, selectedData, WifiDialog } = this.state
        // 下一步按钮
        const nextStepBtnView = (
            <View style={{width:'100%',alignItems:'center'}}>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 下一步
                        this.nextStep()
                    }}
                    style={{
                        backgroundColor: '#18B34F',
                        width:(this.mScreenWidth-40)*0.9,
                        marginVertical:20
                    }}
                    btnText={strings('next')}/>
            </View>)
        const enterDirectlyView =  (
            <View style={{width:'100%',alignItems:'center'}}>
                <TouchableOpacity onPress={()=>this.enterDirectly()}>
                    <Text style={{fontSize:14,color:'#18B34F'}}>{strings('暂不联网进入')}</Text>
                </TouchableOpacity>
            </View>
        )
        const netWorkView = (
            <TouchableOpacity 
                onPress={()=>this.onShow('WifiDialog')} 
                style={{ 
                    height: 50,
                    width:'100%',
                    marginBottom:18,
                    backgroundColor: '#F7F8FB',
                    borderRadius: 27,
                    flexDirection: 'row',
                    alignItems: 'center',
                    overflow: 'hidden',
                    }}>
                    <View style={{paddingHorizontal:20}}>
                        <Image
                            style={[{
                                resizeMode: 'contain'
                            }]}
                            source={connectWifiIcon}/>
                    </View>
                    <View style={{flex:1}}>
                        <Text style={{color:'#404040',fontSize:16}} numberOfLines={1}>{ selectedData?.text }</Text>
                    </View>
                    <View style={{paddingHorizontal:20}}>
                        <Image
                            style={[{
                                resizeMode: 'contain'
                            }]}
                            source={rightIcon}/>
                    </View>
            </TouchableOpacity>
        )

        // 密码
        const passwordView = (
            <TextInputView
                key={0}
                style={{
                    marginBottom:18,
                    width:'100%'
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText: newText})
                }}
                value={passwordText}
                placeholder={strings('input_wifi_pd')}
                keyboardType={'default'}
                placeholderTextColor={'#C4C6CD'}/>)

        // 主要失败原因
        const failureTipsView = (
            <View style={{width:'100%',paddingHorizontal:30}}>
                <Text style={{fontSize:14,color:'#999BA2'}}>{strings('配网失败主要原因提示')}</Text>
            </View>
        )

        // 弹窗
        const dialogView = (
                <ChooseNetWordDialog 
                    title={strings('可添加设备的WiFi')}
                    show={WifiDialog}
                    data={this.WifiData}
                    _checkTab={this._checkTab}
                    onHide={this.onHide}
                 />
        )
        return (
            <View style={{flex:1,backgroundColor:'#fff',height:this.mScreenHeight,width:this.mScreenWidth,justifyContent:'space-between'}}>
                <View style={{width:'100%',paddingLeft:20}}>
                    <Text style={{fontSize:22,color:'#2F2F2F'}}>{strings('选择网络提示')}</Text>
                    <Text style={{fontSize:14,color:'#999BA2'}}>{strings('确认所选网络提示')}</Text>
                </View>
                <View style={{flex:1,justifyContent:'space-between',paddingBottom:20}}>
                        <View>
                            <View style={{paddingHorizontal:20,marginBottom:50}}>
                                <View style={{alignItems:'center'}}>
                                    <Image source={require('../../resources/greenUnion/connect_wifi_tips_ic.png')}/>
                                </View>
                                {netWorkView}
                                {passwordView}
                                {failureTipsView}
                            </View>
                            {nextStepBtnView}
                        </View>
                        {enterDirectlyView}
                </View>
                {dialogView}
            </View>
        );
    }
}


/*
*选择网络列表弹窗
 */
export class ChooseNetWordDialog extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    _checkTab = (item,index) =>{
        const { _checkTab } = this.props;
        _checkTab?.(item)
    }

    render() {
        const { data, title, show, onHide } = this.props;
        return (
            <Modal
                statusBarTranslucent={true}
                animationType={"fade"}
                transparent={true}
                visible={show}
                onRequestClose={() => {

                }}>
                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.22)',
                }}>
                    <TouchableOpacity onPress={()=>{onHide?.('WifiDialog')}} style={{flex:1,}}></TouchableOpacity>
                    <View style={[{width:this.mScreenWidth,height:328,paddingBottom:10,backgroundColor:'#ffffff',borderTopLeftRadius:10,borderTopRightRadius:10}]}>
                        <View style={{height:68,justifyContent:'center',borderBottomWidth:0.5,borderBottomColor:'#EBEBEB',marginHorizontal:20,}}>
                            <Text style={{color:'#2F2F2F',fontSize: 22,fontWeight:'bold'}}>{title}</Text>
                        </View>
                        <ScrollView style={{flex:1}}>
                            <View style={{justifyContent:'center',alignItems:'center',marginHorizontal:20}}>
                                {data?.map((item,index)=>(
                                    <TouchableOpacity style={{width:'100%',height:65,justifyContent:'center',borderBottomWidth:0.5,borderBottomColor:'#EBEBEB',}} key={index} onPress={()=>this._checkTab(item,index)}>
                                        <Text numberOfLines={1} style={{color:'#2F2F2F',fontSize: 16,fontWeight:'bold'}}>{item?.text || ''}</Text>
                                        <Text style={{color:'#999BA2',fontSize: 14,}}>{item.status ? strings('已保存') : ''}</Text>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        );
    }
}