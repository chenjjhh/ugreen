import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput, PixelRatio
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import ShadowCardView from "../View/ShadowCardView";
import CommonBtnView from "../View/CommonBtnView";
import CommonTextView from "../View/CommonTextView";
import ViewHelper from "../View/ViewHelper";

/*
*设备规格
 */

const powerUnit = 'w'
export default class DeviceSpecifications extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={'#FBFBFB'}
                    titleText={strings('device_spec')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            specData: props.navigation.state.params.specData ? props.navigation.state.params.specData : '',
            mac: props.navigation.state.params.mac,
            showMac: props.navigation.state.params.showMac,

            capacity: 0,
            weight1: 0,
            weight2: 0,
            cycleCharge1: 0,
            cycleCharge2: 0,
            chargeTempMin: 0,
            chargeTempMax: 0,
            chargeTempT: 0,
            disChargeTempMin: 0,
            disChargeTempMax: 0,
            disChargeTempT: 0,
            inputAcMin: 0,
            inputAcMax: 0,
            inputPvMax: 0,
            inputWindElcMax: 0,
            batteryPackNum: 0,
            acOutputMaxPower: 0,
            socket20ANum: 0,
            socket30ANum: 0,
            dcOutputMaxPower: 0,
            usbANum: 0,
            usbCNum: 0,
            wirelessChargeNum: 0,
            cigarette12vNum: 0,
            dc12VNum: 0,
            xt60Num: 0,
            deviceType: '——',
            workTempMin: 0,
            workTempMax: 0,
            continueOutputPower: 0,
            maxPower: 0,
        }
        this.cardFlexWidth = (this.mScreenWidth - 42) / 3
    }

    componentWillMount() {
        this._resolveSpecData()
    }

    _resolveSpecData() {
        if (this.state.specData) {
            var specDatas = this.state.specData

            this.setState({
                // capacity: specDatas.capacity,
                // weight1: specDatas.weight1,
                // weight2: specDatas.weight2,
                // cycleCharge1: specDatas.cycleCharge1,
                // cycleCharge2: specDatas.cycleCharge2,
                // chargeTempMin: specDatas.chargeTempMin,
                // chargeTempMax: specDatas.chargeTempMax,
                // chargeTempT: specDatas.chargeTempT,
                // disChargeTempMin: specDatas.disChargeTempMin,
                // disChargeTempMax: specDatas.disChargeTempMax,
                // disChargeTempT: specDatas.disChargeTempT,
                // inputAcMin: specDatas.inputAcMin,
                // inputAcMax: specDatas.inputAcMax,
                // inputPvMax: specDatas.inputPvMax,
                // inputWindElcMax: specDatas.inputWindElcMax,
                // batteryPackNum: specDatas.batteryPackNum,
                // acOutputMaxPower: specDatas.acOutputMaxPower,
                // socket20ANum: specDatas.socket20ANum,
                // socket30ANum: specDatas.socket30ANum,
                // dcOutputMaxPower: specDatas.dcOutputMaxPower,
                // usbANum: specDatas.usbANum,
                // usbCNum: specDatas.usbCNum,
                // wirelessChargeNum: specDatas.wirelessChargeNum,
                // cigarette12vNum: specDatas.cigarette12vNum,
                // dc12VNum: specDatas.dc12VNum,
                // xt60Num: specDatas.xt60Num,
                deviceType: specDatas.deviceType
            })
            if (specDatas.deviceType == '——') {
                this.setState({deviceType: 'Gendome Home 3000'})
            }
        }
        this.setState({
            capacity: 3072,
            weight1: 36,
            cycleCharge1: 2500,
            cycleCharge2: 80,
            workTempMin: -10,
            workTempMax: 55,
            continueOutputPower: 3000,
            maxPower: 6000,

            inputAcMax: 3000,
            inputPvMax: 1500,
            inputWindElcMax: 200,
            batteryPackNum: 2,
            acOutputMaxPower: 3000,
            socket20ANum: 3,
            socket30ANum: 1,
            dcOutputMaxPower: 702,
            usbANum: 4,
            usbCNum: 2,
            wirelessChargeNum: 2,
            cigarette12vNum: 1,
            dc12VNum: 2,
            xt60Num: 1,
        })
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                }}>
                <ScrollView>
                    <View
                        style={{
                            width: this.mScreenWidth,
                            alignItems: 'center'
                        }}>
                        {this._deviceImageView()}
                        {this._deviceNameTypeView()}
                        {this._view1()}
                        {this._inputView()}
                        {this._acMaxView()}
                        {this._dcMaxView()}
                    </View>
                </ScrollView>
            </View>
        );
    }

    _deviceImageView() {
        return (
            <Image
                style={{
                    marginTop: 30,
                    width: this.mScreenWidth * 0.4,
                    height: this._getHeightByWH(149, 143, this.mScreenWidth * 0.4)
                }}
                source={require('../../resources/add_dev_dev_img.png')}/>
        )
    }

    _deviceNameTypeView() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CommonTextView
                    textSize={22}
                    style={{
                        color: '#000',
                    }}
                    text={this.state.deviceType}/>

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#B5BAC5',
                        marginTop: 5
                    }}
                    text={'SN:' + (this.state.showMac ? this.state.showMac : this.state.mac)}/>
            </View>
        )
    }

    _view1() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 30
                }}>
                <View
                    style={{
                        paddingBottom: 6,
                        flexDirection: 'row'
                    }}>
                    {this._cardItemView(strings('capacity'), this.state.capacity + 'Wh')}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('weight'), this.state.weight1 + 'kg')}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('cycling_charge'), strings('times')(this.state.cycleCharge1) + '\n' + strings('remaining')(this.state.cycleCharge2))}
                </View>

                <View
                    style={{
                        paddingBottom: 6,
                        flexDirection: 'row'
                    }}>
                    {this._cardItemView(strings('work_ev_temp'), this.state.workTempMin + '~' + this.state.workTempMax + '℃')}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('input_power_ing'), this.state.continueOutputPower + powerUnit)}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('max_power'), this.state.maxPower + powerUnit)}
                </View>
            </View>
        )
    }

    //输入
    _inputView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 30
                }}>
                {this._titleView(strings('spec_input'))}

                <View
                    style={{
                        paddingBottom: 6,
                        flexDirection: 'row',
                        paddingTop: 6
                    }}>
                    {this._cardItemView(strings('ac'), strings('max') + this.state.inputAcMax + powerUnit)}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('pv'), strings('max') + this.state.inputPvMax + powerUnit)}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('wind_elc'), strings('max') + this.state.inputWindElcMax + powerUnit)}
                </View>

                <View
                    style={{
                        paddingBottom: 6,
                        flexDirection: 'row',
                        marginLeft: 14
                    }}>
                    {this._cardItemViewFixWidth(strings('bat_packge'), this.state.batteryPackNum + strings('ge'), (this.cardFlexWidth * 2) - 30)}
                    {ViewHelper.getFlexView()}
                </View>
            </View>
        )
    }

    //交流输出-最大3000W
    _acMaxView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 30
                }}>
                {this._titleView(strings('ac_out_max')(this.state.acOutputMaxPower))}

                <View
                    style={{
                        paddingBottom: 6,
                        flexDirection: 'row',
                        paddingTop: 6,
                        marginLeft: 14
                    }}>
                    {this._cardItemView(strings('socket_20a'), this.state.socket20ANum + strings('ge'))}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('socket_30a'), this.state.socket30ANum + strings('ge'))}
                    {ViewHelper.getFlexView()}
                </View>
            </View>
        )
    }

    //直流输出-最大702W
    _dcMaxView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    marginTop: 30
                }}>
                {this._titleView(strings('dc_output_max')(this.state.dcOutputMaxPower))}

                <View
                    style={{
                        paddingBottom: 6,
                        flexDirection: 'row',
                        paddingTop: 6,
                    }}>
                    {this._cardItemView(strings('usb_a'), this.state.usbANum + strings('ge'))}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('usb_c'), this.state.usbCNum + strings('ge'))}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('wireless_charge'), this.state.wirelessChargeNum + strings('ge'))}
                </View>

                <View
                    style={{
                        paddingBottom: 20,
                        flexDirection: 'row',
                    }}>
                    {this._cardItemView(strings('cigra_12v'), this.state.cigarette12vNum + strings('ge'))}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('dc_12v'), this.state.dc12VNum + strings('ge'))}
                    {this._itemDivideView()}
                    {this._cardItemView(strings('xt_60'), this.state.xt60Num + strings('ge'))}
                </View>
            </View>
        )
    }

    _itemDivideView() {
        return (
            <View
                style={{
                    width: 6,
                    height: 1
                }}/>
        )
    }

    _cardItemView(title, value) {
        return (
            <ShadowCardView
                style={{
                    width: this.cardFlexWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: this._getHeightByWH(111, 76, this.cardFlexWidth),
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#B5BAC5',
                    }}
                    text={title}/>

                <CommonTextView
                    textSize={16}
                    style={{
                        color: '#000',
                        marginTop: 7,
                        textAlign: 'center'
                    }}
                    text={value}/>
            </ShadowCardView>
        )
    }

    _cardItemViewFixWidth(title, value, itemWidth) {
        return (
            <ShadowCardView
                style={{
                    width: itemWidth,
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 76,
                }}>
                <CommonTextView
                    textSize={16}
                    style={{
                        color: '#B5BAC5',
                    }}
                    text={title}/>

                <CommonTextView
                    textSize={16}
                    style={{
                        color: '#000',
                        marginTop: 7
                    }}
                    text={value}/>
            </ShadowCardView>
        )
    }

    _titleView(title) {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    paddingLeft: 14,
                    paddingRight: 14
                }}>
                <CommonTextView
                    textSize={18}
                    style={{
                        color: '#000',
                    }}
                    text={title}/>
            </View>
        )
    }
}