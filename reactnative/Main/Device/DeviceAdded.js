// 主页设备列表/添加设备/设备添加成功
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Animated,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  DeviceEventEmitter,
  NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import { strings, setLanguage } from "../Language/I18n";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import { color } from "react-native-reanimated";

var GXRNManager = NativeModules.GXRNManager;
const { StatusBarManager } = NativeModules;
if (Platform.OS === "ios") {
  StatusBarManager.getHeight((height) => {
    statusBarHeight = height.height;
  });
} else {
  statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const deviceIcon = require("../../resources/greenUnion/new_popup_dev_img.png");
const deleteIcon = require("../../resources/greenUnion/input_deleteall_ic.png");

export default class DeviceAdded extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      deviceName: props.navigation?.state.params.deviceName || "", //设备命名
      status: 1, //添加进度状态 0：未开始 1：正在连接设备 2：向设备传输信息 3：设备连接网络 4：等待设备连接中, 请稍后 5：绑定设备失败-wifi 6：绑定设备失败-蓝牙 7：连接失败 8：绑定成功
      statusText: [
        { status: 1, text: strings("正在连接设备") },
        { status: 2, text: strings("向设备传输信息") },
        { status: 3, text: strings("设备连接网络") },
        { status: 4, text: strings("等待设备连接中") },
      ],
      addProgress: 10, //添加进度 10%
    };
  }

  // 开始使用
  startUse = () => {};

  deleteIconClick = () => {
    this.setState({
      deviceName: "",
    });
  };

  // 返回重试
  returnToTry = () => {};

  render() {
    const { deviceName, status, statusText, addProgress } = this.state;
    const topView = (
      <View
        style={{
          width: "100%",
          paddingLeft: 20,
          marginTop: 20,
          alignItems: "center",
        }}
      >
        <Image
          style={{ marginBottom: 30, marginTop: 100 }}
          source={deviceIcon}
        />
        <View style={{ width: "100%", marginBottom: 30, alignItems: "center" }}>
          <Text style={{ fontSize: 24, color: "#2F2F2F", lineHeight: 40 }}>
            {strings("设备添加成功")}
          </Text>
          <Text style={{ fontSize: 18, color: "#808080" }}>
            {strings("欢迎来到绿联的无线互联世界")}
          </Text>
        </View>
      </View>
    );

    const addingView = (
      <View style={{ width: "100%", alignItems: "center" }}>
        <View
          style={{
            marginTop: 50,
            marginBottom: 10,
            width: "100%",
            alignItems: "center",
          }}
        >
        {/* 此处图片用Lottie动画代替 */}
        <LottieView source={require('../../Lottie/bindingAnimation/bindingAnimation.json')} loop={true} />
          <Image
            style={{ marginBottom: 15, width:198, height:198}}
            source={require("../../resources/greenUnion/connect_secess_ic.png")}
          />
        </View>
        <View
          style={{ flexDirection: "row", alignItems: "flex-end", height: 60 }}
        >
          <Text style={{ color: "#404040", fontSize: 50 }}>{addProgress}</Text>
          <Text style={{ color: "#404040", fontSize: 28 }}>{"%"}</Text>
        </View>
        <View style={{ alignItems: "center", marginVertical: 100 }}>
          {statusText?.map((item, index) => (
            <Text
              key={index}
              style={{
                color: item.status === status ? "#2F2F2F" : "#999BA2",
                fontSize: item.status === status ? 20 : 14,
                lineHeight: 30,
              }}
            >
              {item.text}
            </Text>
          ))}
        </View>
      </View>
    );

    // （5：绑定设备失败-wifi 6：绑定设备失败-蓝牙 7：连接失败） 底部操作按钮
    const returnBtnView = (
      <View>
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            this.returnToTry();
          }}
          style={{
            backgroundColor: "#18B34F",
            width: (this.mScreenWidth - 40) * 0.9,
          }}
          btnText={
            status === 5 || status === 6 ? strings("返回重试") : strings("返回")
          }
        />
      </View>
    );
    // 设备命名
    const deviceNameView = (
      <View style={{ width: "100%", alignItems: "center" }}>
        <TextInputComponent
          style={{ height: 50 }}
          inputBoxStyle={{ borderRadius: 27 }}
          placeholder={strings("请输入设备名称")}
          onChangeText={this.onChangeText}
          value={deviceName}
          type={"deviceName"}
          rightIcon={deleteIcon}
          rightIconClick={this.deleteIconClick}
        />
      </View>
    );
    // 添加成功后开始使用按钮
    const startUseBtnView = (
      <View>
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            this.startUse();
          }}
          style={{
            backgroundColor: "#18B34F",
            width: (this.mScreenWidth - 40) * 0.9,
          }}
          btnText={strings("开始使用")}
        />
      </View>
    );

    // 添加失败视图
    const failView = (
      <View style={{ width: "100%", alignItems: "center" }}>
        <View
          style={{ marginVertical: 50, width: "100%", alignItems: "center" }}
        >
          <Image
            style={{ marginBottom: 15 }}
            source={require("../../resources/greenUnion/connect_failure_ic.png")}
          />
          <Text style={{ color: "#404040", fontSize: 18, lineHeight: 24 }}>
            {status === 7 ? strings("连接失败") : strings("绑定设备失败")}
          </Text>
          <Text style={{ color: "#999BA2", fontSize: 14, lineHeight: 20 }}>
            {status === 7
              ? strings("请重置设备后进行连接")
              : strings("请排查以下问题后重试")}
          </Text>
        </View>
        {status === 5 || status === 6 ? (
          <View style={{ width: "100%", marginVertical: 40 }}>
            <Text style={{ color: "#999BA2", fontSize: 14, lineHeight: 20 }}>
              {"1. " + strings("设备是否已开机且电量充足")}
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ color: "#999BA2", fontSize: 14, lineHeight: 20 }}>
                {strings("设备显示屏") + "【"}
              </Text>
              <Image
                source={
                  status === 5
                    ? require("../../resources/greenUnion/failure_bluetooth_ic.png")
                    : require("../../resources/greenUnion/failure_wifi_ic.png")
                }
              />
              <Text style={{ color: "#999BA2", fontSize: 14, lineHeight: 20 }}>
                {"】" + strings("是否处于闪烁状态")}
              </Text>
            </View>
            <Text style={{ color: "#999BA2", fontSize: 14, lineHeight: 20 }}>
              {"3. " + strings("请检查设备所接入的网络是否可正常使用")}
            </Text>
            <View style={{ marginVertical: 20 }}>
              <Text style={{ color: "#999BA2", fontSize: 14, lineHeight: 20 }}>
                {strings(
                  "以上逐一排查后，设备仍处于离线状态，请重启设备，并重新配网。"
                )}
              </Text>
            </View>
          </View>
        ) : null}
      </View>
    );
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff",
          alignItems: "center",
          height: this.mScreenHeight,
          width: this.mScreenWidth,
        }}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ width: this.mScreenWidth, alignItems: "center" }}>
            <View
              style={{
                width: this.mScreenWidth,
                paddingHorizontal: 20,
                alignItems: "center",
              }}
            >
              <View
                style={{
                  overflow: "hidden",
                  width: this.mScreenWidth - 40,
                  height: this.mScreenHeight,
                  paddingVertical: 25,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                {/* 正在添加设备的视图 */}
                {status === 1 || status === 2 || status === 3 || status === 4
                  ? addingView
                  : null}

                {/* 添加成功 */}
                {status === 8 ? (
                  <>
                    <View style={{ width: "100%" }}>
                      {topView}
                      {deviceNameView}
                    </View>
                    {startUseBtnView}
                  </>
                ) : null}

                {/* 绑定设备失败-蓝牙 */}
                {status === 5 || status === 6 || status === 7 ? (
                  <>
                    <View style={{ width: "100%" }}>{failView}</View>
                    {returnBtnView}
                  </>
                ) : null}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
