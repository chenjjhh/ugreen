// 电源详情/加电包
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Animated,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio,ScrollView,DeviceEventEmitter,NativeModules
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from '../View/TitleBar'
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from '../View/ItemBtnView'
import TextInputComponent from '../View/TextInputComponent'
import CommonBtnView from '../View/CommonBtnView'
import UpLoadView from '../View/UpLoadView'
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";

var GXRNManager = NativeModules.GXRNManager
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
const leftIcon = (require('../../resources/back_black_ic.png'))
export default class ChargingPackage extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
            <TitleBar
                    titleText={strings('加电包')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            data:[
                {name:'主电池包',status:0,statusText:'待机',electricity:'70%'},
                {name:'主电池包01',status:1,statusText:'放电',electricity:'70%'},
                {name:'主电池包02',status:2,statusText:'充电',electricity:'90%'},
            ]
            
        };
      }

    
    
    render() {
        const { data } = this.state

        return (
            <View style={{flex:1,backgroundColor:'#F7F7F7',alignItems:'center',height:this.mScreenHeight,width:this.mScreenWidth}}>
                <ScrollView style={{flex:1,}}>
                    <View style={{width:this.mScreenWidth,alignItems:'center',}}>
                        <View style={{width:this.mScreenWidth,paddingHorizontal:20,alignItems:'center'}}>
                            {data?.map((item,index)=>(
                                <View 
                                    style={{
                                    overflow: 'hidden',
                                    backgroundColor:'#fff',
                                    width:this.mScreenWidth-40,
                                    borderRadius:12,
                                    marginBottom:10,
                                    paddingHorizontal:21,
                                    paddingVertical:18
                                    }}>
                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                        <Text style={{color:'#404040',fontSize:16}} >{item.name}</Text>
                                        <View style={{paddingHorizontal:5,backgroundColor:item.status === 0 ? '#999BA2':item.status === 1? '#18B34F' : 'rgba(24, 179, 79, 0.1)',borderRadius:7.5,marginHorizontal:10,justifyContent:'center',alignItems:'center',height:15}}><Text style={{color:item.status === 2?'#18B34F':'#fff',fontSize:10}}>{item.statusText}</Text></View>
                                    </View>
                                    <Text style={{color:'#999BA2',fontSize:14}}>{strings('电池电量')}</Text>
                                    <Text style={{color:'#18B34F',fontSize:30,fontWeight:'bold'}}>{item.electricity}</Text>
                                    <Image style={{position:'absolute',right:10,bottom:0}} source={require('../../resources/greenUnion/batteryBgIcon.png')}/> 
                                </View>
                            ))}               
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}