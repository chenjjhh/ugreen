// 电源详情/设置/固件升级/升级详情
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Animated,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  DeviceEventEmitter,
  NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import StorageHelper from "../Utils/StorageHelper";

var GXRNManager = NativeModules.GXRNManager;
const { StatusBarManager } = NativeModules;
if (Platform.OS === "ios") {
  StatusBarManager.getHeight((height) => {
    statusBarHeight = height.height;
  });
} else {
  statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
const leftIcon = require("../../resources/back_black_ic.png");
export default class DeviceUpgradeDetails extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          titleText={strings("固件升级")}
          backgroundColor={"#FBFBFB"}
          leftIcon={leftIcon}
          rightIcon={rightIcon}
          leftIconClick={() => {
            navigation.goBack();
          }}
        />
      ),
    };
  };
  constructor(props) {
    super(props);
    const { data } = props.navigation?.state?.params || {};
    this.state = {
      data: data || {},
      status: "", // FAIL-失败 UPGRADING-升级中 SUCCESS-升级成功
      progress: "", //升级进度
      token: StorageHelper.getTempToken() || "",
    };
  }

  // 获取状态
  getUpgradeStateText = () => {
    // FAIL-失败 UPGRADING-升级中 SUCCESS-升级成功
    const { status } = this.state;
    let text;
    switch (status) {
      case "UPGRADING":
        text = strings("设备升级中");
        break;
      case "FAIL":
        text = strings("升级失败");
        break;
      case "SUCCESS":
        text = strings("升级成功");
        break;
      default:
        text = "";
        break;
    }
    return text;
  };

  // 不同状态图标
  getImageView = () => {
    const { status } = this.state;
    let ImageView;
    switch (status) {
      case "UPGRADING":
        ImageView = (
          <Image
            source={require("../../resources/greenUnion/unexposed_ufo_icon.png")}
          />
        );
        break;
      case "FAIL":
        ImageView = (
          <Image source={require("../../resources/greenUnion/failed_ic.png")} />
        );
        break;
      case "SUCCESS":
        ImageView = (
          <Image
            source={require("../../resources/greenUnion/success_ic.png")}
          />
        );
        break;
      default:
        break;
    }
    return ImageView;
  };

  componentWillUnmount() {
    this.firmwareUpgradeTimer && clearInterval(this.firmwareUpgradeTimer);
  }

  //固件升级
  firmwareUpgrade = () => {
    const { data, token } = this.state;
    NetUtil.firmwareUpgrade(token, data?.upgradeId, false)
      .then((res) => {
        // 通知服务器升级后,立即获取升级进度
        this._startTimer();
      })
      .catch((error) => {
        throw error;
      });
  };

  //获取升级进度
  firmwareUpgradeProgress = () => {
    const { data, token } = this.state;
    NetUtil.firmwareUpgradeProgress(token, data?.upgradeId, false)
      .then((res) => {
        if (res?.info) {
          const { progress, status } = res?.info || {};
          this.setState({
            progress, //进度
            status, //状态 FAIL-失败 UPGRADING-升级中 SUCCESS-升级成功
          });
          //  有升级结果后清掉定时器
          if (status === "SUCCESS" || status === "FAIL") {
              this._stopTimer();
              if(status === "SUCCESS"){
                // 成功后刷新固件升级列表
                  EventUtil.sendEvent(EventUtil.UPDATEFIRWARELIST);
              }
          }
        }
      })
      .catch((error) => {
        throw error;
      });
  };

  _startTimer = () => {
    this._stopTimer();
    this.firmwareUpgradeTimer = setInterval(() => {
      this.firmwareUpgradeProgress();
    }, 5000);
  };

  _stopTimer() {
    if (this.firmwareUpgradeTimer) {
      clearInterval(this.firmwareUpgradeTimer);
      this.firmwareUpgradeTimer = null;
    }
  }

  render() {
    const { data, status, progress } = this.state;
    const upgradeWithoutStarting = (
      <>
        <View style={{ width: "100%", paddingHorizontal: 20 }}>
          <View style={{ alignItems: "center", marginVertical: 30 }}>
            <View style={{ marginBottom: 20 }}>
              <Image
                source={require("../../resources/greenUnion/unexposed_ufo_icon.png")}
              />
            </View>
            <Text style={{ color: "#404040", fontSize: 18, lineHeight: 30 }}>
              {data?.currentVersion || ""}
            </Text>
            <Text style={{ color: "#808080", fontSize: 14, lineHeight: 30 }}>
              {strings("当前固件") + "：" + (data?.thoroughfareName || "")}
            </Text>
          </View>
          <View>
            <Text style={{ color: "#404040", fontSize: 16, lineHeight: 30 }}>
              {strings("升级提示语")}
            </Text>
            <Text style={{ color: "#18B34F", fontSize: 14, lineHeight: 30 }}>
              {strings("最新固件") +
                "：" +`${data?.targetVersion ? data?.targetVersion + 'V ' : ''}` +
                `${data?.firmwareSize ? data?.firmwareSize + 'M' : ''}`}
            </Text>
          </View>
          <View style={{ marginVertical: 30 }}>
            <Text style={{ color: "#404040", fontSize: 16, lineHeight: 30 }}>
              {strings("升级前须知")}
            </Text>
            <Text style={{ color: "#808080", fontSize: 14, lineHeight: 30 }}>
              {data?.upgradePrompt || ""}
            </Text>
          </View>
          <View>
            <Text style={{ color: "#404040", fontSize: 16, lineHeight: 30 }}>
              {strings("升级提示语")}
            </Text>
            <Text
              style={{ color: "#808080", fontSize: 14, lineHeight: 30 }}
            >
              {data?.upgradeContent || ""}
            </Text>
          </View>
        </View>
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            // 开始升级
            this.firmwareUpgrade();
          }}
          style={{
            backgroundColor: "#18B34F",
            width: (this.mScreenWidth - 40) * 0.9,
            marginTop:20,
          }}
          btnText={strings("开始升级")}
        />
      </>
    );

    const upgradingView = (
      <View style={{ height: "100%", justifyContent: "space-between" }}>
        <View style={{ justifyContent:'center',alignItems: "center", marginVertical: 100 ,}}>
          <View
            style={{
              borderRadius: 71,
              width: 154,
              height: 154,
              marginBottom: 20,
            }}
          >
            {this.getImageView()}
          </View>
          <Text style={{ color: "#404040", fontSize: 50 }}>{`${
            progress ?  progress + '%': ""
          }`}</Text>
          <Text style={{ color: "#808080", fontSize: 14, lineHeight: 30 }}>
            {this.getUpgradeStateText()}
          </Text>
        </View>
        <Text style={{ color: "#999BA2", fontSize: 14 }}>
          {strings("固件升级提示语")}
        </Text>
      </View>
    );

    const upgradeFailedView = (
      <View style={{ height: "100%", justifyContent: "space-between" }}>
        <View style={{ alignItems: "center", marginVertical: 100 }}>
          <View
            style={{
              backgroundColor: "rgba(24, 179, 79, 0.1)",
              borderRadius: 58,
              width: 118,
              height: 118,
              marginBottom: 20,
            }}
          >
            {this.getImageView()}
          </View>
          <Text style={{ color: "#404040", fontSize: 18, lineHeight: 30 }}>
            {this.getUpgradeStateText()}
          </Text>
          <View style={{ width: "80%" }}>
            <Text
              style={{
                color: "#808080",
                fontSize: 14,
                lineHeight: 20,
                textAlign: "center",
              }}
            >
              {strings("升级失败提示")}
            </Text>
          </View>
        </View>
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            // 重试
            this.firmwareUpgrade();
          }}
          style={{
            backgroundColor: "#18B34F",
            width: (this.mScreenWidth - 40) * 0.9,
          }}
          btnText={strings("重试")}
        />
      </View>
    );
    const upgradeSuccessView = (
      <View style={{ height: "100%", justifyContent: "space-between" }}>
        <View style={{ alignItems: "center", marginVertical: 100 }}>
          <View
            style={{
              backgroundColor: "rgba(24, 179, 79, 0.1)",
              borderRadius: 58,
              width: 118,
              height: 118,
              marginBottom: 20,
            }}
          >
            {this.getImageView()}
          </View>
          <Text style={{ color: "#404040", fontSize: 18, lineHeight: 30 }}>
            {this.getUpgradeStateText()}
          </Text>
          <View style={{ width: "80%" }}>
            <Text
              style={{
                color: "#808080",
                fontSize: 14,
                lineHeight: 20,
                textAlign: "center",
              }}
            >
              {strings("当前固件") + ': ' + `${data?.targetVersion ? data?.targetVersion + 'V':''}` }
            </Text>
          </View>
        </View>
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            // 返回上一页
            this.props.navigation.goBack();
          }}
          style={{
            backgroundColor: "#18B34F",
            width: (this.mScreenWidth - 40) * 0.9,
          }}
          btnText={strings("ok")}
        />
      </View>
    );
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#F7F7F7",
          alignItems: "center",
          width: this.mScreenWidth,
          height: this.mScreenHeight
        }}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ width: this.mScreenWidth, alignItems: "center" }}>
            <View
              style={{
                width: this.mScreenWidth,
                paddingHorizontal: 20,
                alignItems: "center",
              }}
            >
              <View
                style={{
                  overflow: "hidden",
                  backgroundColor: "#fff",
                  width: this.mScreenWidth - 40,
                  borderRadius: 12,
                  marginBottom: 10,
                  paddingVertical: 25,
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                {status === 'UPGRADING'
                  ? upgradingView
                  : status === 'FAIL'
                  ? upgradeFailedView
                  : status === 'SUCCESS'
                  ? upgradeSuccessView
                  : upgradeWithoutStarting}
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
