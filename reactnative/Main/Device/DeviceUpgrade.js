// 电源详情/设置/固件升级
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Animated,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  DeviceEventEmitter,
  NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import FastImage from "react-native-fast-image";
import SwitchBtn from "../View/SwitchBtn";
import ShowOrHideBtnView from "../View/ShowOrHideBtnView";
import StorageHelper from "../Utils/StorageHelper";
import DeviceListUtil from "../Utils/DeviceListUtil";
var GXRNManager = NativeModules.GXRNManager;
const { StatusBarManager } = NativeModules;
if (Platform.OS === "ios") {
  StatusBarManager.getHeight((height) => {
    statusBarHeight = height.height;
  });
} else {
  statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;
const rightIcon = require("../../resources/greenUnion/top_history_ic.png");
const leftIcon = require("../../resources/back_black_ic.png");
export default class DeviceUpgrade extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          titleText={strings("固件升级")}
          backgroundColor={"#F1F2F6"}
          leftIcon={leftIcon}
          rightIcon={rightIcon}
          leftIconClick={() => {
            navigation.goBack();
          }}
          rightIconClick={() => {
            // 升级历史
          }}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    const { deviceId, isWifiConnect, isOnline, mac } =
      props.navigation?.state?.params || {};
    this.state = {
      deviceId,
      isWifiConnect,
      mac,
      updateNotice: strings("固件升级须知"),
      isOnline,
      data: [
        // {
        //   thoroughfareId: "1", //通道id
        //   thoroughfareName: "模组OTA", //通道名称
        //   thoroughfareNumber: "1", //通道编号
        //   upgrade: false, //t-可以升级 f-不能升级
        //   upgradeStatus: "UPGRADABLE", //升级状态 UPGRADABLE-可升级 UPGRADED-升级完成(已是最新版本) UPGRADING-可升级
        //   firmwareSize: "1M", //固件包大小
        //   upgradePrompt: "升级提示", //升级提示
        //   upgradeContent: "优化了响应速度", //升级内容
        //   firmwareUrl: "", //固件url
        //   upgradeId: "", //升级id
        //   currentVersion: "1.0.1", //当前版本
        //   targetVersion: "1.0.2", //目标版本
        //   firmwareMd5: "", //固件md5值
        //   thoroughfareNumber: "2", //通道编号
        // },
      ],
      TipsViewShow: false,
      token: StorageHelper.getTempToken() || "",
    };
  }

  componentDidMount() {
    this.showTipsView();
    this.firmwareList();
    this._startTimer();
    this._eventListenerAdd();
  }

  componentWillUnmount() {
    this.tipsTimer && clearTimeout(this.tipsTimer);
    this.deviceListTimer && clearInterval(this.deviceListTimer);
    this.firmwareListListener?.remove()
  }

  _eventListenerAdd = () => {
    this.firmwareListListener = DeviceEventEmitter.addListener(
        EventUtil.UPDATEFIRWARELIST,
        () => {
            this.firmwareList();
        }
      );
  }

  _startTimer = () => {
    this._stopTimer();
    this.deviceListTimer = setInterval(() => {
      this._getDeviceList();
    }, 60000);
  };

  _stopTimer() {
    if (this.deviceListTimer) {
      clearInterval(this.deviceListTimer);
      this.deviceListTimer = null;
    }
  }

  //获取设备列表
  _getDeviceList() {
    const { token, deviceId } = this.state;
    if (token) {
      DeviceListUtil._getDeviceList(false, token, true)
        .then((res) => {
          if (res?.list?.length > 0) {
            res?.list?.forEach((element) => {
              if (deviceId === element?.deviceId) {
                this.setState({
                  isOnline: element?.onlineStatus,
                });
              }
            });
          }
        })
        .catch((error) => {});
    }
  }

  // 离线提示
  showTipsView = () => {
    const { isOnline } = this.state;
    if (!isOnline) {
      this.setState(
        {
          TipsViewShow: true,
        },
        () => {
          this.tipsTimer && clearTimeout(this.tipsTimer);
          this.tipsTimer = setTimeout(() => {
            this.setState({
              TipsViewShow: false,
            });
          }, 5000);
        }
      );
    }
  };

  // 获取固件列表
  firmwareList = () => {
    const { deviceId, mac, token } = this.state;
    NetUtil.firmwareList(token, deviceId, mac)
      .then((res) => {
        if (res?.list?.length > 0) {
          this.setState({
            data: res?.list || [],
          });
        }
      })
      .catch((error) => {
        throw error;
      });
  };

  render() {
    const { data, updateNotice, isOnline, TipsViewShow } = this.state;

    // 离线提示弹窗
    const TipsView = (
      <View
        style={{
          width: 124,
          height: 124,
          backgroundColor: "rgba(0,0,0,0.6)",
          borderRadius: 22,
          position: "absolute",
          top: (this.mScreenHeight - (statusBarHeight + 65)) / 2 - 62,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View style={{ width: 43, height: 43, marginVertical: 15 }}>
          <Image
            source={require("../../resources/greenUnion/offlineIconToast.png")}
            style={{ width: 43, height: 43 }}
          />
        </View>
        <Text style={{ color: "#fff", fontSize: 14 }}>
          {strings("设备离线")}
        </Text>
      </View>
    );
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#F1F2F6",
          alignItems: "center",
          width: this.mScreenWidth,
        }}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ width: this.mScreenWidth, alignItems: "center" }}>
            <View
              style={{
                width: this.mScreenWidth,
                paddingHorizontal: 20,
                alignItems: "center",
              }}
            >
              {data?.map((item, index) => (
                <View
                  style={{
                    overflow: "hidden",
                    backgroundColor: "#fff",
                    width: this.mScreenWidth - 40,
                    borderRadius: 12,
                    marginBottom: 10,
                    paddingHorizontal: 21,
                    paddingVertical: 25,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      borderBottomColor: "#F2F2F2",
                      borderBottomWidth: 0.5,
                      paddingBottom: 10,
                      marginBottom: 10,
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: "rgba(24, 179, 79, 0.1)",
                        borderRadius: 19,
                        width: 36,
                        height: 36,
                        marginRight: 15,
                      }}
                    >
                      {item.thoroughfareName === "BMU" ? (
                        <Image
                          source={require("../../resources/greenUnion/BMU.png")}
                          style={{ width: 36, height: 36 }}
                        />
                      ) : item.thoroughfareName === "BMS" ? (
                        <Image
                          source={require("../../resources/greenUnion/BMS.png")}
                          style={{ width: 36, height: 36 }}
                        />
                      ) : item.thoroughfareName === "INV" ? (
                        <Image
                          source={require("../../resources/greenUnion/INV.png")}
                          style={{ width: 36, height: 36 }}
                        />
                      ) : null}
                    </View>
                    <View
                      style={{
                        flexDirection: "row",
                        flex: 1,
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <View>
                        <Text style={{ color: "#404040", fontSize: 18 }}>
                          {item.thoroughfareName}
                        </Text>
                        <Text style={{ color: "#808080", fontSize: 14 }}>
                          {strings("当前版本") + (item.currentVersion || "")}
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={{
                          backgroundColor:
                            item.upgradeStatus === "UPGRADED"
                              ? "#F1F2F6"
                              : "#18B34F",
                          paddingHorizontal: 10,
                          borderRadius: 10,
                        }}
                        disabled={item.upgradeStatus === "UPGRADED"}
                        onPress={() => {
                            if(!isOnline){
                                this.onShowToast('device_off_line')
                            }
                          // 升级
                          this.props.navigation.navigate(
                            "DeviceUpgradeDetails",
                            { data: item }
                          );
                        }}
                      >
                        <Text
                          style={{
                            color:
                              item.upgradeStatus === "UPGRADED"
                                ? "#C4C6CD"
                                : "#fff",
                            fontSize: 12,
                            lineHeight: 20,
                          }}
                        >
                          {item.upgradeStatus === "UPGRADED"
                            ? strings("已是最新")
                            : strings("升级")}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{ marginBottom: 10 }}>
                    <View style={{ marginVertical: 15 }}>
                      <Text>
                        {strings("最新版本") + ":"}{" "}
                        <Text style={{ color: "#18B34F", fontSize: 14 }}>
                          {item.targetVersion || ""}
                        </Text>
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: "#999BA2",
                        fontSize: 14,
                        lineHeight: 25,
                      }}
                    >
                      {item?.upgradeContent || ""}
                    </Text>
                  </View>
                  <Text style={{ color: "#999BA2", fontSize: 14 }}>
                    {updateNotice || ""}
                  </Text>
                </View>
              ))}
            </View>
          </View>
        </ScrollView>
        {!isOnline && TipsViewShow ? TipsView : null}
      </View>
    );
  }
}
