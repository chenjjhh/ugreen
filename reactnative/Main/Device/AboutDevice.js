import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Clipboard,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import {container} from "aws-amplify";
import {AboutDeviceData} from "../Utils/data";
import TitleBar from "../View/TitleBar";
import ItemBtnView from "../View/ItemBtnView";
import {ScrollView} from "react-native-gesture-handler";
/*
 *关于本机
 */
export default class AboutDevice extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    backgroundColor={"#F1F2F6"}
                    titleText={strings("关于本机")}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        const {deviceId, mac, productKey} =
        props.navigation?.state?.params?.itemData || {};
        this.state = {
            AboutDeviceData: {
                ...AboutDeviceData,
                deviceId,
                mac,
                productKey,
            },
        };
    }

    componentWillMount() {
        // this.getData();
    }

    getData = () => {
    };

    render() {
        const {
            deviceId,
            mac,
            productKey,
            deviceName,
            productType,
            PN,
            BatteryType,
            productSize,
            productWeight,
            chargingTemperature,
            dischargeTemperature,
            acInput,
            solarChargingInput,
            carChargingInput,
            acOutput,
            usbAInput,
            usbCInput,
            vehicleOutput,
            dc5521Output,
            totalOutput,
        } = this.state.AboutDeviceData || {};
        return (
            <ScrollView
                style={{
                    backgroundColor: "#F1F2F6",
                }}
            >
                <View
                    style={{
                        width: this.mScreenWidth,
                        flex: 1,
                        alignItems: "center",
                    }}
                >
                    <View
                        style={{
                            width: this.mScreenWidth - 40,
                        }}
                    >
                        <View style={{flexDirection: "row", marginVertical: 20}}>
                            <Image
                                style={{
                                    width: (this.mScreenWidth - 40) * 0.4,
                                    height: (this.mScreenWidth - 40) * 0.4 * 0.8,
                                }}
                                source={require("../../resources/greenUnion/new_popup_dev_img.png")}
                            />
                            <View style={{marginLeft: 15, justifyContent: "space-between"}}>
                                <Text
                                    style={[
                                        styles.textStyle,
                                        {color: "#222222", fontSize: 18, fontWeight: "bold"},
                                    ]}
                                >
                                    {deviceName}
                                </Text>
                                <Text style={styles.textStyle}>{"SID:" + productKey}</Text>
                                <Text style={styles.textStyle}>{"MAC:" + mac}</Text>
                                <Text style={styles.textStyle}>{"IOT:" + deviceId}</Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        Clipboard.setString("SID:" + productKey + "\nMAC:" + mac + "\nIOT:" + deviceId)
                                        this.onShowToast(strings("已复制到剪贴板"))
                                    }}>
                                    <Text style={[styles.textStyle, {color: "#18B34F"}]}>
                                        {strings("复制信息")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View
                            style={{
                                backgroundColor: "#fff",
                                borderRadius: 10,
                                overflow: "hidden",
                                width: "100%",
                                marginBottom: 15,
                            }}
                        >
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("产品名称")}
                                rightText={deviceName}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("产品型号")}
                                rightText={PN}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={"P/N"}
                                rightText={productType}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("电池类型")}
                                rightText={BatteryType}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("产品尺寸")}
                                rightText={productSize}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("产品净重")}
                                rightText={productWeight}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("充电温度")}
                                rightText={chargingTemperature}
                            />
                            <ItemBtnView
                                activeOpacity={1}
                                text={strings("放电温度")}
                                rightText={dischargeTemperature}
                            />
                        </View>

                        <View
                            style={{
                                backgroundColor: "#fff",
                                borderRadius: 10,
                                overflow: "hidden",
                                width: "100%",
                                marginBottom: 15,
                            }}
                        >
                            <ItemView title={strings("交流输入")} BottomText={acInput}/>
                            <ItemView
                                title={strings("太阳能充电输入")}
                                BottomText={solarChargingInput}
                            />
                            <ItemView
                                title={strings("车充输入")}
                                BottomText={carChargingInput}
                            />
                        </View>

                        <View
                            style={{
                                backgroundColor: "#fff",
                                borderRadius: 10,
                                overflow: "hidden",
                                width: "100%",
                                marginBottom: 15,
                            }}
                        >
                            <ItemView title={strings("交流输出")} BottomText={acOutput}/>
                            <ItemView title={strings("USBA输出")} BottomText={usbAInput}/>
                            <ItemView title={strings("USBC输出")} BottomText={usbCInput}/>
                            <ItemView
                                title={strings("车充口输出")}
                                BottomText={vehicleOutput}
                            />
                            <ItemView
                                title={strings("DC5521输出")}
                                BottomText={dc5521Output}
                            />
                            <ItemView title={strings("总输出")} BottomText={totalOutput}/>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

export class ItemView extends BaseComponent {
    render() {
        const {title, BottomText} = this.props;
        return (
            <View
                style={{
                    borderBottomColor: "#EBEBEB",
                    borderBottomWidth: 0.5,
                    paddingHorizontal: 20,
                    paddingBottom: 10,
                }}
            >
                <View style={{marginVertical: 10}}>
                    <Text style={[styles.textStyle, {color: "#222222", fontSize: 14}]}>
                        {title}
                    </Text>
                </View>
                <Text style={styles.textStyle}>{BottomText}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 14,
        color: "#808080",
    },
});
