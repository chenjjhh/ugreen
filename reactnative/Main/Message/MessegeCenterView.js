import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
    DeviceEventEmitter
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import TabView from "../View/TabView";
import TitleBar from "../View/TitleBar";
import DevicePush from "./DevicePush";
import SystemNotification from "./SystemNotification";
import SharingEquipment from "./SharingEquipment";
import {ScrollView} from "react-native-gesture-handler";
import CommonMessageDialog from "../View/CommonMessageDialog";
import NetUtil from '../Net/NetUtil'
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";

/*
 *消息中心
 */
const SCREEN_WIDTH = Dimensions.get("window").width;
const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class MessegeCenterView extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                null
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            activeTab: 0,
            detailsData: {
                imgUrl:
                    "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fsafe-img.xhscdn.com%2Fbw1%2F9e8bd863-bf16-4647-abf0-88490ce78ced%3FimageView2%2F2%2Fw%2F1080%2Fformat%2Fjpg&refer=http%3A%2F%2Fsafe-img.xhscdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1703398771&t=fff094408ea54ce5a7727707bc79d5f8",
                createTime: 1692865536560,
                devicePushId: 854605908240433200,
                deviceNickName: "我的户外电源",
                productTypeName: "星辰GS600",
                jumpLink: "www.abc.com",
                lang: "zh_CN",
                title: "设备电量不足",
                deviceId: 843732050713829400,
                content:
                    "概述绿联 T6 是绿联 2023 年的旗舰耳机，具有 -48dB 降噪深度、Hi-Res 金标音质等核心优势，同时在 蓝牙、续航、佩戴等体验也优于同价位耳机，堪称 300 元以内最强的全能型降噪耳机特点-48dB 降噪深度 + A 级降噪认证Hi-Res 金标认证。",
                mac: "test0001",
            },
            MsgdetailsDialog: false,//消息详情弹窗
            tabData: [
                {text: strings("device_push"), id: 0, isNoRead: false}, //设备推送
                {text: strings("device_share"), id: 1, isNoRead: false}, //设备分享
                {text: strings("system_notification"), id: 2, isNoRead: false}, //系统通知
            ],
        }
    }

    componentDidMount() {
        this.getMsgCount()
        this.toReadListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_ALL_READ, (value) => {
            this.getMsgCount()
        });
    }

    componentWillUnmount() {
        this.toReadListener?.remove()
    }

    // 获取消息数量
    getMsgCount = () => {
        const {tabData} = this.state;
        NetUtil.getMsgCount(StorageHelper.getTempToken())
            .then((res) => {
                if (res?.list) {
                    let newTabData = [...tabData]
                    res?.list.forEach(element => {
                        // 1-系统消息（公告） 2-家庭消息 3-设备推送消息 4-设备分享消息
                        if (element?.type == '1' && element?.unReadCnt) {
                            newTabData[2].isNoRead = true
                        } else if (element?.type == '3' && element?.unReadCnt) {
                            newTabData[0].isNoRead = true
                        } else if (element?.type == '4' && element?.unReadCnt) {
                            newTabData[1].isNoRead = true
                        }
                    });
                    this.setState({
                        tabData: newTabData
                    })
                }
            }).catch((error) => {
            console.error(error);
        })

    }

    //全部已读
    allRead = () => {
        // 系统消息（公告）
        NetUtil.allRead(StorageHelper.getTempToken(), 1, true)
            .then((res) => {
                this.onShowToast('全部已读')
                DeviceEventEmitter.emit(EventUtil.DEVICE_ALL_READ)
            }).catch((error) => {
            console.error(error);
        })
        //设备推送消息
        NetUtil.allRead(StorageHelper.getTempToken(), 3, true)
            .then((res) => {
                this.onShowToast('全部已读')
                DeviceEventEmitter.emit(EventUtil.DEVICE_ALL_READ)
            }).catch((error) => {
            console.error(error);
        })
    }

    _checkTab = (data) => {
        this.setState({activeTab: data.id}, () => {
            this.handleTabPress(data.id);
        });
    };

    handleTabPress = (tab) => {
        this.setState({activeTab: tab});
        this.scrollView.scrollTo({x: tab * SCREEN_WIDTH, y: 0, animated: false});
    };

    showDialog = (detailsData) => {
        this.setState({
            detailsData,
            MsgdetailsDialog: true,
        }, () => {
            DeviceEventEmitter.emit(EventUtil.DEVICE_ALL_READ)
        })
    }

    _onHide = (type) => {
        this.setState({
            [type]: false
        })
    }

    // 跳转详情
    goDetails = () => {
        const {detailsData} = this.state;
        this.setState({
            MsgdetailsDialog: false,
        })
        this.props.navigation.navigate('BatteryDetails', {
            deviceName: detailsData?.deviceNickName,
            itemData: detailsData
        })
    }

    render() {
        const {} = this.props;
        const {activeTab, detailsData, MsgdetailsDialog, tabData} = this.state;
        const _tabView = (
            <TabView
                data={tabData}
                currentId={activeTab}
                _checkTab={this._checkTab}
                containerStyle={{marginBottom: 18, marginTop: 18}}
            />
        );
        {
            /* 详情弹窗 */
        }
        const MsgdetailsDialogView = (
            <CommonMessageDialog
                style={{alignItems: "flex-start"}}
                rightBtnTextColor={"#ffffff"}
                onRightBackgroundColor={"#18B34F"}
                onRequestClose={() => {
                    this._onHide("MsgdetailsDialog");
                }}
                overViewClick={() => {
                    this._onHide("MsgdetailsDialog");
                }}
                modalVisible={MsgdetailsDialog}
                children={
                    <View style={{marginVertical: 25}}>
                        <Text
                            style={{
                                color: "#808080",
                                lineHeight: 19,
                                fontSize: 14,
                            }}
                        >
                            {detailsData?.content}
                        </Text>
                        {detailsData.mac ? (
                            <TouchableOpacity
                                onPress={() => this.goDetails()}
                                style={{
                                    flexDirection: "row",
                                    height: 85,
                                    alignItems: "center",
                                }}
                            >
                                <View style={{marginRight: 15}}>
                                    <Image
                                        style={{width: 50, height: 50}}
                                        source={{uri: detailsData?.imgUrl}}
                                    />
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        flex: 1,
                                        justifyContent: "space-between",
                                        alignItems: "center",
                                    }}
                                >
                                    <View>
                                        <Text
                                            style={{
                                                color: "#404040",
                                                lineHeight: 22,
                                                fontSize: 16,
                                            }}
                                        >
                                            {detailsData?.deviceNickName}
                                        </Text>
                                        <Text
                                            style={{
                                                color: "#808080",
                                                lineHeight: 18,
                                                fontSize: 13,
                                            }}
                                        >
                                            {detailsData?.productTypeName}
                                        </Text>
                                    </View>
                                    <Image source={rightIcon}/>
                                </View>
                            </TouchableOpacity>
                        ) : (
                            <Image
                                style={{
                                    width: this.mScreenWidth - 40,
                                    height: (this.mScreenWidth - 40) / 2,
                                    marginTop: 20,
                                }}
                                source={{uri: detailsData?.imgUrl}}
                            />
                        )}
                    </View>
                }
                title={detailsData?.title}
                message={detailsData?.content}
                rightBtnText={strings("知道了")}
                onRightBtnClick={() => {
                    //确定
                    this._onHide("MsgdetailsDialog");
                }}
            />
        );
        StatusBar.setBarStyle("light-content");
        return (
            <View style={{width: this.mScreenWidth}}>
                <StatusBar translucent={true} backgroundColor="transparent"/>
                <TitleBar
                    titleText={strings("messege_center")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        this.props.navigation.goBack();
                    }}
                    rightText={strings("all_read")}
                    rightTextClick={() => {
                        // 全部已读
                        this.allRead();
                    }}
                />
                {_tabView}
                <ScrollView
                    ref={(ref) => (this.scrollView = ref)}
                    horizontal
                    pagingEnabled
                    showsHorizontalScrollIndicator={false}
                    scrollEnabled={false}
                >
                    <DevicePush showDialog={this.showDialog}/>
                    <SharingEquipment/>
                    <SystemNotification showDialog={this.showDialog}/>
                </ScrollView>
                {MsgdetailsDialogView}
            </View>
        );
    }
}
