/*
*消息中心/设备分享
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    FlatList,
    RefreshControl,
    DeviceEventEmitter
} from 'react-native';
import AcceptDeviceShareDialog from "../View/AcceptDeviceShareDialog";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from '../Net/NetUtil'
import CommonTextView from '../View/CommonTextView'
import I18n from 'react-native-i18n';
import Helper from "../Utils/Helper";
import StorageHelper from "../Utils/StorageHelper";
import FastImage from "react-native-fast-image";
import EventUtil from "../Event/EventUtil";

class SharingEquipment extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            pageNum: 1,
            pageSize: 10,
            loading: false,
            refreshing: false,
            noMoreData: true,
            isLoadingMore: false,
            token: StorageHelper.getTempToken() || '',
            FindNewDevice: '',//发现新设备设备名称
            FindNewDeviceId: '',//发现新设备设备Id
            accpetShareDeviceData: {},//接受分享设备数据
            shareDialogVis: false,

        };
    }

    componentDidMount() {
        this.getData();
        this.toReadListener = DeviceEventEmitter.addListener(EventUtil.DEVICE_ALL_READ, (value) => {
            this.getData();
        });
    }

    componentWillUnmount() {
        this.toReadListener?.remove()
    }

    getData = () => {
        const {pageNum, pageSize, list, token} = this.state;
        this.setState({loading: true});
        NetUtil.deviceShare(token, pageNum, pageSize, true)
            .then((res) => {
                if (res?.list?.length > 0) {
                    this.setState({
                        list: pageNum === 1 ? res?.list : [...list, ...res?.list],
                        loading: false,
                        refreshing: false,
                        isLoadingMore: false,
                    });
                } else {
                    this.setState({
                        loading: false,
                        refreshing: false,
                        noMoreData: true,
                        isLoadingMore: false,
                    });
                }
            }).catch((error) => {
            console.error(error);
        })
    }

    handleRefresh = () => {
        this.setState({refreshing: true, pageNum: 1, noMoreData: false}, () => {
            this.getData();
        });
    };

    handleLoadMore = () => {
        const {isLoadingMore, noMoreData} = this.state;
        if (!isLoadingMore && !noMoreData) {
            this.setState(
                prevState => ({
                    pageNum: prevState.pageNum + 1,
                    isLoadingMore: true,
                }),
                () => {
                    this.getData();
                }
            );
        }
    };

    // 跳转设备详情
    goDetails = (data) => {
        const {lang, token} = this.state;
        const {deviceId, title, productTypeName,} = data || {};
        this.setState({loading: true});
        // 此处可根据不同设备类型跳转回对应设备详情
        // 电池详情
        this.props.navigation.navigate('BatteryDetails', {
            itemData: {
                deviceId,
                deviceType: productTypeName,
                // deviceInfo,
                // mac,
                // productKey,
                // isOnline,
                // uType,
            },
            deviceName: title,
        });
    }

    renderFooter = () => {
        const {isLoadingMore, noMoreData, list} = this.state;
        if (isLoadingMore && !noMoreData) {
            return (
                <View style={{paddingVertical: 20}}>
                    <ActivityIndicator animating size='large' color={'#18B34F'}/>
                </View>
            );
        } else if (noMoreData && list?.length > 0) {
            return (
                <View style={{paddingVertical: 20}}>
                    <Text style={{textAlign: 'center'}}>没有更多数据了</Text>
                </View>
            );
        } else {
            return null;
        }
    };

    touchItem = (item) => {
        // status	string	1-已接收 2-未接收 3-过期
        const {status} = item;
        if (status == 2) {
            this.setState({
                accpetShareDeviceData: item,
                shareDialogVis: true,
            }, () => {
                DeviceEventEmitter.emit(EventUtil.DEVICE_ALL_READ)
            })
        }
        return
    }

    // 接受或者稍后处理接受分享
    _accpteShareDevice({deviceShareId, accpetShareDeviceName}, receive) {
        const {accpetShareDeviceData} = this.state;
        NetUtil.deviceReceveDetails(StorageHelper.getTempToken(), deviceShareId, receive, accpetShareDeviceName, true)
            .then((res) => {
                this.onShowToast(strings('successed'))
                EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY)
                this.getData()
                this.goDetails(accpetShareDeviceData);
            }).catch((error) => {
        })
    }

    renderItem = (item, index) => {
        return (<TouchableOpacity onPress={() => this.touchItem(item)} key={index}
                                  style={{width: this.mScreenWidth, flexDirection: 'row', justifyContent: 'center'}}>
            <View style={{
                backgroundColor: '#fff',
                height: 100,
                width: this.mScreenWidth - 40,
                flexDirection: 'row',
                alignItems: 'center',
                borderRadius: 10,
                marginBottom: 15
            }}>
                <View style={{width: 70, height: 70, marginHorizontal: 20}}>
                    {/*<Image style={{position:'absolute',width:'100%',height:'100%',}} source={require('../../resources/ic_display_small.png')}/>*/}
                    <FastImage
                        style={{width: '100%', height: '100%'}}
                        source={{
                            uri: item?.imgUrl,
                        }}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                </View>
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginRight: 10
                }}>
                    <View style={{flex: 1}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Text numberOfLines={1}>
                                <CommonTextView
                                    textSize={15}
                                    text={item.title}
                                    style={{
                                        color: '#404040',
                                        fontWeight: 'bold',
                                    }}/>
                            </Text>
                            {!item.read ? <View style={{
                                width: 7,
                                height: 7,
                                backgroundColor: '#18B34F',
                                borderRadius: 5,
                                marginHorizontal: 5
                            }}></View> : null}
                        </View>
                        <Text numberOfLines={1}>
                            <CommonTextView
                                textSize={12}
                                text={item.content}
                                style={{
                                    color: '#808080',
                                }}/>
                        </Text>
                        <CommonTextView
                            textSize={11}
                            text={Helper._formatTimestamp(item?.createTime)}
                            style={{
                                color: '#C4C6CD',
                                fontWeight: 'normal',
                            }}/>
                    </View>
                    <Image source={require('../../resources/greenUnion/list_arrow_ic.png')}/>
                </View>
            </View>
        </TouchableOpacity>)
    }

    render() {
        const {list, loading, refreshing, accpetShareDeviceData, shareDialogVis} = this.state;
        const _accpetShareDialog = (
            <AcceptDeviceShareDialog
                deviceImage={accpetShareDeviceData?.imgUrl}
                modalVisible={shareDialogVis}
                userName={accpetShareDeviceData?.nickName}
                deviceType={accpetShareDeviceData?.productTypeName}
                deviceName={accpetShareDeviceData?.productTypeName}
                onModalClose={() => {
                    this.setState({shareDialogVis: false})
                }}
                onChangeText={(text) => {
                    var accpetShareDeviceName = text.toString().trim()
                    this.setState({accpetShareDeviceData: {...accpetShareDeviceData, accpetShareDeviceName}})
                }}
                onLeftBtnClick={() => {
                    //稍后
                    this.setState({shareDialogVis: false})
                    this._accpteShareDevice(accpetShareDeviceData, 2)
                }}
                onRightBtnClick={() => {
                    //接受
                    if (accpetShareDeviceData?.accpetShareDeviceName) {
                        this.setState({shareDialogVis: false})
                        this._accpteShareDevice(accpetShareDeviceData, 1)
                    } else {
                        this.setState({shareDialogVis: false})
                        this.onShowToast(strings('设备名称不能为空'))
                    }
                }}/>)
        return (
            <View style={{width: this.mScreenWidth, alignItems: 'center'}}>
                <FlatList
                    data={list}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => this.renderItem(item, index)}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={this.handleRefresh}
                            tintColor="#18B34F" // 设置小图标颜色为蓝色ios
                            colors={["#18B34F"]} // 设置小图标颜色为蓝色android
                            title={strings('loading')}//iOS
                            titleColor={'#999BA2'}
                        />
                    }
                    onEndReached={this.handleLoadMore}
                    ListFooterComponent={this.renderFooter}
                    onEndReachedThreshold={0.5}
                    ListEmptyComponent={
                        !loading && list.length === 0 ? (
                            <View style={{
                                paddingVertical: 20,
                                width: this.mScreenWidth,
                                height: 500,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                <Image source={require('../../resources/greenUnion/msg_nomsg_ic.png')}/>
                            </View>
                        ) : null
                    }
                />
                {_accpetShareDialog}
            </View>
        );
    }
}

export default SharingEquipment;


