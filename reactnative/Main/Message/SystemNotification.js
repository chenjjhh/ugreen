
/*
*消息中心/系统通知
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, 
    TouchableOpacity, 
    TextInput,
    ActivityIndicator,
    FlatList,
    RefreshControl,
    DeviceEventEmitter
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from '../Net/NetUtil'
import CommonTextView from '../View/CommonTextView'
import I18n from 'react-native-i18n';
import StorageHelper from "../Utils/StorageHelper";
import FastImage from "react-native-fast-image";
import Helper from "../Utils/Helper";
import EventUtil from "../Event/EventUtil";
class SystemNotification extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pageNum: 1,
      pageSize:10,
      loading: false,
      refreshing: false,
      noMoreData: true,
      isLoadingMore: false,
      token: StorageHelper.getTempToken() || ''
    };
  }

  componentDidMount() {
    this.getData();
    this.toReadListener = DeviceEventEmitter.addListener(EventUtil.SYS_ALL_READ, (value) => {
      this.getData();
    });
  }

  componentWillUnmount(){
    this.toReadListener?.remove()
  }

  getData = () => {
    const { pageNum,pageSize, list, token} = this.state;
    this.setState({ loading: true });
    NetUtil.systemNotice(token,pageNum,pageSize,true)
            .then((res) => {
                if (res?.list?.length > 0) {
                    this.setState({
                        list: pageNum === 1 ? res?.list : [...list, ...res?.list],
                        loading: false,
                        refreshing: false,
                        isLoadingMore: false,
                      });
                }else{
                    this.setState({
                        loading: false,
                        refreshing: false,
                        noMoreData: true,
                        isLoadingMore: false,
                      });
                }
            }).catch((error) => {
                console.error(error);
        })
    }

  handleRefresh = () => {
    this.setState({ refreshing: true, pageNum: 1, noMoreData: false }, () => {
      this.getData();
    });
  };

  handleLoadMore = () => {
    const { isLoadingMore, noMoreData } = this.state;
    if (!isLoadingMore && !noMoreData) {
      this.setState(
        prevState => ({
          pageNum: prevState.pageNum + 1,
          isLoadingMore: true,
        }),
        () => {
          this.getData();
        }
      );
    }
  };

  // 消息详情
  goDetails = (data) => {
    const {  token } = this.state;
    this.setState({ loading: true });
    this.props.showDialog?.(res?.info)

    NetUtil.sysNoticeDetails(token,data?.noticePushId,true)
            .then((res) => {
                if (res?.info) {
                    this.props.showDialog?.(res?.info)
                }
            }).catch((error) => {
                console.error(error);
        })
    }
  
  renderFooter = () => {
      const { isLoadingMore, noMoreData, list } = this.state;
      if (isLoadingMore && !noMoreData) {
        return (
          <View style={{ paddingVertical: 20 }}>
            <ActivityIndicator animating size='large' color={'#18B34F'}/>
          </View>
        );
      } else if (noMoreData && list?.length > 0) {
        return (
          <View style={{ paddingVertical: 20 }}>
            <Text style={{ textAlign: 'center' }}>没有更多数据了</Text>
          </View>
        );
      } else {
        return null;
      }
  };

  renderItem = (item, index)=>{
    return(<TouchableOpacity onPress={()=>this.goDetails(item)} key={index} style={{width:this.mScreenWidth,flexDirection:'row',justifyContent:'center'}}>
        <View style={{backgroundColor:'#fff',height:100,width:this.mScreenWidth-40,flexDirection:'row',alignItems:'center',borderRadius:10,marginBottom:15}}>
            <View style={{width:70,height:70,marginHorizontal:20}}>
                <Image style={{position:'absolute',width:'100%',height:'100%',}} source={require('../../resources/ic_display_small.png')}/>
                <FastImage
                    style={{ width: '100%', height: '100%' }}
                    source={{
                        uri: item?.imgUrl,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                />
            </View> 
            <View style={{flex:1,flexDirection: 'row',alignItems:'center',justifyContent:'space-between',marginRight:10}}>
                <View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Text numberOfLines={1}>
                            <CommonTextView
                            textSize={15}
                            text={item.title}
                            style={{
                                color: '#404040',
                                fontWeight: 'bold',
                            }}/>
                        </Text>
                        {!item.read?<View style={{width:7,height:7,backgroundColor:'#18B34F',borderRadius:5,marginHorizontal:5}}></View>:null}
                    </View>
                    <Text numberOfLines={1}>
                        <CommonTextView
                            textSize={12}
                            text={item.content}
                            style={{
                                color: '#808080',
                                }}/>
                    </Text>
                    <CommonTextView
                        textSize={11}
                        text={Helper._formatTimestamp(item?.createTime)}
                        style={{
                            color: '#C4C6CD',
                            fontWeight: 'normal',
                            }}/>       
                </View>
                <Image source={require('../../resources/greenUnion/list_arrow_ic.png')}/>
            </View>
        </View>
    </TouchableOpacity>)
  }

  render() {
    const { list, loading, refreshing } = this.state;
    return (
        <View style={{width:this.mScreenWidth,alignItems:'center'}}>
            <FlatList
              data={list}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) => this.renderItem(item,index)}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={this.handleRefresh}
                  tintColor="#18B34F" // 设置小图标颜色为蓝色ios
                  colors={["#18B34F"]} // 设置小图标颜色为蓝色android
                  title={strings('loading')}//iOS
                  titleColor={'#999BA2'}
                />
              }
              onEndReached={this.handleLoadMore}
              ListFooterComponent={this.renderFooter}
              onEndReachedThreshold={0.5}
              ListEmptyComponent={
                !loading && list.length === 0 ? (
                  <View style={{ paddingVertical: 20,width:this.mScreenWidth,height:500,alignItems:'center',justifyContent:'center',}}>
                    <Image source={require('../../resources/greenUnion/msg_nomsg_ic.png')}/>
                  </View>
                ) : null
              }
            />
        </View>
    );
  }
}

export default SystemNotification;



