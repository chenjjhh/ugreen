import { every, stubFalse, stubTrue } from "lodash";
import {
  check,
  checkMultiple,
  checkNotifications,
  PERMISSIONS,
  request,
  requestMultiple,
} from "react-native-permissions";
import { NativeModules, Platform } from "react-native";
// stubTrue: 这个函数会返回一个始终返回 true 的函数，无论其输入是什么
// every: 这个函数会检查集合（数组或者对象）中的所有元素是否都满足某个断言函数。如果所有元素都使得断言函数返回 true，那么 every 函数就会返回 true；否则，返回 false。
// stubFalse: 这个函数与 stubTrue 相反，它会返回一个始终返回 false 的函数，无论其输入是什么。
const checkMultipleAction = async (...permissions) => {
  try {
    const results = await checkMultiple(permissions);
    return results;
  } catch (error) {
    console.log("error----", error);
  }
  //   return Promise.all(permissions.map(permission => check(permission)))
  //     .then(results => {
  //       console.log(results);
  //       return every(results, result => {
  //         // 'granted' | 'denied' | 'never_ask_again'
  //         return result
  //       });
  //     })
  //     .catch(stubFalse);
};

const requestMultipleAction = async (...permissions) => {
  try {
    const results = await requestMultiple(permissions);
    return results;
  } catch (error) {
    console.log("error----", error);
  }
};

// 检查公共
const checkCommon = Platform.select({
    ios: (keys) => checkMultipleAction(keys),
    android: (keys) => checkMultipleAction(keys),
});

// 请求公共
const requestCommon = Platform.select({
    ios: (keys) => requestMultiple(keys),
    android: (keys) => requestMultiple(keys),
});

// 检查蓝牙
const checkBlueTooth = Platform.select({
  ios: () => checkMultipleAction(PERMISSIONS.IOS.BLUETOOTH),
  android: () => checkMultipleAction(PERMISSIONS.ANDROID.BLUETOOTH),
});

// 请求蓝牙
const requestBlueTooth = Platform.select({
  ios: () => requestMultipleAction(PERMISSIONS.IOS.BLUETOOTH),
  android: async () => requestMultipleAction(PERMISSIONS.ANDROID.BLUETOOTH),
});

// 检查照相机/扫码
const checkCamera = Platform.select({
  ios: () => checkMultipleAction(PERMISSIONS.IOS.CAMERA),
  android: () => checkMultipleAction(PERMISSIONS.ANDROID.CAMERA),
});

// 请求照相机/扫码
const requestCamera = Platform.select({
  ios: () => requestMultipleAction(PERMISSIONS.IOS.CAMERA),
  android: () => requestMultipleAction(PERMISSIONS.ANDROID.CAMERA),
});

// 检查相册和存储
const checkLibrary = Platform.select({
  ios: () => checkMultipleAction(PERMISSIONS.IOS.PHOTO_LIBRARY),
  android: () => checkMultipleAction(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE),
});

// 请求相册和存储
const requestLibrary = Platform.select({
  ios: () => requestMultipleAction(PERMISSIONS.IOS.PHOTO_LIBRARY),
  android: () =>
    requestMultipleAction(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE),
});

// 检查定位
const checkLocation = Platform.select({
  ios: () =>
    checkMultipleAction(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE),
  android: () =>
    checkMultipleAction(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION),
});

// 请求定位
const requestLocation = Platform.select({
  ios: () =>
    requestMultipleAction(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE), // iOS 上的使用时访问位置权限
  android: () =>
    requestMultipleAction(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION), // Android 上的精确位置访问权限
});

// 检查网络
const checkNetwork = () => {
  const { PermissionsModule } = NativeModules;
  PermissionsModule.networkPermission().then(stubTrue).catch(stubFalse);
};

// 检查通知
const checkNotification = () => {
  checkNotifications()
    .then(({ status }) => status === "granted")
    .catch(stubFalse);
};

export {
  checkCamera,
  requestCamera,
  checkLibrary,
  requestLibrary,
  checkLocation,
  requestLocation,
  checkNetwork,
  checkNotification,
  checkBlueTooth,
  requestBlueTooth,
  checkCommon,
  requestCommon,
};
