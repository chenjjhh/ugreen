import BaseComponent from "../Base/BaseComponent";
import React from "react";
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View,
    BackHandler,
    Clipboard,
    TextInput,
} from "react-native";
import {strings, setLanguage} from "../Language/I18n";
import TitleBar from "../View/TitleBar";
import TextInputView from "../View/TextInputView";
import ViewHelper from "../View/ViewHelper";
import CommonBtnView from "../View/CommonBtnView";
import Helper from "../Utils/Helper";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import RouterUtil from "../Utils/RouterUtil";
import DeviceListUtil from "../Utils/DeviceListUtil";
import MessageDialog from "../View/MessageDialog";
import CommonMessageDialog from "../View/CommonMessageDialog";
import I18n from "react-native-i18n";
import NetConstants from "../Net/NetConstants";
import CommonTextView from "../View/CommonTextView";
/*
 *注销账号
 */

export default class SignOutAccount extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("delete_account")}
                    backgroundColor={"#FBFBFB"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            account: "", //邮箱
            verCode: "", //验证码
            passwordText: "", //新密码
            verCodeText: strings("get_vail_code"),
            showPdError1: false,
            getCodeAble: true,
            submitBtnEnable: false,
            verCodeClickTime: 0,
            deleteAccountUnbindDialog: false,
            deleteAccountDialog: false,
            token: StorageHelper.getTempToken() || "",
        };
    }

    componentDidMount() {
        //获取登录账号(国内：手机号、国外：邮箱)
        StorageHelper.getLoginAccount().then((account) => {
            if (account) {
                this.setState({
                    account: account,
                });
            }
        });
    }

    componentWillUnmount() {
        this._stopTimer();
    }

    //获取验证码
    _getVerCode() {
        const {account} = this.state;
        const lang = I18n.locale === "en" ? "en-US " : "zh_CN";
        if (!account) {
            this.onShowToast(strings("请检查账户是否有效"));
            return;
        }
        this.setState({
            verCodeClickTime: new Date().getTime(),
            getCodeAble: false,
        });

        NetUtil.getEmailVerCode(
            NetConstants.EMAIL_VER_CODE_LOGOFF,
            account,
            true
        )
            .then((res) => {
                this._startTimer();
            })
            .catch((error) => {
                this.setState({
                    verCodeText: strings("get_vail_code"),
                    getCodeAble: true,
                });
            });
    }

    _startTimer() {
        this._stopTimer();
        let countdownDate = new Date(new Date().getTime() + 61 * 1000);
        this.intervalTimer = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000;
            if (dif <= 0) {
                this.setState({
                    verCodeText: strings("get_vail_code"),
                    getCodeAble: true,
                });
                this._stopTimer();
            } else {
                this.setState({
                    verCodeText: parseInt(dif) + "s",
                    getCodeAble: false,
                });
            }
        }, 1000);
    }

    _stopTimer() {
        if (this.intervalTimer) {
            clearInterval(this.intervalTimer);
            this.intervalTimer = null;
        }
    }

    //注销账号前获取设备列表，查看是否有绑定的设备
    _beforeCancelGetDeviceList() {
        const {token} = this.state;
        if (!token) return;
        DeviceListUtil._getDeviceList(true, token, false)
            .then((res) => {
                var deviceInfo = res.info;
                if (deviceInfo && deviceInfo.length > 0) {
                    //存在未解绑的设备，无法注销账号
                    this.setState({
                        deleteAccountUnbindDialog: true,
                    });
                } else {
                    this.setState({
                        deleteAccountDialog: true,
                    });
                }
            })
            .catch((error) => {
                // console.log('error: ', error);
            });
    }

    //注销账号
    _userCancelAccount() {
        const {token, passwordText, verCode, account} = this.state;
        if (!token) return;
        NetUtil.userCancelAccount(token, account, passwordText, verCode, true)
            .then((res) => {
                this.onShowToast(strings("successed"));
                this._singOut();
            })
            .catch((error) => {
                this.onShowToast(strings("failed"));
            });
    }

    _singOut() {
        StorageHelper.clearAllData().then(() => {
            RouterUtil.toLogin();
        });
    }


    _getBtnEnable() {
        const {account, verCode, passwordText} = this.state;
        var flag =
            (Helper._ifValidEmail(account) && I18n.locale === "en") ||
            (Helper._ifValidPhone(account) &&
                I18n.locale === "zh") &&
            Helper._ifValidPassword(passwordText) &&
            verCode.length >= 5;

        this.setState({
            submitBtnEnable: flag,
        });
    }

    setPdErrorView(passwordText) {
        this.setState({
            showPdError1: !Helper._ifValidPassword(passwordText),
        });
    }

    render() {
        const {
            account,
            verCodeText,
            showPdError1,
            getCodeAble,
            passwordText,
            verCode,
            submitBtnEnable,
        } = this.state;
        const _accountView = (
            <View
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    borderRadius: 27,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    overflow: "hidden",
                    marginTop: 54,
                    backgroundColor: "rgba(24, 179, 79, 0.1)",
                }}
            >
                <Text style={{fontSize: this.getSize(16), color: "#18B34F"}}>
                    {account}
                </Text>
            </View>
        );
        const _verifyCodeLogin = (
            <TextInputView
                key={0}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    marginTop: 20,
                    backgroundColor: "#EBEEF0",
                }}
                verifyCodeLogin={true}
                maxLength={6}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({verCode: newText}, () => {
                        this._getBtnEnable();
                    });
                }}
                value={verCode}
                placeholder={strings("please_enter_verification_code")}
                keyboardType={"numeric"}
                placeholderTextColor={"#C4C6CD"}
                onPress={() => this._getVerCode()}
                getCodeAble={getCodeAble}
                verCodeText={verCodeText}
                {...this.props}
            />
        );

        const _passwordView = (
            <TextInputView
                passwordIconStyle={{
                    width: 24,
                    height: 24,
                }}
                key={1}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    marginTop: 20,
                    backgroundColor: "#EBEEF0",
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({passwordText: newText}, () => {
                        this._getBtnEnable();
                    });
                    this.setPdErrorView(newText);
                }}
                value={passwordText}
                placeholder={strings("sure_password")}
                keyboardType={"default"}
                placeholderTextColor={"rgba(0, 0, 0, 0.26)"}
            />
        );

        const _submitBtnView = (
            <CommonBtnView
                clickAble={submitBtnEnable}
                onPress={() => {
                    this._beforeCancelGetDeviceList();
                }}
                style={{
                    marginBottom: 40,
                    marginTop: 40,
                    width: this.mScreenWidth - 60,
                }}
                btnText={strings("delete_account")}
            />
        );
        const _dialogView = (
            <View>
                <CommonMessageDialog
                    messageStyle={{textAlign: "center"}}
                    leftBtnTextColor={"#ffffff"}
                    onLeftBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({deleteAccountUnbindDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({deleteAccountUnbindDialog: false});
                    }}
                    modalVisible={this.state.deleteAccountUnbindDialog}
                    title={strings("delete_account")}
                    message={strings("delete_account_unbind_device")}
                    leftBtnText={strings("ok")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({deleteAccountUnbindDialog: false});
                    }}
                />
                <CommonMessageDialog
                    messageStyle={{textAlign: "center"}}
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({deleteAccountDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({deleteAccountDialog: false});
                    }}
                    modalVisible={this.state.deleteAccountDialog}
                    title={strings("注销确认")}
                    message={strings("delete_account_tips")}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({deleteAccountDialog: false});
                    }}
                    rightBtnText={strings("confirm")}
                    onRightBtnClick={() => {
                        //确认
                        this.setState({deleteAccountDialog: false});
                        this._userCancelAccount();
                    }}
                />
            </View>
        );

        const _pdErrorTipsView = (
            <CommonTextView
                textSize={12}
                text={strings("input_password_error_tips")}
                style={{
                    color: "#DE7575",
                    width: this.viewCommonWith,
                    marginTop: 12,
                }}
            />
        );

        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#FBFBFB",
                    alignItems: "center",
                }}
            >
                {_accountView}
                {_verifyCodeLogin}
                {_passwordView}
                {/*{showPdError1 && passwordText ? _pdErrorTipsView : null}*/}
                {_submitBtnView}
                {_dialogView}
            </View>
        );
    }
}
