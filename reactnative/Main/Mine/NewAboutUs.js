import BaseComponent from '../Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput, Linking, ImageBackground
} from 'react-native';
import {strings, setLanguage} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import ShadowCardView from '../View/ShadowCardView'
import ListItemView from '../View/ListItemView'
import CommonTextView from '../View/CommonTextView'
import ViewHelper from "../View/ViewHelper";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import RouterUtil from "../Utils/RouterUtil";
import DeviceInfo from 'react-native-device-info';
import ItemBtnView from '../View/ItemBtnView'
import CommonMessageDialog from "../View/CommonMessageDialog";
/*
*关于我们
 */
const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
const meRegion = (require('../../resources/greenUnion/me_region_ic.png'))
const aboutInto = (require('../../resources/greenUnion/about_into_ic.png'))
const aboutPhone = (require('../../resources/greenUnion/about_phone_ic.png'))
const aboutEmail = (require('../../resources/greenUnion/about_mail_ic.png'))


const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

export default class NewAboutUs extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={''}
                    backgroundColor={'#F1F2F6'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            callDialog: false,
            webDialog: false,
            emailDialog: false,
            ruleUrl: '',//用户协议
            privacyUrl: '',//隐私协议
        }
    }

    componentWillMount() {
        this._getRule(false)
        this._getPrivacy(false)
    }

    _toRule() {
        if (this.state.ruleUrl == '') {
            this._getRule(true)
        } else {
            this._toWeb(this.state.ruleUrl, strings('user_rule'))
        }
    }

    _toPrivacy() {
        if (this.state.privacyUrl == '') {
            this._getPrivacy(true)
        } else {
            this._toWeb(this.state.privacyUrl, strings('privacy_rule'))
        }
    }

    _toWeb(toUrl, title) {
        if (toUrl) {
            this.props.navigation.navigate('UrlWebView', {url: toUrl, title: title});
        }
    }

    // 发送邮件
    _toEmail = () => {
        const url = 'mailto:cnfw@ugreen.com';
        Linking.canOpenURL(url).then((supported) => {
            if (!supported) {
                this.onShowToast('not support')
                return
            }
            return Linking.openURL(url);
        }).catch((err) => Alert(err));
    }

    _getRule(toWeb) {
        NetUtil.getRule(StorageHelper.getTempToken())
            .then((res) => {
                var url = res.info.path
                this.setState({ruleUrl: url})
                if (toWeb)
                    this._toWeb(url, strings('user_rule'))
            })
            .catch((error) => {
            })
    }

    _getPrivacy(toWeb) {
        NetUtil.getPrivacy(StorageHelper.getTempToken())
            .then((res) => {
                var url = res.info.path
                this.setState({privacyUrl: url})
                if (toWeb)
                    this._toWeb(url, strings('privacy_rule'))
            })
            .catch((error) => {
            })
    }

    render() {
        const _titleView = (
            <View style={{flex: 1, justifyContent: 'center'}}>
                <Image source={require('../../resources/greenUnion/about_logo_ic.png')}/>
            </View>
        )
        const _protocolView = (
            <View style={{
                backgroundColor: '#ffffff',
                width: this.mScreenWidth - 40,
                alignItems: 'center',
                borderRadius: 15,
                overflow: 'hidden'
            }}>
                <ItemBtnView
                    onPress={() => {
                        //用户协议
                        this._toRule()
                    }}
                    text={strings('用户协议')}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {
                        //隐私政策
                        this._toPrivacy()
                    }}
                    text={strings('个人信息保护政策')}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {

                    }}
                    text={strings('关于我们')}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {

                    }}
                    text={strings('清除缓存')}
                    rightText={'25.48MB'}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {

                    }}
                    text={strings('APP版本')}
                    rightText={'V' + DeviceInfo.getVersion()}
                    rightIcon={rightIcon}
                />
            </View>
        )

        const _informationView = (
            <View style={{
                backgroundColor: '#ffffff',
                width: this.mScreenWidth - 40,
                alignItems: 'center',
                borderRadius: 15,
                overflow: 'hidden',
                marginVertical: 12
            }}>
                <ItemBtnView
                    onPress={() => {
                        this.setState({
                            webDialog: true
                        })
                    }}
                    containerStyle={{marginLeft: 0}}
                    text={'www.lulian.com'}
                    rightIcon={aboutInto}
                    leftIcon={meRegion}
                />
                <ItemBtnView
                    onPress={() => {
                        this.setState({
                            callDialog: true
                        })
                    }}
                    containerStyle={{marginLeft: 0}}
                    text={'0755-28066995'}
                    rightIcon={rightIcon}
                    leftIcon={aboutPhone}
                />
                <ItemBtnView
                    onPress={() => {
                        this.setState({
                            emailDialog: true
                        })
                    }}
                    containerStyle={{marginLeft: 0}}
                    text={'cnfw@ugreen.com'}
                    rightIcon={rightIcon}
                    leftIcon={aboutEmail}
                />
            </View>
        )

        const _dialogView = (
            <View>
                {/* 拨打电话弹窗 */}
                <CommonMessageDialog
                    messageStyle={{textAlign: 'center'}}
                    leftBtnTextColor={'#808080'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    rightBtnTextColor={'#ffffff'}
                    onRightBackgroundColor={'#18B34F'}
                    onRequestClose={() => {
                        this.setState({callDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({callDialog: false})
                    }}
                    children={(
                        <View style={{alignItems: 'center', marginVertical: 25}}>
                            <Text
                                style={{color: '#808080', fontSize: this.getSize(13)}}>{strings('是否拨打绿联官方客服热线')}</Text>
                            <Text style={{color: '#808080', fontSize: this.getSize(13)}}>{'0755-28066995'}</Text>
                        </View>
                    )}
                    style={{paddingTop: 0}}
                    modalVisible={this.state.callDialog}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({callDialog: false})
                    }}
                    rightBtnText={strings('拨打')}
                    onRightBtnClick={() => {
                        //拨打
                        this.setState({callDialog: false}, () => {
                            const url = 'tel:0755-28066995';
                            Linking.canOpenURL(url).then((supported) => {
                                if (!supported) {
                                    this.onShowToast('not support')
                                    return
                                }
                                return Linking.openURL(url);
                            }).catch((err) => Alert(err));
                        })
                    }}/>
                {/* 跳转网站弹窗 */}
                <CommonMessageDialog
                    messageStyle={{textAlign: 'center'}}
                    leftBtnTextColor={'#808080'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    rightBtnTextColor={'#ffffff'}
                    onRightBackgroundColor={'#18B34F'}
                    onRequestClose={() => {
                        this.setState({webDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({webDialog: false})
                    }}
                    children={(
                        <View style={{alignItems: 'center', marginVertical: 40}}>
                            <Text style={{color: '#808080', fontSize: this.getSize(13)}}>{strings('跳转到绿联官网提示语')}</Text>
                        </View>
                    )}
                    style={{paddingTop: 0}}
                    modalVisible={this.state.webDialog}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({webDialog: false})
                    }}
                    rightBtnText={strings('comfire')}
                    onRightBtnClick={() => {
                        //跳转官网
                        this.setState({webDialog: false}, () => {
                            this._toWeb('https://www.gendome.com', DeviceInfo.getApplicationName() ? DeviceInfo.getApplicationName() : 'GENDOME')
                        })
                    }}/>
                {/* 跳转到发送邮件提示弹窗 */}
                <CommonMessageDialog
                    messageStyle={{textAlign: 'center'}}
                    leftBtnTextColor={'#808080'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    rightBtnTextColor={'#ffffff'}
                    onRightBackgroundColor={'#18B34F'}
                    onRequestClose={() => {
                        this.setState({emailDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({emailDialog: false})
                    }}
                    children={(
                        <View style={{alignItems: 'center', marginVertical: 40}}>
                            <Text style={{color: '#808080', fontSize: this.getSize(13)}}>{strings('跳转到发送邮件提示语')}</Text>
                        </View>
                    )}
                    style={{paddingTop: 0}}
                    modalVisible={this.state.emailDialog}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({emailDialog: false})
                    }}
                    rightBtnText={strings('comfire')}
                    onRightBtnClick={() => {
                        this.setState({emailDialog: false})
                        //跳转发送邮件
                        this._toEmail()
                    }}/>
            </View>

        )
        return (
            // <View style={{position:'absolute',top:-(statusBarHeight+55),height:this.mScreenHeight+statusBarHeight+55,width:this.mScreenWidth}}>
            //     <ImageBackground
            //         source={require('../../resources/greenUnion/loginBack.png')}
            //         resizeMode={'stretch'}
            //         style={{flex: 1,paddingTop:statusBarHeight+55,justifyContent: 'space-between',alignItems:'center'}}>
            <ScrollView style={{flex: 1}}>
                <View style={{
                    height: this.mScreenHeight - (statusBarHeight + 55),
                    width: this.mScreenWidth,
                    alignItems: 'center',
                }}>
                    {_titleView}
                    {_protocolView}
                    {_informationView}
                    {_dialogView}
                </View>
            </ScrollView>
            //         </ImageBackground>
            // </View>
        );
    }

}