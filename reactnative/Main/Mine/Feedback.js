import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import ViewHelper from '../View/ViewHelper'
import TextInputView from "../View/TextInputView";
import ShadowCardView from "../View/ShadowCardView";
import ListItemView from "../View/ListItemView";
import CommonBtnView from "../View/CommonBtnView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import {launchCamera, launchImageLibrary} from "react-native-image-picker";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager

/*
*意见反馈
 */

var options = {
    quality: 0.3,
    mediaType: 'photo'
}

export default class Feedback extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    titleText={strings('feedback')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            suggestionValue: '',
            questionTypeDialog: false,
            questionTypeArray: [strings('feedbackType1'), strings('feedbackType2'), strings('feedbackType3')],
            questionTypeSelectIndex: 0,
            deviceId: '',
            deviceName: '',
            token: props.navigation.state.params.token,
            imgs: [],
            imgsUri: [],
            imagePickerDialogArray: [strings('take_photo'), strings('photo')],
            imagePickerDialog: false,
            imgIndex: 1,
            btnClickTime: 0
        }
    }

    componentWillMount() {
        this._eventListenerAdd()
    }

    componentWillUnmount() {
        this._eventListenerRemove()
    }

    _eventListenerAdd() {
        this.feedbackDeviceListener = DeviceEventEmitter.addListener(EventUtil.FEEDBACK_DEVICE, (value) => {
            //收到通知
            var deviceData = value.deviceData
            this.setState({
                deviceId: deviceData.deviceId,
                deviceName: deviceData.deviceNickname,
            })
        })
    }

    _eventListenerRemove() {
        this.feedbackDeviceListener && this.feedbackDeviceListener.remove()
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                    alignItems: 'center'
                }}>
                {this._inputSuggestionView()}
                {this._imageView()}
                {this._listItemView()}
                {ViewHelper.getFlexView()}
                {this._doneBtnView()}

                {this._dialogView()}
            </View>
        );
    }

    _cancelTextInputFouce() {
        Keyboard.dismiss();
    }

    _dialogView() {
        return (
            <View>
                <SelectDialog
                    onCancelClick={() => {
                        this.setState({questionTypeDialog: false})
                    }}
                    onRequestClose={() => {
                        this.setState({questionTypeDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({questionTypeDialog: false})
                    }}
                    modalVisible={this.state.questionTypeDialog}
                    selectArray={this.state.questionTypeArray}
                    onItemClick={(index) => {
                        this.setState({
                            questionTypeDialog: false,
                            questionTypeSelectIndex: index
                        })
                    }}
                />


                <SelectDialog
                    onCancelClick={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    title={''}
                    onRequestClose={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    modalVisible={this.state.imagePickerDialog}
                    selectArray={this.state.imagePickerDialogArray}
                    onItemClick={(index) => {
                        this.setState({imagePickerDialog: false}, () => setTimeout(() => {
                            if (index == 0) {//拍照
                                if (Platform.OS == 'android') {
                                    this._launchCamera()
                                } else {
                                    GXRNManager.checkPhotoPermissions(1).then((datas) => {
                                        this._launchCamera()
                                    }).catch((err) => {
                                        LogUtil.debugLog('err', err);
                                    });
                                }

                            } else {//相册
                                if (Platform.OS == 'android') {
                                    this._launchImageLibrary()
                                } else {
                                    GXRNManager.checkPhotoPermissions(2).then((datas) => {
                                        this._launchImageLibrary()
                                    }).catch((err) => {
                                        LogUtil.debugLog('err', err);
                                    });
                                }
                            }
                        }, 500));
                    }}
                />
            </View>
        )
    }

    _inputSuggestionView() {
        return (
            <TextInputView
                ref={ref => this.InputText = ref}
                //maxLength={1000}
                multiline={true}
                style={{
                    width: this.mScreenWidth - 36,
                    height: this.mScreenHeight * 0.2,
                    marginTop: 20
                }}
                inputStyle={{
                    height: this.mScreenHeight * 0.2,
                    textAlignVertical: 'top',
                    paddingTop: 20,
                    paddingBottom: 20,
                    paddingLeft: 24,
                    paddingRight: 24
                }}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({suggestionValue: newText})
                }}
                value={this.state.suggestionValue}
                placeholder={strings('input_your_suggestion')}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}
            />
        )
    }

    _imageView() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    marginTop: 20,
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                {this._imageItemView(0)}
                <View style={{
                    width: 20, height: 1
                }}/>
                {this._imageItemView(1)}
                <View style={{
                    width: 20, height: 1
                }}/>
                {this._imageItemView(2)}
            </View>
        )
    }

    _imageItemView(index) {
        return (
            <ShadowCardView
                onPress={() => {
                    this.setState({
                        imgIndex: index,
                        imagePickerDialog: true,
                    })
                    this._cancelTextInputFouce()
                }}
                style={{
                    width: this.mScreenWidth * 0.26,
                    height: this.mScreenWidth * 0.26,
                    alignItems: 'center',
                    justifyContent: 'center',
                    overflow: 'hidden'
                }}>
                <Image
                    source={require('../../resources/upload_pic_ic.png')}/>


                <Image
                    style={{
                        width: this.mScreenWidth * 0.26,
                        height: this.mScreenWidth * 0.26,
                        resizeMode: 'cover',
                        position: 'absolute'
                    }}
                    source={this.state.imgsUri[index] ? {uri: this.state.imgsUri[index]} :
                        this.state.imgs[index] ? {uri: this.state.imgs[index]} : 0}/>
            </ShadowCardView>
        )
    }

    _listItemView() {
        return (
            <View
                style={{
                    marginTop: 20
                }}>
                <ShadowCardView>
                    <ListItemView
                        title={strings('question_type')}
                        value={this.state.questionTypeArray[this.state.questionTypeSelectIndex]}
                        onPress={() => {
                            //问题类型
                            this.setState({questionTypeDialog: true})
                            this._cancelTextInputFouce()
                        }}/>
                </ShadowCardView>

                {
                    this.state.questionTypeSelectIndex == 1 ?
                        <ShadowCardView
                            style={{
                                marginTop: 16
                            }}>
                            <ListItemView
                                title={strings('select_device')}
                                value={this.state.deviceId == '' ? strings('select_bind_device') : this.state.deviceName}
                                onPress={() => {
                                    //选择设备
                                    this.props.navigation.push('SelectDeviceList', {type: 1});
                                }}/>
                        </ShadowCardView> : null
                }

            </View>
        )
    }

    _doneBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.suggestionValue.length > 0}
                onPress={() => {
                    this._submit()
                }}
                style={{
                    marginBottom: 44,
                    width: this.mScreenWidth - 100
                }}
                btnText={strings('submit')}/>
        )
    }

    _submit() {
        if (new Date().getTime() - this.state.btnClickTime < 2000) {
            return
        }

        if (this.state.suggestionValue.length > 1000) {
            this.onShowToast(strings('submit_failed_over_1000_strings'))
            return
        }

        if (this.state.questionTypeSelectIndex == 1 && this.state.deviceId == '') {
            this.onShowToast(strings('select_bind_device'))
            return
        }

        this.setState({
            btnClickTime: new Date().getTime()
        })

        var type = parseInt(this.state.questionTypeSelectIndex) + 1

        NetUtil.feedback(this.state.token, this.state.deviceId, type, this.state.suggestionValue, Helper.getPlatformOsFlag(), this.state.imgs, true)
            .then((res) => {
                this.onShowToast(strings('submit_success'))
                this.props.navigation.goBack();
            })
            .catch((error) => {
                this.onShowToast(strings('failed'))
            })
    }


    //打开相机
    _launchCamera() {
        launchCamera(options, (response) => {
            LogUtil.debugLog('Response = ', JSON.stringify(response));

            if (response.didCancel) {
                LogUtil.debugLog('User cancelled image picker');
            } else if (response.errorMessage) {
                LogUtil.debugLog('ImagePicker Error: ', response.errorMessage);
                Helper.openSettings(strings('camera_permission_tips'))
            } else if (response.errorCode) {
                LogUtil.debugLog('errorCode: ', response.errorCode);
                Helper.openSettings(strings('camera_permission_tips'))
            } else {
                const source = {uri: response.uri};

                var responseUri = response.assets[0].uri

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let file = {
                    uri: responseUri,
                    type: 'multipart/form-data',
                    name: this.state.imgIndex + 'feedback.png'
                };   //这里的key(uri和type和name)不能改变,

                this._getImageUploadUrl(this.state.token, file, responseUri)

            }


        });
    }

    //打开相册
    _launchImageLibrary() {
        launchImageLibrary(options, (response) => {
            LogUtil.debugLog('Response = ', JSON.stringify(response));

            if (response.didCancel) {
                LogUtil.debugLog('User cancelled image picker');
            } else if (response.errorMessage) {
                LogUtil.debugLog('ImagePicker Error: ', response.errorMessage);
                Helper.openSettings(strings('camera_permission_tips'))
            } else if (response.errorCode) {
                LogUtil.debugLog('errorCode: ', response.errorCode);
                Helper.openSettings(strings('camera_permission_tips'))
            } else {
                const source = {uri: response.uri};

                var responseUri = response.assets[0].uri

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let file = {
                    uri: responseUri,
                    type: 'multipart/form-data',
                    name: this.state.imgIndex + 'feedback.png'
                };   //这里的key(uri和type和name)不能改变,

                this._getImageUploadUrl(this.state.token, file, responseUri)
            }


        });
    }

    //获取图片的上传地址
    _getImageUploadUrl(token, file, imageUri) {
        NetUtil.getFileUploadUrl(token, NetConstants.IMAGE_AVATAE_FILE_NAME, true)
            .then((res) => {
                var urlData = res.info
                if (urlData) {
                    var imageUploadUrl = urlData.preUrl
                    var imageUrl = urlData.fileUrl
                    this._uploadImage(imageUploadUrl, file, imageUrl, imageUri)
                }
            })
            .catch((error) => {

            })
    }

    //上传图片
    _uploadImage(preUrl, file, imageUrl, imageUri) {
        NetUtil.uploadFile(preUrl, file, true)
            .then((res) => {
                //图片上传成功
                this._putImageToArray(this.state.imgIndex, imageUrl, imageUri)
            })
            .catch((error) => {

            })
    }

    _putImageToArray(index, imageUrl, imageUri) {
        var tempImageUrls = this.state.imgs
        var tempImageUris = this.state.imgsUri
        var index = this.state.imgIndex
        tempImageUrls[index] = imageUrl
        tempImageUris[index] = imageUri

        this.setState({
            imgs: tempImageUrls,
            imgsUri: tempImageUris
        })
    }
}