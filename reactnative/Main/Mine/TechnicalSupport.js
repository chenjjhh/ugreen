// 我的/技术支持
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Platform,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import { strings } from "../Language/I18n";
import NetUtil from "../Net/NetUtil";
import CommonTextView from "../View/CommonTextView";
import I18n from "react-native-i18n";
import CommonMessageDialog from "../View/CommonMessageDialog";
import TitleBar from "../View/TitleBar";
import CommonBtnView from "../View/CommonBtnView";
import FastImage from "react-native-fast-image";
import Helper from "../Utils/Helper";
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";
export default class TechnicalSupport extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          titleText={strings("技术支持")}
          backgroundColor={"#F1F2F6"}
          leftIcon={require("../../resources/back_black_ic.png")}
          leftIconClick={() => {
            navigation.goBack();
          }}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      hint: strings("扫码提示语"),
      customerServiceHotline: "0000-553535",
      onlineTime: strings("周一至周六") + "  " + "9:00-22:00",
    };
  }

  componentDidMount() {}

  render() {
    const { hint, customerServiceHotline, onlineTime } = this.state;
    return (
      <View
        style={{
          backgroundColor: "#F1F2F6",
          alignItems: "center",
          height: this.mScreenHeight,
          width: this.mScreenWidth,
        }}
      >
        <View
          style={{
            flex: 1,
            width: this.mScreenWidth - 40,
            alignItems: "center",
          }}
        >
          <View
            style={{ width: "100%", marginVertical: 20, alignItems: "center" }}
          >
            <Text
              style={{
                fontSize: 14,
                color: "#808080",
                lineHeight: 22,
                textAlign: "center",
              }}
            >
              {hint}
            </Text>
            <View
              style={{
                height: 222,
                width: 222,
                borderRadius: 12,
                alignItems: "center",
                justifyContent: "center",
                marginTop: 20,
                backgroundColor: "#ffffff",
              }}
            >
              <Image
                style={{ height: 173, width: 173, borderRadius: 12 }}/>
            </View>
          </View>
          <View
            style={{
              width: "100%",
              backgroundColor: "#fff",
              borderRadius: 10,
              paddingHorizontal: 21,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                height: 60,
                borderBottomColor: "#EBEBEB",
                borderBottomWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 16, color: "#404040" }}>
                {strings("客服热线")}
              </Text>
              <Text style={{ fontSize: 14, color: "#808080" }}>
                {customerServiceHotline}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                height: 60,
                borderBottomColor: "#EBEBEB",
                borderBottomWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 16, color: "#404040" }}>
                {strings("在线时间")}
              </Text>
              <Text style={{ fontSize: 14, color: "#808080" }}>
                {onlineTime}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
