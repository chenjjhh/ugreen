import BaseComponent from '../Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';
import {strings, setLanguage} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import ShadowCardView from '../View/ShadowCardView'
import ListItemView from '../View/ListItemView'
import CommonTextView from '../View/CommonTextView'
import LinkThirdDialog from '../View/LinkThirdDialog'
import MessageDialog from '../View/MessageDialog'
import StorageHelper from "../Utils/StorageHelper";
import CommonBtnView from "../View/CommonBtnView";
import ViewHelper from "../View/ViewHelper";
import RouterUtil from "../Utils/RouterUtil";
import NetUtil from "../Net/NetUtil";
import DeviceListUtil from "../Utils/DeviceListUtil";
import {AccessToken, LoginManager, Profile} from "react-native-fbsdk-next";
import Helper from "../Utils/Helper";
import {GoogleSignin, statusCodes} from "@react-native-google-signin/google-signin";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";
import ItemBtnView from '../View/ItemBtnView'
import SelectDialog from '../View/SelectDialog'
import TextInputDialog from "../View/TextInputDialog";
import LoadingUtil from "../View/Loading/LoadingUtil";
import FastImage from "react-native-fast-image";
import I18n from 'react-native-i18n';
import CommonMessageDialog from "../View/CommonMessageDialog";
import EventUtil from "../Event/EventUtil";

var GXRNManager = NativeModules.GXRNManager

/*
*我的/用户信息
 */

const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

var options = {
    quality: 0.2,
    mediaType: 'photo'
}

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
export default class NewAccount extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('个人信息')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            disassociateDialog: false,//解绑弹窗
            deleteAccountDialog: false,//注销弹窗
            deleteAccountUnbindDialog: false,//注销未解绑设备弹窗
            accountValue: '',
            signOutDialog: false,
            token: StorageHelper.getTempToken() || '',
            nickName: '',
            thirdType: '',
            thirdEmail: '',

            thirdInfo: [],
            unBindType: '',
            imagePickerDialog: false,
            updateNickNameDialog: false,
            userName: '',
            userNameInput: '',//记录输入框值
            imagePickerDialogArray: [strings('take_photo'), strings('photo')],
            googleNickName: strings('未关联'),//关联的谷歌账号昵称
            facebookNickName: strings('未关联'),//关联的脸书账号昵称
            appleNickName: strings('未关联'),//关联的apple账号昵称
            wechatNickName: strings('未关联'),//关联的微信账号昵称
            qqNickName: strings('未关联'),//关联的QQ账号昵称

        }
    }

    componentWillMount() {
        this._getUserInfo()
        this._getThirdInfo()
        this._eventListenerAdd()
    }

    componentWillUnmount() {
        this.updateUserinfoDeviceNameListener?.remove()
    }

    _eventListenerAdd() {
        // 获取用户信息
        this._getUserInfo()
        // 获取第三那方用户信息
        this._getThirdInfo()
    }

    _getThirdUserInfo(type) {
        if (type == 'Facebook') {
            this.getFacebookProfile()
        } else if (type == 'Google') {
            this.getGoogleCurrentUser()
        }
    }

    getGoogleCurrentUser = async () => {
        const currentUser = await GoogleSignin.getCurrentUser();
        if (currentUser?.user?.nickName) {
            this.setState({
                googleNickName: currentUser.user.nickName
            })
        }
    };

    getFacebookProfile() {
        Profile.getCurrentProfile().then(
            function (currentProfile) {
                if (currentProfile?.nickName) {
                    this.setState({
                        facebookNickName: currentProfile.nickName
                    })
                }
            }
        );
    }

    //获取本地存储的用户信息
    _getUserInfo() {
        StorageHelper.getUserInfo()
            .then((userInfo) => {
                    console.log('userInfo: ', userInfo);
                    if (userInfo) {
                        this.setState({
                            accountValue: userInfo?.phone || '',
                            avatarUrl: userInfo?.avatar || '',
                            userName: userInfo?.name || '',
                            token: StorageHelper.getTempToken() || '',
                        })
                    }
                }
            )
    }

    // 获取第三方登录信息
    _getThirdInfo() {
        if (this.state.token) {
            NetUtil.getThirdInfo(this.state.token, true)
                .then((res) => {
                    if (res.info && res.info.length > 0) {
                        var data = res.info[0]
                        this.setState({
                            nickName: data.nickName,
                            thirdType: data.type,
                            thirdInfo: res.info
                        })
                        this._getThirdUserInfo(data.type)
                    } else {
                        this.setState({
                            nickName: '',
                            thirdType: '',
                            thirdEmail: '',
                            thirdInfo: []
                        })
                    }
                })
        }
    }

    // 隐藏加载图标
    _dimissLoadingAndToast() {
        LoadingUtil.dismissLoading()
        this.onShowToast(strings('update_fail'))
    }

    //修改用户名称
    _updateUserName(updateName, token) {
        console.log('token: ', token);
        console.log('updateName: ', updateName);
        console.log('this.state.userName: ', this.state.userName);
        if (!token || (updateName.toString().trim() == this.state.userName.toString().trim())) {
            return
        }

        NetUtil.updateUserInfo(true, token, updateName)
            .then((res) => {
                this.onShowToast(strings('update_success'))
                this.setState({userName: updateName})
                EventUtil.sendEventWithoutValue(EventUtil.UPDATE_USER_INFO)
            })
            .catch((error) => {
                this.onShowToast(strings('update_fail'))
            })
    }

    //修改头像
    _updateAvatar(avatarUrl, token, imageUri) {
        if (token == '' || avatarUrl == '') {
            LoadingUtil.dismissLoading()
            return
        }

        NetUtil.updateUserInfo(false, token, null, avatarUrl)
            .then((res) => {
                //更新头像成功
                this.onShowToast(strings('update_success'))
                this.setState({
                    avatarUrl: avatarUrl,//网络
                    avatarUri: imageUri,//本地图片
                })
                EventUtil.sendEventWithoutValue(EventUtil.UPDATE_USER_INFO)
                LoadingUtil.dismissLoading()
            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }

    //打开相机
    _launchCamera() {
        launchCamera(options, (response) => {
            LogUtil.debugLog('Response = ', JSON.stringify(response));

            if (response.didCancel) {
                LogUtil.debugLog('User cancelled image picker');
            } else if (response.errorMessage) {
                LogUtil.debugLog('ImagePicker Error: ', response.errorMessage);
                Helper.openSettings(strings('camera_permission_tips'))
            } else if (response.errorCode) {
                LogUtil.debugLog('errorCode: ', response.errorCode);
                Helper.openSettings(strings('camera_permission_tips'))
            } else {
                const source = {uri: response.uri};

                var responseUri = response.assets[0].uri

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let file = {
                    uri: responseUri,
                    type: 'multipart/form-data',
                    name: NetConstants.IMAGE_AVATAE_FILE_NAME
                };   //这里的key(uri和type和name)不能改变,

                this._getImageUploadUrl(this.state.token, file, responseUri)
            }
        });
    }

    //打开相册
    _launchImageLibrary() {
        launchImageLibrary(options, (response) => {
            LogUtil.debugLog('Response = ', JSON.stringify(response));

            if (response.didCancel) {
                LogUtil.debugLog('User cancelled image picker');
            } else if (response.errorMessage) {
                LogUtil.debugLog('ImagePicker Error: ', response.errorMessage);
                Helper.openSettings(strings('camera_permission_tips'))
            } else if (response.errorCode) {
                LogUtil.debugLog('errorCode: ', response.errorCode);
                Helper.openSettings(strings('camera_permission_tips'))
            } else {
                const source = {uri: response.uri};

                var responseUri = response.assets[0].uri

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let file = {
                    uri: responseUri,
                    type: 'multipart/form-data',
                    name: NetConstants.IMAGE_AVATAE_FILE_NAME
                };   //这里的key(uri和type和name)不能改变,

                this._getImageUploadUrl(this.state.token, file, responseUri)
            }
        });
    }

    //获取图片的上传地址
    _getImageUploadUrl(token, file, imageUri) {
        LoadingUtil.showLoading()
        NetUtil.getFileUploadUrl(token, NetConstants.IMAGE_AVATAE_FILE_NAME, false)
            .then((res) => {
                var urlData = res.info
                if (urlData) {
                    var imageUploadUrl = urlData.preUrl
                    var imageUrl = urlData.fileUrl
                    this._uploadImage(imageUploadUrl, file, imageUrl, imageUri)
                } else {
                    this._dimissLoadingAndToast()
                }
            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }

    //上传图片
    _uploadImage(preUrl, file, imageUrl, imageUri) {
        NetUtil.uploadFile(preUrl, file, false)
            .then((res) => {
                //头像上传成功
                this._updateAvatar(imageUrl, this.state.token, imageUri)

            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }

    // 绑定第三方
    _thirdEvent(thirdFlag) {
        var removeBindFlag = false
        // 判断是否已经绑定相关第三方
        this.state.thirdInfo.map((item, index) => {
            if (thirdFlag == item.type) {
                removeBindFlag = true
                // 记录解绑类型
                this.setState({
                    unBindType: item.type
                })
            }
        })

        // 如果已绑定，则显示解绑弹窗
        if (removeBindFlag) {
            this.setState({
                disassociateDialog: true,

            })
            return
        }

        if (thirdFlag == Helper.channel_login_facebook) {
            this._facebookLogin()
        } else if (thirdFlag == Helper.channel_login_google) {
            this._googleLogin()
        } else if (thirdFlag == Helper.channel_login_apple) {
            this._appleLogin()
        }
    }

    // 退出登录
    _singOut() {
        StorageHelper.clearAllData().then(() => {
            RouterUtil.toLogin()
        })
    }

    //注销账号前获取设备列表，查看是否有绑定的设备
    _beforeCancelGetDeviceList() {
        if (this.state.token == '')
            return

        DeviceListUtil._getDeviceList(true, this.state.token, false)
            .then((res) => {
                var deviceInfo = res.info
                if (deviceInfo && deviceInfo.length > 0) {
                    //存在未解绑的设备，无法注销账号
                    this.setState({
                        deleteAccountUnbindDialog: true
                    })
                } else {
                    this.setState({
                        deleteAccountDialog: true
                    })
                }
            })
            .catch((error) => {
            })
    }

    //注销账号
    _userCancelAccount() {
        if (this.state.token == '')
            return

        NetUtil.userCancelAccount(this.state.token, true)
            .then((res) => {
                this.onShowToast(strings('successed'))
                this._singOut()
            })
            .catch((error) => {
                this.onShowToast(strings('failed'))
            })
    }

    // 解绑第三方
    _unBindThird() {
        if (this.state.token == '' || this.state.unBindType == '')
            return

        NetUtil.unBindThird(this.state.token, this.state.unBindType, true)
            .then((res) => {
                this._getThirdInfo()
                this._thirdSignOut(this.state.unBindType)
            })
    }

    _thirdSignOut(type) {
        if (type == 'Facebook') {
            this.facebookSignOut()
        } else if (type == 'Google') {
            this.googleSignOut()
        }

    }

    googleSignOut = async () => {
        try {
            await GoogleSignin.signOut();
        } catch (error) {
            LogUtil.debugLog(error);
        }
    };

    facebookSignOut = async () => {
        try {
            await LoginManager.logOut()
        } catch (error) {
            LogUtil.debugLog(error);
        }
    };

    _facebookLogin() {
        LoginManager.logInWithPermissions(["public_profile"]).then(
            (result) => {
                if (result.isCancelled) {
                    LogUtil.debugLog("Login cancelled");
                } else {
                    LogUtil.debugLog(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            LogUtil.debugLog('facebook_AccessToken:' + data.accessToken.toString())
                            if (data.accessToken) {
                                this._thirdAuth(NetConstants.FACEBOOK_APPID, data.accessToken, Helper.channel_login_facebook)
                            }
                        }
                    )
                }
            },
            (error) => {
                LogUtil.debugLog("Login fail with error: " + error);
            }
        );
    }

    // Somewhere in your code
    _googleLogin = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            LogUtil.debugLog('google_login_userInfo:' + JSON.stringify(userInfo))
            const idToken = await GoogleSignin.getTokens();
            LogUtil.debugLog('google_login_idToken:' + JSON.stringify(idToken))
            if (userInfo.idToken) {
                this._thirdAuth(Platform.OS == 'android' ? NetConstants.GOOGLE_ANDROID_CLIENTID : NetConstants.GOOGLE_IOS_CLIENTID, userInfo.idToken, Helper.channel_login_google)
            } else {
                this.onShowToast('idToken is null')
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                LogUtil.debugLog('google_login_fail:user cancelled ====>' + JSON.stringify(error))
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
                LogUtil.debugLog('google_login_fail:is in progress already ====>' + JSON.stringify(error))
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                LogUtil.debugLog('google_login_fail:play services not available or outdated ====>' + JSON.stringify(error))
            } else {
                // some other error happened
                LogUtil.debugLog('google_login_fail:some other error happened ====>' + JSON.stringify(error))
            }
        }
    };

    _appleLogin() {
        GXRNManager.signInWithAppleId().then((datas) => {
            LogUtil.debugLog('apple_login:', JSON.stringify(datas));
            if (datas.identityToken) {
                this._thirdAuth(NetConstants.APPLE_CLIENTID, datas.identityToken, Helper.channel_login_apple)
            }
        }).catch((err) => {
            LogUtil.debugLog('apple_login_fail:', JSON.stringify(err));
        });
    }

    _thirdAuth(clientId, idToken, loginType) {
        NetUtil.accountBindThird(clientId, idToken, loginType, this.state.token, true)
            .then((res) => {
                this.onShowToast(strings('bind_third_success'))
                this._getThirdInfo()
            })
        // if (info) {
        //     if (info.register == true) {
        //         //需要绑定邮箱
        //         this._toBindEmail(info.data, loginType)
        //     } else {
        //         //直接登录
        //         var token = info.data.granwin_token
        //         StorageHelper.saveToken(token)
        //         StorageHelper.saveLoginInfo(info)
        //         RouterUtil.toMainDeviceList(token)
        //     }
        // }
    }

    _toBindEmail(data, loginType) {
        this.props.navigation.navigate('BindEmail', {
            data: data,
            loginType: loginType
        });
    }

    render() {
        StatusBar.setBarStyle('light-content');
        const {
            avatarUrl,
            avatarUri,
            accountValue,
            userName,
            nickName,
            googleNickName,
            facebookNickName,
            appleNickName,
            wechatNickName,
            qqNickName
        } = this.state;
        const _accountView = (
            <View style={{
                width: this.mScreenWidth - 40,
                height: 94,
                borderRadius: 12,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingHorizontal: 15,
                backgroundColor: '#fff',
                marginVertical: 10
            }}>
                <CommonTextView
                    textSize={14}
                    text={strings('头像')}
                    style={{
                        color: '#404040',
                        fontWeight: 'normal',
                    }}/>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({imagePickerDialog: true})
                    }}
                    style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{width: 60, height: 60, borderRadius: 40, overflow: 'hidden', marginHorizontal: 10}}>
                        <Image style={{position: 'absolute', width: 70, height: 70,}}
                               source={require('../../resources/ic_display_small.png')}/>
                        <FastImage
                            style={{width: '100%', height: '100%'}}
                            source={{
                                uri: avatarUri || avatarUrl,
                            }}
                            resizeMode={FastImage.resizeMode.cover}
                        />
                    </View>
                    <Image source={require('../../resources/greenUnion/list_arrow_ic.png')}/>
                </TouchableOpacity>
            </View>
        )

        const _accountInfoView = (
            <View style={{borderRadius: 15, overflow: 'hidden'}}>
                <ItemBtnView
                    onPress={() => {
                        this.setState({updateNickNameDialog: true})
                    }}
                    text={strings('昵称')}
                    rightText={userName}
                    rightIcon={rightIcon}
                />
                <ItemBtnView
                    onPress={() => {
                    }}
                    text={strings('账号')}
                    rightText={I18n.locale === 'zh' ? Helper._encryptedPhoneNumber(accountValue) : accountValue}
                    rightIcon={rightIcon}
                />
                {I18n.locale === 'zh' ? (<View>
                    <ItemBtnView
                        onPress={() => {
                            // 微信
                        }}
                        text={strings('关联微信')}
                        rightText={wechatNickName}
                        rightIcon={rightIcon}
                    />
                    <ItemBtnView
                        onPress={() => {
                            // QQ
                        }}
                        text={strings('关联QQ')}
                        rightText={qqNickName}
                        rightIcon={rightIcon}
                    />
                </View>) : (<View>

                    <ItemBtnView
                        onPress={() => {
                            this._thirdEvent(Helper.channel_login_google,)
                        }}
                        text={strings('关联Google')}
                        rightText={googleNickName}
                        rightIcon={rightIcon}
                    />
                    <ItemBtnView
                        onPress={() => {
                            this._thirdEvent(Helper.channel_login_facebook)
                        }}
                        text={strings('关联Facebook')}
                        rightText={facebookNickName}
                        rightIcon={rightIcon}
                    />
                    <ItemBtnView
                        onPress={() => {
                            this._thirdEvent(Helper.channel_login_apple)
                        }}
                        text={strings('关联Apple')}
                        rightText={appleNickName}
                        rightIcon={rightIcon}
                    />
                </View>)}

            </View>
        )

        const _dialogView = (
            <View>
                {/* 修改昵称 */}
                <CommonMessageDialog
                    leftBtnTextColor={'#808080'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    rightBtnTextColor={'#ffffff'}
                    onRightBackgroundColor={'#18B34F'}
                    children={(<View
                        style={{
                            flexDirection: 'row',
                            height: 45,
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginVertical: 30
                        }}>
                        <TextInput
                            style={[
                                {
                                    height: 55,
                                    textAlign: 'left',
                                    paddingHorizontal: 15,
                                    fontSize: this.getSize(14),
                                    color: '#404040',
                                    backgroundColor: '#EBEEF0',
                                    width: '100%',
                                    borderRadius: 40,
                                }
                            ]}
                            maxLength={20}
                            onChangeText={(text) => {
                                this.setState({
                                    userNameInput: text
                                })
                            }}
                            defaultValue={this.state.userName}
                            // placeholderTextColor={}
                            // placeholder={}
                        />
                    </View>)}
                    onRequestClose={() => {
                        this.setState({updateNickNameDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({updateNickNameDialog: false})
                    }}
                    modalVisible={this.state.updateNickNameDialog}
                    title={strings('昵称')}
                    leftBtnText={strings('cancel')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({updateNickNameDialog: false})
                    }}
                    rightBtnText={strings('comfire')}
                    onRightBtnClick={() => {
                        //确定
                        this.setState({updateNickNameDialog: false})
                        this._updateUserName(this.state.userNameInput?.replace(/(^\s*)|(\s*$)/g, ""), this.state.token)
                    }}/>

                {/* 修改头像 */}
                <SelectDialog
                    onCancelClick={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    // title={this.state.imagePickType == 0 ? strings('update_avatar') : strings('update_bg_image')}
                    onRequestClose={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    modalVisible={this.state.imagePickerDialog}
                    selectArray={this.state.imagePickerDialogArray}
                    onItemClick={(index) => {
                        this.setState({imagePickerDialog: false}, () => setTimeout(() => {
                            if (index == 0) {//拍照
                                if (Platform.OS == 'android') {
                                    this._launchCamera()
                                } else {
                                    GXRNManager.checkPhotoPermissions(1).then((datas) => {
                                        this._launchCamera()
                                    }).catch((err) => {
                                        LogUtil.debugLog('err', err);
                                    });
                                }
                            } else {//相册
                                if (Platform.OS == 'android') {
                                    this._launchImageLibrary()
                                } else {
                                    GXRNManager.checkPhotoPermissions(2).then((datas) => {
                                        this._launchImageLibrary()
                                    }).catch((err) => {
                                        LogUtil.debugLog('err', err);
                                    });
                                }

                            }
                        }, 500));
                    }}
                />

                {/* 退出登录 */}
                <MessageDialog
                    onRequestClose={() => {
                        this.setState({signOutDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({signOutDialog: false})
                    }}
                    modalVisible={this.state.signOutDialog}
                    title={strings('sign_out')}
                    message={strings('signout_tips')}
                    leftBtnText={strings('cancel')}
                    rightBtnText={strings('comfire')}
                    leftBtnTextColor={'#808080'}
                    rightBtnTextColor={'#ffffff'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    onRightBackgroundColor={'#18B34F'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({signOutDialog: false})
                    }}
                    onRightBtnClick={() => {
                        //确定
                        this.setState({signOutDialog: false})
                        this._singOut()
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>
                {/*解绑弹窗 */}
                <MessageDialog
                    onRequestClose={() => {
                        this.setState({disassociateDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({disassociateDialog: false})
                    }}
                    modalVisible={this.state.disassociateDialog}
                    title={strings('remove_bind')}
                    message={strings('remove_bind_tips')}
                    leftBtnText={strings('cancel')}
                    rightBtnText={strings('unbind')}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({disassociateDialog: false})
                    }}
                    leftBtnTextColor={'#808080'}
                    rightBtnTextColor={'#F77979'}
                    onLeftBackgroundColor={'#F1F2F6'}
                    onRightBackgroundColor={'#18B34F'}
                    onRightBtnClick={() => {
                        //确定
                        this.setState({disassociateDialog: false})
                        this._unBindThird()
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>
                {/* 未解绑提示弹窗 */}
                <MessageDialog
                    onRequestClose={() => {
                        this.setState({deleteAccountUnbindDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({deleteAccountUnbindDialog: false})
                    }}
                    modalVisible={this.state.deleteAccountUnbindDialog}
                    title={strings('delete_account')}
                    message={strings('delete_account_unbind_device')}
                    rightBtnText={strings('ok')}
                    rightBtnTextColor={'#fff'}
                    onRightBackgroundColor={'#18B34F'}
                    onRightBtnClick={() => {
                        //好的
                        this.setState({deleteAccountUnbindDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

            </View>
        )

        const _unSignoutBtnView = (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //退出登录
                    this.setState({signOutDialog: true})
                }}
                style={{
                    marginBottom: 40,
                    width: this.mScreenWidth - 50,
                    backgroundColor: '#fff',
                }}
                textStyle={{color: '#E16262'}}
                btnText={strings('sign_out')}/>
        )

        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    backgroundColor: '#F1F2F6',
                    width: this.mScreenWidth,
                }}>
                <StatusBar translucent={true} backgroundColor="transparent"/>
                <View style={{flex: 1, justifyContent: 'space-between',}}>
                    <View>
                        {_accountView}
                        {_accountInfoView}
                        {_dialogView}
                    </View>
                    {_unSignoutBtnView}
                </View>
            </View>
        );
    }
}