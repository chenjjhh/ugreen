// 我的/使用帮助
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class NewUseHelp extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("使用帮助")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/greenUnion/nav_close_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    //   rightIcon={require("../../resources/greenUnion/nav_help_ic.png")}
                    //   rightIconClick={() => {
                    //     // 客服
                    //   }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            deviceListData: [
                {
                    deviceType: 1,
                    list: [
                        {
                            productTypeName: "GS600",
                            deviceImge: require("../../resources/greenUnion/list_power_img.png"),
                            productId: "840126776388972500",
                        },
                        {
                            productTypeName: "GS1200",
                            deviceImge: require("../../resources/greenUnion/list_power_img.png"),
                            productId: "1",
                        },
                    ],
                },
                {
                    deviceType: 0,
                    list: [
                        {
                            productTypeName: "HP202",
                            deviceImge: require("../../resources/greenUnion/list_headphone_img.png"),
                            productId: "1",
                        },
                        {
                            productTypeName: "WS200",
                            deviceImge: require("../../resources/greenUnion/list_headphone_img.png"),
                            productId: "1",
                        },
                        {
                            productTypeName: "WS201",
                            deviceImge: require("../../resources/greenUnion/list_headphone_img.png"),
                            productId: "1",
                        },
                    ],
                },
            ],
        };
    }

    render() {
        const {deviceListData} = this.state;
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F1F2F6",
                    alignItems: "center",
                    justifyContent: "space-between",
                    width: this.mScreenWidth,
                }}
            >
                <View>
                    <View
                        style={{
                            flexDirection: "row",
                            width: "100%",
                            paddingHorizontal: 12,
                            marginVertical: 12,
                        }}
                    >
                        <Text style={styles.textStyle}>{strings("查看说明")}</Text>
                    </View>
                    <ItemBtnView text={strings("应用使用帮助")} onPress={() => {
                        this.props.navigation.navigate(
                            "NewUseHelpDeviceDetails",
                            {type: 'applicationHelp'}
                        );
                    }}/>
                </View>
                <View
                    style={{
                        width: this.mScreenWidth,
                        maxHeight: 400,
                        minHeight: 250,
                        marginHorizontal: 20,
                        paddingTop: 20,
                        backgroundColor: "#fff",
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                    }}
                >
                    <View style={{paddingHorizontal: 20, marginBottom: 12}}>
                        <Text
                            style={[styles.textStyle, {fontSize: 16, color: "#404040"}]}
                        >
                            {strings("产品列表")}
                        </Text>
                        <Text style={styles.textStyle}>{strings("查看说明")}</Text>
                    </View>
                    <ScrollView style={{flex: 1, paddingHorizontal: 20}}>
                        {deviceListData?.map((item, index) => (
                            <View
                                key={index}
                                style={{marginTop: 0, width: this.mScreenWidth}}
                            >
                                <Text style={{fontSize: 16, color: "#404040"}}>
                                    {item.deviceType === 0 ? strings("耳机") : strings("电源")}
                                </Text>
                                <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                                    {item?.list?.map((items, indexs) => (
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.props.navigation.navigate(
                                                    "NewUseHelpDeviceDetails",
                                                    {title: items.productTypeName, productId: items.productId}
                                                );
                                            }}
                                            key={indexs}
                                            style={[
                                                styles.box,
                                                {
                                                    width: (this.mScreenWidth - 40) / 3,
                                                    height: (this.mScreenWidth - 40) / 3,
                                                },
                                            ]}
                                        >
                                            <Image source={items.deviceImge}/>
                                            <Text>{items.productTypeName}</Text>
                                        </TouchableOpacity>
                                    ))}
                                </View>
                            </View>
                        ))}
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {color: "#808080", fontSize: 14},
    box: {justifyContent: "center", alignItems: "center"},
});

class ItemBtnView extends BaseComponent {
    render() {
        const {text, onPress} = this.props;
        return (
            <TouchableOpacity
                onPress={() => {
                    onPress && onPress();
                }}
                style={{
                    width: this.mScreenWidth - 24,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    backgroundColor: "#fff",
                    height: 84,
                    borderRadius: 10,
                    paddingHorizontal: 21,
                }}
            >
                <Text style={{fontSize: this.getSize(15), color: "#2F2F2F"}}>
                    {text}
                </Text>
                <Image
                    style={[
                        {
                            resizeMode: "contain",
                            // paddingHorizontal: 25,
                        },
                    ]}
                    source={rightIcon}
                />
            </TouchableOpacity>
        );
    }
}
