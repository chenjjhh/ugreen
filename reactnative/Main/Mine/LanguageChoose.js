// 我的/语言选择
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    ScrollView,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import SwitchBtn from "../View/SwitchBtn";
import TitleBar from '../View/TitleBar'
export default class LanguageChoose extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('选择语言')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            currentId: props?.navigation?.state?.params?.id || 0,
            tabData : [
              { text1: strings('简体中文'),text2: strings('简体中文'), id: 0, key:'zh'},
              { text1: strings('English'),text2: strings('英语'),  id: 1, key:'en'},
              { text1: strings('Français'),text2: strings('法语'), id: 2, key:''},
              { text1: strings('Deutsch'), text2: strings('德语'), id: 3, key:''},
              { text1: strings('日本语'), text2: strings('日语'), id: 4, key:''},
              { text1: strings('Italiano'),text2: strings('意大利语'),  id: 5, key:''},
              { text1: strings('Español'),text2: strings('西班牙语'),  id: 6, key:''},
            ],
        };
      }
    componentDidUpdate( prevProps ){
        if(prevProps?.navigation?.state?.params?.id !== this.props?.navigation?.state?.params?.id){
            this.setState({
                currentId: this.props?.navigation?.state?.params?.id
            })
        }
    }

    onValueChange=(item)=>{
        const { onValueChange } = this.props.navigation?.state?.params;
        this.setState({
            currentId : item.id
        },onValueChange?.(item))
    }

    render() {
        const { tabData , currentId} = this.state
        return (
            <View style={{flex:1,backgroundColor:'#F7F7F7',alignItems:'center',width:this.mScreenWidth}}>
                <ScrollView style={{flex:1,width:this.mScreenWidth}}>
                    <View style={{justifyContent:'space-between',width:this.mScreenWidth,borderRadius:10,paddingHorizontal:21,}}>
                        {tabData?.map((item,index)=>(
                        <TouchableOpacity onPress={()=>{this.onValueChange(item)}} style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',paddingHorizontal:20,paddingVertical:20,height:84,borderRadius:10,backgroundColor:'#ffffff',marginBottom:10}}>
                            <View>
                                <Text style={{fontSize:this.getSize(16),color:'#2F2F2F'}}>{item?.text1}</Text>
                                <Text style={{fontSize:this.getSize(14),color:'#404040'}}>{item?.text2}</Text>
                            </View>
                            {item?.id === currentId ? <Image style={{width:21,height:21}}  source={require('../../resources/greenUnion/sel_ic.png')}/> : null}
                        </TouchableOpacity>))}
                    </View>
                </ScrollView>
            </View>
        );
    }
}