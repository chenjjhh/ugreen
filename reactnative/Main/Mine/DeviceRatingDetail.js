import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import TextInputView from "../View/TextInputView";
import CommonBtnView from "../View/CommonBtnView";
import ViewHelper from "../View/ViewHelper";
import ShadowCardView from "../View/ShadowCardView";
import StarRatingView from "../View/StarRatingView";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import RouterUtil from "../Utils/RouterUtil";

/*
*设备评分详情
 */

export default class DeviceRatingDetail extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    titleText={strings('device_rating')}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            deviceTypeName: '',
            itemData: this.props.navigation.state.params.data,
            suggestionValue: '',
            starScore: 5
        }
    }

    componentWillMount() {
        if (this.state.itemData) {
            var itemData = this.state.itemData
            this.setState({
                deviceTypeName: itemData.deviceNickname
            })
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                    alignItems: 'center'
                }}>
                {this._deviceView()}
                {this._inputSuggestionView()}
                {this._ratingView()}
                {ViewHelper.getFlexView()}
                {this._doneBtnView()}
            </View>
        );
    }

    _deviceView() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    marginTop: 30
                }}>
                <Image
                    source={require('../../resources/list_dev_img.png')}/>

                <CommonTextView
                    textSize={12}
                    style={{
                        color: '#000000',
                        marginTop: 10
                    }}
                    text={this.state.deviceTypeName}/>
            </View>
        )
    }

    _inputSuggestionView() {
        return (
            <TextInputView
                //maxLength={1000}
                multiline={true}
                style={{
                    width: this.mScreenWidth - 36,
                    height: this.mScreenHeight * 0.2,
                    marginTop: 29
                }}
                inputStyle={{
                    height: this.mScreenHeight * 0.2,
                    textAlignVertical: 'top',
                    paddingTop: 20,
                    paddingBottom: 20,
                    paddingLeft: 24,
                    paddingRight: 24
                }}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({suggestionValue: newText})
                }}
                value={this.state.suggestionValue}
                placeholder={strings('input_your_suggestion')}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}
            />
        )
    }

    _ratingView() {
        return (
            <ShadowCardView
                style={{
                    marginTop: 19,
                    paddingTop: 17,
                    paddingLeft: 20,
                    paddingRight: 20,
                    paddingBottom: 29
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000000',
                    }}
                    text={strings('rating_tips')}/>

                <StarRatingView
                    starScore={this.state.starScore}
                    onSelect={(index) => {
                        this.setState({starScore: index + 1})
                    }}/>
            </ShadowCardView>
        )
    }

    _doneBtnView() {
        return (
            <CommonBtnView
                clickAble={this.state.suggestionValue.length > 0}
                onPress={() => {
                    this._submit()
                }}
                style={{
                    marginBottom: 44,
                    width: this.mScreenWidth - 100
                }}
                btnText={strings('submit')}/>
        )
    }

    _submit() {
        if (this.state.suggestionValue.length > 1000) {
            this.onShowToast(strings('submit_failed_over_1000_strings'))
            return
        }

        var deviceId = this.state.itemData.deviceId
        NetUtil.deviceGrade(StorageHelper.getTempToken(), this.state.suggestionValue, this.state.starScore, deviceId, true)
            .then((res) => {
                this.onShowToast(strings('submit_success'))
                this.props.navigation.goBack();
            }).catch((error) => {
            ;
        })
    }
}