import BaseComponent from "../Base/BaseComponent";
import React from "react";
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View,
    BackHandler,
    Clipboard,
    TextInput,
} from "react-native";
import {strings, setLanguage} from "../Language/I18n";
import TitleBar from "../View/TitleBar";
import TextInputView from "../View/TextInputView";
import ViewHelper from "../View/ViewHelper";
import CommonBtnView from "../View/CommonBtnView";
import Helper from "../Utils/Helper";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import RouterUtil from "../Utils/RouterUtil";
import I18n from "react-native-i18n";
import NetConstants from "../Net/NetConstants";
import CommonTextView from "../View/CommonTextView";
/*
 *修改密码
 */

export default class NewChangePassword extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("change_pd")}
                    backgroundColor={"#FBFBFB"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            account: "", //邮箱
            verCode: "", //验证码
            passwordText: "", //新密码
            comfirePasswordText: "", //确认新密码
            verCodeText: strings("get_vail_code"),
            showPdError1: false,
            showPdError2: false,
            getCodeAble: true,
            submitBtnEnable: false,
            verCodeClickTime: 0,
        };
    }

    componentDidMount() {
        //获取登录账号(国内：手机号、国外：邮箱)
        StorageHelper.getLoginAccount().then((account) => {
            if (account) {
                this.setState({
                    account: account,
                });
            }
        });
    }

    componentWillUnmount() {
        this._stopTimer();
    }

    //密码重置
    _updatePd() {
        const {passwordText, comfirePasswordText, account, verCode} = this.state;
        if (!passwordText || !comfirePasswordText) {
            this.onShowToast(strings("input_cur_pd"));
            return;
        } else if (
            !Helper._ifValidPassword(passwordText) ||
            !Helper._ifValidPassword(comfirePasswordText)
        ) {
            this.onShowToast(strings("new_pd_input_password_error_tips"));
            return;
        } else if (passwordText !== comfirePasswordText) {
            this.onShowToast(strings("tiwce_pd_dif"));
            return;
        }
        NetUtil.pdReset(account, passwordText, verCode, true)
            .then((res) => {
                this.onShowToast(strings("update_success"));
                //清空数据，重新登录
                StorageHelper.clearAllData().then(() => {
                    RouterUtil.toLogin();
                });
            })
            .catch((error) => {
            });
    }

    //获取验证码
    _getVerCode() {
        const {account} = this.state;
        const lang = I18n.locale === "en" ? "en-US " : "zh_CN";
        if (!account) {
            this.onShowToast(strings("请检查账户是否有效"));
            return;
        }
        this.setState({
            verCodeClickTime: new Date().getTime(),
            getCodeAble: false,
        });

        NetUtil.getEmailVerCode(
            NetConstants.EMAIL_VER_CODE_PWD_RESET,
            account,
            true
        )
            .then((res) => {
                this._startTimer();
            })
            .catch((error) => {
                this.setState({
                    verCodeText: strings("get_vail_code"),
                    getCodeAble: true,
                });
            });
    }

    _startTimer() {
        this._stopTimer();
        let countdownDate = new Date(new Date().getTime() + 61 * 1000);
        this.intervalTimer = setInterval(() => {
            var dif = (countdownDate.getTime() - new Date().getTime()) / 1000;
            if (dif <= 0) {
                this.setState({
                    verCodeText: strings("get_vail_code"),
                    getCodeAble: true,
                });
                this._stopTimer();
            } else {
                this.setState({
                    verCodeText: parseInt(dif) + "s",
                    getCodeAble: false,
                });
            }
        }, 1000);
    }

    _stopTimer() {
        if (this.intervalTimer) {
            clearInterval(this.intervalTimer);
            this.intervalTimer = null;
        }
    }

    _getBtnEnable() {
        const {account, verCode, passwordText, comfirePasswordText} = this.state;
        var flag =
            (Helper._ifValidEmail(account) && I18n.locale === "en") ||
            (Helper._ifValidPhone(account) &&
                I18n.locale === "zh") &&
            Helper._ifValidPassword(passwordText) &&
            Helper._ifValidPassword(comfirePasswordText) &&
            verCode.length >= 5;

        this.setState({
            submitBtnEnable: flag,
        });
    }

    setPdErrorView(passwordText, comfirePasswordText) {
        this.setState({
            showPdError1: !Helper._ifValidPassword(passwordText),
            showPdError2: !Helper._ifValidPassword(comfirePasswordText),
        });
    }

    render() {
        const {
            account,
            verCodeText,
            showPdError1,
            getCodeAble,
            passwordText,
            showPdError2,
            verCode,
            comfirePasswordText,
            submitBtnEnable,
        } = this.state;
        const _accountView = (
            <View
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    borderRadius: 27,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                    overflow: "hidden",
                    marginTop: 54,
                    backgroundColor: "rgba(24, 179, 79, 0.1)",
                }}
            >
                <Text style={{fontSize: this.getSize(16), color: "#18B34F"}}>
                    {account}
                </Text>
            </View>
        );
        const _verifyCodeLogin = (
            <TextInputView
                key={0}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    marginTop: 20,
                    backgroundColor: "#EBEEF0",
                }}
                verifyCodeLogin={true}
                maxLength={6}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({verCode: newText}, () => {
                        this._getBtnEnable();
                    });
                }}
                value={verCode}
                placeholder={strings("please_enter_verification_code")}
                keyboardType={"numeric"}
                placeholderTextColor={"#C4C6CD"}
                onPress={() => this._getVerCode()}
                getCodeAble={getCodeAble}
                verCodeText={verCodeText}
                {...this.props}
            />
        );

        const _passwordView1 = (
            <TextInputView
                passwordIconStyle={{
                    width: 24,
                    height: 24,
                }}
                key={1}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    marginTop: 20,
                    backgroundColor: "#EBEEF0",
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({passwordText: newText}, () => {
                        this._getBtnEnable();
                    });
                    this.setPdErrorView(newText, comfirePasswordText);
                }}
                value={passwordText}
                placeholder={strings("请输入新的登录密码")}
                keyboardType={"default"}
                placeholderTextColor={"rgba(0, 0, 0, 0.26)"}
            />
        );

        const _passwordView2 = (
            <TextInputView
                passwordIconStyle={{
                    width: 24,
                    height: 24,
                }}
                key={2}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 60,
                    marginTop: 20,
                    backgroundColor: "#EBEEF0",
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(" ", "");
                    this.setState({comfirePasswordText: newText}, () => {
                        this._getBtnEnable();
                    });
                    this.setPdErrorView(passwordText, newText);
                }}
                value={comfirePasswordText}
                placeholder={strings("请再次输入新的登录密码")}
                keyboardType={"default"}
                placeholderTextColor={"rgba(0, 0, 0, 0.26)"}
            />
        );

        const _submitBtnView = (
            <CommonBtnView
                clickAble={submitBtnEnable}
                onPress={() => {
                    this._updatePd();
                }}
                style={{
                    marginBottom: 40,
                    marginTop: 40,
                    width: this.mScreenWidth - 60,
                }}
                btnText={strings("确认修改")}
            />
        );
        const _pdErrorTipsView = (
            <CommonTextView
                textSize={12}
                text={strings("input_password_error_tips")}
                style={{
                    color: "#DE7575",
                    width: this.viewCommonWith,
                    marginTop: 12,
                }}
            />
        );

        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#FBFBFB",
                    alignItems: "center",
                }}
            >
                {_accountView}
                {_verifyCodeLogin}
                {_passwordView1}
                {showPdError1 && passwordText ? _pdErrorTipsView : null}
                {_passwordView2}
                {showPdError2 && comfirePasswordText ? _pdErrorTipsView : null}
                {_submitBtnView}
            </View>
        );
    }
}
