import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {Image, NativeModules, Platform, ScrollView, TouchableOpacity, View} from 'react-native';
import {setLanguage, strings} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import ShadowCardView from '../View/ShadowCardView'
import ListItemView from '../View/ListItemView'
import StorageHelper from "../Utils/StorageHelper";
import I18n from "react-native-i18n";
import SelectDialog from '../View/SelectDialog'
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import CommonTextView from "../View/CommonTextView";
import TextInputDialog from "../View/TextInputDialog";
import NetUtil from "../Net/NetUtil";
import EventUtil from "../Event/EventUtil";
import NetConstants from "../Net/NetConstants";
import Helper from "../Utils/Helper";
import LoadingUtil from "../View/Loading/LoadingUtil";
import FastImage from "react-native-fast-image";
import LogUtil from "../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager

/*
*我的界面
 */

var options = {
    quality: 0.2,
    mediaType: 'photo'
}

export default class Mine extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            accountValue: props?.navigation?.state?.params?.account || '',
            messagePush: false,
            languageValue: '',
            languageDialog: false,
            tempUnitDialog: false,
            languageArray: [strings('chinese'), strings('english')],
            tempUnitArray: ['℃', '℉'],
            unitValue: '',
            userNameValue: props?.navigation?.state?.params?.userName || 'User',
            updateNickNameDialog: false,
            inputNameValue: props?.navigation?.state?.params?.userName|| 'User',
            token: '',

            imagePickerDialogArray: [strings('take_photo'), strings('photo')],
            imagePickerDialog: false,
            imagePickType: 0,//0头像  1背景图片
            avatarUrl: '',
            bgUrl: '',
            avatarUri: '',
            bgUri: '',
        }
    }

    componentWillMount() {
        if (this.props.navigation?.state?.params?.avatarUrl) {
            this.setState({avatarUrl: this.props.navigation.state.params.avatarUrl})
        }
        if (this.props.navigation?.state?.params?.bgUrl) {
            this.setState({bgUrl: this.props.navigation.state.params.bgUrl})
        }
        this._getUserInfo()
        this._setLanguageValue()
    }

    //获取本地存储的用户信息
    _getUserInfo() {
        StorageHelper.getUserInfo()
            .then((userInfo) => {
                    if (userInfo) {
                        this.setState({
                            accountValue: userInfo.account ? userInfo.account : userInfo.email,
                            bgUrl: userInfo.background
                        })
                        if (userInfo.avatar) {
                            this.setState({
                                avatarUrl: userInfo.avatar,
                            })
                        } else {
                            this._getLocalThirdAvatar()
                        }
                    } else {
                        this._getLocalThirdAvatar()
                    }
                }
            ).catch((error) => {
            this._getLocalThirdAvatar()
        })

        this.setState({token: StorageHelper.getTempToken()})

        //获取本地存储的温度单位
        StorageHelper.getTempUnit()
            .then((unit) => {
                if (unit != undefined) {
                    this.setState({unitValue: parseInt(unit) == 0 ? '℃' : '℉'})
                    return
                }
                this.setState({unitValue: '℉'})
            })
    }

    _getLocalThirdAvatar() {
        StorageHelper.getThirdAvatar()
            .then((res) => {
                if (res) {
                    this.setState({avatarUrl: res})
                }
            })
    }

    //设置语言
    _setLanguageValue() {
        if (I18n.locale == 'zh') {
            this.setState({languageValue: strings('chinese')})
        } else {
            this.setState({languageValue: strings('english')})
        }
    }

    //保存温度单位
    _saveTempUnit(unit) {
        StorageHelper.saveTempUnit(unit)
        EventUtil.sendEvent(EventUtil.SEND_TEMP_UNIT, unit)
    }

    render() {
        return (
            <View
                style={{
                    flex: 1
                }}>

                {this._topView()}

                <View
                    style={{
                        position: 'absolute',
                    }}>
                    <TitleBar
                        backgroundColor={'rgba(0,0,0,0)'}
                        leftIcon={require('../../resources/back_white_ic.png')}
                        leftIconClick={() => {
                            this.props.navigation.goBack();
                        }}
                        rightIcon={require('../../resources/background_ic.png')}
                        rightIconClick={() => {
                            this._showImagePicker(1)
                        }}
                    />
                </View>

                <ScrollView>
                    <View
                        style={{
                            alignItems: 'center',
                            width: this.mScreenWidth,
                            marginTop: 17
                        }}>
                        {this._view1()}
                        {this._view2()}
                        {this._view3()}
                    </View>
                </ScrollView>
                {this._dialogView()}
            </View>
        );
    }

    _dialogView() {
        return (
            <View>
                <SelectDialog
                    onCancelClick={() => {
                        this.setState({languageDialog: false})
                    }}
                    onRequestClose={() => {
                        this.setState({languageDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({languageDialog: false})
                    }}
                    modalVisible={this.state.languageDialog}
                    selectArray={this.state.languageArray}
                    onItemClick={(index) => {
                        var curLocale = I18n.locale
                        var selectLocale = (index == 0 ? 'zh' : 'en')
                        var languageText
                        if (curLocale != selectLocale) {
                            setLanguage(selectLocale)
                            StorageHelper.saveLanguage(selectLocale)
                            this.setState({
                                languageArray: [strings('chinese'), strings('english')]
                            })
                            languageText = (index == 0 ? strings('chinese') : strings('english'))

                            this.setState({
                                imagePickerDialogArray: [strings('take_photo'), strings('photo')]
                            })
                            this.setState({
                                languageValue: languageText
                            })
                        }
                        this.setState({
                            languageDialog: false,
                        })
                    }}
                />

                <SelectDialog
                    onCancelClick={() => {
                        this.setState({tempUnitDialog: false})
                    }}
                    onRequestClose={() => {
                        this.setState({tempUnitDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({tempUnitDialog: false})
                    }}
                    modalVisible={this.state.tempUnitDialog}
                    selectArray={this.state.tempUnitArray}
                    onItemClick={(index) => {
                        this.setState({
                            tempUnitDialog: false,
                            unitValue: this.state.tempUnitArray[index]
                        })
                        this._saveTempUnit(index)
                    }}
                />

                <TextInputDialog
                    inputStyle={{
                        color: 'rgba(0,0,0,0.26)'
                    }}
                    maxLength={16}
                    onRequestClose={() => {
                        this.setState({updateNickNameDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({updateNickNameDialog: false})
                    }}
                    modalVisible={this.state.updateNickNameDialog}
                    title={strings('update_user_nick_name')}
                    rightBtnText={strings('comfire1')}
                    rightBtnTextColor={'#38579D'}
                    onRightBtnClick={() => {
                        //确定
                        if (this.state.inputNameValue == '') {
                            return
                        }

                        this.setState({updateNickNameDialog: false})
                        this._updateUserName(this.state.inputNameValue.replace(/(^\s*)|(\s*$)/g, ""), this.state.token)
                    }}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        this.setState({updateNickNameDialog: false})
                    }}
                    value={this.state.inputNameValue}
                    onChangeText={(text) => {
                        this.setState({inputNameValue: text})
                    }}
                    placeholder={strings('input_user_nick_name')}
                    placeholderTextColor={'rgba(0,0,0,0.26)'}/>

                <SelectDialog
                    onCancelClick={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    // title={this.state.imagePickType == 0 ? strings('update_avatar') : strings('update_bg_image')}
                    onRequestClose={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({imagePickerDialog: false})
                    }}
                    modalVisible={this.state.imagePickerDialog}
                    selectArray={this.state.imagePickerDialogArray}
                    onItemClick={(index) => {
                        this.setState({imagePickerDialog: false}, () => setTimeout(() => {
                            if (index == 0) {//拍照
                                if (Platform.OS == 'android') {
                                    this._launchCamera(this.state.imagePickType)
                                } else {
                                    GXRNManager.checkPhotoPermissions(1).then((datas) => {
                                        this._launchCamera(this.state.imagePickType)
                                    }).catch((err) => {
                                        LogUtil.debugLog('err', err);
                                    });
                                }
                            } else {//相册
                                if (Platform.OS == 'android') {
                                    this._launchImageLibrary(this.state.imagePickType)
                                } else {
                                    GXRNManager.checkPhotoPermissions(2).then((datas) => {
                                        this._launchImageLibrary(this.state.imagePickType)
                                    }).catch((err) => {
                                        LogUtil.debugLog('err', err);
                                    });
                                }

                            }
                        }, 500));
                    }}
                />
            </View>
        )
    }

    //修改用户名称
    _updateUserName(updateName, token) {
        if (token == '' || updateName.toString().trim() == this.state.userNameValue.toString().trim()) {
            return
        }

        NetUtil.updateUserInfo(true, token, updateName)
            .then((res) => {
                this.onShowToast(strings('update_success'))
                this.setState({userNameValue: updateName})
                EventUtil.sendEventWithoutValue(EventUtil.UPDATE_USER_INFO)
            })
            .catch((error) => {
                this.onShowToast(strings('update_fail'))
            })
    }

    //修改头像
    _updateAvatar(avatarUrl, token, imageUri) {
        if (token == '' || avatarUrl == '') {
            LoadingUtil.dismissLoading()
            return
        }

        NetUtil.updateUserInfo(false, token, null, avatarUrl)
            .then((res) => {
                //更新头像成功
                this.onShowToast(strings('update_success'))
                this.setState({
                    avatarUrl: avatarUrl,
                    avatarUri: imageUri
                })
                EventUtil.sendEventWithoutValue(EventUtil.UPDATE_USER_INFO)
                LoadingUtil.dismissLoading()
            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }

    //修改背景
    _updateBg(bgUrl, token, imageUri) {
        if (token == '' || bgUrl == '') {
            LoadingUtil.dismissLoading()
            return
        }

        NetUtil.updateUserInfo(false, token, null, null, bgUrl)
            .then((res) => {
                //更新背景成功
                this.onShowToast(strings('update_success'))
                this.setState({
                    bgUrl: bgUrl,
                    bgUri: imageUri
                })
                EventUtil.sendEventWithoutValue(EventUtil.UPDATE_USER_INFO)
                LoadingUtil.dismissLoading()
            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }

    _dimissLoadingAndToast() {
        LoadingUtil.dismissLoading()
        this.onShowToast(strings('update_fail'))
    }

    _topView() {
        return (
            <View>
                <FastImage
                    style={{
                        width: this.mScreenWidth,
                        height: this._getHeightByWH(375, 80, this.mScreenWidth),
                    }}
                    source={this.state.bgUri ? {uri: this.state.bgUri} :
                        this.state.bgUrl ? {uri: this.state.bgUrl}
                            : require('../../resources/mine_default_bg.png')}
                    resizeMode={FastImage.resizeMode.cover}
                />

                <View
                    style={{
                        position: 'absolute',
                        width: this.mScreenWidth,
                        height: this._getHeightByWH(375, 280, this.mScreenWidth),
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <TouchableOpacity
                        activeOpacity={0.9}
                        onPress={() => {
                            this._showImagePicker(0)
                        }}>
                        <View
                            style={{
                                width: this.mScreenWidth * 0.3,
                                height: this.mScreenWidth * 0.3,
                                borderRadius: (this.mScreenWidth * 0.3) / 2,
                                overflow: 'hidden'
                            }}>
                            <Image
                                style={{
                                    width: this.mScreenWidth * 0.3,
                                    height: this.mScreenWidth * 0.3,
                                    resizeMode: 'cover',
                                    position: 'absolute'
                                }}
                                source={require('../../resources/ic_display_big.png')}/>

                            <FastImage
                                style={{
                                    width: this.mScreenWidth * 0.3,
                                    height: this.mScreenWidth * 0.3,
                                }}
                                source={this.state.avatarUri ? {uri: this.state.avatarUri} :
                                    this.state.avatarUrl ? {uri: this.state.avatarUrl}
                                        : require('../../resources/ic_display_big.png')}
                                resizeMode={FastImage.resizeMode.cover}
                            />
                        </View>

                        <Image
                            style={{
                                position: 'absolute',
                                top: (this.mScreenWidth * 0.3) * 0.09,
                                right: (this.mScreenWidth * 0.3) * 0.035
                            }}
                            source={require('../../resources/switch_avatar_ic.png')}/>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                inputNameValue: this.state.userNameValue,
                                updateNickNameDialog: true
                            })
                        }}
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginTop: 10,
                        }}>
                        <CommonTextView
                            textSize={18}
                            text={this.state.userNameValue}
                            style={{
                                color: '#FFFFFF',
                                textShadowOffset: {width: 0, hegith: 5},
                                textShadowRadius: 10,
                                textShadowColor: '#000'
                            }}
                        />

                        <Image
                            source={require('../../resources/edit_ic.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    _view1() {
        return (
            <ShadowCardView
                style={{
                    marginBottom: 15
                }}>
                <ListItemView
                    title={strings('account')}
                    value={this.state.accountValue}
                    onPress={() => {
                        //账号
                        this.props.navigation.navigate('Account', {
                            token: this.state.token,
                            accountValue: this.state.accountValue
                        });
                    }}/>
            </ShadowCardView>
        )
    }

    _view2() {
        return (
            <ShadowCardView
                style={{
                    marginBottom: 15
                }}>
                <ListItemView
                    title={strings('language')}
                    value={this.state.languageValue}
                    onPress={() => {
                        //语言
                        this.setState({languageDialog: true})
                    }}/>
                <ListItemView
                    title={strings('temp_unit')}
                    value={this.state.unitValue}
                    onPress={() => {
                        //温度单位
                        this.setState({tempUnitDialog: true})
                    }}/>
                {/*<ListItemView*/}
                {/*hideArrowIcon={true}*/}
                {/*title={strings('message_push')}*/}
                {/*isCheck={this.state.messagePush}*/}
                {/*onCheck={() => {*/}
                {/*//消息推送*/}
                {/*this.setState({messagePush: !this.state.messagePush})*/}
                {/*}}/>*/}
            </ShadowCardView>
        )
    }

    _view3() {
        return (
            <ShadowCardView
                style={{
                    marginBottom: 15
                }}>
                <ListItemView
                    title={strings('use_help')}
                    value={''}
                    onPress={() => {
                        //使用帮助
                        this.props.navigation.navigate('SelectDeviceList', {type: 0});
                        //this.props.navigation.navigate('UseHelp');
                    }}/>
                <ListItemView
                    title={strings('feedback')}
                    value={''}
                    onPress={() => {
                        //反馈
                        this.props.navigation.navigate('Feedback', {token: this.state.token});
                    }}/>
                {/*<ListItemView*/}
                    {/*title={strings('device_rating')}*/}
                    {/*value={''}*/}
                    {/*onPress={() => {*/}
                        {/*//设备评分*/}
                        {/*this.props.navigation.navigate('SelectDeviceList', {type: 2});*/}
                    {/*}}/>*/}
                <ListItemView
                    title={strings('about_us')}
                    value={''}
                    onPress={() => {
                        //关于我们
                        this.props.navigation.navigate('AboutUs');
                    }}/>
            </ShadowCardView>
        )
    }

    _showImagePicker(imagePickType) {
        this.setState({
            imagePickerDialog: true,
            imagePickType: imagePickType
        })
    }

    //打开相机
    _launchCamera(imagePickType) {
        launchCamera(options, (response) => {
            LogUtil.debugLog('Response = ', JSON.stringify(response));

            if (response.didCancel) {
                LogUtil.debugLog('User cancelled image picker');
            } else if (response.errorMessage) {
                LogUtil.debugLog('ImagePicker Error: ', response.errorMessage);
                Helper.openSettings(strings('camera_permission_tips'))
            } else if (response.errorCode) {
                LogUtil.debugLog('errorCode: ', response.errorCode);
                Helper.openSettings(strings('camera_permission_tips'))
            } else {
                const source = {uri: response.uri};

                var responseUri = response.assets[0].uri

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let file = {
                    uri: responseUri,
                    type: 'multipart/form-data',
                    name: imagePickType == 0 ? NetConstants.IMAGE_AVATAE_FILE_NAME : NetConstants.IMAGE_BG_FILE_NAME
                };   //这里的key(uri和type和name)不能改变,

                this._getImageUploadUrl(this.state.token, file, responseUri)
            }
        });
    }

    //打开相册
    _launchImageLibrary(imagePickType) {
        launchImageLibrary(options, (response) => {
            LogUtil.debugLog('Response = ', JSON.stringify(response));

            if (response.didCancel) {
                LogUtil.debugLog('User cancelled image picker');
            } else if (response.errorMessage) {
                LogUtil.debugLog('ImagePicker Error: ', response.errorMessage);
                Helper.openSettings(strings('camera_permission_tips'))
            } else if (response.errorCode) {
                LogUtil.debugLog('errorCode: ', response.errorCode);
                Helper.openSettings(strings('camera_permission_tips'))
            } else {
                const source = {uri: response.uri};

                var responseUri = response.assets[0].uri

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let file = {
                    uri: responseUri,
                    type: 'multipart/form-data',
                    name: imagePickType == 0 ? NetConstants.IMAGE_AVATAE_FILE_NAME : NetConstants.IMAGE_BG_FILE_NAME
                };   //这里的key(uri和type和name)不能改变,

                this._getImageUploadUrl(this.state.token, file, responseUri)
            }
        });
    }

    //获取图片的上传地址
    _getImageUploadUrl(token, file, imageUri) {
        LoadingUtil.showLoading()
        NetUtil.getFileUploadUrl(token, NetConstants.IMAGE_AVATAE_FILE_NAME, false)
            .then((res) => {
                var urlData = res.info
                if (urlData) {
                    var imageUploadUrl = urlData.preUrl
                    var imageUrl = urlData.fileUrl
                    this._uploadImage(imageUploadUrl, file, imageUrl, imageUri)
                } else {
                    this._dimissLoadingAndToast()
                }
            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }

    //上传图片
    _uploadImage(preUrl, file, imageUrl, imageUri) {
        NetUtil.uploadFile(preUrl, file, false)
            .then((res) => {
                if (this.state.imagePickType == 0) {
                    //头像上传成功
                    this._updateAvatar(imageUrl, this.state.token, imageUri)
                } else {
                    //背景上传成功
                    this._updateBg(imageUrl, this.state.token, imageUri)
                }
            })
            .catch((error) => {
                this._dimissLoadingAndToast()
            })
    }


}
