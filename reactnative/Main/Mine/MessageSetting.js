// 我的/消息设置
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import SwitchBtn from "../View/SwitchBtn";
import TitleBar from '../View/TitleBar'
export default class MessegeSetting extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('消息设置')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            tabData : [
              {text: strings('device_push'), id: 0, switch: true},
              {text: strings('device_share'), id: 1, switch: false},
              {text: strings('system_notification'), id: 2, switch: false},
            ]
        };
      }
    onValueChange=(value, type)=>{
        const { tabData } = this.state;
        const NewTabData = tabData.map((item,index)=>{
            if(item.id === type){
                item.switch = value
                return item
            }
            return item
        })
        this.setState({
            tabData : NewTabData
        })
    }

    render() {
        const { tabData } = this.state
        return (
            <View style={{flex:1,backgroundColor:'#F7F7F7',alignItems:'center',width:this.mScreenWidth}}>
                <View style={{justifyContent:'space-between',width:this.mScreenWidth-24,backgroundColor:'#fff',borderRadius:10,paddingHorizontal:21,}}>
                    {tabData?.map((item,index)=>(
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',height:60,borderBottomColor:'#EBEBEB',borderBottomWidth:0.5}}>
                        <Text style={{fontSize:this.getSize(16),color:'#404040'}}>{item?.text}</Text>
                        <SwitchBtn 
                            type={item?.id}
                            value={Number(item?.switch)} 
                            onValueChange={this.onValueChange}/>
                    </View>))}
                </View>
            </View>
        );
    }
}