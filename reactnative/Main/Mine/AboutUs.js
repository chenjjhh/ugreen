import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput, Linking
} from 'react-native';
import {strings, setLanguage} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import ShadowCardView from '../View/ShadowCardView'
import ListItemView from '../View/ListItemView'
import CommonTextView from '../View/CommonTextView'
import ViewHelper from "../View/ViewHelper";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import RouterUtil from "../Utils/RouterUtil";
import DeviceInfo from 'react-native-device-info';

/*
*关于我们
 */

export default class AboutUs extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('about_us')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            ruleUrl: '',//用户协议
            privacyUrl: '',//隐私协议
        }
    }

    componentWillMount() {
        this._getRule(false)
        this._getPrivacy(false)
    }

    _toRule() {
        if (this.state.ruleUrl == '') {
            this._getRule(true)
        } else {
            this._toWeb(this.state.ruleUrl, strings('user_rule'))
        }
    }

    _toPrivacy() {
        if (this.state.privacyUrl == '') {
            this._getPrivacy(true)
        } else {
            this._toWeb(this.state.privacyUrl, strings('privacy_rule'))
        }
    }

    _toWeb(toUrl, title) {
        if (toUrl) {
            this.props.navigation.navigate('UrlWebView', {url: toUrl, title: title});
        }
    }

    _getRule(toWeb) {
        NetUtil.getRule(StorageHelper.getTempToken())
            .then((res) => {
                var url = res.info.path
                this.setState({ruleUrl: url})
                if (toWeb)
                    this._toWeb(url, strings('user_rule'))
            })
            .catch((error) => {
            })
    }

    _getPrivacy(toWeb) {
        NetUtil.getPrivacy(StorageHelper.getTempToken())
            .then((res) => {
                var url = res.info.path
                this.setState({privacyUrl: url})
                if (toWeb)
                    this._toWeb(url, strings('privacy_rule'))
            })
            .catch((error) => {
            })
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    backgroundColor: '#FBFBFB'
                }}>
                {this._appIconView()}
                {this._appNameView()}
                {this._versionView()}
                {this._addressView()}
                {ViewHelper.getFixFlexView(1)}
                {this._phoneView()}
                {ViewHelper.getFixFlexView(1.5)}
                {this._ruleView()}
                {this._bottomlogoView()}
            </View>
        );
    }

    _appIconView() {
        return (
            <Image
                style={{
                    width: this.mScreenWidth * 0.31,
                    height: this.mScreenWidth * .31,
                    resizeMode: "contain",
                    marginTop: 35
                }}
                source={require('../../resources/about_us_app_icon.png')}/>
        )
    }

    _appNameView() {
        return (
            <CommonTextView
                textSize={16}
                text={DeviceInfo.getApplicationName() ? DeviceInfo.getApplicationName() : 'GENDOME'}
                style={{
                    color: '#000',
                    marginTop: 20
                }}/>
        )
    }

    //+ '(build-' + DeviceInfo.getBuildNumber() + ')'
    _versionView() {
        return (
            <CommonTextView
                textSize={12}
                text={'V' + DeviceInfo.getVersion()}
                style={{
                    color: '#B5BAC5',
                    marginTop: 10
                }}/>
        )
    }

    _addressView() {
        return (
            <CommonTextView
                onPress={() => {
                    this._toWeb('https://www.gendome.com', DeviceInfo.getApplicationName() ? DeviceInfo.getApplicationName() : 'GENDOME')
                }}
                textSize={14}
                text={'www.gendome.com'}
                style={{
                    color: '#38579D',
                    marginTop: 20
                }}/>
        )
    }

    _phoneView() {
        return (
            <ShadowCardView
                // onPress={() => {
                //     const url = `tel:${ 8008008900 }`;
                //     Linking.canOpenURL(url).then((supported) => {
                //         if (!supported) {
                //             this.onShowToast('not support')
                //             return
                //         }
                //         return Linking.openURL(url);
                //     }).catch((err) => Alert(err));
                // }}
                style={{
                    marginBottom: 10,
                    alignItems: 'center',
                    height: 56,
                    justifyContent: 'center',
                    marginTop: 10,
                    flexDirection: 'row',
                    width: this.mScreenWidth * 0.72
                }}>
                {/*<Image*/}
                {/*source={require('../../resources/phone_ic.png')}/>*/}

                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000000',
                        //marginLeft: 5,
                        textAlign: 'center'
                    }}
                    text={'marketing@gendome.com'}/>

                {/*<View*/}
                {/*style={{*/}
                {/*height: 56,*/}
                {/*position: 'absolute',*/}
                {/*width: this.mScreenWidth * 0.72,*/}
                {/*justifyContent: 'center',*/}
                {/*paddingLeft: this.mScreenWidth * 0.14*/}
                {/*}}>*/}
                {/*<Image*/}
                {/*source={require('../../resources/phone_ic.png')}/>*/}
                {/*</View>*/}
            </ShadowCardView>
        )
    }

    _ruleView() {
        return (
            <Text
                style={{
                    marginBottom: 47,
                    fontFamily: 'DIN Alternate Bold'
                }}>

                <Text
                    onPress={() => {
                        //用户协议
                        this._toRule()
                    }}
                    style={{
                        fontSize: this.getSize(14),
                        color: '#38579D',
                        fontFamily: 'DIN Alternate Bold'
                    }}>
                    {strings('user_rule')}
                </Text>

                <Text
                    style={{
                        fontSize: this.getSize(14),
                        color: '#969AA1',
                        fontFamily: 'DIN Alternate Bold'
                    }}>
                    {strings('and')}
                </Text>

                <Text
                    onPress={() => {
                        //隐私政策
                        this._toPrivacy()
                    }}
                    style={{
                        fontSize: this.getSize(14),
                        color: '#38579D',
                        fontFamily: 'DIN Alternate Bold'
                    }}>
                    {strings('privacy_rule')}
                </Text>
            </Text>
        )
    }

    _bottomlogoView() {
        return (
            <Image
                style={{
                    marginBottom: 35
                }}
                source={require('../../resources/about_logo_ic.png')}/>
        )
    }

}