// 我的/个人信息保护
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from '../View/TitleBar'
import CommonMessageDialog from "../View/CommonMessageDialog";

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
export default class PersonalInfoProtection extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('个人信息保护')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            withdrawDialog: false,

        };
      }

    render() {
        const {  } = this.state
        {/* 撤回同意弹窗 */}
        const _dialogView = (
            <CommonMessageDialog 
            leftBtnTextColor={'#808080'}
            onLeftBackgroundColor={'#F1F2F6'}
            rightBtnTextColor={'#ffffff'}
            onRightBackgroundColor={'#18B34F'}
            onRequestClose={() => {
                this.setState({withdrawDialog: false})
            }}
            overViewClick={() => {
                this.setState({withdrawDialog: false})
            }}
            title={strings('撤回同意')}
            children={(
                <View style={{alignItems:'center',marginVertical:25}}>
                    <Text style={{color:'#808080',fontSize:this.getSize(13),textAlign:'center',lineHeight:25}}>{strings('撤回同意提示')}</Text>
                </View>
            )}
            style={{paddingTop:30}}
            modalVisible={this.state.withdrawDialog}
            leftBtnText={strings('cancel')}
            onLeftBtnClick={() => {
                //取消
                this.setState({withdrawDialog: false})
            }}
            rightBtnText={strings('comfire')}
            onRightBtnClick={() => {
                //确定
                this.setState({withdrawDialog: false})
            }}/>
        )
        return (
            <View style={{flex:1,backgroundColor:'#F7F7F7',alignItems:'center',width:this.mScreenWidth}}>
                <View style={{width:this.mScreenWidth-24,backgroundColor:'#fff',borderRadius:10,paddingHorizontal:21,}}>
                    <ItemBtnView text={strings('个人信息保护政策')} onPress={()=>{}}/>
                    <ItemBtnView text={strings('个人信息保护政策摘要')} onPress={()=>{}}/>
                    <ItemBtnView text={strings('个人信息收集清单')} onPress={()=>{}}/>
                    <ItemBtnView text={strings('个人信息共享清单')} onPress={()=>{}}/>
                </View>
                <View style={{width:this.mScreenWidth-24,backgroundColor:'#fff',borderRadius:10,paddingHorizontal:21,marginVertical:12}}>
                    <ItemBtnView text={strings('系统权限管理')} onPress={()=>{this.props.navigation.navigate('SystemPermissionsManagement');}}/>
                </View>
                <View style={{width:this.mScreenWidth-24,backgroundColor:'#fff',borderRadius:10,paddingHorizontal:21,}}>
                    <ItemBtnView 
                        text={strings('撤回同意')} 
                        onPress={()=>{this.setState({withdrawDialog:true})}}/>
                </View>
                {_dialogView}
            </View>
        );
    }
}

class ItemBtnView extends BaseComponent{
    render(){
        const {text, onPress} = this.props;
        return(
            <TouchableOpacity 
                onPress={()=>{
                    onPress && onPress();
                }}  
                style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',height:60,borderBottomColor:'#EBEBEB',borderBottomWidth:0.5}}>
                <Text style={{fontSize:this.getSize(16),color:'#404040'}}>{text}</Text>
                <Image
                    style={[{
                        resizeMode: 'contain',
                        // paddingHorizontal: 25,
                    }, ]}
                    source={rightIcon}/>
            </TouchableOpacity>
        )
    }
}