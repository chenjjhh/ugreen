// 我的/使用帮助/设备帮助详情
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  RefreshControl,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class NewUseHelpDeviceDetails extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          titleText={navigation?.state?.params?.title || ""}
          backgroundColor={"#F1F2F6"}
          leftIcon={require("../../resources/back_black_ic.png")}
          leftIconClick={() => {
            navigation.goBack();
          }}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    const { productId, type } =
      props.navigation?.state?.params || {};
    this.state = {
      refreshing: false,
      token: StorageHelper.getTempToken() || "",
      type,
      productId,
      list: [
        {
            "path": "https://granwin-file.oss-cn-shanghai.aliyuncs.com/html/设备测试02-0007.html",
            "img": "https://oss.cstest.granwin.com/image/UGREEN_GS2200.png",
            "productId": 840126776388972500,
            "createTime": 1657182931077,
            "dataCenterId": 704942213334224900,
            "visitPath": "https://www.baidu.com/",
            "weight": 1,
            "title": "设备测试02-0007",
            "type": 2,
            "status": 1
          }
      ],
      searchContent: "", //搜索内容
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const { type } =
      this.props.navigation?.state?.params || {};
    const { token, productId, searchContent } = this.state;
    this.setState({ refreshing: true });
    if( type === "applicationHelp"){
        NetUtil.getApplicationHelpList(token).then((res) => {
            this.setState({
                list: res?.list || [],
                refreshing: false,
              });
          })
          .catch((error) => {
            this.setState({
                refreshing: false,
              });
          });
    }else{
        NetUtil.getHelpList(token, productId, searchContent, false)
          .then((res) => {
            this.setState({
                list: res?.list || [],
                refreshing: false,
              });
          })
          .catch((error) => {
            this.setState({
                refreshing: false,
              });
          });
    }
  };

  render() {
    const { list, refreshing, searchContent } = this.state;
    const { type } =
      this.props.navigation?.state?.params || {};
    const searchInputView = (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#fff",
          width: this.mScreenWidth - 50,
          height: 44,
          marginHorizontal: 20,
          marginTop: 20,
          marginBottom: 30,
          borderRadius: 20,
          overflow: "hidden",
        }}
      >
        <Image
          style={{ height: 18, width: 18, marginLeft: 69 }}
          source={require("../../resources/greenUnion/input_search_ic.png")}
        />
        <TextInput
          style={[
            {
              height: "100%",
              flex: 1,
              textAlign: "left",
              paddingRight: 15,
              fontSize: this.getSize(14),
              color: "#404040",
              backgroundColor: "#fff",
            },
          ]}
          onChangeText={(text) => {
            this.setState({
              searchContent: text,
            });
          }}
          onBlur={()=>{
            this.getData()
          }}
          defaultValue={searchContent}
          placeholderTextColor={"#C4C6CD"}
          placeholder={strings("请输入文字进行搜索")}
        />
      </View>
    );
    return (
      <View
        style={{
          height: this.mScreenHeight,
          backgroundColor: "#F1F2F6",
          alignItems: "center",
          width: this.mScreenWidth,
        }}
      >
        {type !== "applicationHelp" ? searchInputView : null}
        <ScrollView
          style={{ flex: 1, width: this.mScreenWidth }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.getData}
              tintColor="#18B34F" // 设置小图标颜色为蓝色ios
              colors={["#18B34F"]} // 设置小图标颜色为蓝色android
              title={strings("loading")} //iOS
              titleColor={"#999BA2"}
            />
          }
        >
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              paddingHorizontal: 20,
              paddingVertical: 15,
            }}
          >
            <Text style={styles.textStyle}>{strings("use_help")}</Text>
          </View>
          <View style={{ width: "100%", alignItems: "center" }}>
            {list?.map((item, index) => (
              <ItemBtnView
                key={index}
                text={`${item.title ? item.title + '?' : ''}`}
                onPress={() => {
                  this.props.navigation.navigate("NewUseHelpDetails", {
                    data: item
                  });
                }}
              />
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textStyle: { color: "#808080", fontSize: 14 },
  box: { justifyContent: "center", alignItems: "center" },
});

class ItemBtnView extends BaseComponent {
  render() {
    const { text, onPress } = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          onPress && onPress();
        }}
        style={{
          width: this.mScreenWidth - 24,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          backgroundColor: "#fff",
          height: 84,
          borderRadius: 10,
          paddingHorizontal: 21,
          marginBottom: 15,
        }}
      >
        <Text style={{ fontSize: this.getSize(15), color: "#2F2F2F" }}>
          {text}
        </Text>
        <Image
          style={[
            {
              resizeMode: "contain",
              // paddingHorizontal: 25,
            },
          ]}
          source={rightIcon}
        />
      </TouchableOpacity>
    );
  }
}
