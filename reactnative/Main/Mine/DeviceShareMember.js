/*
 *我的/设备管理（设备共享）
 */
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    FlatList,
    RefreshControl,
    ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from "../Net/NetUtil";
import CommonTextView from "../View/CommonTextView";
import I18n from "react-native-i18n";
import CommonMessageDialog from "../View/CommonMessageDialog";
import TitleBar from "../View/TitleBar";
import CommonBtnView from "../View/CommonBtnView";
import FastImage from "react-native-fast-image";
import Helper from "../Utils/Helper";
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";

class DeviceShareMember extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("设备共享")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            list: [],
            deleteDialog: false, //删除设备弹窗
            accountDialog: false, //分享邮箱弹窗
            shareMemberData: {}, //当前删除分享人数据
            account: "", //分享邮箱
            token: StorageHelper.getTempToken() || "",
            deviceId: props?.navigation?.state?.params?.deviceData?.deviceId || "",
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        const {token, deviceId} = this.state;
        this.setState({refreshing: true});
        NetUtil.deviceShareMember(token, deviceId, 2, true)
            .then((res) => {
                if (res?.list) {
                    this.setState({
                        list: res?.list,
                        refreshing: false,
                    });
                } else {
                    this.setState({
                        refreshing: false,
                    });
                }
            })
            .catch((error) => {
            });
    };

    // 删除接收设备
    delete = (shareMemberData, index) => {
        const {list, token} = this.state;
        NetUtil.removeDeviceMember(
            token,
            shareMemberData?.deviceId,
            shareMemberData?.userId,
            true
        )
            .then((res) => {
                const newList = list.filter(
                    (element) => shareMemberData?.deviceId !== element.deviceId
                );
                this.setState({
                    list: newList,
                });
                this.onShowToast(strings("已成功删除"));
                EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY)
            })
            .catch((error) => {

            });
    };

    //发送设备分享
    _sendShare() {
        const {deviceId} =
        this.props?.navigation?.state?.params?.deviceData || {};
        const {token, account} = this.state;
        if (!account || !token || !deviceId) {
            this.onShowToast(strings("请输入接收者账号"));
            return
        }
        NetUtil.shareDevice(token, deviceId, account, true)
            .then((res) => {
                this.onShowToast(strings("shareDeviceSuccessfully"));
            })
            .catch((error) => {
            });
    }

    renderItem = (item, index) => {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    flexDirection: "row",
                    justifyContent: "center",
                }}
            >
                <View
                    style={{
                        backgroundColor: "#fff",
                        height: 100,
                        width: this.mScreenWidth - 40,
                        flexDirection: "row",
                        alignItems: "center",
                        borderRadius: 10,
                        marginBottom: 15,
                    }}
                >
                    <View
                        style={{
                            height: 48,
                            width: 48,
                            borderRadius: 10,
                            overflow: "hidden",
                            marginHorizontal: 20,
                        }}
                    >
                        <Image
                            style={{
                                position: "absolute",
                                height: 48,
                                width: 48,
                            }}
                            source={require("../../resources/greenUnion/default_avatar.png")}
                        />
                        <FastImage
                            style={{height: 48, width: 48}}
                            source={{
                                uri: "",
                                priority: FastImage.priority.normal,
                            }}
                            resizeMode={FastImage.resizeMode.cover}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            marginRight: 10,
                        }}
                    >
                        <View>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <Text numberOfLines={1}>
                                    <CommonTextView
                                        textSize={15}
                                        text={item.name}
                                        style={{
                                            color: "#404040",
                                            fontWeight: "bold",
                                        }}
                                    />
                                </Text>
                            </View>
                            <Text numberOfLines={1}>
                                <CommonTextView
                                    textSize={13}
                                    text={item.phone}
                                    style={{
                                        color: "#808080",
                                        lineHeight: 25,
                                    }}
                                />
                            </Text>
                            <Text numberOfLines={1}>
                                <CommonTextView
                                    textSize={12}
                                    text={strings('共享于') + Helper._formatTimestamp(item.createTime)}
                                    style={{
                                        color: "#C4C6CD",
                                    }}
                                />
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={{paddingHorizontal: 10}}
                            onPress={() => {
                                //alert(JSON.stringify(item))
                                this.setState({deleteDialog: true, shareMemberData: item});
                            }}
                        >
                            <Image
                                source={require("../../resources/greenUnion/delete_btn.png")}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    };
    ListEmptyComponent = () => {
        return (
            <View
                style={{
                    height: this.mScreenHeight,
                    width: this.mScreenWidth,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#F1F2F6",
                }}
            >
                <Image
                    source={require("../../resources/greenUnion/share_member_none_ic.png")}
                />
            </View>
        );
    };

    render() {
        const {list, shareMemberData, account, refreshing, accountDialog} = this.state;
        const _dialogView = (
            <View>
                {/* 删除共享弹窗 */}
                <CommonMessageDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({deleteDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({deleteDialog: false});
                    }}
                    title={strings("删除共享")}
                    children={
                        <View style={{alignItems: "center", marginVertical: 25}}>
                            <Text
                                style={{
                                    color: "#808080",
                                    fontSize: this.getSize(13),
                                    textAlign: "center",
                                }}
                            >
                                {strings("删除共享提示")}
                            </Text>
                        </View>
                    }
                    style={{paddingTop: 30}}
                    modalVisible={this.state.deleteDialog}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({deleteDialog: false});
                    }}
                    rightBtnText={strings("comfire")}
                    onRightBtnClick={() => {
                        //确定
                        this.setState({deleteDialog: false}, () => {
                            this.delete(shareMemberData);
                        });
                    }}
                />
                {/* 分享邮箱 */}
                <CommonMessageDialog
                    leftBtnTextColor={"#808080"}
                    onLeftBackgroundColor={"#F1F2F6"}
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    title={strings("sharing_equipment")}
                    style={{paddingTop: 20}}
                    children={
                        <View
                            style={{
                                width: "100%",
                                alignItems: "center",
                                justifyContent: "center",
                                marginVertical: 20,
                            }}
                        >
                            <View style={{backgroundColor: "#fff"}}>
                                <Text
                                    style={{
                                        fontSize: this.getSize(14),
                                        color: "#808080",
                                        textAlign: "center",
                                        lineHeight: 25,
                                    }}
                                >
                                    {strings("分享设备提示")}
                                </Text>
                            </View>
                            <TextInput
                                style={[
                                    {
                                        height: 55,
                                        textAlign: "left",
                                        paddingHorizontal: 15,
                                        fontSize: this.getSize(14),
                                        color: "#404040",
                                        backgroundColor: "#EBEEF0",
                                        width: "100%",
                                        borderRadius: 40,
                                        marginTop: 20,
                                    },
                                ]}
                                maxLength={20}
                                onChangeText={(text) => {
                                    this.setState({
                                        account: text,
                                    });
                                }}
                                defaultValue={account}
                                // placeholderTextColor={}
                                placeholder={strings('请输入接收者账号')}
                            />
                        </View>
                    }
                    onRequestClose={() => {
                        this.setState({accountDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({accountDialog: false});
                    }}
                    modalVisible={accountDialog}
                    leftBtnText={strings("cancel")}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({accountDialog: false});
                    }}
                    rightBtnText={strings("comfire")}
                    onRightBtnClick={() => {
                        //确定
                        this._sendShare();
                        this.setState({accountDialog: false});
                    }}
                />
            </View>
        );

        const _shareBtnView = (
            <View style={{marginVertical: 20}}>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 分享设备
                        this.setState({
                            accountDialog: true,
                        });
                    }}
                    style={{
                        height: 54,
                        backgroundColor: "#18B34F",
                    }}
                    btnText={strings("sharing_equipment")}
                />
            </View>
        );
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    height: "100%",
                    alignItems: "center",
                }}
            >
                <View
                    style={{
                        width: this.mScreenWidth,
                        alignItems: "flex-start",
                        backgroundColor: "#F1F2F6",
                        paddingHorizontal: 35,
                        marginVertical: 10,
                    }}
                >
                    <Text style={{fontSize: this.getSize(14), color: "#404040"}}>
                        {strings("成员列表")}
                    </Text>
                </View>
                <ScrollView
                    style={{flex: 1}}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={this.getData}
                            tintColor="#18B34F" // 设置小图标颜色为蓝色ios
                            colors={["#18B34F"]} // 设置小图标颜色为蓝色android
                            title={strings("loading")} //iOS
                            titleColor={"#999BA2"}
                        />
                    }
                >
                    {list?.length > 0
                        ? list?.map((item, index) => this.renderItem(item, index))
                        : this.ListEmptyComponent()}
                </ScrollView>
                {_shareBtnView}
                {_dialogView}
            </View>
        );
    }
}

export default DeviceShareMember;
