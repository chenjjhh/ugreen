import BaseComponent from '../Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';
import {strings, setLanguage} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import TextInputView from "../View/TextInputView";
import ViewHelper from "../View/ViewHelper";
import CommonBtnView from "../View/CommonBtnView";
import Helper from "../Utils/Helper";
import NetUtil from "../Net/NetUtil";
import StorageHelper from "../Utils/StorageHelper";
import RouterUtil from "../Utils/RouterUtil";


/*
*修改密码
 */

export default class ChangePassword extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('change_pd')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            passwordText: '',//当前密码
            passwordText1: '',//新密码
            passwordText2: '',//确认新密码
        }
    }


    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                    alignItems: 'center'
                }}>
                {this._passwordView()}
                {this._passwordView1()}
                {this._passwordView2()}

                {ViewHelper.getFlexView()}

                {this._submitBtnView()}
            </View>
        );
    }


    _passwordView() {
        return (
            <TextInputView
                passwordIconStyle={{
                    width: 24,
                    height: 24,
                }}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 32,
                    marginTop: 10,
                    backgroundColor: '#EBEEF0'
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText: newText})
                }}
                value={this.state.passwordText}
                placeholder={strings('input_cur_pd')}
                keyboardType={'default'}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}/>
        )
    }

    _passwordView1() {
        return (
            <TextInputView
                passwordIconStyle={{
                    width: 24,
                    height: 24,
                }}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 32,
                    marginTop: 12,
                    backgroundColor: '#EBEEF0'
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText1: newText})
                }}
                value={this.state.passwordText1}
                placeholder={strings('input_new_pd')}
                keyboardType={'default'}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}/>
        )
    }

    _passwordView2() {
        return (
            <TextInputView
                passwordIconStyle={{
                    width: 24,
                    height: 24,
                }}
                style={{
                    height: 56,
                    width: this.mScreenWidth - 32,
                    marginTop: 12,
                    backgroundColor: '#EBEEF0'
                }}
                isPassword={true}
                maxLength={32}
                onChangeText={(text) => {
                    const newText = text.replace(' ', '');
                    this.setState({passwordText2: newText})
                }}
                value={this.state.passwordText2}
                placeholder={strings('comfire_new_pd')}
                keyboardType={'default'}
                placeholderTextColor={'rgba(0, 0, 0, 0.26)'}/>
        )
    }

    _submitBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    this._updatePd()
                }}
                style={{
                    marginBottom: 40,
                    width: this.mScreenWidth - 50
                }}
                btnText={strings('submit_update')}/>
        )
    }

    //密码重置
    _updatePd() {
        if (this.state.passwordText == '') {
            this.onShowToast(strings('input_cur_pd'))
            return
        } else if (!Helper._ifValidPassword(this.state.passwordText1)) {
            this.onShowToast(strings('new_pd_input_password_error_tips'))
            return
        } else if (this.state.passwordText1 != this.state.passwordText2) {
            this.onShowToast(strings('tiwce_pd_dif'))
            return
        } else if (this.state.passwordText == this.state.passwordText1) {
            this.onShowToast(strings('new_pd_same_as_cur_pd'))
            return
        }

        NetUtil.updatePassword(StorageHelper.getTempToken(), this.state.passwordText, this.state.passwordText1, true)
            .then((res) => {
                this.onShowToast(strings('update_success_login'))
                //清空数据，重新登录
                StorageHelper.clearAllData().then(() => {
                    RouterUtil.toLogin()
                })
            }).catch((error) => {

        })

    }
}