// 我的/账户安全
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform, TouchableOpacity, TextInput, Modal, PixelRatio
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from '../Language/I18n';
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from '../View/TitleBar'

const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
export default class AcountSave extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('账户安全')}
                    backgroundColor={'#F7F7F7'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {} = this.state
        return (
            <View style={{backgroundColor: '#F7F7F7', width: this.mScreenWidth, height: this.mScreenHeight}}>
                <View style={{
                    flex: 1,
                    backgroundColor: '#F7F7F7',
                    alignItems: 'center',
                    width: this.mScreenWidth,
                    marginTop: 10
                }}>
                    <View style={{
                        width: this.mScreenWidth - 24,
                        backgroundColor: '#fff',
                        borderRadius: 10,
                        paddingHorizontal: 21,
                    }}>
                        <TouchableOpacity
                            onPress={() => {//修改密码
                                this.props.navigation.navigate('NewChangePassword');
                            }}
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                height: 60,
                                borderBottomColor: '#EBEBEB',
                                borderBottomWidth: 0.5
                            }}>
                            <Text style={{fontSize: this.getSize(16), color: '#404040'}}>{strings('change_pd')}</Text>
                            <Image
                                style={[{
                                    resizeMode: 'contain',
                                    // paddingHorizontal: 25,
                                },]}
                                source={rightIcon}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {//注销账户
                                this.props.navigation.navigate('SignOutAccount');
                            }}
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                height: 60,
                                borderBottomColor: '#EBEBEB',
                                borderBottomWidth: 0.5
                            }}>
                            <Text style={{
                                fontSize: this.getSize(16),
                                color: '#E16262'
                            }}>{strings('sign_out_account')}</Text>
                            <Image
                                style={[{
                                    resizeMode: 'contain',
                                    // paddingHorizontal: 25,
                                },]}
                                source={rightIcon}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}