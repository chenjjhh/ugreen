import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, TouchableOpacity, TextInput, ScrollView, NativeModules
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import TabView from '../View/TabView'
import TitleBar from '../View/TitleBar'
import MyDevice from './MyDevice';
import ReceiveDevice from './ReceiveDevice';
/*
*我的/共享管理
 */
const SCREEN_WIDTH = Dimensions.get('window').width;
export default class ShareManage extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: <TitleBar
                titleText={strings('共享管理')}
                backgroundColor={'#F1F2F6'}
                leftIcon={require('../../resources/back_black_ic.png')}
                leftIconClick={() => {
                    navigation.goBack();
                }}
            />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            activeTab: 0,
        }
        this.tabData = [
            {text: strings('我的设备'), id: 0},//我的设备
            {text: strings('接收设备'), id: 1},//接收设备
        ]
    }

    _checkTab = (data) => {
        this.setState({activeTab: data.id,}, () => {
            this.handleTabPress(data.id)
        })
    }

    handleTabPress = tab => {
        this.setState({activeTab: tab});
        this.scrollView.scrollTo({x: tab * SCREEN_WIDTH, y: 0, animated: false});
    };

    render() {
        const {} = this.props;
        const {activeTab} = this.state;
        const _tabView = (
            <TabView
                containerStyle={{marginBottom: 15}}
                style={{paddingHorizontal: 50, justifyContent: 'space-between'}}
                data={this.tabData}
                currentId={activeTab}
                _checkTab={this._checkTab}
            />
        )
        StatusBar.setBarStyle('light-content');
        return (
            <View style={{width: this.mScreenWidth, marginTop: 10}}>
                <StatusBar translucent={true} backgroundColor="transparent"/>
                {_tabView}
                <ScrollView ref={ref => (this.scrollView = ref)} horizontal pagingEnabled
                            showsHorizontalScrollIndicator={false} scrollEnabled={false}>
                    <MyDevice {...this.props}/>
                    <ReceiveDevice {...this.props}/>
                </ScrollView>
            </View>
        );
    }
}

