// 我的/建议与反馈/售后申请
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Switch,
  Platform,
  TouchableOpacity,
  TextInput,
  Modal,
  PixelRatio,
  ScrollView,
  DeviceEventEmitter,
  NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import { strings, setLanguage } from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import LogUtil from "../Utils/LogUtil";
import StorageHelper from "../Utils/StorageHelper";

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class afterSalesApplication extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          titleText={strings("售后申请")}
          backgroundColor={"#FBFBFB"}
          leftIcon={require("../../resources/back_black_ic.png")}
          leftIconClick={() => {
            navigation.goBack();
          }}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      orderId: "", //订单号
      channel: "", //购买渠道
      ContactInformation: "", //联系方式
      contacts: "", //联系人
      contactAddress: "", //联系地址
      userContent: "", //反馈内容
      afterSalesTypeDialog: false,
      afterSalesTypeArray: [
        strings("退货"),
        strings("换货"),
        strings("维修"),
        strings("其他"),
      ],
      afterSalesTypeSelectIndex: 0,
      selectDevice: {}, //选择设备
      token: StorageHelper.getTempToken() || "",
      submittedSuccessfully: false, //提交成功弹窗
      userFiles: [], //记录上传文件
    };
  }

  componentWillMount() {
    this._eventListenerAdd();
  }

  componentWillUnmount() {
    this._eventListenerRemove();
  }

  _eventListenerAdd() {
    this.feedbackDeviceListener = DeviceEventEmitter.addListener(
      EventUtil.FEEDBACK_DEVICE,
      (value) => {
        //收到通知
        const { deviceId, deviceName, productId, deviceSn } =
          value?.deviceData || {};
        this.setState(() => {
          let obj = {};
          deviceId && (obj["deviceId"] = deviceId); // 设备id
          deviceName && (obj["deviceName"] = deviceName); // 设备名称
          productId && (obj["productId"] = productId); // 产品id
          deviceSn && (obj["deviceSn"] = deviceSn); // 设备sn
          return { selectDevice: obj };
          // return {...obj};
        });
      }
    );
  }

  _eventListenerRemove() {
    this.feedbackDeviceListener?.remove();
  }

  // 更新输入框值
  onChangeText = (type, value) => {
    this.setState({
      [type]: value,
    });
  };

  // 获取选择图片组件返回的已选图片
  getImageData = (AllSelectImage, currentImage) => {
    let newAllSelectImage = AllSelectImage?.map(({fileUrl, fileName, fileType})=>({fileUrl, fileName, fileType}))
    this.setState({
      userFiles: newAllSelectImage,
    });
  };

  // 提交售后申请
  _submit() {
    const {
      afterSalesTypeSelectIndex,
      selectDevice: { productId, deviceSn, deviceId },
      token,
      orderId,
      channel,
      contactInformation,
      contacts,
      contactAddress,
      userContent,
      userFiles,
    } = this.state;

    if (!orderId) {
      this.onShowToast(strings("订单号不能为空"));
      return;
    }

    if (!channel) {
      this.onShowToast(strings("购买渠道不能为空"));
      return;
    }

    if (!contactInformation) {
      this.onShowToast(strings("联系方式不能为空"));
      return;
    }

    if (
      !Helper._ifValidPhone(contactInformation) &&
      !Helper._ifValidEmail(contactInformation)
    ) {
      this.onShowToast(strings("请检查联系方式是否有效"));
      return;
    }

    if (!contacts) {
      this.onShowToast(strings("联系人不能为空"));
      return;
    }

    if (!contactAddress) {
      this.onShowToast(strings("联系地址不能为空"));
      return;
    }

    if (!userContent) {
      this.onShowToast(strings("反馈内容不能为空"));
      return;
    }

    if (userFiles?.length < 1) {
      this.onShowToast(strings("请上传图片"));
      return;
    }

    if (
      !Helper._ifValidPhone(contactInformation) &&
      !Helper._ifValidEmail(contactInformation)
    ) {
      this.onShowToast(strings("请检查联系方式是否有效"));
      return;
    }

    if (userFiles?.length < 1) {
      this.onShowToast(strings("请上传图片"));
      return;
    }

    // 售后类型 RETURN_GOODS-退货 EXCHANGE_GOODS-换货 REPAIR_GOODS-维修 OTHER-其他
    let type;
    if (afterSalesTypeSelectIndex === 0) {
      type = "RETURN_GOODS";
    } else if (afterSalesTypeSelect === 1) {
      type = "EXCHANGE_GOODS";
    } else if (afterSalesTypeSelect === 2) {
      type = "REPAIR_GOODS";
    } else if (afterSalesTypeSelect === 3) {
      type = "OTHER";
    }
    NetUtil.addAfterSales(
      token,
      type,
      productId,
      deviceId,
      deviceSn,
      orderId,
      channel,
      contactInformation,
      contacts,
      contactAddress,
      userContent,
      userFiles,
      Helper.getPlatformOsFlag(),
      true
    )
      .then((res) => {
        this.setState({
          submittedSuccessfully: true,
        });
      })
      .catch((error) => {
        this.onShowToast(strings("failed"));
      });
  }

  render() {
    const {
      ContactInformation,
      userContent,
      orderId,
      channel,
      contacts,
      contactAddress,
      submittedSuccessfully,
      afterSalesTypeDialog,
      afterSalesTypeArray,
      selectDevice: { deviceName },
      afterSalesTypeSelectIndex,
    } = this.state;
    const _dialogView = (
      <View>
        {/* 售后类型弹窗 */}
        <SelectDialog
          onCancelClick={() => {
            this.setState({ afterSalesTypeDialog: false });
          }}
          onRequestClose={() => {
            this.setState({ afterSalesTypeDialog: false });
          }}
          overViewClick={() => {
            this.setState({ afterSalesTypeDialog: false });
          }}
          modalVisible={afterSalesTypeDialog}
          selectArray={afterSalesTypeArray}
          onItemClick={(index) => {
            this.setState({
              afterSalesTypeDialog: false,
              afterSalesTypeSelectIndex: index,
            });
          }}
        />
        {/* 提交成功弹窗 */}
        <CommonMessageDialog
          rightBtnTextColor={"#ffffff"}
          onRightBackgroundColor={"#18B34F"}
          onRequestClose={() => {
            this.setState({ submittedSuccessfully: false });
          }}
          overViewClick={() => {
            this.setState({ submittedSuccessfully: false });
          }}
          modalVisible={submittedSuccessfully}
          title={strings("submit_success")}
          message={strings("提交成功提示")}
          rightBtnText={strings("comfire")}
          onRightBtnClick={() => {
            //确定
            this.setState({ submittedSuccessfully: false }, () => {
              this.props.navigation?.goBack();
            });
          }}
        />
      </View>
    );
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#F7F7F7",
          alignItems: "center",
          height: this.mScreenHeight,
          width: this.mScreenWidth,
        }}
      >
        <ScrollView style={{ flex: 1 }}>
          <View style={{ width: this.mScreenWidth, alignItems: "center" }}>
            <ItemBtnView
              style={{ borderRadius: 10, marginBottom: 15 }}
              onPress={() => {
                this.setState({
                  afterSalesTypeDialog: true,
                });
              }}
              text={strings("售后类型")}
              rightText={afterSalesTypeArray[afterSalesTypeSelectIndex]}
              rightIcon={rightIcon}
            />
            <ItemBtnView
              style={{ borderRadius: 10, marginBottom: 15 }}
              onPress={() => {
                //选择设备
                this.props.navigation.push("NewSelectDeviceList", {
                  type: 1,
                  pageKey: this.props.navigation.state.key,
                });
              }}
              text={strings("选择设备提示")}
              rightText={deviceName || strings("请选择设备")}
              rightIcon={rightIcon}
            />
            <TextInputComponent
              style={{ height: 50 }}
              containerStyle={{ marginBottom: 15 }}
              title={strings("订单号提示")}
              placeholder={strings("请输入订单号")}
              onChangeText={this.onChangeText}
              value={orderId}
              type={"orderId"}
            />
            <TextInputComponent
              style={{ height: 50 }}
              containerStyle={{ marginBottom: 15 }}
              title={strings("购买渠道提示")}
              placeholder={strings("请填写购买渠道")}
              onChangeText={this.onChangeText}
              value={channel}
              type={"channel"}
            />
            <View
              style={{
                flexDirection: "row",
                width: "100%",
                paddingHorizontal: 20,
                height: 30,
                marginTop: 10,
              }}
            >
              <Text style={{ color: "#404040", fontSize: 14 }}>
                {strings("寄回地址信息")}
              </Text>
            </View>
            <TextInputComponent
              style={{ height: 50 }}
              containerStyle={{ marginBottom: 15 }}
              title={strings("联系方式提示")}
              placeholder={strings("请填写联系方式")}
              onChangeText={this.onChangeText}
              value={ContactInformation}
              type={"ContactInformation"}
            />
            <TextInputComponent
              style={{ height: 50 }}
              containerStyle={{ marginBottom: 15 }}
              title={strings("联系人提示")}
              placeholder={strings("请填写联系人")}
              onChangeText={this.onChangeText}
              value={contacts}
              type={"contacts"}
            />
            <TextInputComponent
              style={{ height: 50 }}
              containerStyle={{ marginBottom: 15 }}
              title={strings("联系地址提示")}
              placeholder={strings("请填写联系地址")}
              onChangeText={this.onChangeText}
              value={contactAddress}
              type={"contactAddress"}
            />
            <TextInputComponent
              multiline={true}
              style={{ height: 127 }}
              containerStyle={{ marginBottom: 15 }}
              title={strings("反馈内容提示")}
              placeholder={strings("请输入反馈内容")}
              onChangeText={this.onChangeText}
              value={userContent}
              type={"userContent"}
            />

            <UpLoadView
              title={strings("上传")}
              tipsText={strings("上传提示")}
              getImageData={this.getImageData}
            />
          </View>
        </ScrollView>
        {/* 提交售后按钮 */}
        <CommonBtnView
          clickAble={true}
          onPress={() => {
            // 提交反馈
          }}
          style={{
            height: 54,
            backgroundColor: "#18B34F",
            marginBottom: 20,
            marginTop: 20,
          }}
          btnText={strings("提交售后")}
        />
        {_dialogView}
      </View>
    );
  }
}
