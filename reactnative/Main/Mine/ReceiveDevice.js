/*
 *我的/设备管理（接收设备）
 */
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Platform,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import { strings } from "../Language/I18n";
import NetUtil from "../Net/NetUtil";
import CommonTextView from "../View/CommonTextView";
import I18n from "react-native-i18n";
import CommonMessageDialog from "../View/CommonMessageDialog";
import EventUtil from "../Event/EventUtil";
import StorageHelper from "../Utils/StorageHelper";
import DeviceListUtil from "../Utils/DeviceListUtil";
import Helper from "../Utils/Helper";

class ReceiveDevice extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      refreshing: false,
      deleteDialog: false, //删除设备弹窗
      deleteDeviceId: "", //当前删除设备id
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    this.setState({ refreshing: true });
    if (StorageHelper.getTempToken()) {
      DeviceListUtil._getDeviceList(
        false,
        StorageHelper.getTempToken(),
        true,
        2
      )
        .then((res) => {
          if (res?.list?.length > 0) {
            this.setState({
              list: res?.list,
              refreshing: false,
            });
          } else {
            this.setState({
              refreshing: false,
            });
          }
        })
        .catch((error) => {});
    }
  };

  // 删除接收设备
  delete = () => {
    const { list, deleteDeviceId } = this.state;
    const newList = list.filter(
      (element) => deleteDeviceId !== element.deviceId
    );
    NetUtil.delDevice(deleteDeviceId, StorageHelper.getTempToken(), true)
      .then((res) => {
        this.setState(
          {
            list: newList,
            deleteDialog: false,
          },
          () => {
            this.onShowToast(strings("successed"));
            EventUtil.sendEventWithoutValue(EventUtil.REFRESH_DEVICE_LIST_KEY);
          }
        );
      })
      .catch((error) => {
        this.onShowToast(strings("failed"));
      });

    this.setState({
      list: newList,
    });
  };

  renderItem = (item, index) => {
    return (
      <View
        key={index}
        style={{
          width: this.mScreenWidth,
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            backgroundColor: "#fff",
            height: 100,
            width: this.mScreenWidth - 40,
            flexDirection: "row",
            alignItems: "center",
            borderRadius: 10,
            marginBottom: 15,
          }}
        >
          <Image
            style={{ marginHorizontal: 20, width: 80, height: 60 }}
            source={{ uri: item?.img }}
          />
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              marginRight: 10,
            }}
          >
            <View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text numberOfLines={1}>
                  <CommonTextView
                    textSize={15}
                    text={item.deviceName}
                    style={{
                      color: "#404040",
                      fontWeight: "bold",
                    }}
                  />
                </Text>
              </View>
              <Text numberOfLines={1}>
                <CommonTextView
                  textSize={12}
                  text={Helper._formatTimestamp(item.createTime)}
                  style={{
                    color: "#808080",
                  }}
                />
              </Text>
            </View>
            <TouchableOpacity
              style={{ paddingHorizontal: 10 }}
              onPress={() => {
                this.setState({
                  deleteDialog: true,
                  deleteDeviceId: item?.deviceId,
                });
              }}
            >
              <Image
                source={require("../../resources/greenUnion/delete_btn.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  ListEmptyComponent = () => {
    return (
      <View
        style={{
          height: this.mScreenHeight,
          width: this.mScreenWidth,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#F1F2F6",
        }}
      >
        <Image
          source={require("../../resources/greenUnion/share_none_ic.png")}
        />
      </View>
    );
  };

  render() {
    const { list, refreshing } = this.state;
    const _dialogView = (
      <View>
        <CommonMessageDialog
          leftBtnTextColor={"#808080"}
          onLeftBackgroundColor={"#F1F2F6"}
          rightBtnTextColor={"#ffffff"}
          onRightBackgroundColor={"#18B34F"}
          onRequestClose={() => {
            this.setState({ deleteDialog: false });
          }}
          overViewClick={() => {
            this.setState({ deleteDialog: false });
          }}
          title={strings("delete_device")}
          children={
            <View style={{ alignItems: "center", marginVertical: 25 }}>
              <Text
                style={{
                  color: "#808080",
                  fontSize: this.getSize(13),
                  textAlign: "center",
                }}
              >
                {strings("删除设备提示语")}
              </Text>
            </View>
          }
          style={{ paddingTop: 30 }}
          modalVisible={this.state.deleteDialog}
          leftBtnText={strings("cancel")}
          onLeftBtnClick={() => {
            //取消
            this.setState({ deleteDialog: false });
          }}
          rightBtnText={strings("comfire")}
          onRightBtnClick={() => {
            //确定
            this.setState({ deleteDialog: false }, () => {
              this.delete();
            });
          }}
        />
      </View>
    );
    return (
      <View style={{ width: this.mScreenWidth, flex: 1, alignItems: "center" }}>
        <ScrollView
          style={{ flex: 1 }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.getData}
              tintColor="#18B34F" // 设置小图标颜色为蓝色ios
              colors={["#18B34F"]} // 设置小图标颜色为蓝色android
              title={strings("loading")} //iOS
              titleColor={"#999BA2"}
            />
          }
        >
          {list?.length > 0
            ? list?.map((item, index) => this.renderItem(item, index))
            : this.ListEmptyComponent()}
        </ScrollView>
        {_dialogView}
      </View>
    );
  }
}

export default ReceiveDevice;
