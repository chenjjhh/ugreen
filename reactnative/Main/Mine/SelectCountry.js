/*
*登录/选择国家
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, 
    TouchableOpacity, 
    TextInput,
    ImageBackground,
    NativeModules,
    ScrollView,
    DeviceEventEmitter
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from '../Net/NetUtil'
import CommonTextView from '../View/CommonTextView'
import I18n from 'react-native-i18n';
import ItemView from '../View/ItemView'
import FastImage from "react-native-fast-image";
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";
import ChooseDialog from '../View/ChooseDialog'
import countryCode from '../CountryData/CountryData';
const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

// 图标
const selectIcon = (require('../../resources/greenUnion/sel_ic.png'))

class SelectCountry extends BaseComponent {
  static navigationOptions = ({navigation}) => { 
    return {
        header: null
    };
}
  constructor(props) {
    super(props);
    this.state = {
      selectData:{
        country: "埃及",
        countryEn: "Egypt",
        id: 1,
        region: "非洲",
        regionEn: "Africa",
        regionId: "5",
        coordinate: [31.14, 30.01],
        code: "eg",
      },//当前选中国家
    };
    
  }

  // 选择国家
  select=(item, index)=>{
    const { selectData } = this.state;
    const { nextStep } = this.props;
    this.setState({
      selectData: item
    },()=>{
      nextStep?.(selectData)
      this.peops.navigation?.goBack()
    })
    

  }

  render() {
    StatusBar.setBarStyle('light-content');
    const { selectData } = this.state;
    const topView = (
      <View style={{width:'100%',paddingHorizontal:20}}>
            <Text style={{color:'#2F2F2F',lineHeight:60,fontSize:22}}>{strings('选择区域')}</Text>
            <Text style={{color:'#999BA2',lineHeight:24,fontSize:12}}>{strings('选择区域提示')}</Text>
      </View>
    )
    return (
        <View style={{width:this.mScreenWidth,height:this.mScreenHeight,alignItems:'center',}}>
            <StatusBar translucent={true} backgroundColor="transparent"/>
              {topView}
              <ScrollView style={{flex:1,paddingHorizontal:20,marginTop:30}}>
                 <View style={{width:this.mScreenWidth-40,}}>
                    {countryCode?.map((item,index)=>(
                      <TouchableOpacity onPress={()=>this.select(item,index)} style={[{flexDirection:'row',backgroundColor:'#fff',height:60,paddingHorizontal:15,overflow:'hidden'},index === 0 ? {borderTopLeftRadius:15,borderTopRightRadius:15} : null,index === countryCode.length-1 ? {borderBottomLeftRadius:15,borderBottomRightRadius:15} : null]}>
                        <View style={{flexDirection:'row',alignItems:'center',width:this.mScreenWidth-70,justifyContent:'space-between',borderBottomColor:'#EBEBEB',borderBottomWidth:0.5}}>
                            <Text style={{color:'#404040',fontSize:16}}>{I18n.locale === 'zh' ? item.country : item.countryEn}</Text>
                            {selectData?.id === item.id ? <Image style={{width:22,height:22}} source={selectIcon}/> : null}
                        </View>
                      </TouchableOpacity>
                    ))}
                 </View> 
              </ScrollView>
        </View>
    );
  }
}

export default SelectCountry;