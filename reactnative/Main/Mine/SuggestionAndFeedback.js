// 我的/建议与反馈
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class SuggestionAndFeedback extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("建议与反馈")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    rightText={strings("记录")}
                    rightTextStyle={{color: "#18B34F"}}
                    rightTextClick={() => {
                        // 记录
                        navigation.push('RecordListView')
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            withdrawDialog: false,
        };
    }

    render() {
        const {} = this.state;
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F1F2F6",
                    alignItems: "center",
                    width: this.mScreenWidth,
                }}
            >
                <View
                    style={{
                        width: this.mScreenWidth - 24,
                        backgroundColor: "#fff",
                        borderRadius: 10,
                        paddingHorizontal: 21,
                        marginVertical: 10,
                    }}
                >
                    <ItemBtnView
                        text={strings("使用帮助")}
                        onPress={() => {
                            this.props.navigation.navigate("afterSalesApplication");
                        }}
                    />
                    <ItemBtnView
                        text={strings("问题反馈")}
                        onPress={() => {
                            this.props.navigation.navigate("NewFeedback");
                        }}
                    />
                    <ItemBtnView
                        text={strings("申请售后")}
                        onPress={() => {
                            this.props.navigation.navigate("afterSalesApplication");
                        }}
                    />
                    <ItemBtnView text={strings("技术支持")} onPress={() => {
                        this.props.navigation.navigate("TechnicalSupport");
                    }}/>
                </View>
            </View>
        );
    }
}

class ItemBtnView extends BaseComponent {
    render() {
        const {text, onPress} = this.props;
        return (
            <TouchableOpacity
                onPress={() => {
                    onPress && onPress();
                }}
                style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    height: 60,
                    borderBottomColor: "#EBEBEB",
                    borderBottomWidth: 0.5,
                }}
            >
                <Text style={{fontSize: this.getSize(16), color: "#404040"}}>
                    {text}
                </Text>
                <Image
                    style={[
                        {
                            resizeMode: "contain",
                            // paddingHorizontal: 25,
                        },
                    ]}
                    source={rightIcon}
                />
            </TouchableOpacity>
        );
    }
}
