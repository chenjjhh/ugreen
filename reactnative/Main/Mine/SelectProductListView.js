/*
 *我的/问题反馈/选择设备列表/选择产品类型列表
 */
import React from "react";
import {Image, NativeModules, Platform, ScrollView, Text, TouchableOpacity, View,} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import TitleBar from "../View/TitleBar";
import BleModule from "../Bluetooth/BleModule";
import BleProtocol from "../Bluetooth/BleProtocol";

const inputScan = require("../../resources/greenUnion/input_scan_ic.png");
const titleTipsIcon = require("../../resources/greenUnion/tips_ic.png");

var GXRNManager = NativeModules.GXRNManager
global.BluetoothManager = new BleModule();
global.BluetoothProtocol = new BleProtocol();

export default class SelectProductListView extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("选择设备")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            typeId: 0,
            list: [],
            data: [
                {
                    typeName: "储能电源",
                    id: 0,
                    data1: [
                        {
                            typeName: "电池",
                            data2: [
                                {
                                    deviceName: "GS600",
                                    deviceImge: require("../../resources/greenUnion/list_power_img.png"),
                                    productId: "840126776388972500",
                                },
                                {
                                    deviceName: "GS1200",
                                    deviceImge: require("../../resources/greenUnion/list_power_img.png"),
                                    productId: "1",
                                },
                            ],
                        },
                    ],
                },
                {
                    typeName: "智能音频",
                    id: 1,
                    data1: [
                        {
                            typeName: "入耳式耳机",
                            data2: [
                                {
                                    deviceName: "T6",
                                    deviceImge: require("../../resources/earbuds/t6/list_headphone_img.png"),
                                    productId: "T6",
                                },
                            ],
                        },
                       /* {
                            typeName: "头戴式耳机",
                            data2: [
                                {
                                    deviceName: "WS200",
                                    deviceImge: require("../../resources/greenUnion/list_headphone_img.png"),
                                    productId: "1",
                                },
                            ],
                        },
                        {
                            typeName: "3.5mm有线耳机",
                            data2: [
                                {
                                    deviceName: "WS201",
                                    deviceImge: require("../../resources/greenUnion/list_headphone_img.png"),
                                    productId: "1",
                                },
                            ],
                        },*/
                    ],
                },
            ],
        };
    }

    componentWillMount() {
        this.selectType({id: 0});
    }

    selectType = (item) => {
        this.setState(
            {
                typeId: item.id,
            },
            () => {
                this.getListData(item.id);
            }
        );
    };

    // 渲染右边列表数据
    getListData = (id) => {
        const {data} = this.state;
        data?.filter((item, index) => {
            if (id === index) {
                this.setState({list: item.data1});
            }
        });
    };

    callBackSelect = (data) => {
        /*const {productId, deviceName} = data || {};
        const {selectDevice} = this.props.navigation?.state?.params || {};
        selectDevice?.({productId, deviceName});
        this.props.navigation.goBack()*/

        if(data.productId=='840126776388972500'){
            //gs600
        }else if(data.productId=='T6'){
            //t6
            if (Platform.OS === "android") {
                NativeModules.RNModule.openScanEarbuds()
            }
        }

        // this.props.navigation.dispatch(
        //   StackActions.popToTop({
        //     key: this.props.navigation?.state?.params?.pageKey,
        //   }) // 指定要返回的页面的key
        // );
    };

    renderItem = (item, index) => {
        return (
            <View style={{paddingHorizontal: 20}}>
                <Text style={{color: "#404040", fontSize: 15, lineHeight: 50}}>
                    {item.typeName}
                </Text>
                {item?.data2?.map((items, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => {
                            this.callBackSelect(items);
                        }}
                        style={{
                            backgroundColor: "#FAFAFA",
                            borderRadius: 10,
                            flexDirection: "row",
                            alignItems: "center",
                            marginBottom: 14,
                        }}
                    >
                        <Image
                            style={{marginHorizontal: 20}}
                            source={items?.deviceImge}
                        />
                        <View style={{flex: 1}}>
                            <Text
                                style={{color: "#404040", fontSize: 15}}
                                numberOfLines={1}
                            >
                                {items?.deviceName}
                            </Text>
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        );
    };

    render() {
        const {data, typeId, list} = this.state;
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: "row",
                    width: this.mScreenWidth,
                    height: this.mScreenHeight,
                    paddingLeft: 15,
                    alignItems: "center",
                    backgroundColor: "#F1F2F6",
                }}
            >
                <View style={{height: "100%", paddingTop: 20, paddingRight: 20}}>
                    {data?.map((item, index) => (
                        <TouchableOpacity
                            onPress={() => this.selectType(item)}
                            key={index + "type"}
                            style={{
                                flexDirection: "row",
                                alignItems: "center",
                                marginBottom: 20,
                            }}
                        >
                            <View
                                style={{
                                    width: 2.5,
                                    height: 15,
                                    marginRight: 5,
                                    backgroundColor: typeId === item.id ? "#18B34F" : "#F1F2F6",
                                    borderRadius: 2,
                                }}
                            ></View>
                            <Text
                                style={{
                                    fontSize: 14,
                                    color: typeId === item.id ? "#18B34F" : "#404040",
                                }}
                            >
                                {item.typeName}
                            </Text>
                        </TouchableOpacity>
                    ))}
                </View>
                <View
                    style={{
                        flex: 1,
                        backgroundColor: "#ffffff",
                        borderTopLeftRadius: 15,
                        borderTopRightRadius: 15,
                    }}
                >
                    <ScrollView style={{flex: 1}}>
                        {list?.map((item, index) => this.renderItem(item, index))}
                    </ScrollView>
                </View>
            </View>
        );
    }
}
