// 我的/建议与反馈/问题反馈
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    ScrollView,
    DeviceEventEmitter,
    NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings, setLanguage} from "../Language/I18n";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import ItemBtnView from "../View/ItemBtnView";
import TextInputComponent from "../View/TextInputComponent";
import CommonBtnView from "../View/CommonBtnView";
import UpLoadView from "../View/UpLoadView";
import SelectDialog from "../View/SelectDialog";
import EventUtil from "../Event/EventUtil";
import NetUtil from "../Net/NetUtil";
import Helper from "../Utils/Helper";
import StorageHelper from "../Utils/StorageHelper";

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class NewFeedback extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("问题反馈")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            contactInformation: "", //联系方式
            userContent: "", //反馈内容
            questionTypeDialog: false,
            questionTypeArray: [
                strings("账号问题"),
                strings("固件升级问题"),
                strings("体验问题"),
                strings("性能问题"),
                strings("友好建议"),
            ],
            questionTypeSelectIndex: 1,
            selectDevice: {}, //选择设备
            token: StorageHelper.getTempToken() || "",
            submittedSuccessfully: false, //提交成功弹窗
            userFiles: [], //选择图片
        };
    }

    componentWillMount() {
        this._eventListenerAdd();
        // console.log('this.props.navigation.state.key',this.props.navigation.state.key);
    }

    componentWillUnmount() {
        this._eventListenerRemove();
    }

    _eventListenerAdd() {
        this.feedbackDeviceListener = DeviceEventEmitter.addListener(
            EventUtil.FEEDBACK_DEVICE,
            (value) => {
                //收到通知
                const {deviceId, deviceName, productId, deviceSn} =
                value?.deviceData || {};
                this.setState(() => {
                    let obj = {};
                    deviceId && (obj["deviceId"] = deviceId); // 设备id
                    deviceName && (obj["deviceName"] = deviceName); // 设备名称
                    productId && (obj["productId"] = productId); // 产品id
                    deviceSn && (obj["deviceSn"] = deviceSn); // 设备sn
                    return {selectDevice: obj};
                    // return {...obj};
                });
            }
        );
    }

    _eventListenerRemove() {
        this.feedbackDeviceListener?.remove();
    }

    // 更新输入框值
    onChangeText = (type, value) => {
        this.setState({
            [type]: value,
        });
    };

    // 获取选择图片组件返回的已选图片
    getImageData = (AllSelectImage, currentImage) => {
        let newAllSelectImage = AllSelectImage?.map(({fileUrl, fileName, fileType}) => ({fileUrl, fileName, fileType}))
        this.setState({
            userFiles: newAllSelectImage,
        });
    };

    // 提交反馈
    _submit() {
        const {
            userContent,
            contactInformation,
            questionTypeSelectIndex,
            selectDevice: {productId, deviceSn, deviceId},
            token,
            userFiles,
        } = this.state;

        if (!userContent) {
            this.onShowToast(strings("请至少填写必要内容后再提交"));
            return;
        }

        if (!contactInformation) {
            this.onShowToast(strings("联系方式不能为空"));
            return;
        }

        if (
            !Helper._ifValidPhone(contactInformation) &&
            !Helper._ifValidEmail(contactInformation)
        ) {
            this.onShowToast(strings("请检查联系方式是否有效"));
            return;
        }

        if (userFiles?.length < 1) {
            this.onShowToast(strings("请上传图片"));
            return;
        }

        let type = String(questionTypeSelectIndex + 1);
        // deviceId: 设备id
        // type: 反馈类型 1-账号问题 2-固件升级问题 3-体验问题 4-性能问题 5-友好建议
        // source: 来源 【0】ios 【1】Android
        // userContent: 用户提交的内容
        // contactInformation: 联系方式
        // userFiles: 文件列表，约定为json数组格式[{“fileName”:”xxx”,”fileUrl”:”xxx”,”fileType”:”xxx”}] 文件类型 picture-图片 video-视频
        // productId: 产品id
        // deviceSn: 设备sn
        NetUtil.addFeedback(
            token,
            type,
            Helper.getPlatformOsFlag(),
            userContent,
            contactInformation,
            userFiles,
            productId,
            deviceSn,
            deviceId,
            true
        )
            .then((res) => {
                this.setState({
                    submittedSuccessfully: true,
                });
            })
            .catch((error) => {
                this.onShowToast(strings("failed"));
            });
    }

    render() {
        const {
            contactInformation,
            userContent,
            questionTypeDialog,
            questionTypeArray,
            selectDevice: {deviceName},
            questionTypeSelectIndex,
            submittedSuccessfully,
        } = this.state;
        const _dialogView = (
            <View>
                {/* 问题类型弹窗 */}
                <SelectDialog
                    onCancelClick={() => {
                        this.setState({questionTypeDialog: false});
                    }}
                    onRequestClose={() => {
                        this.setState({questionTypeDialog: false});
                    }}
                    overViewClick={() => {
                        this.setState({questionTypeDialog: false});
                    }}
                    modalVisible={questionTypeDialog}
                    selectArray={questionTypeArray}
                    onItemClick={(index) => {
                        this.setState({
                            questionTypeDialog: false,
                            questionTypeSelectIndex: index + 1,
                        });
                    }}
                />
                {/* 提交成功弹窗 */}
                <CommonMessageDialog
                    rightBtnTextColor={"#ffffff"}
                    onRightBackgroundColor={"#18B34F"}
                    onRequestClose={() => {
                        this.setState({submittedSuccessfully: false});
                    }}
                    overViewClick={() => {
                        this.setState({submittedSuccessfully: false});
                    }}
                    modalVisible={submittedSuccessfully}
                    title={strings("submit_success")}
                    message={strings("提交成功提示")}
                    rightBtnText={strings("comfire")}
                    onRightBtnClick={() => {
                        //确定
                        this.setState({submittedSuccessfully: false}, () => {
                            this.props.navigation?.goBack();
                        });
                    }}
                />
            </View>
        );
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F1F2F6",
                    alignItems: "center",
                    height: this.mScreenHeight,
                    width: this.mScreenWidth,
                }}
            >
                <ScrollView style={{flex: 1}}>
                    <View style={{width: this.mScreenWidth, alignItems: "center"}}>
                        <TextInputComponent
                            style={{height: 127}}
                            containerStyle={{marginBottom: 15}}
                            title={strings("请输入反馈内容")}
                            placeholder={strings("请输入反馈内容")}
                            onChangeText={this.onChangeText}
                            value={userContent}
                            type={"userContent"}
                        />
                        <ItemBtnView
                            style={{borderRadius: 10, marginBottom: 15}}
                            onPress={() => {
                                this.setState({
                                    questionTypeDialog: true,
                                });
                            }}
                            text={strings("反馈类型")}
                            rightText={questionTypeArray[questionTypeSelectIndex - 1]}
                            rightIcon={rightIcon}
                        />
                        <ItemBtnView
                            style={{borderRadius: 10, marginBottom: 15}}
                            onPress={() => {
                                //选择设备
                                this.props.navigation.push("NewSelectDeviceList", {
                                    type: 1,
                                    pageKey: this.props.navigation.state.key,
                                });
                            }}
                            text={strings("选择设备提示")}
                            rightText={deviceName || strings("请选择设备")}
                            rightIcon={rightIcon}
                        />
                        <TextInputComponent
                            style={{height: 50}}
                            containerStyle={{marginBottom: 15}}
                            title={strings("联系方式")}
                            placeholder={strings("请输入联系方式")}
                            onChangeText={this.onChangeText}
                            value={contactInformation}
                            type={"contactInformation"}
                        />
                        <UpLoadView
                            title={strings("上传")}
                            getImageData={this.getImageData}
                        />
                    </View>
                </ScrollView>
                {/* 提交反馈按钮 */}
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 提交反馈
                        this._submit();
                    }}
                    style={{
                        height: 54,
                        backgroundColor: "#18B34F",
                        marginVertical: 10,
                    }}
                    btnText={strings("提交反馈")}
                />
                {_dialogView}
            </View>
        );
    }
}
