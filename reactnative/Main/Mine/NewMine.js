/*
*消息中心/设备推送
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
    ImageBackground,
    NativeModules,
    ScrollView,
    DeviceEventEmitter
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings, setLanguage} from "../Language/I18n";
import NetUtil from '../Net/NetUtil'
import CommonTextView from '../View/CommonTextView'
import I18n from 'react-native-i18n';
import ItemView from '../View/ItemView'
import FastImage from "react-native-fast-image";
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";
import ChooseDialog from '../View/ChooseDialog'

const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

// 图标
const rightIcon = (require('../../resources/greenUnion/list_arrow_ic.png'))
const me_share_ic = (require('../../resources/greenUnion/me_share_ic.png'))
const me_help_ic = (require('../../resources/greenUnion/me_help_ic.png'))
const me_msg_ic = (require('../../resources/greenUnion/me_msg_ic.png'))
const me_language_ic = (require('../../resources/greenUnion/me_language_ic.png'))
const me_theme_ic = (require('../../resources/greenUnion/me_theme_ic.png'))
const me_network_ic = (require('../../resources/greenUnion/me_network_ic.png'))
const me_region_ic = (require('../../resources/greenUnion/me_region_ic.png'))
const me_safe_ic = (require('../../resources/greenUnion/me_safe_ic.png'))
const me_protect_ic = (require('../../resources/greenUnion/me_protect_ic.png'))
const me_about_ic = (require('../../resources/greenUnion/me_about_ic.png'))

class NewMine extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: null
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            name: '',//昵称
            account: '',//登录的账号
            avatarUrl: '',//头像
            themeMode: false,//个性皮肤弹窗
            themeType: 0,//选择个性皮肤
            LanguageType: {text1: strings('简体中文'), text2: strings('简体中文'), id: 0},//语言设置
            selectAreaData: {
                areaId: 1,
                area: strings('亚太地区'),
            },//区域切换
        };
        this.tabData = [
            {text: strings('跟随系统'), id: 0},
            {text: strings('深色'), id: 1},
            {text: strings('浅色'), id: 2},
        ];
    }

    componentWillMount() {
        this._eventListenerAdd()
        this._getUserInfo()
    }

    componentWillUnmount() {
        this.updateUserinfoDeviceNameListener?.remove()
    }

    _eventListenerAdd() {
        this.updateUserinfoDeviceNameListener = DeviceEventEmitter.addListener(EventUtil.UPDATE_USER_INFO, (value) => {
            //收到通知，获取用户信息
            this._getUserInfo()
        })
    }


    //获取用户信息
    _getUserInfo() {
        if (StorageHelper.getTempToken()) {
            NetUtil.getUserInfo(StorageHelper.getTempToken(), false)
                .then((res) => {
                    var userInfo = res.info
                    if (userInfo) {
                        StorageHelper.saveUserInfo(userInfo)
                        StorageHelper.getLoginAccount().then((account) => {
                            if (account) {
                                this.setState({
                                    name: userInfo.name,
                                    account: account,//根据不同登录方式显示账号为手机号或者邮箱
                                    avatarUrl: userInfo.avatar,
                                    // bgUrl: userInfo.background
                                })
                            }
                        })
                        if (!userInfo.avatar)
                            this._setThirdAvatar()
                    } else {
                        this._setThirdAvatar()
                    }
                })
                .catch((error) => {
                    this._setThirdAvatar()
                })
            this.setState({token: StorageHelper.getTempToken()})
        } else {
            console.log('not token')
        }
    }

    // 获取第三房登录用户头像
    _setThirdAvatar() {
        StorageHelper.getThirdAvatar()
            .then((res) => {
                if (res) {
                    this.setState({avatarUrl: res})
                }

            })
    }


    // 选择个性皮肤
    _checkTab = (data) => {
        // 0:系统跟随 1:深色 2:浅色
        this.setState({themeType: data.id, themeMode: false})
    }

    _onHide = () => {
        this.setState({
            themeMode: false
        })
    }

    // 选择语言
    onValueChange = (item) => {
        // 请完善en.js文件英文翻译和LanguageChoose.js文件的Key
        if (I18n.locale !== item.key) {
            setLanguage(item.key)
            StorageHelper.saveLanguage(item.key)
            this.setState({
                LanguageType: item
            })
        }
    }

    // 区域切换
    nextStep = (data) => {
        this.setState({
            selectAreaData: data
        })
    }

    render() {
        StatusBar.setBarStyle('light-content');
        const {name, account, avatarUrl, themeType, LanguageType, selectAreaData} = this.state;
        const userInfo = (
            <TouchableOpacity
                style={{
                    flexDirection: 'row',
                    paddingTop: 70,
                    paddingBottom: 30,
                    width: this.mScreenWidth,
                    paddingHorizontal: 20,
                }}
                onPress={() => {
                    //账号
                    this.props.navigation.navigate('NewAccount', {
                        token: this.state.token,
                        accountValue: this.state.accountValue
                    })
                }}>
                <View style={{width: 70, height: 70, borderRadius: 40, overflow: 'hidden', marginRight: 20}}>
                    <Image style={{position: 'absolute', width: '100%', height: '100%',}}
                           source={require('../../resources/greenUnion/def_head_icon.png')}/>
                    <FastImage
                        style={{width: '100%', height: '100%', position: 'absolute',}}
                        source={{
                            uri: avatarUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                    />
                </View>

                <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                    <View style={{justifyContent: 'center', flex: 1}}>
                        <Text numberOfLines={1}>
                            <CommonTextView
                                textSize={16}
                                text={name}
                                style={{
                                    color: '#FFFFFF',
                                }}/>
                        </Text>
                        <Text numberOfLines={1}>
                            <CommonTextView
                                textSize={13}
                                text={account}
                                style={{
                                    color: 'rgba(255,255,255,0.3)',
                                }}/>
                        </Text>
                    </View>
                    <Image source={require('../../resources/greenUnion/list_arrow_ic.png')}/>
                </View>
            </TouchableOpacity>
        )

        const _dialogView = (
            <View>
                <ChooseDialog
                    title={strings('个性皮肤')}
                    show={this.state.themeMode}
                    data={this.tabData}
                    currentId={this.state.themeType}
                    _checkTab={this._checkTab}
                    _onHide={this._onHide}
                />
            </View>
        )
        return (
            <View style={{width: this.mScreenWidth, height: this.mScreenHeight, alignItems: 'center',}}>
                <StatusBar translucent={true} backgroundColor="transparent"/>
                <Image source={require('../../resources/greenUnion/me_bg.png')}
                       style={{position: 'absolute', width: this.mScreenWidth, height: this.mScreenHeight}}/>
                <ScrollView style={{flex: 1}}>
                    <View>
                        {userInfo}
                        <View style={{
                            backgroundColor: '#fff',
                            paddingBottom: 60,
                            width: this.mScreenWidth,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10,
                        }}>
                            <ItemView
                                onPress={() => {
                                    // 共享管理
                                    this.props.navigation.navigate('ShareManage')
                                }}
                                text={strings('共享管理')}
                                leftIcon={me_share_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    //使用帮助
                                    this.props.navigation.navigate('NewUseHelp');
                                }}
                                text={strings('帮助中心')}
                                leftIcon={me_help_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    //消息设置
                                    this.props.navigation.navigate('MessageSetting');
                                }}
                                text={strings('消息设置')}

                                leftIcon={me_msg_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    //语言选择
                                    this.props.navigation.navigate('LanguageChoose', {
                                        onValueChange: this.onValueChange,
                                        id: LanguageType?.id
                                    });
                                }}
                                text={strings('语言设置')}
                                rightText={LanguageType?.text2}
                                leftIcon={me_language_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    this.setState({
                                        themeMode: true,
                                    })
                                }}
                                text={strings('个性皮肤')}
                                rightText={themeType === 0 ? strings('跟随系统') : themeType === 1 ? strings('深色') : strings('浅色')}
                                leftIcon={me_theme_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    this.props.navigation.navigate('SelectArea', {
                                        nextStep: this.nextStep,
                                        id: selectAreaData.areaId
                                    })
                                }}
                                text={strings('区域切换')}
                                rightText={selectAreaData.area}
                                leftIcon={me_region_ic}
                                rightIcon={rightIcon}
                            />

                            <ItemView
                                onPress={() => {
                                }}
                                text={strings('网络检测')}
                                leftIcon={me_network_ic}
                                rightIcon={rightIcon}
                            />

                            <ItemView
                                onPress={() => {
                                    //账户安全
                                    this.props.navigation.navigate('AccountSave');
                                }}
                                text={strings('账户安全')}
                                leftIcon={me_safe_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    //个人信息保护
                                    this.props.navigation.navigate('PersonalInfoProtection');
                                }}
                                text={strings('个人信息保护')}
                                leftIcon={me_protect_ic}
                                rightIcon={rightIcon}
                            />
                            <ItemView
                                onPress={() => {
                                    //关于
                                    this.props.navigation.navigate('NewAboutUs');
                                }}
                                text={strings('关于')}
                                leftIcon={me_about_ic}
                                rightIcon={rightIcon}
                            />
                        </View>
                    </View>
                </ScrollView>
                {_dialogView}
            </View>
        );
    }
}

export default NewMine;