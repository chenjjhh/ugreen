/*
*我的/选择区域
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform, 
    TouchableOpacity, 
    TextInput,
    ImageBackground,
    NativeModules,
    ScrollView,
    DeviceEventEmitter
} from 'react-native';
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from '../Net/NetUtil'
import CommonTextView from '../View/CommonTextView'
import I18n from 'react-native-i18n';
import ItemView from '../View/ItemView'
import FastImage from "react-native-fast-image";
import StorageHelper from "../Utils/StorageHelper";
import EventUtil from "../Event/EventUtil";
import ChooseDialog from '../View/ChooseDialog'
import countryCode from '../CountryData/CountryData';
import CommonBtnView from '../View/CommonBtnView';

const {StatusBarManager} = NativeModules;
if (Platform.OS === 'ios') {
    StatusBarManager.getHeight(height => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;

// 图标
const selectIcon = (require('../../resources/greenUnion/sel_ic.png'))

class SelectArea extends BaseComponent {
  static navigationOptions = ({navigation}) => { 
    return {
        header: null
    };
}
  constructor(props) {
    super(props);
    this.state = {
     selectData:{
        areaId : props?.navigation?.state?.params?.id || 1,
        area:strings('亚太地区'),
      },//当前选中国家数据
    };
    this.areaData = [
      {
        areaId : 1,
        area:strings('亚太地区'),
      },
      {
        areaId : 2,
        area:strings('北美'),
      },
      {
        areaId : 3,
        area:strings('欧洲'),
      }
    ]
    
  }

  componentDidUpdate( prevProps ){
    if(prevProps?.navigation?.state?.params?.id !== this.props?.navigation?.state?.params?.id){
        this.setState({
          areaId: this.props?.navigation?.state?.params?.id
        })
    }

  }

  // 区域
  select=(item, index)=>{
    this.setState({
      selectData: item
    })
  }

  // 下一步
  nextStep = () =>{
    const { selectData } = this.state;
    const { nextStep } = this.props;
    nextStep?.(selectData)
    this.props.navigation?.goBack()
  }



  render() {
    StatusBar.setBarStyle('light-content');
    const { selectData } = this.state;
    const topView = (
      <View style={{width:'100%',paddingHorizontal:20}}>
            <Text style={{color:'#2F2F2F',lineHeight:60,fontSize:22}}>{strings('选择区域')}</Text>
            <Text style={{color:'#999BA2',lineHeight:24,fontSize:12}}>{strings('选择区域提示')}</Text>
      </View>
    )

    const nextStepBtnView = (
      <View>
          <CommonBtnView
              clickAble={true}
              onPress={() => {
                  this.nextStep()
              }}
              style={{
                  backgroundColor: '#18B34F',
                  width:(this.mScreenWidth-40)*0.9,
                  marginBottom:38,
                  narginTop:10
              }}
              btnText={strings('next')}/>
      </View>
          )
    return (
        <View style={{width:this.mScreenWidth,height:this.mScreenHeight,alignItems:'center',paddingTop:statusBarHeight}}>
            <StatusBar translucent={true} backgroundColor="transparent"/>
              {topView}
              <ScrollView style={{flex:1,paddingHorizontal:20,marginTop:30}}>
                 <View style={{width:this.mScreenWidth-40,}}>
                    { this.areaData?.map((item,index)=>(
                      <TouchableOpacity onPress={()=>this.select(item,index)} style={[{flexDirection:'row',backgroundColor:'#fff',height:60,paddingHorizontal:15,overflow:'hidden'},index === 0 ? {borderTopLeftRadius:15,borderTopRightRadius:15} : null,index === countryCode.length-1 ? {borderBottomLeftRadius:15,borderBottomRightRadius:15} : null]}>
                        <View style={{flexDirection:'row',alignItems:'center',width:this.mScreenWidth-70,justifyContent:'space-between',borderBottomColor:'#EBEBEB',borderBottomWidth:0.5}}>
                            <Text style={{color:'#404040',fontSize:16}}>{item.area}</Text>
                            {selectData.areaId === item.areaId ? <Image style={{width:22,height:22}} source={selectIcon}/> : null}
                        </View>
                      </TouchableOpacity>
                    ))}
                 </View> 
              </ScrollView>
              {nextStepBtnView}
        </View>
    );
  }
}

export default SelectArea;