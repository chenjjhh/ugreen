import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';
import {strings, setLanguage} from '../Language/I18n';
import TitleBar from '../View/TitleBar'
import ShadowCardView from '../View/ShadowCardView'
import ListItemView from '../View/ListItemView'
import CommonTextView from '../View/CommonTextView'
import LinkThirdDialog from '../View/LinkThirdDialog'
import MessageDialog from '../View/MessageDialog'
import StorageHelper from "../Utils/StorageHelper";
import CommonBtnView from "../View/CommonBtnView";
import ViewHelper from "../View/ViewHelper";
import RouterUtil from "../Utils/RouterUtil";
import NetUtil from "../Net/NetUtil";
import DeviceListUtil from "../Utils/DeviceListUtil";
import {AccessToken, LoginManager, Profile} from "react-native-fbsdk-next";
import Helper from "../Utils/Helper";
import {GoogleSignin, statusCodes} from "@react-native-google-signin/google-signin";
import NetConstants from "../Net/NetConstants";
import LogUtil from "../Utils/LogUtil";

var GXRNManager = NativeModules.GXRNManager

/*
*账号设置
 */

export default class Account extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={strings('account')}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            thirdLinkDialog: false,//关联第三方弹窗
            disassociateDialog: false,//解绑弹窗
            deleteAccountDialog: false,//注销弹窗
            deleteAccountUnbindDialog: false,//注销未解绑设备弹窗
            accountValue: props.navigation.state.params.accountValue,
            signOutDialog: false,
            token: props.navigation.state.params.token,
            nickName: '',
            thirdType: '',
            thirdEmail: '',

            thirdInfo: [],
            unBindType: '',
        }
    }

    componentWillMount() {
        this._getUserInfo()
        this._getThirdInfo()
    }

    _getThirdUserInfo(type) {
        if (type == 'Facebook') {
            this.getFacebookProfile()
        } else if (type == 'Google') {
            this.getGoogleCurrentUser()
        }
    }

    getGoogleCurrentUser = async () => {
        const currentUser = await GoogleSignin.getCurrentUser();
        if (currentUser.user && currentUser.user.email) {
            this.setState({
                thirdEmail: currentUser.user.email
            })
        }
    };

    getFacebookProfile() {
        Profile.getCurrentProfile().then(
            function (currentProfile) {
                if (currentProfile.email) {
                    this.setState({
                        thirdEmail: currentProfile.email
                    })
                }
            }
        );
    }

    //获取本地存储的用户信息
    _getUserInfo() {
        StorageHelper.getUserInfo()
            .then((userInfo) => {
                    if (userInfo) {
                        this.setState({
                            accountValue: userInfo.account ? userInfo.account : userInfo.email
                        })
                    }
                }
            )
    }

    _getThirdInfo() {
        if (this.state.token) {
            NetUtil.getThirdInfo(this.state.token, true)
                .then((res) => {
                    if (res.info && res.info.length > 0) {
                        var data = res.info[0]
                        this.setState({
                            nickName: data.nickName,
                            thirdType: data.type,
                            thirdInfo: res.info
                        })
                        this._getThirdUserInfo(data.type)
                    } else {
                        this.setState({
                            nickName: '',
                            thirdType: '',
                            thirdEmail: '',
                            thirdInfo: []
                        })
                    }
                })
        }
    }


    render() {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    backgroundColor: '#FBFBFB'
                }}>
                {this._accountView()}
                {this._bottomView()}
                {ViewHelper.getFlexView()}
                {this._unSignoutBtnView()}

                {this._dialogView()}
            </View>
        );
    }

    _dialogView() {
        return (
            <View>
                <LinkThirdDialog
                    onRequestClose={() => {
                        this.setState({thirdLinkDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({thirdLinkDialog: false})
                    }}
                    modalVisible={this.state.thirdLinkDialog}
                    onFacebookClickEvent={() => {
                        //脸书
                        this._thirdEvent(Helper.channel_login_facebook)
                    }}
                    onAppleClickEvent={() => {
                        //苹果
                        this._thirdEvent(Helper.channel_login_apple)
                    }}
                    onGoogleClickEvent={() => {
                        //谷歌
                        this._thirdEvent(Helper.channel_login_google)
                    }}/>

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({disassociateDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({disassociateDialog: false})
                    }}
                    modalVisible={this.state.disassociateDialog}
                    title={strings('remove_bind')}
                    message={strings('remove_bind_tips')}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({disassociateDialog: false})
                    }}
                    rightBtnText={strings('unbind')}
                    rightBtnTextColor={'#F77979'}
                    onRightBtnClick={() => {
                        //解绑
                        this.setState({disassociateDialog: false})
                        this._unBindThird()
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>


                <MessageDialog
                    onRequestClose={() => {
                        this.setState({deleteAccountDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({deleteAccountDialog: false})
                    }}
                    modalVisible={this.state.deleteAccountDialog}
                    title={strings('delete_account')}
                    message={strings('delete_account_tips')}
                    leftBtnText={strings('cancel')}
                    leftBtnTextColor={'#000000'}
                    onLeftBtnClick={() => {
                        //取消
                        this.setState({deleteAccountDialog: false})
                    }}
                    rightBtnText={strings('delete')}
                    rightBtnTextColor={'#F77979'}
                    onRightBtnClick={() => {
                        //注销
                        this.setState({deleteAccountDialog: false})
                        this._userCancelAccount()
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>


                <MessageDialog
                    onRequestClose={() => {
                        this.setState({deleteAccountUnbindDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({deleteAccountUnbindDialog: false})
                    }}
                    modalVisible={this.state.deleteAccountUnbindDialog}
                    title={strings('delete_account')}
                    message={strings('delete_account_unbind_device')}
                    leftBtnText={strings('ok')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({deleteAccountUnbindDialog: false})
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>

                <MessageDialog
                    onRequestClose={() => {
                        this.setState({signOutDialog: false})
                    }}
                    overViewClick={() => {
                        this.setState({signOutDialog: false})
                    }}
                    modalVisible={this.state.signOutDialog}
                    title={strings('sign_out')}
                    message={strings('signout_tips')}
                    leftBtnText={strings('comfire')}
                    leftBtnTextColor={'#38579D'}
                    onLeftBtnClick={() => {
                        //好的
                        this.setState({signOutDialog: false})
                        this._singOut()
                    }}
                    messageStyle={{
                        textAlign: 'center'
                    }}/>
            </View>
        )
    }

    _accountView() {
        return (
            <ShadowCardView
                style={{
                    marginBottom: 15,
                    alignItems: 'center',
                    height: 56,
                    justifyContent: 'center',
                    marginTop: 10
                }}>
                <CommonTextView
                    textSize={14}
                    style={{
                        color: '#000000',
                    }}
                    text={this.state.accountValue}/>
            </ShadowCardView>
        )
    }

    _bottomView() {
        return (
            <ShadowCardView
                style={{
                    marginBottom: 15,
                }}>
                <ListItemView
                    title={strings('change_pd')}
                    value={''}
                    onPress={() => {
                        //修改密码
                        this.props.navigation.navigate('ChangePassword');
                    }}/>
                <ListItemView
                    rightIcons={this._getThirdIcons()}
                    title={strings('linked_account')}
                    //value={this.state.thirdEmail}
                    onPress={() => {
                        // if (this.state.thirdType) {
                        //     //解除绑定
                        //     this.setState({disassociateDialog: true})
                        // } else {
                        //     //关联账号
                        //     this.setState({thirdLinkDialog: true})
                        // }
                        this.setState({thirdLinkDialog: true})
                    }}/>
                <ListItemView
                    title={strings('sign_out_account')}
                    value={''}
                    onPress={() => {
                        //注销账号
                        this._beforeCancelGetDeviceList()
                    }}/>
            </ShadowCardView>
        )
    }

    _thirdEvent(thirdFlag) {
        this.setState({thirdLinkDialog: false})
        var removeBindFlag = false
        this.state.thirdInfo.map((item, index) => {
            if (thirdFlag == item.type) {
                removeBindFlag = true
                this.setState({
                    unBindType: item.type
                })
            }
        })

        if (removeBindFlag) {
            //解绑
            this.setState({disassociateDialog: true})
            return
        }

        if (thirdFlag == Helper.channel_login_facebook) {
            this._facebookLogin()
        } else if (thirdFlag == Helper.channel_login_google) {
            this._googleLogin()
        } else if (thirdFlag == Helper.channel_login_apple) {
            this._appleLogin()
        }
    }

    _getThirdIcon(type) {
        return type == Helper.channel_login_facebook ? require('../../resources/link_facebook_btn.png')
            : type == Helper.channel_login_google ? require('../../resources/link_google_btn.png')
                : type == Helper.channel_login_apple ? require('../../resources/account_apple_ic.png')
                    : 0
    }

    _getThirdIcons() {
        var iconArray = []
        this.state.thirdInfo.map((item, index) => {
            if (item.type == Helper.channel_login_facebook) {
                iconArray.push(require('../../resources/link_facebook_btn.png'))
            } else if (item.type == Helper.channel_login_google) {
                iconArray.push(require('../../resources/link_google_btn.png'))
            } else if (item.type == Helper.channel_login_apple) {
                iconArray.push(require('../../resources/account_apple_ic.png'))
            }
        })

        return iconArray
    }

    _unSignoutBtnView() {
        return (
            <CommonBtnView
                clickAble={true}
                onPress={() => {
                    //退出登录
                    this.setState({signOutDialog: true})
                }}
                style={{
                    marginBottom: 40,
                    width: this.mScreenWidth - 50,
                    backgroundColor: '#F77979',
                }}
                btnText={strings('sign_out')}/>
        )
    }

    _singOut() {
        StorageHelper.clearAllData().then(() => {
            RouterUtil.toLogin()
        })
    }

    //注销账号前获取设备列表，查看是否有绑定的设备
    _beforeCancelGetDeviceList() {
        if (this.state.token == '')
            return

        DeviceListUtil._getDeviceList(true, this.state.token, false)
            .then((res) => {
                var deviceInfo = res.info
                if (deviceInfo && deviceInfo.length > 0) {
                    //存在未解绑的设备，无法注销账号
                    this.setState({
                        deleteAccountUnbindDialog: true
                    })
                } else {
                    this.setState({
                        deleteAccountDialog: true
                    })
                }
            })
            .catch((error) => {
            })
    }

    //注销账号
    _userCancelAccount() {
        if (this.state.token == '')
            return

        NetUtil.userCancelAccount(this.state.token, true)
            .then((res) => {
                this.onShowToast(strings('successed'))
                this._singOut()
            })
            .catch((error) => {
                this.onShowToast(strings('failed'))
            })
    }

    _unBindThird() {
        if (this.state.token == '' || this.state.unBindType == '')
            return

        NetUtil.unBindThird(this.state.token, this.state.unBindType, true)
            .then((res) => {
                this._getThirdInfo()
                this._thirdSignOut(this.state.unBindType)
            })
    }

    _thirdSignOut(type) {
        if (type == 'Facebook') {
            this.facebookSignOut()
        } else if (type == 'Google') {
            this.googleSignOut()
        }
    }

    googleSignOut = async () => {
        try {
            await GoogleSignin.signOut();
        } catch (error) {
            LogUtil.debugLog(error);
        }
    };

    facebookSignOut = async () => {
        try {
            await LoginManager.logOut()
        } catch (error) {
            LogUtil.debugLog(error);
        }
    };

    _facebookLogin() {
        LoginManager.logInWithPermissions(["public_profile"]).then(
            (result) => {
                if (result.isCancelled) {
                    LogUtil.debugLog("Login cancelled");
                } else {
                    LogUtil.debugLog(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            LogUtil.debugLog('facebook_AccessToken:' + data.accessToken.toString())
                            if (data.accessToken) {
                                this._thirdAuth(NetConstants.FACEBOOK_APPID, data.accessToken, Helper.channel_login_facebook)
                            }
                        }
                    )
                }
            },
            (error) => {
                LogUtil.debugLog("Login fail with error: " + error);
            }
        );
    }

    // Somewhere in your code
    _googleLogin = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            LogUtil.debugLog('google_login_userInfo:' + JSON.stringify(userInfo))
            const idToken = await GoogleSignin.getTokens();
            LogUtil.debugLog('google_login_idToken:' + JSON.stringify(idToken))
            if (userInfo.idToken) {
                this._thirdAuth(Platform.OS == 'android' ? NetConstants.GOOGLE_ANDROID_CLIENTID : NetConstants.GOOGLE_IOS_CLIENTID, userInfo.idToken, Helper.channel_login_google)
            } else {
                this.onShowToast('idToken is null')
            }
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                LogUtil.debugLog('google_login_fail:user cancelled ====>' + JSON.stringify(error))
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
                LogUtil.debugLog('google_login_fail:is in progress already ====>' + JSON.stringify(error))
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                LogUtil.debugLog('google_login_fail:play services not available or outdated ====>' + JSON.stringify(error))
            } else {
                // some other error happened
                LogUtil.debugLog('google_login_fail:some other error happened ====>' + JSON.stringify(error))
            }
        }
    };

    _appleLogin() {
        GXRNManager.signInWithAppleId().then((datas) => {
            LogUtil.debugLog('apple_login:', JSON.stringify(datas));
            if (datas.identityToken) {
                this._thirdAuth(NetConstants.APPLE_CLIENTID, datas.identityToken, Helper.channel_login_apple)
            }
        }).catch((err) => {
            LogUtil.debugLog('apple_login_fail:', JSON.stringify(err));
        });
    }

    _thirdAuth(clientId, idToken, loginType) {
        NetUtil.accountBindThird(clientId, idToken, loginType, this.state.token, true)
            .then((res) => {
                this.onShowToast(strings('bind_third_success'))
                this._getThirdInfo()
            })
        // if (info) {
        //     if (info.register == true) {
        //         //需要绑定邮箱
        //         this._toBindEmail(info.data, loginType)
        //     } else {
        //         //直接登录
        //         var token = info.data.granwin_token
        //         StorageHelper.saveToken(token)
        //         StorageHelper.saveLoginInfo(info)
        //         RouterUtil.toMainDeviceList(token)
        //     }
        // }
    }

    _toBindEmail(data, loginType) {
        this.props.navigation.navigate('BindEmail', {
            data: data,
            loginType: loginType
        });
    }
}