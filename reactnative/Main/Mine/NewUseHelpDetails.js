// 我的/使用帮助/设备帮助详情/详情
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    NativeModules,
    Switch,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import DialogContainerView from "../View/DialogContainerView";
import CommonTextView from "../View/CommonTextView";
import {strings, setLanguage} from "../Language/I18n";
import DialogBottomBtnView from "../View/DialogBottomBtnView";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
import CommonBtnView from "../View/CommonBtnView";
import {WebView} from "react-native-webview";

const {StatusBarManager} = NativeModules;
if (Platform.OS === "ios") {
    StatusBarManager.getHeight((height) => {
        statusBarHeight = height.height;
    });
} else {
    statusBarHeight = StatusBar.currentHeight;
}
//获取状态栏高度
let statusBarHeight;
export default class NewUseHelpDetails extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        const {title} = navigation?.state?.params?.data || {};
        return {
            header: (
                <TitleBar
                    titleText={title ? title + '?' : ""}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        const {data} = props.navigation?.state?.params || {};
        this.state = {
            //   data: {
            //     title: "",
            //     content:
            //       "设备进入配对状态后，点击“设备-添加”，进入“添加设备”页面，App会通过蓝牙自动搜索附近设备，点击已发现的设备图标，即可进行连接添加。若蓝牙未发现设备，可点击设备所属的产品列表图片，查看设备添加引导，按引导图操作设备进入配对状态后，再进行以上步骤添加。",
            //     img: "https://oss.cstest.granwin.com/image/UGREEN_GS2200.png",
            //   },
            data,
            imageSize: {}, //网络图片尺寸
            webViewHeight: 0, //网页内容高度
        };
    }

    componentDidMount() {
        // this.getwebImageSize();
    }

    getwebImageSize = () => {
        let imageSize = {};
        const {img} = this.state.data || {};
        // // 获取网络图片size
        Image.getSize(
            img || "",
            (w, h) => {
                let proportion = w / (this.mScreenWidth - 40); //计算网络图片的原始宽高比
                let height = h / proportion; //计算新高度
                let width = w / proportion; //计算新宽度
                imageSize["height"] = height;
                imageSize["width"] = width;
                this.setState({imageSize});
            },
            (error) => {
                imageSize["height"] = 0;
                imageSize["width"] = 0;
                this.setState({imageSize});
            }
        );
    };

    /* 根据内容计算 WebView 高度 */
    onWebViewMessage = (event, index) => {
        this.setState({
            webViewHeight: Number(event.nativeEvent.data),
        });
    };

    render() {
        const {data, imageSize, webViewHeight} = this.state;
        return (
            <View
                style={{
                    height: this.mScreenHeight,
                    backgroundColor: "#F1F2F6",
                    alignItems: "center",
                    width: this.mScreenWidth,
                }}
            >
                <ScrollView style={{flex: 1, width: this.mScreenWidth}}>
                    {/* <View
            style={{
              width: "100%",
              paddingHorizontal: 20,
              paddingVertical: 15,
              alignItems: "center",
            }}
          >
            <Text style={styles.textStyle}>{data?.content}</Text>
            <Image
              style={{
                width: imageSize.width,
                height: imageSize.height,
                marginVertical: 20,
              }}
              source={{
                uri: data?.img || "",
              }}
            />
          </View> */}
                    <WebView
                        ref={(webview) => {
                            this.webview = webview;
                        }}
                        originWhitelist={["*"]}
                        // source={{uri: "https://www.baidu.com/" }}
                        source={{uri: data?.visitPath}}
                        // 内容类型配置网页展示
                        // source={{
                        //   html: `<html>
                        //             <head>
                        //                 <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;">
                        //             </head>
                        //             <body style="margin-left:0;margin-top:0;">
                        //                 ${''}
                        //             </body>
                        //         </html>`,
                        // }}
                        injectedJavaScript="window.ReactNativeWebView.postMessage(document.body.scrollHeight)"
                        onMessage={(event) => {
                            this.onWebViewMessage(event);
                        }}
                        androidHardwareAccelerationDisabled={true} //可以禁用WebView组件的硬件加速功能，从而避免应用程序崩溃或闪退的问题。这是因为禁用硬件加速后，应用程序将使用软件方式进行渲染，可以更好地处理WebView组件中的返回操作和Android系统的返回键操作之间的冲突。
                        style={{height: webViewHeight, backgroundColor: "#F1F2F6"}}
                    />
                </ScrollView>
                <View
                    style={{marginBottom: statusBarHeight + 55 + 20, marginTop: 10}}
                >
                    <CommonBtnView
                        clickAble={true}
                        onPress={() => {
                            // 建议与反馈
                            this.props.navigation.navigate("SuggestionAndFeedback");
                        }}
                        style={{
                            height: 54,
                            backgroundColor: "#18B34F",
                        }}
                        btnText={strings("建议与反馈")}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {color: "#808080", fontSize: 14, lineHeight: 23},
    box: {justifyContent: "center", alignItems: "center"},
});
