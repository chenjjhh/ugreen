import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import PagerTitleView from "../View/PagerTitleView";
import DeviceCard from "../View/DeviceCard";
import ViewHelper from '../View/ViewHelper'


/*
*设备评分
 */

export default class DeviceRating extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FFFFFF'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            powerDeviceArray: [],
            cleanDeviceArray: [],
        }
    }

    componentWillMount() {
        this._setTempData()
    }

    _setTempData() {
        var tempPowerDeviceArray = [
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT4'}]
        ]
        var tempCleanDeviceArray = [
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT'}, {deviceTypeName: 'VOGEE XT1'}, {deviceTypeName: 'VOGEE XT3'}],
            [{deviceTypeName: 'VOGEE XT4'}, {deviceTypeName: 'VOGEE XT5'}]
        ]
        this.setState({
            powerDeviceArray: tempPowerDeviceArray,
            cleanDeviceArray: tempCleanDeviceArray,
        })
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FFFFFF',
                }}>
                {this._titleView()}
                {this._tipsView()}
                {this._deviceView()}
            </View>
        );
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={strings('use_help')}
            />
        )
    }

    _tipsView() {
        return (
            <CommonTextView
                textSize={14}
                text={strings('select_device_type')}
                style={{
                    color: '#969AA1',
                    marginLeft: 18,
                    marginTop: 15,
                }}/>
        )
    }


    _deviceView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 36,
                    height: this.state.bottomContainerViewHeight,
                    marginLeft: 18,
                    marginBottom: 50
                }}>
                <ScrollView
                    showsVerticalScrollIndicator={false}>
                    {this._deviceItemView(strings('power_device'), this.state.powerDeviceArray, false)}
                    {/*{this._deviceItemView(strings('clean_device'), this.state.cleanDeviceArray, false)}*/}
                </ScrollView>
            </View>
        )
    }

    _deviceItemView(title, deviceArray, showShadow) {
        return (
            <View>
                <CommonTextView
                    textSize={14}
                    text={title}
                    style={{
                        color: '#000000',
                        marginTop: 50
                    }}/>

                {
                    deviceArray.map((item, index) => {
                        return (
                            <View
                                key={index}
                                style={{
                                    flexDirection: 'row'
                                }}>
                                {
                                    item.map((item1, index1) => {
                                        return (
                                            <DeviceCard
                                                onPress={() => {
                                                    this.props.navigation.navigate('DeviceRatingDetail', {deviceTypeName: item1.deviceTypeName});
                                                }}
                                                style={{
                                                    marginBottom: showShadow ? 10 : 0
                                                }}
                                                key={index1}
                                                showShadow={showShadow}
                                                isNewShare={item1.isNewShare ? item1.isNewShare : false}
                                                deviceTypeName={item1.deviceTypeName}/>
                                        )
                                    })
                                }
                                {
                                    item.length == 1 ?
                                        ViewHelper.getFlex2View()
                                        : item.length == 2 ? ViewHelper.getFlexView()
                                        : null
                                }
                            </View>
                        )
                    })
                }
            </View>
        )
    }

}