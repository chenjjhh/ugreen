/*
 *我的/建议与反馈/问题反馈或售后申请列表
 */
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  KeyboardAvoidingView,
  StatusBar,
  Platform,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  DeviceEventEmitter,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import { strings } from "../Language/I18n";
import NetUtil from "../Net/NetUtil";
import CommonTextView from "../View/CommonTextView";
import I18n from "react-native-i18n";
import StorageHelper from "../Utils/StorageHelper";
import FastImage from "react-native-fast-image";
import Helper from "../Utils/Helper";
import EventUtil from "../Event/EventUtil";
import TitleBar from "../View/TitleBar";
import CommonMessageDialog from "../View/CommonMessageDialog";
export default class RecordListView extends BaseComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <TitleBar
          titleText={strings("我的记录")}
          backgroundColor={"#F1F2F6"}
          leftIcon={require("../../resources/back_black_ic.png")}
          leftIconClick={() => {
            navigation.goBack();
          }}
        />
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      list: [
        // {
        //   feedbackHandleStatus: "0",
        //   flag: "FEEDBACK",
        //   productId: 123,
        //   createTime: 1701425208474,
        //   userContent: "测试提交",
        //   feedbackId: 890507774388318200,
        //   source: 2,
        //   userFiles: '[{"fileName":"xxx","fileUrl":"xxx","fileType":"xxx"}]',
        //   deviceSn: "123",
        //   feedbackType: 2,
        //   feedbackSolveStatus: "PROCESSING",
        // },
        // {
        //   afterSalesId: 890505740230570000,
        //   flag: "AFTER_SALES",
        //   productId: 2344,
        //   channel: "淘宝",
        //   source: 0,
        //   deviceSn: "1234",
        //   afterSalesType: "RETURN_GOODS",
        //   contactInformation: "13875110551",
        //   createTime: 1701424723494,
        //   userContent: "不好用",
        //   contactAddress: "阿里巴巴",
        //   userFiles: '[{"fileName":"xxx","fileUrl":"xxx","fileType":"video"}]',
        //   afterSalesHandleStatus: "WAIT_AUDIT",
        //   contacts: "caoyu",
        // },
        // {
        //   afterSalesId: 890505740230570000,
        //   flag: "AFTER_SALES",
        //   productId: 2344,
        //   channel: "淘宝",
        //   source: 0,
        //   deviceSn: "1234",
        //   afterSalesType: "RETURN_GOODS",
        //   contactInformation: "13875110551",
        //   createTime: 1701424723494,
        //   userContent: "不好用",
        //   contactAddress: "阿里巴巴",
        //   userFiles: '[{"fileName":"xxx","fileUrl":"xxx","fileType":"video"}]',
        //   afterSalesHandleStatus: "PASSED_AUDIT",
        //   contacts: "caoyu",
        // },
        // {
        //   afterSalesId: 890505740230570000,
        //   flag: "AFTER_SALES",
        //   productId: 2344,
        //   channel: "淘宝",
        //   source: 0,
        //   deviceSn: "1234",
        //   afterSalesType: "RETURN_GOODS",
        //   contactInformation: "13875110551",
        //   createTime: 1701424723494,
        //   userContent: "不好用",
        //   contactAddress: "阿里巴巴",
        //   userFiles: '[{"fileName":"xxx","fileUrl":"xxx","fileType":"video"}]',
        //   afterSalesHandleStatus: "COMPLETED",
        //   contacts: "caoyu",
        // },
        // {
        //   flag: "FEEDBACK",
        //   productId: 123,
        //   feedbackId: 890499235361996800,
        //   source: 2,
        //   deviceSn: "123",
        //   feedbackHandleStatus: "1",
        //   replyFiles:
        //     '{"fileName":"111.png","fileUrl":"https://granwin-file.oss-cn-shanghai.aliyuncs.com/image/892277228301197312_111.png","fileType":"picture"}',
        //   createTime: 1701423172612,
        //   userContent: "测试提交",
        //   replyContent: "3434",
        //   userFiles:
        //     '[{"fileName":"xxx","fileType":"picture","fileUrl":"https://granwin-file.oss-cn-shanghai.aliyuncs.com/image/892267327100039168_111.png"}]',
        //   feedbackType: 2,
        //   feedbackSolveStatus: "UNDETERMINED",
        // },
      ],
      pageNum: 1,
      pageSize: 10,
      loading: false,
      refreshing: false,
      noMoreData: true,
      isLoadingMore: false,
      token: StorageHelper.getTempToken() || "",
      trackingNumber: "", //填写的快递单号
      trackingNumberDialog: false,
      cuurentItem: {}, //当前操作的列表项
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const { pageNum, pageSize, list, token } = this.state;
    this.setState({ loading: true });
    NetUtil.recordList(token, pageNum, pageSize, true)
      .then((res) => {
        if (res?.list?.length > 0) {
              this.setState({
                  list: pageNum === 1 ? res?.list : [...list, ...res?.list],
                  loading: false,
                  refreshing: false,
                  isLoadingMore: false,
                });
          }else{
              this.setState({
                  loading: false,
                  refreshing: false,
                  noMoreData: true,
                  isLoadingMore: false,
                });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  handleRefresh = () => {
    this.setState({ refreshing: true, pageNum: 1, noMoreData: false }, () => {
      this.getData();
    });
  };

  handleLoadMore = () => {
    const { isLoadingMore, noMoreData } = this.state;
    if (!isLoadingMore && !noMoreData) {
      this.setState(
        (prevState) => ({
          pageNum: prevState.pageNum + 1,
          isLoadingMore: true,
        }),
        () => {
          this.getData();
        }
      );
    }
  };

  // 更新反馈状态
  updateFeedback = (solveStatus, feedbackId) => {
    // type: UNSOLVED //未解决 SOLVED: 已解决
    const { token } = this.state;
    NetUtil.upDateFeedback(token, feedbackId, solveStatus, true)
      .then((res) => {
        this.onShowToast(strings("successed"));
        this.getData();
      })
      .catch((error) => {});
  };

  // 更新售后状态
  upDateAfterSale = () => {
    const {
      token,
      cuurentItem: { afterSalesId },
      trackingNumber,
    } = this.state;
    NetUtil.upDateAfterSale(token, afterSalesId, trackingNumber, true)
      .then((res) => {
        this.onShowToast(strings("successed"));
        this.getData();
      })
      .catch((error) => {});
  };

  renderFooter = () => {
    const { isLoadingMore, noMoreData, list } = this.state;
    if (isLoadingMore && !noMoreData) {
      return (
        <View style={{ paddingVertical: 20 }}>
          <ActivityIndicator animating size="large" color={"#18B34F"} />
        </View>
      );
    } else if (noMoreData && list?.length > 0) {
      return (
        <View style={{ paddingVertical: 20 }}>
          <Text style={{ textAlign: "center" }}>没有更多数据了</Text>
        </View>
      );
    } else {
      return null;
    }
  };

  renderItem = (item, index) => {
    const {
      flag, //	string	类型 FEEDBACK -反馈 AFTER_SALES-售后
      replyFiles, //	string	回复文件
      replyContent, //	string	客服回复内容
      userFiles, //	string	用户文件
      userContent, //	string	用户提交的内容
      deviceSn, //	string	设备sn
      createTime, //	string	创建时间
      productId, //	string	产品id
      source, //	string	来源 【0】ios 【1】Android
      feedbackId, //	string	反馈id 反馈才有该字段
      feedbackHandleStatus, //	string	反馈处理状态 反馈才有该字段 0-未处理 1-已处理，这个字段app目前应该用不上
      feedbackType, //	string	反馈类型 反馈才有该字段 1-账号问题 2-固件升级问题 3- 体验问题 4- 性能问题 5-友好建议
      feedbackSolveStatus, //	string	反馈解决状态 反馈才有该字段 UNSOLVED-未解决 SOLVED-已解决 PROCESSING-处理中 UNDETERMINED-待确认
      afterSalesId, //	string	售后id 售后才有该字段
      channel, //	string	渠道 售后才有该字段
      afterSalesType, //	string	售后类型 售后才有该字段
      contactInformation, //	string	联系方式 售后才有该字段
      contactAddress, //	string	联系地址 售后才有该字段
      afterSalesHandleStatus, //	string	售后处理状态 售后才有该字段  WAIT_AUDIT-等待审核 | PROCESSING-售后处理中 | PASSED_AUDIT-审核通过 | REJECTED-已拒绝 | COMPLETED-已完成
      supportTrackingNumber, //	string	后台填写的邮寄编码 售后才有该字段
      customerTrackingNumber, //	string	客户填写的邮寄编码 售后才有该字段
      contacts, //	string	联系人 售后才有该字段
    } = item;
    return (
      <View
        key={index}
        style={{
          width: this.mScreenWidth,
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <View
          style={{
            backgroundColor: "#fff",
            width: this.mScreenWidth - 40,
            justifyContent: "center",
            borderRadius: 10,
            marginBottom: 15,
            paddingHorizontal: 20,
          }}
        >
          {/* 记录标题、状态等 */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              paddingVertical: 10,
            }}
          >
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  fontSize: 14,
                  color: "#404040",
                  lineHeight: 25,
                  fontWeight: "bold",
                }}
              >
                {flag === "FEEDBACK"
                  ? strings("问题反馈")
                  : strings("售后申请")}
              </Text>
              <Text style={{ fontSize: 12, color: "#C4C6CD", lineHeight: 16 }}>
                {Helper.formatTimestamp(createTime)}
              </Text>
            </View>
            {/* 灰色：问题反馈已解决、售后申请已完成 / 红色：售后申请已拒绝、 问题反馈未解决 绿色：其他状态*/}
            <Text
              style={{
                fontSize: 12,
                color:
                  feedbackSolveStatus === "SOLVED" ||
                  afterSalesHandleStatus === "COMPLETED"
                    ? "#C4C6CD"
                    : feedbackSolveStatus === "UNSOLVED" ||
                      afterSalesHandleStatus === "REJECTED"
                    ? "#E16262"
                    : "#18B34F",
                lineHeight: 16,
              }}
            >
              {/* todo: 问题反馈类型状态：UNSOLVED-未解决 SOLVED-已解决 PROCESSING-处理中 UNDETERMINED-待确认 */}
              {/* 售后处理类型状态  WAIT_AUDIT-等待审核 | PROCESSING-售后处理中 | PASSED_AUDIT-审核通过 | REJECTED-已拒绝 | COMPLETED-已完成   */}
              {flag === "FEEDBACK"
                ? feedbackSolveStatus === "PROCESSING"
                  ? strings("处理中")
                  : feedbackSolveStatus === "UNDETERMINED"
                  ? strings("待确认")
                  : feedbackSolveStatus === "SOLVED"
                  ? strings("已解决")
                  : feedbackSolveStatus === "UNSOLVED"
                  ? strings("未解决")
                  : ""
                : afterSalesHandleStatus === "WAIT_AUDIT"
                ? strings("等待审核")
                : afterSalesHandleStatus === "PROCESSING"
                ? strings("售后处理中")
                : afterSalesHandleStatus === "PASSED_AUDIT"
                ? strings("审核通过")
                : afterSalesHandleStatus === "REJECTED"
                ? strings("已拒绝")
                : afterSalesHandleStatus === "COMPLETED"
                ? strings("已完成")
                : ""}
            </Text>
          </View>
          {/* 反馈内容 */}
          <View
            style={{
              paddingBottom: 10,
              borderTopColor: "#EBEBEB",
              borderTopWidth: 0.5,
            }}
          >
            <Text
              style={{
                fontSize: 12,
                color: "#404040",
                lineHeight: 39,
                fontWeight: "bold",
              }}
            >
              {strings("反馈内容")}
            </Text>
            <Text style={{ fontSize: 12, lineHeight: 18, color: "#808080" }}>
              {userContent}
            </Text>
          </View>

          {/* 客服回复 */}
          {replyContent ? (
            <View
              style={{
                paddingBottom: 10,
                borderTopColor: "#EBEBEB",
                borderTopWidth: 0.5,
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  color: "#404040",
                  lineHeight: 39,
                  fontWeight: "bold",
                }}
              >
                {strings("客服回复")}
              </Text>
              <Text style={{ fontSize: 12, lineHeight: 18, color: "#808080" }}>
                {replyContent}
              </Text>
            </View>
          ) : null}

          {/* 售后申请已拒绝，重新提交按钮 */}
          {flag === "AFTER_SALES" && afterSalesHandleStatus === "REJECTED" ? (
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  borderRadius: 22,
                  overflow: "hidden",
                  marginVertical: 20,
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("afterSalesApplication")
                  }
                  style={{
                    backgroundColor: "#18B34F",
                    height: 28,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 20,
                  }}
                >
                  <Text style={{ fontSize: 12, color: "#fff", lineHeight: 16 }}>
                    {strings("重新提交")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}

          {/* 售后申请完成后，商家寄回快递单号 */}
          {flag === "AFTER_SALES" && afterSalesHandleStatus === "COMPLETED" ? (
            <View
              style={{
                paddingVertical: 10,
                borderTopColor: "#EBEBEB",
                borderTopWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 12, lineHeight: 18, color: "#808080" }}>
                {strings("售后申请完成并寄回") + (supportTrackingNumber || "")}
              </Text>
            </View>
          ) : null}
          {/* 更新反馈状态按钮 */}
          {flag === "FEEDBACK" && feedbackSolveStatus === "UNDETERMINED" ? (
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  borderRadius: 22,
                  overflow: "hidden",
                  marginVertical: 20,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.updateFeedback("UNSOLVED", feedbackId)}
                  style={{
                    backgroundColor: "#F0F0F0",
                    height: 28,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 20,
                  }}
                >
                  <Text
                    style={{ fontSize: 12, color: "#808080", lineHeight: 16 }}
                  >
                    {strings("未解决")}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.updateFeedback("SOLVED", feedbackId)}
                  style={{
                    backgroundColor: "#18B34F",
                    height: 28,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 20,
                  }}
                >
                  <Text style={{ fontSize: 12, color: "#fff", lineHeight: 16 }}>
                    {strings("已解决")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}

          {/* 售后申请且审核通过后，提交快递单号按钮 */}
          {flag === "AFTER_SALES" &&
          afterSalesHandleStatus === "PASSED_AUDIT" ? (
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  borderRadius: 22,
                  overflow: "hidden",
                  marginBottom: 20,
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      trackingNumberDialog: true,
                      cuurentItem: item,
                    });
                  }}
                  style={{
                    backgroundColor: "#18B34F",
                    height: 28,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 20,
                  }}
                >
                  <Text style={{ fontSize: 12, color: "#fff", lineHeight: 16 }}>
                    {strings("提交快递单号")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}

          {/* 售后申请且完成后，复制快递单号按钮 */}
          {flag === "AFTER_SALES" && afterSalesHandleStatus === "COMPLETED" ? (
            <View
              style={{
                width: "100%",
                flexDirection: "row",
                justifyContent: "flex-end",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  borderRadius: 22,
                  overflow: "hidden",
                  marginBottom: 20,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.copyTrackingNumber("")}
                  style={{
                    backgroundColor: "#18B34F",
                    height: 28,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: 20,
                  }}
                >
                  <Text style={{ fontSize: 12, color: "#fff", lineHeight: 16 }}>
                    {strings("复制快递单号")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </View>
      </View>
    );
  };

  render() {
    const { list, loading, refreshing, trackingNumberDialog } = this.state;

    const _dialogView = (
      <View>
        {/* 提交快递单号弹窗 */}
        <CommonMessageDialog
          leftBtnTextColor={"#808080"}
          onLeftBackgroundColor={"#F1F2F6"}
          rightBtnTextColor={"#ffffff"}
          onRightBackgroundColor={"#18B34F"}
          children={
            <View
              style={{
                flexDirection: "row",
                height: 45,
                width: "100%",
                alignItems: "center",
                justifyContent: "center",
                marginVertical: 30,
              }}
            >
              <TextInput
                style={[
                  {
                    height: 55,
                    textAlign: "left",
                    paddingHorizontal: 15,
                    fontSize: this.getSize(14),
                    color: "#404040",
                    backgroundColor: "#EBEEF0",
                    width: "100%",
                    borderRadius: 40,
                  },
                ]}
                maxLength={20}
                onChangeText={(text) => {
                  this.setState({
                    trackingNumber: text,
                  });
                }}
              />
            </View>
          }
          onRequestClose={() => {
            this.setState({ trackingNumberDialog: false });
          }}
          overViewClick={() => {
            this.setState({ trackingNumberDialog: false });
          }}
          modalVisible={trackingNumberDialog}
          title={strings("昵称")}
          leftBtnText={strings("cancel")}
          onLeftBtnClick={() => {
            //取消
            this.setState({ trackingNumberDialog: false });
          }}
          rightBtnText={strings("comfire")}
          onRightBtnClick={() => {
            //确定
            this.setState({ trackingNumberDialog: false }, () => {
              this.upDateAfterSale();
            });
          }}
        />
      </View>
    );
    return (
      <View
        style={{
          flex: 1,
          width: this.mScreenWidth,
          alignItems: "center",
          backgroundColor: "#F1F2F6",
        }}
      >
        <FlatList
          data={list}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.handleRefresh}
              tintColor="#18B34F" // 设置小图标颜色为蓝色ios
              colors={["#18B34F"]} // 设置小图标颜色为蓝色android
              title={strings("loading")} //iOS
              titleColor={"#999BA2"}
            />
          }
          onEndReached={this.handleLoadMore}
          ListFooterComponent={this.renderFooter}
          onEndReachedThreshold={0.5}
          ListEmptyComponent={
            !loading && list.length === 0 ? (
              <View
                style={{
                  paddingVertical: 20,
                  width: this.mScreenWidth,
                  height: 500,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Image
                  source={require("../../resources/greenUnion/msg_nomsg_ic.png")}
                />
              </View>
            ) : null
          }
        />
        {_dialogView}
      </View>
    );
  }
}
