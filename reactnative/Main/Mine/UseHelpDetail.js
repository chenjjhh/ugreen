import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import ShadowCardView from '../View/ShadowCardView'
import NetUtil from "../Net/NetUtil";
import AutoHeightWebView from 'react-native-autoheight-webview'
import StorageHelper from "../Utils/StorageHelper";
import LogUtil from "../Utils/LogUtil";

/*
*使用帮助详情
 */

export default class UseHelpDetail extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                    titleText={navigation.state.params ? navigation.state.params.deviceTypeName : ''}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            dataArray: [],
            token: props.navigation.state.params.token,
            productId: props.navigation.state.params.productId,
            imgSmall: props.navigation.state.params.imgSmall,
            avatarUrl: '',
        }
    }

    _getUseHelp() {
        NetUtil.getUseHelp(this.state.token, this.state.productId, true)
            .then((res) => {
                var list = res.list
                if (list && list.length > 0) {
                    var tempQuestArray = []
                    list.map((item, index) => {
                        tempQuestArray.push(item)
                    })

                    var tempArray = []
                    tempArray.push({
                        type: 0,
                        questionArray: tempQuestArray,
                        answer: '',
                        itemType: 0,
                    })
                    this.setState({
                        dataArray: tempArray
                    })
                }
            })
            .catch((error) => {

            })
    }

    componentWillMount() {
        this._getUserInfo()
        this._getUseHelp()
        // var tempArray = [{
        //     type: 0,//客服
        //     questionArray: ['1.问题问题问题', '2.问题问题问题', '3.问题问题问题', '4.问题问题问题', '5.问题问题问题', '6.问题问题问题'],
        //     answer: '',
        //     itemType: 0,//0问题  1回答
        // }, {
        //     type: 1,//用户
        //     question: '1.问题问题问题',
        // }, {
        //     type: 0,
        //     answer: '66666666',
        //     itemType: 1,
        // }]
        //
        // this.setState({
        //     dataArray: tempArray
        // })
    }

    _getUserInfo() {
        StorageHelper.getUserInfo().then((res) => {
            if (res && res.avatar) {
                this.setState({
                    avatarUrl: res.avatar
                })
            }
        })
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                }}>
                {this._listView()}
                {this._bottomView()}
            </View>
        );
    }

    _listView() {
        return (
            <View
                style={{
                    flex: 1
                }}>
                <ScrollView
                    ref={(ref) => {
                        this.scrollView = ref;
                    }}
                    showsVerticalScrollIndicator={false}>
                    <View style={{
                        height: 15,
                        width: 1
                    }}/>
                    {this.state.dataArray.map((item, index) => {
                        if (item.type == 0) {
                            return (this._kfItemView(item, index))
                        } else {
                            return (this._userItemView(item, index))
                        }
                    })}
                </ScrollView>
            </View>
        )
    }

    _kfItemView(item, index) {
        return (
            <View
                key={index}
                style={{
                    flexDirection: 'row',
                    paddingRight: 18,
                    marginTop: 15,
                    marginBottom: 15
                }}>

                <View
                    style={{
                        flex: 1,
                        alignItems: 'center'
                    }}>
                    <Image
                        style={{
                            width: 60,
                            height: 60,
                            resizeMode: 'contain'
                        }}
                        source={this.state.imgSmall ? {uri: this.state.imgSmall} : require('../../resources/list_dev_img.png')}/>
                </View>

                {
                    item.itemType == 0 ?
                        this._questionView(item.questionArray) : this._kfAnswerItemView(item.answer)
                }

            </View>
        )
    }

    _questionView(questionArray) {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth * 0.68,
                    padding: 20
                }}>
                {
                    questionArray && questionArray.length > 0 ?
                        questionArray.map((item1, index1) => {
                            return (this._questionItemView(item1, index1))
                        }) : null
                }
            </ShadowCardView>
        )
    }

    _questionItemView(item, index) {
        return (
            <TouchableOpacity
                key={index}
                onPress={() => {
                    this._getAnswerByQId(item.id, item.title)
                }}>
                <CommonTextView
                    text={'Q' + (index + 1) + ':' + item.title}
                    textSize={12}
                    style={{
                        color: '#000000',
                        marginTop: index != 0 ? 10 : 0,
                    }}
                />
            </TouchableOpacity>
        )
    }

    _kfAnswerItemView(item) {
        return (
            <ShadowCardView
                style={{
                    width: this.mScreenWidth * 0.68,
                    padding: 20
                }}>
                {/*<CommonTextView*/}
                {/*text={item}*/}
                {/*textSize={12}*/}
                {/*style={{*/}
                {/*color: '#000000',*/}
                {/*}}*/}
                {/*/>*/}

                <AutoHeightWebView
                    style={{width: (this.mScreenWidth * 0.68) - 40, marginTop: 0,opacity: 0.99}}
                    customScript={`document.body.style.background = '#fff';`}
                    customStyle={`
      * {
        font-family: 'DIN Alternate Bold';
      }
      p {
        font-size: 12px;
        color:black;
        font-weight:bold;
      }
    `}
                    onSizeUpdated={size => LogUtil.debugLog(size.height)}
                    files={[{
                        href: 'cssfileaddress',
                        type: 'text/css',
                        rel: 'stylesheet'
                    }]}
                    source={{html: item}}
                    scalesPageToFit={false}
                    viewportContent={'width=device-width, user-scalable=no'}
                    /*
                    other react-native-webview props
                    */
                />
            </ShadowCardView>
        )
    }

    _userItemView(item, index) {
        return (
            <View
                key={index}
                style={{
                    flexDirection: 'row',
                    paddingLeft: 18,
                    marginTop: 15,
                    marginBottom: 15
                }}>
                <ShadowCardView
                    style={{
                        width: this.mScreenWidth * 0.68,
                        padding: 20,
                        backgroundColor: '#38579D'
                    }}>
                    <CommonTextView
                        text={item.question}
                        textSize={12}
                        style={{
                            color: '#FFFFFF',
                        }}
                    />
                </ShadowCardView>

                <View
                    style={{
                        flex: 1,
                        alignItems: 'center'
                    }}>
                    <View
                        style={{
                            width: 62,
                            height: 62,
                            borderRadius: 62 / 2,
                            overflow: 'hidden',
                        }}>
                        <Image
                            style={{
                                width: 62,
                                height: 62,
                                resizeMode: 'cover',
                                borderRadius: 62 / 2
                            }}
                            source={this.state.avatarUrl ? {uri: this.state.avatarUrl} : require('../../resources/ic_display_big.png')}/>
                    </View>
                </View>
            </View>
        )
    }

    _bottomView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginBottom: 16
                }}>
                {/*{this._bottomBtnItemView(strings('device_rating'), require('../../resources/help_rating_ic.png'), () => {*/}
                {/*//设备评分*/}
                {/*//this.props.navigation.navigate('DeviceRating');*/}
                {/*this.props.navigation.push('SelectDeviceList', {type: 2});*/}
                {/*})}*/}
                {/*<View*/}
                {/*style={{*/}
                {/*width: 65,*/}
                {/*height: 1*/}
                {/*}}/>*/}
                {this._bottomBtnItemView(strings('feedback'), require('../../resources/help_feedback_ic.png'), () => {
                    //意见反馈
                    this.props.navigation.navigate('Feedback', {token: this.state.token});
                })}
            </View>
        )
    }

    _bottomBtnItemView(text, icon, onClickEvent) {
        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <ShadowCardView
                    onPress={onClickEvent}
                    style={{
                        width: 60,
                        height: 60,
                        borderRadius: 30,
                        shadowRadius: 32,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <Image
                        source={icon}/>
                </ShadowCardView>

                <CommonTextView
                    textSize={12}
                    text={text}
                    style={{
                        color: '#000000',
                        marginTop: 10
                    }}/>
            </View>
        )
    }

    //根据问题id获取帮助详情
    _getAnswerByQId(id, title) {
        if (id) {
            this._addQuest(title)
            NetUtil.getUseHelpDetail(this.state.token, id, true)
                .then((res) => {
                    var answerInfo = res.info
                    if (answerInfo && answerInfo.content) {
                        var tempArray = this.state.dataArray
                        tempArray.push({
                            type: 0,
                            answer: answerInfo.content,
                            itemType: 1,
                        })
                        this.setState({dataArray: tempArray})

                        if (this.scrollView) {
                            this.scrollView.scrollToEnd({animated: true});

                        }
                    }
                })
                .catch((error) => {

                })
        }
    }

    //添加问题数据
    _addQuest(answer) {
        if (answer) {
            var tempArray = this.state.dataArray
            tempArray.push({
                type: 1,
                question: answer,
            })
            this.setState({dataArray: tempArray})
        }
    }

}