/*
 *我的/设备管理（我的设备）
 */
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    FlatList,
    RefreshControl,
    ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from "../Net/NetUtil";
import CommonTextView from "../View/CommonTextView";
import I18n from "react-native-i18n";
import StorageHelper from "../Utils/StorageHelper";
import DeviceListUtil from "../Utils/DeviceListUtil";
import Helper from "../Utils/Helper";

class MyDevice extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            refreshing: false,
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        this.setState({refreshing: true});
        if (StorageHelper.getTempToken()) {
            DeviceListUtil._getDeviceList(
                false,
                StorageHelper.getTempToken(),
                true,
                1,
                1
            )
                .then((res) => {
                    if (res?.list?.length > 0) {
                        this.setState({
                            list: res?.list,
                            refreshing: false,
                        });
                    } else {
                        this.setState({
                            refreshing: false,
                        });
                    }
                })
                .catch((error) => {
                });
        }
    };

    renderItem = (item, index) => {
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate("DeviceShareMember", {
                        deviceData: item,
                    })
                }
                style={{
                    width: this.mScreenWidth,
                    flexDirection: "row",
                    justifyContent: "center",
                }}
            >
                <View
                    style={{
                        backgroundColor: "#fff",
                        height: 100,
                        width: this.mScreenWidth - 40,
                        flexDirection: "row",
                        alignItems: "center",
                        borderRadius: 10,
                        marginBottom: 15,
                    }}
                >
                    <Image
                        style={{marginHorizontal: 20, width: 80, height: 60}}
                        source={{uri: item?.img}}
                    />
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            marginRight: 10,
                        }}
                    >
                        <View>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <Text numberOfLines={1}>
                                    <CommonTextView
                                        textSize={15}
                                        text={item.deviceName}
                                        style={{
                                            color: "#404040",
                                            fontWeight: "bold",
                                        }}
                                    />
                                </Text>
                            </View>
                            <Text numberOfLines={1}>
                                <CommonTextView
                                    textSize={12}
                                    text={item.shareCount ? item.shareCount + strings('个共享') : strings('暂无共享')}
                                    style={{
                                        color: "#808080",
                                    }}
                                />
                            </Text>
                        </View>
                        <Image
                            source={require("../../resources/greenUnion/list_arrow_ic.png")}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    ListEmptyComponent = () => {
        return (
            <View
                style={{
                    height: this.mScreenHeight,
                    width: this.mScreenWidth,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#F1F2F6",
                }}
            >
                <Image
                    source={require("../../resources/greenUnion/share_none_ic.png")}
                />
            </View>
        );
    };

    render() {
        const {list, refreshing} = this.state;
        return (
            <View style={{width: this.mScreenWidth, flex: 1, alignItems: "center"}}>
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={this.getData}
                            tintColor="#18B34F" // 设置小图标颜色为蓝色ios
                            colors={["#18B34F"]} // 设置小图标颜色为蓝色android
                            title={strings("loading")} //iOS
                            titleColor={"#999BA2"}
                        />
                    }
                >
                    {list?.length > 0
                        ? list?.map((item, index) => this.renderItem(item, index))
                        : this.ListEmptyComponent()}
                </ScrollView>
            </View>
        );
    }
}

export default MyDevice;
