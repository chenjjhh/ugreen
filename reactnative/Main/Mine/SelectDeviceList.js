import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {
    Alert,
    AsyncStorage,
    DeviceEventEmitter,
    Dimensions,
    Image,
    Keyboard,
    Modal,
    NativeModules,
    Networking,
    Platform,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View, BackHandler, Clipboard, TextInput
} from 'react-native';

import TitleBar from '../View/TitleBar'
import {strings, setLanguage} from '../Language/I18n';
import CommonTextView from "../View/CommonTextView";
import PagerTitleView from "../View/PagerTitleView";
import DeviceCard from "../View/DeviceCard";
import ViewHelper from '../View/ViewHelper'
import DeviceListUtil from '../Utils/DeviceListUtil'
import Helper from "../Utils/Helper";
import StorageHelper from "../Utils/StorageHelper";
import NetUtil from "../Net/NetUtil";
import EventUtil from "../Event/EventUtil";


/*
*使用帮助、反馈意见、设备评分选择设备
 */

export default class SelectDeviceList extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            powerDeviceArray: [],
            cleanDeviceArray: [],
            type: props.navigation?.state?.params?.type ? props.navigation?.state?.params.type : 0,//0使用帮助、1反馈意见、2设备评分
            token: '',
            productArray: []
        }
    }

    _toNext(itemData) {
        if (this.state.type == 0) {//使用帮助详情
            this.props.navigation.navigate('UseHelpDetail', {
                deviceTypeName: itemData.name,
                token: this.state.token,
                productId: itemData.id,
                imgSmall: itemData.imgSmall
            });
        } else if (this.state.type == 1) {//反馈意见选择设备
            EventUtil.sendEvent(EventUtil.FEEDBACK_DEVICE, {deviceData: itemData})
            this.props.navigation.goBack();
        } else if (this.state.type == 2) {//设备评分详情
            this.props.navigation.navigate('DeviceRatingDetail', {data: itemData});
        }
    }

    componentWillMount() {
        //this._setTempData()
        var token = StorageHelper.getTempToken()
        this.setState({token: token})

        if (this.state.type == 0) {
            this._getProductClassList(token)
        } else {
            this._getLocalDeviceList()
            this._getDeviceList(token, false)
        }

    }

    //获取本地缓存设备列表
    _getLocalDeviceList() {
        DeviceListUtil._getLocalDeviceList()
            .then((res) => {
                var deviceInfo = res
                if (deviceInfo) {
                    this._dealDeviceInfo(deviceInfo)
                }
            })
            .catch((error) => {

            })
    }

    //获取设备列表
    _getDeviceList(token, showLoading) {
        if (token) {
            DeviceListUtil._getDeviceList(showLoading, token, true)
                .then((res) => {
                    var deviceInfo = res.info
                    if (deviceInfo) {
                        this._dealDeviceInfo(deviceInfo)
                    }
                })
                .catch((error) => {
                })
        }
    }

    _dealDeviceInfo(deviceInfo) {
        if (deviceInfo.length > 0) {
            var array = Helper._getArray3(deviceInfo)
            this.setState({
                powerDeviceArray: array,
            })
        }
    }

    _getProductClassList(token) {
        NetUtil.getProductList(token, true)
            .then((res) => {
                if (res.list && res.list.length > 0) {
                    var tempArray = []
                    var resList = res.list
                    resList.map((item, index) => {
                        if (parseInt(item.status) == 1)
                            tempArray.push(item)
                    })
                    var productArray = Helper._resolveAllProductInfo(tempArray.sort(Helper.compare('createTime')))
                    if (productArray.length > 0) {
                        this.setState({
                            productArray: productArray,
                        })
                    }
                }
            })
    }

    _setTempData() {
        var tempPowerDeviceArray = [
            [{deviceNickname: 'VOGEE XT'}, {deviceNickname: 'VOGEE XT1'}, {deviceNickname: 'VOGEE XT3'}],
            [{deviceNickname: 'VOGEE XT'}, {deviceNickname: 'VOGEE XT1'}, {deviceNickname: 'VOGEE XT3'}],
            [{deviceNickname: 'VOGEE XT4'}]
        ]
        var tempCleanDeviceArray = [
            [{deviceNickname: 'VOGEE XT'}, {deviceNickname: 'VOGEE XT1'}, {deviceNickname: 'VOGEE XT3'}],
            [{deviceNickname: 'VOGEE XT'}, {deviceNickname: 'VOGEE XT1'}, {deviceNickname: 'VOGEE XT3'}],
            [{deviceNickname: 'VOGEE XT4'}, {deviceNickname: 'VOGEE XT5'}]
        ]
        this.setState({
            powerDeviceArray: tempPowerDeviceArray,
            cleanDeviceArray: tempCleanDeviceArray,
        })
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#FBFBFB',
                }}>
                {this._titleView()}
                {this._tipsView()}
                {this.state.powerDeviceArray.length > 0 || this.state.productArray.length > 0 ? this._deviceView() : null}
            </View>
        );
    }

    _titleView() {
        return (
            <PagerTitleView
                titleText={this._getTitleText()}
            />
        )
    }

    _getTitleText() {
        return this.state.type == 0 ? strings('use_help') : this.state.type == 1 ? strings('feedback') : strings('device_rating')
    }

    _tipsView() {
        return (
            <CommonTextView
                textSize={14}
                text={this.state.powerDeviceArray.length > 0 || this.state.productArray.length > 0 ? strings('select_device_type') : strings('not_bind_device')}
                style={{
                    color: '#969AA1',
                    marginLeft: 18,
                    marginTop: 15,
                }}/>
        )
    }


    _deviceView() {
        return (
            <View
                style={{
                    width: this.mScreenWidth - 36,
                    height: this.state.bottomContainerViewHeight,
                    marginLeft: 18,
                    marginBottom: 50
                }}>
                <ScrollView
                    showsVerticalScrollIndicator={false}>
                    {
                        this.state.type == 0 ?
                            this.state.productArray.map((item, index) => {
                                return (
                                    this._deviceItemView(index, item.name, item.data, false)
                                )
                            })
                            :
                            this._deviceItemView(0, strings('power_device'), this.state.powerDeviceArray, false)
                    }
                    {/*{this._deviceItemView(strings('power_device'), this.state.powerDeviceArray, false)}*/}
                    {/*{this._deviceItemView(strings('clean_device'), this.state.cleanDeviceArray, false)}*/}
                </ScrollView>
            </View>
        )
    }

    _deviceItemView(index, title, deviceArray, showShadow) {
        return (
            <View
                key={index}>
                <CommonTextView
                    textSize={14}
                    text={title}
                    style={{
                        color: '#000000',
                        marginTop: 50,
                        marginBottom: 15
                    }}/>

                {
                    deviceArray.map((item, index) => {
                        return (
                            <View
                                key={index}
                                style={{
                                    flexDirection: 'row'
                                }}>
                                {
                                    item.map((item1, index1) => {
                                        return (
                                            <DeviceCard
                                                imageUrl={item1.imgSmall ? {uri: item1.imgSmall} : require('../../resources/add_dev_dev_img.png')}
                                                onPress={() => {
                                                    //使用帮助详情
                                                    //this.props.navigation.navigate('UseHelpDetail', {deviceTypeName: item1.deviceTypeName});
                                                    this._toNext(item1)
                                                }}
                                                style={{
                                                    marginBottom: showShadow ? 10 : 0,
                                                    backgroundColor: 'rgba(0,0,0,0)'
                                                }}
                                                key={index1}
                                                showShadow={showShadow}
                                                isNewShare={false}
                                                deviceTypeName={this.state.type === 0 ? item1.name : item1.deviceNickname}
                                                />
                                        )
                                    })
                                }
                                {
                                    item.length == 1 ?
                                        ViewHelper.getFlex2View()
                                        : item.length == 2 ? ViewHelper.getFlexView()
                                        : null
                                }
                            </View>
                        )
                    })
                }
            </View>
        )
    }

}