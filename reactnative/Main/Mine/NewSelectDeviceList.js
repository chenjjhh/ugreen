/*
 *我的/问题反馈/选择设备列表
 */
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Platform,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    FlatList,
    RefreshControl,
    ScrollView,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";
import {strings} from "../Language/I18n";
import NetUtil from "../Net/NetUtil";
import CommonTextView from "../View/CommonTextView";
import I18n from "react-native-i18n";
import CommonMessageDialog from "../View/CommonMessageDialog";
import TitleBar from "../View/TitleBar";
import CommonBtnView from "../View/CommonBtnView";
import FastImage from "react-native-fast-image";
import Helper from "../Utils/Helper";
import StorageHelper from "../Utils/StorageHelper";
import DeviceListUtil from "../Utils/DeviceListUtil";
import TextInputComponent from "../View/TextInputComponent";
import {ItemBtnView} from "./SystemPermissionsManagement";
import EventUtil from "../Event/EventUtil";
// import { Camera, useCodeScanner} from "react-native-vision-camera"

const inputScan = require("../../resources/greenUnion/input_scan_ic.png");
const titleTipsIcon = require("../../resources/greenUnion/tips_ic.png");

// const codeScanner = useCodeScanner({
//   codeTypes: ['qr', 'ean-13'],
//   onCodeScanned: (codes) => {
//     console.log(`Scanned ${codes} codes!`)
//   }
// })

export default class NewSelectDeviceList extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("选择设备")}
                    backgroundColor={"#F1F2F6"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            list: [
                // {
                //   createTime: 1702610886420,
                //   deviceId: "895464843183857664",
                //   deviceName: "CJH",
                //   deviceSn: "G06BC0223050207",
                //   img: "https://oss.cstest.granwin.com/image/UGREEN_GS600.png",
                //   mac: "G06FC2303212585",
                //   onlineStatus: false,
                //   productKey: "hodm7VI5UpX",
                //   protocol: 2,
                //   type: 2,
                // },
            ],
            lang: I18n.locale === "zh" ? "zh_CN" : "english",
            selectDeviceId: "", //当前选择设备id
            deviceSn: "", //SN码
            token: StorageHelper.getTempToken() || "",
            deviceData: {}, //选中设备
        };
    }

    componentDidMount() {
        const {token} = this.state;
        this._getDeviceList(token, false);
    }

    //获取设备列表
    _getDeviceList(token, showLoading) {
        if (token) {
            DeviceListUtil._getDeviceList(showLoading, token, true)
                .then((res) => {
                    this.setState({
                        list: res?.list || [],
                    });
                })
                .catch((error) => {
                });
        }
    }

    // 更新输入框值
    onChangeText = (type, value) => {
        this.setState({
            [type]: value,
        });
    };

    selectDevice = (data, index) => {
        const {deviceData} = this.state;
        this.setState({
            selectDeviceId: data?.deviceId,
            // deviceData: {...deviceData, ...data},
            deviceData: data,
        });
    };

    // 输入设备S/N提示按钮
    titleTipsIcOnClick = () => {
        // this.onShowToast(strings(''))
    };

    takeVideoStart = () => {
        // Camera.current.startRecording({
        //   flash: "off",
        //   onRecordingFinished: (video) => console.log(video),
        //   onRecordingError: (error) => console.error(error),
        // });
    };

    renderItem = (item, index) => {
        const {selectDeviceId} = this.state;
        return (
            <TouchableOpacity
                key={index}
                onPress={() => {
                    // 选择我的设备
                    this.selectDevice(item, index);
                }}
                style={{
                    width: this.mScreenWidth,
                    flexDirection: "row",
                    justifyContent: "center",
                }}
            >
                <View
                    style={{
                        backgroundColor: "#fff",
                        height: 100,
                        width: this.mScreenWidth - 40,
                        flexDirection: "row",
                        alignItems: "center",
                        borderRadius: 10,
                        marginBottom: 15,
                    }}
                >
                    <View
                        style={{
                            height: 49,
                            width: 62,
                            borderRadius: 10,
                            overflow: "hidden",
                            marginHorizontal: 20,
                        }}
                    >
                        <FastImage
                            style={{height: 49, width: 62}}
                            source={{
                                uri: item?.img,
                                priority: FastImage.priority.normal,
                            }}
                            resizeMode={FastImage.resizeMode.cover}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "space-between",
                            marginRight: 10,
                        }}
                    >
                        <View>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <Text numberOfLines={1}>
                                    <CommonTextView
                                        textSize={15}
                                        text={item.deviceName}
                                        style={{
                                            color: "#404040",
                                            fontWeight: "bold",
                                        }}
                                    />
                                </Text>
                            </View>
                            <Text numberOfLines={1}>
                                <CommonTextView
                                    textSize={13}
                                    text={item.deviceType}
                                    style={{
                                        color: "#808080",
                                        lineHeight: 25,
                                    }}
                                />
                            </Text>
                        </View>
                        <View style={{paddingHorizontal: 10}}>
                            <Image
                                style={{width: 22, height: 22}}
                                source={
                                    selectDeviceId === item.deviceId
                                        ? require("../../resources/greenUnion/sel_ic.png")
                                        : require("../../resources/greenUnion/unsel_ic.png")
                                }
                            />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };

    render() {
        const {list, deviceSn, deviceData} = this.state;
        const _comfireBtnView = (
            <View style={{marginBottom: 15}}>
                <CommonBtnView
                    clickAble={true}
                    onPress={() => {
                        // 确定
                        EventUtil.sendEvent(EventUtil.FEEDBACK_DEVICE, {deviceData});
                        this.props.navigation.goBack();
                    }}
                    style={{
                        height: 54,
                        backgroundColor: "#18B34F",
                    }}
                    btnText={strings("comfire")}
                />
            </View>
        );

        const _ListEmptyComponent = (
            <View
                style={{
                    height: 230,
                    width: this.mScreenWidth,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#F1F2F6",
                }}
            >
                <Image
                    style={{width: 60, height: 60}}
                    source={require("../../resources/greenUnion/warning_ic.png")}
                />
                <CommonTextView
                    textSize={13}
                    text={strings("暂未添加设备")}
                    style={{
                        color: "#808080",
                        lineHeight: 25,
                    }}
                />
            </View>
        );
        const _inputView = (
            <View style={{width: this.mScreenWidth, alignItems: "center"}}>
                <TextInputComponent
                    title={strings("设备SN输入提示")}
                    titleTipsIcon={titleTipsIcon}
                    placeholder={strings("请输入编码")}
                    onChangeText={this.onChangeText}
                    value={deviceSn}
                    type={"deviceSn"}
                    rightIcon={inputScan}
                    titleTipsIcOnClick={this.titleTipsIcOnClick}
                    rightIconClick={this.takeVideoStart}
                />
            </View>
        );

        const _selectDeviceView = (
            <ItemBtnView
                title={strings("选择产品")}
                content={strings("选择产品型号提示")}
                style={{minHeight: 80}}
                onPress={() => {
                    this.props.navigation.push("SelectProductListView", {
                        selectDevice: this.selectDevice,
                        pageKey: this.props.navigation?.state?.params?.pageKey
                    });
                }}
            />
        );
        return (
            <View
                style={{
                    width: this.mScreenWidth,
                    flex: 1,
                    alignItems: "center",
                    backgroundColor: "#F1F2F6",
                }}
            >
                <View
                    style={{
                        width: this.mScreenWidth,
                        alignItems: "flex-start",
                        backgroundColor: "#F1F2F6",
                        paddingHorizontal: 35,
                        marginVertical: 10,
                    }}
                >
                    <Text style={{fontSize: this.getSize(14), color: "#404040"}}>
                        {strings("我的设备")}
                    </Text>
                </View>
                <View style={{flex: 1}}>
                    <View style={{height: (list?.length <= 2 && list?.length !== 0) ? list?.length * 115 : 230}}>
                        <ScrollView style={{flex: 1}}>
                            {list?.length > 0
                                ? list?.map((item, index) => this.renderItem(item, index))
                                : _ListEmptyComponent}
                        </ScrollView>
                    </View>
                    {_inputView}
                </View>
                {_selectDeviceView}
                {_comfireBtnView}
                {/* <Camera ref={(ref)=>{this.Camera = ref}} {...this.props} codeScanner={codeScanner} /> */}
            </View>
        );
    }
}
