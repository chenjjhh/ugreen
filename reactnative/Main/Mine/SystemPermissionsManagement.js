// 我的/个人信息保护/系统权限管理
import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Linking,
    Dimensions,
    KeyboardAvoidingView,
    StatusBar,
    Switch,
    Platform,
    TouchableOpacity,
    TextInput,
    Modal,
    PixelRatio,
    PermissionsAndroid, NativeModules,
} from "react-native";
import BaseComponent from "../Base/BaseComponent";

import {strings, setLanguage} from "../Language/I18n";
import TitleBar from "../View/TitleBar";
import {
    check,
    checkMultiple,
    PERMISSIONS,
    request,
} from "react-native-permissions";

const rightIcon = require("../../resources/greenUnion/list_arrow_ic.png");
export default class SystemPermissionsManagement extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header: (
                <TitleBar
                    titleText={strings("系统权限管理")}
                    backgroundColor={"#FBFBFB"}
                    leftIcon={require("../../resources/back_black_ic.png")}
                    leftIconClick={() => {
                        navigation.goBack();
                    }}
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            //  0、已禁止  1、已允许  2、未开启
            permissions: [
                {
                    isAllow: "",
                    title: strings("访问蓝牙权限"),
                    content: strings("访问蓝牙提示"),
                    type:
                        Platform.OS === "ios"
                            ? PERMISSIONS.IOS.BLUETOOTH
                            : PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
                }, //蓝牙
                {
                    isAllow: "",
                    title: strings("访问地理位置"),
                    content: strings("访问地理位置提示"),
                    type:
                        Platform.OS === "ios"
                            ? PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
                            : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                }, //地理位置权限
                {
                    isAllow: "",
                    title: strings("访问设备存储"),
                    content: strings("访问设备存储提示"),
                    type:
                        Platform.OS === "ios"
                            ? PERMISSIONS.IOS.PHOTO_LIBRARY
                            : PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
                }, //设备存储权限
                {
                    isAllow: "",
                    title: strings("访问相机权限"),
                    content: strings("访问相机权限提示"),
                    type:
                        Platform.OS === "ios"
                            ? PERMISSIONS.IOS.CAMERA
                            : PERMISSIONS.ANDROID.CAMERA,
                }, //相机权限
            ],
        };
    }

    componentDidMount() {
        this.setDefaultPermissions();
    }

    // 格式化权限数据
    assignPermissions = (defaultObj, permissions, isDefault) => {
        permissions.forEach((permission) => {
            if (defaultObj.hasOwnProperty(permission.type)) {
                // 'denied'(禁止 + 禁止后不再提示) | 'granted'(允许) | 'unavailable'(不支持)
                if (defaultObj[permission.type] === "granted") {
                    permission.isAllow = 1;
                } else if (defaultObj[permission.type] === "denied") {
                    permission.isAllow = isDefault ? 2 : 0;
                } else if (defaultObj[permission.type] === "unavailable") {
                    permission.isAllow = 3;
                } else {
                    permission.isAllow = 2;
                }
            }
        });
        return permissions;
    };

    setDefaultPermissions = async () => {
        const {permissions} = this.state;
        //permissionsKeys:["android.permission.BLUETOOTH_SCAN", "android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"]
        let permissionsKeys = permissions?.map(({type}) => type);
        //checkResult: {"android.permission.ACCESS_FINE_LOCATION": "granted", "android.permission.BLUETOOTH_SCAN": "unavailable", "android.permission.CAMERA": "granted", "android.permission.READ_EXTERNAL_STORAGE": "granted"}
        try {
            const checkResult = await checkMultiple(permissionsKeys);
            console.log('checkResult: ', checkResult);
            let newPermissions = this.assignPermissions(
                checkResult,
                [...permissions],
                true
            );
            this.setState({
                permissions: newPermissions,
            });
        } catch (error) {
            console.log("error----", error);
        }
    };

    openSystemSettings = () => {
        if (Platform.OS === "ios") {
            const url = 'app-settings:'
            Linking.canOpenURL(url)
                .then((supported) => {
                    if (!supported) {
                        console.log('Can\'t handle url: ' + url);
                    } else {
                        return Linking.openURL(url)
                    }
                })
        } else {
            NativeModules.RNModule.toAppSetting()
        }
    };


    //  选择权限
    checkPermission = async (permission) => {
        const {permissions} = this.state;
        try {
            const checkResult = await check(permission);
            // checkResult: 'denied'(禁止 + 禁止后不再提示) | 'granted'(允许) | 'unavailable'(不支持)
            // 如果上一次禁止时选择不再提示或者已允许，则不执行下面代码
            if (checkResult === "granted" || checkResult === "unavailable") {
                // 调用该函数进行跳转到系统设置页面
                this.openSystemSettings();
                return;
            }

            const requestResult = await request(permission);
            // requestResult: 'blocked'(禁止后不再提示) | 'denied'(禁止) | 'granted'(允许)
            let newPermissions = permissions?.map((item, index) => {
                // 已允许
                if (permission === item?.type) {
                    if (requestResult === "granted") {
                        item.isAllow = 1;
                        // 已禁止
                    } else if (
                        requestResult === "denied" ||
                        requestResult === "blocked"
                    ) {
                        item.isAllow = 0;
                    } else {
                        // 默认情况：显示空  点击对应项：显示'去开启'
                        item.isAllow = 2;
                    }
                }
                return item;
            });
            this.setState({
                permissions: newPermissions,
            });
        } catch (error) {
            console.log("error----", error);
        }
    };

    //   以下代码逻辑为PermissionsAndroid插件的，保留
    //   setDefaultPermissions = () => {
    //     const { permissions } = this.state;
    //     try {
    //       permissions?.forEach((item, index) => {
    //         this.checkPermission(item?.type, true);
    //       });
    //     } catch (error) {
    //       console.log(error);
    //     }
    //   };

    //   checkPermission = async (permission, isDefault) => {
    //     // 蓝牙：PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN
    //     // 地理位置：PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    //     // 设备存储：PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
    //     // 相机：PermissionsAndroid.PERMISSIONS.CAMERA
    //     const { permissions } = this.state;
    //     try {
    //       // checkResult true(允许) /false(禁止 + 禁止后不再提示)
    //       const checkResult = await PermissionsAndroid.check(permission);
    //       // requestResult: 'granted'(允许) | 'denied'(禁止) | 'never_ask_again'(禁止 + 禁止后不再提示)
    //       const requestResult = await PermissionsAndroid.request(permission);
    //       // 如果上一次禁止时选择不再提示，则不执行下面代码
    //       if (
    //         (checkResult ||
    //           requestResult === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) &&
    //         !isDefault
    //       ) {
    //         return;
    //       }
    //       let newPermissions = permissions?.map((item, index) => {
    //         // 已允许
    //         if (permission === item?.type) {
    //           if (requestResult === PermissionsAndroid.RESULTS.GRANTED) {
    //             item.isAllow = 1;
    //             // 已禁止
    //           } else if (
    //             requestResult === PermissionsAndroid.RESULTS.DENIED ||
    //             requestResult === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN
    //           ) {
    //             item.isAllow = 0;
    //           } else {
    //             // 默认情况：显示空  点击对应项：显示'去开启'
    //             item.isAllow = !isDefault ? 2 : "";
    //           }
    //         }
    //         return item;
    //       });
    //       this.setState({
    //         permissions: newPermissions,
    //       });
    //     } catch (error) {
    //       console.log("error----", error);
    //     }
    //   };

    render() {
        const {permissions} = this.state;
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: "#F7F7F7",
                    alignItems: "center",
                    width: this.mScreenWidth,
                }}
            >
                {permissions?.map((item, index) => (
                    <ItemBtnView
                        key={index}
                        title={item?.title}
                        content={item?.content}
                        isAllow={item?.isAllow}
                        onPress={() => {
                            this.checkPermission(item?.type);
                        }}
                    />
                ))}
            </View>
        );
    }
}

export class ItemBtnView extends BaseComponent {
    render() {
        const {title, content, onPress, isAllow, style} = this.props;
        return (
            <TouchableOpacity
                onPress={() => {
                    onPress && onPress();
                }}
                style={[
                    {
                        flexDirection: "row",
                        width: this.mScreenWidth - 24,
                        minHeight: 100,
                        justifyContent: "space-between",
                        alignItems: "center",
                        borderRadius: 10,
                        paddingVertical: 10,
                        paddingHorizontal: 20,
                        marginBottom: 13,
                        backgroundColor: "#ffffff",
                    },
                    style,
                ]}
            >
                <View style={{flex: 1, paddingRight: 20}}>
                    <Text
                        style={{fontSize: this.getSize(16), color: "#404040"}}
                        numberOfLines={1}
                    >
                        {title}
                    </Text>
                    <Text
                        style={{fontSize: this.getSize(13), color: "#808080"}}
                        numberOfLines={2}
                    >
                        {content}
                    </Text>
                </View>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Text
                        style={{
                            color:
                                isAllow === 0 || isAllow === 3
                                    ? "#E16262"
                                    : isAllow === 1
                                    ? "#808080"
                                    : "#18B34F",
                        }}
                    >
                        {isAllow === 0
                            ? strings("已禁止")
                            : isAllow === 1
                                ? strings("已允许")
                                : isAllow === 2
                                    ? strings("去开启")
                                    : isAllow === 3
                                        ? strings("不支持")
                                        : ""}
                    </Text>
                    <Image
                        style={[
                            {
                                resizeMode: "contain",
                                marginLeft: 5,
                            },
                        ]}
                        source={rightIcon}
                    />
                </View>
            </TouchableOpacity>
        );
    }
}
