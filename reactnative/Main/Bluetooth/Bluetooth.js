/**
 *
 * 蓝牙连接**/

import {BleManager, State} from 'react-native-ble-plx';
import {Buffer} from 'buffer';
import {Platform} from "react-native";
import LogUtil from "../Utils/LogUtil";

global.isConnected = false
global.isBleOpen = false

let isOnlyOne = false

module.exports = {
    Init() {
        this.manager = new BleManager();
        this._bleLog('蓝牙已初始化')
        this.NoticeStateChange()
    },

    //打开蓝牙
    _enableBluetooth(){
        if(this.manager){
        }else {
            this.manager = new BleManager();
        }
        this.manager.enable();
    },

    /**
     * 蓝牙状态监听*/
    NoticeStateChange() {
        this.manager.onStateChange((state) => {
            this._bleLog('蓝牙状态===', state)
            if (state === State.PoweredOff) {
                global.isConnected = false
                global.isBleOpen = false
                alert('Bluetooth is turned off. Please turn on Bluetooth and proceed to the next step');

                if (Platform.OS === 'android') {
                    const enable = this.manager.enable();
                    this._bleLog(this.manager.state());
                } else {
                    // ios不能直接打开，用对话框提示打开蓝牙
                }
            }
            if (state === State.PoweredOn) {
                global.isBleOpen = true
            }
        }, true);
    },

    /**
     * 蓝牙搜索-----默认 5 秒, 如果传入设备名称，就返回所要搜索的设备,如果不传则默认 5秒后 返回搜索得到的列表*/
    SearchBle(deviceName, successCallback, errorCallback, stopCallback, seconds = 5000,) {
        this.timer && clearTimeout(this.timer)
        this._bleLog('开始搜索=====>>>>>')
        let device_list = []
        this.manager.startDeviceScan(null, null, (error, device) => {
            if (error) {
                // Handle error (scanning will be stopped automatically)
                errorCallback(error)
                return
            }
            if (deviceName && device.localName) {
                // if (device.name === deviceName && device.serviceUUIDs !== null) {
                if (device.localName.toString().indexOf(deviceName) != -1 || device.name.toString().indexOf(deviceName) != -1) {
                    // Stop scanning as it's not necessary if you are scanning for one device.
                    device_list.push(device)
                    successCallback(device_list)
                }
            }
            // else {
            //     device_list.push(device)
            //     successCallback(device_list)
            // }
            this._bleLog('******扫描到的设备===' + device.id + '======localName:' + device.localName + '======name:' + device.name)
        });
        // if (global.isBleOpen) {
        //     this.timer = setTimeout(() => {
        //         this._bleLog('扫描结束====停止扫描',)
        //         successCallback(device_list)
        //         this.manager.stopDeviceScan();
        //     }, seconds)
        // }


        // this.timer && clearTimeout(this.timer);
        // this.timer = setTimeout(() => {
        //     this._bleLog('扫描结束====停止扫描',)
        //     this.StopSearchBle();
        //     stopCallback()
        // }, seconds)
    },


    ConnectBle(macId, successCallback, errorCallback) {
        if (global.isConnected) {
            alert('Only one Bluetooth can be connected ')
            errorCallback('device is already connected')
        } else {
            this.manager.connectToDevice(macId, {autoConnect: false}).then((device) => {
                this._bleLog('connect success===', JSON.stringify(device))
                global.isConnected = true
                // 查找设备的所有服务、特征和描述符。
                this.manager.discoverAllServicesAndCharacteristicsForDevice(device.id).then((data) => {
                    // 如果所有可用的服务和特征已经被发现，它会返回能用的Device对象。
                    this._bleLog('可用的服务和特征已经被发现，它会返回能用的Device对象: ', device.id)
                    this.GetServiceId(device, successCallback, errorCallback)
                }, err => this._bleLog('可用的服务和特征已经被发现 fail : ', err))
            }).catch((err) => {
                this._bleLog('connect fail===', err)
                errorCallback(err)
            })
        }
    },

    //获取蓝牙设备的服务uuid,5.0    //服务uuid可能有多个
    GetServiceId(device, successCallback, errorCallback) {
        this.manager.servicesForDevice(device.id).then((data) => {
            data.map((item,index)=>{
                this._bleLog(index+"****servicesForDevice===>"+JSON.stringify(item))
            })
            // 为设备发现的服务id对象数组
            this.mac_id = device.id
            let server_uuid = data[2].uuid
            this._bleLog('设备发现的服务id对象数组: ', device.id + '===' + server_uuid)
            this.GetCharacterIdNotify(server_uuid, successCallback, errorCallback)
        }, err => this._bleLog('服务id对象数组 fail===', err))
    },

    // 根据服务uuid获取蓝牙特征值,开始监听写入和接收
    GetCharacterIdNotify(server_uuid, successCallback, errorCallback) {
        this.manager.characteristicsForDevice(this.mac_id, server_uuid).then((data) => {
            this.writeId = data[0].serviceUUID //写入id
            this.notifyId = data[0].uuid //接收id
            this._bleLog('characteristics list: ', this.writeId + '====' + this.notifyId)
            this.StartNoticeBle()
            this.onDisconnect()
            successCallback(this.mac_id, this.writeId, this.notifyId)
        }, err => {
            this._bleLog('characteristics list fail:', err);
            errorCallback(err)
        })
    },

    // 开启蓝牙监听功能
    StartNoticeBle() {
        this._bleLog('开始数据接收监听', this.mac_id, this.writeId, this.notifyId)
        this.manager.monitorCharacteristicForDevice(this.mac_id, this.writeId, this.notifyId, (error, characteristic) => {
            if (error) {
                this._bleLog('ble response hex data fail：', error)
                global.isConnected = false
                this.DisconnectBle()
            } else {
                let resData = Buffer.from(characteristic.value, 'base64').toString('hex')
                this._bleLog('ble response hex data:', resData);
                this.responseData = resData
            }
        }, 'monitor')
    },


    //  三、 设备返回的数据接收
    BleWrite(value, successCallback, errorCallback) {
        this.responseData = ''
        this.recivetimer && clearInterval(this.recivetimer)
        if (!global.isConnected) {
            alert(' Bluetooth not connected ')
            return
        }
        let formatValue = Buffer.from(value, 'hex').toString('base64');
        this.manager.writeCharacteristicWithResponseForDevice(this.mac_id, this.writeId,
            this.notifyId, formatValue, 'write')
            .then(characteristic => {
                let resData = Buffer.from(characteristic.value, 'base64').toString('hex')
                this._bleLog('write success', resData);
                this.recivetimer = setInterval(() => {
                    if (this.responseData) {
                        // this._bleLog('while do ===',this.responseData);
                        successCallback(this.responseData)
                        this.recivetimer && clearInterval(this.recivetimer)
                    }
                }, 500)
            }, error => {
                this._bleLog('write fail: ', error);
                errorCallback(error)
                // alert('write fail: ', error.reason);
            })
    },

    //只需向设备下发指令，无需接收
    BleWrite1(value, successCallback, errorCallback) {
        if (!global.isConnected) {
            if (!isOnlyOne) {
                isOnlyOne = true
                alert(' Bluetooth not connected ')
            }
            return
        }
        let formatValue = Buffer.from(value, 'hex').toString('base64');
        this.manager.writeCharacteristicWithoutResponseForDevice(this.mac_id, this.writeId,
            this.notifyId, formatValue, 'write')
            .then(characteristic => {
                let resData = Buffer.from(characteristic.value, 'base64').toString('hex')
                this._bleLog('write success1', resData);
            }, error => {
                this._bleLog('write fail: ', error);
                errorCallback(error)
                // alert('write fail: ', error.reason);
            })
    },

    // write(value,index){
    //     let formatValue;
    //     if(value === '0D0A') {  //直接发送小票打印机的结束标志
    //         formatValue = value;
    //     }else {  //发送内容，转换成base64编码
    //         // formatValue = new Buffer(value, "base64").toString('ascii');
    //         formatValue = Buffer.from(value, 'hex').toString('base64');
    //     }
    //     let transactionId = 'write';
    //     return new Promise( (resolve, reject) =>{
    //         this.manager.writeCharacteristicWithResponseForDevice(this.mac_id,this.writeId,
    //             this.notifyId,formatValue,transactionId)
    //             .then(characteristic=>{
    //                 this._bleLog('write success',value);
    //                 resolve(characteristic);
    //             },error=>{
    //                 this._bleLog('write fail: ',error);
    //                 // this.alert('write fail: ',error.reason);
    //                 reject(error);
    //             })
    //     });
    // },

    //手动停止搜索----在搜索里面，可以自己修改
    StopSearchBle() {
        this.timer && clearTimeout(this.timer);
        this.manager.stopDeviceScan();
        this._bleLog('终止设备扫描')
    },

    // 关闭蓝牙连接
    DisconnectBle() {
        if (!this.mac_id) {
            return
        }
        this.manager.cancelDeviceConnection(this.mac_id)
            .then(res => {
                LogUtil.debugLog('disconnect success', res);
                global.isConnected = false
            })
            .catch(err => {
                LogUtil.debugLog('disconnect fail', err);
                alert(' Bluetooth disconnection failed :', err)
            })
    },

    //关闭蓝牙模块
    destroy() {
        global.isConnected = false
        this.manager.destroy();
    },

    //监听蓝牙断开
    onDisconnect() {
        this.manager.onDeviceDisconnected(this.mac_id, (error, device) => {
            if (error) {  //蓝牙遇到错误自动断开
                this._bleLog('onDeviceDisconnected', 'device disconnect', error);
            } else {
                this._bleLog('蓝牙连接状态', device)
            }
        })
    },

    /**==========工具函数=========**/
    // 指令生成
    order(instruction, data) { //传入指令和内容
        // let instruction = '02',data = '00'
        let length = (instruction + data).length / 2
        let hexLength = this.ten2Hex(length) //十六进制长度
        // this._bleLog('lenth==',length,hexLength)

        let str = instruction + data //命令字与数据包字节
        let id = 0
        let sum = 0
        for (let i = 0; i < str.length / 2; i++) {
            id += 2
            let hexstr = str.slice(id - 2, id) //将数据拆分，将其转成10进制累加
            let intstr = this.hex2int(hexstr) //十进制
            sum += intstr
        }

        let checkstr = String(sum)
        let check = checkstr.slice(checkstr.length - 2, checkstr.length) //取得累加和后两位数后，转成16进制校验码
        let hexcheck = this.ten2Hex(check)


        // this._bleLog('最终和为==',sum,check,hexcheck)
        let order = '550000' + '00' + hexLength + instruction + data + hexcheck +
            'AA' //     '00' 与 hexLength 共两个字节， hexLength 最大为ff,即长度暂时不要超过255,若是需要长度超过255的需要判断16进制的数据是否需要自动进位,本项目不需要再多做处理
        return order
    },


    //字符转换成16进制发送到服务器
    Char2Hex(str) {
        if (str === "") {
            return "";
        } else {
            var hexCharCode = '';
            for (var i = 0; i < str.length; i++) {
                hexCharCode += (str.charCodeAt(i)).toString(16);
            }
            return hexCharCode //  tuh:  747568
        }
    },

    //字符转换成16进制发送到服务器[转换放到新数组]
    Char2Hex2(str) {
        if (str === "") {
            return "";
        } else {
            var hexCharCode = [];
            for (var i = 0; i < str.length; i++) {
                hexCharCode.push('0x' + (str.charCodeAt(i)).toString(16));
            }
            hexCharCode.join(",");
            return hexCharCode //tuh:  ["0x74", "0x75", "0x68"]
        }
    },


    // ArrayBuffer转16进度字符串示例
    ab2hex(buffer) {
        const hexArr = Array.prototype.map.call(
            new Uint8Array(buffer),
            function (bit) {
                return ('00' + bit.toString(16)).slice(-2)
            }
        )
        return hexArr.join('')
    },

    // 16进制转buffer
    hexStringToArrayBuffer(str) {
        if (!str) {
            return new ArrayBuffer(0);
        }
        var buffer = new ArrayBuffer(str.length);
        let dataView = new DataView(buffer)
        let ind = 0;
        for (var i = 0, len = str.length; i < len; i += 2) {
            let code = parseInt(str.substr(i, 2), 16)
            dataView.setUint8(ind, code)
            ind++
        }
        return buffer;
    },

    bytesToHexString(arrBytes) {
        var str = "";
        for (var i = 0; i < arrBytes.length; i++) {
            var tmp;
            var num=arrBytes[i];
            if (num < 0) {
                //此处填坑，当byte因为符合位导致数值为负时候，需要对数据进行处理
                tmp =(255+num+1).toString(16);
            } else {
                tmp = num.toString(16);
            }
            if (tmp.length == 1) {
                tmp = "0" + tmp;
            }
            str += tmp;
        }
        return str;
    },

    // 10进制转16进制
    ten2Hex(number) {
        return Number(number) < 16 ? '0' + Number(number).toString(16) : Number(number).toString(16)
    },

    // 16进制转10进制整数
    hex2int(hex) {
        var len = hex.length,
            a = new Array(len),
            code;
        for (var i = 0; i < len; i++) {
            code = hex.charCodeAt(i);
            if (48 <= code && code < 58) {
                code -= 48;
            } else {
                code = (code & 0xdf) - 65 + 10;
            }
            a[i] = code;
        }

        return a.reduce(function (acc, c) {
            acc = 16 * acc + c;
            return acc;
        }, 0);
    },

    //16进制转10进制浮点数
    hex2Float(t) {

        t = t.replace(/\s+/g, "");
        if (t == "") {
            return "";
        }
        if (t == "00000000") {
            return "0";
        }
        if ((t.length > 8) || (isNaN(parseInt(t, 16)))) {
            return "Error";
        }
        if (t.length < 8) {
            t = this.FillString(t, "0", 8, true);
        }
        t = parseInt(t, 16).toString(2);
        t = this.FillString(t, "0", 32, true);
        var s = t.substring(0, 1);
        var e = t.substring(1, 9);
        var m = t.substring(9);
        e = parseInt(e, 2) - 127;
        m = "1" + m;
        if (e >= 0) {
            m = m.substr(0, e + 1) + "." + m.substring(e + 1)
        } else {
            m = "0." + this.FillString(m, "0", m.length - e - 1, true)
        }
        if (m.indexOf(".") == -1) {
            m = m + ".0";
        }
        var a = m.split(".");
        var mi = parseInt(a[0], 2);
        var mf = 0;
        for (var i = 0; i < a[1].length; i++) {
            mf += parseFloat(a[1].charAt(i)) * Math.pow(2, -(i + 1));
        }
        m = parseInt(mi) + parseFloat(mf);
        if (s == 1) {
            m = 0 - m;
        }
        return m;
    },

    //浮点数转16进制
    float2Hex(t) {
        if (t == "") {
            return "";
        }
        t = parseFloat(t);
        if (isNaN(t) == true) {
            return "Error";
        }
        if (t == 0) {
            return "00000000";
        }
        var s,
            e,
            m;
        if (t > 0) {
            s = 0;
        } else {
            s = 1;
            t = 0 - t;
        }
        m = t.toString(2);
        if (m >= 1) {
            if (m.indexOf(".") == -1) {
                m = m + ".0";
            }
            e = m.indexOf(".") - 1;
        } else {
            e = 1 - m.indexOf("1");
        }
        if (e >= 0) {
            m = m.replace(".", "");
        } else {
            m = m.substring(m.indexOf("1"));
        }
        if (m.length > 24) {
            m = m.substr(0, 24);
        } else {
            m = this.FillString(m, "0", 24, false)
        }
        m = m.substring(1);
        e = (e + 127).toString(2);
        e = this.FillString(e, "0", 8, true);
        var r = parseInt(s + e + m, 2).toString(16);
        r = this.FillString(r, "0", 8, true);
        return this.InsertString(r, " ", 2).toUpperCase();
    },

    //需要用到的函数
    InsertString(t, c, n) {
        var r = new Array();
        for (var i = 0; i * 2 < t.length; i++) {
            r.push(t.substr(i * 2, n));
        }
        return r.join(c);
    },
    //需要用到的函数
    FillString(t, c, n, b) {
        if ((t == "") || (c.length != 1) || (n <= t.length)) {
            return t;
        }
        var l = t.length;
        for (var i = 0; i < n - l; i++) {
            if (b == true) {
                t = c + t;
            } else {
                t += c;
            }
        }
        return t;
    },

    _bleLog(log) {
        LogUtil.debugLog('ble_log====>' + log)
    },

    //是否负数
    _isNegativeValue(specValue) {
        var isNegativeValue = false
        var binData = this._addZero(parseInt(specValue.toString(), 16).toString(2), 16)
        var binDataArray = binData.split('')
        if (binDataArray.length > 0) {
            //低位为1，负数
            isNegativeValue = parseInt(binDataArray[0]) == 1
        }

        return isNegativeValue
    },

    //上报数据为负数处理
    _getNegativeValue(hexValue) {
        var binData = this._addZero(parseInt(hexValue.toString(), 16).toString(2), 16)
        var binDataArray = binData.split('')
        var value = ''
        binDataArray.map((item, index) => {
            if (index == 0) {
                value += '0'
            } else {
                value += item
            }
        })
        return -parseInt(value, 2)
    },

    //功能点数值(十进制)为负数处理
    _getSpecNegativeValue(value) {
        var hexSpecValue = ''
        //取负数的绝对值，转为整数，再转2进制
        hexSpecValue = this._addZero(
            parseInt(this._addZero4(Math.abs(value).toString(16)), 16).toString(2),
            16
        )
        //hexSpecValue转2进制，低位改为1，转十进制，再转回16进制
        hexSpecValue = this._addZero4(parseInt(this._replaceChat(hexSpecValue, 0, 1), 2).toString(16))
        return hexSpecValue
    },

    //数据下发，单功能点转16进制
    _singleSpec2Hex(specId, specValue) {
        var hexSpecNums = '0001'
        var hexSpecId = this._addZero4(Number(specId).toString(16))
        var hexSpecValue = this._addZero4(Number(specValue).toString(16))
        if (parseInt(specValue) < 0) {
            //下发负值
            hexSpecValue = this._getSpecNegativeValue(specValue)
        }
        return hexSpecNums + hexSpecId + hexSpecValue
    },

    //数据下发，多功能点转16进制
    _mulSpec2Hex(specsArray) {
        var hexStr = ''
        var specNums = this._addZero4(specsArray.length)
        hexStr += specNums
        specsArray.map((item, index) => {
            if (item.specId) {
                var hexSpecId = this._addZero4(Number(item.specId).toString(16))
                var hexSpecValue = this._addZero4(Number(item.specValue).toString(16))
                if (parseInt(item.specValue) < 0) {
                    //下发负值
                    hexSpecValue = this._getSpecNegativeValue(item.specValue)
                }
                var singleSpecHex = hexSpecId + hexSpecValue
                hexStr += singleSpecHex
            }
        })
        return hexStr
    },

    //解析上报数据，将16进制解析为json格式
    _hex2Json(hexStr) {
        if (hexStr == '' || hexStr == undefined || hexStr.length < 4) {
            return null
        }

        var hexStrArray = [];
        for (var i = 0; i < hexStr.length; i += 4) {
            hexStrArray.push(hexStr.slice(i, i + 4));
        }
        var object = {}
        if (hexStrArray.length > 0) {
            var specNums = this._hex2Dec(hexStrArray[0])
            for (var j = 1; j < (specNums * 2) + 1; j += 2) {
                var key = this._hex2Dec(hexStrArray[j])
                var value = this._hex2Dec(hexStrArray[j + 1])
                if (this._isMulSpec(key)) {
                    //位功能点，需要另外处理
                    var binData = this._addZero(parseInt(value, 10).toString(2), 16)
                    var binDataArray = binData.split('')
                    binDataArray.reverse()
                    binDataArray.map((item, index) => {
                        var byteKey = key + '_' + (index + 1)
                        var byteValue = parseInt(item) == 1
                        object[byteKey] = byteValue
                    })
                } else {
                    if (this._isNegativeValue(hexStrArray[j + 1])) {
                        //是负数，需要另外处理
                        object[key] = this._getNegativeValue(hexStrArray[j + 1])
                    } else {
                        object[key] = value
                    }
                }
            }
        }
        return object
    },

    //是否位功能点
    _isMulSpec(specId) {
        var isMulSpec = false
        var mulSpecs = ['168', '170', '171', '172', '174', '176']
        // mulSpecs.map((item, index) => {
        //     if (specId == item) {
        //         isMulSpec = true
        //     }
        // })
        if (mulSpecs.indexOf(specId.toString()) != -1) {
            isMulSpec = true
        }
        return isMulSpec
    },

    _addZero4(string) {//补齐0  补齐4位
        var temp = string
        if (string.toString().length != 4)
            for (var i = 0; i < 4 - string.toString().length; i++) {
                temp = '0' + temp
            }

        return temp
    },

    _addZero(string, length) {//补齐0  补齐n位
        var temp = string
        if (string.toString().length != length)
            for (var i = 0; i < length - string.toString().length; i++) {
                temp = '0' + temp
            }

        return temp
    },

    //16进制转10进制
    _hex2Dec(hexStr) {
        return parseInt(hexStr, 16)
    },

    //字符串转16进制
    strToHexCharCode(str) {
        if (str === "")
            return "";
        var hexCharCode = [];
        hexCharCode.push("0x");
        for (var i = 0; i < str.length; i++) {
            hexCharCode.push((str.charCodeAt(i)).toString(16));
        }
        return hexCharCode.join("");
    },

    //16进制转字符串
    hexCharCodeToStr(hexCharCodeStr) {
        var trimedStr = hexCharCodeStr.trim();
        var rawStr =
            trimedStr.substr(0, 2).toLowerCase() === "0x"
                ?
                trimedStr.substr(2)
                :
                trimedStr;
        var len = rawStr.length;
        if (len % 2 !== 0) {
            alert("Illegal Format ASCII Code!");
            return "";
        }
        var curCharCode;
        var resultStr = [];
        for (var i = 0; i < len; i = i + 2) {
            curCharCode = parseInt(rawStr.substr(i, 2), 16); // ASCII Code Value
            resultStr.push(String.fromCharCode(curCharCode));
        }
        return resultStr.join("");
    },

    //修改指定位置字符
    _replaceChat(source, pos, newChar) {
        if (pos < 0 || pos >= source.length || source.length == 0) {
            return "invalid parameters...";
        }
        var iBeginPos = 0, iEndPos = source.length;
        var sFrontPart = source.substr(iBeginPos, pos);
        var sTailPart = source.substr(pos + 1, source.length);
        var sRet = sFrontPart + newChar + sTailPart;
        return sRet;
    }
}