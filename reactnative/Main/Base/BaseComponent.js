import React, {Component} from 'react';

import {
    BackAndroid,
    BackHandler,
    Dimensions,
    PixelRatio, Platform, Keyboard, DeviceEventEmitter
} from 'react-native';

import Toast from 'react-native-root-toast';


class BaseComponent extends React.Component {

    mScreenWidth = Dimensions.get('window').width;

    mScreenHeight = Dimensions.get('window').height;

    //控件通用宽度
    viewCommonWith = this.mScreenWidth * 0.73


    isFullScreen = Platform.OS == 'ios' ? false : ((this.mScreenHeight / this.mScreenWidth) > 1.8 ? true : false)

    //最小显示单位
    mOnePixel = (PixelRatio.get() == 3 ? 2 : 1) / PixelRatio.get();

    //初始化常量
    static defaultProps = {
        //时间间隔  单位 毫秒
        Mode : 'Light',//'Light' 'Dark' 'Follow'
        isDark : true,//true false
        backColor : '#ffffff', //浅色背景色
        fontColor : '#222222', //深色字体色
    }

    constructor(props) {
        super(props);
        this.state = {
            keyboardHeight: 0
        }
    }

    /**
     * return 當前分辨率下的數值
     * @param {*} size 375标注图下的值
     */
    getSize(size) {
        return parseInt(this.mScreenWidth * size / 375);
    }

    calculateDP(dp) {
        return Platform.select({
            ios: dp,
            android: dp / 2.75 * 3
        });
    }

    debounce(fn, delay) {  
        let timer = null;  
        return function() {  
          clearTimeout(timer);  
          timer = setTimeout(() => {  
            fn.apply(this, arguments);  
          }, delay);  
        };  
      }

    useSize(pt, dp) {
        return Platform.select({
            ios: pt,
            android: dp / 2.75 * 3
        });
    }


    componentWillMount() {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener("hardwareBackPress", this.onBackClicked);
        } else {

        }
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') {
            BackHandler.removeEventListener("hardwareBackPress", this.onBackClicked);
        } else {

        }
    }


    //强制隐藏键盘
    dissmissKeyboard() {
        Keyboard.dismiss();
        //console.log("输入框当前焦点状态：" + this.refs.bottomInput.isFocused());
    }

    //返回 ;//return  true 表示返回上一页  false  表示跳出RN
    onBackClicked = () => { // 默认 表示跳出RN
        return false;
    }

    onShowToast = (message) => {
        Toast.show(message, {
            duration: Toast.durations.SHORT,
            position: Toast.positions.CENTER,// 弹窗位置，可以设置为CENTER、TOP、BOTTOM等 
            shadow: false,
            backgroundColor: 'rgba(122,122,122,0.9)',
            textColor: '#fff',
            onShow: () => {

            },
            onShown: () => {

            },
            onHide: () => {

            },
            onHidden: () => {

            }
        });
    }

    /*
    *根据具体宽高获取等比例高度
    * orgWidth 具体宽度
    * orgHeight 具体高度
    * targetWidth 目标宽度
    * return targetHeight 目标高度
     */
    _getHeightByWH(orgWidth, orgHeight, targetWidth) {
        var scale
        scale = (targetWidth * 0.1) / (orgWidth * 0.1)
        var targetHeight = orgHeight * scale

        return targetHeight
    }
}

export default BaseComponent;
