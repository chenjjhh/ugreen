import BaseComponent from '../../Main/Base/BaseComponent';
import React from 'react';
import {BackHandler, Platform, View} from 'react-native';
import TitleBar from '../View/TitleBar'
import AutoHeightWebView from 'react-native-autoheight-webview'

/*
*webview
 */

var that
export default class UrlWebView extends BaseComponent {
    static navigationOptions = ({navigation}) => {
        return {
            header:
                <TitleBar
                    titleText={navigation.state.params.title ? navigation.state.params.title : ''}
                    backgroundColor={'#FBFBFB'}
                    leftIcon={require('../../resources/back_black_ic.png')}
                    leftIconClick={() => {
                        that._goBack()
                    }}
                />
        };
    }

    constructor(props) {
        super(props);
        that = this
        this.state = {
            url: this.props.navigation.state.params.url,
            backButtonEnabled: false,
            progress: 0,
        }
    }

    componentWillMount() {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', this.onBackAndroid);
        }
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') {
            BackHandler.removeEventListener('hardwareBackPress', this.onBackAndroid);
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1
                }}>
                {this.state.progress != 1 ? this._progressBar() : null}
                <AutoHeightWebView
                    ref={'webview'}
                    onNavigationStateChange={this.onNavigationStateChange}
                    cacheEnabled={false}
                    javaScriptEnabled={true}
                    style={{
                        width: this.mScreenWidth,
                    }}
                    source={{uri: this.state.url.trim()}}
                    onShouldStartLoadWithRequest={(e) => {
                        var url = e.url

                        //一定要拦截非http链接，否则会有警告
                        if (url.indexOf("http") != -1 || url.indexOf("https") != -1) {
                            return true;
                        }

                        //不加载该请求
                        return false;

                    }
                    }
                    onLoadProgress={({nativeEvent}) => {
                        this.setState({progress: nativeEvent.progress})
                    }
                    }
                />
            </View>
        );
    }

    _progressBar() {
        return (
            <View style={{
                backgroundColor: '#E42000',
                height: 2,
                width: this.mScreenWidth * this.state.progress
            }}/>
        )
    }

    onNavigationStateChange = (navState) => {
        this.setState({
            backButtonEnabled: navState.canGoBack,
        });
    };

    onBackAndroid = () => {
        if (this.state.backButtonEnabled) {
            this.refs['webview'].goBack();
            return true;
        } else {
            return false;
        }
    };

    _goBack() {
        if (this.state.backButtonEnabled) {
            this.refs['webview'].goBack();
        } else {
            this.props.navigation.goBack();
        }
    }

}