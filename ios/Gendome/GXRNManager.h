//
//  GXRNManager.h
//  Gendome
//
//  Created by 朱迪龙 on 2022/5/29.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridge.h>

NS_ASSUME_NONNULL_BEGIN

@interface GXRNManager : RCTEventEmitter <RCTBridgeModule>

@end
NS_ASSUME_NONNULL_END
