//
//  GWDevice.h
//  GranwinAPKit
//
//  Created by (╹◡╹) on 2019/5/15.
//  Copyright © 2019 granwin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GWDevice : NSObject

@property (nonatomic, copy) NSString *pk;

@property (nonatomic, copy) NSString *mid;

@property (nonatomic, copy) NSString *mac;

@property (nonatomic, copy) NSString *fver;

@property (nonatomic, copy) NSString *productKey;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

- (NSDictionary *)getDictionaryFormat;

@end

NS_ASSUME_NONNULL_END
