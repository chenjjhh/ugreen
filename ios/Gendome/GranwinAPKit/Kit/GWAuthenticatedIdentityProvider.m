//
//  DeveloperAuthenticatedIdentityProvider.m
//  TianJiang
//
//  Created by 朱迪龙 on 2022/3/2.
//

#import "GWAuthenticatedIdentityProvider.h"

@implementation GWAuthenticatedIdentityProvider


- (AWSTask <NSString*> *) token {
    return [AWSTask taskWithResult:self.showToken];
}

@end
