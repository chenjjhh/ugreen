//
//  DeveloperAuthenticatedIdentityProvider.h
//  TianJiang
//
//  Created by 朱迪龙 on 2022/3/2.
//

#import <Foundation/Foundation.h>

#if __has_include(<AWSMobileClient/AWSMobileClient.h>)
#import <AWSMobileClient/AWSMobileClient.h>
#else
#import "AWSMobileClientXCF/AWSMobileClientXCF.h"
#endif


NS_ASSUME_NONNULL_BEGIN

@interface GWAuthenticatedIdentityProvider : AWSCognitoCredentialsProviderHelper

@property (nonatomic, copy) NSString *showToken;    ///< token

@end

NS_ASSUME_NONNULL_END
