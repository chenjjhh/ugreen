//
//  GWDevice.m
//  GranwinAPKit
//
//  Created by (╹◡╹) on 2019/5/15.
//  Copyright © 2019 granwin. All rights reserved.
//

#import "GWDevice.h"

@implementation GWDevice

- (instancetype)initWithDictionary:(NSDictionary *)dic {
    if (self = [super init]) {
        _pk = dic[@"PK"];
        _mid = dic[@"MID"];
        _mac = dic[@"MAC"];
        _fver = dic[@"FVER"];
        _productKey = dic[@"product_key"];
        
        if (!self.mid.length) {
            _mid = self.mac;
        }
    }
    return self;
}

- (NSDictionary *)getDictionaryFormat {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"PK"] = self.pk;
    dic[@"MID"] = self.mid;
    dic[@"MAC"] = self.mac;
    dic[@"FVER"] = self.fver;
    dic[@"product_key"] = self.productKey;
    return [NSDictionary dictionaryWithDictionary:dic];
}

@end
